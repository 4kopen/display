/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418preformatter.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STIH418PREFORMATTER
#define _STIH418PREFORMATTER

#include  <display/ip/tvout/stmpreformatter.h>

class CDisplayDevice;

#define TVO_COMPO_SYNC_SELECT          0x34
#define TVO_COMPO_SYNC_SELECT_MAIN_REF 0
#define TVO_COMPO_SYNC_SELECT_AUX_REF  0x10

class CSTiH418Preformatter: public CSTmPreformatter
{
public:
  CSTiH418Preformatter(const char                   *name,
                       CDisplayDevice               *pDev,
                       uint32_t                      ulPFRegs,
                       uint32_t                      capabilities = 0):CSTmPreformatter(name, pDev, ulPFRegs, capabilities)
                       {
                         /*
                          * For stiH418 the main preformatter does have an adaptive filter,
                          * we use distinguish main and aux PF and avoid having seperate class for main and aux PF
                          *
                          * following code does not work on VSOC as register is not available in TVOUT model
                          * TODO: activate code on real chip and when Cannes2.5 TVOUT model will be available.
                          */
                         /* if (m_ulCapabilities&(VOUT_PF_CAPS_ADF|VOUT_PF_CAPS_ADF)) */
                         /*   WriteReg(TVO_COMPO_SYNC_SELECT, TVO_COMPO_SYNC_SELECT_MAIN_REF); */
                         /* else  WriteReg(TVO_COMPO_SYNC_SELECT, TVO_COMPO_SYNC_SELECT_AUX_REF); */
                       }

private:
  CSTiH418Preformatter(const CSTiH418Preformatter&);
  CSTiH418Preformatter& operator=(const CSTiH418Preformatter&);
};

#endif //_STIH418PREFORMATTER
