/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418dvo.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STIH418DVO_H
#define _STIH418DVO_H

#include <display/ip/tvout/stmc8fdvo_v2_1.h>

#define TVOUT_DVO_EMB_DELAY_HD       (0)
#define TVOUT_DVO_EMB_DELAY_ED       (1)
#define TVOUT_DVO_EMB_DELAY_SD       (2)

#define TVOUT_DVO_EXT_DELAY_HD       (6)
#define TVOUT_DVO_EXT_DELAY_ED       (5)
#define TVOUT_DVO_EXT_DELAY_SD       (4)


class CSTmTVOutClkDivider;
class CSTmVTG;


class CSTiH418DVO: public CSTmC8FDVO_V2_1
{
public:
  CSTiH418DVO(CDisplayDevice              *pDev,
              COutput                     *pMainOutput,
              COutput                     *pAuxOutput,
              CSTmVTG                     *pMainVTG,
              CSTmVTG                     *pAuxVTG,
              CSTmClockLLA                *pClkDivider,
              CSTmVIP                     *pDVOVIP,
              const stm_display_sync_id_t *pSyncMap,
              uint32_t                     uDVOBase);

  bool Create(void);

  uint32_t SetControl(stm_output_control_t, uint32_t newVal);

private:
  CSTmClockLLA *m_pClkDivider;
  COutput      *m_pMainOutput;
  CSTmVTG      *m_pMainVTG;
  COutput      *m_pAuxOutput;
  CSTmVTG      *m_pAuxVTG;
  CSTmVTG      *m_pMasterVTG;

  const stm_display_sync_id_t *m_pTVOSyncMap;
  bool                         m_bClocksDisbaled;

  bool SetOutputFormat(const uint32_t format, const stm_display_mode_t* pModeLine);
  bool ConfigureClocks(const uint32_t format, const stm_display_mode_t* pModeLine);
  void DisableClocks(void);

  virtual bool SetVTGSyncs(const stm_display_mode_t* pModeLine);

  CSTiH418DVO(const CSTiH418DVO&);
  CSTiH418DVO& operator=(const CSTiH418DVO&);
};


#endif /* _STIH418DVO_H */
