/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418hdmi.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STIH418HDMI_H
#define _STIH418HDMI_H

#include <display/ip/hdmi/stmhdmi.h>

class COutput;
class CSTmClockLLA;
class CSTmVIP;
class CSTmVTG;

class CSTiH418HDMI: public CSTmHDMI
{
public:
  CSTiH418HDMI(CDisplayDevice      *pDev,
               COutput             *pMainOutput,
               COutput             *pAuxOutput,
               CSTmVTG             *pMainVTG,
               CSTmVTG             *pAuxVTG,
               CSTmVIP             *pHDMIVIP,
               stm_clock_t         *clk_src_map,
               uint32_t             clk_src_mapsize,
       const stm_display_sync_id_t *pSyncMap);

  virtual ~CSTiH418HDMI(void);

  bool Create(void);

  uint32_t SetControl(stm_output_control_t ctrl, uint32_t newVal);
  bool SetVTGSyncs(const stm_display_mode_t *);

private:
  uint32_t m_uAudioClkOffset;
  uint32_t m_uUniplayerHDMIOffset;

  static stm_clock_t   m_pClockOut[];
  stm_clock_t         *m_SrcClkMap;
  uint32_t             m_SrcClkMapSize;
  CSTmClockLLA        *m_pClkDivider;
  CSTmVIP             *m_pVIP;
  CSTmVTG             *m_pMasterVTG;
  COutput             *m_pMainOutput;
  COutput             *m_pAuxOutput;
  CSTmVTG             *m_pMainVTG;
  CSTmVTG             *m_pAuxVTG;

  bool                 m_bClocksDisbaled;

  const stm_display_sync_id_t *m_pTVOSyncMap;

  bool SetOutputFormat(uint32_t format);
  void SetSignalRangeClipping(void);

  int  GetAudioFrequency(void);
  void GetAudioHWState(stm_hdmi_audio_state_t *);

  void DisableClocks(void);
  bool PreConfiguration(const stm_display_mode_t*);
  bool PostConfiguration(const stm_display_mode_t*);
  bool IntermediateConfiguration(void);

  void WriteUniplayerReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pDevRegs, (m_uUniplayerHDMIOffset+reg), val); }
  uint32_t ReadUniplayerReg(uint32_t reg) { return vibe_os_read_register(m_pDevRegs, (m_uUniplayerHDMIOffset+reg)); }

  void WriteAudioClkReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pDevRegs, (m_uAudioClkOffset+reg), val); }
  uint32_t ReadAudioClkReg(uint32_t reg) { return vibe_os_read_register(m_pDevRegs, (m_uAudioClkOffset+reg)); }

  CSTiH418HDMI(const CSTiH418HDMI&);
  CSTiH418HDMI& operator=(const CSTiH418HDMI&);

};

#endif //_STIH418HDMI_H
