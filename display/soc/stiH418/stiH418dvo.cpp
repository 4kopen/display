/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418dvo.cpp
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stmdisplay.h>
#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/ip/tvout/stmvip.h>

#include <display/ip/displaytiming/stmclocklla.h>
#include <display/ip/displaytiming/stmvtg.h>

#include "stiH418device.h"
#include "stiH418dvo.h"
#include "stiH418reg.h"

#define VTG_DVO_SYNC_ID   m_pTVOSyncMap[TVO_VIP_SYNC_DVO_IDX]

CSTiH418DVO::CSTiH418DVO(
  CDisplayDevice              *pDev,
  COutput                     *pMainOutput,
  COutput                     *pAuxOutput,
  CSTmVTG                     *pMainVTG,
  CSTmVTG                     *pAuxVTG,
  CSTmClockLLA                *pClkDivider,
  CSTmVIP                     *pDVOVIP,
  const stm_display_sync_id_t *pSyncMap,
  uint32_t                     uDVOBase): CSTmC8FDVO_V2_1( "fdvo0",
                                                  STiH418_OUTPUT_IDX_DVO,
                                                  pDev,
                                                  pDVOVIP,
                                                  uDVOBase,
                                                  pMainOutput)
{
  OUTPUT_TRCIN( TRC_ID_DVO, "" );

  m_pClkDivider = pClkDivider;
  m_pMainOutput = pMainOutput;
  m_pMainVTG    = pMainVTG;
  m_pAuxOutput  = pAuxOutput;
  m_pAuxVTG     = pAuxVTG;
  m_pTVOSyncMap = pSyncMap;

  m_pMasterOutput = m_pMainOutput;
  m_pMasterVTG    = m_pMainVTG;

  m_bClocksDisbaled = true;

  OUTPUT_TRCOUT( TRC_ID_DVO, "" );
}

bool CSTiH418DVO::Create(void)
{
  OUTPUT_TRCIN( TRC_ID_DVO, "" );

  if (!CSTmC8FDVO_V2_1::Create())
    return false;

  /*
   * TODO: Setup the SAS VTG sync outputs to the FlexDVO formatter.
   */

  OUTPUT_TRCOUT( TRC_ID_DVO, "" );
  return true;
}

void CSTiH418DVO::DisableClocks(void)
{
  stm_clk_divider_output_name_t   pixelclock = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?STM_CLK_PIX_MAIN:STM_CLK_PIX_AUX;

  OUTPUT_TRCIN( TRC_ID_DVO, "" );

  if(m_bClocksDisbaled)
    goto exit;

  OUTPUT_TRC( TRC_ID_DVO, "Disabling DVO Clocks" );
  m_pClkDivider->Disable(STM_CLK_PIX_DVO);
  m_pClkDivider->Disable(STM_CLK_OUT_DVO);
  m_pClkDivider->Disable(pixelclock);

  m_bClocksDisbaled = true;

exit:
  OUTPUT_TRCOUT( TRC_ID_DVO, "" );
}

bool CSTiH418DVO::SetVTGSyncs(const stm_display_mode_t* pModeLine)
{
  OUTPUT_TRCIN( TRC_ID_DVO, "" );

  uint32_t tvStandard = pModeLine->mode_params.output_standards;
  int dvo_sync_shift = 0;

  if(!m_pTVOSyncMap)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "- failed : Undefined Syncs mapping !!" );
    return false;
  }

  if(m_sAwgCodeParams.bAllowEmbeddedSync)
  {
    /* delay caused by AWG firmware is equal to HSync front porch */
    int trailingPixels = pModeLine->mode_timing.pixels_per_line - (pModeLine->mode_params.active_area_start_pixel + pModeLine->mode_params.active_area_width);

    if(tvStandard & (STM_OUTPUT_STD_HD_MASK | STM_OUTPUT_STD_XGA))
    {
      dvo_sync_shift = TVOUT_DVO_EMB_DELAY_HD - trailingPixels;
    }
    else if(tvStandard & (STM_OUTPUT_STD_ED_MASK | STM_OUTPUT_STD_VGA))
    {
      dvo_sync_shift = TVOUT_DVO_EMB_DELAY_ED - trailingPixels;
    }
    else if(tvStandard & STM_OUTPUT_STD_SD_MASK)
    {
      dvo_sync_shift = TVOUT_DVO_EMB_DELAY_SD - trailingPixels;
    }
    m_pMasterVTG->SetSyncType(VTG_DVO_SYNC_ID, STVTG_SYNC_POSITIVE);
  }
  else
  {
    if(tvStandard & (STM_OUTPUT_STD_HD_MASK | STM_OUTPUT_STD_XGA))
    {
      dvo_sync_shift = TVOUT_DVO_EXT_DELAY_HD;
    }
    else if(tvStandard & (STM_OUTPUT_STD_ED_MASK | STM_OUTPUT_STD_VGA))
    {
      dvo_sync_shift = TVOUT_DVO_EXT_DELAY_ED;
    }
    else if(tvStandard & STM_OUTPUT_STD_SD_MASK)
    {
      dvo_sync_shift = TVOUT_DVO_EXT_DELAY_SD;
    }
    m_pMasterVTG->SetSyncType(VTG_DVO_SYNC_ID, STVTG_SYNC_TIMING_MODE);
  }

  if(pModeLine->mode_params.scan_type == STM_INTERLACED_SCAN)
    m_pMasterVTG->SetBnottopType(VTG_DVO_SYNC_ID, STVTG_SYNC_BNOTTOP_INVERTED);
  else
    m_pMasterVTG->SetBnottopType(VTG_DVO_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
  m_pMasterVTG->SetVSyncHOffset(VTG_DVO_SYNC_ID, dvo_sync_shift);
  m_pMasterVTG->SetHSyncOffset(VTG_DVO_SYNC_ID, dvo_sync_shift);

  OUTPUT_TRCOUT( TRC_ID_DVO, "" );
  return true;
}

uint32_t CSTiH418DVO::SetControl(stm_output_control_t ctrl, uint32_t newVal)
{
  switch(ctrl)
  {
    case OUTPUT_CTRL_VIDEO_SOURCE_SELECT:
    {
      if((newVal != STM_VIDEO_SOURCE_MAIN_COMPOSITOR)&&(newVal != STM_VIDEO_SOURCE_AUX_COMPOSITOR))
        return STM_OUT_INVALID_VALUE;

      if(m_bIsStarted)
        return STM_OUT_BUSY;

      m_VideoSource = static_cast<stm_display_output_video_source_t>(newVal);
      m_pMasterOutput->UnRegisterSlavedOutput(this);
      m_pMasterVTG = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?m_pMainVTG:m_pAuxVTG;
      m_pMasterOutput = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?m_pMainOutput:m_pAuxOutput;
      m_pMasterOutput->RegisterSlavedOutput(this);
      SetOutputFormat(m_ulOutputFormat,m_pMasterOutput->GetCurrentDisplayMode());
      break;
    }
    default:
      return CSTmC8FDVO_V2_1::SetControl(ctrl, newVal);
  }

  return 0;
}

bool CSTiH418DVO::ConfigureClocks(const uint32_t format, const stm_display_mode_t* pModeLine)
{
  stm_clk_divider_output_source_t source = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?STM_CLK_SRC_HD:STM_CLK_SRC_SD;
  stm_clk_divider_output_name_t   pixelclock = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?STM_CLK_PIX_MAIN:STM_CLK_PIX_AUX;
  uint32_t out_dvo_rate = 0;
  uint32_t pix_dvo_rate = 0;

  OUTPUT_TRCIN( TRC_ID_DVO, "" );

  pix_dvo_rate = m_pClkDivider->GetRate(pixelclock);
  if(pix_dvo_rate != pModeLine->mode_timing.pixel_clock_freq)
  {
    pix_dvo_rate = pModeLine->mode_timing.pixel_clock_freq;
    m_pClkDivider->SetParent(pixelclock, source);
    m_pClkDivider->SetRate(pixelclock, pix_dvo_rate);
  }

  /*
   * Enable Pixel Clock
   */
  m_pClkDivider->Enable(pixelclock);

  switch(format)
  {
    case (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_24BIT):
    case (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_422 | STM_VIDEO_OUT_16BIT):
    case (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_444 | STM_VIDEO_OUT_24BIT):
    {
      out_dvo_rate = pix_dvo_rate;
      OUTPUT_TRC( TRC_ID_DVO, "FDVO clock == PIX clock" );
      break;
    }
    case (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_444 | STM_VIDEO_OUT_16BIT):
    case (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_ITUR656):
    {
      out_dvo_rate = 2 * pix_dvo_rate;
      OUTPUT_TRC( TRC_ID_DVO, "FDVO clock == 2xPIX clock" );
      break;
    }
    default:
      OUTPUT_TRC( TRC_ID_ERROR, "Invalid format requested" );
      return false;
  }

  /*
   * Set DVO clocks.
   */
  m_pClkDivider->SetParent(STM_CLK_PIX_DVO, source);
  m_pClkDivider->SetRate(STM_CLK_PIX_DVO, pix_dvo_rate);
  m_pClkDivider->Enable(STM_CLK_PIX_DVO);
  m_pClkDivider->SetParent(STM_CLK_OUT_DVO, source);
  m_pClkDivider->SetRate(STM_CLK_OUT_DVO, out_dvo_rate);
  m_pClkDivider->Enable(STM_CLK_OUT_DVO);

  m_bClocksDisbaled = false;

  OUTPUT_TRCOUT( TRC_ID_DVO, "" );
  return true;
}

bool CSTiH418DVO::SetOutputFormat(const uint32_t format,const stm_display_mode_t* pModeLine)
{
  bool ret = true;
  OUTPUT_TRCIN( TRC_ID_DVO, "" );

  if(format == STM_VIDEO_OUT_NONE) /* Disable Output */
  {
    this->Stop();
    return true;
  }

  if(pModeLine)
  {
    /*
     * If the output is already running, work out if we can setup the
     * required clock divides for the new format before setting it.
     */
    ret = ConfigureClocks(format, pModeLine);
  }

  if(ret)
    ret = CSTmC8FDVO_V2_1::SetOutputFormat(format, pModeLine);

  if(ret)
    OUTPUT_TRCOUT( TRC_ID_DVO, "" );
  else
    OUTPUT_TRCOUT( TRC_ID_ERROR, "failed" );

  return ret;
}
