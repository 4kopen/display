/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#ifndef _STIH418_HQVDP_LITE_PLANE_H
#define _STIH418_HQVDP_LITE_PLANE_H

#include <display/ip/hqvdplite/HqvdpLitePlaneV2.h>

class CSTiH418HqvdpLitePlane: public CHqvdpLitePlaneV2
{
public:

  static bool Initialize(
                  const void*                pDisplayDevBase,
                  int                        hqvdpHwNb,
                  const HqvdpLiteHwInfo_s    hqvdpHwInfo[])
  {
      return CHqvdpLitePlaneV2::Initialize(pDisplayDevBase, hqvdpHwNb, hqvdpHwInfo);
  }

  static bool Terminate()
  {
      return CHqvdpLitePlaneV2::Terminate();
  }

  CSTiH418HqvdpLitePlane(
                  const char*                pName,
                  uint32_t                   id,
                  const CDisplayDevice*      pDev,
                  HqvdpLiteProfile_e         profile,
                  bool                       isTemporalDeiAllowed,
                  stm_plane_capabilities_t   caps,
                  const char*                procClockName): CHqvdpLitePlaneV2(pName,
                                                                               id,
                                                                               pDev,
                                                                               profile,
                                                                               isTemporalDeiAllowed,
                                                                               caps,
                                                                               "CLK_PROC_MIXER",
                                                                               procClockName)
  {
    /*
     * All HQVDPs hardware are sharing the same 'CLK_PROC_MIXER' pixel clock.
     * The pixel clock rate is fixed (set by the targetpack) and shouldn't be
     * tuned by the driver.
     *
     * The 'CLK_MAIN_DISP' processing clock is used by hqvdp_10 and hqvdp_11
     * hardware instance while 'CLK_AUX_DISP' processing clocks is for
     * hqvdp_12 hardware instance.
     *
     * In case of plane managing two HQVDP hardware instances (Dual Plane)
     * the clock gating is ensured by SYSTEM_CONFIG (5212/5213/5214) registers.
     *
     *  |----------------------------------|    |------------|-----------|
     *  |         |                |       |    |            |           |
     *  |         |    HQVDP HW    |       |    |    MIXER   |           |
     *  |clk_stbus|                |clk_pix|--->|            |clk_mix_pix|
     *  |         |----------------|       |    |------------|           |
     *  |         |clk_proc/clk_iqi|       |    |clk_mix_proc|           |
     *  |----------------------------------|    |------------|-----------|
     *
     * * clk_stbus      <-----+ CLK_ICN_REG
     * * clk_proc       <--|--+ CLK_MAIN_DISP / CLK_AUX_DISP
     * * clk_iqi        <--|
     * * clk_pix        <--|--+ CLK_PROC_MIXER
     * * clk_mix_proc   <--|
     * * clk_mix_pix    <-----+ CLK_PIX_MAIN_DISP / CLK_PIX_AUX_DISP
     */
    m_hasADedicatedPixelClock = false;

    // For scaling computations, the reference use case must be computed
    // with stream actual bit depth
    m_bUse8BitsSrcForRefUseCase = false;
  }

private:
  CSTiH418HqvdpLitePlane(const CSTiH418HqvdpLitePlane &);
  CSTiH418HqvdpLitePlane& operator= (const CSTiH418HqvdpLitePlane &);
};

#endif // _STIH418_HQVDP_LITE_PLANE_H
