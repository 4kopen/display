/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418mainoutput.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STiH418MAINOUTPUT_H
#define _STiH418MAINOUTPUT_H

#include <display/ip/tvout/stmmaintvoutput.h>
#include <display/ip/misr/stmmisrmaintvout.h>

#include "stiH418reg.h"

class CDisplayDevice;
class CGammaMixer;
class CSTmClockLLA;
class CSTmTVOutDENC;
class CSTmMisrMainTVOut;


class CSTiH418MainOutput: public CSTmMainTVOutput
{
public:
  CSTiH418MainOutput( CDisplayDevice         *pDev,
                      CSTmVTG                *pVTG,
                      CSTmTVOutDENC          *pDENC,
                      CGammaMixer            *pMixer,
                      CSTmFSynth             *pFSynth,
                      CSTmHDFormatter        *pHDFormatter,
                      CSTmClockLLA           *pClkDivider,
                      CSTmVIP                *pHDFVIP,
                      CSTmVIP                *pDENCVIP,
                const stm_display_sync_id_t  *sync_sel_map);

  virtual ~CSTiH418MainOutput();

  uint32_t SetControl(stm_output_control_t, uint32_t newVal);

  // Public interface for MISR collection
  virtual TuningResults SetTuning( uint16_t service,
                                   void *inputList,
                                   uint32_t inputListSize,
                                   void *outputList,
                                   uint32_t outputListSize);

  const char* GetPixelClockName() { return "CLK_PIX_MAIN_DISP"; }

  bool SetMixerMap(const CDisplayPlane * plane, uint32_t mixerIDs);

protected:
  // Virtual output method implementation required by base class
  bool SetVTGSyncs(const stm_display_mode_t *mode);
  bool ConfigureOutput(uint32_t format);
  void ConfigureDisplayClocks(const stm_display_mode_t *mode, const uint32_t format);
  void DisableClocks(void);

  void EnableDACs(void);
  void DisableDACs(void);
  void PowerDownHDDACs(void);
  void PowerDownDACs(void);

private:
  CSTmFSynth                  *m_pFSynth;
  CSTmClockLLA                *m_pClkDivider;
  CSTmVIP                     *m_pHDFVIP;
  CSTmVIP                     *m_pDENCVIP;
  const stm_display_sync_id_t *m_sync_sel_map;

  bool                         m_bClocksDisbaled;

  // Private MISR implementation
  CSTmMisrMainTVOut           *m_pMisrMain;
  virtual void SetMisrData(const stm_time64_t LastVTGEvtTime, uint32_t  LastVTGEvt);
  virtual void UpdateMisrCtrl(void);

  stm_clk_divider_output_divide_t m_clk_divider;
  void ConfigureClockDivider(const stm_display_mode_t *mode);

  // Hardware specific register access

  void     WriteReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pDevReg, (STiH418_TVOUT1_BASE + reg), val); }
  uint32_t ReadReg(uint32_t reg)                { return vibe_os_read_register(m_pDevReg, (STiH418_TVOUT1_BASE +reg)); }

  void WriteBitField(uint32_t RegAddr, int Offset, int Width, uint32_t bitFieldValue)
  {
    #define BIT_FIELD_MASK        (((unsigned int)0xffffffff << (Offset+Width)) | ~((unsigned int)0xffffffff << Offset))
    WriteReg(RegAddr, ( (ReadReg( RegAddr ) & BIT_FIELD_MASK) | ((bitFieldValue << Offset) & ~BIT_FIELD_MASK) ));
  }

  void     WriteSysCfgReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pDevReg, (STiH418_SYSCFG_CORE + reg), val); }
  uint32_t ReadSysCfgReg(uint32_t reg)                { return vibe_os_read_register(m_pDevReg, (STiH418_SYSCFG_CORE +reg)); }

  CSTiH418MainOutput(const CSTiH418MainOutput&);
  CSTiH418MainOutput& operator=(const CSTiH418MainOutput&);
};


#endif //_STiH418MAINOUTPUT_H
