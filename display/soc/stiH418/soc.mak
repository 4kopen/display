# Classes for STiH418 device
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/soc/stiH418/,               \
                        stiH418hdmi.cpp                                        \
                        stiH418hdmiphy.cpp                                     \
                        stiH418mainoutput.cpp                                  \
                        stiH418auxoutput.cpp                                   \
                        stiH418denc.cpp                                        \
                        stiH418mixer.cpp                                       \
                        stiH418device.cpp)
ifneq ($(CONFIG_FDVO),)
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/soc/stiH418/, stiH418dvo.cpp)
endif

