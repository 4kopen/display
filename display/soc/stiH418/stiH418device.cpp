/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-09-11
***************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplaySource.h>

#include <display/ip/displaytiming/stmfsynthlla.h>
#include <display/ip/displaytiming/stmclocklla.h>
#include <display/ip/displaytiming/stmc8vtg_v8_4.h>

#include <display/ip/tvout/stmvip.h>
#include <display/ip/tvout/stmtvoutteletext.h>
#include <display/ip/tvout/stmpreformatter.h>
#include <display/ip/hdf/stmhdf_v5_0.h>
#include <display/ip/videoPlug/VideoPlug.h>
#include <display/ip/gdpplus/GdpPlus.h>

#include <display/ip/queuebufferinterface/PureSwQueueBufferInterface.h>

#include <display/ip/gdp/VBIPlane.h>

#include "stiH418reg.h"
#include "stiH418device.h"
#include "stiH418mainoutput.h"
#include "stiH418auxoutput.h"
#include "stiH418denc.h"
#include "stiH418preformatter.h"
#include "stiH418hdmi.h"
#include "stiH418mixer.h"
#include "stiH418gdp.h"
#include "stiH418dvo.h"
#include "stiH418hqvdpliteplane.h"

static const bool STM_SYSTEM_RESUME_ON_HDMI_EVENTS = true;

static const int STM_C8VTG_NUM_SYNC_OUTPUTS = 6;

static const stm_display_sync_id_t  tvo_vip_sync_sel_map_vga_dvo_pads[] =
{
    STM_SYNC_SEL_1          /* TVO_VIP_SYNC_DENC_IDX */
  , STM_SYNC_SEL_2          /* TVO_VIP_SYNC_SDDCS_IDX */
  , STM_SYNC_SEL_REF        /* TVO_VIP_SYNC_SDVTG_IDX */
  , STM_SYNC_SEL_REF        /* TVO_VIP_SYNC_TTXT_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_HDF_IDX */
  , STM_SYNC_SEL_4          /* TVO_VIP_SYNC_HDDCS_IDX */
  , STM_SYNC_SEL_5          /* TVO_VIP_SYNC_HDMI_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_DVO_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_DVO_HS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_DVO_VS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_DVO_BNOT_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_DVO_HS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_DVO_VS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_HS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_VS_PAD_IDX */
};

static const stm_display_sync_id_t  tvo_vip_sync_sel_map[] =
{
    STM_SYNC_SEL_1          /* TVO_VIP_SYNC_DENC_IDX */
  , STM_SYNC_SEL_2          /* TVO_VIP_SYNC_SDDCS_IDX */
  , STM_SYNC_SEL_REF        /* TVO_VIP_SYNC_SDVTG_IDX */
  , STM_SYNC_SEL_REF        /* TVO_VIP_SYNC_TTXT_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_HDF_IDX */
  , STM_SYNC_SEL_4          /* TVO_VIP_SYNC_HDDCS_IDX */
  , STM_SYNC_SEL_5          /* TVO_VIP_SYNC_HDMI_IDX */
  , STM_SYNC_SEL_6          /* TVO_VIP_SYNC_DVO_IDX */
  , STM_SYNC_SEL_6          /* TVO_VIP_SYNC_DVO_HS_PAD_IDX */
  , STM_SYNC_SEL_6          /* TVO_VIP_SYNC_DVO_VS_PAD_IDX */
  , STM_SYNC_SEL_6          /* TVO_VIP_SYNC_DVO_BNOT_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_DVO_HS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_DVO_VS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_HS_PAD_IDX */
  , STM_SYNC_SEL_3          /* TVO_VIP_SYNC_VGA_VS_PAD_IDX */
};

/* Clockgen D2 (Video display and output stage) */
stm_clock_t CSTiH418Device:: m_pClockFs[] = {
  { STM_CLK_SRC_HD,     "CLK_D2_FS0"          },
  { STM_CLK_SRC_SD,     "CLK_D2_FS1"          },
  { STM_CLK_SRC_IN_0,   "CLK_TMDSOUT_HDMI"    },
};

stm_clock_t CSTiH418Device::m_pClockOut[] = {
  /* TVOUT */
//{ STM_CLK_PIX_MAIN_4K,"CLK_PIX_MAIN_4K"     },
  { STM_CLK_PIX_MAIN,   "CLK_PIX_MAIN_DISP"   },
  { STM_CLK_PIX_HDDACS, "CLK_PIX_HDDAC"       },
  { STM_CLK_OUT_HDDACS, "CLK_HDDAC"           },
  { STM_CLK_PIX_AUX,    "CLK_PIX_AUX_DISP"    },
  { STM_CLK_DENC,       "CLK_DENC"            },
  { STM_CLK_OUT_SDDACS, "CLK_SDDAC"           },
  /* DVO */
  { STM_CLK_PIX_DVO,    "CLK_PIX_DVO"         },
  { STM_CLK_OUT_DVO,    "CLK_DVO"             },
};

static char sourceNames[STiH418_SOURCE_COUNT][10];

CSTiH418Device::CSTiH418Device(stm_device_configuration_t *DevConfig): CDisplayDevice(0, STiH418_OUTPUT_COUNT, STiH418_PLANE_COUNT, STiH418_SOURCE_COUNT)

{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  m_pReg = (uint32_t*)vibe_os_map_memory(STiH418_REGISTER_BASE, STiH418_REG_ADDR_SIZE);
  ASSERTF(m_pReg,("CSTiH418Device::CSTiH418Device 'unable to map device registers'\n"));

  TRC( TRC_ID_MAIN_INFO, "Register remapping 0x%08x -> 0x%08x", STiH418_REGISTER_BASE,(uint32_t)m_pReg );

  /*
   * Now setup the basic chip config and reset state
   */
  PowerOnSetup();

  /*
   * Global setup of the display device.
   */
  m_devConfig      = *DevConfig;

  m_pFSynthHD      = 0;
  m_pFSynthSD      = 0;

  m_pClkDivider    = 0;

  m_pMainVTG       = 0;
  m_pAuxVTG        = 0;

  m_pMainMixer     = 0;
  m_pAuxMixer      = 0;

  m_pVideoPlug1    = 0;
  m_pVideoPlug2    = 0;
  m_pVideoPlug3    = 0;

  m_bVGAUsingDVOPads  = false;
  m_pHDFormatter   = 0;

  m_pMainPreformatter = 0;
  m_pAuxPreformatter  = 0;

  m_pDENCVIP       = 0;
  m_pHDFVIP        = 0;
  m_pHDMIVIP       = 0;
  m_pDVOVIP        = 0;

  m_pDENC          = 0;
  m_pTeletext      = 0;

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


CSTiH418Device::~CSTiH418Device()
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  RemovePlanes();
  CSTiH418HqvdpLitePlane::Terminate();
  CGDPPlus::DeInit();
  RemoveSources();
  RemoveOutputs();

  delete m_pFSynthHD;
  m_pFSynthHD = NULL;
  delete m_pFSynthSD;
  m_pFSynthSD = NULL;
  delete m_pClkDivider;
  m_pClkDivider = NULL;

  delete m_pMainVTG;
  m_pMainVTG = NULL;
  delete m_pAuxVTG;
  m_pAuxVTG = NULL;

  delete m_pMainMixer;
  m_pMainMixer = NULL;
  delete m_pAuxMixer;
  m_pAuxMixer = NULL;

  delete m_pVideoPlug1;
  m_pVideoPlug1 = NULL;
  delete m_pVideoPlug2;
  m_pVideoPlug2 = NULL;
  delete m_pVideoPlug3;
  m_pVideoPlug3 = NULL;

  delete m_pHDFormatter;
  m_pHDFormatter = NULL;

  delete m_pMainPreformatter;
  m_pMainPreformatter = NULL;
  delete m_pAuxPreformatter;
  m_pAuxPreformatter = NULL;

  delete m_pDENCVIP;
  m_pDENCVIP = NULL;
  delete m_pHDFVIP;
  m_pHDFVIP = NULL;
  delete m_pHDMIVIP;
  m_pHDMIVIP = NULL;
  delete m_pDVOVIP;
  m_pDVOVIP = NULL;

  delete m_pDENC;
  m_pDENC = NULL;
  if(CONFIG_TVOUT_TELETEXT)
  {
    delete m_pTeletext;
    m_pTeletext = NULL;
  }

  PowerDown();

  vibe_os_unmap_memory(m_pReg);
  m_pReg = 0;

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


void CSTiH418Device::PowerOnSetup(void)
{
  uint32_t tmp;
  TRCIN( TRC_ID_MAIN_INFO, "" );

  tmp = ReadDevReg(STiH418_SYSCFG_CORE + SYS_CFG5131);
  tmp |= (SYS_CFG5131_RST_N_HDTVOUT);
  WriteDevReg(STiH418_SYSCFG_CORE + SYS_CFG5131, tmp);


  tmp = ReadDevReg(STiH418_SYSCFG_SBC_x + SYS_CFG4121);
  if((tmp & 0xA) != 0xA)
  {
    tmp |= 0xA;
    WriteDevReg(STiH418_SYSCFG_SBC_x + SYS_CFG4121, tmp);
  }


  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


void CSTiH418Device::PowerDown(void)
{
  uint32_t tmp;

  if(m_bIsSuspended)
    return;

  TRCIN( TRC_ID_MAIN_INFO, "" );

  /*
   * Unfortunately we have only one bit controlling the whole
   * power of the HDTVOUT blocks but not seperate bits like Orly2
   * So do not power down HDTVOUT if you still need the HDMI block
   * to wake up system on HPD event
   */
  if(STM_SYSTEM_RESUME_ON_HDMI_EVENTS)
    return;

  tmp = ReadDevReg(STiH418_SYSCFG_CORE + SYS_CFG5131);
  tmp &= ~SYS_CFG5131_RST_N_HDTVOUT;
  WriteDevReg(STiH418_SYSCFG_CORE + SYS_CFG5131, tmp);

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


bool CSTiH418Device::Create()
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(!CDisplayDevice::Create())
  {
    TRC( TRC_ID_ERROR, "Base class Create failed" );
    return false;
  }

  if(!CreateClocks())
    return false;

  if(!CreateInfrastructure())
    return false;

  if(!CreatePlanes())
    return false;

  if(!CreateSources())
    return false;

  if(!CreateOutputs())
    return false;

  /*
   * Now disable all clocks except SPARE clocks if we don't use splashscreen
   */
  if(!BypassHwInitialization())
  {
    for (uint32_t i=0; i<N_ELEMENTS(m_pClockOut); i++)
    {
      stm_clk_divider_output_name_t clk_name = static_cast<stm_clk_divider_output_name_t>(m_pClockOut[i].id);
      if(clk_name != STM_CLK_SPARE)
        m_pClkDivider->Disable(clk_name);
    }
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );

  return true;
}


bool CSTiH418Device::CreateClocks(void)
{

  if((m_pClkDivider = new CSTmClockLLA(m_pClockFs, N_ELEMENTS(m_pClockFs), m_pClockOut, N_ELEMENTS(m_pClockOut), this)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create ClkDivider" );
    return false;
  }

  if((m_pFSynthHD = new CSTmFSynthLLA("CLK_D2_FS0", "CLK_PIX_MAIN_DISP")) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create FSynthHD" );
    return false;
  }

  if((m_pFSynthSD = new CSTmFSynthLLA("CLK_D2_FS1", "CLK_PIX_AUX_DISP")) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create FSynthSD" );
    return false;
  }

  return true;
}


bool CSTiH418Device::CreateInfrastructure(void)
{
  if((m_pMainVTG = new CSTmC8VTG_V8_4("VTG-Main", this, STiH418_VTG_MAIN_BASE, STM_C8VTG_NUM_SYNC_OUTPUTS, m_pFSynthHD, false, STVTG_SYNC_POSITIVE, true, false)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create Main VTG" );
    return false;
  }

  if((m_pAuxVTG = new CSTmC8VTG_V8_4("VTG-Aux", this, STiH418_VTG_AUX_BASE, STM_C8VTG_NUM_SYNC_OUTPUTS, m_pFSynthSD, false, STVTG_SYNC_POSITIVE, true, false)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create Aux VTG" );
    return false;
  }

  if(CONFIG_TVOUT_TELETEXT)
  {
    m_pTeletext = new CSTmTVOutTeletext(this, STiH418_DENC_BASE,
                                              STiH418_TVO_TTXT_BASE,
                                              STiH418_REGISTER_BASE,
                                              SDTP_DENC1_TELETEXT);
    /*
     * Slight difference for Teletext, if we fail to create it then clean up
     * and continue. We may not have been able to claim an FDMA channel and we
     * are not treating that as fatal.
     */
    if(m_pTeletext && !m_pTeletext->Create())
    {
      TRC( TRC_ID_ERROR, "failed to create Teletext" );
      delete m_pTeletext;
      m_pTeletext = 0;
    }
  }

  m_pDENC = new CSTiH418DENC(this, STiH418_DENC_BASE, m_pTeletext);
  if(!m_pDENC || !m_pDENC->Create())
  {
    TRC( TRC_ID_ERROR, "failed to create DENC" );
    return false;
  }

  /*
   * Capability VOUT_PF_CAPS_COMPO_SYNC_SELECT not used as this feature is not implemented yet.
   * TODO: confirm if we need to adjust VTG syncs feeding the compo, if so implement this feature.
   */
  m_pMainPreformatter = new CSTiH418Preformatter("PF-Main", this, STiH418_TVO_MAIN_PF_BASE,
                                                       VOUT_PF_CAPS_ADF|VOUT_PF_CAPS_VDF);
  if(m_pMainPreformatter == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create Main PreFormatter" );
    return false;
  }

  m_pAuxPreformatter = new CSTiH418Preformatter("PF-Aux", this, STiH418_TVO_AUX_PF_BASE);
  if(m_pAuxPreformatter == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create Aux PreFormatter" );
    return false;
  }

  m_bVGAUsingDVOPads = vibe_os_of_property_read_bool("vga_using_dvo_pads");

  uint32_t dvo_sync_type = (TVO_VIP_SYNC_TYPE_DVO|TVO_VIP_SYNC_TYPE_DVO_BNOT_PAD);
  uint32_t hdf_sync_type = (TVO_VIP_SYNC_TYPE_HDF | TVO_VIP_SYNC_TYPE_HDDCS);

  if (m_bVGAUsingDVOPads)
  {
    hdf_sync_type |= (TVO_VIP_SYNC_TYPE_VGA_DVO_HS_PAD|TVO_VIP_SYNC_TYPE_VGA_DVO_VS_PAD);
  }
  else
  {
    dvo_sync_type |= (TVO_VIP_SYNC_TYPE_DVO_HS_PAD|TVO_VIP_SYNC_TYPE_DVO_VS_PAD);
    hdf_sync_type |= (TVO_VIP_SYNC_TYPE_VGA_HS_PAD|TVO_VIP_SYNC_TYPE_VGA_VS_PAD);
  }

  if((m_pDENCVIP = new CSTmVIP( "VIP-DENC",
                                this, STiH418_TVO_VIP_DENC_BASE
                                    , m_pMainPreformatter
                                    , m_pAuxPreformatter
                                    , (TVO_VIP_SYNC_TYPE_DENC | TVO_VIP_SYNC_TYPE_SDDCS | TVO_VIP_SYNC_TYPE_SDVTG | TVO_VIP_SYNC_TYPE_TTXT)
                                    , (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads
                                    , TVO_VIP_INPUT_IS_INVERTED | TVO_VIP_BYPASS_INPUT_IS_RGB)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create DENC VIP" );
    return false;
  }

  if((m_pHDFVIP  = new CSTmVIP( "VIP-HDF",
                               this, STiH418_TVO_VIP_HDF_BASE
                                   , m_pMainPreformatter
                                   , m_pAuxPreformatter
                                   , hdf_sync_type
                                   , (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads
                                   , TVO_VIP_INPUT_IS_INVERTED | TVO_VIP_BYPASS_INPUT_IS_RGB)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create HDF VIP" );
    return false;
  }

  if((m_pHDMIVIP  = new CSTmVIP( "VIP-HDMI",
                               this, STiH418_TVO_VIP_HDMI_BASE
                                    , m_pMainPreformatter
                                    , m_pAuxPreformatter
                                    , TVO_VIP_SYNC_TYPE_HDMI
                                    , (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads
                                    , TVO_VIP_INPUT_IS_INVERTED | TVO_VIP_BYPASS_INPUT_IS_RGB
                                    | TVO_VIP_HAS_MAIN_FILTERED_422_INPUT | TVO_VIP_HAS_MAIN_FILTERED_420_INPUT)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create HDMI VIP" );
    return false;
  }
  if((m_pDVOVIP  = new CSTmVIP( "VIP-DVO",
                               this, STiH418_TVO_VIP_DVO_BASE
                                      , m_pMainPreformatter
                                      , m_pAuxPreformatter
                                      , dvo_sync_type
                                      , (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads
                                      , TVO_VIP_INPUT_IS_INVERTED | TVO_VIP_BYPASS_INPUT_IS_RGB
                                      | TVO_VIP_HAS_MAIN_FILTERED_422_INPUT | TVO_VIP_HAS_MAIN_FILTERED_420_INPUT)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create DVO VIP" );
    return false;
  }

  m_pHDFormatter = new CSTmHDF_V5_0(this,
                                    STiH418_TVOUT1_BASE,
                                    STiH418_HD_FORMATTER_BASE,
                                    STiH418_HD_FORMATTER_AWG,
                                    STiH418_HD_FORMATTER_AWG_SIZE);
  if(!m_pHDFormatter || !m_pHDFormatter->Create())
  {
    TRC( TRC_ID_ERROR, "failed to create HDFormatter" );
    return false;
  }

  /*
   * Disable ALT filters for this platform :
   * This has been recommended by validation - See Bug38649
   */
  if(m_pHDFormatter->SetControl(OUTPUT_CTRL_DAC_HD_ALT_FILTER, 0) != STM_OUT_OK)
  {
    TRC( TRC_ID_ERROR, "failed setup default configuration for HDFormatter" );
    return false;
  }

  if((m_pMainMixer = new CSTiH418MainMixer(this,
                                           m_pMainPreformatter)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create main mixer" );
    return false;
  }

  if ((m_pAuxMixer = new CSTiH418AuxMixer(this,
                                          m_pAuxPreformatter,
                                          MIXER_ID_GDP6)) == 0)
  {
    TRC( TRC_ID_ERROR, "failed to create aux mixer" );
    return false;
  }

  return true;
}


bool CSTiH418Device::CreatePlanes(void)
{

  /*
   * Initialize GDP Plus LLD module.
   */
  GdpPlusHwInfo_s GdpPlusHwInfo[STiH418_DISPLAY_GDP_PLUS_HW_NB];

  GdpPlusHwInfo[0].offset      = STiH418_DISPLAY_GDP_PLUS_1_BASE;
  GdpPlusHwInfo[0].mixerBit    = MIXER_ID_GDP1;
  GdpPlusHwInfo[1].offset      = STiH418_DISPLAY_GDP_PLUS_2_BASE;
  GdpPlusHwInfo[1].mixerBit    = MIXER_ID_GDP2;
  GdpPlusHwInfo[2].offset      = STiH418_DISPLAY_GDP_PLUS_3_BASE;
  GdpPlusHwInfo[2].mixerBit    = MIXER_ID_GDP3;
  GdpPlusHwInfo[3].offset      = STiH418_DISPLAY_GDP_PLUS_4_BASE;
  GdpPlusHwInfo[3].mixerBit    = MIXER_ID_GDP4;

  // Use physical address not virtual mapped address !!!
  if(!CGDPPlus::Init(GetCtrlRegisterBase(), STiH418_DISPLAY_GDP_PLUS_HW_NB, GdpPlusHwInfo))
  {
    TRC( TRC_ID_ERROR, "failed to initialize GDP Plus LLD" );
    return false;
  }


  {
    /*
     * This GDP plane will be used in Dual configuration for framebuffer rendering in 4K2KP@60Hz display mode
     */
    if(!AddPlane(new CSTiH418GDPPlus("Main-GDP1",
                                 STiH418_PLANE_IDX_GDP1_MAIN,
                                 this,
                                 (stm_plane_capabilities_t)(PLANE_CAPS_GRAPHICS|PLANE_CAPS_PRIMARY_OUTPUT|PLANE_CAPS_HDR_FORMAT),
                                 GDPGQR_PROFILE_4KP60)))
    {
      TRC( TRC_ID_ERROR, "failed to create Main-GDP1" );
      return false;
    }

    /*
     * This GDP plane will be used in Dual configuration for V4L2 rendering in 4K2KP@60Hz display mode
     */
    if(!AddPlane(new CSTiH418GDPPlus("Main-GDP2",
                                 STiH418_PLANE_IDX_GDP2_MAIN,
                                 this,
                                 (stm_plane_capabilities_t)(PLANE_CAPS_GRAPHICS|PLANE_CAPS_PRIMARY_OUTPUT|PLANE_CAPS_HDR_FORMAT),
                                 GDPGQR_PROFILE_4KP60)))
    {
      TRC( TRC_ID_ERROR, "failed to create Main-GDP2" );
      return false;
    }

    /*
     * This is the only GDP we are fixing to the aux mixer for framebuffer usage.
     */
    if(!AddPlane(new CSTiH418GDP("Aux-GDP1",
                                 STiH418_PLANE_IDX_GDP1_AUX,
                                 this,
                                 (stm_plane_capabilities_t)(PLANE_CAPS_GRAPHICS|PLANE_CAPS_SECONDARY_OUTPUT),
                                 STiH418_COMPOSITOR_BASE + STiH418_GDP6_OFFSET)))
    {
      TRC( TRC_ID_ERROR, "failed to create Aux-GDP1" );
      return false;
    }

    /*
     * This GDP is shared between MAin and Aux mixers for WM usage.
     */
    if(!AddPlane(new CSTiH418GDP("Aux-GDP2",
                                 STiH418_PLANE_IDX_GDP2_AUX,
                                 this,
                                 (stm_plane_capabilities_t)(PLANE_CAPS_GRAPHICS|PLANE_CAPS_PRIMARY_OUTPUT|PLANE_CAPS_SECONDARY_OUTPUT|PLANE_CAPS_HDR_FORMAT),
                                 STiH418_COMPOSITOR_BASE + STiH418_GDP5_OFFSET)))
    {
      TRC( TRC_ID_ERROR, "failed to create Aux-GDP2" );
      return false;
    }
  }

  /*
   * Create Aux VBI plane if supported by Mixer.
   */
  CDisplayPlane * plane = GetPlane(STiH418_PLANE_IDX_GDP1_AUX);

  if ( !plane ||
       (!AddPlane(new CVBIPlane("Aux-VBI",
                                STiH418_PLANE_IDX_VBI_AUX,
                                (stm_plane_capabilities_t)(PLANE_CAPS_VBI|PLANE_CAPS_SECONDARY_OUTPUT),
                                static_cast<CGdpPlane*>(plane)))))
  {
    TRC( TRC_ID_ERROR, "failed to create Aux-VBI" );
    return false;
  }

  /*
   * Create video planes.
   */
  m_pVideoPlug1 = new CVideoPlug(this, STiH418_COMPOSITOR_BASE + STiH418_VID1_OFFSET, true, true, false, true, true);
  if(!m_pVideoPlug1)
  {
    TRC( TRC_ID_ERROR, "failed to create video plug 1");
    return false;
  }

  m_pVideoPlug2 = new CVideoPlug(this, STiH418_COMPOSITOR_BASE + STiH418_VID2_OFFSET, true, true, false, true, true);
  if(!m_pVideoPlug2)
  {
    TRC( TRC_ID_ERROR, "failed to create video plug 2");
    return false;
  }

  m_pVideoPlug3 = new CVideoPlug(this, STiH418_COMPOSITOR_BASE + STiH418_VID3_OFFSET, true, true, false, true, true);
  if(!m_pVideoPlug3)
  {
    TRC( TRC_ID_ERROR, "failed to create video plug 3");
    return false;
  }

  const HqvdpLiteHwInfo_s HqvdpLiteHwInfo[] =
  {
     [0] = { .offset = STiH418_DISPLAY_HQVDP_0_BASE, .pVideoPlug = m_pVideoPlug1, .mixerBit = MIXER_ID_VID1 },
     [1] = { .offset = STiH418_DISPLAY_HQVDP_1_BASE, .pVideoPlug = m_pVideoPlug2, .mixerBit = MIXER_ID_VID2 },
     [2] = { .offset = STiH418_DISPLAY_HQVDP_2_BASE, .pVideoPlug = m_pVideoPlug3, .mixerBit = MIXER_ID_VID3 }
  };

  /* Provide the HQVDP subsystem with available HW information */
  if(!CSTiH418HqvdpLitePlane::Initialize(GetCtrlRegisterBase(), N_ELEMENTS(HqvdpLiteHwInfo), HqvdpLiteHwInfo))
  {
      TRC( TRC_ID_ERROR, "failed to initialize HQVDP planes subsystem");
      return false;
  }

  /* Create Main HQVDP plane */
  CDisplayPlane* pMainPlane = new CSTiH418HqvdpLitePlane("Main-VID",
                                                         STiH418_PLANE_IDX_VID_MAIN,
                                                         this,
                                                         HQVDPLITE_PROFILE_4KP60,
                                                         true,
                                                         (stm_plane_capabilities_t)(  PLANE_CAPS_VIDEO
                                                                                    | PLANE_CAPS_VIDEO_BEST_QUALITY
                                                                                    | PLANE_CAPS_PRIMARY_PLANE
                                                                                    | PLANE_CAPS_PRIMARY_OUTPUT
                                                                                    | PLANE_CAPS_SECONDARY_OUTPUT
                                                                                    | PLANE_CAPS_HDR_FORMAT),
                                                         "CLK_MAIN_DISP");
  if(!AddPlane(pMainPlane))
  {
    TRC( TRC_ID_ERROR, "failed to create HQVDP Main plane");
    return false;
  }

  SetMasterVideoPlane(pMainPlane);

  /* Create PIP HQVDP plane */
  CDisplayPlane* pPipPlane = new CSTiH418HqvdpLitePlane("Main-PIP",
                                                        STiH418_PLANE_IDX_VID_PIP,
                                                        this,
                                                        HQVDPLITE_PROFILE_4KP60_PIXEL_REPEAT,
                                                        false,
                                                        (stm_plane_capabilities_t)(  PLANE_CAPS_VIDEO
                                                                                   | PLANE_CAPS_PRIMARY_OUTPUT
                                                                                   | PLANE_CAPS_SECONDARY_OUTPUT),
                                                        "CLK_AUX_DISP");
  if(!AddPlane(pPipPlane))
  {
    TRC( TRC_ID_ERROR, "failed to create HQVDP PIP plane");
    return false;
  }

  return true;
}


bool CSTiH418Device::CreateSources(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );
  CDisplaySource                *pSource;
  CPureSwQueueBufferInterface    *pInterface;

  /* Create as many Sources/Interfaces as Decoder */
  /* because the link decoder-source will remains connected */
  /* sizeof(CDisplaySource) = 64, sizeof(CPureSwQueueBufferInterface) = 280 */
  /* so total memory consumption is 50x344=17200 bytes */
  for(int i=0;i<STiH418_SOURCE_COUNT;i++)
  {
      vibe_os_snprintf (sourceNames[i], sizeof(sourceNames[i]),"Source.%d",i);
      pSource = new CDisplaySource(sourceNames[i], i, this, STiH418_PLANE_COUNT);
      if(!AddSource(pSource))
      {
          TRC( TRC_ID_ERROR, "failed to create Source" );
          return false;
      }

      pInterface = new CPureSwQueueBufferInterface(i, pSource);
      if(!pInterface || !pInterface->Create())
      {
          TRC( TRC_ID_ERROR, "failed to create Interface" );
          delete pInterface;
          return false;
      }
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;
}


bool CSTiH418Device::CreateOutputs(void)
{
  CSTiH418MainOutput *pMainOutput = new CSTiH418MainOutput( this,
                                                            m_pMainVTG,
                                                            m_pDENC,
                                                            m_pMainMixer,
                                                            m_pFSynthHD,
                                                            m_pHDFormatter,
                                                            m_pClkDivider,
                                                            m_pHDFVIP,
                                                            m_pDENCVIP,
                                                            (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads);
  if(!AddOutput(pMainOutput))
  {
    /*
     * NOTE: AddOutput will destroy the passed object for us if it fails.
     */
    TRC( TRC_ID_ERROR, "failed to create main output" );
    return false;
  }

  CSTiH418AuxOutput *pAuxOutput = new CSTiH418AuxOutput(this, m_pAuxVTG,
                                            m_pDENC,
                                            m_pAuxMixer,
                                            m_pFSynthSD,
                                            m_pHDFormatter,
                                            m_pClkDivider,
                                            m_pHDFVIP,
                                            m_pDENCVIP,
                                            (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads );

  if(!AddOutput(pAuxOutput))
  {
    TRC( TRC_ID_ERROR, "failed to create aux output" );
    return false;
  }

  if(!AddOutput(new CSTiH418HDMI(this, GetOutput(STiH418_OUTPUT_IDX_MAIN),
                                       GetOutput(STiH418_OUTPUT_IDX_AUX),
                                       m_pMainVTG,
                                       m_pAuxVTG,
                                       m_pHDMIVIP,
                                       m_pClockFs, N_ELEMENTS(m_pClockFs),
                                       (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads)))
  {
    TRC( TRC_ID_ERROR, "failed to allocate HDMI output" );
    return false;
  }

  if(!AddOutput(new CSTiH418DVO (this, GetOutput(STiH418_OUTPUT_IDX_MAIN),
                                       GetOutput(STiH418_OUTPUT_IDX_AUX),
                                       m_pMainVTG,
                                       m_pAuxVTG,
                                       m_pClkDivider,
                                       m_pDVOVIP,
                                       (m_bVGAUsingDVOPads==false)?tvo_vip_sync_sel_map : tvo_vip_sync_sel_map_vga_dvo_pads,
                                       STiH418_FLEXDVO_BASE)))
  {
    TRC( TRC_ID_ERROR, "failed to create FDVO output" );
    return false;
  }

  return true;
}


int CSTiH418Device::Freeze(void)
{
  int res;

  if((res = CDisplayDevice::Freeze()) != 0)
    return res;

  if((res = m_pClkDivider->Freeze()) != 0)
    return res;

  PowerDown();

  PmSetState(DEVICE_FROZEN);

  return res;
}


int CSTiH418Device::Suspend(void)
{
  int res;

  if((res = CDisplayDevice::Suspend()) != 0)
    return res;

  if((res = m_pClkDivider->Suspend()) != 0)
    return res;

  PowerDown();

  PmSetState(DEVICE_SUSPENDED);

  return res;
}


int CSTiH418Device::Resume(void)
{
  int res;

  /* Restore basic chip config and reset state */
  PowerOnSetup();

  if((res = m_pClkDivider->Resume()) != 0)
    return res;

  return CDisplayDevice::Resume();
}


/*
 * This is the top level of device creation.
 * There should be exactly one of these per kernel module.
 */
CDisplayDevice* AnonymousCreateDevice(stm_device_configuration_t *DevConfig)
{
  if(DevConfig->id == 0)
    return new CSTiH418Device(DevConfig);

  return 0;
}
