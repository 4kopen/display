/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418gdp.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STiH418GDP_H
#define _STiH418GDP_H

#include <display/ip/gdp/GdpPlane.h>
#include <display/ip/gdpplus/GdpPlusPlane.h>
#include <display/ip/gdp/GdpDefs.h>
#include <display/ip/gdpplus/GdpPlusDefs.h>

/*
 * Color space conversion matrices.
 *
 * Each matrix is a 3x3 coefficients matrix, stored in a 5 entries table.
 * Each table entry stores 2 coefficients, except last entry that has
 * only one coefficient:
 *   [0] [31:16] Coef 1 [15:0] Coef 0
 *   [1] [31:16] Coef 3 [15:0] Coef 2
 *   [2] [31:16] Coef 5 [15:0] Coef 4
 *   [3] [31:16] Coef 7 [15:0] Coef 6
 *   [4] [31:16] N/A    [15:0] Coef 8
 */
static const uint32_t gdp_bt709_to_bt2020_gamut_table[5] = {
  0x0a8a1414,
  0x02360163,
  0x005d1d6d,
  0x02d10086,
  0x00001ca9
};

class CSTiH418GDP: public CGdpPlane
{
public:
  CSTiH418GDP(const char              *name,
              uint32_t                 id,
        const CDisplayDevice          *pDev,
        const stm_plane_capabilities_t caps,
              uint32_t                 baseAddr): CGdpPlane(name,
                                                            id,
                                                            pDev,
                                                            static_cast<stm_plane_capabilities_t>(caps | PLANE_CAPS_GRAPHICS_BEST_QUALITY),
                                                            baseAddr,
                                                            "CLK_PROC_MIXER", "CLK_COMPO_DVP", "",
                                                            true)
  {
    m_bHasVFilter       = true;
    m_bHasFlickerFilter = true;
    m_b4k2k             = true;
    m_ulMaxHSrcInc = m_fixedpointONE*4; // downscale to 1/4
    m_ulMinHSrcInc = m_fixedpointONE/8; // upscale 8x
    /*
     * As per other SoCs we are using line skip via changing the pitch for
     * downscales more than 1/2.
     */
    m_ulMaxVSrcInc = m_fixedpointONE*2; // downscale to 1/2
    m_ulMinVSrcInc = m_fixedpointONE/8;

    m_bHas4_13_precision = true;

    m_hasIgnoreOnMixer = false;
    m_ForceOnMixerMask = GDP_PPT_ONLY_FORCE_ON_MIX0|GDP_PPT_ONLY_FORCE_ON_MIX1;

    /*
     * All GDPs hardware are sharing the same 'CLK_PROC_MIXER' pixel clock.
     * The pixel clock rate is fixed (set by the targetpack) and shouldn't be
     * tuned by the driver. The 'CLK_COMPO_DVP' processing clock is shared
     * also for ALL GDP hardware planes.
     *
     *  |----------------------------------|    |------------|-----------|
     *  |         |            |           |    |            |           |
     *  |         |  GDP HW    |           |    |    MIXER   |           |
     *  |clk_stbus|            |clk_gdp_pix|--->|            |clk_mix_pix|
     *  |         |------------|           |    |------------|           |
     *  |         |clk_gdp_proc|           |    |clk_mix_proc|           |
     *  |----------------------------------|    |------------|-----------|
     *
     * * clk_stbus      <-----+ CLK_ICN_REG
     * * clk_gdp_proc   <-----+ CLK_COMPO_DVP
     * * clk_gdp_pix    <--|
     * * clk_mix_proc   <--|--+ CLK_PROC_MIXER
     * * clk_mix_pix    <-----+ CLK_PIX_MAIN_DISP / CLK_PIX_AUX_DISP
     */
    m_hasADedicatedPixelClock = false;

    /*
     * GDP hardware has BT709 -> BT2020 conversion using Gamut LUT.
     */
    m_pGamutMatrix = gdp_bt709_to_bt2020_gamut_table;

    /*
     * Pixel Repeat is supported by GDP hardware included in H418 SoC.
     */
    m_bHasPixelRepeat = true;

    /* Issue Rate Regulation is supported by GDP */
    m_bHasIrrSupported = true;
  }

  /*
   * SDR --> HDR conversion supported using Gain/Offset Adjustement.
   */
  bool SetupHDROutFormat(void)
  {
    GENERIC_GDP_LLU_NODE    &topNode  = m_NextGdpSetup.topNode;
    GENERIC_GDP_LLU_NODE    &botNode  = m_NextGdpSetup.botNode;

    return AdjustGainAndOffsetForHDROutFormat(topNode, botNode);
  }

private:
  // STiH418 specific HDROut Format support through Gain/Offset Adjustement
  bool AdjustGainAndOffsetForHDROutFormat(GENERIC_GDP_LLU_NODE       &topNode,
                                          GENERIC_GDP_LLU_NODE       &botNode)
  {
    PLANE_TRC( TRC_ID_GDP_IT, "OLD : m_ulHDRGain = %d - m_ulHDROffset = %d", m_ulHDRGain, m_ulHDROffset);
    switch(m_outputInfo.outputHDRFormat.eotf_type)
    {
      case STM_EOTF_GAMMA_HDR_HLG:
      {
        m_ulHDRGain     = m_HDRGainOffset.hdr_hlg_gain;
        m_ulHDROffset   = m_HDRGainOffset.hdr_hlg_offset;

        /* Force non premultiplied source in case of HDR output */
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
        {
          PLANE_TRC( TRC_ID_GDP_PLANE, "Force Non-Premultiplied Alpha for HDR/HLG output." );
          topNode.GDPn_CTL &= ~GDP_CTL_PREMULT_FORMAT;
          botNode.GDPn_CTL &= ~GDP_CTL_PREMULT_FORMAT;
        }
      }
      break;

      case STM_EOTF_GAMMA_HDR:
      case STM_EOTF_SMPTE_ST2084:
      {
        m_ulHDRGain     = m_HDRGainOffset.hdr_st2084_gain;
        m_ulHDROffset   = m_HDRGainOffset.hdr_st2084_offset;

        /* Force non premultiplied source in case of HDR output */
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
        {
          PLANE_TRC( TRC_ID_GDP_PLANE, "Force Non-Premultiplied Alpha for HDR/ST2084 output." );
          topNode.GDPn_CTL &= ~GDP_CTL_PREMULT_FORMAT;
          botNode.GDPn_CTL &= ~GDP_CTL_PREMULT_FORMAT;
        }
      }
      break;

      case STM_EOTF_GAMMA_SDR:
      default:
      {
        m_ulHDRGain     = m_HDRGainOffset.sdr_gain;
        m_ulHDROffset   = m_HDRGainOffset.sdr_offset;

        /* Re-enable premultiplied source if source was pre-multiplied */
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
        {
          PLANE_TRC( TRC_ID_GDP_PLANE, "Setting back Premultiplied Alpha for SDR output." );
          topNode.GDPn_CTL |= GDP_CTL_PREMULT_FORMAT;
          botNode.GDPn_CTL |= GDP_CTL_PREMULT_FORMAT;
        }
      }
      break;
    }

    PLANE_TRC( TRC_ID_GDP_IT, "Dynamic Range Updated - Recalculate Gain/Offset values");
    m_IsGainChanged = true;

    PLANE_TRC( TRC_ID_GDP_IT, "NEW : m_ulHDRGain = %d - m_ulHDROffset = %d", m_ulHDRGain, m_ulHDROffset);

    return true;
  }

  CSTiH418GDP(const CSTiH418GDP&);
  CSTiH418GDP& operator=(const CSTiH418GDP&);
};


class CSTiH418GDPPlus: public CGdpPlusPlane
{
public:
  CSTiH418GDPPlus(const char                    *name,
              uint32_t                       id,
              const CDisplayDevice          *pDev,
              const stm_plane_capabilities_t caps,
              gdpgqr_lld_profile_e           profile = GDPGQR_PROFILE_FULL_HD,
              uint32_t                       max_viewports = 1): CGdpPlusPlane(name,
                                                                  id,
                                                                  pDev,
                                                                  static_cast<stm_plane_capabilities_t>(caps | PLANE_CAPS_GRAPHICS_BEST_QUALITY),
                                                                  profile,
                                                                  "CLK_PROC_MIXER", "CLK_COMPO_DVP",
                                                                  max_viewports,
                                                                 true)
  {
    /*
     * GDPPlus hardware doesn't support Flicker Filtering.
     */
    m_bHasVFilter       = true;
    m_b4k2k             = true;
    m_ulMaxHSrcInc = m_fixedpointONE*8; // downscale to 1/8
    m_ulMinHSrcInc = m_fixedpointONE/8; // upscale 8x
    /*
     * As per other SoCs we are using line skip via changing the pitch for
     * downscales more than 1/5.
     * Hardware line skip isn't supported by GDPPlus
     */
    m_ulMaxVSrcInc = m_fixedpointONE*5; // downscale to 1/5
    m_ulMinVSrcInc = m_fixedpointONE/5; // upscale 5x

    m_hasIgnoreOnMixer = false;
    m_ForceOnMixerMask = GDP_PLUS_PPT_FORCE_ON_MIX0|GDP_PLUS_PPT_FORCE_ON_MIX1;

    /*
     * All GDP+ hardware are sharing the same 'CLK_PROC_MIXER' pixel clock.
     * The pixel clock rate is fixed (set by the targetpack) and shouldn't be
     * tuned by the driver. The 'CLK_COMPO_DVP' processing clock is shared
     * also for ALL GDP+ hardware planes.
     *
     * In case of plane managing two GDP+ hardware instances (Dual Plane)
     * the clock gating is ensured by hardware itself (internal clock gating).
     *
     *  |----------------------------------|    |------------|-----------|
     *  |         |            |           |    |            |           |
     *  |         |  GDP+ HW   |           |    |    MIXER   |           |
     *  |clk_stbus|            |clk_gdp_pix|--->|            |clk_mix_pix|
     *  |         |------------|           |    |------------|           |
     *  |         |clk_gdp_proc|           |    |clk_mix_proc|           |
     *  |----------------------------------|    |------------|-----------|
     *
     * * clk_stbus      <-----+ CLK_ICN_REG
     * * clk_gdp_proc   <-----+ CLK_COMPO_DVP
     * * clk_gdp_pix    <--|
     * * clk_mix_proc   <--|--+ CLK_PROC_MIXER
     * * clk_mix_pix    <-----+ CLK_PIX_MAIN_DISP / CLK_PIX_AUX_DISP
     */
    m_hasADedicatedPixelClock = false;
    m_bHasIrrSupported = true;

    /*
     * GDP hardware has BT709 -> BT2020 conversion using Gamut LUT.
     */
    m_pGamutMatrix = gdp_bt709_to_bt2020_gamut_table;
  }

  /*
   * SDR --> HDR conversion supported using Gain/Offset Adjustement.
   */
  bool SetupHDROutFormat(void)
  {
    gdpgqr_lld_hw_viewport_s    &topNode  = m_GdpPlusSetup.topNode;
    gdpgqr_lld_hw_viewport_s    &botNode  = m_GdpPlusSetup.botNode;

    return AdjustGainAndOffsetForHDROutFormat(topNode, botNode);
  }

private:
  // STiH418 specific HDROut Format support through Gain/Offset Adjustement
  bool AdjustGainAndOffsetForHDROutFormat(gdpgqr_lld_hw_viewport_s       &topNode,
                                          gdpgqr_lld_hw_viewport_s       &botNode)
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "OLD : m_ulHDRGain = %d - m_ulHDROffset = %d", m_ulHDRGain, m_ulHDROffset);

    switch(m_outputInfo.outputHDRFormat.eotf_type)
    {
      case STM_EOTF_GAMMA_HDR_HLG:
      {
        m_ulHDRGain     = AGC_HDR_HLG_VIDEO_GAIN;
        m_ulHDROffset   = AGC_HDR_HLG_CONSTANT_BLACK_LEVEL;

        /* Force non premultiplied source in case of HDR output */
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
        {
          PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Force Non-Premultiplied Alpha for HDR/HLG output." );
          topNode.CTL &= ~GDP_PLUS_CTL_PREMULT_FORMAT;
          botNode.CTL &= ~GDP_PLUS_CTL_PREMULT_FORMAT;
        }
      }
      break;

      case STM_EOTF_GAMMA_HDR:
      case STM_EOTF_SMPTE_ST2084:
      {
        m_ulHDRGain     = AGC_HDR_ST2084_VIDEO_GAIN;
        m_ulHDROffset   = AGC_HDR_ST2084_CONSTANT_BLACK_LEVEL;

        /* Force non premultiplied source in case of HDR output */
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
        {
          PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Force Non-Premultiplied Alpha for HDR/ST2084 output." );
          topNode.CTL &= ~GDP_PLUS_CTL_PREMULT_FORMAT;
          botNode.CTL &= ~GDP_PLUS_CTL_PREMULT_FORMAT;
        }
      }
      break;

      case STM_EOTF_GAMMA_SDR:
      default:
      {
        m_ulHDRGain     = AGC_SDR_FULL_RANGE_GAIN;
        m_ulHDROffset   = 0;

        /* Re-enable premultiplied source if source was pre-multiplied */
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
        {
          PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Setting back Premultiplied Alpha for SDR output." );
          topNode.CTL |= GDP_PLUS_CTL_PREMULT_FORMAT;
          botNode.CTL |= GDP_PLUS_CTL_PREMULT_FORMAT;
        }
      }
      break;
    }

    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Dynamic Range Updated - Recalculate Gain/Offset values");
    m_IsGainChanged = true;

    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "NEW : m_ulHDRGain = %d - m_ulHDROffset = %d", m_ulHDRGain, m_ulHDROffset);

    return true;
  }

  CSTiH418GDPPlus(const CSTiH418GDPPlus&);
  CSTiH418GDPPlus& operator=(const CSTiH418GDPPlus&);
};

#endif /* _STiH418GDP_H */
