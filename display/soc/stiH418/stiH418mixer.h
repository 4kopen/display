/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418mixer.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STiH418_MIXER_H
#define _STiH418_MIXER_H

#include <display/ip/mixer/GammaMixer.h>
#include <display/ip/mixer/GenericGammaReg.h>
#include <display/ip/mixer/GenericGammaDefs.h>

#include "stiH418device.h" // for STiH418_PLANE_COUNT definition

#include "stiH418reg.h"


class CDisplayDevice;
class CSTmPreformatter;

class CSTiH418Mixer: public CGammaMixer
{
public:
  CSTiH418Mixer(const char *      name,
                CDisplayDevice   *pDev,
                uint32_t          mixerOffset,
                CSTmPreformatter *pPreFormatter,
                stm_mixer_id_t    ulVBILinkedPlane = MIXER_ID_NONE);

  ~CSTiH418Mixer(void);

  void UpdateColorspace(const stm_display_mode_t *pModeLine);

private:
  CSTmPreformatter *m_pPreformatter;
  stm_zorder_item_t      m_aZorder[STiH418_PLANE_COUNT];
  static uint32_t        m_aMixerMap[STiH418_PLANE_COUNT];  // Array containing association between a planeID and one or several stm_mixer_id_t


  CSTiH418Mixer(const CSTiH418Mixer&);
  CSTiH418Mixer& operator=(const CSTiH418Mixer&);

};


class CSTiH418MainMixer: public CSTiH418Mixer
{
public:
  CSTiH418MainMixer(
      CDisplayDevice   *pDev,
      CSTmPreformatter *pPreFormatter,
      stm_mixer_id_t    ulVBILinkedPlane = MIXER_ID_NONE): CSTiH418Mixer("Mixer-Main",
                                                                         pDev,
                                                                         (STiH418_COMPOSITOR_BASE+STiH418_MIXER0_OFFSET),
                                                                         pPreFormatter,
                                                                         ulVBILinkedPlane)
  {
    m_firstcrossbarSize  = 8;
    m_secondcrossbarSize = 1;
  }

private:
  CSTiH418MainMixer(const CSTiH418MainMixer&);
  CSTiH418MainMixer& operator=(const CSTiH418MainMixer&);
};

/* !!! Mixer1 is programmed here as AuxMixer to match TLM mapping !!! */
/*  !!!  This must be adjusted for coemul and soc uses (mixer2)  !!!  */
class CSTiH418AuxMixer: public CSTiH418Mixer
{
public:
  CSTiH418AuxMixer(
      CDisplayDevice   *pDev,
      CSTmPreformatter *pPreFormatter,
      stm_mixer_id_t    ulVBILinkedPlane = MIXER_ID_NONE): CSTiH418Mixer("Mixer-Aux",
                                                     pDev,
                                                     (STiH418_COMPOSITOR_BASE+STiH418_MIXER1_OFFSET),
                                                     pPreFormatter,
                                                     ulVBILinkedPlane)
  {
    m_firstcrossbarSize  = 6;
  }

private:
  CSTiH418AuxMixer(const CSTiH418AuxMixer&);
  CSTiH418AuxMixer& operator=(const CSTiH418AuxMixer&);
};

#endif /* _STiH418_MIXER_H */
