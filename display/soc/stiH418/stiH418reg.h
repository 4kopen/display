/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#ifndef _STIH418REG_H
#define _STIH418REG_H

/* STiH418 Base addresses for display IP -------------------------------------------- */
#define STiH418_REGISTER_BASE           0x08D00000


/* STiH418 Base address size -------------------------------------------------------- */
#define STiH418_REG_ADDR_SIZE           0x02000000


/* Video planes --------------------------------------------------------------------- */
#define STiH418_DISPLAY_HQVDP_0_BASE    0x00F00000
#define STiH418_DISPLAY_HQVDP_1_BASE    0x01700000
#define STiH418_DISPLAY_HQVDP_2_BASE    0x01800000

/* GDP Plus planes ------------------------------------------------------------------ */
#define STiH418_DISPLAY_GDP_PLUS_HW_NB  4
#define STiH418_DISPLAY_GDP_PLUS_1_BASE 0x01900000
#define STiH418_DISPLAY_GDP_PLUS_2_BASE 0x01910000
#define STiH418_DISPLAY_GDP_PLUS_3_BASE 0x01920000
#define STiH418_DISPLAY_GDP_PLUS_4_BASE 0x01930000


/* The compositor (COMP) offset addresses ------------------------------------------- */
#define STiH418_COMPOSITOR_BASE         0x01900000
# define STiH418_GDP1_OFFSET              0x000000
# define STiH418_GDP2_OFFSET              0x010000
# define STiH418_GDP3_OFFSET              0x020000
# define STiH418_GDP4_OFFSET              0x030000
# define STiH418_GDP5_OFFSET              0x040000
# define STiH418_GDP6_OFFSET              0x050000
# define STiH418_VID1_OFFSET              0x080000
# define STiH418_VID2_OFFSET              0x090000
# define STiH418_VID3_OFFSET              0x0A0000
# define STiH418_ALP1_OFFSET              0x0C0000
# define STiH418_ALP2_OFFSET              0x0D0000
# define STiH418_CAP1_OFFSET              0x0E0000
# define STiH418_CAP2_OFFSET              0x0F0000
# define STiH418_MIXER0_OFFSET            0x100000
# define STiH418_MIXER1_OFFSET            0x110000
# define STiH418_MIXER2_OFFSET            0x120000
# define STiH418_MIXER3_OFFSET            0x130000
# define STiH418_MISC_OFFSET              0x1D0000
# define STiH418_CID_OFFSET               0x1E0000
# define STiH418_STREAMID_OFFSET          0x1F0000


/* TVOut blocks ---------------------------------------------------------------------- */
/*
 * There's two TVOut register blocks:
 * TVOUT0 is TVOUT base address, TVOUT1 is TVOUT glue base address
 */
#define STiH418_TVOUT0_BASE             0x01B00000
# define STiH418_DENC_BASE                (STiH418_TVOUT0_BASE + 0x0000)
# define STiH418_VTG_AUX_BASE             (STiH418_TVOUT0_BASE + 0x0200)
# define STiH418_FLEXDVO_BASE             (STiH418_TVOUT0_BASE + 0x0400)
# define STiH418_HD_FORMATTER_BASE        (STiH418_TVOUT0_BASE + 0x2000)
#  define STiH418_HD_FORMATTER_AWGANC     (STiH418_HD_FORMATTER_BASE + 0x0200)
#  define STiH418_HD_FORMATTER_AWG        (STiH418_HD_FORMATTER_BASE + 0x0300)
# define STiH418_VTG_MAIN_BASE            (STiH418_TVOUT0_BASE + 0x2800)
# define STiH418_HDMI_BASE                (STiH418_TVOUT0_BASE + 0x4000)

#define STiH418_TVOUT1_BASE             0x01C00000
# define STiH418_TVO_MAIN_PF_BASE         (STiH418_TVOUT1_BASE + 0x000)
# define STiH418_TVO_AUX_PF_BASE          (STiH418_TVOUT1_BASE + 0x100)
# define STiH418_TVO_VIP_DENC_BASE        (STiH418_TVOUT1_BASE + 0x200)
# define STiH418_TVO_TTXT_BASE            (STiH418_TVOUT1_BASE + 0x300)
# define STiH418_TVO_VIP_HDF_BASE         (STiH418_TVOUT1_BASE + 0x400)
# define STiH418_TVO_VIP_HDMI_BASE        (STiH418_TVOUT1_BASE + 0x500)
# define STiH418_TVO_VIP_DVO_BASE         (STiH418_TVOUT1_BASE + 0x600)

#define STiH418_HD_FORMATTER_AWG_SIZE   64

#define STiH418_UNIPLAYER_HDMI_BASE     0x00080000

#define STiH418_CLKGEN_A_12             0x00401000    /* HDMI Rejection */
#define STiH418_CLKGEN_D_10             0x00404000    /* HDMI Audio Clock */
#define STiH418_CLKGEN_D_12             0x00406000    /* Video clocks/VCC */


/* STiH418 TVO regs, Analogue DAC ----------------------------------------------------- */
#define TVO_SD_DAC_CONFIG                  0x220
#define TVO_HD_DAC_CONFIG                  0x420
#define TVO_DAC_DEL_SHIFT                  8
#define TVO_DAC_DEL_WIDTH                  3
#define TVO_DAC_DENC_OUT_SD_SHIFT          4
#define TVO_DAC_DENC_OUT_SD_WIDTH          1
#define TVO_DAC_DENC_OUT_SD_DAC123         0L
#define TVO_DAC_DENC_OUT_SD_DAC456         1L
#define TVO_DAC_POFF_DACS_SHIFT            0
#define TVO_DAC_POFF_DACS_WIDTH            1
#define TVO_DAC_POFF_DACS                  1L

#define TVO_ADAPTIVE_DCMTN_FLTR_CFG        0x20
#define TVO_DCMTN_FLTR_CFG_23TAP_ADAPTIVE  0x0
#define TVO_DCMTN_FLTR_CFG_23TAP_LINEAR    0x1
#define TVO_DCMTN_FLTR_CFG_SAMPLE_DROP     0x3


/* STiH418 TVO MISR --------------------------------------------------------------- */
#define STiH418_TVO_HD_OUT_CTRL           (STiH418_TVOUT1_BASE + 0x04A0)
#define STiH418_TVO_MAIN_PF_CTRL          (STiH418_TVOUT1_BASE + 0x0480)
#define STiH418_TVO_SD_OUT_CTRL           (STiH418_TVOUT1_BASE + 0x02C0)
#define STiH418_TVO_AUX_PF_CTRL           (STiH418_TVOUT1_BASE + 0x0280)
#define STiH418_TVO_DVO_CTRL              (STiH418_TVOUT1_BASE + 0x0680)


/* STiH418 SYSCFG ------------------------------------------------------------------ */
#define STiH418_SYSCFG_CORE              0x005B0000
#define STiH418_SYSCFG_SBC_x             0x00900000

/* Sysconfig Core 5072, Video DACs control -----------------------------------*/
#define SYS_CFG5072                      0x120
#define SYS_CFG5072_DAC_HZX              (1L<<0) /* CVBS SD DAC, note NO S-VIDEO on this design */
#define SYS_CFG5072_DAC_HZUVW            (1L<<1) /* YUV/RGB HD DACs all together */

/* Sysconfig Core 5131, Reset Generator control 0 ----------------------------*/
#define SYS_CFG5131                      0x20c
#define SYS_CFG5131_RST_N_HDTVOUT            (1L<<11)
#define SYS_CFG5131_GLOBAL_HDMI_TX_PHY_RESET (1L<<21)

/* Sysconfig Core 5132, Reset Generator control 1 ----------------------------*/
#define SYS_CFG5132                      0x210


/* Sysconfig Core 4121, HDMI Tx DDC IOs Control   ----------------------------------- */
#define SYS_CFG4121                        0x1E4

#endif // _STIH418REG_H
