/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418vtacphy.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STiH418VTACPHY_H
#define _STiH418VTACPHY_H

#include <stm_display_output.h>
#include <vibe_os.h>
#include <display/ip/stmvtac.h>


class CSTiH418VTACPhy: public CSTmVTAC
{
public:
  CSTiH418VTACPhy(CDisplayDevice       *pDev,
            stm_display_vtac_id_t ID,
            uint32_t              syscfg_video_base,
            uint32_t              vtac_tx_offset,
            uint32_t              vtac_rx_offset);

  virtual ~CSTiH418VTACPhy(void);

protected:
  uint32_t  m_VTACTXPHYCFGOffset[2];

  bool ConfigVTACPHYParams(void);

private:

  CSTiH418VTACPhy(const CSTiH418VTACPhy&);
  CSTiH418VTACPhy& operator=(const CSTiH418VTACPhy&);
};

#endif /* _STiH418VTACPHY_H */
