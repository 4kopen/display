/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418hdmiphy.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STiH418HDMIPHY_H
#define _STiH418HDMIPHY_H

#include <display/ip/hdmi/stmhdmitx6g0_c28_phy.h>

class CSTiH418HDMIPhy: public CSTmHDMITx6G0_C28_Phy
{
public:
  CSTiH418HDMIPhy(CDisplayDevice *pDev);
  ~CSTiH418HDMIPhy(void);

  bool Start(const stm_display_mode_t*, uint32_t outputFormat);
  void Stop(void);
  void ApplyReset(void);

private:
  uint32_t m_uClockGenA12;

  bool SetupRejectionPLL(const stm_display_mode_t*);
  void DisableRejectionPLL(void);

  void WriteClkGenA12Reg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pDevRegs, (m_uClockGenA12+reg), val); }
  uint32_t ReadClkGenA12Reg(uint32_t reg) { return vibe_os_read_register(m_pDevRegs, (m_uClockGenA12+reg)); }

  CSTiH418HDMIPhy(const CSTiH418HDMIPhy&);
  CSTiH418HDMIPhy& operator=(const CSTiH418HDMIPhy&);
};

#endif //_STiH418HDMIPHY_H
