/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418mixer.cpp
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/ip/tvout/stmpreformatter.h>

#include "stiH418mixer.h"


uint32_t CSTiH418Mixer::m_aMixerMap[STiH418_PLANE_COUNT] = { MIXER_ID_NONE };

CSTiH418Mixer::CSTiH418Mixer (const char               *name,
                              CDisplayDevice           *pDev,
                              uint32_t                  mixerOffset,
                              CSTmPreformatter         *pPreFormatter,
                              stm_mixer_id_t            ulVBILinkedPlane): CGammaMixer(name,pDev,mixerOffset,ulVBILinkedPlane, "CLK_PROC_MIXER")
{
  int i=0;

  MIXER_TRCIN( TRC_ID_MIXER, "" );

  /* All mixer ID inputs are valid for stiH418 */
  m_validPlanes = MIXER_ID_MASK;

  if (ulVBILinkedPlane != MIXER_ID_NONE)
    m_validPlanes |= MIXER_ID_VBI;

  /*
   * H418 Mixer only outputs unsigned and signed RGB, conversion to YUV is
   * in the TVOut pre-formatter.
   */
  m_bHasHwYCbCrMatrix = false;
  m_pPreformatter = pPreFormatter;

  m_crossbarEntryShift  = 4;
  m_crossbarEntryMask   = 0xF;

  /*
   * H418 Mixer has a VTG counter.
   */
  m_bHasVTGCounter = true;

  /*
   * There is no CURSOR plane on H418 Mixer.
   */
  m_bHasCursorPlane = false;
  m_planeCount = STiH418_PLANE_COUNT;

  /*
   * H418 Mixer has a Memory Status register (MST)
   */
  m_bHasMSTRegister = true;

  if (CSTiH418Mixer::pMixerMap == NULL)
  {
    CSTiH418Mixer::pMixerMap = (uint32_t*)m_aMixerMap;

    m_aMixerMap[STiH418_PLANE_IDX_GDP1_AUX] = MIXER_ID_GDP6;
    m_aMixerMap[STiH418_PLANE_IDX_GDP2_AUX] = MIXER_ID_GDP5;
  }

  for (i=0; i<(int)m_planeCount; i++)
  {
    m_aZorder[i].planeId  = m_planeCount;
    m_aZorder[i].bEnabled = FALSE;
  }

  // Initialize Zorder array with default plane values
  m_aZorder[0].planeId  = STiH418_PLANE_IDX_VID_MAIN ; m_nbConnectedPlane++;
  m_aZorder[1].planeId  = STiH418_PLANE_IDX_VID_PIP  ; m_nbConnectedPlane++;
  m_aZorder[2].planeId  = STiH418_PLANE_IDX_GDP1_MAIN; m_nbConnectedPlane++;
  m_aZorder[3].planeId  = STiH418_PLANE_IDX_GDP2_MAIN; m_nbConnectedPlane++;
  m_aZorder[4].planeId  = STiH418_PLANE_IDX_GDP1_AUX ; m_nbConnectedPlane++;
  m_aZorder[5].planeId  = STiH418_PLANE_IDX_GDP2_AUX ; m_nbConnectedPlane++;

  m_pZorder = (stm_zorder_item_t*)m_aZorder;

  MIXER_TRCOUT( TRC_ID_MIXER, "" );
}

CSTiH418Mixer::~CSTiH418Mixer(void) {}

void CSTiH418Mixer::UpdateColorspace(const stm_display_mode_t *pModeLine)
{
  MIXER_TRCIN( TRC_ID_MIXER, "" );

  stm_ycbcr_colorspace_t      colorspaceMode;

  CGammaMixer::UpdateColorspace(pModeLine);

  if( (pModeLine) && (m_colorspaceMode == STM_YCBCR_COLORSPACE_AUTO_SELECT) )
      if((pModeLine->mode_params.output_standards & (STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861))
      || (pModeLine->mode_params.output_standards & STM_OUTPUT_STD_HD_MASK))
        colorspaceMode = STM_YCBCR_COLORSPACE_709;
      else
        colorspaceMode = STM_YCBCR_COLORSPACE_601;
  else
      colorspaceMode = m_colorspaceMode;

  m_pPreformatter->SetColorSpaceConversion(colorspaceMode, VOUT_PF_CONV_RGB_TO_YUV);

  MIXER_TRCOUT( TRC_ID_MIXER, "" );
}


