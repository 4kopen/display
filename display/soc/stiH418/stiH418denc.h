/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418denc.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef STIH418DENC_H
#define STIH418DENC_H

#include <display/ip/tvout/stmtvoutdenc.h>

class CSTiH418DENC : public CSTmTVOutDENC
{
public:
  CSTiH418DENC(CDisplayDevice* pDev, uint32_t baseAddr, CSTmTVOutTeletext *pTeletext);
  CSTiH418DENC(void);

  bool SetMainOutputFormat(uint32_t);

private:
  CSTiH418DENC(const CSTiH418DENC&);
  CSTiH418DENC& operator=(const CSTiH418DENC&);
};

#endif // STIH418DENC_H
