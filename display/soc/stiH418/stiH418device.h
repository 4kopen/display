/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418device.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STIH418DEVICE_H
#define _STIH418DEVICE_H

#ifdef __cplusplus

#include <display/ip/displaytiming/stmclocklla.h>
#include <display/generic/DisplayDevice.h>

class CSTmC8VTG_V8_4;
class CSTmHDF_V5_0;
class CSTmVIP;
class CVideoPlug;
class CSTmTVOutTeletext;
class CSTmFSynthLLA;
class CSTiH418MainMixer;
class CSTiH418AuxMixer;
class CSTmClockLLA;
class CSTiH418DENC;
class CSTmPreformatter;

class CSTiH418Device : public CDisplayDevice
{
public:
  CSTiH418Device(stm_device_configuration_t *DevConfig);
  virtual ~CSTiH418Device(void);

  bool Create(void);

private:
  static stm_clock_t      m_pClockFs[];
  static stm_clock_t      m_pClockOut[];
  CSTmFSynthLLA          *m_pFSynthHD;
  CSTmFSynthLLA          *m_pFSynthSD;
  CSTmClockLLA           *m_pClkDivider;

  CSTmC8VTG_V8_4         *m_pMainVTG;
  CSTmC8VTG_V8_4         *m_pAuxVTG;

  CSTiH418MainMixer      *m_pMainMixer;
  CSTiH418AuxMixer       *m_pAuxMixer;
  CVideoPlug             *m_pVideoPlug1;
  CVideoPlug             *m_pVideoPlug2;
  CVideoPlug             *m_pVideoPlug3;

  bool                    m_bVGAUsingDVOPads;
  CSTmHDF_V5_0           *m_pHDFormatter;

  CSTmPreformatter       *m_pMainPreformatter;
  CSTmPreformatter       *m_pAuxPreformatter;

  CSTmVIP                *m_pDENCVIP;
  CSTmVIP                *m_pHDFVIP;
  CSTmVIP                *m_pHDMIVIP;
  CSTmVIP                *m_pDVOVIP;

  CSTiH418DENC           *m_pDENC;
  CSTmTVOutTeletext      *m_pTeletext;

  bool CreateClocks(void);
  bool CreateInfrastructure(void);
  bool CreatePlanes(void);
  bool CreateSources(void);
  bool CreateOutputs(void);

  int Freeze(void);
  int Suspend(void);
  int Resume(void);

  void PowerOnSetup(void);
  void PowerDown(void);

  CSTiH418Device(const CSTiH418Device&);
  CSTiH418Device& operator=(const CSTiH418Device&);
};

#endif /* __cplusplus */

enum {
  STiH418_OUTPUT_IDX_MAIN,
  STiH418_OUTPUT_IDX_HDMI,
  STiH418_OUTPUT_IDX_AUX,
  STiH418_OUTPUT_IDX_DVO,
  STiH418_OUTPUT_COUNT
};

enum {
  STiH418_PLANE_IDX_GDP1_MAIN,
  STiH418_PLANE_IDX_GDP2_MAIN,
  STiH418_PLANE_IDX_GDP1_AUX,
  STiH418_PLANE_IDX_GDP2_AUX,
  STiH418_PLANE_IDX_VID_MAIN,
  STiH418_PLANE_IDX_VID_PIP,
  STiH418_PLANE_IDX_VBI_AUX,
  STiH418_PLANE_COUNT
};

/* HW Mapping for GDPs */
enum {
  STiH418_PLANE_IDX_GDP1 = STiH418_PLANE_IDX_GDP1_MAIN,
  STiH418_PLANE_IDX_GDP2 = STiH418_PLANE_IDX_GDP2_MAIN,
  STiH418_PLANE_IDX_GDP3 = STiH418_PLANE_IDX_GDP1_AUX,
  STiH418_PLANE_IDX_GDP4 = STiH418_PLANE_IDX_GDP2_AUX
};

// There will be as many Sources as Decoder
// because the link decoder-source will remains connected
#define STiH418_SOURCE_COUNT     50


#endif // _STIH418DEVICE_H
