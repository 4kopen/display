/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418hdmiphy.cpp
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include "stiH418reg.h"
#include "stiH418device.h"
#include "stiH418hdmiphy.h"

////////////////////////////////////////////////////////////////////////////////
//

CSTiH418HDMIPhy::CSTiH418HDMIPhy(CDisplayDevice *pDev): CSTmHDMITx6G0_C28_Phy(pDev,
                                                                              STiH418_HDMI_BASE)
{
  TRCIN( TRC_ID_MAIN_INFO, "%p: pDev = %p", this, pDev );

  m_uClockGenA12 = STiH418_CLKGEN_A_12;

  TRCOUT( TRC_ID_MAIN_INFO, "%p", this );
}


CSTiH418HDMIPhy::~CSTiH418HDMIPhy()
{
}


bool CSTiH418HDMIPhy::SetupRejectionPLL(const stm_display_mode_t *mode)
{
  return true;
}


void CSTiH418HDMIPhy::DisableRejectionPLL()
{
}


bool CSTiH418HDMIPhy::Start(const stm_display_mode_t *mode, uint32_t outputFormat)
{
  TRCIN( TRC_ID_MAIN_INFO, "%p", this );

  bool ret = false;
  if(SetupRejectionPLL(mode))
  {
    if(!(ret = CSTmHDMITx6G0_C28_Phy::Start(mode, outputFormat)))
      DisableRejectionPLL();
  }

  TRCOUT( TRC_ID_MAIN_INFO, "%p", this );

  return ret;
}


void CSTiH418HDMIPhy::Stop(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "%p", this );

  CSTmHDMITx6G0_C28_Phy::Stop();

  DisableRejectionPLL();

  TRCOUT( TRC_ID_MAIN_INFO, "%p", this );
}

void CSTiH418HDMIPhy::ApplyReset(void)
{
  uint32_t val_sys_cfg5131;

  TRCIN( TRC_ID_MAIN_INFO, "%p", this );

  val_sys_cfg5131 = vibe_os_read_register(m_pDevRegs,STiH418_SYSCFG_CORE + SYS_CFG5131);
  val_sys_cfg5131 &= ~SYS_CFG5131_GLOBAL_HDMI_TX_PHY_RESET;
  vibe_os_write_register(m_pDevRegs,STiH418_SYSCFG_CORE + SYS_CFG5131, val_sys_cfg5131);

  vibe_os_stall_execution(15); /* ensure reset applied */

  val_sys_cfg5131 |= SYS_CFG5131_GLOBAL_HDMI_TX_PHY_RESET;
  vibe_os_write_register(m_pDevRegs,STiH418_SYSCFG_CORE + SYS_CFG5131, val_sys_cfg5131);

  TRCOUT( TRC_ID_MAIN_INFO, "%p", this );
}
