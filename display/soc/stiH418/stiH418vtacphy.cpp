/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418vtacphy.cpp
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/generic/DisplayDevice.h>

#include "stiH418vtacphy.h"
#include "stiH418reg.h"

/* VTAC PHY registers */
#define VTAC_MAIN_TX_PHY_CONFIG0         SYS_CFG8521
#define VTAC_MAIN_TX_PHY_CONFIG1         SYS_CFG8522
#define VTAC_AUX_TX_PHY_CONFIG0          SYS_CFG8525
#define VTAC_AUX_TX_PHY_CONFIG1          SYS_CFG8526

/* VTAC PHY config*/
#define  CONFIG_VTAC_TX_ENABLE_CLK_PHY          (0x01 << 0)
#define  CONFIG_VTAC_TX_PROG_N3                 (0x04 << 7)
#define  CONFIG_VTAC_TX_ENABLE_CLK_DLL          (0x01 << 1)
#define  CONFIG_VTAC_TX_RST_N_DLL_SWITCH        (0x01 << 4)
#define  CONFIG_VTAC_TX_PLL_NOT_OSC_MODE        (0x01 << 3)

/* VTAC PHY CFG Offsets */
static const stm_display_vtac_phy_params_t VTACTXPHYCFGOffset[2] =
{
    { VTAC_MAIN_TX_PHY_CONFIG0  , VTAC_MAIN_TX_PHY_CONFIG1 }
  , { VTAC_AUX_TX_PHY_CONFIG0   , VTAC_AUX_TX_PHY_CONFIG1  }
};

CSTiH418VTACPhy::CSTiH418VTACPhy (CDisplayDevice       *pDev,
                    stm_display_vtac_id_t ID,
                    uint32_t              syscfg_video_base,
                    uint32_t              vtac_tx_offset,
                    uint32_t              vtac_rx_offset):CSTmVTAC(pDev,ID,syscfg_video_base,vtac_tx_offset,vtac_rx_offset)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  m_VTACTXPHYCFGOffset[0] = VTACTXPHYCFGOffset[m_ID].cfg0;
  m_VTACTXPHYCFGOffset[1] = VTACTXPHYCFGOffset[m_ID].cfg1;

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


CSTiH418VTACPhy::~CSTiH418VTACPhy (void) {}

bool CSTiH418VTACPhy::ConfigVTACPHYParams(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );
  uint32_t Value=0;

  /*
   * This should be the SoC default, i.e. ODT disabled, SSTL and powered up
   * but lets make sure.
   */
  WriteSYSCFGVideoReg(m_VTACTXPHYCFGOffset[1], 0);

  WriteSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0], CONFIG_VTAC_TX_ENABLE_CLK_PHY);
  Value = ReadSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0]);
  WriteSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0], (CONFIG_VTAC_TX_PROG_N3|Value));
  Value = ReadSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0]);
  WriteSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0], (CONFIG_VTAC_TX_ENABLE_CLK_DLL|Value));

  Value = ReadSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0]);
  WriteSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0], (CONFIG_VTAC_TX_RST_N_DLL_SWITCH|Value));
  Value = ReadSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0]);
  WriteSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0], (CONFIG_VTAC_TX_PLL_NOT_OSC_MODE|Value));

  TRC( TRC_ID_MAIN_INFO, "CSTiH418VTACPhy::ConfigVTACPHYParams: %p = 0x%08x", ABSOLUTE_ADDRESS(m_uSYSCFGVideoOffset, m_VTACTXPHYCFGOffset[0]), ReadSYSCFGVideoReg(m_VTACTXPHYCFGOffset[0]) );
  TRC( TRC_ID_MAIN_INFO, "CSTiH418VTACPhy::ConfigVTACPHYParams: %p = 0x%08x", ABSOLUTE_ADDRESS(m_uSYSCFGVideoOffset, m_VTACTXPHYCFGOffset[1]), ReadSYSCFGVideoReg(m_VTACTXPHYCFGOffset[1]) );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;
}

