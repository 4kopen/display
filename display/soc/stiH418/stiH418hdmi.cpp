/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418hdmi.cpp
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/ip/displaytiming/stmclocklla.h>
#include <display/ip/displaytiming/stmvtg.h>

#include <display/ip/hdmi/stmhdmiregs.h>
#include <display/ip/hdmi/stmv29iframes.h>

#include <display/ip/tvout/stmvip.h>

#include "stiH418reg.h"
#include "stiH418device.h"
#include "stiH418hdmi.h"
#include "stiH418hdmiphy.h"

#define UNIPLAYER_SOFT_RST      0x0

#define UNIPLAYER_CTRL                 0x44
#define UNIPLAYER_CTRL_OPMASK          (0x7<<0)
#define UNIPLAYER_CTRL_OFF             (0)
#define UNIPLAYER_CTRL_MUTE_PCM        (0x1<<0)
#define UNIPLAYER_CTRL_MUTE_PAUSEBURST (0x2<<0)
#define UNIPLAYER_CTRL_AUDIO_DATA      (0x3<<0)
#define UNIPLAYER_CTRL_ENCODED         (0x4<<0)
#define UNIPLAYER_CTRL_CD              (0x5<<0)
#define UNIPLAYER_CTRL_DIV_ONE         (0x1<<5)
#define UNIPLAYER_CTRL_EN_SPDIF_FMT    (0x1<<17)

#define UNIPLAYER_FMT                  0x48
#define UNIPLAYER_FMT_NUM_CH_MASK      (0x7<<9)
#define UNIPLAYER_FMT_2CH              (0x1<<9)

#define TVO_HDMI_FORCE_COLOR_R_CR_MASK  (0xFF0000)
#define TVO_HDMI_FORCE_COLOR_G_Y_MASK   (0xFF00)
#define TVO_HDMI_FORCE_COLOR_B_CB_MASK  (0xFF)
#define TVO_HDMI_FORCE_COLOR_R_CR_SHIFT (16)  /* Force color R/Cr shift*/
#define TVO_HDMI_FORCE_COLOR_G_Y_SHIFT  (8)   /* Force color G/Y shift */
#define TVO_HDMI_FORCE_COLOR_B_CB_SHIFT (0)   /* Force color B/Cb shift*/

#define TVO_HDMI_FORCE_COLOR(x) ((x<<4)| (((x & 0xF0))>>4))

#define VTG_HDMI_SYNC_ID m_pTVOSyncMap[TVO_VIP_SYNC_HDMI_IDX]

#define PCM0_DEFAULT_CLOCK_FREQUENCY    (48000*128)

/*
 * These constants definition are reserved for the work around to set intermediate phy clock
 * and pll value allowing the sink to catch new transition occuring when we do a switch between 4k<->2K,
 * refer to ticket:73248.
 */
#define HDMI_CLOCK_PHY_SETTING          (99000037)
#define HDMI_WAIT_CLOCK_SWITCH_US       (100000)
#define HDMI_PLL_CTRL_VALUE             (0x2041400)

static const int HDMI_DELAY     = 6;
static const int HDMI_FILTER_DELAY = 38;

stm_clock_t CSTiH418HDMI::m_pClockOut[] = {
  /* PIX */
  { STM_CLK_PIX_MAIN,       "CLK_PIX_MAIN_DISP"   },
  { STM_CLK_PIX_AUX,        "CLK_PIX_AUX_DISP"    },
  /* HDMI */
  { STM_CLK_TMDS_HDMI,      "CLK_TMDS_HDMI"       },
  { STM_CLK_PIX_HDMI,       "CLK_PIX_HDMI"        },
  { STM_CLK_HDMI_PHY,       "CLK_REF_HDMIPHY"     },
  { STM_CLK_TMDS_HDMI_DIV2, "CLK_TMDS_HDMI_DIV2"  },
};


////////////////////////////////////////////////////////////////////////////////
//

CSTiH418HDMI::CSTiH418HDMI(
  CDisplayDevice            *pDev,
  COutput                   *pMainOutput,
  COutput                   *pAuxOutput,
  CSTmVTG                   *pMainVTG,
  CSTmVTG                   *pAuxVTG,
  CSTmVIP                   *pHDMIVIP,
  stm_clock_t               *clk_src_map,
  uint32_t                   clk_src_mapsize,
const stm_display_sync_id_t *pSyncMap): CSTmHDMI("hdmi0",
                                                 STiH418_OUTPUT_IDX_HDMI,
                                                 pDev,
                                                 (stm_hdmi_hardware_features_t )(STM_HDMI_DEEP_COLOR | STM_HDMI_RX_SENSE),
                                                 STiH418_HDMI_BASE,
                                                 pMainOutput)
{
  TRCIN( TRC_ID_MAIN_INFO, "%p: pDev = %p  main output = %p", this, pDev, pMainOutput );

  m_pMainOutput = pMainOutput;
  m_pAuxOutput  = pAuxOutput;
  m_pMainVTG    = pMainVTG;
  m_pAuxVTG     = pAuxVTG;

  m_pMasterOutput = m_pMainOutput;
  m_pMasterVTG    = m_pMainVTG;

  m_pVIP        = pHDMIVIP;
  m_pTVOSyncMap = pSyncMap;

  m_uAudioClkOffset      = STiH418_CLKGEN_D_10;
  m_uUniplayerHDMIOffset = STiH418_UNIPLAYER_HDMI_BASE;
  m_pClkDivider = NULL;

  /* Support of Ultra High Definition modes */
  m_ulCapabilities |= (OUTPUT_CAPS_UHD_DIGITAL | OUTPUT_CAPS_HDR_FORMAT);

  /* Disable hdmi internal decimation */
  m_hdmi_decimation_bypass=true;

  m_SrcClkMap     = clk_src_map;
  m_SrcClkMapSize = clk_src_mapsize;

  m_bClocksDisbaled = true;

  TRCOUT( TRC_ID_MAIN_INFO, "%p", this );
}


CSTiH418HDMI::~CSTiH418HDMI()
{
  delete m_pClkDivider;
  m_pClkDivider = NULL;
}


bool CSTiH418HDMI::Create(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  m_pClkDivider = new CSTmClockLLA(m_SrcClkMap, m_SrcClkMapSize, m_pClockOut, N_ELEMENTS(m_pClockOut), m_pDisplayDevice);
  if(!m_pClkDivider)
  {
    TRC( TRC_ID_ERROR, "failed to create ClkDivider" );
    return false;
  }

  m_pIFrameManager = new CSTmV29IFrames(m_pDisplayDevice,m_uHDMIOffset);
  if(!m_pIFrameManager || !m_pIFrameManager->Create(this,m_pMasterOutput))
  {
    TRC( TRC_ID_MAIN_INFO, "Unable to create HDMI v2.9 Info Frame manager" );
    delete m_pIFrameManager;
    m_pIFrameManager = 0;
    return false;
  }

  m_pPHY = new CSTiH418HDMIPhy(m_pDisplayDevice);
  if(!m_pPHY)
    return false;

  if(!CSTmHDMI::Create())
    return false;

  /*
   * Do this last now we have a lock allocated.
   */
  SetAudioSource(STM_AUDIO_SOURCE_2CH_I2S);

  /*
   * Now disable all clocks if there is no use of splashscreen
   */
  if(!m_bBypassHwInit)
  {
    for (uint32_t i=0; i<N_ELEMENTS(m_pClockOut); i++)
    {
      stm_clk_divider_output_name_t clk_name = static_cast<stm_clk_divider_output_name_t>(m_pClockOut[i].id);
      m_pClkDivider->Disable(clk_name);
    }
    m_bClocksDisbaled = true;
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;
}


bool CSTiH418HDMI::SetVTGSyncs(const stm_display_mode_t* pModeLine)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(!m_pTVOSyncMap)
  {
    TRC( TRC_ID_ERROR, "- failed : Undefined Syncs mapping !!" );
    return false;
  }

  /*
   * VTG sync setup;
   */
  m_pMasterVTG->SetSyncType(VTG_HDMI_SYNC_ID, STVTG_SYNC_POSITIVE);
  m_pMasterVTG->SetBnottopType(VTG_HDMI_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);

  if(((m_pVIP->GetCapabilities()&TVO_VIP_HAS_MAIN_FILTERED_422_INPUT) && (GetOutputFormat() & STM_VIDEO_OUT_422))||
    ((m_pVIP->GetCapabilities()&TVO_VIP_HAS_MAIN_FILTERED_420_INPUT) && (GetOutputFormat() & STM_VIDEO_OUT_420)))
  {
    /*
     * Adaptative Decimation filter is active, 36-tap added for YUV422 and YUV420 outputs
     */
      m_pMasterVTG->SetHSyncOffset(VTG_HDMI_SYNC_ID, HDMI_DELAY+HDMI_FILTER_DELAY);
      m_pMasterVTG->SetVSyncHOffset(VTG_HDMI_SYNC_ID, HDMI_DELAY+HDMI_FILTER_DELAY);
  }
  else
  {
    /*
     * Default Delay
     */
    m_pMasterVTG->SetHSyncOffset(VTG_HDMI_SYNC_ID, HDMI_DELAY);
    m_pMasterVTG->SetVSyncHOffset(VTG_HDMI_SYNC_ID, HDMI_DELAY);
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );

  return true;

}


void CSTiH418HDMI::DisableClocks(void)
{
  stm_clk_divider_output_name_t   pixelclock = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?STM_CLK_PIX_MAIN:STM_CLK_PIX_AUX;

  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(m_bClocksDisbaled)
    goto exit;

  TRC( TRC_ID_MAIN_INFO, "Disabling HDMI Clocks" );
  m_pClkDivider->Disable(STM_CLK_PIX_HDMI);
  m_pClkDivider->Disable(STM_CLK_HDMI_PHY);
  m_pClkDivider->Disable(STM_CLK_TMDS_HDMI);
  m_pClkDivider->Disable(STM_CLK_TMDS_HDMI_DIV2);
  m_pClkDivider->Disable(pixelclock);

  m_bClocksDisbaled = true;

exit:
  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


bool CSTiH418HDMI::PreConfiguration(const stm_display_mode_t *mode)
{
  TRCIN( TRC_ID_MAIN_INFO, "%p", this );

  int pixel_repeat = 1;
  uint32_t tmds_div2_rate = 0;
  uint32_t pixelrep_flags = mode->mode_params.flags & STM_MODE_FLAGS_HDMI_PIXELREP_MASK;
  stm_clk_divider_output_source_t source = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?STM_CLK_SRC_HD:STM_CLK_SRC_SD;
  stm_clk_divider_output_name_t   pixelclock = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?STM_CLK_PIX_MAIN:STM_CLK_PIX_AUX;
  tvo_vip_sync_source_t           sync_source = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?TVO_VIP_MAIN_SYNCS:TVO_VIP_AUX_SYNCS;
  uint32_t pixclk_rate = 0;

  if(pixelrep_flags != 0)
  {
    switch(pixelrep_flags)
    {
      case STM_MODE_FLAGS_HDMI_PIXELREP_2X:
        pixel_repeat = 2;
        break;
      case STM_MODE_FLAGS_HDMI_PIXELREP_4X:
        pixel_repeat = 4;
        break;
      default:
        pixel_repeat = 1;
    }
  }

  pixclk_rate = m_pClkDivider->GetRate(pixelclock);
  if(pixclk_rate != (mode->mode_timing.pixel_clock_freq / pixel_repeat))
  {
    /*
     * Pix clock is not in use by Master output or was not at same rate
     * so reconfigure it here with new parent and rate.
     */
    pixclk_rate = (mode->mode_timing.pixel_clock_freq / pixel_repeat);
    m_pClkDivider->SetParent(pixelclock, source);
    m_pClkDivider->SetRate(pixelclock, pixclk_rate);
  }

  TRC( TRC_ID_MAIN_INFO, "Enabling HDMI Clocks" );
  /*
   * Enable Pixel Clock
   */
  m_pClkDivider->Enable(pixelclock);

  /*
   * HDMI pixel clock must be the same as main pixel clock.
   */
  m_pClkDivider->SetParent(STM_CLK_TMDS_HDMI, STM_CLK_SRC_IN_0);
  m_pClkDivider->Enable(STM_CLK_TMDS_HDMI);
  tmds_div2_rate = m_pClkDivider->GetRate(STM_CLK_TMDS_HDMI) >> 1;
  m_pClkDivider->SetParent(STM_CLK_TMDS_HDMI_DIV2, STM_CLK_SRC_IN_0);
  m_pClkDivider->SetRate(STM_CLK_TMDS_HDMI_DIV2, tmds_div2_rate);
  m_pClkDivider->Enable(STM_CLK_TMDS_HDMI_DIV2);
  m_pClkDivider->SetParent(STM_CLK_HDMI_PHY, source);
  if (m_ulOutputFormat & STM_VIDEO_OUT_420)
  {
     m_pClkDivider->SetRate(STM_CLK_HDMI_PHY, pixclk_rate>>1);
  }
  else
  {
     m_pClkDivider->SetRate(STM_CLK_HDMI_PHY, pixclk_rate);
  }

  m_pClkDivider->Enable(STM_CLK_HDMI_PHY);
  m_pClkDivider->SetParent(STM_CLK_PIX_HDMI, source);
  m_pClkDivider->SetRate(STM_CLK_PIX_HDMI, pixclk_rate);
  m_pClkDivider->Enable(STM_CLK_PIX_HDMI);

  m_bClocksDisbaled = false;

  /*
   * Configure VIP syncs up front.
   */
  m_pVIP->SelectSync(sync_source);
  return true;
}

bool CSTiH418HDMI::PostConfiguration(const stm_display_mode_t*)
{
  return true;
}
/*
 * Switch 4k<->2K requires intermediate phy clock setting
 * which allows the sink to detect clock variation and catch new transition
 * and clock settings partcicularly for this switch
 */
bool CSTiH418HDMI::IntermediateConfiguration(void)
{
  if (m_bForceClock)
  {
    TRC( TRC_ID_HDMI, "Configure PLL for intermediate frequency needed for transition" );

    m_pClkDivider->SetRate(STM_CLK_HDMI_PHY, HDMI_CLOCK_PHY_SETTING);
    m_pClkDivider->Enable(STM_CLK_HDMI_PHY);

    m_pPHY->SetPll(HDMI_PLL_CTRL_VALUE);

    /* wait and relax while doing the switch
     * TO DO: this work around will be applied for a specific list of resolutions switch and not all of them.
     */
    vibe_os_stall_execution(HDMI_WAIT_CLOCK_SWITCH_US);
  }
  return true;
}

int CSTiH418HDMI::GetAudioFrequency(void)
{
  uint32_t  clock_rate = 0;
  struct vibe_clk pcm0_clock;
  if(!vibe_os_clk_get("CLK_PCM_HDMI", &pcm0_clock))
  {
    clock_rate = vibe_os_clk_get_rate(&pcm0_clock);
    if(!clock_rate)
      clock_rate = PCM0_DEFAULT_CLOCK_FREQUENCY;
    TRC( TRC_ID_HDMI, "PCM0 clock rate is %dKHz!", clock_rate );
    vibe_os_clk_put(&pcm0_clock);
    return (clock_rate);
  }
  return 0;
}


void CSTiH418HDMI::GetAudioHWState(stm_hdmi_audio_state_t *state)
{
  uint32_t tmp = ReadUniplayerReg(UNIPLAYER_CTRL);
  switch(tmp & UNIPLAYER_CTRL_OPMASK)
  {
    case UNIPLAYER_CTRL_MUTE_PAUSEBURST:
    case UNIPLAYER_CTRL_AUDIO_DATA:
    case UNIPLAYER_CTRL_ENCODED:
      state->status = STM_HDMI_AUDIO_RUNNING;
      break;
    case UNIPLAYER_CTRL_MUTE_PCM: /* Stop on PCM mute */
    case UNIPLAYER_CTRL_OFF:
    default:
      state->status = STM_HDMI_AUDIO_STOPPED;
  }

  state->clock_divide = 2;
  state->clock_cts_divide = 1;
}


bool CSTiH418HDMI::SetOutputFormat(uint32_t format)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  if((m_pVIP->GetCapabilities()&TVO_VIP_HAS_MAIN_FILTERED_422_INPUT) && (format & STM_VIDEO_OUT_422 ))
  {
    /* STM_VIDEO_OUT_36BIT is forced to have 12 bits rounding per component in VIP */
    m_pVIP->SetInputParams((m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?TVO_VIP_MAIN_FILTERED:TVO_VIP_AUX_VIDEO,
                           ((format & (~STM_VIDEO_OUT_DEPTH_MASK))|STM_VIDEO_OUT_36BIT), m_signalRange);
  }
  else if((m_pVIP->GetCapabilities()&TVO_VIP_HAS_MAIN_FILTERED_420_INPUT) && (format & STM_VIDEO_OUT_420 ))
  {
    m_pVIP->SetInputParams((m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?TVO_VIP_MAIN_FILTERED:TVO_VIP_AUX_VIDEO, format, m_signalRange);
  }
  else
  {
    m_pVIP->SetInputParams((m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?TVO_VIP_MAIN_VIDEO:TVO_VIP_AUX_VIDEO, format, m_signalRange);
  }

  /*
   * TODO: Get this out of here and out of the H415 and H416 HDMI classes and
   *       into the VIP class itself.
   *
   *       (a) we cannot have it copied every time there is a new SoC
   *       (b) it should be available on other output types, particularly DVO
   */
  if (m_bForceColor)
  {
    uint32_t R, ucRCr;
    uint32_t G, ucGY;
    uint32_t B, ucBCb;
    uint32_t tmp;
    stm_ycbcr_colorspace_t      m_colorspaceMode;

    R = (( m_uForcedOutputColor & TVO_HDMI_FORCE_COLOR_R_CR_MASK) >> TVO_HDMI_FORCE_COLOR_R_CR_SHIFT);
    G = (( m_uForcedOutputColor & TVO_HDMI_FORCE_COLOR_G_Y_MASK)  >> TVO_HDMI_FORCE_COLOR_G_Y_SHIFT );
    B = (( m_uForcedOutputColor & TVO_HDMI_FORCE_COLOR_B_CB_MASK) >> TVO_HDMI_FORCE_COLOR_B_CB_SHIFT);

    if ( (format & STM_VIDEO_OUT_YUV )== STM_VIDEO_OUT_YUV)
    {
      m_pMasterOutput->GetControl(OUTPUT_CTRL_YCBCR_COLORSPACE, &tmp);
      m_colorspaceMode = static_cast<stm_ycbcr_colorspace_t>(tmp);

      if (m_colorspaceMode == STM_YCBCR_COLORSPACE_601)
      {
        ucGY =  (uint32_t)((299 * R + 587 * G + 114 * B)/1000);
        ucBCb = (uint32_t)(128 + (((5000 * B )/10000)-((1687 * R)/10000)-((3313 * G)/10000)));
        ucRCr = (uint32_t)(128 + (((5000 * R)/10000)-((4187 * G)/10000)-((813 * B)/10000)));
      }
      else
      {

        ucGY =  (uint32_t)((212 * R + 715 * G + 72 * B)/1000);
        ucBCb = (uint32_t)(128 + (((5000 * B )/10000)-((1145 * R)/10000)-((3854 * G)/10000)));
        ucRCr = (uint32_t)(128 + (((5000 * R)/10000)-((4541 * G)/10000)-((458 * B)/10000)));

      }
      m_pVIP->SetForceColor(true, TVO_HDMI_FORCE_COLOR(ucRCr), TVO_HDMI_FORCE_COLOR(ucGY), TVO_HDMI_FORCE_COLOR(ucBCb));
    }
    else
    {
      m_pVIP->SetForceColor(true, TVO_HDMI_FORCE_COLOR(R), TVO_HDMI_FORCE_COLOR(G), TVO_HDMI_FORCE_COLOR(B));
    }
  }
  else
  {
    m_pVIP->SetForceColor(false, 0, 0, 0);
  }

  if(!CSTmHDMI::SetOutputFormat(format))
    return false;

  return SetVTGSyncs(GetCurrentDisplayMode());
}


void CSTiH418HDMI::SetSignalRangeClipping(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  /*
   * We need to defer to SetOutputFormat() rather than setting the VIP
   * directly, in order to maintain the forced color output if it is
   * currently enabled.
   */
  SetOutputFormat(m_ulOutputFormat);

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


uint32_t CSTiH418HDMI::SetControl(stm_output_control_t ctrl, uint32_t newVal)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  switch (ctrl)
  {
    case OUTPUT_CTRL_VIDEO_SOURCE_SELECT:
    {
      if((newVal != STM_VIDEO_SOURCE_MAIN_COMPOSITOR)&&(newVal != STM_VIDEO_SOURCE_AUX_COMPOSITOR))
        return STM_OUT_INVALID_VALUE;

      if(m_bIsStarted)
        return STM_OUT_BUSY;

      m_VideoSource = static_cast<stm_display_output_video_source_t>(newVal);
      m_pMasterOutput->UnRegisterSlavedOutput(this);
      m_pMasterVTG = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?m_pMainVTG:m_pAuxVTG;
      m_pMasterOutput = (m_VideoSource==STM_VIDEO_SOURCE_MAIN_COMPOSITOR)?m_pMainOutput:m_pAuxOutput;
      m_pMasterOutput->RegisterSlavedOutput(this);

      break;
    }
    case OUTPUT_CTRL_FORCE_COLOR:
    {
      m_bForceColor = (newVal != 0);
      SetOutputFormat(m_ulOutputFormat);
      break;
    }
    case OUTPUT_CTRL_FORCED_RGB_VALUE:
    {
      m_uForcedOutputColor = newVal;
      SetOutputFormat(m_ulOutputFormat);
      break;
    }
    case OUTPUT_CTRL_HDMI_FORCE_CLOCK :
    {
      m_bForceClock = (newVal != 0);
      IntermediateConfiguration();
      break;
    }
    default:
      return CSTmHDMI::SetControl(ctrl, newVal);
  }

  return STM_OUT_OK;
}
