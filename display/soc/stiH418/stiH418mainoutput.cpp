/***********************************************************************
 *
 * File: display/soc/stiH418/stiH418mainoutput.cpp
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplayPlane.h>

#include <display/ip/displaytiming/stmclocklla.h>
#include <display/ip/displaytiming/stmfsynth.h>
#include <display/ip/displaytiming/stmvtg.h>

#include <display/ip/tvout/stmtvoutdenc.h>
#include <display/ip/tvout/stmvip.h>
#include <display/ip/hdf/stmhdf.h>

#include <display/ip/stmviewport.h>
#include <display/ip/misr/stmmisrmaintvout.h>



#include "stiH418reg.h"
#include "stiH418device.h"
#include "stiH418mainoutput.h"
#include "stiH418mixer.h"

#define VTG_DENC_SYNC_ID  m_sync_sel_map[TVO_VIP_SYNC_DENC_IDX]
#define VTG_HDF_SYNC_ID   m_sync_sel_map[TVO_VIP_SYNC_HDF_IDX]
#define VTG_PADS_SYNC_ID  m_sync_sel_map[TVO_VIP_SYNC_TYPE_SDDCS]

/*
 * TODO: VGA delay is defined using visual tests
 * this should be reverified by h/w validation team
 */
#define VGA_DELAY (16)

//////////////////////////////////////////////////////////////////////////////
//
// HD, SD Progressive and SD Interlaced Output on main HD Dacs
//
CSTiH418MainOutput::CSTiH418MainOutput(
  CDisplayDevice               *pDev,
  CSTmVTG                      *pVTG,
  CSTmTVOutDENC                *pDENC,
  CGammaMixer                  *pMixer,
  CSTmFSynth                   *pFSynth,
  CSTmHDFormatter              *pHDFormatter,
  CSTmClockLLA                 *pClkDivider,
  CSTmVIP                      *pHDFVIP,
  CSTmVIP                      *pDENCVIP,
  const stm_display_sync_id_t  *syncMap): CSTmMainTVOutput("analog_hdout0",
                                                           STiH418_OUTPUT_IDX_MAIN,
                                                           pDev,
                                                           pVTG,
                                                           pDENC,
                                                           pMixer,
                                                           pHDFormatter)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  m_pFSynth         = pFSynth;
  m_pClkDivider     = pClkDivider;
  m_pHDFVIP         = pHDFVIP;
  m_pDENCVIP        = pDENCVIP;

  m_uDENCSyncOffset = DENC_DELAY;

  m_sync_sel_map    = syncMap;

  /*
   * Analog output does not support HDR but we add this capability here
   * to allow the control OUTPUT_CTRL_HDR_FORMAT on This master output.
   * The control will be simply transmitted to HDMI slaved output and
   * all connected planes to the Mixer owned by this master output.
   */
  m_ulCapabilities |= OUTPUT_CAPS_HDR_FORMAT;

  if((m_pMisrMain = new CSTmMisrMainTVOut(pDev, STiH418_TVO_MAIN_PF_CTRL, STiH418_TVO_HD_OUT_CTRL, STiH418_TVO_SD_OUT_CTRL, STiH418_TVO_DVO_CTRL))==0)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "failed to create m_pMisrMain" );
  }

  /*
   * Max Resolution is 2160P60
   */
  m_ulMaxPixClock   = 594000000;
  m_clk_divider     = STM_CLK_DIV_1;
  m_bClocksDisbaled = true;

  if(!pDev->BypassHwInitialization())
    DisableDACs();

  TRCOUT( TRC_ID_TVOUT, "" );
}


CSTiH418MainOutput::~CSTiH418MainOutput()
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );
  delete m_pMisrMain;
  m_pMisrMain = NULL;
  TRCOUT( TRC_ID_TVOUT, "" );
}

///////////////////////////////////////////////////////////////////////////////
// Main MISR Data capture implementation
//
void CSTiH418MainOutput::SetMisrData(const stm_time64_t LastVTGEvtTime, uint32_t  LastVTGEvt)
{
  const stm_display_mode_t   *pCurrentMode;
  OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "Configuring MAIN MISR capture" );
  if(m_pMisrMain == NULL)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "m_pMisrMain is NULL" );
    return;
  }
  pCurrentMode=GetCurrentDisplayMode();
  if(!pCurrentMode)
  {
    OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "MISR Capture start requested, but Display is not active, pCurrentMode NULL" );
    return;
  }

  m_pMisrMain->ReadMisrSigns(LastVTGEvtTime, LastVTGEvt, m_ulOutputFormat);
}

void CSTiH418MainOutput::UpdateMisrCtrl(void)
{
  const stm_display_mode_t *pCurrentMode;

  if(m_pMisrMain == NULL)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "m_pMisrMain is NULL" );
    return;
  }

  pCurrentMode  = GetCurrentDisplayMode();
  if (pCurrentMode)
  {
    m_pMisrMain->UpdateMisrControlValue(pCurrentMode, m_ulOutputFormat);
  }
}

TuningResults CSTiH418MainOutput::SetTuning( uint16_t service,
                                             void    *inputList,
                                             uint32_t inputListSize,
                                             void    *outputList,
                                             uint32_t outputListSize)
{
    TuningResults res = TUNING_INVALID_PARAMETER;
    tuning_service_type_t ServiceType = (tuning_service_type_t)service;

    OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "ServiceType %x", ServiceType );

    if(m_pMisrMain == NULL)
    {
      OUTPUT_TRC( TRC_ID_ERROR, "m_pMisrMain is NULL" );
      return TUNING_SERVICE_NOT_SUPPORTED;
    }

    switch(ServiceType)
    {
      case MISR_CAPABILITY:
      {
        res = m_pMisrMain->GetMisrCapability(outputList, outputListSize);
        break;
      }
      case MISR_SET_CONTROL:
      {
        /*Get Current Mode for Scan Type and Vport Params*/
        const stm_display_mode_t *pCurrentMode;
        pCurrentMode  = GetCurrentDisplayMode();
        if(pCurrentMode)
        {
          res = m_pMisrMain->SetMisrControlValue(service, inputList, pCurrentMode);
        }
        else
        {
          res = TUNING_SERVICE_NOT_SUPPORTED;
        }
        break;
      }
      case MISR_COLLECT:
      {
        res = m_pMisrMain->CollectMisrValues(outputList);
        break;
      }
      case MISR_STOP:
      {
        m_pMisrMain->ResetMisrState(m_ulOutputFormat);
        res = TUNING_OK;
        break;
      }
      default:
        break;
    }
    return res;
}

bool CSTiH418MainOutput::SetMixerMap(const CDisplayPlane * plane, uint32_t mixerIDs)
{
  return m_pMixer->SetMixerMap(plane, mixerIDs);
}


///////////////////////////////////////////////////////////////////////////////
// Main output configuration
//

bool CSTiH418MainOutput::ConfigureOutput(const uint32_t format)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  /*
   * The HDF VIP feeds 10bit DACs, so the color depth should be 30bit. The
   * DENC takes a fixed input format from its VIP, which is 24bit YUV.
   */
  uint32_t hdfformat = (format & ~STM_VIDEO_OUT_DEPTH_MASK) | STM_VIDEO_OUT_30BIT;

  /*
   * NOTE: This assumes that when using the DENC, we always want to use
   *       the DENC's formatted output on the HDDACs. If dual SCART is required
   *       in the future then we would have to add some additional setup here.
   */
  if(m_bUsingDENC)
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Configuring DENC Output" );
    m_pHDFVIP->SelectSync(TVO_VIP_MAIN_SYNCS);
    m_pHDFVIP->SetColorChannelOrder(TVO_VIP_CB_B, TVO_VIP_Y_G, TVO_VIP_CR_R);
    m_pHDFVIP->SetInputParams(TVO_VIP_DENC123, hdfformat, STM_SIGNAL_FULL_RANGE);
  }
  else
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Configuring HD Output" );
    m_pHDFVIP->SetColorChannelOrder(TVO_VIP_CR_R, TVO_VIP_Y_G, TVO_VIP_CB_B);
    m_pHDFVIP->SetInputParams(TVO_VIP_MAIN_VIDEO, hdfformat, m_signalRange);
  }
  TRCOUT( TRC_ID_TVOUT, "" );
  return true;
}


void CSTiH418MainOutput::ConfigureDisplayClocks(const stm_display_mode_t *mode, const uint32_t format)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  uint32_t tvStandard = mode->mode_params.output_standards;

  m_pClkDivider->Enable(STM_CLK_PIX_MAIN, STM_CLK_SRC_HD, m_clk_divider);

  if(tvStandard & STM_OUTPUT_STD_SD_MASK)
  {
    /*
     * Only switch the DENC and SDDACs output clock if we are using the DENC.
     *
     * We should not need to touch CLK_PIX_AUX at all for the Main->DENC case.
     * However due to a limitation of current hardware which makes the DENC clock
     * hardwired to the CLK_PIX_AUX clock we're also setting its source to Main
     * when moving the DENC to Main mixer too !!
     *
     * This will be fixed for coming HW.
     */
    if(m_bUsingDENC)
    {
      m_pClkDivider->Enable(STM_CLK_PIX_AUX   , STM_CLK_SRC_HD, m_clk_divider);
      m_pClkDivider->Enable(STM_CLK_DENC      , STM_CLK_SRC_HD, STM_CLK_DIV_4);
      m_pClkDivider->Enable(STM_CLK_OUT_SDDACS, STM_CLK_SRC_HD, STM_CLK_DIV_1);
    }
  }

  if(format & (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_YUV))
  {
    if(m_bUsingDENC)
    {
      /*
       * When we are routing DENC output to the HD Formatter the HDDACs pixel clock
       * needs to be the same as the DENC clock (27MHz) not the HD pixel clock
       * (13.5MHz).
       */
      m_pClkDivider->Enable(STM_CLK_PIX_HDDACS, STM_CLK_SRC_HD, STM_CLK_DIV_4);
    }
    else
    {
      m_pClkDivider->Enable(STM_CLK_PIX_HDDACS, STM_CLK_SRC_HD, m_clk_divider);
    }

    if(tvStandard & (STM_OUTPUT_STD_HD_MASK | STM_OUTPUT_STD_XGA))
      m_pClkDivider->Enable(STM_CLK_OUT_HDDACS, STM_CLK_SRC_HD, STM_CLK_DIV_4);
    else
      m_pClkDivider->Enable(STM_CLK_OUT_HDDACS, STM_CLK_SRC_HD, STM_CLK_DIV_1);
  }

  m_bClocksDisbaled = false;

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTiH418MainOutput::ConfigureClockDivider(const stm_display_mode_t *mode)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  uint32_t tvStandard = mode->mode_params.output_standards;

  if(tvStandard & STM_OUTPUT_STD_NTG5)
  {
    m_clk_divider = STM_CLK_DIV_2;
  }
  else if(tvStandard & (STM_OUTPUT_STD_ED_MASK | STM_OUTPUT_STD_VGA))
  {
    m_clk_divider = STM_CLK_DIV_4;
  }
  else if(tvStandard & STM_OUTPUT_STD_SD_MASK)
  {
    m_clk_divider = STM_CLK_DIV_8;
  }
  else
  {
    uint8_t divider = 0;

    while(m_ulMaxPixClock && (divider <= STM_CLK_DIV_8))
    {
      if ((((m_ulMaxPixClock / mode->mode_timing.pixel_clock_freq) >> divider) & 0x1) == 0x1)
        break;
      divider++;
    }

    m_clk_divider = static_cast<stm_clk_divider_output_divide_t>(divider);
  }

  OUTPUT_TRC( TRC_ID_TVOUT, "set fsynth divide to %d", (1 << m_clk_divider) );
  m_pFSynth->SetDivider((1 << m_clk_divider));

  TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTiH418MainOutput::DisableClocks(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(m_bClocksDisbaled)
    goto exit;

  if(!m_pHDFormatter->IsStarted())
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Disabling HD Clocks" );
    m_pClkDivider->Disable(STM_CLK_OUT_HDDACS);
    m_pClkDivider->Disable(STM_CLK_PIX_HDDACS);
  }

  if(m_bUsingDENC)
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Disabling SD Clocks" );
    m_pClkDivider->Disable(STM_CLK_OUT_SDDACS);
    m_pClkDivider->Disable(STM_CLK_DENC);
  }

  OUTPUT_TRC( TRC_ID_TVOUT, "Disabling PIX Clock" );
  m_pClkDivider->Disable(STM_CLK_PIX_MAIN);

  m_bClocksDisbaled = true;

exit:
  TRCOUT( TRC_ID_TVOUT, "" );
}


bool CSTiH418MainOutput::SetVTGSyncs(const stm_display_mode_t *mode)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(!m_sync_sel_map)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "- failed : Undefined Syncs mapping !!" );
    return false;
  }

  uint32_t tvStandard = mode->mode_params.output_standards;

  ConfigureClockDivider(mode);

  if((tvStandard == STM_OUTPUT_STD_NTG5) || (tvStandard == (STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861)) || (tvStandard == STM_OUTPUT_STD_CEA861))
  {
    /*
     * Digital only formats, just set the AWG syncs to a default
     */
    m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID,  STVTG_SYNC_POSITIVE);
    m_pVTG->SetBnottopType(VTG_HDF_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
    m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID, 0);
    m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, 0);
  }
  else if(tvStandard & (STM_OUTPUT_STD_HD_MASK  | STM_OUTPUT_STD_XGA))
  {
    if (m_ulOutputFormat & STM_VIDEO_OUT_RGB)
    {
    m_pVTG->SetBnottopType(VTG_HDF_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
    //1280x1024-60
    if(mode->mode_id == STM_TIMING_MODE_1024P60000_108000)
    {
      m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID, STVTG_SYNC_POSITIVE);
      m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID, VGA_DELAY -11);
      m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, VGA_DELAY);
    }
    //1024x768-60
    if(mode->mode_id == STM_TIMING_MODE_768P60000_65000)
    {
      m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID, STVTG_SYNC_NEGATIVE);
      m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID,VGA_DELAY -4);
      m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, VGA_DELAY);
    }
    //1024x768-70
    else if(mode->mode_id == STM_TIMING_MODE_768P70000_75000)
          {
          m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID, STVTG_SYNC_NEGATIVE);
          m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID,VGA_DELAY -7);
          m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, VGA_DELAY);
          }
    //1024x768-75
    else if(mode->mode_id == STM_TIMING_MODE_768P75000_78750)
          {
          m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID, STVTG_SYNC_POSITIVE);
          m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID,VGA_DELAY -10);
          m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, VGA_DELAY);
          }
    }
    else if (m_ulOutputFormat & STM_VIDEO_OUT_YUV)
    {
      int yuv_hsync_delay = AWG_DELAY_HD + 1;
      /* INFO: following should be reverified if above assignement makes yuv_hsync_delay positive */
      int yuv_vsync_h_delay = mode->mode_timing.pixels_per_line + yuv_hsync_delay;
      if(mode->mode_params.scan_type == STM_INTERLACED_SCAN)
      {
          yuv_vsync_h_delay = (mode->mode_timing.pixels_per_line/2) + yuv_hsync_delay;
        /* End of INFO */
        m_pVTG->SetBnottopType(VTG_HDF_SYNC_ID, STVTG_SYNC_BNOTTOP_INVERTED);
      }
      else
      {
        /* 720P50 has 1 pixel horizental shift to the right */
        if(mode->mode_timing.pixels_per_line == 1980)
        {
          yuv_hsync_delay += 1;
          yuv_vsync_h_delay += 1;
        }
        m_pVTG->SetBnottopType(VTG_HDF_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
      }
      m_pVTG->SetSyncType(VTG_HDF_SYNC_ID, STVTG_SYNC_POSITIVE);
      m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID, yuv_hsync_delay);
      m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, yuv_vsync_h_delay);
    }
  }
  else if(tvStandard & STM_OUTPUT_STD_SD_MASK)
  {
    m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID,  STVTG_SYNC_POSITIVE);
    m_pVTG->SetBnottopType(VTG_HDF_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
    m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID, AWG_DELAY_SD);
    m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, AWG_DELAY_SD);

    if(m_bUsingDENC)
    {
      int denc_sync_shift = m_uDENCSyncOffset;
      if(mode->mode_timing.lines_per_frame == 525)
      {
        /*
         * Account for the difference in the size of the front porch between the
         * analog and digital 525line mode definitions. The mode line we specify
         * is the digital one. This was expected to be 3 pixels but that produces
         * a clear de-centering of the image. This could be down to the rise time
         * to the center of the analog sync pulse or more likely the fact we do not
         * have a clear explanation for the main to DENC delay in the first place.
         */
        denc_sync_shift -= 2;
      }
      m_pVTG->SetSyncType   (VTG_DENC_SYNC_ID, STVTG_SYNC_TOP_NOT_BOT);
      m_pVTG->SetBnottopType(VTG_DENC_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
      m_pVTG->SetHSyncOffset(VTG_DENC_SYNC_ID,  denc_sync_shift);
      m_pVTG->SetVSyncHOffset(VTG_DENC_SYNC_ID, denc_sync_shift);

      /*
       * Set the DENC VIP here as the input format is fixed, to make sure the
       * syncs, clocks and data from the main path are routed to the DENC before
       * we start it.
       *
       * Note: we do not enable clipping in the DENC VIP because the clipping is
       *       controlled as part of the DENC's signal formatting.
       *
       */
      OUTPUT_TRC( TRC_ID_TVOUT, "Setting DENC VIP to Main path" );
      m_pDENCVIP->SetInputParams(TVO_VIP_MAIN_VIDEO,
                                 (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_24BIT),
                                 STM_SIGNAL_FULL_RANGE);

    }
  }
  else if(tvStandard & (STM_OUTPUT_STD_ED_MASK | STM_OUTPUT_STD_VGA))
  {
    if (m_ulOutputFormat & STM_VIDEO_OUT_RGB)
    {
      //640x480-60/59
      m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID, STVTG_SYNC_NEGATIVE);
      m_pVTG->SetBnottopType(VTG_HDF_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
      m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID, VGA_DELAY -9);
      m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, VGA_DELAY);
    }
    else if (m_ulOutputFormat & STM_VIDEO_OUT_YUV)
    {
      /* INFO: following should be reverified if above assignement makes yuv_hsync_delay positive */
      int yuv_hsync_delay = AWG_DELAY_ED;
      int yuv_vsync_h_delay = yuv_hsync_delay;
      if(mode->mode_timing.lines_per_frame == 525)
      {
        yuv_hsync_delay += 1;
        yuv_vsync_h_delay = -(mode->mode_timing.pixels_per_line + yuv_hsync_delay);
      }
      /* End of INFO */
      m_pVTG->SetSyncType   (VTG_HDF_SYNC_ID, STVTG_SYNC_POSITIVE);
      m_pVTG->SetHSyncOffset(VTG_HDF_SYNC_ID, yuv_hsync_delay);
      m_pVTG->SetVSyncHOffset(VTG_HDF_SYNC_ID, yuv_vsync_h_delay );
      m_pVTG->SetBnottopType(VTG_HDF_SYNC_ID, STVTG_SYNC_BNOTTOP_NOT_INVERTED);
    }
  }
  else
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Unsupported Output Standard" );
    return false;
  }

  TRCOUT( TRC_ID_TVOUT, "" );
  return true;
}


uint32_t CSTiH418MainOutput::SetControl(stm_output_control_t ctrl, uint32_t val)
{
  OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "ctrl = %d val = %u", ctrl, val );
  switch(ctrl)
  {
    case OUTPUT_CTRL_CLIP_SIGNAL_RANGE:
    {
      /*
       * Deliberately skip the MainTVOutput implementation as the HDFormatter
       * does not control the clipping, the VIP does.
       */
      if(CSTmMasterOutput::SetControl(ctrl,val) != STM_OUT_OK)
        return STM_OUT_INVALID_VALUE;

      if(m_bIsStarted && (m_pHDFormatter->GetOwner() == this))
        ConfigureOutput(m_ulOutputFormat);

      break;
    }
    default:
      return CSTmMainTVOutput::SetControl(ctrl,val);
  }

  return STM_OUT_OK;
}


void CSTiH418MainOutput::EnableDACs(void)
{
  uint32_t val;
  bool power_up_dacs = false;

  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  /*
   * Do nothing when DACs are powered down
   */
  if(m_bDacPowerDown)
  {
    TRCOUT( TRC_ID_TVOUT, "Video DACs are powered down !!" );
    return;
  }

  val = ReadSysCfgReg(SYS_CFG5072);

  if(!m_pHDFormatter->IsStarted()
  || ((m_pHDFormatter->GetOwner() == this) && ((m_ulOutputFormat & (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_YUV))==0)) )
  {
    /*
     * Only tri-state the HD DAC setup if this pipeline is not
     * using the HD formatter.
     */
    OUTPUT_TRC( TRC_ID_TVOUT, "Tri-Stating HD DACs" );
    val |=  SYS_CFG5072_DAC_HZUVW;
  }
  else if ((m_pHDFormatter->GetOwner() == this) && (m_ulOutputFormat & (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_YUV)))
  {
    /*
     * We can blindly enable the HD DACs if we need them, it doesn't
     * matter if they were already in use by any other pipeline.
     */
    OUTPUT_TRC( TRC_ID_TVOUT, "Enabling HD DACs" );
    val &= ~SYS_CFG5072_DAC_HZUVW;

    power_up_dacs = true;
  }

  if(!m_pDENC->IsStarted()
  || ((m_pDENC->GetOwner() == this) && ((m_ulOutputFormat & (STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC))==0)) )
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Tri-Stating CVBS DAC" );
    val |= SYS_CFG5072_DAC_HZX;
  }
  else if ((m_pDENC->GetOwner() == this) && (m_ulOutputFormat & (STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC)))
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Enabling CVBS DAC" );
    val &= ~SYS_CFG5072_DAC_HZX;

    WriteBitField(TVO_SD_DAC_CONFIG, TVO_DAC_DENC_OUT_SD_SHIFT, TVO_DAC_DENC_OUT_SD_WIDTH, TVO_DAC_DENC_OUT_SD_DAC456);

    power_up_dacs = true;
  }

  /*
   * Note: No S-Video DACs
   */
  WriteSysCfgReg(SYS_CFG5072,val);

  if(power_up_dacs)
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Powering Up DACs" );
    WriteBitField(TVO_HD_DAC_CONFIG, TVO_DAC_POFF_DACS_SHIFT, TVO_DAC_POFF_DACS_WIDTH, ~TVO_DAC_POFF_DACS);
  }

  TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTiH418MainOutput::DisableDACs(void)
{
  uint32_t val;

  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  val = ReadSysCfgReg(SYS_CFG5072);

  if(!m_pHDFormatter->IsStarted())
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Tri-Stating HD DACs" );
    val |=  SYS_CFG5072_DAC_HZUVW;
  }

  if(!m_pDENC->IsStarted())
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Tri-Stating CVBS DAC" );
    val |= SYS_CFG5072_DAC_HZX;
  }

  WriteSysCfgReg(SYS_CFG5072,val);

  /*
   * In spite of having two configuration registers one for HD DACs and another for SD DACs
   * we only have one power down configuration bit available only inside the HD DACs configuration's
   * register.
   * So setting the bit '0' in TVO_HD_DAC_CONFIG register will stop both HD and SD DACs !!
   */
  if(!m_pHDFormatter->IsStarted() && !m_pDENC->IsStarted())
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "Powering Down DACs" );
    WriteBitField(TVO_HD_DAC_CONFIG, TVO_DAC_POFF_DACS_SHIFT, TVO_DAC_POFF_DACS_WIDTH, TVO_DAC_POFF_DACS);
  }

  TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTiH418MainOutput::PowerDownHDDACs(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  WriteBitField(TVO_HD_DAC_CONFIG, TVO_DAC_POFF_DACS_SHIFT, TVO_DAC_POFF_DACS_WIDTH, TVO_DAC_POFF_DACS);

  TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTiH418MainOutput::PowerDownDACs(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  WriteBitField(TVO_HD_DAC_CONFIG, TVO_DAC_POFF_DACS_SHIFT, TVO_DAC_POFF_DACS_WIDTH, TVO_DAC_POFF_DACS);

  COutput::PowerDownDACs();

  TRCOUT( TRC_ID_TVOUT, "" );
}
