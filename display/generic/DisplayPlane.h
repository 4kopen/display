/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2000-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-09-30
***************************************************************************/

#ifndef _DISPLAYPLANE_H
#define _DISPLAYPLANE_H

#include "stm_display_plane.h"
#include "DisplayDevice.h"
#include "DisplayNode.h"
#include "DisplayQueue.h"
#include "DisplayInfo.h"
#include "ControlNode.h"
#include "ListenerNode.h"
#include <vibe_debug.h>


#define VALID_PLANE_HANDLE              0x01250125
#define CONTROL_STATUS_ARRAY_MAX_NB     10

/*
 * SDR --> HDR10 (Video Range) : 6.25% hdr_offset and 63.679% hdr_gain, approximately.
 *
 * * RGBOUT = ( ( ( (RGBIN x gain_register_hdr) >> 4) + 1) >> 1) + offset_register_hdr
 *
 *    - gain_register_hdr   = gain_register x hdr_gain      = 54.688%
 *    - offset_register_hdr = offset_register + hdr_offset  = 12.5%
 */
static const uint32_t AGC_HDR_ST2084_VIDEO_GAIN            = 82;  /* additional HDR10 gain adjustment   */
static const uint32_t AGC_HDR_ST2084_CONSTANT_BLACK_LEVEL  = 64;  /* additional HDR10 offset adjustment */

/*
 * SDR --> HDR-HLG (Video Range) : 0% hlg_offset (no additional HLG adjustement)
 *                                 and 64.06% hlg_gain, approximately.
 *
 * * RGBOUT = ( ( ( (RGBIN x gain_register_hlg) >> 4) + 1) >> 1) + offset_register_hlg
 *
 *    - gain_register_hlg   = gain_register x hlg_gain      = 64.06%
 *    - offset_register_hlg = offset_register + hlg_offset  = 0%
 *      (Note: % values are given above base on m_ulGain = 255)
 */
static const uint32_t AGC_HDR_HLG_VIDEO_GAIN            = 82;  /* additional HLG gain adjustment   */
static const uint32_t AGC_HDR_HLG_CONSTANT_BLACK_LEVEL  = 0;   /* No additional HLG offset adjustment */

static const uint32_t AGC_SDR_FULL_RANGE_GAIN       = 128;

class CDisplayDevice;
class CDisplaySource;
class COutput;
class CDisplayQueue;
class CDisplayNode;
class CDisplayInfo;
class CControlNode;
class CListenerNode;



/*
 * Control operation return values. Note we are not using errno
 * values internally, in case the mapping needs to change.
 */
enum DisplayPlaneResults
{
    STM_PLANE_OK,
    STM_PLANE_INVALID_VALUE,
    STM_PLANE_ALREADY_CONNECTED,
    STM_PLANE_CANNOT_CONNECT,
    STM_PLANE_NOT_SUPPORTED,
    STM_PLANE_NO_MEMORY
};

struct stm_display_plane_s
{
  CDisplayPlane *  handle;
  uint32_t         magic;
  CDisplayDevice * pDev;    // Parent device
};

struct InputFrameInfo {
  stm_rect_t                SrcVisibleArea;
  stm_rational_t            SrcPixelAspectRatio;
  uint32_t                  SrcFlags;
  stm_display_3d_format_t   Src3DFormat;
};

#define STM_PLANE_TRANSPARENCY_OPAQUE 255

enum ScalingResults
{
    SCALING_NOT_POSSIBLE           = 0,
    SCALING_WITH_PRIMARY_PICTURE   = 1,
    SCALING_WITH_SECONDARY_PICTURE = 2
};

struct PlaneStatistics {
    uint32_t PicDisplayed;
    uint32_t PicRepeated;
    uint32_t CurDispPicPTS;                 /* current displaying picture presentation time come with the stream itself */
    uint32_t SourceId;                      /* Id of the source connected to this plane */
    uint32_t FieldInversions;               /* Incremented when an interlaced field is presented on the wrong output polarity */
    uint32_t UnexpectedFieldInversions;     /* Some field inversions are normal, for example in case of Frame Rate Conversion or
                                               slow motion. When STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY is set, no field inversion
                                               is expected to happen. If it happens, this stat will be incremented.
                                             */
    uint32_t IsScalingPossible;             /* type of ScalingResults, to indicate which picture is selected for being displayed */
    uint32_t DataRateUseCaseInMBperS;       /* data rate of current use case in MByte per seconds */
    uint32_t ColorimetryConversion;         /* current colorimetry conversion */
};

struct stm_plane_rescale_caps_t
{
  stm_rational_t minHorizontalRescale; /*!< Minimum horizontal rescale factor */
  stm_rational_t maxHorizontalRescale; /*!< Maximum horizontal rescale factor */
  stm_rational_t minVerticalRescale;   /*!< Minimum vertical rescale factor   */
  stm_rational_t maxVerticalRescale;   /*!< Maximum vertical rescale factor   */
};

typedef enum
{
  /*source to display*/
    TOP_TO_TOP,         // 0
    TOP_TO_BOTTOM,      // 1
    TOP_TO_FRAME,       // 2

    BOTTOM_TO_TOP,      // 3
    BOTTOM_TO_BOTTOM,   // 4
    BOTTOM_TO_FRAME,    // 5

    FRAME_TO_TOP,       // 6
    FRAME_TO_BOTTOM,    // 7
    FRAME_TO_FRAME,     // 8

    MAX_USE_CASES
} stm_display_use_cases_t;


typedef struct display_info_s
{
    bool                    isDisplayInterlaced;
    stm_display_mode_t      currentMode;
    stm_rect_t              displayVisibleArea;
    uint32_t                display3DModeFlags;

    stm_rational_t          aspectRatio;
    stm_rational_t          pixelAspectRatio;
    stm_time64_t            outputVSyncDurationInUs;
    bool                    isOutputStarted;
    uint32_t                pixel_clock_freq;
    stm_ycbcr_colorspace_t  outputColorSpace;
    stm_ycbcr_colorspace_t  prevOutputColorSpace;
    stm_hdr_format_t        outputHDRFormat;
} output_info_t;

typedef enum
{
  STM_PLANE_OUTPUT_STOPPED   = (1L<<0),   /*!< Master Output is stopped */
  STM_PLANE_OUTPUT_UPDATED   = (1L<<1),   /*!< Master Output is updated */
  STM_PLANE_OUTPUT_STARTED   = (1L<<2),   /*!< Master Output is started */
} stm_plane_update_reason_t;

typedef struct display_triplet_s
{
    CDisplayNode      *pPrevNode;
    CDisplayNode      *pCurNode;
    CDisplayNode      *pNextNode;
} display_triplet_t;

typedef enum
{
    STM_PLANE_HW_STOPPED,               // Plane's HW is stopped but plane's clocks still enabled
    STM_PLANE_HW_AND_CLOCK_STOPPED,     // Plane's HW is stopped and plane's clocks are gated
    STM_PLANE_HW_MASKED,                // Plane's HW and plane's clocks are active but plane is hidden
    STM_PLANE_HW_ENABLED                // Plane's HW and plane's clocks are active
} stm_plane_state_t;

/* Public trace macros: exposed here so that derived classes can use them */
/* These macros add the plane name at the beginning of all traces         */
#define PLANE_TRC(id,fmt,args...)       TRC   (id, "%s - " fmt, GetName(), ##args)
#define PLANE_TRCIN(id,fmt,args...)     TRCIN (id, "%s - " fmt, GetName(), ##args)
#define PLANE_TRCOUT(id,fmt,args...)    TRCOUT(id, "%s - " fmt, GetName(), ##args)
#define PLANE_TRCBL(id)                 TRCBL (id, "%s", GetName())

class CDisplayPlane
{
public:
    // Construction/Destruction
    CDisplayPlane(const char           *name,
                  uint32_t              planeID,
                  const CDisplayDevice *pDev,
                  const stm_plane_capabilities_t Caps,
                  const char *pixClockName,
                  const char *procClockName);

    virtual ~CDisplayPlane(void);
    virtual bool Create(void);

    // Plane Identification
    const char           *GetName(void)         const { return m_name; }
    uint32_t              GetID(void)           const { return m_planeID; }
    const CDisplayDevice *GetParentDevice(void) const { return m_pDisplayDevice; }
    uint32_t              GetTimingID(void)     const { return m_ulTimingID; }
    output_info_t         GetOutputInfo(void)   const { return m_outputInfo; }
    stm_time64_t          GetPlaneLatency(void) const { return m_planeLatency; }

    // Plane Behaviour
    stm_plane_capabilities_t GetCapabilities(void) const { return m_capabilities; }
    int                      GetFormats(const stm_pixel_format_t** pData) const;

    int                      GetListOfFeatures( stm_plane_feature_t *list, uint32_t number);
    virtual bool             IsFeatureApplicable( stm_plane_feature_t feature, bool *applicable) const;

    virtual DisplayPlaneResults SetControl(stm_display_plane_control_t ctrl, uint32_t newVal);
    virtual DisplayPlaneResults GetControl(stm_display_plane_control_t ctrl, uint32_t *currentVal) const;
    virtual bool GetControlRange(stm_display_plane_control_t selector,
                                 stm_display_plane_control_range_t *range);
    virtual bool GetControlMode(stm_display_control_mode_t *mode);
    virtual bool SetControlMode(stm_display_control_mode_t mode);
    virtual bool ApplySyncControls();

    virtual DisplayPlaneResults SetCompoundControl(stm_display_plane_control_t  ctrl, void * newVal);
    virtual DisplayPlaneResults GetCompoundControl(stm_display_plane_control_t  ctrl, void * currentVal);
    virtual bool GetCompoundControlRange(stm_display_plane_control_t selector,
                                         stm_compound_control_range_t *range);

    // Manage delayed control notification
    DisplayPlaneResults RegisterAsynchCtrlListener(const stm_ctrl_listener_t *listener, int *listener_id);
    DisplayPlaneResults UnregisterAsynchCtrlListener(int listener_id);


    virtual bool GetTuningDataRevision(stm_display_plane_tuning_data_control_t ctrl, uint32_t * revision);
    virtual DisplayPlaneResults GetTuningDataControl(stm_display_plane_tuning_data_control_t ctrl, stm_tuning_data_t * currentVal);
    virtual DisplayPlaneResults SetTuningDataControl(stm_display_plane_tuning_data_control_t ctrl, stm_tuning_data_t * newVal);

    virtual DisplayPlaneResults ConnectToOutput(COutput* pOutput);
    virtual void DisconnectFromOutput(COutput* pOutput);
    virtual int  GetConnectedOutputID(uint32_t *id, uint32_t max_ids);

    virtual bool GetConnectedSourceID(uint32_t *id);
    virtual DisplayPlaneResults ConnectToSource(CDisplaySource *pSource);
    virtual bool DisconnectFromSource(CDisplaySource *pSource);
    virtual int  GetAvailableSourceID(uint32_t *id, uint32_t max_ids);
    virtual DisplayPlaneResults ConnectToSourceProtected(CDisplaySource *pSource);
    virtual bool                DisconnectFromSourceProtected(CDisplaySource *pSource);

    virtual bool Pause(void);
    virtual bool UnPause(void);
    virtual bool PauseCreateWorkQueue();
    void HandlePause(CDisplayNode       **pPrevNode,
                                             CDisplayNode       **pCurrNode,
                                             CDisplayNode       **pNextNode);
    bool PauseBufferCopyStart(CDisplayNode* pSrcNode, CDisplayNode ** ppDestNode, struct PauseCopyBufParams * pPauseCopyBufParams);
    void         PauseBufferCopyStop();
    virtual bool HideRequestedByTheApplication(void);
    virtual bool ShowRequestedByTheApplication(void);
    bool         isDisplayPossible(CDisplayNode *pCurrNode);
    void         UpdatePlaneState(void);
    bool         SetDepth(COutput *pOutput, int32_t depth, bool activate);
    bool         GetDepth(COutput *pOutput, int *depth) const;
    virtual TuningResults SetTuning(uint16_t service,
                                    void *inputList,
                                    uint32_t inputListSize,
                                    void *outputList,
                                    uint32_t outputListSize);

    // Member Accessors
    bool        isEnabled(void)                 const { return (m_currentState == STM_PLANE_HW_ENABLED); }
    bool        isPaused(void)                  const { return (m_PauseState!=PAUSESTATE_DISABLED); }
    bool        isVideoPlane(void)              const { return (m_capabilities & PLANE_CAPS_VIDEO); }
    bool        isVbiPlane(void)                const { return (m_capabilities & PLANE_CAPS_VBI); }
    uint32_t    GetStatus(void)                 const { return m_status; }
    bool        hasADeinterlacer(void)          const { return m_hasADeinterlacer; }

    /*
     * Helper method to setup colour keying, note it is static and public
     * so it can be used directly by the video plug class as well as the
     * plane implementations.
     */
    static bool GetRGBYCbCrKey(uint8_t& ucRCr,
                               uint8_t& ucGY,
                               uint8_t& ucBCb,
                               uint32_t ulPixel,
                    stm_pixel_format_t colorFmt,
                               bool     pixelIsRGB);

    virtual bool UpdateFromMixer(COutput* pOutput, stm_plane_update_reason_t update_reason);

    void UpdateOutputHDRInfo (void);

    virtual void OutputVSyncThreadedIrqUpdateHW(bool isDisplayInterlaced, bool isTopFieldOnDisplay, const stm_time64_t &vsyncTime);

    // pCurrNode is the node to display at next VSync.
    // pPrevNode and pNextNode are optional and can be set in case of Deinterlacer HW
    virtual void PresentDisplayNode             (CDisplayNode *pPrevNode,
                                                 CDisplayNode *pCurrNode,
                                                 CDisplayNode *pNextNode,
                                                 bool isPictureRepeated,
                                                 bool isDisplayInterlaced,
                                                 bool isTopFieldOnDisplay,
                                                 const stm_time64_t &vsyncTime) = 0;

    /*
     * Power Managment stuff
     */
    virtual void Freeze(void);
    virtual void Suspend(void);
    virtual void Resume(void);

    virtual bool RegisterStatistics(void);

    void           SetMasterVideoPlane(bool IsMaster) { m_bIsMasterVideoPlane = IsMaster; }

protected:
    bool           EnablePlane(void);               // Perform all the actions to enable this plane
    virtual void   DisablePlane(void);              // Perform all the actions to disable this plane
    virtual void   MaskPlane(void);                 // Perform all the actions to disable this plane

    virtual void   DisableHW(void);                 // Send a command specific to this plane HW to stop it

    virtual bool   DisableClocks(void);                 // Stop the clocks pacing this plane
    virtual bool   DisablePixelClock(void);             // Stop Pixel clock
    virtual bool   DisableProcClock(void);              // Stop Processing clock

    virtual bool   EnableClocks(void);                  // Start the clocks pacing this plane
    virtual bool   EnablePixelClock(void);              // Start Pixel clock
    virtual bool   EnableProcClock(void);               // Start Processing clock

    virtual bool   SetPixelClock(void);                 // Set Pixel Clock rate for the plane (if harware support it)

    virtual bool   PrepareIOWindows                     (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo);
    bool           SetSrcFrameRect                      (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect) const;
    bool           SetDstFrameRect                      (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &dstFrameRect) const;
    bool           IsSrcRectFullyOutOfBounds            (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect) const;
    bool           IsDstRectFullyOutOfBounds            (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &dstFrameRect) const;
    bool           TruncateIOWindowsToLimits            (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect, stm_rect_t &dstFrameRect) const;
    bool           AdjustIOWindowsForAspectRatioConv    (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect, stm_rect_t &dstFrameRect) const;
    virtual bool   AdjustIOWindowsForHWConstraints      (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo) const;
    virtual bool   IsScalingPossible                    (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo);
    virtual void   FillSelectedPictureDisplayInfo       (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo);
    void           FillSrcFrameRect                     (const CDisplayInfo *pDisplayInfo, stm_rect_t& srcFrameRect) const;
    void           FillSelectedPictureFormatInfo        (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo);
    uint32_t       GetHorizontalDecimationFactor        (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo);
    uint32_t       GetVerticalDecimationFactor          (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo);

    void            ReleaseDisplayNode                  (CDisplayNode *pNodeToRelease, const stm_time64_t &vsyncTime);

    bool            FillDisplayInfo                     (CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo);
    bool            FillNodeInfo                        (CDisplayNode *pNode);

    void            ResetDisplayInfo                    (void);
    void            UpdateOuptutInfo                    (void);
    void            UpdateOutputInfoSavedInSource       (void);
    void            SendOutputModeChangedEvent          (void);
    void            SendConnectionsChangedEvent         (void);

    stm_display_use_cases_t GetCurrentUseCase           (BufferNodeType srcType, bool isDisplayInterlaced, bool isTopFieldOnDisplay);
    virtual void    ResetEveryUseCases                  (void) { return; };
    virtual bool    IsContextChanged                    (CDisplayNode *pNodeToBeDisplayed, bool isPictureRepeated);
    void            AreSrcCharacteristicsChanged        (CDisplayNode *pNode, bool isPictureRepeated);
    CDisplayNode *  SelectPictureForNextVSync           (const stm_time64_t &vsyncTime);

    void            UpdatePictureDisplayedStatistics    (CDisplayNode       *pCurrNode,
                                                         bool                isPictureRepeated,
                                                         bool                isDisplayInterlaced,
                                                         bool                isTopFieldOnDisplay);

    void            ClearColorimetryConversionStatistics();
    void            UpdateColorimetryConversionStatistics(const CDisplayNode* pCurrNode);

    void            ResetTriplet                        (display_triplet_t *pTriplet);
    virtual void    ProcessLastVsyncStatus              (const stm_time64_t &vsyncTime, CDisplayNode *pNodeDisplayed) = 0;

    /*
     * Called during Flush or Pause with flush to give subclasses a chance to
     * reset any queuing state kept between calls to QueueBuffer.
     */
    virtual void ResetQueueBufferState(void);

    // Useful helpers for setting up hardware
    inline int32_t ValToFixedPoint(int32_t val, unsigned multiple = 1) const;
    inline int FixedPointToInteger(int32_t fp_val, int *frac=0) const;
    inline int LimitSizeToRegion(int start_coord, int invalid_coord, int size) const;

    // Helper to set scaling limits in the capability structure
    void SetScalingCapabilities(stm_plane_rescale_caps_t *caps) const;

    // Reset m_ComputedInputWindowValue and m_ComputedOutputWindowValue to their invalid value
    void ResetComputedInputOutputWindowValues(void);

    // Manage delayed controls
    DisplayPlaneResults AddSimpleControlToQueue(stm_display_plane_control_t ctrl, uint32_t newVal);
    DisplayPlaneResults AddCompoundControlToQueue(stm_display_plane_control_t ctrl, void * newVal);
    DisplayPlaneResults ProcessControlQueue(const stm_time64_t &vsyncTime);
    int ConvertDisplayPlaneResult(const DisplayPlaneResults result);

    // Manage Color Key
    void FillColorKeyConfig (stm_color_key_config_t       * const dst,
                             const stm_color_key_config_t * const src) const;

    const char     * m_name;           // Human readable name of this object
    CDisplayDevice * m_pDisplayDevice; // Parent Device
    uint32_t         m_numConnectedOutputs;
    COutput        * m_pOutput;        // Currently only one, need to support attachment to multiple mixer outputs.
    CDisplaySource * m_pSource;        // Source attached to this plane.

    uint32_t         m_planeID;        // HW Plane identifier
    uint32_t         m_ulTimingID;     // The timing ID of m_pOutput when connected

    // Information about the clocks associated to this plane
    char             m_pixelClockName[VIBE_OS_CLK_MAX_NAME_LENGTH];
    char             m_procClockName[VIBE_OS_CLK_MAX_NAME_LENGTH];

    struct vibe_clk  m_pixelClock;         // Pixel clock associated to this plane
    struct vibe_clk  m_procClock;          // Processing clock associated to this plane

    /*** Variables read by the VSync Threaded IRQ and protected by m_vsyncLock ***/
    // To know if the context changed. In that case, all the parameters already computed based on previous pictures
    // must be re-computed to display this new picture.
    bool           m_ContextChanged;

    // Plane aspect ratio conversion mode
    stm_plane_aspect_ratio_conversion_mode_t m_AspectRatioConversionMode;

    // Information about the current display mode and display aspect ratio
    output_info_t       m_outputInfo;

    stm_plane_mode_t    m_InputWindowMode;
    stm_plane_mode_t    m_OutputWindowMode;
    stm_rect_t          m_InputWindowValue;
    stm_rect_t          m_OutputWindowValue;

    display_triplet_t   m_picturesPreparedForNextVSync;
    display_triplet_t   m_picturesUsedByHw;

    // Transparency 0 (transparent) .. 255 (opaque)
    uint32_t            m_Transparency;
    stm_plane_control_state_t m_TransparencyState; /* CONTROL_ON = m_Transparency has precedence over buffer */

    // Color Key support over STKPI
    stm_color_key_config_t      m_ColorKeyConfig;
    stm_plane_control_state_t   m_ColorKeyState; /* CONTROL_ON = m_ColorKeyConfig has precedence over buffer */

    // Queued controls
    CDisplayQueue       m_ControlQueue;

    bool                m_hideRequestedByTheApplication; // Is the application requesting to hide this plane? We can save this request whatever the current plane status

    int32_t             m_lastRequestedDepth;       // Last requested depth (will be applied at next VSync)
    bool                m_updateDepth;              // Request to update plane's depth at next VSync

    bool                m_updateHDROutFormat;       // Request to update plane's HDROUT Format at next VSync

    uint32_t            m_ulPrevSinkHDRFormats;     // member to memorise previous SinkHDRFormat Support
    stm_hdr_format_t    m_PrevNodeHdr_Fmt;          // member to memorise previous node HDR format

    // Listener parameters
    CDisplayQueue       m_ListenerQueue;

    stm_plane_state_t   m_currentState;             // Small state machine used to control the activation/deactivation of plane's HW and Clocks
    uint32_t            m_hwDeactivationCountDown;  // Counter used to disable the plane's hardware a certain number of VSync after the HW masking
    uint32_t            m_clkDeactivationCountDown; // Counter used to stop the plane's clocks a certain number of VSync after the HW deactivation
    bool                m_isProcClkEnabled;
    bool                m_isPixelClkEnabled;
    bool                m_areClocksAlwaysEnabled;  // Pixel and Processing clocks are always enabled

    /**************************************************************************************/


    bool           m_hasADeinterlacer;      // Does this plane has a HW deinterlacer?
    volatile uint32_t m_status;             // Public view of the current plane status

    stm_time64_t   m_planeLatency;   // Latency of the display pipe. The timer is started when the first pixel is read
                                     //  in memory and stopped when the same pixel starts to be output by the plane

    stm_plane_ctrl_hide_mode_policy_t  m_eHideMode;     // Hiding mode requested by user throught specifig control

    int32_t       m_fixedpointONE;   // Value of 1 in the n.m fixed point format
                                     // used for hardware rescaling.

    /*
     * In general the plane pipeline (or hardware) does have a dedicated
     * pixel clock that the DE driver should set according to the
     * current video timing configuration. It is also the case for the
     * processing clock except that the rate for this one is set by the
     * TargetPack at initialization time (driver shouldn't set the rate for
     * processing clock).
     *
     * The below drawing gives an idea about how the hardware plane is
     * clocked and which are clocks and kind of clocks needed by the plane
     * (apply for video and graphical planes).
     *
     *  |--------------------------|    |------------|    |------|
     *  |         |        |       |    |            |    |      |
     *  |         |Plane HW|       |    |    MIXER   |    |      |
     *  |clk_stbus|        |clk_pix|--->|            |--->|Output|
     *  |         |--------|       |    |------------|    |      |
     *  |         |clk_proc|       |    |clk_mix_proc|    |      |
     *  |--------------------------|    |------------|    |------|
     *
     * * clk_stbus      <-----+ CLK_ICN_REG
     * * clk_proc       <-----+ CLK_COMPO_DVP / CLK_MAIN_DISP / CLK_AUX_DISP
     * * clk_pix        <-----+ CLK_PIX_MAIN_DISP / CLK_PIX_AUX_DISP / CLK_PIX_GDPX
     * * clk_mix_proc   <-----+ CLK_COMPO_DVP
     *
     * With the new SoC generation the above clock dependency was simplified
     * and ALL hardware planes are now using SAME pixel clock (communicating
     * with the same pixel clock with Mixer block : 'CLK_PROC_MIXER' clock).
     *
     * This means that for we do have hardware planes that doesn't own
     * a dedicated pixel clock; only two pixel clocks are available (per Mixer) :
     * 'CLK_PIX_MAIN_DISP' and 'CLK_PIX_AUX_DISP' clocks. The clock rate still
     * depend on the video timing but now this should be fine tuned by the
     * Output entity and no more by planes (planes have a fixed pixel clock).
     *
     * Video planes does have their own processing clock ('CLK_MAIN_DISP' /
     * 'CLK_AUX_DISP'). Same for ALL graphical planes (GDP and GDP+) which
     * are sharing the same processing clock used also by DVP hardware
     * ('CLK_COMPO_DVP' clock).
     *
     * If the hardware plane do have a dedicated Pixel Clock then the
     * driver should setup the clock rate according to the current display
     * mode configured in the output. Otherwise driver should skip setting
     * the pixel clock.
     */
    bool           m_hasADedicatedPixelClock;

    // Min and Max sample rate converter steps for image rescaling
    uint32_t       m_ulMaxHSrcInc;
    uint32_t       m_ulMinHSrcInc;
    uint32_t       m_ulMaxVSrcInc;
    uint32_t       m_ulMinVSrcInc;

    // If the plane has a vertical and horizontal scaler we can be supporting
    // automatic IO Windows.
    bool           m_hasAScaler;

    // If the plane has an ignore on mixer capabilitie.
    bool           m_hasIgnoreOnMixer;

    // Bitmask for mixer changing from soc to another
    uint32_t       m_ForceOnMixerMask;

    stm_plane_rescale_caps_t m_rescale_caps;

    // Display plane surface format capabilities
    const stm_pixel_format_t*  m_pSurfaceFormats;
    uint32_t                   m_nFormats;
    stm_plane_capabilities_t   m_capabilities;
    uint32_t                   m_nFeatures;
    const stm_plane_feature_t* m_pFeatures;

    // for GDPs, the code does not deal with this one being overridden
    // by children! (and it makes no sense for GDPs, at least at the moment)
    uint32_t           m_ulMaxLineStep;

    // In AUTO mode, save input and output windows value to return to user
    // (taking into account hardware limits and aspect ratio conversion)
    stm_rect_t         m_ComputedInputWindowValue;
    stm_rect_t         m_ComputedOutputWindowValue;

    // Power Managment state
    bool               m_bIsSuspended; // Has the HW been powered off
    bool               m_bIsFrozen;

    /* Plane statistics */
    struct PlaneStatistics m_Statistics;

    /*
     * Number of Vsync ticks that driver should waiting for before
     * disabling hardware.
     */
    uint32_t           m_HwDeactivationVsyncCount;

    /*
     * Number of Vsync ticks that driver should waiting for before
     * stopping clocks.
     */
    uint32_t           m_ClockDeactivationVsyncCount;

    // Listener parameters
    stm_asynch_ctrl_status_t m_ControlStatusArray[CONTROL_STATUS_ARRAY_MAX_NB];

    // Pause management
    enum PauseState {
      PAUSESTATE_DISABLED,  /* no pause ongoing */
      PAUSESTATE_PAUSE_REQUESTED,     /* allocate and copy buffer at next vsync */
      PAUSESTATE_ENABLED,   /* displaying copied buffer */
      PAUSESTATE_UNPAUSE_REQUESTED, /* do the actual disable at next vsync */
      PAUSESTATE_DISABLING, /* deallocate copied buffer */
    } m_PauseState;
    VIBE_OS_WorkQueue_t m_PauseWorkQueue;
    CDisplayNode * m_pPauseNode;
    struct PauseCopyBufParams * m_pPauseCopyBufParams;
    void * m_pPauseCopyThreadDesc; /* pause copy thread descriptor */
    bool m_bPauseDisplayed; /* pause node was already displayed once */
    int m_PauseMissingNodeCount;

    // Use to detect source characterics changed for forcing context update
    stm_display_buffer_t     m_prevBufferDesc;

    bool                     m_bIsMasterVideoPlane;     // Master Plane for Automatic HDR format selection
};

inline int32_t CDisplayPlane::ValToFixedPoint(int32_t val, unsigned multiple) const
{
    /*
     * Convert integer val to n.m fixed point defined by the value of
     * m_fixedpointONE. The result is then divided by multiple, allowing the
     * direct conversion of for example N 16ths to a fixed point number
     * without the calling worrying about 32bit overflows. This is used
     * in resize filter setup calculations.
     *
     * Note that we have to take care with negative values as we only
     * have an unsigned 64bit divide available on Linux through the OS
     * abstraction.
     */
    int32_t tmp = 1;
    if(val < 0)
    {
      tmp = -1;
      val = -val;
    }
    tmp *= (int32_t)vibe_os_div64(((uint64_t)val * m_fixedpointONE), multiple);
    return tmp;
}


inline int CDisplayPlane::FixedPointToInteger(int32_t fp_val, int *frac) const
{
    int integer = fp_val / m_fixedpointONE;
    if(frac)
        *frac = fp_val - (integer * m_fixedpointONE);

    return integer;
}


inline int CDisplayPlane::LimitSizeToRegion(int start_val, int max_val, int size) const
{
    if ((start_val + size) > max_val)
    {
        size = max_val - start_val + 1;
        if(size<0)
            size = 0;

        PLANE_TRC( TRC_ID_UNCLASSIFIED, "adjusting size to %d", size );
    }

    return size;
}

#endif
