/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2000-2014 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2014-05-30
***************************************************************************/

#ifndef _OUTPUT_H
#define _OUTPUT_H

#include "stm_display_output.h"
#include "stm_display_plane.h"
#include "DisplayDevice.h"
#include "MetaDataQueue.h"

#define VALID_OUTPUT_HANDLE 0x01240124

class CDisplayDevice;
class CDisplayPlane;
class CDisplayVTG;

/*
 * Control operation return values. Note we are not using errno
 * values internally, in case the mapping needs to change.
 */
enum OutputResults
{
  STM_OUT_OK,
  STM_OUT_NO_CTRL,
  STM_OUT_INVALID_VALUE,
  STM_OUT_BUSY,
  STM_OUT_NOT_SUPPORTED
};

struct stm_display_output_s
{
  COutput         * handle;
  uint32_t          magic;
  CDisplayDevice  * pDev;    // Parent device
};


/* Public trace macros: exposed here so that derived classes can use them */
/* These macros add the output name at the beginning of all traces        */
#define OUTPUT_TRC(id,fmt,args...)       TRC   (id, "%s - " fmt, GetName(), ##args)
#define OUTPUT_TRCIN(id,fmt,args...)     TRCIN (id, "%s - " fmt, GetName(), ##args)
#define OUTPUT_TRCOUT(id,fmt,args...)    TRCOUT(id, "%s - " fmt, GetName(), ##args)
#define OUTPUT_TRCBL(id)                 TRCBL (id, "%s", GetName())


class COutput
{
public:
  // Construction/Destruction
  COutput(const char     *name,
          uint32_t        id,
          CDisplayDevice* pDev,
          uint32_t        timingID);

  virtual ~COutput(void);

  // Hooks for Device creation and destruction.
  virtual bool Create(void);
  virtual void CleanUp(void);

  // Output Identification
  const char            *GetName(void)         const { return m_name; }
  uint32_t               GetID(void)           const { return m_ID; }
  const CDisplayDevice  *GetParentDevice(void) const { return m_pDisplayDevice; }
  uint32_t               GetTimingID()         const { return m_ulTimingID; }


  // Output Behaviour
  uint32_t GetCapabilities(void) const { return m_ulCapabilities; }

  void RemoveCapabilities(uint32_t caps) { m_ulCapabilities &= ~caps; }

  virtual const stm_display_mode_t* SupportedMode(const stm_display_mode_t *) const;

  virtual OutputResults Start(const stm_display_mode_t*);
  virtual bool Stop(void);
  virtual void Suspend(void);
  virtual void Resume(void);
  virtual void PowerDownDACs(void) { m_bDacPowerDown = true; }

  virtual bool HandleInterrupts(void) = 0;

  virtual void UpdateHW();
  virtual TuningResults SetTuning( uint16_t service,
                                   void *inputList,
                                   uint32_t inputListSize,
                                   void *outputList,
                                   uint32_t outputListSize);


  virtual bool CanShowPlane(const CDisplayPlane *);
  virtual bool ShowPlane   (const CDisplayPlane *);
  virtual void HidePlane   (const CDisplayPlane *);

  virtual bool SetPlaneDepth(const CDisplayPlane *, int depth, bool activate);
  virtual bool GetPlaneDepth(const CDisplayPlane *, int *depth) const;

  virtual bool SetMixerMap(const CDisplayPlane * plane, uint32_t mixerIDs) { return true; };

  virtual uint32_t SetControl(stm_output_control_t, uint32_t newVal) = 0;
  virtual uint32_t GetControl(stm_output_control_t, uint32_t *val) const;
  virtual uint32_t SetCompoundControl(stm_output_control_t, void *newVal);
  virtual uint32_t GetCompoundControl(stm_output_control_t, void *val) const;

  virtual stm_display_metadata_result_t QueueMetadata(stm_display_metadata_t *);
  virtual void FlushMetadata(stm_display_metadata_type_t);

  virtual void SetClockReference(stm_clock_ref_frequency_t, int error_ppm);

  virtual void SoftReset(void);

  virtual bool SetVTGSyncs(const stm_display_mode_t *mode) = 0;

  virtual const stm_display_mode_t* GetModeParamsLine(stm_display_mode_id_t mode) const;
  virtual const stm_display_mode_t* FindMode(uint32_t uXRes, uint32_t uYRes, uint32_t uMinLines, uint32_t uMinPixels, uint32_t uPixClock, stm_scan_type_t) const;

  virtual const char* GetPixelClockName() { return NULL; }

  // Retreive the current output color space (BT601, BT709 or BT2020)
  virtual void GetOutputColorspace(stm_ycbcr_colorspace_t &) const {}

  // Retreive the current output HDR format
  virtual bool GetOutputHDRFormat(stm_hdr_format_t &) const { return false; }
  // Update the current output HDR format
  virtual bool SetOutputHDRFormat(stm_hdr_format_t *) { return false; }
  // Check For Automatic HDR Mode
  void UpdateOutputHDRMode(stm_output_hdr_mode_t newVal) { m_HDRMode = newVal; }
  // Check For Automatic HDR Mode
  bool IsHDRModeAuto(void) const { return (m_HDRMode == STM_OUTPUT_HDR_MODE_AUTO); }

  // Retrieve the current output supported Sink Formats (SDR, HDR, HLG or ST2086)
  virtual uint32_t GetSinkSupportedHDRFormats(void) const { return m_ulSinkHDRFormats; }
  // Update the current output supported Sink Formats (SDR, HDR, HLG or ST2086)
  virtual void SetSinkSupportedHDRFormats(uint32_t newVal);

  // Member Accessors
  bool                   IsStarted(void) { return m_bIsStarted; }
  const stm_display_mode_t* GetCurrentDisplayMode(void) const { return m_bIsStarted?&m_CurrentOutputMode:0; }
  stm_time64_t           GetFieldOrFrameDuration(void)  const { return m_fieldframeDuration; }
  static stm_time64_t    GetFieldOrFrameDurationFromMode(const stm_display_mode_t *);

  uint32_t               GetOutputFormat(void)          const { return m_ulOutputFormat;     }

  bool                   GetDisplayStatus(stm_display_output_connection_status_t *status) const;
  void                   SetDisplayStatus(stm_display_output_connection_status_t status);

  stm_display_signal_range_t GetSignalRange(void)       const { return m_signalRange; }

  // VTG Interrupt management for outputs that are timing masters.
  uint32_t     GetCurrentVTGEvent()     const { return m_LastVTGEvent;     }
  stm_time64_t GetCurrentVTGEventTime() const { return m_LastVTGEventTime; }

  /*
   * Management of outputs that are slaved to another output's timing generator
   */
  bool SetTimingID(uint32_t ulTimingID);
  bool RegisterSlavedOutput(COutput *);
  bool UnRegisterSlavedOutput(COutput *);
  bool SetSlavedOutputsVTGSyncs(const stm_display_mode_t *mode);

  uint64_t GetPixelClockToleranceInNs(void) const { return m_pixelClockToleranceInNs; }

protected:
  // Identification
  const char     * m_name;
  uint32_t         m_ID;
  CDisplayDevice * m_pDisplayDevice;
  uint32_t         m_ulTimingID;     // Unique ID of the VTG pacing this output
  uint32_t         m_ulCapabilities;
  void           * m_lock;

  // Control state
  uint32_t         m_ulMaxPixClock;

  stm_display_output_video_source_t m_VideoSource;
  stm_display_output_audio_source_t m_AudioSource;

  uint32_t         m_ulOutputFormat; // RGB, Y/C, CVBS
  bool             m_bForceColor;
  uint32_t         m_uForcedOutputColor; // 0xXXRRGGBB format

  bool             m_bBypassHwInit;
  bool             m_bDacPowerDown;
  bool             m_bIsSuspended;
  bool             m_bIsStarted;

  stm_display_mode_t m_CurrentOutputMode;

  stm_display_signal_range_t m_signalRange;

  // Active display mode state
  volatile stm_time64_t            m_fieldframeDuration; // frame rate in real time (us)
  volatile uint32_t                m_LastVTGEvent;
  volatile stm_time64_t            m_LastVTGEventTime;
  volatile stm_display_output_connection_status_t m_displayStatus;

  stm_rational_t                   m_DisplayAspectRatio;
  stm_hdr_format_t                 m_HDRFormat;
  stm_output_hdr_mode_t            m_HDRMode;

  /*
   * Management of outputs that are slaved to another output's timing generator
   */
  COutput **m_pSlavedOutputs;

  void StopSlavedOutputs(void);

  /*
   * Called on slaves to inform them of a fast mode change or a change in
   * 3D flags. Can be overridden to provide output specific functionality.
   */
  virtual void UpdateOutputMode(const stm_display_mode_t &) = 0;

  /* Output Pixel clock tolerances in Ns (+/-50 ppm) */
  uint64_t m_pixelClockToleranceInNs;

  /* Current output color space (BT601 / BT709 / BT2020) */
  stm_ycbcr_colorspace_t m_OutputColorSpace;

  /* Current HDR formats supported by connected Sink (SDR / HDR-10 / HDR-HLG / ST2086) */
  uint32_t  m_ulSinkHDRFormats;

  /* Output HW init */
  virtual void InitializeHardware(void) {};

private:

  COutput(const COutput&);
  COutput& operator=(const COutput&);
};


/*
 * Mode structure comparisons
 */

inline bool IsModeTimingValid(const stm_display_mode_t &m1)
{
  if(m1.mode_timing.pixels_per_line       > 0
  && m1.mode_timing.lines_per_frame       > 0
  && m1.mode_timing.pixel_clock_freq      > 0
  && m1.mode_timing.hsync_width           > 0
  && m1.mode_timing.vsync_width           > 0
  && m1.mode_params.vertical_refresh_rate > 0
  && m1.mode_params.active_area_width > 0
  && m1.mode_params.active_area_height > 0
  && m1.mode_params.active_area_start_line > 0)
    return true;

  return false;
}


inline bool AreModeTimingsIdentical(const stm_display_mode_t &m1, const stm_display_mode_t &m2)
{
  if(m1.mode_timing.pixels_per_line       == m2.mode_timing.pixels_per_line
  && m1.mode_timing.lines_per_frame       == m2.mode_timing.lines_per_frame
  && m1.mode_timing.pixel_clock_freq      == m2.mode_timing.pixel_clock_freq
  && m1.mode_timing.hsync_polarity        == m2.mode_timing.hsync_polarity
  && m1.mode_timing.hsync_width           == m2.mode_timing.hsync_width
  && m1.mode_timing.vsync_polarity        == m2.mode_timing.vsync_polarity
  && m1.mode_timing.vsync_width           == m2.mode_timing.vsync_width
  && m1.mode_params.vertical_refresh_rate == m2.mode_params.vertical_refresh_rate
  && m1.mode_params.scan_type             == m2.mode_params.scan_type)
    return true;

  return false;
}


inline bool AreModeTimingsCompatible(const stm_display_mode_t &m1, const stm_display_mode_t &m2)
{
  if(m1.mode_timing.pixels_per_line  == m2.mode_timing.pixels_per_line
  && m1.mode_timing.lines_per_frame  == m2.mode_timing.lines_per_frame
  && m1.mode_timing.hsync_polarity   == m2.mode_timing.hsync_polarity
  && m1.mode_timing.hsync_width      == m2.mode_timing.hsync_width
  && m1.mode_timing.vsync_polarity   == m2.mode_timing.vsync_polarity
  && m1.mode_timing.vsync_width      == m2.mode_timing.vsync_width
  && m1.mode_params.scan_type        == m2.mode_params.scan_type)
    return true;

  return false;
}


inline bool AreModeActiveAreasIdentical(const stm_display_mode_t &m1, const stm_display_mode_t &m2)
{
  if(m1.mode_params.active_area_start_line  == m2.mode_params.active_area_start_line
  && m1.mode_params.active_area_start_pixel == m2.mode_params.active_area_start_pixel
  && m1.mode_params.active_area_height      == m2.mode_params.active_area_height
  && m1.mode_params.active_area_width       == m2.mode_params.active_area_width)
    return true;

  return false;
}


inline bool AreModesIdentical(const stm_display_mode_t &m1, const stm_display_mode_t &m2)
{
  return AreModeTimingsIdentical(m1,m2) && AreModeActiveAreasIdentical(m1,m2);
}


inline bool AreModesCompatible(const stm_display_mode_t &m1, const stm_display_mode_t &m2)
{
  return AreModeTimingsCompatible(m1,m2) && AreModeActiveAreasIdentical(m1,m2);
}

#endif /* _OUTPUT_H */
