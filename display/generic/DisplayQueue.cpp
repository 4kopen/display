/***********************************************************************
 *
 * File: display/generic/DisplayQueue.cpp
 * Copyright (c) 2013 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <vibe_debug.h>
#include <vibe_os.h>

#include "DisplayQueue.h"


CDisplayQueue::CDisplayQueue(void)
{
    m_pNodeHead             = 0;
    m_queueLock             = 0;
    m_queueName[0]          = '\0';
}

CDisplayQueue::~CDisplayQueue()
{
    // Check if the queue is empty
    if(m_pNodeHead)
    {
        TRC( TRC_ID_ERROR, "Queue %s is not empty!!!", m_queueName );
    }

    vibe_os_delete_mutex(m_queueLock);
    m_queueLock = 0;

    TRC(TRC_ID_MAIN_INFO, "Delete %s %p", m_queueName, this);
}

// create the queue
bool CDisplayQueue::CreateQueue(char *name)
{
    vibe_os_snprintf( m_queueName, sizeof( m_queueName ), "%s", name );

    /* Create mutex for protecting queue */
    m_queueLock = vibe_os_create_mutex();
    if (!m_queueLock)
    {
        TRC(TRC_ID_ERROR, "Queue %s cannot create its mutex", m_queueName);
        return false;
    }

    TRC(TRC_ID_MAIN_INFO, "Create %s %p", m_queueName, this);

    return true;
}

// lock the queue
bool CDisplayQueue::LockQueue(void)
{
    // Check if queue was created and valid
    if (m_queueLock == 0)
    {
        TRC(TRC_ID_ERROR, "Queue %s %p was never created or deleted", m_queueName, this);
        return false;
    }

    // take the mutex
    vibe_os_lock_mutex(m_queueLock);

    return true;
}

// unlock the queue
void CDisplayQueue::UnlockQueue(void)
{
    // release the mutex
    vibe_os_unlock_mutex(m_queueLock);
}


bool CDisplayQueue::QueueDisplayNode(CNode *pNode)
{
    CNode   *pTail;

    TRCIN( TRC_ID_UNCLASSIFIED, "" );

    // Check node validity
    if ((pNode==0) || (!pNode->IsNodeValid()) )
    {
        TRC( TRC_ID_ERROR, "Queue %s tried to queue an invalid node %p!!!", m_queueName, pNode);
        return false;
    }

    // Lock the queue
    if (LockQueue() == false)
    {
        return false;
    }

    // Queue it at the tail of the queue
    if (m_pNodeHead)
    {
        pTail = GetTail(m_pNodeHead);  // This cannot fail except if queued nodes were corrupted
        if (!pTail)
        {
            // The Queue is NOT empty but we failed to find the last node
            TRC( TRC_ID_ERROR, "Queue %s : Failed to find the tail!!!", m_queueName);

            // Unlock the queue
            UnlockQueue();

            return false;
        }
        pTail->SetNextNode(pNode);
    }
    else
    {
        // This is the first node in the queue
        m_pNodeHead = pNode;
    }

    // Unlock the queue
    UnlockQueue();

    return true;
}

// Release a node
// The queue is updated to remove the reference to this node.
bool CDisplayQueue::ReleaseDisplayNode(CNode *pNodeToRelease)
{
    bool      res   = false;
    CNode    *pNode;
    bool      nodeFound = false;

    TRCIN( TRC_ID_UNCLASSIFIED, "" );

    // Check node validity
    if ((pNodeToRelease==0) || (!pNodeToRelease->IsNodeValid()) )
    {
        TRC( TRC_ID_ERROR, "Queue %s tried to release an invalid node %p!!!", m_queueName, pNodeToRelease);
        return false;
    }

    // Lock the queue
    if (LockQueue() == false)
    {
        return false;
    }

    // First, check if the node to release is part of the current Queue
    // In that case, update the queue to remove references to it
    if (pNodeToRelease == m_pNodeHead)
    {
        // m_pNodeHead can become NULL if pNodeToRelease is the only node in the queue
        m_pNodeHead = pNodeToRelease->GetNextNode();

        pNodeToRelease->SetNextNode(0);
        nodeFound = true;
    }
    else
    {
        pNode = m_pNodeHead;
        while ( (pNode != 0) && (nodeFound == false) )
        {
            if (pNode->GetNextNode() == pNodeToRelease)
            {
                pNode->SetNextNode(pNodeToRelease->GetNextNode());
                nodeFound = true;
            }
            pNode = pNode->GetNextNode();
        }
    }

    // Unlock the queue
    UnlockQueue();

    // delete node if found in the queue
    if(nodeFound)
    {
        delete pNodeToRelease;
        pNodeToRelease = 0;
        res = true;
    }
    else
    {
        // The node was not found in the queue but this can happen if the queue was cut before this node (thanks to CutBeforeNode)
        // Delete the node if it is valid
        if(pNodeToRelease->IsNodeValid())
        {
            delete pNodeToRelease;
            pNodeToRelease = 0;
            res = true;
        }
    }

    return (res);
}

// Function returning the 1st node of the queue
CNode* CDisplayQueue::GetFirstNode(void)
{
    CNode   *pNode = 0;

    // Lock the queue
    if (LockQueue() == false)
    {
        return 0;
    }

    // Get first node
    pNode = m_pNodeHead;

    // Unlock the queue
    UnlockQueue();

    // Check node validity
    if ( pNode && !pNode->IsNodeValid() )
    {
        TRC( TRC_ID_ERROR, "Queue %s has an invalid node %p!!!", m_queueName, pNode);
    }

    return(pNode);
}

CNode *CDisplayQueue::GetLastNode(void)
{
    CNode   *pNode = 0;

    // Lock the queue
    if (LockQueue() == false)
    {
        return 0;
    }

    pNode = m_pNodeHead;

    // Check node validity
    if (pNode && !pNode->IsNodeValid())
    {
        TRC( TRC_ID_ERROR, "Queue %s has an invalid node %p!!!", m_queueName, pNode);
    }

    // Get last node
    if(pNode)
    {
        while (pNode->GetNextNode())
        {
                pNode = pNode->GetNextNode();
        }
    }

    // Unlock the queue
    UnlockQueue();

    return (pNode);
}

// Similar function as GetLastNode() excepted that it starts from a given node
// Need to be called with queue protection taken
CNode *CDisplayQueue::GetTail(CNode *pNode)
{
    // Check node validity
    if ( (pNode==0) || (!pNode->IsNodeValid()) )
    {
        TRC( TRC_ID_ERROR, "Queue %s has an invalid node %p!!!", m_queueName, pNode);
        return 0;
    }

    while (pNode->GetNextNode())
    {
        pNode = pNode->GetNextNode();
    }

    return (pNode);
}

// Cut the queue AFTER the indicated node
bool CDisplayQueue::CutAfterNode(CNode *pNode)
{
    // Check node validity
    if ( (pNode==0) || (!pNode->IsNodeValid()) )
    {
        TRC( TRC_ID_ERROR, "Queue %s has an invalid node %p!!!", m_queueName, pNode);
        return false;
    }

    // Lock the queue
    if (LockQueue() == false)
    {
        return false;
    }

    pNode->SetNextNode(0);

    // Unlock the queue
    UnlockQueue();

    return true;
}

// Cut the queue BEFORE the indicated node
bool CDisplayQueue::CutBeforeNode(CNode *pNode)
{
    CNode           *pCurrentNode;
    bool             nodeFound = false;

    // Check node validity
    if ( (pNode==0) || (!pNode->IsNodeValid()) )
    {
        TRC( TRC_ID_ERROR, "Queue %s has an invalid node %p!!!", m_queueName, pNode);
        return false;
    }

    // Lock the queue
    if (LockQueue() == false)
    {
        return false;
    }

    // Look for this node in the Queue
    // NB: The node can be present only one time in the Queue otherwise the linked list would loop forever
    if (pNode == m_pNodeHead)
    {
        m_pNodeHead = 0;
        nodeFound   = true;
    }
    else
    {
        pCurrentNode = m_pNodeHead;

        while ( (pCurrentNode != 0) && (nodeFound == false) )
        {
            if (pCurrentNode->GetNextNode() == pNode)
            {
                pCurrentNode->SetNextNode(0);
                nodeFound = true;
            }
            pCurrentNode = pCurrentNode->GetNextNode();
        }
    }

    // Unlock the queue
    UnlockQueue();

    if (!nodeFound)
    {
        TRC( TRC_ID_ERROR, "Queue %s tried to cut before a node not present in queue!", m_queueName );
    }

    return (nodeFound);
}


