/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2011-2014 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2014-05-30
***************************************************************************/

#ifndef _QUEUEBUFFERINTERFACE_H
#define _QUEUEBUFFERINTERFACE_H

#include "DisplayDevice.h"
#include "DisplayPlane.h"
#include "DisplaySource.h"
#include "SourceInterface.h"


enum QueueBufferInterfaceResults {
    QBI_OK,                     // Succeed
    QBI_ALREADY_LOCKED,         // Already locked by another requester
    QBI_FLUSH_ON_GOING,         // Flush on-going so operation rejected
    QBI_FLUSH_NOTHING_TO_DO,    // Flush operation already done
    QBI_INVALID_FLUSH,          // Invalid flush request so operation rejected
    QBI_NOT_LOCKED,             // Source is not locked
    QBI_NOT_LOCKED_BY_ASKER,    // This requester/asker hasn't the lock on this queue
    QBI_NODES_REMAINING,        // Nodes remaining in the display queue
    QBI_INVALID_QUEUED_BUFFER,  // Invalid queued buffer
    QBI_STILL_LOCKED,           // Still locked by current requester
    QBI_QUEUEING_BUFFER_FAILED, // Queueing buffer failed

    QBI_NO_MEMORY,              // Cannot allocate memory
    QBI_RESTARTSYS,             // wait queue interrupted by a signal, asking upper layer to re-call
};

/*
 * Type of flush requested on the queue buffer interface
 */
typedef enum {
    NO_FLUSH,
    FLUSH_ALL,
    FLUSH_PARTIAL,
    FLUSH_PARTIAL_WITH_COPY
} flush_requested_t;


class CQueueBufferInterface: public CSourceInterface
{
public:
    // Construction/Destruction
    CQueueBufferInterface ( uint32_t interfaceID, CDisplaySource  *pSource );

    virtual ~CQueueBufferInterface(void);
    bool     Create(void);

    virtual uint32_t                    GetTimingID(void) const = 0;

    virtual void                        UpdateOutputInfo(CDisplayPlane *pPlane) = 0;

    // Interface Queue Buffer Identification
    stm_display_source_interfaces_t     GetInterfaceType(void)      const { return STM_SOURCE_QUEUE_IFACE; }

    virtual QueueBufferInterfaceResults LockUse(void *user) = 0;
    virtual QueueBufferInterfaceResults Unlock (void *user) = 0;
    virtual QueueBufferInterfaceResults Release(void *user) = 0;

    virtual QueueBufferInterfaceResults QueueBuffer(const stm_display_buffer_t * const ,
                                                    const void                 * const ) = 0;

    virtual bool                        IsEmpty();

    virtual QueueBufferInterfaceResults FlushStart(flush_requested_t  &requestedFlush,
                                                   const void * const user) = 0;
    virtual QueueBufferInterfaceResults WaitFlushCompletion(flush_requested_t  requestedFlush,
                                                            const void * const user) = 0;

    virtual int                         GetBufferFormats(const stm_pixel_format_t **) const = 0;

    virtual bool                        SetListener(stm_display_source_queue_listener   listener,
                                                    void *                              user_data);
    virtual void                        SendOutputModeChangedEvent(void);
    virtual void                        SendConnectionsChangedEvent(void);

    virtual uint32_t                    GetControl(stm_display_source_ctrl_t ctrl, uint32_t *ctrl_val) const;
    virtual uint32_t                    SetControl(stm_display_source_ctrl_t ctrl, uint32_t ctrl_val);

    virtual bool                        RegisterStatistics(void);

    virtual TuningResults               SetTuning( uint16_t service, void *inputList, uint32_t inputListSize, void *outputList, uint32_t outputListSize);

protected:
    const CDisplayDevice               *m_pDisplayDevice;        // Parent Device access .

    void                              * m_lock;         // Queue lock
    struct SourceStatistics             m_Statistics;   // Source statistics

    stm_display_source_queue_listener   m_listener;
    void *                              m_listener_user_data;

    // The 3 following bolean are kept temporary while we keep the old notification mechanism in display_callback.
    // They can be removed when we don't use it anymore.
    bool                                m_isOutputModeChanged;
    bool                                m_areConnectionsChanged;
};

#endif /* _QUEUEBUFFERINTERFACE_H */
