/***********************************************************************
 *
 * File: display/generic/DisplayQueue.h
 * Copyright (c) 2013 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef DISPLAY_QUEUE_H
#define DISPLAY_QUEUE_H

#include "Node.h"

class CNode;

#define QUEUE_NAME_MAX_LENGTH 32

class CDisplayQueue
{
public:
               CDisplayQueue(void);
    virtual   ~CDisplayQueue(void);

    // create the queue
    bool       CreateQueue(char *name);

    // queue given node at the end of the queue
    bool       QueueDisplayNode   (CNode *pNode);

    // release given node from the queue
    bool       ReleaseDisplayNode (CNode *pNode);

    // Function returning the 1st node of the queue
    CNode     *GetFirstNode (void);

    // get the last valid node
    CNode     *GetLastNode(void);

    // Cut the queue AFTER the indicated node
    bool       CutAfterNode(CNode *pNode);

    // Cut the queue BEFORE the indicated node
    bool       CutBeforeNode(CNode *pNode);

private:
    // lock the queue
    bool       LockQueue(void);

    // unlock the queue
    void       UnlockQueue(void);

    // get the tail node
    CNode     *GetTail(CNode *pNode);

    // head of node queue
    CNode     *m_pNodeHead;

    // mutex for protecting queue
    void      *m_queueLock;

    // name of the queue
    char       m_queueName[QUEUE_NAME_MAX_LENGTH];
};



#endif /* DISPLAY_QUEUE_H */

