/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2000-2014 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2014-05-30
***************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include "Output.h"
#include "DisplayPlane.h"
#include "DisplaySource.h"
#include "DisplayDevice.h"
#include <display_device_priv.h>

#include <display/ip/buffercopy/buffercopy.h>

#define MAX_STKPI_DURATION_IN_US    3000

#define MAX_DEVICES_NBR  2

/*
 * CoreDisplay registry types.
 * We don't manage SourceInterface Objects here. This is managed by
 * the DisplaySource object where the interface is begin registered.
 */
static unsigned stm_display_device_type;  /* common device type */
static unsigned stm_display_output_type;
static unsigned stm_display_plane_type;
static unsigned stm_display_source_type;

static struct {
    const char *tag;
    stm_object_h type;
} stm_display_types[] = {
  {"stm_display_device", &stm_display_device_type},
  {"stm_display_output", &stm_display_output_type},
  {"stm_display_plane",  &stm_display_plane_type },
  {"stm_display_source", &stm_display_source_type},
};

CDisplayDevice::CDisplayDevice(uint32_t id, int nOutputs, int nPlanes, int nSources)
{
  TRCIN( TRC_ID_MAIN_INFO, "%p (id=%u, nOutputs=%d nPlanes=%d)", this, id, nOutputs, nPlanes );

  m_ID                 = id;
  m_nNumTuningServices = 0;
  m_pTuningCaps        = 0;
  m_CurrentVTGSync     = 0;
  m_pOutputs           = 0;
  m_numOutputs         = nOutputs;
  m_pPlanes            = 0;
  m_numPlanes          = nPlanes;
  m_pPlanesPrio        = 0;
  m_pSources           = 0;
  m_numSources         = nSources;
  m_pReg               = 0;
  m_bIsSuspended       = false;
  m_bIsFrozen          = false;
  m_vsyncLock          = 0;
  m_use_count          = 0;
  m_stkpi_mutex_lock   = 0;
  m_stkpi_lock_time    = 0;
  m_stkpi_unlock_time  = 0;
  m_MasterVideoPlane   = 0;
  vibe_os_zero_memory(&m_pm, sizeof(m_pm) );
  vibe_os_zero_memory(&m_devConfig, sizeof(m_devConfig) );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


CDisplayDevice::~CDisplayDevice()
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  CBufferCopy::FreeCopiedPictBuffers();

  /*
   * We have to defer destroying outputs to the subclasses,
   * as their destructors may need to access device registers
   * and we cannot guarantee that those registers are still mapped
   * in the memory space at this point.
   */
  delete[] m_pOutputs;
  m_pOutputs = NULL;
  delete[] m_pPlanes;
  m_pPlanes = NULL;
  delete[] m_pPlanesPrio;
  m_pPlanesPrio = NULL;
  delete[] m_pSources;
  m_pSources = NULL;

  /*
   * Remove all remaining CoreDisplay's objects from registry database
   */
  RegistryTerm();

  if(m_vsyncLock)
  {
    vibe_os_delete_mutex(m_vsyncLock);
    m_vsyncLock = 0;
  }

  if(m_stkpi_mutex_lock)
  {
    vibe_os_delete_mutex(m_stkpi_mutex_lock);
    m_stkpi_mutex_lock = 0;
  }

  if(m_pm.spinLock)
  {
    vibe_os_delete_resource_lock(m_pm.spinLock);
    m_pm.spinLock = 0;
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


bool CDisplayDevice::Create()
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  ASSERTF((m_pOutputs == 0 && m_pPlanes == 0 && m_pSources == 0),("CDisplayDevice::Create() has already been called"));

  /*
   * It might be odd to have a device without outputs, but there may be a
   * use for such a thing so don't prevent it.
   */
  if(m_numOutputs > 0)
  {
    if(!(m_pOutputs = new COutput *[m_numOutputs]))
      return false;

    for (uint32_t i = 0; i < m_numOutputs; i++) m_pOutputs[i] = 0;
  }

  if(m_numPlanes > 0)
  {
    if(!(m_pPlanes = new CDisplayPlane *[m_numPlanes]))
      return false;

    if(!(m_pPlanesPrio = new CDisplayPlane *[m_numPlanes]))
      return false;

    for (uint32_t i = 0; i < m_numPlanes; i++) m_pPlanes[i] = 0;
  }

  if(m_numSources > 0)
  {
    if(!(m_pSources = new CDisplaySource *[m_numSources]))
      return false;

    for (uint32_t i = 0; i < m_numSources; i++) m_pSources[i] = 0;
  }

  /*
   * Add object types in the stm_registry database.
   */
  RegistryInit();

  m_vsyncLock = vibe_os_create_mutex();

  m_stkpi_mutex_lock = vibe_os_create_mutex();
  if(m_stkpi_mutex_lock == 0)
  {
    TRC( TRC_ID_ERROR, "Creation of STKPI mutex lock failed!" );
    return false;
  }

  m_pm.spinLock = vibe_os_create_resource_lock();
  if(m_pm.spinLock == 0)
  {
    TRC( TRC_ID_ERROR, "Failed to create power management spinLock" );
    return false;
  }

  PmSetState(DEVICE_ACTIVE);

  CBufferCopy::AllocateCopiedPictBuffers();

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;
}


void CDisplayDevice::VSyncLock(void) const
{
    vibe_os_lock_mutex(m_vsyncLock);
}

void CDisplayDevice::VSyncUnlock(void) const
{
    vibe_os_unlock_mutex(m_vsyncLock);
}

void CDisplayDevice::StkpiLock(const char *fct_name)
{
    vibe_os_lock_mutex(m_stkpi_mutex_lock);

    /* lock_time should be collected AFTER the lock */
    m_stkpi_lock_time = vibe_os_get_system_time();
}

int CDisplayDevice::StkpiUnlock(const char *fct_name)
{
    vibe_time64_t   lock_duration_in_us;

    /* unlock_time should be collected BEFORE the unlock */
    m_stkpi_unlock_time = vibe_os_get_system_time();

    lock_duration_in_us = m_stkpi_unlock_time - m_stkpi_lock_time;
    TRC(TRC_ID_STKPI_LOCK_DURATION, "%s STKPI lock duration : %llu us", fct_name, lock_duration_in_us);

    if(lock_duration_in_us > MAX_STKPI_DURATION_IN_US)
    {
        TRC(TRC_ID_MAIN_INFO, "WARNING! %s locking the STKPI for %llu us!", fct_name, lock_duration_in_us);
    }

    vibe_os_unlock_mutex(m_stkpi_mutex_lock);

    return 0;
}

void CDisplayDevice::DecrementUseCount(void)
{
    if(m_use_count > 0)
    {
        m_use_count--;
    }
    else
    {
        TRC(TRC_ID_ERROR, "Use count cannot be decremented!");
    }
}


void CDisplayDevice::DebugCheckVSyncLockTaken(const char *function_name) const
{
    // Check if m_vsyncLock was taken in the specific function calling it.
    // It is a security check for debug activity.
    // Normally, we should check that this thread is owning the m_vsyncLock but
    // as we haven't this feature, we try to get the m_vsyncLock to see if a thread
    // has already taken it.

    if ( vibe_os_in_interrupt() != 0 )
    {
        TRC(TRC_ID_ERROR, "Error: %s called from interrupt context", function_name);
        return;
    }

    if ( vibe_os_is_locked_mutex(m_vsyncLock) == 0 )
    {
        // m_vsyncLock is not locked
        TRC(TRC_ID_ERROR, "Error: %s has not taken m_vsyncLock previously", function_name);
    }
}

void CDisplayDevice::RegistryInit()
{
  char     DeviceTag[STM_REGISTRY_MAX_TAG_SIZE];
  unsigned i=0;
  int res = 0;

  TRCIN( TRC_ID_MAIN_INFO, "" );

  /*
   * Add Display type into stm_registry database. This will be
   * used as parent object for all created display devices
   * objects.
   *
   * We should not break execution if the device type was previously
   * added to the registry. This will ensure the adding of second device
   * objects in the registry using same object type
   */
  res = stm_registry_add_object(STM_REGISTRY_TYPES,
                  stm_display_types[i].tag,
                  stm_display_types[i].type);
  if (0 == res)
  {
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' type (%p)",  stm_display_types[i].tag, stm_display_types[i].type );
  }

  /*
   * Add real Display object into stm_registry database. This will be
   * used in stm_display_open_device() as parent object instead using fake
   * object.
   */
  vibe_os_snprintf (DeviceTag, sizeof(DeviceTag), "%s%d",
                    stm_display_types[i].tag, m_ID);
  res = stm_registry_add_object(stm_display_types[i].type,
                    DeviceTag,
                    (stm_object_h)this);
  if (0 == res)
  {
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' object (%p)", DeviceTag, this );

    /* CoreDisplay Object is the Root Object */
    for (i=1; i<N_ELEMENTS(stm_display_types); i++)
    {
      res = stm_registry_add_object((stm_object_h)this,
                        stm_display_types[i].tag,
                        stm_display_types[i].type);
      if (0 != res)
        TRC( TRC_ID_ERROR, "Cannot register '%s' type (%d)",  stm_display_types[i].tag, res );
      else
        TRC( TRC_ID_MAIN_INFO, "Registered '%s' type (%d)",  stm_display_types[i].tag, res );
    }
  }
  else
    TRC( TRC_ID_ERROR, "Cannot register '%s' type (%d)",  stm_display_types[i].tag, res );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}

void CDisplayDevice::RegistryTerm()
{
  int i = N_ELEMENTS(stm_display_types)-1;

  TRCIN( TRC_ID_MAIN_INFO, "" );

  /*
   * Remove object types from the stm_registry database.
   * Don't remove the display type object until we move this object
   * otherwise we will fail to clean up the registry.
   */
  do
  {
    int res = stm_registry_remove_object(stm_display_types[i].type);
    if (0 != res)
    {
      TRC( TRC_ID_ERROR, "Cannot unregister '%s' type (%d)",  stm_display_types[i].tag, res );
    }
    else
    {
      TRC( TRC_ID_MAIN_INFO, "Unregistered '%s' type (%d)",  stm_display_types[i].tag, res );
    }
    i--;
  } while ( i > 0 );

  /*
   * Remove this object from the stm_registry database.
   */
  if(stm_registry_remove_object((stm_object_h)this) < 0)
    TRC( TRC_ID_ERROR, "failed to remove display object = %p from the registry",this );
  else
    TRC( TRC_ID_MAIN_INFO, "Remove display object = %p from the registry",this );

  /*
   * Now remove the display type object from the stm_registry database.
   */
  if(stm_registry_remove_object(stm_display_types[i].type) < 0)
    TRC( TRC_ID_ERROR, "failed to remove display type = %p from the registry",stm_display_types[i].type );
  else
    TRC( TRC_ID_MAIN_INFO, "Remove display type = %p from the registry",stm_display_types[i].type );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}

void CDisplayDevice::SetMasterVideoPlane(CDisplayPlane *plane)
{
  if(!plane || (m_MasterVideoPlane == plane))
    return;

  for(uint32_t plane_id=0; plane_id<m_numPlanes; plane_id++)
  {

    if(!m_pPlanes[plane_id])
      continue;

    if(m_pPlanes[plane_id] == plane)
    {
      m_pPlanes[plane_id]->SetMasterVideoPlane(true);
      m_MasterVideoPlane = plane;
    }
    else
    {
      m_pPlanes[plane_id]->SetMasterVideoPlane(false);
    }
  }
}

int CDisplayDevice::FindOutputsWithCapabilities(
                               stm_display_output_capabilities_t  caps_match,
                               stm_display_output_capabilities_t  caps_mask,
                               uint32_t*                          id,
                               uint32_t                           max_ids)
{
  int             nb_output_found=0;
  uint32_t        output_id;
  TRCIN( TRC_ID_MAIN_INFO, "" );

  for(output_id=0; output_id<m_numOutputs; output_id++ ) {

    if(!m_pOutputs[output_id])
      continue;

    if( (m_pOutputs[output_id]->GetCapabilities() & ((uint32_t)caps_mask)) == ((uint32_t)caps_match) ) {

      if(max_ids==0) {

        // Count all output having this specific capabilities
        nb_output_found++;

      } else {

        // Fill output "id" table and count number of output having this specific capabilities until max_ids reached
        id[nb_output_found]=output_id;
        nb_output_found++;
        if((uint32_t)(nb_output_found) == max_ids) break;

      }

    }

  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return nb_output_found;
}

bool CDisplayDevice::AddOutput(COutput *pOutput)
{
  char OutputTag[STM_REGISTRY_MAX_TAG_SIZE];

  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(!pOutput)
    return false;

  TRC( TRC_ID_MAIN_INFO, "Trying to add output \"%s\" ID:%u (%p) to device ID:%u (%p)",pOutput->GetName(),pOutput->GetID(),pOutput,GetID(),this );

  if(pOutput->GetID() >= m_numOutputs)
  {
    TRC( TRC_ID_ERROR, "Output ID (%u) out of valid range for device",pOutput->GetID() );
    goto error;
  }

  if(m_pOutputs[pOutput->GetID()] != 0)
  {
    TRC( TRC_ID_ERROR, "Output ID (%u) already registered with device",pOutput->GetID() );
    goto error;
  }

  if(!pOutput->Create())
  {
    TRC( TRC_ID_ERROR, "Failed to create output before adding to device" );
    goto error;
  }


  m_pOutputs[pOutput->GetID()] = pOutput;

  /*
   * Add the real Plane object to the registry.
   */
  vibe_os_snprintf (OutputTag, sizeof(OutputTag), "%s",
                    pOutput->GetName());

  if (stm_registry_add_object(&stm_display_output_type,
                  OutputTag,
                (stm_object_h)pOutput) != 0)
    TRC( TRC_ID_ERROR, "Cannot register '%s' object (%d)",  OutputTag, pOutput->GetID() );
  else
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' object (%d)",  OutputTag, pOutput->GetID() );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;

error:
  delete pOutput;
  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return false;
}

int CDisplayDevice::FindPlanesWithCapabilities(
                               stm_plane_capabilities_t  caps_match,
                               stm_plane_capabilities_t  caps_mask,
                               uint32_t*                 id,
                               uint32_t                  max_ids)
{
  int             nb_plane_found=0;
  uint32_t        plane_id;
  TRCIN( TRC_ID_MAIN_INFO, "" );

  for(plane_id=0; plane_id<m_numPlanes; plane_id++ ) {

    if(!m_pPlanes[plane_id])
      continue;

    if( (m_pPlanes[plane_id]->GetCapabilities() & caps_mask) == caps_match ) {

      if(max_ids==0) {

        // Count all plane having this specific capabilities
        nb_plane_found++;

      } else {

        // Fill plane "id" table and count number of plane having this specific capabilities until max_ids reached
        id[nb_plane_found]=plane_id;
        nb_plane_found++;
        if((uint32_t)(nb_plane_found) == max_ids) break;

      }

    }

  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return nb_plane_found;
}

bool CDisplayDevice::AddPlane(CDisplayPlane *pPlane)
{
  char PlaneTag[STM_REGISTRY_MAX_TAG_SIZE];

  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(!pPlane)
    return false;

  TRC( TRC_ID_MAIN_INFO, "Trying to add plane \"%s\" ID:%u (%p) to device ID:%u (%p)",pPlane->GetName(),pPlane->GetID(),pPlane,GetID(),this );

  if(pPlane->GetID() >= m_numPlanes)
  {
    TRC( TRC_ID_ERROR, "Plane ID (%u) out of valid range for device",pPlane->GetID() );
    goto error;
  }

  if(m_pPlanes[pPlane->GetID()] != 0)
  {
    TRC( TRC_ID_ERROR, "Plane ID (%u) already registered with device",pPlane->GetID() );
    goto error;
  }

  if(!pPlane->Create())
  {
    TRC( TRC_ID_ERROR, "Failed to create plane before adding to device" );
    goto error;
  }

  m_pPlanes[pPlane->GetID()] = pPlane;
  m_pPlanesPrio[pPlane->GetID()] = pPlane;

  /*
   * Add the real Plane object to the registry.
   */
  vibe_os_snprintf (PlaneTag, sizeof(PlaneTag), "%s",
                    pPlane->GetName());

  if (stm_registry_add_object(&stm_display_plane_type, PlaneTag, (stm_object_h)pPlane) != 0)
  {
    TRC( TRC_ID_ERROR, "Cannot register '%s' object (%d)", PlaneTag, pPlane->GetID() );
  }
  else
  {
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' object (%d)", PlaneTag, pPlane->GetID() );
    /*
     * Register the attribute to collect statistics on the plane
     */
    pPlane->RegisterStatistics();
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;

error:
  delete pPlane;
  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return false;
}

bool CDisplayDevice::AddSource(CDisplaySource *pSource)
{
  char SourceTag[STM_REGISTRY_MAX_TAG_SIZE];
  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(!pSource)
    return false;

  TRC( TRC_ID_MAIN_INFO, "Trying to add Source \"%s\" ID:%u (%p) to device ID:%u (%p)",pSource->GetName(),pSource->GetID(),pSource,GetID(),this );

  if(pSource->GetID() >= m_numSources)
  {
    TRC( TRC_ID_ERROR, "Source ID (%u) out of valid range for device",pSource->GetID() );
    goto error;
  }

  if(m_pSources[pSource->GetID()] != 0)
  {
    TRC( TRC_ID_ERROR, "Source ID (%u) already registered with device",pSource->GetID() );
    goto error;
  }

  if(!pSource->Create())
  {
    TRC( TRC_ID_ERROR, "Failed to create Source before adding to device" );
    goto error;
  }

  m_pSources[pSource->GetID()] = pSource;

  /*
   * Add the real Source object to the registry.
   */
  vibe_os_snprintf (SourceTag, sizeof(SourceTag), "%s",
                       pSource->GetName());

  if (stm_registry_add_object(&stm_display_source_type, SourceTag, (stm_object_h)pSource) != 0)
  {
    TRC( TRC_ID_ERROR, "Cannot register '%s' object (%d)", SourceTag, pSource->GetID() );
  }
  else
  {
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' object (%d)", SourceTag, pSource->GetID() );
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;

error:
  delete pSource;
  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return false;
}

void CDisplayDevice::RemoveSources(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  for(unsigned i=0;i<m_numSources;i++)
  {
    if(m_pSources[i])
    {
      /*
       * Remember that a SourceInterface was registered as a child of this object
       * into the stm_registry database. In order to successfully clean up
       * the stm_registry database before removing coredisplay module we should
       * remove the SourceInterface object from the database before removing
       * this object. The removal of SourceInterface object will be done in the
       * CDisplaySource destructor.
       */
      delete m_pSources[i];

      /*
       * Now we can remove object from the registry before exiting.
       */
      if(stm_registry_remove_object(m_pSources[i]) < 0)
        TRC( TRC_ID_ERROR, "failed to remove source object %p (= %s) from the registry", m_pSources[i], m_pSources[i]->GetName() );

      m_pSources[i] = 0L;
    }
  }
  TRC( TRC_ID_MAIN_INFO, "CDisplayDevice::RemoveSources() deleted all sources" );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


void CDisplayDevice::RemovePlanes(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );
  for(unsigned i=0;i<m_numPlanes;i++)
  {
    /*
     * Remove object from the registry before exiting.
     */
    if(!m_pPlanes[i])
      continue;
    if(stm_registry_remove_object(m_pPlanes[i]) < 0)
      TRC( TRC_ID_ERROR, "failed to remove plane object %p (= %s) from the registry", m_pPlanes[i], m_pPlanes[i]->GetName() );
    delete m_pPlanes[i];
    m_pPlanes[i] = 0L;
  }
  TRC( TRC_ID_MAIN_INFO, "CDisplayDevice::RemovePlanes() deleted all planes" );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


void CDisplayDevice::RemoveOutputs(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  /*
   * First, call all the output CleanUp routines while all of the output
   * objects still exist.
   */
  for (uint32_t i = 0; i < m_numOutputs; i++)
  {
    if(m_pOutputs[i]) m_pOutputs[i]->CleanUp();
  }

  /*
   * Now delete everything.
   */
  for (uint32_t i = 0; i < m_numOutputs; i++)
  {
    /*
     * Remove object from the registry before exiting.
     */
    if(!m_pOutputs[i])
      continue;
    if(stm_registry_remove_object(m_pOutputs[i]) < 0)
      TRC( TRC_ID_ERROR, "failed to remove output object %p (= %s) from the registry", m_pOutputs[i], m_pOutputs[i]->GetName() );
    delete m_pOutputs[i];
    m_pOutputs[i] = 0L;
  }
  TRC( TRC_ID_MAIN_INFO, "CDisplayDevice::RemoveOutputs() deleted all Outputs" );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


TuningResults CDisplayDevice::SetTuning(uint16_t service,
                                        void *inputList,
                                        uint32_t inputListSize,
                                        void *outputList,
                                        uint32_t outputListSize)
{
    TuningResults         res = TUNING_SERVICE_NOT_SUPPORTED;
    SetTuningInputData_t* input = 0;
    CDisplayPlane*        pPlaneHandle  = 0;
    COutput             * pOutputHandle = NULL;
    CDisplaySource*       pSourceHandle = 0;

    input  = (SetTuningInputData_t*)inputList;

    if(!input)
    {
        TRC(TRC_ID_ERROR, "Error! Invalid input list!");
        return res;
    }

    if(m_bIsSuspended)
    {
        TRC(TRC_ID_MAIN_INFO, "Service not available (driver suspended)");
        return TUNING_SERVICE_NOT_AVAILABLE;
    }

    switch(service)
    {
        case CRC_COLLECT:
        case CRC_CAPABILITY:
        {
            /* to get CRC on video planes */
            pPlaneHandle = GetPlane(input->PlaneId);
            TRC( TRC_ID_UNCLASSIFIED, "pPlaneHandle=%p", pPlaneHandle );
            if(pPlaneHandle)
            {
                res = pPlaneHandle->SetTuning(service, inputList, inputListSize, outputList, outputListSize);
            }
            else
            {
                TRC(TRC_ID_ERROR, "Invalid pPlaneHandle=0x%p", pPlaneHandle);
                res = TUNING_INVALID_PARAMETER;
            }
            break;
        }
        case MDTP_CRC_COLLECT:
        case MDTP_CRC_SET_CONTROL:
        case MDTP_CRC_STOP:
        case MDTP_CRC_CAPABILITY:
        {
            /* to get CRC on sources */
            pSourceHandle = GetSource(input->SourceId);
            TRC( TRC_ID_UNCLASSIFIED, "pSourceHandle=%p", pSourceHandle );
            if(pSourceHandle)
            {
                res = pSourceHandle->SetTuning(service, inputList, inputListSize, outputList, outputListSize);
            }
            else
            {
                TRC(TRC_ID_ERROR, "Invalid pSourceHandle=0x%p", pSourceHandle);
                res = TUNING_INVALID_PARAMETER;
            }
            break;
        }
        case MISR_COLLECT:
        case MISR_SET_CONTROL:
        case MISR_CAPABILITY:
        case MISR_STOP:
        {
            pOutputHandle = GetOutput(input->OutputId);
            TRC( TRC_ID_UNCLASSIFIED, "pOutputHandle=%p", pOutputHandle );
            if(pOutputHandle)
            {
                res = pOutputHandle->SetTuning(service, inputList, inputListSize, outputList, outputListSize);
            }
            else
            {
                TRC(TRC_ID_ERROR, "Invalid pOutputHandle=0x%p", pOutputHandle);
                res = TUNING_INVALID_PARAMETER;
            }
            break;
        }
        case RESET_STATISTICS:
        {
            pPlaneHandle = GetPlane(input->PlaneId);
            if(pPlaneHandle)
            {
                res = pPlaneHandle->SetTuning(service, inputList, inputListSize, outputList, outputListSize);
            }

            pSourceHandle = GetSource(input->SourceId);
            if(pSourceHandle)
            {
                res = pSourceHandle->SetTuning(service, inputList, inputListSize, outputList, outputListSize);
            }
            if(res != TUNING_OK)
            {
                TRC(TRC_ID_ERROR, "Error! RESET_STATISTICS failed (%d)", res);
            }
            break;
        }
        default:
            TRC(TRC_ID_ERROR, "Error! Unkown service! (%d)", service);
            break;
    }

    return res;
}


void CDisplayDevice::UpdateSource(uint32_t timingID)
{
  /*
   * Don't allow any coming updates while suspending the device or if a
   * suspend is already in progress.
   */
  if(m_bIsSuspended)
    return;

  for(unsigned i=0;i<m_numSources;i++)
  {
    if(!m_pSources[i])
      continue;

    if(m_pSources[i]->GetTimingID() == timingID)
    {
        m_pSources[i]->SourceVSyncUpdateHW();
    }
  }
}


void CDisplayDevice::OutputVSyncIrqUpdateHw(uint32_t timingID)
{
  /*
   * Single implementation for devices with single or multiple VTGs; all planes
   * that are connected to outputs with the same timing ID are updated,
   * regardless of which actual output they are being displayed on.
   *
   */
  const stm_display_mode_t *pCurrentMode = 0;
  stm_time64_t           vsyncTime = 0ULL;
  bool                   isTopFieldOnDisplay = false;
  bool                   isDisplayInterlaced = false;

  /*
   * Don't allow any coming updates while suspending the device or if a
   * suspend is already in progress.
   */
  if(m_bIsSuspended)
    return;

  TRCBL(TRC_ID_VSYNC);

  /*
   * Go through all outputs and update them. There may be circumstances where
   * more than one output actually reports the same timing ID.
   */
  for(unsigned i=0;i<m_numOutputs;i++)
  {
    if(!m_pOutputs[i])
      continue;

    if(m_pOutputs[i]->GetTimingID() == timingID)
    {
      if(!pCurrentMode)
      {
        /*
         * It doesn't matter which output we find first, use it to find out
         * about the display mode and timing generator state.
         */
        pCurrentMode = m_pOutputs[i]->GetCurrentDisplayMode();

        /*
         * If there is no valid display mode, we must have got a rogue VTG
         * interrupt from the hardware, ignore it and do not do anything else.
         */
        if(!pCurrentMode)
          return;

        isDisplayInterlaced = (pCurrentMode->mode_params.scan_type == STM_INTERLACED_SCAN);
        isTopFieldOnDisplay = (m_pOutputs[i]->GetCurrentVTGEvent() & STM_TIMING_EVENT_TOP_FIELD) != 0;
        vsyncTime           = m_pOutputs[i]->GetCurrentVTGEventTime();
      }

      m_pOutputs[i]->UpdateHW();
    }
  }
}

uint32_t CDisplayDevice::FindPlanePrio(CDisplayPlane* plane) const
{
    uint32_t i = 0;

    for (i=0; i<m_numPlanes; i++)
    {
      if (m_pPlanesPrio[i] == plane)
      {
        break;
      }
    }

    return i;
}

void CDisplayDevice::PlaneNeedsPrioHwUpdate(CDisplayPlane* plane)
{
    uint32_t prevPrio = FindPlanePrio(plane);
    for(uint32_t i=prevPrio; i<(m_numPlanes-1); i++)
    {
      m_pPlanesPrio[i] = m_pPlanesPrio[i+1];
    }
    m_pPlanesPrio[m_numPlanes-1] = plane;

    for(uint32_t i=0; i<m_numPlanes; i++)
    {
        TRC( TRC_ID_MAIN_INFO, "m_pPlanesPrio[%d] = %s", i, m_pPlanesPrio[i]->GetName());
    }
}

  /*
   * Same as OutputVSyncIrqUpdateHw() but in thread context.
   */
void CDisplayDevice::OutputVSyncThreadedIrqUpdateHw(uint32_t timingID)
{

  const stm_display_mode_t *pCurrentMode = 0;
  stm_time64_t           vsyncTime = 0ULL;
  bool                   isTopFieldOnDisplay = false;
  bool                   isDisplayInterlaced = false;

  /*
   * When System is entering CPS : we allow only updating plane's state to
   * ensure both HW and clocks are safely disabled.
   */
  if(m_bIsFrozen)
  {
    VSyncLock();
    goto update_plane_state;
  }

  if(m_bIsSuspended)
    return;

  /*
   * Go through all outputs just to get the vsync time.
   * The outputs are updated in IRQ.
   */
  for(unsigned i=0;i<m_numOutputs;i++)
  {
    if(!m_pOutputs[i])
      continue;

    if(m_pOutputs[i]->GetTimingID() == timingID)
    {
      if(!pCurrentMode)
      {
        /*
         * It doesn't matter which output we find first, use it to find out
         * about the display mode and timing generator state.
         */
        pCurrentMode = m_pOutputs[i]->GetCurrentDisplayMode();

        /*
         * If there is no valid display mode, we must have got a rogue VTG
         * interrupt from the hardware, ignore it and do not do anything else.
         */
        if(!pCurrentMode)
          return;

        isDisplayInterlaced = (pCurrentMode->mode_params.scan_type == STM_INTERLACED_SCAN);
        isTopFieldOnDisplay = (m_pOutputs[i]->GetCurrentVTGEvent() & STM_TIMING_EVENT_TOP_FIELD) != 0;
        vsyncTime           = m_pOutputs[i]->GetCurrentVTGEventTime();
      }
    }
  }

  VSyncLock();

  for(unsigned i=0;i<m_numPlanes;i++)
  {
    if(m_pPlanesPrio[i] && (m_pPlanesPrio[i]->GetTimingID() == timingID))
    {
      m_pPlanesPrio[i]->OutputVSyncThreadedIrqUpdateHW(isDisplayInterlaced, isTopFieldOnDisplay, vsyncTime);
    }
  }

  for(unsigned i=0;i<m_numSources;i++)
  {
    if(m_pSources[i] && (m_pSources[i]->GetTimingID() == timingID))
    {
      m_pSources[i]->OutputVSyncThreadedIrqUpdateHW(isDisplayInterlaced, isTopFieldOnDisplay, vsyncTime);
    }
  }

update_plane_state:

  // All the planes paced by this VTG should be called again to see if a valid picture has been presented
  // by the source. If not, the plane will be disabled.
  for(unsigned i=0;i<m_numPlanes;i++)
  {
    if(m_pPlanes[i] && (m_pPlanes[i]->GetTimingID() == timingID))
    {
      m_pPlanes[i]->UpdatePlaneState();
    }
  }

  VSyncUnlock();

  /*
   * The selection of the picture, to be presented at next Vsync, was done previously under VSyncLock
   * Due to VSynclock, it is no possible to let the output to notify all connected planes.
   * So the configuration is done now, for all planes.
   */
  if(m_MasterVideoPlane && (m_MasterVideoPlane->GetTimingID() == timingID))
    m_MasterVideoPlane->UpdateOutputHDRInfo();

  for(unsigned i=0;i<m_numPlanes;i++)
  {
    if(m_pPlanes[i] && (m_pPlanes[i]->GetTimingID() == timingID))
    {
      /*
       * When the buffer hdr characteristics changes the master video plane will update the output hdr format during this Vsync
       * For all the other connected planes to the same output which has been changed before by the master video plane, the new changes
       * will be applied one vsync later.
       */
      if(m_pPlanes[i] != m_MasterVideoPlane)
        m_pPlanes[i]->UpdateOutputHDRInfo();
    }
  }
}


int CDisplayDevice::Freeze(void)
{
  int             res=0;
  uint32_t        id;

  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(m_bIsFrozen)
    return res;

  /*
   * Set the device state to frozen to avoid any coming update while
   * we are switch off the display device.
   *
   * Sources, Planes and Outputs will continue processing previous
   * update as they are not yet frozen at this point.
   */
  m_bIsFrozen = true;
  m_bIsSuspended = true;

  for(id=0; id<m_numSources; id++ )
  {
    if(m_pSources[id])
      m_pSources[id]->Freeze();
  }

  for(id=0; id<m_numPlanes; id++ )
  {
    if(m_pPlanes[id])
      m_pPlanes[id]->Freeze();
  }

  for(id=0; id<m_numOutputs; id++ )
  {
    if(m_pOutputs[id])
      m_pOutputs[id]->Suspend();
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return res;
}


int CDisplayDevice::Suspend(void)
{
  int             res=0;
  uint32_t        id;

  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(m_bIsSuspended)
    return res;

  /*
   * Set the device state to suspended to avoid any coming update while
   * we are switch off the display device.
   *
   * Sources, Planes and Outputs will continue processing previous
   * update as they are not yet suspended at this point.
   */
  m_bIsSuspended = true;

  for(id=0; id<m_numSources; id++ )
  {
    if(m_pSources[id])
      m_pSources[id]->Suspend();
  }

  for(id=0; id<m_numPlanes; id++ )
  {
    if(m_pPlanes[id])
      m_pPlanes[id]->Suspend();
  }

  for(id=0; id<m_numOutputs; id++ )
  {
    if(m_pOutputs[id])
      m_pOutputs[id]->Suspend();
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return res;
}


int CDisplayDevice::Resume(void)
{
  int             res=0;
  uint32_t        id;
  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(!m_bIsSuspended)
    return res;

  for(id=0; id<m_numOutputs; id++)
  {
    if(m_pOutputs[id])
      m_pOutputs[id]->Resume();
  }

  for(id=0; id<m_numPlanes; id++)
  {
    if(m_pPlanes[id])
      m_pPlanes[id]->Resume();
  }

  for(id=0; id<m_numSources; id++)
  {
    if(m_pSources[id])
      m_pSources[id]->Resume();
  }

  m_bIsSuspended = false;
  m_bIsFrozen = false;

  PmSetState(DEVICE_ACTIVE);

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return res;
}

void CDisplayDevice::RemoveOutputsCapabilities(uint32_t caps)
{
  uint32_t        id;
  TRCIN( TRC_ID_MAIN_INFO, "" );

  for(id=0; id<m_numOutputs; id++)
  {
    if(m_pOutputs[id])
      m_pOutputs[id]->RemoveCapabilities(caps);
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
  return;
}


void CDisplayDevice::PowerDownVideoDacs(void)
{
  uint32_t        id;
  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(m_bIsSuspended)
    return;

  for(id=0; id<m_numOutputs; id++)
  {
    if(m_pOutputs[id])
      m_pOutputs[id]->PowerDownDACs();
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}

bool CDisplayDevice::PmGetState(DevicePowerState *currentState)
{
  if(!m_pm.spinLock)
  {
    TRC(TRC_ID_ERROR, "Invalid m_pm.spinLock!");
    return false;
  }

  vibe_os_lock_resource(m_pm.spinLock);
  *currentState = m_pm.state;
  vibe_os_unlock_resource(m_pm.spinLock);

  return true;
}

bool CDisplayDevice::PmIsSuspended(const char *fct_name)
{
  DevicePowerState  pm_state;
  bool isSuspended;

  if(!m_pm.spinLock)
  {
    TRC(TRC_ID_ERROR, "Invalid m_pm.spinLock!");
    return false;
  }

  vibe_os_lock_resource(m_pm.spinLock);
  pm_state = m_pm.state;
  vibe_os_unlock_resource(m_pm.spinLock);

  if((pm_state == DEVICE_SUSPENDED) || (pm_state == DEVICE_FROZEN))
  {
    // In the current implementation, IS_DEVICE_SUSPENDED() is only called by STKPI functions
    // to check that the device is not suspended. If it is suspended, it means that some STKPI
    // functions are called while the device is suspended. This is an error.
    TRC(TRC_ID_MAIN_INFO, "DE_ERROR: %s called while device suspended!", fct_name);
    isSuspended = true;
  }
  else
  {
    isSuspended = false;
  }

  return isSuspended;
}



bool CDisplayDevice::PmSetState(DevicePowerState newState)
{
  if(!m_pm.spinLock)
  {
    TRC(TRC_ID_ERROR, "Invalid m_pm.spinLock!");
    return false;
  }

  vibe_os_lock_resource(m_pm.spinLock);
  m_pm.state = newState;
  vibe_os_unlock_resource(m_pm.spinLock);

  return true;
}

void CDisplayDevice::PmGetDevice(void)
{
  if(m_pm.runtime_get)
  {
    m_pm.runtime_get(m_ID);
  }
}

void CDisplayDevice::PmPutDevice(void)
{
  if(m_pm.runtime_put)
  {
    m_pm.runtime_put(m_ID);
  }
}

void CDisplayDevice::PmSetRuntimeHooks(int (*get)(const uint32_t id),
                                       int (*put)(const uint32_t id) )
{
  m_pm.runtime_get = get;
  m_pm.runtime_put = put;
}

//////////////////////////////////////////////////////////////////////////////
// C Device interface

extern "C" {

static int num_devices = 0;
static CDisplayDevice * display_devices[MAX_DEVICES_NBR] = {0, 0};


/*********************************************************************/
/** C Private Device interfaces                                      */
/*********************************************************************/
int stm_display_device_register_pm_runtime_hooks(stm_display_device_h dev, int (*get)(const uint32_t id), int (*put)(const uint32_t id))
{
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  if(!CHECK_ADDRESS(get) || !CHECK_ADDRESS(put))
    return -EFAULT;

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  pDev->PmSetRuntimeHooks(get, put);

  STKPI_UNLOCK(pDev);

  return 0;
}

int stm_display_device_get_power_state(stm_display_device_h dev, uint32_t *pm_state)
{
  CDisplayDevice* pDev = NULL;
  int res;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  /* pm_state pointer should be valid */
  if(!CHECK_ADDRESS(pm_state))
    return -EFAULT;

  pDev = dev->handle;

  /* This function can be called in ISR context so we cannot take "dev->mutex_lock".
     The protection of "m_pm.state" is done thanks to a dedicated spinLock ("m_pm.spinLock")
  */
  if(pDev->PmGetState((DevicePowerState *) pm_state))
  {
    res = 0;
    TRC(TRC_ID_API_DEVICE, "dev : %u, pm_state : %u", pDev->GetID(), *pm_state);
  }
  else
  {
    res = -EINVAL;
  }

  return 0;
}

int stm_display_output_remove_capabilities(stm_display_device_h dev, uint32_t caps)
{
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  pDev->RemoveOutputsCapabilities(caps);

  STKPI_UNLOCK(pDev);

  return 0;
}

int stm_display_device_power_down_video_dacs(stm_display_device_h dev)
{
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  pDev->PowerDownVideoDacs();

  STKPI_UNLOCK(pDev);

  return 0;
}


/*********************************************************************/
/** C Public Device interfaces                                       */
/*********************************************************************/

int stm_display_device_find_outputs_with_capabilities(stm_display_device_h dev,
                                                     stm_display_output_capabilities_t  caps_match,
                                                     stm_display_output_capabilities_t  caps_mask,
                                                     uint32_t*                          id,
                                                     uint32_t                           max_ids)
{
  CDisplayDevice* pDev = NULL;
  int nb_output_found;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  // We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDev = dev->handle;

  if(max_ids > 0) {

    //id pointer should be valid
    if(!CHECK_ADDRESS(id))
      return -EFAULT;
  }

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());
  nb_output_found=pDev->FindOutputsWithCapabilities(caps_match, caps_mask, id, max_ids);

  STKPI_UNLOCK(pDev);

  return (nb_output_found);
}

int stm_display_device_open_output(stm_display_device_h dev, uint32_t outputID, stm_display_output_h *output)
{
  CDisplayDevice* pDev = NULL;
  struct stm_display_output_s *public_output = 0;
  COutput *real_output;
  char OutputTag[STM_REGISTRY_MAX_TAG_SIZE];
  int error_code = 0;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  if(IS_DEVICE_SUSPENDED(pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  STKPI_LOCK(pDev);

  if(!CHECK_ADDRESS(output))
  {
    error_code = -EFAULT;
    goto exit;
  }

  TRC(TRC_ID_API_DEVICE, "dev : %u, outputID : %u", pDev->GetID(), outputID);
  real_output = pDev->GetOutput(outputID);
  if(!real_output)
  {
    error_code = -ENODEV;
    goto exit;
  }

  public_output = new struct stm_display_output_s;
  if(!public_output)
  {
    TRC( TRC_ID_ERROR, "Failed to allocate output handle structure" );
    error_code = -ENOMEM;
    goto exit;
  }

  public_output->handle = real_output;
  public_output->pDev   = pDev;

  stm_coredisplay_magic_set(public_output, VALID_OUTPUT_HANDLE);

  /*
   * Add the Output instance to the registry.
   */
  vibe_os_snprintf (OutputTag, sizeof(OutputTag), "%s-%p",
                    real_output->GetName(), public_output);

  if(stm_registry_add_instance (STM_REGISTRY_INSTANCES,
                                  (stm_object_h)real_output,
                                  OutputTag,
                                  (stm_object_h)public_output) != 0)
    TRC( TRC_ID_ERROR, "Cannot register output instance (%p)", public_output );

  TRC(TRC_ID_API_DEVICE, "output : %s", public_output->handle->GetName());
  TRC(TRC_ID_HANDLE_OPEN_CLOSE, "output %d: 0x%p", outputID, public_output);

exit:
  STKPI_UNLOCK(pDev);

  *output=public_output;
  return error_code;
}


int stm_display_device_find_planes_with_capabilities(stm_display_device_h dev,
                                                     stm_plane_capabilities_t caps_match,
                                                     stm_plane_capabilities_t caps_mask,
                                                     uint32_t* id,
                                                     uint32_t  max_ids)
{
  CDisplayDevice* pDev = NULL;
  int nb_plane_found;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  // We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDev = dev->handle;

  if(max_ids > 0) {

    //id pointer should be valid
    if(!CHECK_ADDRESS(id))
      return -EFAULT;
  }

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());
  nb_plane_found=pDev->FindPlanesWithCapabilities(caps_match, caps_mask, id, max_ids);

  STKPI_UNLOCK(pDev);

  return (nb_plane_found);
}

int stm_display_device_open_plane(stm_display_device_h dev, uint32_t planeID, stm_display_plane_h *plane)
{
  CDisplayDevice* pDev = NULL;
  struct stm_display_plane_s *public_plane = 0;
  CDisplayPlane *real_plane;
  char PlaneTag[STM_REGISTRY_MAX_TAG_SIZE];
  int error_code = 0;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  if(IS_DEVICE_SUSPENDED(pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  STKPI_LOCK(pDev);

  if(!CHECK_ADDRESS(plane))
  {
    error_code = -EFAULT;
    goto exit;
  }

  TRC(TRC_ID_API_DEVICE, "dev : %u, planeID : %u", pDev->GetID(), planeID);
  real_plane = pDev->GetPlane(planeID);
  if(!real_plane)
  {
    error_code = -ENODEV;
    goto exit;
  }

  public_plane = new struct stm_display_plane_s;
  if(!public_plane)
  {
    TRC( TRC_ID_ERROR, "Failed to allocate plane handle structure" );
    error_code = -ENOMEM;
    goto exit;
  }

  public_plane->handle     = real_plane;
  public_plane->pDev       = pDev;
  stm_coredisplay_magic_set(public_plane, VALID_PLANE_HANDLE);

  /*
   * Add the Plane instance to the registry.
   */
  vibe_os_snprintf(PlaneTag, sizeof(PlaneTag), "%s-%p",
                   real_plane->GetName(), public_plane);

  if(stm_registry_add_instance (STM_REGISTRY_INSTANCES,
                                  (stm_object_h)real_plane,
                                  PlaneTag,
                                  (stm_object_h)public_plane) != 0)
    TRC( TRC_ID_ERROR, "Cannot register plane instance (%p)", public_plane );

  TRC(TRC_ID_API_DEVICE, "plane : %s", public_plane->handle->GetName());
  TRC(TRC_ID_HANDLE_OPEN_CLOSE, "plane %d: 0x%p", planeID, public_plane);
exit:
  STKPI_UNLOCK(pDev);

  *plane = public_plane;

  return error_code;
}

int stm_display_device_open_source(stm_display_device_h dev, uint32_t sourceID, stm_display_source_h *source)
{
  CDisplayDevice       * pDev = NULL;
  stm_display_source_h   public_source = 0;
  CDisplaySource       * real_source;
  char                   SourceTag[STM_REGISTRY_MAX_TAG_SIZE];
  int error_code = 0;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  if(IS_DEVICE_SUSPENDED(pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  STKPI_LOCK(pDev);

  if(!CHECK_ADDRESS(source))
  {
    error_code = -EFAULT;
    goto exit;
  }

  TRC(TRC_ID_API_DEVICE, "dev : %u, sourceID : %u", pDev->GetID(), sourceID);
  real_source = pDev->GetSource(sourceID);
  if(!real_source)
  {
    TRC( TRC_ID_ERROR, "Failed to get a valid source object from the device" );
    error_code = -ENODEV;
    goto exit;
  }

  public_source = new struct stm_display_source_s;
  if(!public_source)
  {
    TRC( TRC_ID_ERROR, "Failed to allocate source handle structure" );
    error_code = -ENOMEM;
    goto exit;
  }

  public_source->handle = real_source;
  public_source->pDev   = pDev;

  stm_coredisplay_magic_set(public_source, VALID_SOURCE_HANDLE);

  /*
   * Add the Source instance to the registry.
   */
  vibe_os_snprintf (SourceTag, sizeof(SourceTag), "%s-%p",
                    real_source->GetName(), public_source);

  if(stm_registry_add_instance (STM_REGISTRY_INSTANCES,
                                  (stm_object_h)real_source,
                                  SourceTag,
                                  (stm_object_h)public_source) != 0)
    TRC( TRC_ID_ERROR, "Cannot register source instance (%p)", public_source );

  TRC(TRC_ID_API_DEVICE, "source : %s", public_source->handle->GetName());
  TRC(TRC_ID_HANDLE_OPEN_CLOSE, "source %d: 0x%p", sourceID, public_source);
exit:
  STKPI_UNLOCK(pDev);

  *source=public_source;
  return error_code;
}


int stm_display_device_get_number_of_tuning_services(stm_display_device_h dev, uint16_t *nservices)
{
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  if(IS_DEVICE_SUSPENDED(pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());
  *nservices = pDev->GetNumTuningServices();

  STKPI_UNLOCK(pDev);

  return 0;
}


int stm_display_device_get_tuning_caps(stm_display_device_h dev, stm_device_tuning_caps_t *return_caps)
{
  CDisplayDevice* pDev = NULL;
  const stm_device_tuning_caps_t *dev_caps;
  int services;
  int ret = -1;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  if(IS_DEVICE_SUSPENDED(pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());

  if((services = pDev->GetNumTuningServices()) == 0)
    goto exit;

  if((dev_caps = pDev->GetTuningCaps()) == 0)
    goto exit;

  for(int i=0;i<services;i++)
    return_caps[i] = dev_caps[i];

  ret = 0;
exit:
  STKPI_UNLOCK(pDev);

  return ret;
}


int stm_display_device_set_tuning(stm_display_device_h dev,
                                  uint16_t             service,
                                  void                *input_list,
                                  uint32_t             input_list_size,
                                  void                *output_list,
                                  uint32_t             output_list_size)
{
  TuningResults res = TUNING_INVALID_PARAMETER;
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  pDev = dev->handle;

  if(IS_DEVICE_SUSPENDED(pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  if(!CHECK_ADDRESS(input_list) && (input_list_size != 0))
    return -EFAULT;

  if(!CHECK_ADDRESS(output_list) && (output_list_size != 0))
    return -EFAULT;

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());
  res = pDev->SetTuning(service, input_list, input_list_size, output_list, output_list_size);

  switch(res)
  {
    case TUNING_INVALID_PARAMETER:
      return -ERANGE;
    case TUNING_SERVICE_NOT_SUPPORTED:
      return -ENOTSUP;
    case TUNING_SERVICE_NOT_AVAILABLE:
      return -EAGAIN;
    case TUNING_NO_DATA_AVAILABLE:
      return -ENODATA;
    case TUNING_OK:
      break;
  }

  return 0;
}


void stm_display_device_update_vsync_irq(stm_display_device_h dev, uint32_t timing_id)
{
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return;

  // We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDev = dev->handle;
  TRC(TRC_ID_API_DEVICE, "dev : %u, timing_id : %u", pDev->GetID(), timing_id);

  pDev->OutputVSyncIrqUpdateHw(timing_id);
}

void stm_display_device_update_vsync_threaded_irq(stm_display_device_h dev, uint32_t timing_id)
{
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return;

  // We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDev = dev->handle;
  TRC(TRC_ID_API_DEVICE, "dev : %u, timing_id : %u", pDev->GetID(), timing_id);

  pDev->OutputVSyncThreadedIrqUpdateHw(timing_id);
}

void stm_display_device_source_update(stm_display_device_h dev, uint32_t timing_id)
{
  CDisplayDevice* pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return;

  pDev = dev->handle;
  TRC(TRC_ID_API_DEVICE, "dev : %u, timing_id : %u", pDev->GetID(), timing_id);

  pDev->UpdateSource(timing_id);
}

void stm_display_device_destroy(stm_display_device_h dev)
{
  CDisplayDevice* pDev = NULL;
  uint32_t use_count;

  TRCIN( TRC_ID_MAIN_INFO, "" );

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
  {
    goto exit;
  }

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  use_count = pDev->GetUseCount();
  if(use_count != 0)
  {
    TRC(TRC_ID_ERROR, "Dev %d destroyed while there are still %d handles opened!", pDev->GetID(), use_count);
    /* Continue anyway with the destruction */
  }

  TRC(TRC_ID_API_DEVICE, "dev : %u", dev->handle->GetID());
  TRC(TRC_ID_MAIN_INFO, "Destroying device %d", dev->handle->GetID());

  /*
   * Remove object instance from the registry before exiting.
   */
  if(stm_registry_remove_object(dev) < 0)
  {
    TRC( TRC_ID_ERROR, "failed to remove display object = %p from the registry",dev );
  }

  for(uint32_t id=0; id<MAX_DEVICES_NBR; id++)
  {
    if(display_devices[id] == pDev)
    {
      display_devices[id] = 0;
    }
  }

  delete dev->handle;
  dev->handle = NULL;
  // Clear the "magic" field to invalidate this handle
  stm_coredisplay_magic_clear(dev);
  delete dev;

  num_devices--;

exit:
  TRCOUT( TRC_ID_MAIN_INFO, "" );
}

void stm_display_device_close(stm_display_device_h dev)
{
  CDisplayDevice *pDev = NULL;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return;

  pDev = dev->handle;

  TRCIN(TRC_ID_MAIN_INFO, "");

  STKPI_LOCK(pDev);

  pDev->DecrementUseCount();

  TRC(TRC_ID_API_DEVICE, "dev : %u", dev->handle->GetID() );
  TRC(TRC_ID_HANDLE_OPEN_CLOSE, "Close dev 0x%p. UseCount: %d", dev, pDev->GetUseCount());

  STKPI_UNLOCK(pDev);

  dev->handle = NULL;
  // Clear the "magic" field to invalidate this handle
  stm_coredisplay_magic_clear(dev);
  delete dev;

  /* DPM : Put device */
  pDev->PmPutDevice();

  TRCOUT(TRC_ID_MAIN_INFO, "");
}


int stm_display_device_create(stm_device_configuration_t *device_cfg, stm_display_device_h *device)
{
  struct stm_display_device_s *public_device = 0;
  char            DeviceTag[STM_REGISTRY_MAX_TAG_SIZE];
  CDisplayDevice *pDev = NULL;
  uint32_t        id;

  if(!CHECK_ADDRESS(device_cfg))
    return -EFAULT;

  id = device_cfg->id;

  if(id>=MAX_DEVICES_NBR)
    return -ENODEV;

  if(!CHECK_ADDRESS(device))
    return -EFAULT;

  if(display_devices[id] != 0)
  {
    TRC( TRC_ID_ERROR, "Dev %d already created!", id );
    return -EFAULT;
  }

  TRCIN(TRC_ID_MAIN_INFO, "");

  if((pDev = AnonymousCreateDevice(device_cfg)) == 0)
  {
    TRC( TRC_ID_MAIN_INFO, "create device object %d failed", id );
    goto error_exit;
  }

  if(!pDev->Create())
  {
    TRC( TRC_ID_ERROR, "failed to complete device %d creation", id );
    goto error_exit;
  }

  display_devices[id] = pDev;

  /* Prevent STKPI calls while we finish the creation of the device */
  pDev->StkpiLock(__PRETTY_FUNCTION__);

  public_device = new struct stm_display_device_s;
  if(!public_device)
  {
    TRC( TRC_ID_ERROR, "Failed to allocate device handle structure!");
    pDev->StkpiUnlock(__PRETTY_FUNCTION__);
    return -ENOMEM;
  }

  public_device->handle = pDev;
  stm_coredisplay_magic_set(public_device, VALID_DEVICE_HANDLE);

  // For stm_display_device_create(), UseCount is NOT incremented

  *device = public_device;

  num_devices++;

  TRC(TRC_ID_API_DEVICE, "dev : %u, id : %u", pDev->GetID(), id);

  /*
   * Add the Device instance to the registry.
   * Create one single instance per device as we are providing the
   * same public device handle.
   */
  vibe_os_snprintf (DeviceTag, sizeof(DeviceTag), "display_device%d-%p",
                      id, (*device));
  if(stm_registry_add_instance (STM_REGISTRY_INSTANCES,
                               (stm_object_h) pDev,
                                DeviceTag,
                               (stm_object_h)(*device)) != 0)
  {
    TRC( TRC_ID_ERROR, "Cannot register %s device instance (%p)", DeviceTag, (*device) );
  }

  pDev->StkpiUnlock(__PRETTY_FUNCTION__);

  TRCOUT(TRC_ID_MAIN_INFO, "");

  return 0;

error_exit:
  delete pDev;

  TRCOUT(TRC_ID_MAIN_INFO, "");

  return -ENODEV;
}


int stm_display_open_device(uint32_t id, stm_display_device_h *device)
{
  struct stm_display_device_s *public_device = 0;
  CDisplayDevice *pDev;

  if(id>=MAX_DEVICES_NBR)
    return -ENODEV;

  if(!CHECK_ADDRESS(device))
    return -EFAULT;

  pDev = display_devices[id];
  if(pDev == 0)
  {
    // This error is normal when someone iterates to find how many devices exist
    return -ENODEV;
  }

  TRCIN(TRC_ID_MAIN_INFO, "");

  pDev->StkpiLock(__PRETTY_FUNCTION__);

  public_device = new struct stm_display_device_s;
  if(!public_device)
  {
    TRC( TRC_ID_ERROR, "Failed to allocate device handle structure!");
    pDev->StkpiUnlock(__PRETTY_FUNCTION__);
    return -ENOMEM;
  }

  public_device->handle = pDev;
  stm_coredisplay_magic_set(public_device, VALID_DEVICE_HANDLE);

  pDev->IncrementUseCount();

  *device = public_device;

  TRC(TRC_ID_API_DEVICE, "dev : %u, id : %u, use_count : %d", pDev->GetID(), id, pDev->GetUseCount() );
  TRC(TRC_ID_HANDLE_OPEN_CLOSE, "dev %d: 0x%p. UseCount: %d", id, *device, pDev->GetUseCount());

  pDev->StkpiUnlock(__PRETTY_FUNCTION__);

  /* DPM : Get device (*/
  pDev->PmGetDevice();

  TRCOUT(TRC_ID_MAIN_INFO, "");

  return 0;
}


int stm_display_device_freeze(stm_display_device_h dev)
{
  CDisplayDevice* pDev = NULL;
  int res=0;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  TRC(TRC_ID_MAIN_INFO, "");

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());
  res=pDev->Freeze();

  STKPI_UNLOCK(pDev);

  return res;
}


int stm_display_device_suspend(stm_display_device_h dev)
{
  CDisplayDevice* pDev = NULL;
  int res=0;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  TRC(TRC_ID_MAIN_INFO, "");

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());
  res=pDev->Suspend();

  STKPI_UNLOCK(pDev);

  return res;
}

int stm_display_device_resume(stm_display_device_h dev)
{
  CDisplayDevice* pDev = NULL;
  int res=0;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  TRC(TRC_ID_MAIN_INFO, "");

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());
  res=pDev->Resume();

  STKPI_UNLOCK(pDev);

  return res;
}

int stm_display_device_shutting_down(stm_display_device_h dev)
{
  CDisplayDevice* pDev = NULL;
  int res=0;

  if(!IS_HANDLE_VALID(dev, VALID_DEVICE_HANDLE))
    return -EINVAL;

  TRC(TRC_ID_MAIN_INFO, "");

  pDev = dev->handle;

  STKPI_LOCK(pDev);

  TRC(TRC_ID_API_DEVICE, "dev : %u", pDev->GetID());

  pDev->PmSetState(DEVICE_SHUTTING_DOWN);

  STKPI_UNLOCK(pDev);

  return res;
}

} // extern "C"
