/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2011-2014 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2014-05-30
***************************************************************************/

#include <stm_display.h>


#include <vibe_os.h>
#include <vibe_debug.h>
#include <display_device_priv.h>

#include "QueueBufferInterface.h"

CQueueBufferInterface::CQueueBufferInterface ( uint32_t interfaceID, CDisplaySource * pSource )
    : CSourceInterface(interfaceID, pSource)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  TRC( TRC_ID_MAIN_INFO, "Create Queue Buffer Interface 0x%p with Id = %d", this, interfaceID );

  m_pDisplayDevice = pSource->GetParentDevice();

  m_lock            = 0;
  m_listener                = 0;
  m_listener_user_data      = 0;

  m_isOutputModeChanged     = false;
  m_areConnectionsChanged   = false;

  vibe_os_zero_memory(&m_Statistics, sizeof(m_Statistics));

  TRC( TRC_ID_MAIN_INFO, "Created Queue Buffer Interface  source = 0x%p", this );

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


CQueueBufferInterface::~CQueueBufferInterface(void)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );

  TRC( TRC_ID_MAIN_INFO, "Destroying QueueBufferInterface 0x%p", this );
  vibe_os_delete_resource_lock(m_lock);
  m_lock = 0;

  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


bool CQueueBufferInterface::Create(void)
{
  CSourceInterface * pSI = (CSourceInterface *)this;
  CDisplaySource   * pDS = GetParentSource();

  TRCIN( TRC_ID_MAIN_INFO, "" );

  TRC( TRC_ID_MAIN_INFO, "Creating QueueBufferInterface 0x%p",this );

  m_lock = vibe_os_create_resource_lock();
  if (!m_lock)
  {
    TRC( TRC_ID_ERROR, "failed to allocate resource lock" );
    return false;
  }

  if (!(pDS->RegisterInterface(pSI)))
  {
    TRC( TRC_ID_ERROR, "failed to register interface 0x%p within source 0x%p", pSI, pDS );
    return false;
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );

  return true;
}

uint32_t CQueueBufferInterface::SetControl(stm_display_source_ctrl_t ctrl, uint32_t ctrl_val)
{
  // Default, return failure for an unsupported control
  return STM_SRC_NO_CTRL;
}

uint32_t CQueueBufferInterface::GetControl(stm_display_source_ctrl_t ctrl, uint32_t *ctrl_val) const
{
  // Default, return failure for an unsupported control
  return STM_SRC_NO_CTRL;
}

TuningResults CQueueBufferInterface::SetTuning( uint16_t service, void *inputList, uint32_t inputListSize, void *outputList, uint32_t outputListSize)
{
    TuningResults res = TUNING_SERVICE_NOT_SUPPORTED;
    tuning_service_type_t  serviceType = (tuning_service_type_t)service;

    switch(serviceType)
    {
        case RESET_STATISTICS:
        {
            vibe_os_zero_memory(&m_Statistics, sizeof(m_Statistics));
            res = TUNING_OK;
            break;
        }
        default:
            break;
    }
    return res;
}

bool CQueueBufferInterface::RegisterStatistics(void)
{
  char Tag[STM_REGISTRY_MAX_TAG_SIZE];

  vibe_os_zero_memory(&Tag, STM_REGISTRY_MAX_TAG_SIZE);
  vibe_os_snprintf (Tag, sizeof(Tag), "Stat_PicturesOwned");
  if(stm_registry_add_attribute((stm_object_h)this, Tag, STM_REGISTRY_UINT32, &m_Statistics.PicOwned, sizeof(m_Statistics.PicOwned))!= 0)
  {
    TRC( TRC_ID_ERROR, "Cannot register '%s' attribute (%d)", Tag, m_interfaceID );
    return false;
  }
  else
  {
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' object (%d)", Tag, m_interfaceID );
  }

  vibe_os_zero_memory(&Tag, STM_REGISTRY_MAX_TAG_SIZE);
  vibe_os_snprintf (Tag, sizeof(Tag), "Stat_PicturesReleased");
  if(stm_registry_add_attribute((stm_object_h)this, Tag, STM_REGISTRY_UINT32, &m_Statistics.PicReleased, sizeof(m_Statistics.PicReleased))!= 0)
  {
    TRC( TRC_ID_ERROR, "Cannot register '%s' attribute (%d)", Tag, m_interfaceID );
    return false;
  }
  else
  {
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' object (%d)", Tag, m_interfaceID );
  }

  vibe_os_zero_memory(&Tag, STM_REGISTRY_MAX_TAG_SIZE);
  vibe_os_snprintf (Tag, sizeof(Tag), "Stat_PicturesQueued");
  if(stm_registry_add_attribute((stm_object_h)this, Tag, STM_REGISTRY_UINT32, &m_Statistics.PicQueued, sizeof(m_Statistics.PicQueued))!= 0)
  {
    TRC( TRC_ID_ERROR, "Cannot register '%s' attribute (%d)", Tag, m_interfaceID );
    return false;
  }
  else
  {
    TRC( TRC_ID_MAIN_INFO, "Registered '%s' object (%d)", Tag, m_interfaceID );
  }

  return true;
}

bool CQueueBufferInterface::IsEmpty()
{
    TRC( TRC_ID_MAIN_INFO, "returning always true");
    return true;
}


bool CQueueBufferInterface::SetListener(stm_display_source_queue_listener   listener,
                                        void *                              user_data)
{
  m_listener = listener;
  m_listener_user_data = user_data;

  return true;
}

// Function called every times the output mode is changing
void CQueueBufferInterface::SendOutputModeChangedEvent(void)
{
  // Temporary
  m_isOutputModeChanged = true;

  if(m_listener)
  {
    // Notify the listener
    m_listener(SRC_QUEUE_OUTPUT_DISPLAY_MODE_CHANGE, m_listener_user_data);
    TRC( TRC_ID_MAIN_INFO, "Notify : SRC_QUEUE_OUTPUT_DISPLAY_MODE_CHANGE");
  }
}

// Function called every times the connection between THIS source and a plane or output changed
void CQueueBufferInterface::SendConnectionsChangedEvent(void)
{
  if(m_listener)
  {
    // Notify the listener
    m_listener(SRC_QUEUE_CONNECTION_CHANGE, m_listener_user_data);
    TRC( TRC_ID_MAIN_INFO, "Notify : SRC_QUEUE_CONNECTION_CHANGE");

    // Temporary
    m_areConnectionsChanged = true;
  }
}

////////////////////////////////////////////////////////////////////////////////
// C Display source interface
//

extern "C" {

int stm_display_source_queue_lock(stm_display_source_queue_h queue, ...)
{
  CQueueBufferInterface * pSQ = NULL;
  int res;

  if(!IS_HANDLE_VALID(queue, VALID_SRC_INTERFACE_HANDLE))
    return -EINVAL;

  pSQ = queue->handle;

  if(IS_DEVICE_SUSPENDED(queue->pDev))
    return -EAGAIN;

  STKPI_LOCK(queue->pDev);

  TRC(TRC_ID_API_SOURCE_QUEUE, "Queue locked by 0x%p", queue);
  switch ( pSQ->LockUse(queue) )
  {
    case QBI_OK:
      res = 0;
      break;
    default:
      res = -ENOLCK;
  }


  STKPI_UNLOCK(queue->pDev);

  return res;
}


int stm_display_source_queue_unlock(stm_display_source_queue_h queue)
{
  CQueueBufferInterface * pSQ = NULL;
  int res = 0;

  if(!IS_HANDLE_VALID(queue, VALID_SRC_INTERFACE_HANDLE))
    return -EINVAL;

  pSQ = queue->handle;

  if(IS_DEVICE_SUSPENDED(queue->pDev))
    return -EAGAIN;

  STKPI_LOCK(queue->pDev);

  switch ( pSQ->Unlock(queue) )
  {
    case QBI_OK:
      res = 0;
      break;
    default:
      res = -ENOLCK;
  }

  TRC(TRC_ID_API_SOURCE_QUEUE, "Queue unlocked by 0x%p", queue);

  STKPI_UNLOCK(queue->pDev);

  return res;
}


int stm_display_source_queue_buffer(stm_display_source_queue_h queue, const stm_display_buffer_t *pBuffer)
{
  CQueueBufferInterface * pSQ = NULL;
  int res;

  if(!IS_HANDLE_VALID(queue, VALID_SRC_INTERFACE_HANDLE))
    return -EINVAL;

  pSQ = queue->handle;

  if(IS_DEVICE_SUSPENDED(queue->pDev))
    return -EAGAIN;

  if(!CHECK_ADDRESS(pBuffer))
    return -EFAULT;

  STKPI_LOCK(queue->pDev);

  TRC(TRC_ID_QUEUE_BUFFER, "hdl 0x%p on SrcQueue %d", queue, pSQ->GetID() );

  TRC(TRC_ID_QUEUE_BUFFER, "src flags : %x, x : %d, y : %d, width : %u, height : %u, presentation_time : %lld, PTS : %lld",
      pBuffer->src.flags,
      pBuffer->src.visible_area.x,
      pBuffer->src.visible_area.y,
      pBuffer->src.visible_area.width,
      pBuffer->src.visible_area.height,
      pBuffer->info.presentation_time,
      pBuffer->info.PTS);
  switch ( pSQ->QueueBuffer(pBuffer, queue) )
  {
    case QBI_OK:
      res = 0;
      break;
    default:
      res = -ENOLCK;
  }

  STKPI_UNLOCK(queue->pDev);

  return res;
}


int stm_display_source_queue_flush_common(stm_display_source_queue_h queue, flush_requested_t requestedFlush)
{
  CQueueBufferInterface * pSQ            = NULL;
  int                     res;
  int                     wait_flush_completion = true;

  if(!IS_HANDLE_VALID(queue, VALID_SRC_INTERFACE_HANDLE))
    return -EINVAL;

  pSQ = queue->handle;

  if(IS_DEVICE_SUSPENDED(queue->pDev))
    return -EAGAIN;

  STKPI_LOCK(queue->pDev);

  TRC(TRC_ID_API_SOURCE_QUEUE, "hdl 0x%p on SrcQueue %d with requestedFlush=%d", queue, pSQ->GetID(), requestedFlush);

  // Start the flush request:
  switch ( pSQ->FlushStart(requestedFlush, queue) )
  {
    case QBI_FLUSH_NOTHING_TO_DO:
      wait_flush_completion = false;
      res = 0;
      break;
    case QBI_OK:
      res = 0;
      break;
    default:
      res = -ENOLCK;
  }

  STKPI_UNLOCK(queue->pDev);

  // Handle error case of FlushStart
  if ((res != 0) || (wait_flush_completion == false))
  {
    return res;
  }

  // Wait flush completion:
  switch ( pSQ->WaitFlushCompletion(requestedFlush, queue) )
  {
    case QBI_OK:
      res = 0;
      break;
    case QBI_RESTARTSYS:
      res = -ERESTARTSYS;
      break;
    default:
      res = -ENOLCK;
  }

  return res;
}

int stm_display_source_queue_flush(stm_display_source_queue_h queue, bool flush_buffers_on_display)
{
  int               res;
  flush_requested_t requestedFlush = (flush_buffers_on_display ? FLUSH_ALL : FLUSH_PARTIAL);

  res = stm_display_source_queue_flush_common(queue, requestedFlush);

  return res;
}

int stm_display_source_queue_flush_with_copy(stm_display_source_queue_h queue)
{
  int                     res;
  flush_requested_t       requestedFlush = FLUSH_PARTIAL_WITH_COPY;

  res = stm_display_source_queue_flush_common(queue, requestedFlush);

  return res;
}

int stm_display_source_queue_get_pixel_formats(stm_display_source_queue_h queue, stm_pixel_format_t *formats, uint32_t max_formats)
{
  CQueueBufferInterface * pSQ = NULL;
  int n, i;
  const stm_pixel_format_t *buffer_formats;

  if(!IS_HANDLE_VALID(queue, VALID_SRC_INTERFACE_HANDLE))
    return -EINVAL;

  pSQ = queue->handle;

  if(IS_DEVICE_SUSPENDED(queue->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  if(!CHECK_ADDRESS(formats))
    return -EFAULT;

  STKPI_LOCK(queue->pDev);

  TRC(TRC_ID_API_SOURCE_QUEUE, "Queue 0x%p", queue);
  n = pSQ->GetBufferFormats(&buffer_formats);

  // copy the formats in the specified array
  n = MIN((uint32_t)n, max_formats);
  for(i=0; i<n; i++)
    formats[n]=buffer_formats[n];

  STKPI_UNLOCK(queue->pDev);

  return n;
}

int stm_display_source_queue_set_listener(stm_display_source_queue_h         queue,
                                          stm_display_source_queue_listener  listener,
                                          void *                             user_data)
{
  CQueueBufferInterface * pSQ = NULL;
  int res;

  if(!IS_HANDLE_VALID(queue, VALID_SRC_INTERFACE_HANDLE))
    return -EINVAL;

  pSQ = queue->handle;

  if(IS_DEVICE_SUSPENDED(queue->pDev))
    return -EAGAIN;

  STKPI_LOCK(queue->pDev);

  TRC(TRC_ID_API_SOURCE_QUEUE, "hdl 0x%p on SrcQueue %d set_listener", queue, pSQ->GetID() );
  res = pSQ->SetListener(listener, user_data) ? 0 : -ENOLCK;

  STKPI_UNLOCK(queue->pDev);

  return res;
}
int stm_display_source_queue_release(stm_display_source_queue_h queue)
{
  CQueueBufferInterface * pSQ = NULL;
  int res;

  if(!IS_HANDLE_VALID(queue, VALID_SRC_INTERFACE_HANDLE))
    return -EINVAL;

  pSQ = queue->handle;

  if(IS_DEVICE_SUSPENDED(queue->pDev))
    return -EAGAIN;

  stm_coredisplay_magic_clear(queue);

  STKPI_LOCK(queue->pDev);

  TRC(TRC_ID_API_SOURCE_QUEUE, "Queue 0x%p", queue);
  switch ( pSQ->Release(queue) )
  {
    case QBI_OK :
      res = 0;
      break;
    default:
      res = -ENOLCK;
  }

  STKPI_UNLOCK(queue->pDev);

  /*
   * Remove Source Interface instance from stm_registry database.
   */
  if(stm_registry_remove_object(queue) < 0)
    TRC( TRC_ID_ERROR, "failed to remove queue instance from the registry");

  delete queue;

  return res;
}

} // extern "C"
