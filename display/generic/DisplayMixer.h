/***********************************************************************
 *
 * File: display/generic/DisplayMixer.h
 * Copyright (c) 2009 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _DISPLAY_MIXER_H
#define _DISPLAY_MIXER_H

#include "DisplayPlane.h"
#include <display/ip/mixer/GenericGammaDefs.h>

/*! \file DisplayMixer.h
 *  \brief This is a pure virtual interface to a hardware mixer
 *
 *  Actually there is only one class that subclasses this, CGammaMixer, but
 *  providing the interface makes it easier to implement some of the
 *  more abstract output implementation classes without having to refer to the
 *  hardware specific code. I suppose at some point in the future we could have
 *  another hardware implementation.
 */
class CDisplayDevice;
class CDisplayPlane;

typedef enum
{
  /*
   * To be interpreted as an or'd bit field, hence the explicit values
   */
  STM_MA_NONE     = 0,
  STM_MA_GRAPHICS = 1,
  STM_MA_VIDEO    = 2,
  STM_MA_ALL      = 3
} stm_mixer_activation_t;

typedef enum
{
  /*
   * ST hardware planes, the defines deliberately match the plane enable bit in
   * the hardware compositor mixer control.
   *
   * Note that MIXER_ID_VID3 and MIXER_ID_CUR don't actually conflict.
   * MIXER_ID_CUR  is used for Cannes1 and Cannes2 that don't   have MIXER_ID_VID3
   * MIXER_ID_VID3 is used for Cannes2.5           that doesn't have MIXER_ID_CUR
   */
  MIXER_ID_NONE   = 0,         /*!< No active planes                            */
  MIXER_ID_BKG    = (1L<<0),   /*!< Mixer background colour                     */
  MIXER_ID_VID1   = (1L<<MIX_CRB_VID1),   /*!< Video plane                                 */
  MIXER_ID_VID2   = (1L<<MIX_CRB_VID2),   /*!< Video plane                                 */
  MIXER_ID_GDP1   = (1L<<MIX_CRB_GDP1),   /*!< Graphics plane                              */
  MIXER_ID_GDP2   = (1L<<MIX_CRB_GDP2),   /*!< Graphics plane                              */
  MIXER_ID_GDP3   = (1L<<MIX_CRB_GDP3),   /*!< Graphics plane                              */
  MIXER_ID_GDP4   = (1L<<MIX_CRB_GDP4),   /*!< Graphics plane                              */
  MIXER_ID_GDP5   = (1L<<MIX_CRB_GDP5),   /*!< Graphics plane                              */
  MIXER_ID_GDP6   = (1L<<MIX_CRB_GDP6),   /*!< Graphics plane                              */
  MIXER_ID_GDP7   = (1L<<MIX_CRB_GDP7),   /*!< Graphics plane (Lite)                       */
  MIXER_ID_GDP8   = (1L<<MIX_CRB_GDP8),   /*!< Graphics plane (Lite)                       */

  MIXER_ID_VID3   = (1L<<MIX_CRB_VID3),   /*!< Video plane                                 */
  MIXER_ID_CUR    = (1L<<9),              /*!< Cursor plane                                */

  MIXER_ID_VBI    = (1L<<16),             /*!< A virtual plane linked to a GDP for VBI waveforms */
} stm_mixer_id_t;

#define MIXER_ID_MASK (MIXER_ID_BKG|MIXER_ID_VID1| MIXER_ID_VID2|MIXER_ID_GDP1| MIXER_ID_GDP2|MIXER_ID_GDP3|MIXER_ID_GDP4|MIXER_ID_GDP5|MIXER_ID_GDP6|MIXER_ID_GDP7|MIXER_ID_GDP8|MIXER_ID_VID3)


class CDisplayMixer
{
public:
  CDisplayMixer(void) {}
  virtual ~CDisplayMixer() {};

  virtual bool Start(const stm_display_mode_t *) = 0;
  virtual void Stop(void) = 0;

  virtual void Suspend(void) = 0;
  virtual void Resume(void) = 0;

  virtual bool PlaneValid(const CDisplayPlane *) const = 0;

  virtual uint32_t GetCapabilities(void) const = 0;
  virtual uint32_t SetControl(stm_output_control_t, uint32_t newVal) = 0;
  virtual uint32_t GetControl(stm_output_control_t, uint32_t *val) const = 0;

  virtual bool EnablePlane(const CDisplayPlane *, stm_mixer_activation_t act = STM_MA_NONE) = 0;
  virtual bool DisablePlane(const CDisplayPlane *) = 0;

  virtual bool HasEnabledPlanes(void) const = 0;

  virtual bool SetPlaneDepth(const CDisplayPlane *, int depth, bool activate) = 0;
  virtual bool GetPlaneDepth(const CDisplayPlane *, int *depth) const = 0;

  virtual bool UpdateFromOutput(COutput* pOutput, stm_plane_update_reason_t update_reason) = 0;

  virtual bool SetMixerMap(const CDisplayPlane* plane, uint32_t mixerIDs) = 0;

  virtual bool GetLastVsyncStatus(uint32_t * status) { *status = 0; return false; }

  virtual const char *GetName(void) const = 0;

private:
  CDisplayMixer(const CDisplayMixer&);
  CDisplayMixer& operator=(const CDisplayMixer&);
};

#endif /* _DISPLAY_MIXER_H */
