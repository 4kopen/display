/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2000-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-09-30
***************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display_device_priv.h>

#include "Output.h"
#include "DisplayDevice.h"
#include "DisplaySource.h"
#include "SourceInterface.h"

#include "DisplayQueue.h"
#include "DisplayNode.h"

#include "DisplayPlane.h"

#include <display/ip/buffercopy/buffercopy.h>
#include <display/ip/queuebufferinterface/PureSwQueueBufferInterface.h>

#define COLORSPACE_FROM_FLAGS(f) ((f) & STM_BUFFER_SRC_COLORSPACE_BT2020 ? STM_COLORSPACE_BT2020    : \
                                  (f) & STM_BUFFER_SRC_COLORSPACE_709    ? STM_YCBCR_COLORSPACE_709 : \
                                                                           STM_YCBCR_COLORSPACE_601)

static void PauseBufferCopyWorkFunction(void * data);
struct PauseCopyBufParams {
        CDisplayPlane * plane;
        CDisplayDevice * device;   /* for VSyncLock() */
        CDisplayNode SrcNode;
        CDisplayNode** ppDestNode; /* pointer to m_pPauseNode */
        struct CopiedPictBuffers * pPauseCopiedPictBuffers;
};


CDisplayPlane::CDisplayPlane(const char           *name,
                             uint32_t              planeID,
                             const CDisplayDevice *pDev,
                             const stm_plane_capabilities_t caps,
                             const char *pixClockName,
                             const char *procClockName)
{
  // Initialize plane name at very beginning of object construction
  // so that subsequent traces print out plane name
  m_name = name;

  PLANE_TRCIN( TRC_ID_MAIN_INFO, "" );

  m_planeID        = planeID;
  m_pDisplayDevice = (CDisplayDevice*)pDev;
  m_capabilities   = caps;

  if (pixClockName)
  {
    vibe_os_snprintf (m_pixelClockName, sizeof(m_pixelClockName), "%s",
                      pixClockName);
  }
  else
  {
    m_pixelClockName[0]='\0';
  }

  if (procClockName)
  {
    vibe_os_snprintf (m_procClockName, sizeof(m_procClockName), "%s",
                      procClockName);
  }
  else
  {
    m_procClockName[0]='\0';
  }

  m_ulTimingID = 0;
  m_pOutput    = 0;
  m_pSource    = 0;

  m_status       = 0;
  m_hasADeinterlacer = false;
  m_bIsSuspended = false;
  m_bIsFrozen    = false;

  m_ContextChanged = false;

  m_fixedpointONE = 1; // n.0 i.e. integer, overridden by subclass
  m_ulMaxHSrcInc  = 1;
  m_ulMinHSrcInc  = 1;
  m_ulMaxVSrcInc  = 1;
  m_ulMinVSrcInc  = 1;
  m_ulMaxLineStep = 1;

  m_pSurfaceFormats = 0;
  m_nFormats        = 0;

  m_pFeatures = 0;
  m_nFeatures = 0;
  m_planeLatency = 0;

  m_AspectRatioConversionMode = ASPECT_RATIO_CONV_LETTER_BOX;
  m_InputWindowMode  = AUTO_MODE;
  m_OutputWindowMode = AUTO_MODE;
  m_InputWindowValue.x       = 0;
  m_InputWindowValue.y       = 0;
  m_InputWindowValue.width   = 0;
  m_InputWindowValue.height  = 0;
  m_OutputWindowValue.x      = 0;
  m_OutputWindowValue.y      = 0;
  m_OutputWindowValue.width  = 0;
  m_OutputWindowValue.height = 0;

  ResetComputedInputOutputWindowValues();

  m_hideRequestedByTheApplication = false;
  m_eHideMode                     = PLANE_HIDE_BY_MASKING;
  m_currentState                  = STM_PLANE_HW_AND_CLOCK_STOPPED;
  m_clkDeactivationCountDown      = 0;
  m_hwDeactivationCountDown       = 0;
  m_isProcClkEnabled              = false;
  m_isPixelClkEnabled             = false;
  m_areClocksAlwaysEnabled        = false;

  m_lastRequestedDepth = 0;
  m_updateDepth = false;

  vibe_os_zero_memory(&m_Statistics, sizeof(m_Statistics));

  ResetDisplayInfo();

  m_Transparency = STM_PLANE_TRANSPARENCY_OPAQUE;
  m_TransparencyState = CONTROL_OFF;

  vibe_os_zero_memory( &m_rescale_caps, sizeof( m_rescale_caps ));
  m_numConnectedOutputs = 0;

  m_hasAScaler = true;
  m_hasIgnoreOnMixer = true;
  m_ForceOnMixerMask = 0x0;

  vibe_os_zero_memory(m_ControlStatusArray, sizeof(m_ControlStatusArray));

  ResetTriplet(&m_picturesPreparedForNextVSync);
  ResetTriplet(&m_picturesUsedByHw);

  m_pixelClock.clk = 0;
  m_pixelClock.enabled = false;
  m_procClock.clk = 0;
  m_procClock.enabled =false;

  m_PauseState = PAUSESTATE_DISABLED;
  m_pPauseCopyBufParams = NULL;
  m_pPauseNode = NULL;
  m_PauseWorkQueue = NULL;
  m_bPauseDisplayed = false;
  m_PauseMissingNodeCount = 0;
  vibe_os_zero_memory( &m_ColorKeyConfig, sizeof( m_ColorKeyConfig ));
  m_ColorKeyConfig.flags  = (SCKCF_ENABLE | SCKCF_FORMAT | SCKCF_R_INFO | SCKCF_G_INFO | SCKCF_B_INFO);
  m_ColorKeyConfig.format = SCKCVF_RGB;
  m_ColorKeyConfig.r_info = SCKCCM_DISABLED;
  m_ColorKeyConfig.g_info = SCKCCM_DISABLED;
  m_ColorKeyConfig.b_info = SCKCCM_DISABLED;
  m_ColorKeyState         = CONTROL_OFF;
  m_pPauseCopyThreadDesc  = NULL;
  /*
   * By default we assume that the plane does have its own Pixel Clock.
   */
  m_hasADedicatedPixelClock = true;

  vibe_os_zero_memory(&m_prevBufferDesc, sizeof(m_prevBufferDesc));

  /*
   * When the plane's HW gets masked, this value indicates after how
   * many VSync periods the Plane's hardware will be also disabled.
   *
   * Default value is 1 Vsync duration.
   */
  m_HwDeactivationVsyncCount = 1;

  /*
   * When the plane's HW gets disabled, this value indicates after how
   * many VSync periods the Plane's clocks will be also disabled.
   *
   * Default value is 1 Vsync duration.
   */
  m_ClockDeactivationVsyncCount = 1;

  m_bIsMasterVideoPlane         = false;
  m_updateHDROutFormat          = false;
  m_ulPrevSinkHDRFormats        = 0xFFFFFFFF;
  m_PrevNodeHdr_Fmt.eotf_type   = (stm_eotf_type_t)0xff;
  PLANE_TRCOUT( TRC_ID_MAIN_INFO, "" );
}


CDisplayPlane::~CDisplayPlane(void)
{
  CNode* p_Node;

  PLANE_TRCIN( TRC_ID_MAIN_INFO, "" );

  // Release all the queued controls
  p_Node = m_ControlQueue.GetFirstNode();
  while (p_Node != 0)
  {
    if(!m_ControlQueue.ReleaseDisplayNode(p_Node))
    {
      PLANE_TRC( TRC_ID_ERROR, "Error while releasing node 0x%p from ControlQueue!", p_Node);
    }
    p_Node = m_ControlQueue.GetFirstNode();
  }

  // Release all the queued listeners
  p_Node = m_ListenerQueue.GetFirstNode();
  while (p_Node != 0)
  {
    if(!m_ListenerQueue.ReleaseDisplayNode(p_Node))
    {
      PLANE_TRC( TRC_ID_ERROR, "Error while releasing node 0x%p from ListenerQueue!", p_Node);
    }
    p_Node = m_ListenerQueue.GetFirstNode();
  }

  if (m_PauseWorkQueue)
  {
      vibe_os_destroy_work_queue(m_PauseWorkQueue);
      m_PauseWorkQueue = NULL;
  }
  if (m_pPauseCopyBufParams)
  {
      vibe_os_delete_workerdata(m_pPauseCopyBufParams);
      m_pPauseCopyBufParams = NULL;
  }
  if (m_pPauseNode)
  {
      delete m_pPauseNode;
      m_pPauseNode = NULL;
  }

  if (m_pixelClockName[0] != '\0')
  {
    vibe_os_clk_put(&m_pixelClock);
  }

  if (m_procClockName[0] != '\0')
  {
    vibe_os_clk_put(&m_procClock);
  }

  PLANE_TRCOUT( TRC_ID_MAIN_INFO, "" );
}


void CDisplayPlane::SetScalingCapabilities(stm_plane_rescale_caps_t *caps) const
{
  PLANE_TRCIN( TRC_ID_MAIN_INFO, "" );

  /*
   * We do a simplification of the numbers if the maximum sample rate step
   * is >= 1 and is an integer. But if it isn't we just keep it all in the
   * fixed point format. The scaling value is 1/sample step.
   */
  if((m_ulMaxHSrcInc & (m_fixedpointONE-1)) != 0)
  {
    caps->minHorizontalRescale.numerator   = m_fixedpointONE;
    caps->minHorizontalRescale.denominator = m_ulMaxHSrcInc;
  }
  else
  {
    caps->minHorizontalRescale.numerator   = 1;
    caps->minHorizontalRescale.denominator = m_ulMaxHSrcInc/m_fixedpointONE;
  }

  if((m_ulMaxVSrcInc & (m_fixedpointONE-1)) != 0)
  {
    caps->minVerticalRescale.numerator   = m_fixedpointONE;
    caps->minVerticalRescale.denominator = m_ulMaxVSrcInc * m_ulMaxLineStep;
  }
  else
  {
    caps->minVerticalRescale.numerator   = 1;
    caps->minVerticalRescale.denominator = (m_ulMaxVSrcInc/m_fixedpointONE) * m_ulMaxLineStep;
  }

  /*
   * For the minimum sample rate step we simplify if the step <=1 and that 1/step
   * is an integer. We also assume that m_fixedpointONE*m_fixedpointONE is
   * representable in 32bits, which given the maximum value of fixedpointONE
   * we deal with is 2^13 is fine.
   */
  if((m_ulMinHSrcInc > (uint32_t)m_fixedpointONE)  ||
     (((m_fixedpointONE*m_fixedpointONE)/m_ulMinHSrcInc) & (m_fixedpointONE-1)) != 0)
  {
    caps->maxHorizontalRescale.numerator   = m_fixedpointONE;
    caps->maxHorizontalRescale.denominator = m_ulMinHSrcInc;
  }
  else
  {
    caps->maxHorizontalRescale.numerator   = m_fixedpointONE/m_ulMinHSrcInc;
    caps->maxHorizontalRescale.denominator = 1;
  }

  if((m_ulMinVSrcInc > (uint32_t)m_fixedpointONE)  ||
     (((m_fixedpointONE*m_fixedpointONE)/m_ulMinVSrcInc) & (m_fixedpointONE-1)) != 0)
  {
    caps->maxVerticalRescale.numerator   = m_fixedpointONE;
    caps->maxVerticalRescale.denominator = m_ulMinVSrcInc;
  }
  else
  {
    caps->maxVerticalRescale.numerator   = m_fixedpointONE/m_ulMinVSrcInc;
    caps->maxVerticalRescale.denominator = 1;
  }

  PLANE_TRC( TRC_ID_MAIN_INFO, "horizontal scale %u/%u to %u/%u", caps->minHorizontalRescale.numerator, caps->minHorizontalRescale.denominator, caps->maxHorizontalRescale.numerator, caps->maxHorizontalRescale.denominator );
  PLANE_TRC( TRC_ID_MAIN_INFO, "vertical scale   %u/%u to %u/%u", caps->minVerticalRescale.numerator, caps->minVerticalRescale.denominator, caps->maxVerticalRescale.numerator, caps->maxVerticalRescale.denominator );

  PLANE_TRCOUT( TRC_ID_MAIN_INFO, "" );
}


bool CDisplayPlane::Create(void)
{
  int  ret=0;
  char queue_name[QUEUE_NAME_MAX_LENGTH];

  PLANE_TRCIN( TRC_ID_MAIN_INFO, "" );

  // Create the control queue
  vibe_os_snprintf (queue_name, sizeof(queue_name), "%s_controlQ", m_name);
  if (!m_ControlQueue.CreateQueue(queue_name))
  {
    PLANE_TRC(TRC_ID_ERROR, "Cannot create %s", queue_name);
  }

  // Create the listener queue
  vibe_os_snprintf (queue_name, sizeof(queue_name), "%s_listenerQ", m_name);
  if (!m_ListenerQueue.CreateQueue(queue_name))
  {
    PLANE_TRC(TRC_ID_ERROR, "Cannot create %s", queue_name);
  }

  // Get the clocks
  if (m_pixelClockName[0] != '\0')
  {
     ret = vibe_os_clk_get(m_pixelClockName, &m_pixelClock);
     if (ret)
     {
       PLANE_TRC(TRC_ID_ERROR, "Pixel clock %s get failed", m_pixelClockName );
       return false;
     }
  }

  if (m_procClockName[0] != '\0')
  {
    ret = vibe_os_clk_get(m_procClockName, &m_procClock);
    if (ret)
    {
      vibe_os_clk_put(&m_pixelClock);
      PLANE_TRC(TRC_ID_ERROR, "Proc clock %s get failed", m_procClockName );
      return false;
    }
  }

  PLANE_TRCOUT( TRC_ID_MAIN_INFO, "" );
  return true;
}


DisplayPlaneResults CDisplayPlane::ConnectToOutput(COutput* pOutput)
{
  char     ConnectionTag[STM_REGISTRY_MAX_TAG_SIZE];
  uint32_t updateStatus = 0;

  if(!pOutput)
  {
    PLANE_TRC(TRC_ID_ERROR, "Invalid output 0x%p!", pOutput );
    return STM_PLANE_CANNOT_CONNECT;
  }

  if((m_pOutput != NULL) && (m_numConnectedOutputs == 0))
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Trying to connect Plane while plane is already connected to output %s!", m_pOutput->GetName() );
    /////////// Block the VSync ThreadedIRQ while we disable the plane //////////
    m_pDisplayDevice->VSyncLock();
    DisablePlane();
    m_pOutput = 0;
    m_pDisplayDevice->VSyncUnlock();
    /////////////////////////////////////////////////////////////////////////////
  }

  PLANE_TRC( TRC_ID_MAIN_INFO, "Connect to %s", pOutput->GetName() );

  if(!pOutput->CanShowPlane(this))
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Plane not displayable on output %s", pOutput->GetName() );
    return STM_PLANE_NOT_SUPPORTED;
  }

  if(m_pOutput == pOutput)
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Plane already connected to output %s", pOutput->GetName() );
    return STM_PLANE_ALREADY_CONNECTED;
  }

  if(m_pOutput != NULL)
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Plane already connected to another output: %s", m_pOutput->GetName() );
    return STM_PLANE_CANNOT_CONNECT;
  }

  ////////// Block the VSync ThreadedIRQ while we prepare the plane //////////
  m_pDisplayDevice->VSyncLock();

  // Setting the output pointer will be sufficient to start the
  // process of displaying the plane.
  m_pOutput    = pOutput;
  m_numConnectedOutputs=1;
  m_ulTimingID = pOutput->GetTimingID();
  updateStatus = STM_STATUS_PLANE_CONNECTED_TO_OUTPUT;

  m_status |= updateStatus;
  SendConnectionsChangedEvent();

  vibe_os_snprintf (ConnectionTag, sizeof(ConnectionTag), "connection_to_%s", m_pOutput->GetName());

  if (stm_registry_add_connection((stm_object_h)this, ConnectionTag,(stm_object_h)m_pOutput))
  {
    PLANE_TRC( TRC_ID_ERROR, "Registry : Connection to output %s failed", m_pOutput->GetName() );
  }

  UpdateOuptutInfo(); // Get some info about the output connected to this plane

  // This plane gets connected to a new output. Send an "output mode changed" notification
  SendOutputModeChangedEvent();

  // Restart Semi-Sync clocks for programing final diviser compatible
  // with all clocks belonging to same group. Else, enabling one clock
  // of this group could disable and enable back all enabled clocks
  // of this group causing a black screen
  // Do it only for video clock has there is a specific switch off sequence
  // to respect for GDP plane due to an hardware issue.
  if( isVideoPlane() )
  {
    EnablePixelClock();
    DisablePixelClock();
  }

  m_pDisplayDevice->VSyncUnlock();
  /////////////////////////////////////////////////////////////////////////////

  PLANE_TRC( TRC_ID_MAIN_INFO, "Plane is now connected to output %s", m_pOutput->GetName() );

  return STM_PLANE_OK;
}


void CDisplayPlane::DisconnectFromOutput(COutput* pOutput)
{
  char     ConnectionTag[STM_REGISTRY_MAX_TAG_SIZE];

  if(!pOutput)
  {
    PLANE_TRC(TRC_ID_ERROR, "Invalid output 0x%p!", pOutput );
    return;
  }

  PLANE_TRC( TRC_ID_MAIN_INFO, "Disconnect from %s", pOutput->GetName() );

  // This covers also the case where m_pOutput is null
  if(m_pOutput != pOutput)
  {
    PLANE_TRC(TRC_ID_ERROR, "Plane is NOT connected to Output %s!", pOutput->GetName() );
    return;
  }

  ////////// Block the VSync ThreadedIRQ while we disconnect the plane //////////
  m_pDisplayDevice->VSyncLock();

  vibe_os_snprintf (ConnectionTag, sizeof(ConnectionTag), "connection_to_%s", m_pOutput->GetName());

  if (stm_registry_remove_connection((stm_object_h)this, ConnectionTag))
  {
    PLANE_TRC( TRC_ID_ERROR, "Registry : Disconnection from output %s failed", m_pOutput->GetName() );
  }

  if(m_currentState == STM_PLANE_HW_ENABLED)
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Warning! Request to disconnect plane %s from output %s while plane still active!", GetName(), m_pOutput->GetName() );
    // Ensure that the disabling of the plane is done synchronously on VSync.
    MaskPlane();
  }

  if(m_PauseState != PAUSESTATE_DISABLED)
  {
      PLANE_TRC( TRC_ID_ERROR, "Warning! Request to disconnect plane from output %s while plane is paused!", m_pOutput->GetName() );
      m_PauseState = PAUSESTATE_UNPAUSE_REQUESTED;
  }

  PLANE_TRC( TRC_ID_MAIN_INFO, "Plane is now disconnected from output %s", m_pOutput->GetName() );

  m_numConnectedOutputs=0;
  /*
   * This plane is being disconnected from its output.
   * However, the plane's clocks are not stopped immediately so the plane need to continue to receive
   * the VSync notifications until the HW and the Clocks get fully stopped.
   *
   * "m_ulTimingID" and "m_pOutput" will be reset when the plane reach the state "STM_PLANE_HW_AND_CLOCK_STOPPED"
   */

  m_status &= ~(STM_STATUS_PLANE_CONNECTED_TO_OUTPUT);
  SendConnectionsChangedEvent();

  // UpdateOuptutInfo() will see that this plane is no more connected to an output
  UpdateOuptutInfo();

  m_pDisplayDevice->VSyncUnlock();
  /////////////////////////////////////////////////////////////////////////////
}


bool CDisplayPlane::GetConnectedSourceID(uint32_t *id)
{
  if(m_pSource)
  {
    *id = m_pSource->GetID();
    PLANE_TRC( TRC_ID_UNCLASSIFIED, "Plane %d is connected to source %d", m_planeID, *id  );
    return true;
  }
  else
  {
    PLANE_TRC( TRC_ID_UNCLASSIFIED, "Plane %d is not connected to any source", m_planeID  );
    return false;
  }
}

DisplayPlaneResults CDisplayPlane::ConnectToSourceProtected(CDisplaySource *pSource)
{
  DisplayPlaneResults status;

  ////////// Block the VSync ThreadedIRQ while we connect the plane //////////
  m_pDisplayDevice->VSyncLock();

  status = ConnectToSource(pSource);

  m_pDisplayDevice->VSyncUnlock();
  /////////////////////////////////////////////////////////////////////////////

  return (status);
}

// This function is called by functions where m_pDisplayDevice->VSyncLock() is already taken
DisplayPlaneResults CDisplayPlane::ConnectToSource(CDisplaySource *pSource)
{
  char     ConnectionTag[STM_REGISTRY_MAX_TAG_SIZE];

  PLANE_TRC(TRC_ID_MAIN_INFO, "Requested connection to source %s", pSource?pSource->GetName():"null" );

  // Check that VSyncLock is already taken before accessing to shared variables
  DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

  if ( !pSource )
  {
    PLANE_TRC( TRC_ID_ERROR, "Invalid Source!" );
    return STM_PLANE_INVALID_VALUE;
  }

  if(m_pSource == pSource)
  {
    PLANE_TRC( TRC_ID_ERROR, "Plane already connected to the requested source %s!", m_pSource->GetName() );
    return STM_PLANE_ALREADY_CONNECTED;
  }

  if(!pSource->CanConnectToPlane(this))
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Plane cannot be connected to source %s", pSource->GetName() );
    return STM_PLANE_NOT_SUPPORTED;
  }

  if(m_pSource != 0)
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Plane already connected to source %s. Disconnect it before connecting to the new requested source %s!", m_pSource->GetName(), pSource->GetName() );
    DisconnectFromSource(m_pSource);
    //m_pSource is now null
  }

  if (!pSource->ConnectPlane(this))
  {
    PLANE_TRC( TRC_ID_ERROR, "Connection to source %s failed", pSource->GetName() );
    return STM_PLANE_CANNOT_CONNECT;
  }

  // The source is now considered as connected to this plane. Save the handle of the source which feeds this plane.
  m_pSource = pSource;

  m_status |= STM_STATUS_PLANE_CONNECTED_TO_SOURCE;

  vibe_os_snprintf (ConnectionTag, sizeof(ConnectionTag), "connection_to_%s", pSource->GetName());

  if (stm_registry_add_connection((stm_object_h)this, ConnectionTag,(stm_object_h)m_pSource))
  {
    PLANE_TRC( TRC_ID_ERROR, "Registry : Connection to source %s failed", m_pSource->GetName() );
  }

  m_Statistics.SourceId = m_pSource->GetID();

  PLANE_TRC( TRC_ID_MAIN_INFO, "Plane is now connected to source %s", m_pSource->GetName() );

  // This plane is now connected to a new source. We don't know the history of this source so it is
  // wise to reset all the data saved for each use case.
  m_ContextChanged = true;
  PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new source)" );

  return STM_PLANE_OK;
}


bool CDisplayPlane::DisconnectFromSourceProtected(CDisplaySource *pSource)
{
  bool status;

  ////////// Block the VSync ThreadedIRQ while we disconnect the plane from the source //////////
  m_pDisplayDevice->VSyncLock();

  status = DisconnectFromSource(pSource);

  m_pDisplayDevice->VSyncUnlock();
  /////////////////////////////////////////////////////////////////////////////

  return (status);
}

// This function is called by functions where m_pDisplayDevice->VSyncLock() is already taken
bool CDisplayPlane::DisconnectFromSource(CDisplaySource *pSource)
{
  char     ConnectionTag[STM_REGISTRY_MAX_TAG_SIZE];

  // Check that VSyncLock is already taken before accessing to shared variables
  DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

  PLANE_TRC( TRC_ID_MAIN_INFO, "Requested disconnection from source %s", pSource?pSource->GetName():"null" );

  if( (m_pSource != pSource) || (pSource == 0) )
  {
    PLANE_TRC( TRC_ID_ERROR, "Plane is not connected to source %s", pSource?pSource->GetName():"null" );
    return false;
  }

  if (!pSource->DisconnectPlane(this))
    return false;

  vibe_os_snprintf (ConnectionTag, sizeof(ConnectionTag), "connection_to_%s", m_pSource->GetName());

  if (stm_registry_remove_connection((stm_object_h)this, ConnectionTag))
  {
    PLANE_TRC( TRC_ID_ERROR, "Registry : disconnection from source %s failed", m_pSource->GetName() );
  }

  PLANE_TRC( TRC_ID_MAIN_INFO, "Plane is now disconnected from source %s", m_pSource->GetName() );

  m_Statistics.SourceId = STM_INVALID_SOURCE_ID;
  m_pSource = 0;
  m_status &= ~(STM_STATUS_PLANE_CONNECTED_TO_SOURCE);

  return true;
}

DisplayPlaneResults CDisplayPlane::SetControl(stm_display_plane_control_t ctrl, uint32_t newVal)
{
  DisplayPlaneResults result = STM_PLANE_NOT_SUPPORTED;

  switch(ctrl)
  {
    case PLANE_CTRL_ASPECT_RATIO_CONVERSION_MODE:
      if( newVal > (uint32_t)ASPECT_RATIO_CONV_IGNORE)
      {
        PLANE_TRC( TRC_ID_ERROR, "Invalid PLANE_CTRL_ASPECT_RATIO_CONVERSION_MODE!");
        result = STM_PLANE_INVALID_VALUE;
      }
      else
      {
        m_pDisplayDevice->VSyncLock();
        // The VSync interrupt should be blocked while we change the Aspect Ratio Conversion Mode
        m_AspectRatioConversionMode = (stm_plane_aspect_ratio_conversion_mode_t) newVal;
        m_ContextChanged = true;
        m_pDisplayDevice->VSyncUnlock();

        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new AR conv mode)");
        PLANE_TRC( TRC_ID_MAIN_INFO, "New PLANE_CTRL_ASPECT_RATIO_CONVERSION_MODE=%u",  m_AspectRatioConversionMode );
        result = STM_PLANE_OK;
      }
      break;

    case PLANE_CTRL_INPUT_WINDOW_MODE:
      if( newVal > (uint32_t)AUTO_MODE)
      {
        PLANE_TRC( TRC_ID_ERROR, "Invalid PLANE_CTRL_INPUT_WINDOW_MODE!");
        result = STM_PLANE_INVALID_VALUE;
      }
      /*
       * The plane should be supporting vertical and horizontal scaling
       * as we may have the input window values which doesn't match the
       * output ones. In that case setting input AUTO mode is not
       * supported too.
       */
      else if((newVal == AUTO_MODE) && (!m_hasAScaler))
      {
        PLANE_TRC( TRC_ID_GDP_PLANE, "New PLANE_CTRL_INPUT_WINDOW_MODE mode=NOT SUPPORTED (No Scaler HW support)" );
        result = STM_PLANE_NOT_SUPPORTED;
      }
      else
      {
        m_pDisplayDevice->VSyncLock();
        // The VSync interrupt should be blocked while we change the Input Window Mode
        m_InputWindowMode = (stm_plane_mode_t)newVal;
        if(m_InputWindowMode==AUTO_MODE)
        {
          // Reset to their invalid values when AUTO mode is entered
          ResetComputedInputOutputWindowValues();
        }
        m_ContextChanged = true;
        m_pDisplayDevice->VSyncUnlock();

        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new IW mode)");
        PLANE_TRC( TRC_ID_MAIN_INFO, "New PLANE_CTRL_INPUT_WINDOW_MODE mode=%u",  newVal );
        result = STM_PLANE_OK;
      }
      break;

    case PLANE_CTRL_OUTPUT_WINDOW_MODE:
      if( newVal > (uint32_t)AUTO_MODE)
      {
        PLANE_TRC( TRC_ID_ERROR, "Invalid PLANE_CTRL_OUTPUT_WINDOW_MODE!");
        result = STM_PLANE_INVALID_VALUE;
      }
      /*
       * The plane should be supporting vertical and horizontal scaling
       * as we may have the output window values which doesn't match the
       * input ones. In that case setting output AUTO mode is not
       * supported too.
       */
      else if((newVal == AUTO_MODE) && (!m_hasAScaler))
      {
        PLANE_TRC( TRC_ID_GDP_PLANE, "New PLANE_CTRL_OUTPUT_WINDOW_MODE mode=NOT SUPPORTED (No Scaler HW support)" );
        result = STM_PLANE_NOT_SUPPORTED;
      }
      else
      {
        m_pDisplayDevice->VSyncLock();
        // The VSync interrupt should be blocked while we change the Output Window Mode
        m_OutputWindowMode = (stm_plane_mode_t)newVal;
        m_ContextChanged = true;
        m_pDisplayDevice->VSyncUnlock();

        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new OW mode)");
        PLANE_TRC( TRC_ID_MAIN_INFO, "New PLANE_CTRL_OUTPUT_WINDOW_MODE=%u",  newVal );
        result = STM_PLANE_OK;
      }
      break;
    case PLANE_CTRL_TRANSPARENCY_VALUE:
          if( newVal > STM_PLANE_TRANSPARENCY_OPAQUE)
          {
            PLANE_TRC( TRC_ID_ERROR, "Invalid PLANE_CTRL_TRANSPARENCY_VALUE" );
            result = STM_PLANE_INVALID_VALUE;
          }
          else
          {
            m_pDisplayDevice->VSyncLock();
            // The VSync interrupt should be blocked while we change the Transparency
            m_Transparency = newVal&0xFF;
            m_ContextChanged = true;
            m_pDisplayDevice->VSyncUnlock();

            PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new transparency value)");
            PLANE_TRC( TRC_ID_MAIN_INFO, "New PLANE_CTRL_TRANSPARENCY_VALUE=%u",  m_Transparency );
            result = STM_PLANE_OK;
          }
          break;
    case PLANE_CTRL_TRANSPARENCY_STATE:
        if( (newVal != CONTROL_OFF) && (newVal != CONTROL_ON))
        {
          PLANE_TRC(TRC_ID_MAIN_INFO, "Invalid PLANE_CTRL_TRANSPARENCY_STATE");
          result = STM_PLANE_INVALID_VALUE;
        }
        else
        {
          m_pDisplayDevice->VSyncLock();
          // The VSync interrupt should be blocked while we change the Transparency
          m_TransparencyState = (stm_plane_control_state_t)newVal;
          m_ContextChanged = true;
          m_pDisplayDevice->VSyncUnlock();

          PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new transparency state)");
          PLANE_TRC(TRC_ID_MAIN_INFO, "New PLANE_CTRL_TRANSPARENCY_STATE=%u",
                  m_TransparencyState);
          result = STM_PLANE_OK;
        }
        break;
    case PLANE_CTRL_DEPTH:
        // Do a call to SetDepth() with "activate=false" in order to see if the requested depth is valid
        if(SetDepth(m_pOutput, newVal, false))
        {
            // The requested depth is saved in a variable and will be applied at next VSync.
            m_pDisplayDevice->VSyncLock();
            m_lastRequestedDepth = (int32_t) newVal;
            m_updateDepth = true;
            m_pDisplayDevice->PlaneNeedsPrioHwUpdate(this);
            m_pDisplayDevice->VSyncUnlock();
            result = STM_PLANE_OK;
        }
        else
        {
            PLANE_TRC(TRC_ID_ERROR, "Error! SetDepth failed on output 0x%p", m_pOutput);
            result = STM_PLANE_INVALID_VALUE;
        }
        break;
    case PLANE_CTRL_TEST_DEPTH:
      if(SetDepth(m_pOutput, newVal, false))
      {
        result = STM_PLANE_OK;
      }
      else
      {
        result = STM_PLANE_NOT_SUPPORTED;
        PLANE_TRC(TRC_ID_ERROR, "Error during PLANE_CTRL_TEST_DEPTH on output 0x%p", m_pOutput);
      }
      break;
    case PLANE_CTRL_VISIBILITY:
      if (newVal == PLANE_NOT_VISIBLE)
      {
        PLANE_TRC(TRC_ID_MAIN_INFO, "New PLANE_CTRL_VISIBILITY hide");
        result = HideRequestedByTheApplication()?STM_PLANE_OK:STM_PLANE_INVALID_VALUE;
      }
      else if (newVal == PLANE_VISIBLE)
      {
        PLANE_TRC(TRC_ID_MAIN_INFO, "New PLANE_CTRL_VISIBILITY show");
        result = ShowRequestedByTheApplication()?STM_PLANE_OK:STM_PLANE_INVALID_VALUE;
      }
      else
      {
        PLANE_TRC(TRC_ID_ERROR, "Invalid PLANE_CTRL_VISIBILITY");
        result = STM_PLANE_NOT_SUPPORTED;
      }
      break;
    case PLANE_CTRL_SRC_COLOR_STATE:
      if( (newVal != CONTROL_OFF) && (newVal != CONTROL_ON))
      {
        PLANE_TRC(TRC_ID_MAIN_INFO, "Invalid PLANE_CTRL_SRC_COLOR_STATE");
        result = STM_PLANE_INVALID_VALUE;
      }
      else
      {
        m_pDisplayDevice->VSyncLock();
        // The VSync interrupt should be blocked while we change the color key state
        m_ColorKeyState = (stm_plane_control_state_t)newVal;
        m_ContextChanged = true;
        m_pDisplayDevice->VSyncUnlock();

        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new color key state)");
        PLANE_TRC(TRC_ID_MAIN_INFO, "New PLANE_CTRL_SRC_COLOR_STATE=%u",
                m_ColorKeyState);
        result = STM_PLANE_OK;
      }
      break;
    default:
      break;
  }

  return result;
}

DisplayPlaneResults CDisplayPlane::AddSimpleControlToQueue(stm_display_plane_control_t ctrl, uint32_t newVal)
{
  CControlNode* p_CurrentNode = 0;
  ControlParams ControlParam;

  ControlParam.value = newVal;
  p_CurrentNode =  new CControlNode(ctrl, &ControlParam);
  if (p_CurrentNode == 0)
  {
    return STM_PLANE_NO_MEMORY;
  }

  m_pDisplayDevice->VSyncLock();
  m_ControlQueue.QueueDisplayNode(p_CurrentNode);
  m_pDisplayDevice->VSyncUnlock();

  return STM_PLANE_OK;
}

DisplayPlaneResults CDisplayPlane::AddCompoundControlToQueue(stm_display_plane_control_t ctrl, void * newVal)
{
  CControlNode* p_CurrentNode = 0;
  ControlParams ControlParam;

  switch (ctrl)
  {
    case PLANE_CTRL_CONNECT_TO_SOURCE:
    case PLANE_CTRL_DISCONNECT_FROM_SOURCE:
      ControlParam.display_source = (stm_display_source_h)newVal;
      break;
    default:
      return STM_PLANE_NOT_SUPPORTED;
  }

  p_CurrentNode =  new CControlNode(ctrl, &ControlParam);
  if (p_CurrentNode == 0)
  {
    return STM_PLANE_NO_MEMORY;
  }

  m_pDisplayDevice->VSyncLock();
  m_ControlQueue.QueueDisplayNode(p_CurrentNode);
  m_pDisplayDevice->VSyncUnlock();

  return STM_PLANE_OK;
}

int CDisplayPlane::ConvertDisplayPlaneResult(const DisplayPlaneResults result)
{
  int ret;

  switch (result)
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
      ret = -ENOTSUP;
      break;
    case STM_PLANE_ALREADY_CONNECTED:
      ret = -EALREADY;
      break;
    case STM_PLANE_CANNOT_CONNECT:
      ret = -EBUSY;
      break;
    default:
      ret = -ENOTSUP;
      break;
  }

  return ret;
}


DisplayPlaneResults CDisplayPlane::ProcessControlQueue(const stm_time64_t &vsyncTime)
{
  DisplayPlaneResults result = STM_PLANE_OK;
  CControlNode* p_CurrentNode = 0;
  CListenerNode* p_CurrentListenerNode = 0;
  uint32_t status_cnt = 0;
  int status = 0;

  // Check that VSyncLock is already taken before accessing to shared variables
  DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

  p_CurrentNode = (CControlNode*)m_ControlQueue.GetFirstNode();

  while (p_CurrentNode != 0)
  {
    switch (p_CurrentNode->m_control)
    {
      case PLANE_CTRL_CONNECT_TO_SOURCE:
        result = ConnectToSource(p_CurrentNode->m_params.display_source->handle);
        status = ConvertDisplayPlaneResult(result);
        break;
      case PLANE_CTRL_DISCONNECT_FROM_SOURCE:
        if(DisconnectFromSource(p_CurrentNode->m_params.display_source->handle))
        {
            // SUCCESS
            status = 0;
        }
        else
        {
            status = -ENOTSUP;
        }
        break;
      default:
        PLANE_TRC( TRC_ID_ERROR, "Unknown control !!");
        status = ConvertDisplayPlaneResult(STM_PLANE_NOT_SUPPORTED);
        break;
    }

    if (status_cnt < CONTROL_STATUS_ARRAY_MAX_NB)
    {
      m_ControlStatusArray[status_cnt].ctrl_id = p_CurrentNode->m_control;
      m_ControlStatusArray[status_cnt].error_code = status;
      status_cnt++;
    }
    else
    {
      PLANE_TRC( TRC_ID_ERROR, "Control status array is full: notification not possible !!!");
    }

    if(!m_ControlQueue.ReleaseDisplayNode((CNode*)p_CurrentNode))
    {
      PLANE_TRC( TRC_ID_ERROR, "Error while releasing node 0x%p from ControlQueue!", p_CurrentNode);
    }
    p_CurrentNode = (CControlNode*)m_ControlQueue.GetFirstNode();
  }

  if (status_cnt > 0)
  {
    p_CurrentListenerNode = (CListenerNode*)m_ListenerQueue.GetFirstNode();

    while (p_CurrentListenerNode != 0)
    {
      p_CurrentListenerNode->m_params.notify(p_CurrentListenerNode->m_params.data, vsyncTime,
        (stm_asynch_ctrl_status_t *)m_ControlStatusArray, status_cnt);

      p_CurrentListenerNode = (CListenerNode*)p_CurrentListenerNode->GetNextNode();
    }
  }

  return STM_PLANE_OK;
}

DisplayPlaneResults CDisplayPlane::RegisterAsynchCtrlListener(const stm_ctrl_listener_t *listener, int *listener_id)
{
  CListenerNode* p_CurrentNode = 0;
  bool           status        = false;

  if ((listener == 0) || (listener_id == 0))
  {
    PLANE_TRC( TRC_ID_ERROR, "invalid input parameter" );
    return STM_PLANE_INVALID_VALUE;
  }

  *listener_id = 0;
  if (listener->notify == 0)
  {
    PLANE_TRC( TRC_ID_ERROR, "invalid notify callback" );
    return STM_PLANE_INVALID_VALUE;
  }

  p_CurrentNode =  new CListenerNode(listener);
  if (p_CurrentNode == 0)
  {
    return STM_PLANE_NO_MEMORY;
  }

  m_pDisplayDevice->VSyncLock();
  status = m_ListenerQueue.QueueDisplayNode(p_CurrentNode);
  m_pDisplayDevice->VSyncUnlock();

  if (status == false)
  {
    return STM_PLANE_NOT_SUPPORTED;
  }

  // Warning: this code will not work if pointers are bigger than 32 bits.
  *listener_id = (int)p_CurrentNode;
  return STM_PLANE_OK;
}

DisplayPlaneResults CDisplayPlane::UnregisterAsynchCtrlListener(int listener_id)
{
  DisplayPlaneResults result = STM_PLANE_OK;
  CListenerNode* p_CurrentNode = 0;

  if (listener_id == 0)
  {
    PLANE_TRC( TRC_ID_ERROR, "invalid listener_id" );
    return STM_PLANE_INVALID_VALUE;
  }

  // Warning: this code will not work if pointers are bigger than 32 bits.
  p_CurrentNode = (CListenerNode*)listener_id;

  m_pDisplayDevice->VSyncLock();
  if(!m_ListenerQueue.ReleaseDisplayNode((CNode*)p_CurrentNode))
  {
    PLANE_TRC( TRC_ID_ERROR, "Error while releasing node 0x%p from ListenerQueue!", p_CurrentNode);
  }
  m_pDisplayDevice->VSyncUnlock();

  return result;
}

DisplayPlaneResults CDisplayPlane::GetControl(stm_display_plane_control_t ctrl, uint32_t *currentVal) const
{
  DisplayPlaneResults result = STM_PLANE_NOT_SUPPORTED;

  /* NB: "m_pDisplayDevice->VSyncLock()" is not taken when reading the variables below because they can be
         modified ONLY by stm_display_plane_set_compound_control() and stm_display_plane_set_control().
         Thanks to the STKPI semaphore, we are sure that the GetControl() and GetCompoundControl() functions
         are not executed at the same time as the set() action.
  */

  switch(ctrl)
  {
    case PLANE_CTRL_ASPECT_RATIO_CONVERSION_MODE:
      *currentVal = (uint32_t)m_AspectRatioConversionMode;
      PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_ASPECT_RATIO_CONVERSION_MODE currentVal=%u",  *currentVal );
      result = STM_PLANE_OK;
      break;
    case PLANE_CTRL_INPUT_WINDOW_MODE:
      *currentVal = m_InputWindowMode;
      PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_INPUT_WINDOW_MODE mode=%u",  *currentVal );
      result = STM_PLANE_OK;
      break;
    case PLANE_CTRL_OUTPUT_WINDOW_MODE:
      *currentVal = m_OutputWindowMode;
      PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_OUTPUT_WINDOW_MODE mode=%u",  *currentVal );
      result = STM_PLANE_OK;
      break;
    case PLANE_CTRL_TRANSPARENCY_VALUE:
      *currentVal = m_Transparency;
      PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_TRANSPARENCY_VALUE mode=%u",  *currentVal );
      result = STM_PLANE_OK;
      break;
    case PLANE_CTRL_TRANSPARENCY_STATE:
      *currentVal = m_TransparencyState;
      PLANE_TRC(TRC_ID_MAIN_INFO,"PLANE_CTRL_TRANSPARENCY_STATE mode=%u", *currentVal);
      result = STM_PLANE_OK;
      break;
    case PLANE_CTRL_DEPTH:
      if(m_lastRequestedDepth == 0)
      {
        int depth;
        // The Depth has never been set for this plane: Ask to the mixer what is the default depth for this plane
        if(GetDepth(m_pOutput, &depth))
        {
          *currentVal = depth;
          result = STM_PLANE_OK;
          PLANE_TRC(TRC_ID_MAIN_INFO,"PLANE_CTRL_DEPTH value=%u", *currentVal);
        }
        else
        {
          result = STM_PLANE_NOT_SUPPORTED;
          PLANE_TRC(TRC_ID_ERROR,"Error while reading the plane depth!");
        }
      }
      else
      {
          *currentVal = m_lastRequestedDepth;
          result = STM_PLANE_OK;
          PLANE_TRC(TRC_ID_MAIN_INFO,"PLANE_CTRL_DEPTH value=%u", *currentVal);
      }
      break;
    case PLANE_CTRL_VISIBILITY:
      *currentVal = m_hideRequestedByTheApplication?PLANE_NOT_VISIBLE:PLANE_VISIBLE;
      PLANE_TRC(TRC_ID_MAIN_INFO,"PLANE_CTRL_VISIBILITY value=%u", *currentVal);
      result = STM_PLANE_OK;
      break;
    case PLANE_CTRL_SRC_COLOR_STATE:
      *currentVal = m_ColorKeyState;
      PLANE_TRC(TRC_ID_MAIN_INFO,"PLANE_CTRL_SRC_COLOR_STATE mode=%u", *currentVal);
      result = STM_PLANE_OK;
      break;
    default:
      break;
  }

  return result;
}

bool CDisplayPlane::GetControlRange(stm_display_plane_control_t selector, stm_display_plane_control_range_t *range)
{
  bool result = false;

  switch(selector)
  {
    case PLANE_CTRL_ASPECT_RATIO_CONVERSION_MODE:
      range->min_val = ASPECT_RATIO_CONV_LETTER_BOX;
      range->max_val = ASPECT_RATIO_CONV_IGNORE;
      range->default_val = ASPECT_RATIO_CONV_LETTER_BOX;
      range->step = 1;
      PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_ASPECT_RATIO_CONVERSION_MODE" );
      result = true;
      break;
    case PLANE_CTRL_INPUT_WINDOW_MODE:
      range->min_val = MANUAL_MODE;
      range->max_val = AUTO_MODE;
      range->default_val = AUTO_MODE;
      range->step = 1;
      PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_INPUT_WINDOW_MODE" );
      result = true;
      break;
    case PLANE_CTRL_OUTPUT_WINDOW_MODE:
      range->min_val = MANUAL_MODE;
      range->max_val = AUTO_MODE;
      range->default_val = AUTO_MODE;
      range->step = 1;
      PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_OUTPUT_WINDOW_MODE" );
      result = true;
      break;
    default:
      break;
  }

  return result;
}

bool CDisplayPlane::GetControlMode(stm_display_control_mode_t *mode)
{
  // Default, return failure for an unsupported control
  return false;
}

bool CDisplayPlane::SetControlMode(stm_display_control_mode_t mode)
{
  // Default, return failure for an unsupported control
  return false;
}

bool CDisplayPlane::ApplySyncControls()
{
  // Default, return failure for an unsupported control
  return false;
}

DisplayPlaneResults CDisplayPlane::SetCompoundControl(stm_display_plane_control_t ctrl, void * newVal)
{
  DisplayPlaneResults result = STM_PLANE_NOT_SUPPORTED;
  switch(ctrl)
  {
    case PLANE_CTRL_INPUT_WINDOW_VALUE:
      m_pDisplayDevice->VSyncLock();
      // The VSync interrupt should be blocked while we update the IO Windows
      m_InputWindowValue = *((stm_rect_t *)newVal);
      m_ContextChanged = true;
      m_pDisplayDevice->VSyncUnlock();

      PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new IW)");
      PLANE_TRC( TRC_ID_MAIN_INFO, "New IW: x=%d y=%d w=%u h=%u",  m_InputWindowValue.x, m_InputWindowValue.y, m_InputWindowValue.width, m_InputWindowValue.height );
      result = STM_PLANE_OK;
      break;

    case PLANE_CTRL_OUTPUT_WINDOW_VALUE:
      m_pDisplayDevice->VSyncLock();
      // The VSync interrupt should be blocked while we update the IO Windows
      m_OutputWindowValue = *((stm_rect_t *)newVal);
      m_ContextChanged = true;
      m_pDisplayDevice->VSyncUnlock();

      PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new OW)");
      PLANE_TRC( TRC_ID_MAIN_INFO, "New OW: x=%d y=%d w=%u h=%u",  m_OutputWindowValue.x, m_OutputWindowValue.y, m_OutputWindowValue.width, m_OutputWindowValue.height );
      result = STM_PLANE_OK;
      break;

    case PLANE_CTRL_CONNECT_TO_SOURCE:
    case PLANE_CTRL_DISCONNECT_FROM_SOURCE:

      result = AddCompoundControlToQueue(ctrl, newVal);
      if (!m_pOutput && (result == STM_PLANE_OK) )
      {
        m_pDisplayDevice->VSyncLock();
        result = ProcessControlQueue(0);
        m_pDisplayDevice->VSyncUnlock();
      }
      break;

    case PLANE_CTRL_SRC_COLOR_VALUE:
      m_pDisplayDevice->VSyncLock();
      FillColorKeyConfig(&m_ColorKeyConfig, (stm_color_key_config_t *)newVal);
      m_ContextChanged = true;
      m_pDisplayDevice->VSyncUnlock();

      PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new Color Key)");
      result = STM_PLANE_OK;
      break;

    default:
      break;
  }

  // Default, return failure for an unsupported control
  return result;
}

DisplayPlaneResults CDisplayPlane::GetCompoundControl(stm_display_plane_control_t ctrl, void * currentVal)
{
  stm_compound_control_latency_t *pDisplayLatency;
  DisplayPlaneResults result = STM_PLANE_NOT_SUPPORTED;

  /* NB: "m_pDisplayDevice->VSyncLock()" is not taken when reading the variables below because they can be
         modified ONLY by stm_display_plane_set_compound_control() and stm_display_plane_set_control().
         Thanks to the STKPI semaphore, we are sure that the GetControl() and GetCompoundControl() functions
         are not executed at the same time as the set() action.
  */

  switch(ctrl)
  {
    case PLANE_CTRL_INPUT_WINDOW_VALUE:

      result = STM_PLANE_INVALID_VALUE;
      if(m_InputWindowMode==MANUAL_MODE && m_OutputWindowMode==MANUAL_MODE)
      {
          // In manual mode, return last user value programmed if valid
          if(!(m_InputWindowValue.x==0 && m_InputWindowValue.y==0 &&
               m_InputWindowValue.width==0 && m_InputWindowValue.height==0))
          {
            *((stm_rect_t *)currentVal) = m_InputWindowValue;
            result = STM_PLANE_OK;
          }
      }
      else
      {
          // At least input or output window is in automatic mode,
          // so return last computed value if valid
          // else return last user value programmed if valid and manual mode
          if(!(m_ComputedInputWindowValue.x==0 && m_ComputedInputWindowValue.y==0 &&
               m_ComputedInputWindowValue.width==0 && m_ComputedInputWindowValue.height==0))
          {
            *((stm_rect_t *)currentVal) = m_ComputedInputWindowValue;
            result = STM_PLANE_OK;
          }
          else if(!(m_InputWindowValue.x==0 && m_InputWindowValue.y==0 &&
                    m_InputWindowValue.width==0 && m_InputWindowValue.height==0)
                    && m_InputWindowMode==MANUAL_MODE)
          {
            *((stm_rect_t *)currentVal) = m_InputWindowValue;
            result = STM_PLANE_OK;
          }
      }

      if(result==STM_PLANE_OK)
      {
        PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_INPUT_WINDOW_VALUE x=%d y=%d w=%u h=%u",  ((stm_rect_t *)currentVal)->x, ((stm_rect_t *)currentVal)->y, ((stm_rect_t *)currentVal)->width, ((stm_rect_t *)currentVal)->height );
      }
      else
      {
        PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_INPUT_WINDOW_VALUE Result=%d", result );
      }

      break;
    case PLANE_CTRL_OUTPUT_WINDOW_VALUE:

      result = STM_PLANE_INVALID_VALUE;
      if(m_InputWindowMode==MANUAL_MODE && m_OutputWindowMode==MANUAL_MODE)
      {
          // In manual mode, return last user value programmed if valid
          if(!(m_OutputWindowValue.x==0 && m_OutputWindowValue.y==0 &&
               m_OutputWindowValue.width==0 && m_OutputWindowValue.height==0))
          {
            *((stm_rect_t *)currentVal) = m_OutputWindowValue;
            result = STM_PLANE_OK;
          }
      }
      else
      {
          // At least input or output window is in automatic mode,
          // so return last computed value if valid
          // else return last user value programmed if valid and manual mode
          if(!(m_ComputedOutputWindowValue.x==0 && m_ComputedOutputWindowValue.y==0 &&
               m_ComputedOutputWindowValue.width==0 && m_ComputedOutputWindowValue.height==0))
          {
            *((stm_rect_t *)currentVal) = m_ComputedOutputWindowValue;
            result = STM_PLANE_OK;
          }
          else if(!(m_OutputWindowValue.x==0 && m_OutputWindowValue.y==0 &&
                    m_OutputWindowValue.width==0 && m_OutputWindowValue.height==0)
                    && m_OutputWindowMode==MANUAL_MODE)
          {
            *((stm_rect_t *)currentVal) = m_OutputWindowValue;
            result = STM_PLANE_OK;
          }
      }

      if(result==STM_PLANE_OK)
      {
        PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_OUTPUT_WINDOW_VALUE x=%d y=%d w=%u h=%u",  ((stm_rect_t *)currentVal)->x, ((stm_rect_t *)currentVal)->y, ((stm_rect_t *)currentVal)->width, ((stm_rect_t *)currentVal)->height );
      }
      else
      {
        PLANE_TRC( TRC_ID_MAIN_INFO, "PLANE_CTRL_OUTPUT_WINDOW_VALUE Result=%d", result );
      }

      break;


    case PLANE_CTRL_MIN_VIDEO_LATENCY:
    case PLANE_CTRL_MAX_VIDEO_LATENCY:
        // On GDP, VDP or HQVDP planes, the latency is null because the pixel are read in memory and immediately output to the TV. This is the default latency.
        pDisplayLatency = (stm_compound_control_latency_t *) currentVal;

        pDisplayLatency->nb_input_periods  = 0;
        pDisplayLatency->nb_output_periods = 0;

        if (ctrl == PLANE_CTRL_MIN_VIDEO_LATENCY)
            PLANE_TRC( TRC_ID_UNCLASSIFIED, "Min plane latency : In %d, Out %d", pDisplayLatency->nb_input_periods, pDisplayLatency->nb_output_periods );
        else
            PLANE_TRC( TRC_ID_UNCLASSIFIED, "Max plane latency : In %d, Out %d", pDisplayLatency->nb_input_periods, pDisplayLatency->nb_output_periods );

        result = STM_PLANE_OK;
        break;

    case PLANE_CTRL_SRC_COLOR_VALUE:
      FillColorKeyConfig((stm_color_key_config_t *)currentVal, &m_ColorKeyConfig);
      result = STM_PLANE_OK;
      break;

    default:
      break;
  }

  // Default, return failure for an unsupported control
  return result;
}

bool CDisplayPlane::GetCompoundControlRange(stm_display_plane_control_t selector, stm_compound_control_range_t *range)
{
  bool result = false;

  switch(selector)
  {
    case PLANE_CTRL_INPUT_WINDOW_VALUE:
      range->min_val.x = 0;
      range->min_val.y = 0;
      range->min_val.width = 32;  // 2 macroblocks for smallest rectangle
      range->min_val.height = 32;
      range->max_val.x = 0;
      range->max_val.y = 0;
      range->max_val.width = 1920;
      range->max_val.height = 1080;
      range->default_val.x = 0;
      range->default_val.y = 0;
      range->default_val.width = 1280;
      range->default_val.height = 720;
      range->step.x = 0;
      range->step.y = 0;
      range->step.width = 1;
      range->step.height = 1;
      PLANE_TRC( TRC_ID_MAIN_INFO, "IOW CDisplayPlane::GetCompoundControlRange selector=PLANE_CTRL_INPUT_WINDOW_VALUE" );
      result = true;
      break;

    case PLANE_CTRL_OUTPUT_WINDOW_VALUE:
      range->min_val.x = 0;
      range->min_val.y = 0;
      range->min_val.width = 32;  // 2 macroblocks for smallest rectangle
      range->min_val.height = 32;
      range->max_val.x = 0;
      range->max_val.y = 0;
      range->max_val.width = 1920;
      range->max_val.height = 1080;
      range->default_val.x = 0;
      range->default_val.y = 0;
      range->default_val.width = 1280;
      range->default_val.height = 720;
      range->step.x = 0;
      range->step.y = 0;
      range->step.width = 1;
      range->step.height = 1;
      PLANE_TRC( TRC_ID_MAIN_INFO, "IOW CDisplayPlane::GetCompoundControlRange selector=PLANE_CTRL_OUTPUT_WINDOW_VALUE" );
      result = true;
      break;
    default:
      break;
  }

  // Default, return failure for an unsupported control
  return result;
}

bool CDisplayPlane::GetTuningDataRevision(stm_display_plane_tuning_data_control_t ctrl, uint32_t * revision)
{
  // Default, return failure for an unsupported control
  return false;
}

DisplayPlaneResults CDisplayPlane::GetTuningDataControl(stm_display_plane_tuning_data_control_t ctrl, stm_tuning_data_t * currentVal)
{
  // Default, return failure for an unsupported control
  return STM_PLANE_NOT_SUPPORTED;
}

DisplayPlaneResults CDisplayPlane::SetTuningDataControl(stm_display_plane_tuning_data_control_t ctrl, stm_tuning_data_t * newVal)
{
  // Default, return failure for an unsupported control
  return STM_PLANE_NOT_SUPPORTED;
}

bool CDisplayPlane::SetDepth(COutput *pOutput, int32_t depth, bool activate)
{
  if(!pOutput || !pOutput->CanShowPlane(this))
    return false;

  if(!pOutput->SetPlaneDepth(this, depth, activate))
    return false;

  return true;
}


bool CDisplayPlane::GetDepth(COutput *pOutput, int *depth) const
{
  if(!pOutput || !pOutput->CanShowPlane(this))
    return false;

  if(!pOutput->GetPlaneDepth(this, depth))
    return false;

  return true;
}

/*
 * This allows us to pause the current content on the screen, while the
 * queue still gets processed behind the scenes.
 */
bool CDisplayPlane::Pause(void)
{
    if(!(m_capabilities&PLANE_CAPS_VIDEO)) /* for the moment only video planes can pause */
        return false;

    m_pDisplayDevice->VSyncLock();

    if(m_PauseState != PAUSESTATE_DISABLED)
    {
        PLANE_TRC(TRC_ID_MAIN_INFO,
                  "Pause requested while m_PauseState != PAUSESTATE_DISABLED (m_PauseState = %d)",
                  m_PauseState);
        m_pDisplayDevice->VSyncUnlock();
        return false;
    }

    m_PauseState = PAUSESTATE_PAUSE_REQUESTED; /* pause will start at next Vsync */
    m_status  |= STM_STATUS_PLANE_PAUSED;

    m_pDisplayDevice->VSyncUnlock();

    return true;
}

bool CDisplayPlane::UnPause(void)
{
    if(!(m_capabilities&PLANE_CAPS_VIDEO))
        return false;

    m_pDisplayDevice->VSyncLock();
    m_PauseState = PAUSESTATE_UNPAUSE_REQUESTED;
    m_pDisplayDevice->VSyncUnlock();

    return true;
}


/*
    A display on a plane is possible if:
    - The plane is connected to a source
    - The plane is connected to an output
    - The output is started
    - The application didn't ask to hide the plane (PLANE_HIDE_BY_DISABLING)
    - The Current Node presented is not null
    - We are not in low power mode
 */
bool CDisplayPlane::isDisplayPossible(CDisplayNode *pCurrNode)
{
    if ( (!m_pOutput)                       ||      // The plane is connected to no output
         (!m_pSource)                       ||      // The plane is connected to no source
         (!m_outputInfo.isOutputStarted)    ||      // This plane output is not started so it can not be visible
         (m_bIsFrozen) )                            // This plane is in low power mode
    {
        // Display NOT possible
        return false;
    }

    if(m_hideRequestedByTheApplication)             // The application is requesting to hide this plane
    {
        if(isVideoPlane())
        {
            if(m_eHideMode == PLANE_HIDE_BY_DISABLING)
            {
                // The application asks to disable this Video plane
                return false;
            }
            else
            {
                // The use case "PLANE_HIDE_BY_MASKING" is handled when writing the video plug setup.
                // The plane remains enabled but the transparent color is substituted to the real pixels
            }
        }
        else
        {
            // The application asks to disable this GFX plane
            return false;
        }
    }

    if(!pCurrNode)
    {
        // Invalid pCurrNode: Display NOT possible
        PLANE_TRC( TRC_ID_VSYNC, "pCurrNode is a NULL pointer!" );
        return false;
    }

    // Display possible
    return true;
}


// This function is managing the proper activation/deactivation of plane's HW and Clocks
void CDisplayPlane::UpdatePlaneState(void)
{
    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    // Check if display is possible for next VSync
    if(isDisplayPossible(m_picturesPreparedForNextVSync.pCurNode))
    {
      PLANE_TRC(TRC_ID_PLANE, "new picture queued state!");
      goto plane_enable_hw;
    }

    // No picture was presented for Next VSync. The plane should be disabled
    switch(m_currentState)
    {
        case STM_PLANE_HW_ENABLED:
            /*
             * Hide the plane (will be ignored on Mixer during next VSync).
             */
            MaskPlane();
            PLANE_TRC(TRC_ID_MAIN_INFO, "STM_PLANE_HW_ENABLED (clkDeactivationCountDown=%d)", m_clkDeactivationCountDown);
            break;

        case STM_PLANE_HW_MASKED:
            // HW was already stopped. m_hwDeactivationCountDown is decremented and, when it reaches 0, the plane's hardware will be disabled
            PLANE_TRC(TRC_ID_MAIN_INFO, "m_hwDeactivationCountDown=%d", m_hwDeactivationCountDown);
            if ((m_hwDeactivationCountDown == 0)
             || (m_pOutput && !m_pOutput->IsStarted()))
            {
                DisablePlane();
                PLANE_TRC(TRC_ID_MAIN_INFO, "STM_PLANE_HW_STOPPED");
            }
            else
            {
                // We stay in the state "STM_PLANE_HW_MASKED" until m_hwDeactivationCountDown reaches '0'
                m_hwDeactivationCountDown--;
            }
            break;

        case STM_PLANE_HW_STOPPED:
            // HW was already stopped. m_clkDeactivationCountDown is decremented and, when it reaches 0, the plane's clock will be stopped
            PLANE_TRC(TRC_ID_MAIN_INFO, "clkDeactivationCountDown=%d", m_clkDeactivationCountDown);
            if (m_clkDeactivationCountDown == 0)
            {
                DisableClocks();
                PLANE_TRC(TRC_ID_MAIN_INFO, "STM_PLANE_HW_AND_CLOCK_STOPPED");
                if (m_numConnectedOutputs == 0)
                {
                  m_ulTimingID = 0;
                  m_pOutput    = 0;
                }
            }
            else
            {
                // We stay in the state "STM_PLANE_HW_STOPPED" until m_clkDeactivationCountDown reaches '0'
                m_clkDeactivationCountDown--;
            }
            break;

        case STM_PLANE_HW_AND_CLOCK_STOPPED:
            // No change: The plane HW and Clocks are already stopped
            break;
    }

    // Status update completed
    return;

plane_enable_hw:
    // A valid picture has been presented for next VSync
    if(m_currentState != STM_PLANE_HW_ENABLED)
    {
        if (EnablePlane())
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "STM_PLANE_HW_ENABLED");
        }
        else
        {
            PLANE_TRC(TRC_ID_ERROR, "STM_PLANE_HW_ENABLED failed ");
        }
    }
}

bool CDisplayPlane::EnablePlane(void)
{
    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    // Set the pixel clocks associated to this plane before enabling it
    SetPixelClock();

    // Enable the clocks associated to this plane
    EnableClocks();

    m_status |= STM_STATUS_PLANE_VISIBLE;

    if(m_pOutput)
    {
        if(!m_pOutput->ShowPlane(this))
        {
            PLANE_TRC( TRC_ID_ERROR, "ShowPlane failed!");
            return false;
        }
    }
    else
    {
        PLANE_TRC(TRC_ID_ERROR, "Plane can't be enabled if not connected to an output!");
        return false;
    }

    m_currentState = STM_PLANE_HW_ENABLED;
    return true;
}

void CDisplayPlane::DisablePlane(void)
{
    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    // Do an action specific to this plane HW to stop it
    DisableHW();

    // Warning: m_pOutput can be null if the plane is not connected to an output!
    if(m_pOutput)
    {
        m_pOutput->HidePlane(this);
    }
    else
    {
        PLANE_TRC(TRC_ID_ERROR, "Plane can't be stopped properly if not connected to an output!");
    }

    m_status &= ~STM_STATUS_PLANE_VISIBLE;
    m_currentState = STM_PLANE_HW_STOPPED;
    m_clkDeactivationCountDown = m_ClockDeactivationVsyncCount;

    // Reset some statistics when plane is stopped
    m_Statistics.IsScalingPossible = SCALING_NOT_POSSIBLE;
    m_Statistics.DataRateUseCaseInMBperS = 0;
    m_Statistics.ColorimetryConversion = 0;

    PLANE_TRC(TRC_ID_MAIN_INFO, "STM_PLANE_HW_STOPPED");
}

void CDisplayPlane::MaskPlane(void)
{
  PLANE_TRCIN( TRC_ID_MAIN_INFO, "Mask Plane" );

  if (isVideoPlane())
  {
    // Nothing to do for video planes. Disable the plane (asynchronous call).
    DisablePlane();
  }
  else
  {
    m_currentState = STM_PLANE_HW_MASKED;
    m_hwDeactivationCountDown =  m_HwDeactivationVsyncCount;
  }

  PLANE_TRCOUT( TRC_ID_MAIN_INFO, "" );
}

bool CDisplayPlane::EnableClocks(void)
{
  bool proc_clk_res, pix_clk_res;

  // Enable the ProcClock first
  proc_clk_res = EnableProcClock();
  pix_clk_res  = EnablePixelClock();

  if(proc_clk_res && pix_clk_res)
  {
    return true;
  }
  else
  {
    return false;
  }
}


bool CDisplayPlane::EnableProcClock(void)
{
  int ret = 0;
  unsigned long rate = 0;

  if((!m_isProcClkEnabled) && (!m_areClocksAlwaysEnabled))
  {
    if (m_procClockName[0] != '\0')
    {
      ret = vibe_os_clk_enable(&m_procClock);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Proc clock %s enable failed", m_procClockName );
        goto error;
      }
      else
      {
        rate = vibe_os_clk_get_rate(&m_procClock);
        PLANE_TRC(TRC_ID_MAIN_INFO, "Proc clock %s enabled at %lu Hz", m_procClockName, rate);
      }
    }
    m_isProcClkEnabled = true;
  }
  return true;

error:
  return false;
}

bool CDisplayPlane::EnablePixelClock(void)
{
  int ret = 0;

  if (m_pixelClockName[0] != '\0')
  {
    if((!m_isPixelClkEnabled) && (!m_areClocksAlwaysEnabled))
    {
      ret = vibe_os_clk_enable(&m_pixelClock);
      if(ret<0)
      {
        PLANE_TRC(TRC_ID_ERROR, "Pixel clock %s enable failed", m_pixelClockName );
        return false;
      }

      m_isPixelClkEnabled = true;
      PLANE_TRC(TRC_ID_MAIN_INFO, "Pixel clock %s enabled", m_pixelClockName );
    }
  }

  return true;
}

bool CDisplayPlane::SetPixelClock()
{
  if(m_hasADedicatedPixelClock)
  {
    /*
     * Before setting the Pixel Clock rate we should check that the plane
     * does have a valid pixel clock and this clock is not running (not
     * enabled).
     *
     * For now driver is allowing the clock rate update while the clock
     * is running as this can happen for graphical planes which are not
     * able to detect new pushed buffers while stopping the clock
     * (during 4 VSync time period).
     */
    if (m_pixelClockName[0] != '\0')
    {
      int ret = 0;
      unsigned long rate = 0;
      unsigned long delta = 0;
      struct vibe_clk outputClock;
      struct vibe_clk outputParentClock;
      struct vibe_clk current_parent;
      bool new_parent = true;

      if (m_pOutput == NULL)
      {
        PLANE_TRC(TRC_ID_ERROR, "Output not connected");
        return false;
      }

      if ((m_isPixelClkEnabled) && (m_currentState == STM_PLANE_HW_ENABLED))
      {
        PLANE_TRC(TRC_ID_ERROR, "Setting Pixel clock %s rate while clock is enabled!", m_pixelClockName);
      }

      ret = vibe_os_clk_get(m_pOutput->GetPixelClockName(), &outputClock);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Output clock %s get failed", m_pOutput->GetPixelClockName() );
        return false;
      }

      ret = vibe_os_clk_get_parent(&outputClock, &outputParentClock);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Output clock %s get parent failed", m_pOutput->GetPixelClockName() );
        vibe_os_clk_put(&outputClock);
        return false;
      }

      /*
       * Check if the new clock parent is the same as the old one
       * No need to re-parent if it's the same
       */
      ret = vibe_os_clk_get_parent(&m_pixelClock, &current_parent);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Output clock %s get parent failed", m_pixelClockName );
        return false;
      }
      if (current_parent.clk != outputParentClock.clk)
        ret = vibe_os_clk_set_parent(&m_pixelClock, &outputParentClock);
      else
      {
        new_parent = false;
        PLANE_TRC(TRC_ID_MAIN_INFO, "No need to re-parent %s", m_pixelClockName );
      }

      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Pixel clock %s set parent failed", m_pixelClockName );
        vibe_os_clk_put(&outputClock);
        return false;
      }

      rate = vibe_os_clk_get_rate(&outputClock);
      PLANE_TRC(TRC_ID_MAIN_INFO, "ParentClock = %s running at %lu Hz", m_pOutput->GetPixelClockName(), rate);

      ret = vibe_os_clk_put(&outputClock);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Output clock %s put failed", m_pOutput->GetPixelClockName() );
        return false;
      }

      /*
       * The Parent wasn't updated. Check if the Plane's pixel clock
       * need to be updated or it is already set to the expected rate.
       *
       * If the Parent was updated then the plane pixel clock should be
       * updated even if the current clock rate is same as the new one.
       * This is required for semi-sync clocks where the clock will be
       * disable and enable back to apply the new rate.
       */
      if(!new_parent)
      {
        unsigned long old_rate = vibe_os_clk_get_rate(&m_pixelClock);
        PLANE_TRC(TRC_ID_MAIN_INFO, "PixelClock = %s running at %lu Hz", m_pixelClockName, old_rate);

        if(rate == old_rate)
        {
          PLANE_TRC(TRC_ID_MAIN_INFO, "PixelClock = %s already running at %lu Hz", m_pixelClockName, rate);
          return true;
        }
      }

      ret = vibe_os_clk_set_rate(&m_pixelClock, rate);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Pixel clock %s set rate failed", m_pixelClockName );
        return false;
      }
      PLANE_TRC(TRC_ID_MAIN_INFO, "Pixel clock %s set to %lu Hz", m_pixelClockName, rate);

      // Check that the pixel_clock is running at the expected frequency
      // It should be closed to "m_outputInfo.pixel_clock_freq" (there can be a small delta due to the clock adjustment)
      rate = vibe_os_clk_get_rate(&m_pixelClock);
      if(rate > m_outputInfo.pixel_clock_freq)
      {
        delta = rate - (unsigned long) m_outputInfo.pixel_clock_freq;
      }
      else
      {
        delta = (unsigned long) m_outputInfo.pixel_clock_freq - rate;
      }
      // Print an error if the delta is > than 1% of the expected rate
      if (delta > (rate / 100))
      {
        PLANE_TRC(TRC_ID_ERROR, "Incorrect clock rate! %lu Hz vs %u Hz", rate, m_outputInfo.pixel_clock_freq);
      }
    }
  }

  return true;
}


bool CDisplayPlane::DisableClocks(void)
{
  bool proc_clk_res, pix_clk_res;

  // Disable the Pixel Clock first
  pix_clk_res  = DisablePixelClock();
  proc_clk_res = DisableProcClock();

  m_currentState = STM_PLANE_HW_AND_CLOCK_STOPPED;

  if(proc_clk_res && pix_clk_res)
  {
    return true;
  }
  else
  {
    PLANE_TRC(TRC_ID_ERROR, "Failed to disable plane's clocks!");
    return false;
  }
}

bool CDisplayPlane::DisableProcClock(void)
{
  int ret = 0;

  if (m_procClockName[0] != '\0')
  {
    if((m_isProcClkEnabled) && (!m_areClocksAlwaysEnabled))
    {
      ret = vibe_os_clk_disable(&m_procClock);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Proc clock %s disable failed", m_procClockName );
        goto error;
      }
      else
      {
        PLANE_TRC(TRC_ID_MAIN_INFO, "Proc clock %s disable", m_procClockName );
      }
      m_isProcClkEnabled = false;
    }
  }
  return true;

error:
  return false;
}

bool CDisplayPlane::DisablePixelClock(void)
{
  int ret = 0;

  if ((m_pixelClockName[0] != '\0') && (!m_areClocksAlwaysEnabled))
  {
    if (m_isPixelClkEnabled)
    {
      ret = vibe_os_clk_disable(&m_pixelClock);
      if (ret)
      {
        PLANE_TRC(TRC_ID_ERROR, "Pixel clock %s disable failed", m_pixelClockName );
        goto error;
      }
      else
      {
        PLANE_TRC(TRC_ID_MAIN_INFO, "Pixel clock %s disable", m_pixelClockName );
      }
      m_isPixelClkEnabled = false;
    }
  }
  return true;

error:
  return false;
}


void CDisplayPlane::DisableHW(void)
{
  PLANE_TRC(TRC_ID_MAIN_INFO, "Disable HW" );

  // This will cause the next presented picture to be displayed from scratch (history flushed)
  m_ContextChanged = true;
  PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed");
}


bool CDisplayPlane::HideRequestedByTheApplication(void)
{
  bool status = false;

  m_pDisplayDevice->VSyncLock();

  PLANE_TRC(TRC_ID_MAIN_INFO, "Application requiring to hide plane (m_eHideMode=%u)", m_eHideMode );

  if(!m_hideRequestedByTheApplication)
  {
    m_hideRequestedByTheApplication = true;
    status = true;
  }

  m_pDisplayDevice->VSyncUnlock();

  return(status);
}


bool CDisplayPlane::ShowRequestedByTheApplication(void)
{
  bool status = false;

  m_pDisplayDevice->VSyncLock();

  PLANE_TRC(TRC_ID_MAIN_INFO, "Application requiring to unhide plane" );

  if(m_hideRequestedByTheApplication)
  {
    m_hideRequestedByTheApplication = false;
    status = true;
  }

  m_pDisplayDevice->VSyncUnlock();

  return(status);
}


bool CDisplayPlane::UpdateFromMixer(COutput* pOutput, stm_plane_update_reason_t update_reason)
{
  /*
   * Do nothing if Output is not the same connected on this plane
   */
  if(m_pOutput != pOutput)
    return true;

  // Check that this function is called from thread context and not from interrupt context
  DEBUG_CHECK_THREAD_CONTEXT();

  ////////// Block the VSync ThreadedIRQ while update the output info //////////
  m_pDisplayDevice->VSyncLock();

  switch(update_reason)
  {
    case STM_PLANE_OUTPUT_STOPPED:
    {
      PLANE_TRC( TRC_ID_MAIN_INFO, "STM_PLANE_OUTPUT_STOPPED");
      ResetDisplayInfo();
      m_outputInfo.isOutputStarted = false;

      UpdateOutputInfoSavedInSource();
    }
    break;

    case STM_PLANE_OUTPUT_UPDATED:
    {
      PLANE_TRC( TRC_ID_MAIN_INFO, "STM_PLANE_OUTPUT_UPDATED" );
      // The output has changed. Get some info about the new display mode
      UpdateOuptutInfo();
    }
    break;

    case STM_PLANE_OUTPUT_STARTED:
    {
      PLANE_TRC( TRC_ID_MAIN_INFO, "STM_PLANE_OUTPUT_STARTED");

      // The output has changed. Get some info about the new display mode
      UpdateOuptutInfo();
      SendOutputModeChangedEvent();

      // Restart Semi-Sync clocks for programing final diviser compatible
      // with all clocks belonging to same group. Else, enabling one clock
      // of this group could disable and enable back all enabled clocks
      // of this group causing a black screen
      // Do it only for video clock has there is a specific switch off sequence
      // for GDP plane due to an hardware issue.
      if( isVideoPlane() )
      {
        EnablePixelClock();
        DisablePixelClock();
      }

    }
    break;

    default:
      PLANE_TRC( TRC_ID_ERROR, "Invalid update_reason (%d)", update_reason);
      m_pDisplayDevice->VSyncUnlock();
      return false;
  }

  m_pDisplayDevice->VSyncUnlock();
  /////////////////////////////////////////////////////////////////////////////

  return true;
}


int CDisplayPlane::GetFormats(const stm_pixel_format_t** pData) const
{
    *pData = m_pSurfaceFormats;
    return m_nFormats;
}


void CDisplayPlane::UpdateOutputHDRInfo(void)
{
  CDisplayNode *pNodeToBeDisplayed = m_picturesPreparedForNextVSync.pCurNode;

  if(!pNodeToBeDisplayed)
    return;

  if((m_capabilities & PLANE_CAPS_HDR_FORMAT) == 0)
    return;

  if (!m_pOutput || !m_pOutput->IsHDRModeAuto())
    return;

  stm_hdr_format_t outputHDRFormat;
  // Get the current Output Dynamic Range Format...
  if(m_pOutput->GetOutputHDRFormat(outputHDRFormat))
  {
    if(m_bIsMasterVideoPlane)
    {
      // check if hdr_format or output colorimetry have changed
      if (   (vibe_os_memcmp((void*)&(pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt), (void*) &m_PrevNodeHdr_Fmt, sizeof(stm_hdr_format_t))!=0)
          || (m_pOutput->GetSinkSupportedHDRFormats() != m_ulPrevSinkHDRFormats)
          || (m_outputInfo.outputColorSpace != m_outputInfo.prevOutputColorSpace))
      {
        PLANE_TRC( TRC_ID_MAIN_INFO, "m_ulPrevSinkHDRFormats 0x%x, GetSinkSupportedHDRFormats() 0x%x",m_ulPrevSinkHDRFormats,m_pOutput->GetSinkSupportedHDRFormats() );
        PLANE_TRC( TRC_ID_MAIN_INFO, "pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt.eotf_type 0x%x, m_PrevNodeHdr_Fmt.eotf_type 0x%x",pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt.eotf_type,m_PrevNodeHdr_Fmt.eotf_type);
        PLANE_TRC( TRC_ID_MAIN_INFO, "m_outputInfo.outputColorSpace 0x%x", m_outputInfo.outputColorSpace);

        // Check if new HDR format is supported by connected output Sink and output colorimetry is HDR-compatible
        if(   (m_outputInfo.outputColorSpace == STM_COLORSPACE_BT2020)
           && ((1 << pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt.eotf_type) & m_pOutput->GetSinkSupportedHDRFormats()))
        {
          if(!m_pOutput->SetOutputHDRFormat(&pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt))
          {
            PLANE_TRC( TRC_ID_ERROR, "Failed to set Output HDR Format! (0x%u)", static_cast<unsigned int>(pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt.eotf_type));
          }
        }
        else
        {
          // If HDR format is not supported or output colorimetry is not HDR-compatible, notify a SDR Mode to Stop DRMInfoFrames
          stm_hdr_format_t sdrFormat;
          sdrFormat.eotf_type = STM_EOTF_GAMMA_SDR;
          sdrFormat.is_st2086_metadata_present = false;
          sdrFormat.is_hdr_metadata_present = false;

          PLANE_TRC( TRC_ID_CONTEXT_CHANGE, "HDR Format isn't supported by current connected Output Sink! (0x%u)", static_cast<unsigned int>(pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt.eotf_type));
          if(!m_pOutput->SetOutputHDRFormat(&sdrFormat))
          {
            PLANE_TRC( TRC_ID_ERROR, "Failed to Force Output to SDR Format!");
          }
        }

        /* Simply update HDROUT Format */
        m_updateHDROutFormat = true;

        // memorize GetSinkSupportedHDRFormats and Prev node Info
        m_ulPrevSinkHDRFormats = m_pOutput->GetSinkSupportedHDRFormats();
        m_PrevNodeHdr_Fmt = pNodeToBeDisplayed->m_bufferDesc.src.hdr_fmt;
        m_outputInfo.prevOutputColorSpace = m_outputInfo.outputColorSpace;
      }
    }
    else if(vibe_os_memcmp((void*)&(m_outputInfo.outputHDRFormat), (void*) &outputHDRFormat, sizeof(stm_hdr_format_t))!=0)
    {
      // Update the current plane's output HDR Format
      m_outputInfo.outputHDRFormat = outputHDRFormat;

      /* Simply update HDROUT Format */
      m_updateHDROutFormat = true;
    }
  }
}


int CDisplayPlane::GetListOfFeatures( stm_plane_feature_t *list, uint32_t number)
{
    if(number > m_nFeatures)
        return -ERANGE;

    if(number > 0 && list != NULL )
    {
        for(uint32_t f=0; f<number; f ++)
            list[f]=m_pFeatures[f] ;
    }

    return m_nFeatures;
}

bool CDisplayPlane::IsFeatureApplicable( stm_plane_feature_t feature, bool *applicable) const
{
    bool isFeatureSupported = false;

    // Go trough list of feature to check the requested one is available
    for(uint32_t f=0; f<m_nFeatures; f++)
        if(m_pFeatures[f] == feature)
        {
            isFeatureSupported=true;
            break;
        }

    // By default, a supported feature is applicable but it can be customized in a derived method
    if(applicable)
        *applicable = isFeatureSupported;

    return (isFeatureSupported);
}


void CDisplayPlane::ResetQueueBufferState(void) {}

////////////////////////////////////////////////////////////////////////////////
//

bool CDisplayPlane::GetRGBYCbCrKey(uint8_t&            ucRCr,
                                   uint8_t&            ucGY,
                                   uint8_t&            ucBCb,
                                   uint32_t            ulPixel,
                                   stm_pixel_format_t colorFmt,
                                   bool                pixelIsRGB)
{
  bool  bResult = true;

  uint32_t R = (ulPixel & 0xFF0000) >> 16;
  uint32_t G = (ulPixel & 0xFF00) >> 8;
  uint32_t B = (ulPixel & 0xFF);

  switch(colorFmt)
  {
    case SURF_YCBCR422MB:
    case SURF_YCBCR422R:
    case SURF_YCBCR420MB:
    case SURF_YUYV:
    case SURF_YUV420:
    case SURF_YVU420:
    case SURF_YUV422P:
    case SURF_YCbCr420R2B:
    case SURF_YCbCr422R2B:
      if (pixelIsRGB)
      {
        ucRCr = (uint8_t)(128 + ((131 * R) / 256) - (((110 * G) / 256) + ((21 * B) / 256)));
        ucGY  = (uint8_t)(((77 * R) /256) + ((150 * G) / 256) + ((29 * B) / 256));
        ucBCb = (uint8_t)(128 + ((131 * B) / 256) - (((44 * R) / 256) + ((87 * G) / 256)));
      }
      else
      {
    ucRCr = R;
    ucGY  = G;
    ucBCb = B;
      }
    break;

    case SURF_RGB565:
    case SURF_RGB888:
    case SURF_ARGB8565:
    case SURF_ARGB8888:
    case SURF_BGRA8888:
    case SURF_ARGB1555:
    case SURF_ARGB4444:
    case SURF_CLUT8:
      if (pixelIsRGB)
      {
        ucRCr = (uint8_t)R;
        ucGY  = (uint8_t)G;
        ucBCb = (uint8_t)B;
      }
      else
      {
        TRC( TRC_ID_ERROR, "Pixel format can only be RGB for surface %u.", (uint32_t)colorFmt );
        bResult = false;
      }
    break;

    default:
      TRC( TRC_ID_ERROR, "Unsupported surface format %u.", (uint32_t)colorFmt );
      bResult = false;
    break;
  }

  //Fill the LSBs with zero
  switch(colorFmt)
  {
    case SURF_RGB565:
    case SURF_ARGB8565:
      ucRCr <<= 3;
      ucGY <<= 2;
      ucBCb <<= 3;
    break;

    case SURF_ARGB1555:
      ucRCr <<= 3;
      ucGY <<= 3;
      ucBCb <<= 3;
    break;

    case SURF_ARGB4444:
      ucRCr <<= 4;
      ucGY <<= 4;
      ucBCb <<= 4;
    break;

    default:
      break;
  }

  return bResult;
}

void CDisplayPlane::FillColorKeyConfig (stm_color_key_config_t       * const dst,
                                      const stm_color_key_config_t * const src) const
{
  dst->flags = (src->flags | dst->flags);
  /* Update only required data */
  if (src->flags & SCKCF_ENABLE)
    dst->enable = src->enable;
  if (src->flags & SCKCF_FORMAT)
    dst->format = src->format;
  if (src->flags & SCKCF_R_INFO)
    dst->r_info = src->r_info;
  if (src->flags & SCKCF_G_INFO)
    dst->g_info = src->g_info;
  if (src->flags & SCKCF_B_INFO)
    dst->b_info = src->b_info;
  if (src->flags & SCKCF_MINVAL)
    dst->minval = src->minval;
  if (src->flags & SCKCF_MAXVAL)
    dst->maxval = src->maxval;
}

TuningResults CDisplayPlane::SetTuning(uint16_t service, void *inputList, uint32_t inputListSize, void *outputList, uint32_t outputListSize)
{
    TuningResults res = TUNING_SERVICE_NOT_SUPPORTED;
    tuning_service_type_t  serviceType = (tuning_service_type_t)service;

    switch(serviceType)
    {
        case RESET_STATISTICS:
        {
            vibe_os_zero_memory(&m_Statistics, sizeof(m_Statistics));
            res = TUNING_OK;
            break;
        }
        default:
            break;
    }
    return res;
}

int CDisplayPlane::GetConnectedOutputID(uint32_t *id, uint32_t max_ids)
{
  uint32_t        numId = 0;

  if (id == 0)
    return m_numConnectedOutputs;

  for(uint32_t i=0; (i<m_numConnectedOutputs) && (numId<max_ids) ; i++)
  {
  // only one output is supported for the moment.
  // Need to be updated when multiple output support is available
  //    COutput       *pOutput  = m_pConnectedOutputs[i];
      COutput       *pOutput  = m_pOutput;
      if (pOutput)
      {
        id[numId++] = pOutput->GetID();
      }
  }

  return numId;
}


int CDisplayPlane::GetAvailableSourceID(uint32_t *id, uint32_t max_ids)
{
  uint32_t        numId = 0;
  uint32_t        numSources;
  CDisplaySource* source = 0;

  numSources= m_pDisplayDevice->GetNumberOfSources();
  if (id == 0)
    return numSources;

  for(uint32_t i=0; (i<numSources) && (numId<max_ids) ; i++)
  {
    source = m_pDisplayDevice->GetSource(i);
    id[numId++] = (source)?source->GetID():0xFFFF;
  }

  return numId;
}

void CDisplayPlane::Suspend(void)
{
  PLANE_TRC( TRC_ID_MAIN_INFO, "Suspending" );
  m_bIsSuspended = true;
}

void CDisplayPlane::Freeze(void)
{
  PLANE_TRC( TRC_ID_MAIN_INFO, "Freezing" );
  m_pDisplayDevice->VSyncLock();
  m_bIsFrozen = true;
  m_pDisplayDevice->VSyncUnlock();
}

void CDisplayPlane::Resume(void)
{
  PLANE_TRC( TRC_ID_MAIN_INFO, "Resuming" );

  // We exit from suspended state so it is better to reset the history
  m_pDisplayDevice->VSyncLock();

  m_bIsFrozen = false;
  m_bIsSuspended = false;
  m_ContextChanged = true;
  PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (leaving low power mode)");

  m_pDisplayDevice->VSyncUnlock();
}

void CDisplayPlane::ResetComputedInputOutputWindowValues(void)
{
  m_ComputedInputWindowValue.x      = 0;
  m_ComputedInputWindowValue.y      = 0;
  m_ComputedInputWindowValue.width  = 0;
  m_ComputedInputWindowValue.height = 0;

  m_ComputedOutputWindowValue.x      = 0;
  m_ComputedOutputWindowValue.y      = 0;
  m_ComputedOutputWindowValue.width  = 0;
  m_ComputedOutputWindowValue.height = 0;
}

stm_display_use_cases_t CDisplayPlane::GetCurrentUseCase(BufferNodeType srcType, bool isDisplayInterlaced, bool isTopFieldOnDisplay)
{
    stm_display_use_cases_t useCase = MAX_USE_CASES;

    /* to find the current use case */
    switch(srcType)
    {
        case GNODE_PROGRESSIVE:
        {
            if(isDisplayInterlaced)
            {
                if(isTopFieldOnDisplay)
                    useCase = FRAME_TO_TOP;
                else
                    useCase = FRAME_TO_BOTTOM;
            }
            else
            {
                useCase = FRAME_TO_FRAME;
            }
            break;
        }
        case GNODE_TOP_FIELD:
        {
            if(isDisplayInterlaced)
            {
                if(isTopFieldOnDisplay)
                    useCase = TOP_TO_TOP;
                else
                    useCase = TOP_TO_BOTTOM;
            }
            else
            {
                useCase = TOP_TO_FRAME;
            }
            break;
        }
        case GNODE_BOTTOM_FIELD:
        {
            if(isDisplayInterlaced)
            {
                if(isTopFieldOnDisplay)
                    useCase = BOTTOM_TO_TOP;
                else
                    useCase = BOTTOM_TO_BOTTOM;
            }
            else
            {
                useCase = BOTTOM_TO_FRAME;
            }
            break;
        }
    }

    PLANE_TRC( TRC_ID_UNCLASSIFIED, "Use case = %d", useCase );
    return useCase;
}

void CDisplayPlane::AreSrcCharacteristicsChanged(CDisplayNode *pNode, bool isPictureRepeated)
 {
    // If the same picture is repeated, no need to check if the "source characteristics" have changed
    // because references have already been recomputed when this picture has been displayed for the first time,
    // otherwise, there is a problem when a pause is happened on a picture with new characteristics because
    // this picture will be repeated many times and the driver will think that the context has changed all the time.
     if(!isPictureRepeated)
     {
         if((pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) != (m_prevBufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED))
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.horizontal_decimation_factor != m_prevBufferDesc.src.horizontal_decimation_factor)
         ||(pNode->m_bufferDesc.src.vertical_decimation_factor   != m_prevBufferDesc.src.vertical_decimation_factor))
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.pixel_aspect_ratio.numerator   != m_prevBufferDesc.src.pixel_aspect_ratio.numerator)
         ||(pNode->m_bufferDesc.src.pixel_aspect_ratio.denominator != m_prevBufferDesc.src.pixel_aspect_ratio.denominator))
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.primary_picture.color_fmt   != m_prevBufferDesc.src.primary_picture.color_fmt)
         ||(pNode->m_bufferDesc.src.primary_picture.height      != m_prevBufferDesc.src.primary_picture.height)
         ||(pNode->m_bufferDesc.src.primary_picture.width       != m_prevBufferDesc.src.primary_picture.width)
         ||(pNode->m_bufferDesc.src.primary_picture.pitch       != m_prevBufferDesc.src.primary_picture.pitch)
         ||(pNode->m_bufferDesc.src.primary_picture.pixel_depth != m_prevBufferDesc.src.primary_picture.pixel_depth))
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.secondary_picture.color_fmt   != m_prevBufferDesc.src.secondary_picture.color_fmt)
         ||(pNode->m_bufferDesc.src.secondary_picture.height      != m_prevBufferDesc.src.secondary_picture.height)
         ||(pNode->m_bufferDesc.src.secondary_picture.width       != m_prevBufferDesc.src.secondary_picture.width)
         ||(pNode->m_bufferDesc.src.secondary_picture.pitch       != m_prevBufferDesc.src.secondary_picture.pitch)
         ||(pNode->m_bufferDesc.src.secondary_picture.pixel_depth != m_prevBufferDesc.src.secondary_picture.pixel_depth))
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.visible_area.height != m_prevBufferDesc.src.visible_area.height)
          ||(pNode->m_bufferDesc.src.visible_area.width != m_prevBufferDesc.src.visible_area.width)
          ||(pNode->m_bufferDesc.src.visible_area.x     != m_prevBufferDesc.src.visible_area.x)
          ||(pNode->m_bufferDesc.src.visible_area.y     != m_prevBufferDesc.src.visible_area.y))
            goto characteristics_changing;

        if(pNode->m_bufferDesc.src.linear_center_percentage != m_prevBufferDesc.src.linear_center_percentage)
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.src_frame_rate.numerator   != m_prevBufferDesc.src.src_frame_rate.numerator)
         ||(pNode->m_bufferDesc.src.src_frame_rate.denominator != m_prevBufferDesc.src.src_frame_rate.denominator))
            goto characteristics_changing;

        if(( (pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_CONST_ALPHA) != (m_prevBufferDesc.src.flags&STM_BUFFER_SRC_CONST_ALPHA))
         ||(pNode->m_bufferDesc.src.ulConstAlpha != m_prevBufferDesc.src.ulConstAlpha))
            goto characteristics_changing;

        if(pNode->m_bufferDesc.src.config_3D.formats != m_prevBufferDesc.src.config_3D.formats)
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.config_3D.parameters.field_alt.is_left_right_format != m_prevBufferDesc.src.config_3D.parameters.field_alt.is_left_right_format)
         ||(pNode->m_bufferDesc.src.config_3D.parameters.field_alt.vactive              != m_prevBufferDesc.src.config_3D.parameters.field_alt.vactive)
         ||(pNode->m_bufferDesc.src.config_3D.parameters.field_alt.vblank               != m_prevBufferDesc.src.config_3D.parameters.field_alt.vblank))
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.config_3D.parameters.frame_packed.is_left_right_format != m_prevBufferDesc.src.config_3D.parameters.frame_packed.is_left_right_format)
         ||(pNode->m_bufferDesc.src.config_3D.parameters.frame_packed.vactive_space1 != m_prevBufferDesc.src.config_3D.parameters.frame_packed.vactive_space1)
         ||(pNode->m_bufferDesc.src.config_3D.parameters.frame_packed.vactive_space2 != m_prevBufferDesc.src.config_3D.parameters.frame_packed.vactive_space2))
            goto characteristics_changing;

        if(pNode->m_bufferDesc.src.config_3D.parameters.frame_seq != m_prevBufferDesc.src.config_3D.parameters.frame_seq)
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.config_3D.parameters.l_d.vact_space != m_prevBufferDesc.src.config_3D.parameters.l_d.vact_space)
         ||(pNode->m_bufferDesc.src.config_3D.parameters.l_d.vact_video != m_prevBufferDesc.src.config_3D.parameters.l_d.vact_video))
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.config_3D.parameters.l_d__g_g.vact_space != m_prevBufferDesc.src.config_3D.parameters.l_d__g_g.vact_space)
         ||(pNode->m_bufferDesc.src.config_3D.parameters.l_d__g_g.vact_video != m_prevBufferDesc.src.config_3D.parameters.l_d__g_g.vact_video))
            goto characteristics_changing;

        if(pNode->m_bufferDesc.src.config_3D.parameters.picture_interleave != m_prevBufferDesc.src.config_3D.parameters.picture_interleave)
            goto characteristics_changing;

        if((pNode->m_bufferDesc.src.config_3D.parameters.sbs.is_left_right_format != m_prevBufferDesc.src.config_3D.parameters.sbs.is_left_right_format)
         ||(pNode->m_bufferDesc.src.config_3D.parameters.sbs.sbs_sampling_mode    != m_prevBufferDesc.src.config_3D.parameters.sbs.sbs_sampling_mode))
            goto characteristics_changing;

        // check if hdr_format has changed
        if(vibe_os_memcmp((void*)&(pNode->m_bufferDesc.src.hdr_fmt), (void*) &(m_prevBufferDesc.src.hdr_fmt), sizeof(stm_hdr_format_t))!=0)
        {
            if(m_capabilities & PLANE_CAPS_HDR_FORMAT)
            {
                // The plane support HDR
                goto characteristics_changing;
            }
            else
            {
                PLANE_TRC( TRC_ID_MAIN_INFO, "Plane doesn't support HDRIn Format!");
            }
        }
     }

     // Characteristics not changing
     return;

 characteristics_changing:
     PLANE_TRC( TRC_ID_CONTEXT_CHANGE, "Context changed (new source picture characteristics)");

     PLANE_TRC( TRC_ID_MAIN_INFO, "Node 0x%p with new pict charact:", pNode );

     PLANE_TRC( TRC_ID_MAIN_INFO, "Picture size in memory: W%d H%d P%d",
                 pNode->m_bufferDesc.src.primary_picture.width,
                 pNode->m_bufferDesc.src.primary_picture.height,
                 pNode->m_bufferDesc.src.primary_picture.pitch);

     PLANE_TRC( TRC_ID_MAIN_INFO, "Visible_area: x=%d, y=%d, w=%d, h=%d",
                 pNode->m_bufferDesc.src.visible_area.x,
                 pNode->m_bufferDesc.src.visible_area.y,
                 pNode->m_bufferDesc.src.visible_area.width,
                 pNode->m_bufferDesc.src.visible_area.height);

     PLANE_TRC( TRC_ID_MAIN_INFO, "FR%d %c",
                 ( pNode->m_bufferDesc.src.src_frame_rate.denominator ? (1000 * pNode->m_bufferDesc.src.src_frame_rate.numerator) / pNode->m_bufferDesc.src.src_frame_rate.denominator : 0),
                 ( (pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) ? 'I' : 'P')  );

     if(m_capabilities & PLANE_CAPS_HDR_FORMAT)
     {
       PLANE_TRC( TRC_ID_MAIN_INFO, "HDRIn Format : EOTF Type=%d ST2086 Metadata - %s, HDR Metadata - %s," ,
                   (int)pNode->m_bufferDesc.src.hdr_fmt.eotf_type,
                   pNode->m_bufferDesc.src.hdr_fmt.is_st2086_metadata_present ? "Present" : "NOT Present",
                   pNode->m_bufferDesc.src.hdr_fmt.is_hdr_metadata_present ?    "Present" : "NOT Present");
     }

     m_prevBufferDesc = (pNode->m_bufferDesc);

     m_ContextChanged = true;
 }

bool CDisplayPlane::IsContextChanged(CDisplayNode *pNodeToBeDisplayed, bool isPictureRepeated)
{
    // Check if source characteristic have changed
    AreSrcCharacteristicsChanged(pNodeToBeDisplayed, isPictureRepeated);

    // If any context parameters have changed (like source characteristic, transparency, input/output windows, ...)
    if(m_ContextChanged)
    {
        PLANE_TRCBL(TRC_ID_CONTEXT_CHANGE);
        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "*** Context has changed ***" );
        m_ContextChanged = false;
        return true;
    }

    // The context is unchanged
    return false;
}

uint32_t CDisplayPlane::GetHorizontalDecimationFactor(CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo)
{
    uint32_t horizontalDecimationFactor;

    switch(pNodeToDisplay->m_bufferDesc.src.horizontal_decimation_factor)
    {
        case STM_NO_DECIMATION:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "H1");
            horizontalDecimationFactor = 1;
            break;
        }
        case STM_DECIMATION_BY_TWO:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "H2");
            horizontalDecimationFactor = 2;
            break;
        }
        case STM_DECIMATION_BY_FOUR:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "H4");
            horizontalDecimationFactor = 4;
            break;
        }
        case STM_DECIMATION_BY_EIGHT:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "H8");
            horizontalDecimationFactor = 8;
            break;
        }
        default:
        {
            PLANE_TRC(TRC_ID_ERROR, "Invalid horizontal decimation factor");
            horizontalDecimationFactor = 1;
            break;
        }
    }

    return horizontalDecimationFactor;
}

uint32_t CDisplayPlane::GetVerticalDecimationFactor(CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo)
{
    uint32_t verticalDecimationFactor;

    switch(pNodeToDisplay->m_bufferDesc.src.vertical_decimation_factor)
    {
        case STM_NO_DECIMATION:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "V1");
            verticalDecimationFactor = 1;
            break;
        }
        case STM_DECIMATION_BY_TWO:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "V2");
            verticalDecimationFactor = 2;
            break;
        }
        case STM_DECIMATION_BY_FOUR:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "V4");
            verticalDecimationFactor = 4;
            break;
        }
        case STM_DECIMATION_BY_EIGHT:
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "V8");
            verticalDecimationFactor = 8;
            break;
        }
        default:
        {
            PLANE_TRC(TRC_ID_ERROR, "Invalid vertical decimation factor");
            verticalDecimationFactor = 1;
            break;
        }
    }

    return verticalDecimationFactor;
}

bool CDisplayPlane::IsScalingPossible(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo)
{
    pDisplayInfo->m_isSecondaryPictureSelected = false;
    return true;
}


void CDisplayPlane::FillSelectedPictureFormatInfo(CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo)
{
    switch(pDisplayInfo->m_selectedPicture.colorFmt)
    {
        case SURF_YCBCR420MB:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = false;
            pDisplayInfo->m_selectedPicture.isSrc420 = true;
            pDisplayInfo->m_selectedPicture.isSrc422 = false;
            break;
        }
        case SURF_YCBCR422MB:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = false;
            pDisplayInfo->m_selectedPicture.isSrc420 = false;
            pDisplayInfo->m_selectedPicture.isSrc422 = true;
            break;
        }
        case SURF_YUV420:
        case SURF_YVU420:
        case SURF_YCbCr420R2B:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = false;
            pDisplayInfo->m_selectedPicture.isSrc420 = true;
            pDisplayInfo->m_selectedPicture.isSrc422 = false;
            break;
        }
        case SURF_YCBCR422R:
        case SURF_YUYV:
        case SURF_YUV422P:
        case SURF_YCbCr422R2B:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = false;
            pDisplayInfo->m_selectedPicture.isSrc420 = false;
            pDisplayInfo->m_selectedPicture.isSrc422 = true;
            break;
        }

        case SURF_YCbCr422R2B10:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = true;
            pDisplayInfo->m_selectedPicture.isSrc420 = false;
            pDisplayInfo->m_selectedPicture.isSrc422 = true;
            break;
        }

        case SURF_YCbCr420R2B10:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = true;
            pDisplayInfo->m_selectedPicture.isSrc420 = true;
            pDisplayInfo->m_selectedPicture.isSrc422 = false;
            break;
        }

        case SURF_CRYCB101010:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = true;
            pDisplayInfo->m_selectedPicture.isSrc420 = false;
            pDisplayInfo->m_selectedPicture.isSrc422 = false;
            break;
        }

        case SURF_CRYCB888:
        case SURF_ACRYCB8888:
        {
            pDisplayInfo->m_selectedPicture.isSrcOn10bits = false;
            pDisplayInfo->m_selectedPicture.isSrc420 = false;
            pDisplayInfo->m_selectedPicture.isSrc422 = false;
            break;
        }
        default:
            break;
    }
}

void CDisplayPlane::FillSelectedPictureDisplayInfo(CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo)
{
    PLANE_TRC(TRC_ID_MAIN_INFO, "%s picture selected", pDisplayInfo->m_isSecondaryPictureSelected ? "Secondary" : "Primary");

    const stm_buffer_src_picture_desc_t& selectedPicture = pDisplayInfo->m_isSecondaryPictureSelected ?
                                                                pNodeToDisplay->m_bufferDesc.src.secondary_picture :
                                                                pNodeToDisplay->m_bufferDesc.src.primary_picture;

    // Copy selected picture info
    pDisplayInfo->m_selectedPicture.width      = selectedPicture.width;
    pDisplayInfo->m_selectedPicture.height     = selectedPicture.height;
    pDisplayInfo->m_selectedPicture.pitch      = selectedPicture.pitch;
    pDisplayInfo->m_selectedPicture.colorFmt   = selectedPicture.color_fmt;
    pDisplayInfo->m_selectedPicture.pixelDepth = selectedPicture.pixel_depth;

    // Fill source rectangle according to primary vs. secondary selection
    FillSrcFrameRect(pDisplayInfo, pDisplayInfo->m_selectedPicture.srcFrameRect);

    // Adjust frame height and pitch based on number of lines to skip in source picture
    pDisplayInfo->m_selectedPicture.srcFrameRect.y      /= pDisplayInfo->m_srcLinesSkipped + 1;
    pDisplayInfo->m_selectedPicture.srcFrameRect.height /= pDisplayInfo->m_srcLinesSkipped + 1;
    pDisplayInfo->m_selectedPicture.srcFramePitch        = selectedPicture.pitch * (pDisplayInfo->m_srcLinesSkipped + 1);

    // Compute frame height in field/frame coordinates
    pDisplayInfo->m_selectedPicture.srcHeight  = pDisplayInfo->m_isSrcInterlaced ?
                                                     pDisplayInfo->m_selectedPicture.srcFrameRect.height / 2 :
                                                     pDisplayInfo->m_selectedPicture.srcFrameRect.height;

    // Fill color format related picture info (bit depth, etc.)
    FillSelectedPictureFormatInfo(pNodeToDisplay, pDisplayInfo);

    // Now that picture is selected, adjust IO windows
    AdjustIOWindowsForHWConstraints(pNodeToDisplay, pDisplayInfo);
}

void CDisplayPlane::FillSrcFrameRect(const CDisplayInfo *pDisplayInfo, stm_rect_t& srcFrameRect) const
{
    if(pDisplayInfo->m_isSecondaryPictureSelected)
    {
        srcFrameRect.x      = pDisplayInfo->m_primarySrcFrameRect.x      / pDisplayInfo->m_horizontalDecimationFactor;
        srcFrameRect.y      = pDisplayInfo->m_primarySrcFrameRect.y      / pDisplayInfo->m_verticalDecimationFactor;
        srcFrameRect.width  = pDisplayInfo->m_primarySrcFrameRect.width  / pDisplayInfo->m_horizontalDecimationFactor;
        srcFrameRect.height = pDisplayInfo->m_primarySrcFrameRect.height / pDisplayInfo->m_verticalDecimationFactor;

        /*
         * Horizontal advance decimation is using a ponderated formula based on current and 7
         * following pixels for calculating current pixel value. So at the end of horizontal line,
         * the 7 pixel are coming from next line that's why we reduce visible area of some pixels.
         * Vertical advance decimation is using a ponderated formula based on current and 1
         * following pixel. So only the last vertical line could be bad, but it is not visible
         * that's why no need to remove it.
         */
        if(pDisplayInfo->m_horizontalDecimationFactor != 1)
        {
            const uint32_t SUPP_PIXEL_HORIZONTALLY = 4;

            /* Reduce visible area for removing last pixels that could be averaged with pixels of following line.*/
            if(srcFrameRect.width > SUPP_PIXEL_HORIZONTALLY)
            {
                srcFrameRect.width -= SUPP_PIXEL_HORIZONTALLY;
            }
        }

        if(pDisplayInfo->m_verticalDecimationFactor != 1)
        {
            const uint32_t SUPP_PIXEL_VERTICALLY = 0;

            /* Reduce visible area for removing last pixels that could be averaged with pixels of following lines.*/
            if(srcFrameRect.height > SUPP_PIXEL_VERTICALLY)
            {
                srcFrameRect.height -= SUPP_PIXEL_VERTICALLY;
            }
        }
    }
    else
    {
        srcFrameRect = pDisplayInfo->m_primarySrcFrameRect;
    }
}


/*
    PrepareIOWindows() will determine the IO Windows to use and decide if the decimated picture should be used (to achieve a bigger downscaling).
    The analyzis is always started with the primary picture. Then, when the IO windows are computed, the driver will check if it is necessary
    to use the secondary (aka decimated) picture.

    The IO Windows will be prepared with the following sequence:
    - We set the "SrcFrameRect". It can be either the  source_visible_area or the  Input Window requested by the application
    - We set the "dstFrameRect". It can be either the display_visible_area or the Output Window requested by the application

    - TruncateIOWindowsToLimits will then ensure that "SrcFrameRect" and "dstFrameRect" are not out of bounds.
    - AdjustIOWindowsForAspectRatioConv will reduce the size of "SrcFrameRect" and/or "dstFrameRect" to preserve the aspect ratio

    - At this stage the IO Windows are calculated so we can then check if a decimation is needed. This is done by a HW specific function.

    - A HW specific function will then be called to adapt the rectangles to the constraint specific to this HW (ex: rounding to even values, or multiple of N...)

    During each of those steps, the IO rectangles will be updated (NB: They can only become smaller).
    In case of error, "areIOWindowsValid" will be set and the rectangles set to null.
*/
bool CDisplayPlane::PrepareIOWindows(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo)
{
     pDisplayInfo->m_horizontalDecimationFactor = GetHorizontalDecimationFactor(pNodeToDisplay, pDisplayInfo);
     pDisplayInfo->m_verticalDecimationFactor   = GetVerticalDecimationFactor  (pNodeToDisplay, pDisplayInfo);
     pDisplayInfo->m_srcLinesSkipped            = 0;

    if (!SetSrcFrameRect(pNodeToDisplay, pDisplayInfo, pDisplayInfo->m_primarySrcFrameRect))
        goto error;

    if (!SetDstFrameRect(pNodeToDisplay, pDisplayInfo, pDisplayInfo->m_dstFrameRect))
        goto error;

    if (!TruncateIOWindowsToLimits(pNodeToDisplay,
                                   pDisplayInfo,
                                   pDisplayInfo->m_primarySrcFrameRect,
                                   pDisplayInfo->m_dstFrameRect))
        goto error;

    if (!AdjustIOWindowsForAspectRatioConv(pNodeToDisplay,
                                           pDisplayInfo,
                                           pDisplayInfo->m_primarySrcFrameRect,
                                           pDisplayInfo->m_dstFrameRect) )
        goto error;


    // Now that the srcFrameRect and dstFrameRect are known, check whether the scaling is possible
    // This function will select the secondary picture if necessary
    if (!IsScalingPossible(pNodeToDisplay, pDisplayInfo) )
        goto error;

    // Set scaling statistic in case of success
    if(pDisplayInfo->m_isSecondaryPictureSelected)
    {
        m_Statistics.IsScalingPossible       = SCALING_WITH_SECONDARY_PICTURE;
    }
    else
    {
        m_Statistics.IsScalingPossible       = SCALING_WITH_PRIMARY_PICTURE;
    }

    if(m_outputInfo.isDisplayInterlaced)
    {
      // Interlaced output

      /*
       * Note that:
       * - the start line must also be located on a top field to prevent fields
       *   getting accidentally swapped so "y" must be even.
       * - we do not support an "odd" number of lines on an interlaced display,
       *   there must always be a matched top and bottom field line.
       */
      pDisplayInfo->m_dstFrameRect.y      = pDisplayInfo->m_dstFrameRect.y & ~(0x1);
      pDisplayInfo->m_dstFrameRect.height = pDisplayInfo->m_dstFrameRect.height & ~(0x1);
      pDisplayInfo->m_dstHeight           = pDisplayInfo->m_dstFrameRect.height / 2;
    }
    else
    {
      // Progressive output
      pDisplayInfo->m_dstHeight      = pDisplayInfo->m_dstFrameRect.height;
    }

    // IO Windows calculated successfully!
    pDisplayInfo->m_areIOWindowsValid = true;

    // Save computed input and output window value to return to user in AUTO mode
    m_ComputedInputWindowValue = pDisplayInfo->m_primarySrcFrameRect;
    m_ComputedOutputWindowValue = pDisplayInfo->m_dstFrameRect;

    PLANE_TRC( TRC_ID_MAIN_INFO, "FR%d %c",
                 ( pNodeToDisplay->m_bufferDesc.src.src_frame_rate.denominator ? (1000 * pNodeToDisplay->m_bufferDesc.src.src_frame_rate.numerator) / pNodeToDisplay->m_bufferDesc.src.src_frame_rate.denominator : 0),
                 ( (pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) ? 'I' : 'P')  );

    PLANE_TRC(TRC_ID_MAIN_INFO, "New IO Windows");

    PLANE_TRC(TRC_ID_MAIN_INFO, "Selected picture: W=%d H=%d P=%d",
        pDisplayInfo->m_selectedPicture.width,
        pDisplayInfo->m_selectedPicture.height,
        pDisplayInfo->m_selectedPicture.pitch);

    PLANE_TRC(TRC_ID_MAIN_INFO, "srcFrameRect: x=%4d, y=%4d, w=%4d, h=%4d",
        pDisplayInfo->m_selectedPicture.srcFrameRect.x,
        pDisplayInfo->m_selectedPicture.srcFrameRect.y,
        pDisplayInfo->m_selectedPicture.srcFrameRect.width,
        pDisplayInfo->m_selectedPicture.srcFrameRect.height);

    PLANE_TRC(TRC_ID_MAIN_INFO, "srcLinesSkipped=%u => srcFramePitch=%u",
        pDisplayInfo->m_srcLinesSkipped,
        pDisplayInfo->m_selectedPicture.srcFramePitch);

    PLANE_TRC(TRC_ID_MAIN_INFO, "srcHeight=%4d", pDisplayInfo->m_selectedPicture.srcHeight);

    PLANE_TRC(TRC_ID_MAIN_INFO, "dstFrameRect: x=%4d, y=%4d, w=%4d, h=%4d",
        pDisplayInfo->m_dstFrameRect.x,
        pDisplayInfo->m_dstFrameRect.y,
        pDisplayInfo->m_dstFrameRect.width,
        pDisplayInfo->m_dstFrameRect.height);

    PLANE_TRC(TRC_ID_MAIN_INFO, "dstHeight=%4d", pDisplayInfo->m_dstHeight);

    return true;

error:
    // This reset will set m_areIOWindowsValid to false and all the rectangles to null
    pDisplayInfo->Reset();

    ResetComputedInputOutputWindowValues();

    // Reset statistics in case of error
    m_Statistics.IsScalingPossible       = SCALING_NOT_POSSIBLE;
    m_Statistics.DataRateUseCaseInMBperS = 0;
    m_Statistics.ColorimetryConversion   = 0;

    PLANE_TRC( TRC_ID_ERROR, "Failed to set the IO Windows!" );

    return false;
}

bool CDisplayPlane::TruncateIOWindowsToLimits(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect, stm_rect_t &dstFrameRect) const
{
  int32_t       srcX         = srcFrameRect.x / 16;
  int32_t       srcY         = srcFrameRect.y / 16;
  stm_rect_t    visible_area = pNodeToDisplay->m_bufferDesc.src.visible_area;
  stm_rect_t    display_visible_area = m_outputInfo.displayVisibleArea;
  int32_t       nbr_of_bytes_truncated;

  /* This function is going to check the overlap between various rectangles. Here are the rectangles involved:

     For the source:
     - First there is the full picture size in memory. This info is not needed here.
     - Then there is "SrcVisibleArea" which specifies the only area that should be displayed.
       The content outside this area should be ignored by the display.
     - Then there is "srcFrameRect" which corresponds to what part of the source picture we are asked to display.

     For the output:
     - There is the "DisplayVisibleArea": Nothing can be displayed outside this area.
     - There is "dstFrameRect" which corresponds to where on the output screen we are asked to display.
  */

  if ( (srcFrameRect.width  == 0) ||
       (srcFrameRect.height == 0) ||
       (dstFrameRect.width  == 0) ||
       (dstFrameRect.height == 0) )
  {
    goto nothing_to_display;
  }

  if( IsSrcRectFullyOutOfBounds(pNodeToDisplay, pDisplayInfo, srcFrameRect) ||
      IsDstRectFullyOutOfBounds(pNodeToDisplay, pDisplayInfo, dstFrameRect) )
  {
    goto nothing_to_display;
  }

  //Truncate srcFrameRect to srcVisibleArea
  if (srcX < visible_area.x)
  {
    nbr_of_bytes_truncated = visible_area.x - srcX;    // We are sure that "nbr_of_bytes_truncated" is positive
    srcFrameRect.width -= nbr_of_bytes_truncated;

    srcX = visible_area.x;
    srcFrameRect.x = visible_area.x;
  }
  if(srcY < visible_area.y)
  {
    nbr_of_bytes_truncated = visible_area.y - srcY;    // We are sure that "nbr_of_bytes_truncated" is positive
    srcFrameRect.height -= nbr_of_bytes_truncated;

    srcY = visible_area.y;
    srcFrameRect.y = visible_area.y;
  }

  if(srcX + (int32_t)srcFrameRect.width > visible_area.x + (int32_t)visible_area.width)
  {
    nbr_of_bytes_truncated = (srcX + (int32_t)srcFrameRect.width) - (visible_area.x + (int32_t)visible_area.width);    // We are sure that "nbr_of_bytes_truncated" is positive
    srcFrameRect.width -= nbr_of_bytes_truncated;
  }
  if(srcY + (int32_t)srcFrameRect.height > visible_area.y + (int32_t)visible_area.height)
  {
    nbr_of_bytes_truncated = (srcY + (int32_t)srcFrameRect.height) - (visible_area.y + (int32_t)visible_area.height);  // We are sure that "nbr_of_bytes_truncated" is positive
    srcFrameRect.height -= nbr_of_bytes_truncated;
  }


  //Truncate dstFrameRect to DisplayVisibleArea
  if (dstFrameRect.x < display_visible_area.x)
  {
    nbr_of_bytes_truncated = display_visible_area.x - dstFrameRect.x;    // We are sure that "nbr_of_bytes_truncated" is positive
    dstFrameRect.width -= nbr_of_bytes_truncated;
    dstFrameRect.x = display_visible_area.x;
  }
  if(dstFrameRect.y < display_visible_area.y)
  {
    nbr_of_bytes_truncated = display_visible_area.y - dstFrameRect.y;    // We are sure that "nbr_of_bytes_truncated" is positive
    dstFrameRect.height -= nbr_of_bytes_truncated;
    dstFrameRect.y = display_visible_area.y;
  }

  if(dstFrameRect.x + (int32_t)dstFrameRect.width > display_visible_area.x + (int32_t)display_visible_area.width)
  {
    nbr_of_bytes_truncated = (dstFrameRect.x + (int32_t)dstFrameRect.width) - (display_visible_area.x + (int32_t)display_visible_area.width);    // We are sure that "nbr_of_bytes_truncated" is positive
    dstFrameRect.width -= nbr_of_bytes_truncated;
  }
  if(dstFrameRect.y + (int32_t)dstFrameRect.height > display_visible_area.y + (int32_t)display_visible_area.height)
  {
    nbr_of_bytes_truncated = (dstFrameRect.y + (int32_t)dstFrameRect.height) - (display_visible_area.y + (int32_t)display_visible_area.height);  // We are sure that "nbr_of_bytes_truncated" is positive
    dstFrameRect.height -= nbr_of_bytes_truncated;
  }

  return true;

nothing_to_display:
  // Indicate that IO windows are invalids but incoming picture will not be refused.
  PLANE_TRC(TRC_ID_ERROR, "Invalid src or dst rect! Src=(%d,%d,%d,%d) Dst=(%d,%d,%d,%d)",
        srcFrameRect.x,
        srcFrameRect.y,
        srcFrameRect.width,
        srcFrameRect.height,
        dstFrameRect.x,
        dstFrameRect.y,
        dstFrameRect.width,
        dstFrameRect.height);

  return false;
}

bool CDisplayPlane::IsSrcRectFullyOutOfBounds(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect) const
{
  stm_rect_t    visible_area = pNodeToDisplay->m_bufferDesc.src.visible_area;

  int32_t x = srcFrameRect.x/16;
  int32_t y = srcFrameRect.y/16;

  if( (x > (visible_area.x + (int32_t)visible_area.width) )  ||
      (x + (int32_t)srcFrameRect.width < visible_area.x)     ||
      (y > (visible_area.y + (int32_t)visible_area.height) ) ||
      (y + (int32_t)srcFrameRect.height < visible_area.y) )
    return true;
  else
    return false;
}

bool CDisplayPlane::IsDstRectFullyOutOfBounds(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &dstFrameRect) const
{
  stm_rect_t    display_visible_area = m_outputInfo.displayVisibleArea;

  if( (dstFrameRect.x > (display_visible_area.x + (int32_t)display_visible_area.width) )  ||
      (dstFrameRect.x + (int32_t)dstFrameRect.width < display_visible_area.x)             ||
      (dstFrameRect.y > (display_visible_area.y + (int32_t)display_visible_area.height) ) ||
      (dstFrameRect.y + (int32_t)dstFrameRect.height < display_visible_area.y) )
    return true;
  else
    return false;
}

bool CDisplayPlane::AdjustIOWindowsForAspectRatioConv(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect, stm_rect_t &dstFrameRect) const
{
  // SAR = Source  Aspect Ratio
  // DAR = Display Aspect Ratio
  uint64_t       sar_numerator, sar_denominator;
  uint64_t       dar_numerator, dar_denominator;
  uint64_t       ratioNum, ratioDen;
  stm_rational_t srcPixelAspectRatio, displayPixelAspectRatio;
  stm_plane_aspect_ratio_conversion_mode_t aspectRatioConversionMode;

  // Aspect ratio conversion is always done with information from the PRIMARY picture
  srcPixelAspectRatio = pNodeToDisplay->m_bufferDesc.src.pixel_aspect_ratio;
  displayPixelAspectRatio = m_outputInfo.pixelAspectRatio;

  if( (m_InputWindowMode == MANUAL_MODE) && (m_OutputWindowMode == MANUAL_MODE) )
  {
    PLANE_TRC( TRC_ID_MAIN_INFO, "Input and Output Windows in manual mode: Aspect Ratio cannot be preserved");
    return true;
  }

  PLANE_TRC( TRC_ID_MAIN_INFO, "Src  pixel AR: %d / %d", srcPixelAspectRatio.numerator, srcPixelAspectRatio.denominator);
  PLANE_TRC( TRC_ID_MAIN_INFO, "Disp pixel AR: %d / %d", displayPixelAspectRatio.numerator, displayPixelAspectRatio.denominator);

  // NB: uint32_t is not big enough for all those calculations
  sar_numerator   = ((uint64_t)srcPixelAspectRatio.numerator       * (uint64_t)srcFrameRect.width );
  sar_denominator = ((uint64_t)srcPixelAspectRatio.denominator     * (uint64_t)srcFrameRect.height);

  dar_numerator   = ((uint64_t)displayPixelAspectRatio.numerator   * (uint64_t)dstFrameRect.width );
  dar_denominator = ((uint64_t)displayPixelAspectRatio.denominator * (uint64_t)dstFrameRect.height);

  PLANE_TRC( TRC_ID_MAIN_INFO, "Src  AR: %llu / %llu", sar_numerator, sar_denominator);
  PLANE_TRC( TRC_ID_MAIN_INFO, "Disp AR: %llu / %llu", dar_numerator, dar_denominator);

  if ( (sar_numerator   == 0) ||
       (sar_denominator == 0) ||
       (dar_numerator   == 0) ||
       (dar_denominator == 0) )
  {
    PLANE_TRC( TRC_ID_ERROR, "Invalid sar or dar!" );
    return false;
  }

  vibe_os_rational_reduction(&sar_numerator, &sar_denominator);
  PLANE_TRC( TRC_ID_MAIN_INFO, "vibe_os_rational_reduction(): sar_numerator=%llu, sar_denominator=%llu", sar_numerator, sar_denominator);

  vibe_os_rational_reduction(&dar_numerator, &dar_denominator);
  PLANE_TRC( TRC_ID_MAIN_INFO, "vibe_os_rational_reduction(): dar_numerator=%llu, dar_denominator=%llu", dar_numerator, dar_denominator);

  // RATIO = SAR / DAR = (sar_numerator * dar_denominator) / (dar_numerator * sar_denominator)
  ratioNum = (uint64_t)sar_numerator * (uint64_t)dar_denominator;
  ratioDen = (uint64_t)dar_numerator * (uint64_t)sar_denominator;

  vibe_os_rational_reduction(&ratioNum, &ratioDen);
  PLANE_TRC( TRC_ID_MAIN_INFO, "ratioNum=%llu, ratioDen=%llu", ratioNum, ratioDen);

  // Sanity check to prevent division by 0 in the following code
  if ( (ratioNum == 0) ||
       (ratioDen == 0) )
  {
    PLANE_TRC( TRC_ID_ERROR, "Invalid ratioNum or ratioDen! (0x%llx / 0x%llx)", ratioNum, ratioDen);
    return false;
  }

  aspectRatioConversionMode = m_AspectRatioConversionMode;

  /* In case of 3D, the aspect ratio conversion is disabled because there are strong constraints on horizontal
     and vertical cropping (to avoid a risk of mismatch between the 2 views) */
  if ( (pNodeToDisplay->m_bufferDesc.src.config_3D.formats != FORMAT_3D_NONE) &&
       (m_outputInfo.display3DModeFlags != STM_MODE_FLAGS_NONE) )
  {
    /* 3D source and 3D display: Force the "Ignore" mode */
    aspectRatioConversionMode = ASPECT_RATIO_CONV_IGNORE;
  }

  // Resize input/output window according to aspect ratio conversion mode selected
  switch(aspectRatioConversionMode)
  {
    case ASPECT_RATIO_CONV_LETTER_BOX:
      PLANE_TRC( TRC_ID_MAIN_INFO, "AR Conv: LetterBox");
      if(ratioNum < ratioDen)
      {
        uint32_t new_width;
        new_width           = (uint32_t) vibe_os_div64( (ratioNum * dstFrameRect.width), ratioDen );
        dstFrameRect.x      = dstFrameRect.x + (dstFrameRect.width - new_width) / 2;
        dstFrameRect.width  = new_width;
      }
      else
      {
        uint32_t new_height;
        new_height          = (uint32_t) vibe_os_div64( (ratioDen * dstFrameRect.height), ratioNum );
        dstFrameRect.y      = dstFrameRect.y + (dstFrameRect.height - new_height) / 2;
        dstFrameRect.height = new_height;
      }
      break;
    case ASPECT_RATIO_CONV_PAN_AND_SCAN:
      PLANE_TRC( TRC_ID_MAIN_INFO, "AR Conv: Pan&Scan");
      if(ratioNum < ratioDen)
      {
        uint32_t new_height;
        new_height          = (uint32_t) vibe_os_div64( (ratioNum * srcFrameRect.height), ratioDen );
        // srcFrameRect.y is equal to ( srcFrameRect.y/16 + (srcFrameRect.height - new_height) / 2 ) * 16;
        srcFrameRect.y      = srcFrameRect.y + (srcFrameRect.height - new_height) * 8;
        srcFrameRect.height = new_height;
      }
      else
      {
        uint32_t new_width;
        new_width           = (uint32_t) vibe_os_div64( (ratioDen * srcFrameRect.width), ratioNum );
        // srcFrameRect.x is equal to ( srcFrameRect.x/16 + (srcFrameRect.width - new_width) / 2 ) * 16;
        srcFrameRect.x      = srcFrameRect.x + (srcFrameRect.width - new_width) * 8;
        srcFrameRect.width  = new_width;
      }
      break;
    case ASPECT_RATIO_CONV_COMBINED:
      PLANE_TRC( TRC_ID_MAIN_INFO, "AR Conv: Combined");
      if(ratioNum < ratioDen)
      {
        uint32_t new_height;
        uint32_t new_width;
        new_height          = (uint32_t) vibe_os_div64( (srcFrameRect.height * (ratioDen + ratioNum)), (2L * ratioDen) );
        // srcFrameRect.y is equal to ( srcFrameRect.y/16 + (srcFrameRect.height - new_height) / 2 ) * 16;
        srcFrameRect.y      = srcFrameRect.y + (srcFrameRect.height - new_height) * 8;
        srcFrameRect.height = new_height;

        new_width           = (uint32_t) vibe_os_div64( (dstFrameRect.width * (ratioDen + ratioNum)), (2L * ratioDen) );
        dstFrameRect.x      = dstFrameRect.x + (dstFrameRect.width - new_width) / 2;
        dstFrameRect.width  = new_width;
      }
      else
      {
        uint32_t new_height;
        uint32_t new_width;
        new_width           = (uint32_t) vibe_os_div64( (srcFrameRect.width * 2 * ratioDen), (ratioDen + ratioNum) );
        // srcFrameRect.x is equal to ( srcFrameRect.x/16 + (srcFrameRect.width - new_width) / 2 ) * 16;
        srcFrameRect.x      = srcFrameRect.x + (srcFrameRect.width - new_width) * 8;
        srcFrameRect.width  = new_width;

        new_height          = (uint32_t) vibe_os_div64( (dstFrameRect.height * 2 * ratioDen), (ratioDen + ratioNum) );
        dstFrameRect.y      = dstFrameRect.y + (dstFrameRect.height - new_height) / 2;
        dstFrameRect.height = new_height;
      }
      break;
    case ASPECT_RATIO_CONV_IGNORE:
      PLANE_TRC( TRC_ID_MAIN_INFO, "AR Conv: Ignore");
      break;
    default:
      break;
  }

  return true;
}


bool CDisplayPlane::AdjustIOWindowsForHWConstraints(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo) const
{
    return true;
}


// Set source rectangle (in Frame notation)
bool CDisplayPlane::SetSrcFrameRect(CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &srcFrameRect) const
{
  if (m_InputWindowMode == AUTO_MODE)
  {
    // Take the full input picture
    srcFrameRect = pNodeToDisplay->m_bufferDesc.src.visible_area;
  }
  else
  {
    // Take the requested input window
    srcFrameRect = m_InputWindowValue;
  }

  // Switch x and y in sixteenth of pixel unit
  srcFrameRect.x *= 16;
  srcFrameRect.y *= 16;

  return true;
}


// Set destination rectangle (in Frame notation)
bool CDisplayPlane::SetDstFrameRect(CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo, stm_rect_t &dstFrameRect) const
{
  if (m_OutputWindowMode == AUTO_MODE)
  {
    // Take the full screen
    dstFrameRect = m_outputInfo.displayVisibleArea;
  }
  else
  {
  // Take the requested output window
    dstFrameRect = m_OutputWindowValue;
  }

  return true;
}


// Information filled during the VSync interrupt when the buffer is going to be displayed
bool CDisplayPlane::FillDisplayInfo(CDisplayNode *pNodeToDisplay, CDisplayInfo *pDisplayInfo)
{
    /* set isSrcInterlaced */
    pDisplayInfo->m_isSrcInterlaced = ((pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) == STM_BUFFER_SRC_INTERLACED);

    return true;
}


void CDisplayPlane::ResetDisplayInfo(void)
{
    vibe_os_zero_memory(&m_outputInfo, sizeof(m_outputInfo));
}


void CDisplayPlane::UpdateOuptutInfo(void)
{
    const stm_display_mode_t*   pCurrentMode;
    stm_rational_t              displayAspectRatio;
    stm_ycbcr_colorspace_t      outputColorSpace;
    stm_hdr_format_t            outputHDRFormat;

    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    if(!m_pOutput)
    {
        // Plane not connected to an output
        goto output_info_not_available;
    }

    if(!m_pOutput->IsStarted())
    {
        // Ouptut not started
        goto output_info_not_available;
    }

    m_outputInfo.outputVSyncDurationInUs = m_pOutput->GetFieldOrFrameDuration();
    pCurrentMode = m_pOutput->GetCurrentDisplayMode();

    if(!pCurrentMode)
    {
        // Output not started
        goto output_info_not_available;
    }

    if ((m_outputInfo.currentMode.mode_id != pCurrentMode->mode_id) ||
        (m_outputInfo.currentMode.mode_params.flags != pCurrentMode->mode_params.flags))
    {
        // The display mode has changed: Save the info about the new one
        m_outputInfo.isDisplayInterlaced = (pCurrentMode->mode_params.scan_type == STM_INTERLACED_SCAN);

        m_outputInfo.currentMode = *pCurrentMode;

        m_outputInfo.displayVisibleArea.x      = 0;
        m_outputInfo.displayVisibleArea.y      = 0;
        m_outputInfo.displayVisibleArea.width  = pCurrentMode->mode_params.active_area_width;
        m_outputInfo.displayVisibleArea.height = pCurrentMode->mode_params.active_area_height;

        m_outputInfo.display3DModeFlags = pCurrentMode->mode_params.flags & STM_MODE_FLAGS_3D_MASK;

        m_outputInfo.pixel_clock_freq = pCurrentMode->mode_timing.pixel_clock_freq;

        // (flush history)
        m_ContextChanged = true;
        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new output mode)" );

        PLANE_TRC( TRC_ID_MAIN_INFO, "New display mode: %dx%d%c%d (Mode %d)",
                m_outputInfo.displayVisibleArea.width,
                m_outputInfo.displayVisibleArea.height,
                m_outputInfo.isDisplayInterlaced?'I':'P',
                m_outputInfo.currentMode.mode_params.vertical_refresh_rate,
                m_outputInfo.currentMode.mode_id );
    }

    // Get the Display Aspect Ratio...
    if(m_pOutput->GetCompoundControl(OUTPUT_CTRL_DISPLAY_ASPECT_RATIO, &(displayAspectRatio)) != STM_OUT_OK)
    {
        PLANE_TRC( TRC_ID_ERROR, "Impossible to get the current display aspect ratio!" );
        goto output_info_not_available;
    }

    // ...and check if it has changed
    if((displayAspectRatio.denominator != m_outputInfo.aspectRatio.denominator)
     ||(displayAspectRatio.numerator   != m_outputInfo.aspectRatio.numerator))
    {
        // Save the new aspect ratio
        m_outputInfo.aspectRatio = displayAspectRatio;

        // (flush history)
        m_ContextChanged = true;
        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new output AR)" );
    }

    // Warning: The pixelAspectRatio can change even if the displayAspectRatio didn't change.
    // Example: Output mode changed from 1920x1080 (16/9) to 720x576 (16/9)
    //          Both are 16/9 but:
    //          In the 1st case, the pixelAspectRatio is 1:1
    //          In the 2nd case, the pixelAspectRatio is (16*576)/(9*720) = 1.4222
    m_outputInfo.pixelAspectRatio.numerator   = m_outputInfo.aspectRatio.numerator * pCurrentMode->mode_params.active_area_height;
    m_outputInfo.pixelAspectRatio.denominator = m_outputInfo.aspectRatio.denominator * pCurrentMode->mode_params.active_area_width;
    PLANE_TRC( TRC_ID_MAIN_INFO, "New display aspect ratio: %d/%d", m_outputInfo.aspectRatio.numerator, m_outputInfo.aspectRatio.denominator );

    // Get the current Output Color Space...
    m_pOutput->GetOutputColorspace(outputColorSpace);
    // ...and check if it has changed
    if(outputColorSpace != m_outputInfo.outputColorSpace)
    {
        // Save the new output color space
        m_outputInfo.prevOutputColorSpace = m_outputInfo.outputColorSpace;
        m_outputInfo.outputColorSpace = outputColorSpace;

        // (flush history)
        m_ContextChanged = true;
        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "Context changed (new output CS = %d)", (int)outputColorSpace);
    }

    if(m_capabilities & PLANE_CAPS_HDR_FORMAT)
    {
      // Get the current Output Dynamic Range Format...
      if(m_pOutput->GetOutputHDRFormat(outputHDRFormat))
      {
        // HDR supported by the output ... check if it has changed
        if(vibe_os_memcmp((void*)&outputHDRFormat, (void*) &(m_outputInfo.outputHDRFormat), sizeof(stm_hdr_format_t))!=0)
        {
            // Save the new output HDR Format
            m_outputInfo.outputHDRFormat = outputHDRFormat;

            /* Simply update HDROUT Format */
            m_updateHDROutFormat = true;
            PLANE_TRC( TRC_ID_CONTEXT_CHANGE, "Context changed (new HDROut Format : EOTF Type=%d ST2086 Metadata - %s, HDR Metadata - %s,",
                (int)outputHDRFormat.eotf_type,
                outputHDRFormat.is_st2086_metadata_present ? "Present" : "NOT Present",
                outputHDRFormat.is_hdr_metadata_present ?    "Present" : "NOT Present");
        }
      }
      else
      {
          PLANE_TRC( TRC_ID_CONTEXT_CHANGE, "HDR format is not supported on this output!" );
          // Don't issue an error as this would break display on outputs without HDROut support (i.e SD output)
      }
    }

    m_outputInfo.isOutputStarted = true;

    UpdateOutputInfoSavedInSource();


    return;

output_info_not_available:
    // This plane is not connected to an output or this output is currently not started
    ResetDisplayInfo();
    m_outputInfo.isOutputStarted = false;

    UpdateOutputInfoSavedInSource();

    return;
}


void CDisplayPlane::UpdateOutputInfoSavedInSource(void)
{
    // Update source saved information about the output info that are just changing: TimingID, ...
    CSourceInterface *source_interface;

    if (m_pSource)
    {
        // A source is connected to this plane so get its interface
        source_interface = m_pSource->GetInterface(STM_SOURCE_QUEUE_IFACE);
        // for update output info saved in the source interface
        if (source_interface)
        {
            source_interface->UpdateOutputInfo(this);
        }
    }
}

void CDisplayPlane::SendOutputModeChangedEvent(void)
{
    CSourceInterface *source_interface;

    if (m_pSource)
    {
        // A source is connected to this plane so get its interface
        source_interface = m_pSource->GetInterface(STM_SOURCE_QUEUE_IFACE);

        if (source_interface)
        {
            source_interface->SendOutputModeChangedEvent();
        }
    }
}

// Function called when connections between plane and output changed
// Notify the source (if any)
// Notification about Source-Plane connections are handled directly by the source itself
void CDisplayPlane::SendConnectionsChangedEvent(void)
{
    CSourceInterface *source_interface;

    if (m_pSource)
    {
        // A source is connected to this plane so get its interface
        source_interface = m_pSource->GetInterface(STM_SOURCE_QUEUE_IFACE);

        if (source_interface)
        {
            source_interface->SendConnectionsChangedEvent();
        }
    }
}


bool  CDisplayPlane::RegisterStatistics(void)
{
    char Tag[STM_REGISTRY_MAX_TAG_SIZE];

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_PicDisplayed" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.PicDisplayed, sizeof( m_Statistics.PicDisplayed )) != 0 )
    {
        PLANE_TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        PLANE_TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_PicRepeated" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.PicRepeated, sizeof( m_Statistics.PicRepeated )) != 0 )
    {
        PLANE_TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        PLANE_TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_CurDispPicPTS" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.CurDispPicPTS, sizeof( m_Statistics.CurDispPicPTS )) != 0 )
    {
        PLANE_TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        PLANE_TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "SourceId" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.SourceId, sizeof( m_Statistics.SourceId )) != 0 )
    {
        PLANE_TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        PLANE_TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_FieldInversions" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.FieldInversions, sizeof( m_Statistics.FieldInversions )) != 0 )
    {
        PLANE_TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        PLANE_TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_UnexpectedFieldInversions" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.UnexpectedFieldInversions, sizeof( m_Statistics.UnexpectedFieldInversions )) != 0 )
    {
        PLANE_TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        PLANE_TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_IsScalingPossible" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.IsScalingPossible, sizeof( m_Statistics.IsScalingPossible )) != 0 )
    {
        TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_DataRateUseCaseInMBperS" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.DataRateUseCaseInMBperS, sizeof( m_Statistics.DataRateUseCaseInMBperS )) != 0 )
    {
        TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    vibe_os_zero_memory( &Tag, STM_REGISTRY_MAX_TAG_SIZE );
    vibe_os_snprintf( Tag, sizeof( Tag ), "Stat_ColorimetryConversion" );
    if ( stm_registry_add_attribute(( stm_object_h )this, Tag, STM_REGISTRY_UINT32, &m_Statistics.ColorimetryConversion, sizeof( m_Statistics.ColorimetryConversion )) != 0 )
    {
        TRC( TRC_ID_ERROR, "Cannot register '%s' attribute ( %d )", Tag, m_planeID );
        return false;
    }
    else
    {
        TRC( TRC_ID_MAIN_INFO, "Registered '%s' object ( %d )", Tag, m_planeID );
    }

    return true;
}


/*
 * Generic handling of OutputVSync in threaded IRQ.
 * This may be overloaded in CDisplayPlane daughter classes
 * depending if OutputVsync should be handled in IRQ.
 */
void CDisplayPlane::OutputVSyncThreadedIrqUpdateHW(bool isDisplayInterlaced, bool isTopFieldOnDisplay, const stm_time64_t &vsyncTime)
{
    PLANE_TRC(TRC_ID_VSYNC, "%s at %llu", GetName(), vsyncTime);

    /* Process the CRC computed during the last VSync period.
       Those CRC are related to the picture displayed during the previous period (=  m_picturesUsedByHw.pCurNode) */
    ProcessLastVsyncStatus(vsyncTime, m_picturesUsedByHw.pCurNode);

    // A new VSync has happened: The pictures prepared for next VSync become the pictures used by the HW
    m_picturesUsedByHw = m_picturesPreparedForNextVSync;

    // A new triplet is going to be chosen for next VSync
    ResetTriplet(&m_picturesPreparedForNextVSync);

    if(m_updateDepth)
    {
        // Reset flag to indicate that it has been executed
        m_updateDepth = false;

        if(!SetDepth(m_pOutput, m_lastRequestedDepth, true))
        {
            PLANE_TRC(TRC_ID_ERROR, "SetDepth failed on output 0x%p!", m_pOutput);
        }
    }

    ProcessControlQueue(vsyncTime);

    return;
}

void CDisplayPlane::UpdatePictureDisplayedStatistics(CDisplayNode       *pCurrNode,
                                                     bool                isPictureRepeated,
                                                     bool                isDisplayInterlaced,
                                                     bool                isTopFieldOnDisplay)
{
    bool isNextVSyncTop;

    if (not isPictureRepeated)
    {
        m_Statistics.PicDisplayed++;
    }
    else
    {
        m_Statistics.PicRepeated++;
    }

    if(isDisplayInterlaced & (pCurrNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED))
    {
        // Source and Display are Interlaced: Check if there is a Field Inversion

        // NB: Field Inversions are normal in some cases like FRC or Trickmode.
        //     StreamingEngine is setting the flag STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY when no
        //     FieldInversion is expected.

        // The next VSync will have the opposite polarity of current polarity
        isNextVSyncTop = !isTopFieldOnDisplay;

        if ( ( (pCurrNode->m_srcPictureType == GNODE_BOTTOM_FIELD) &&  isNextVSyncTop) ||
             ( (pCurrNode->m_srcPictureType == GNODE_TOP_FIELD)    && !isNextVSyncTop) )
        {
            m_Statistics.FieldInversions++;

            // Check if this field inversion is expected
            if(pCurrNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY)
            {
                // Field inversion while STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY is set.
                // This can happen at the end of a playback when the plane has nothing else to display
                // (so it keeps repeating the last picture)
                m_Statistics.UnexpectedFieldInversions++;
                PLANE_TRC(TRC_ID_FIELD_INVERSION, "Unexpected Field Inversion" );
            }
        }
    }
}

void CDisplayPlane::ClearColorimetryConversionStatistics()
{
    m_Statistics.ColorimetryConversion = 0;
}


void CDisplayPlane::UpdateColorimetryConversionStatistics(
                                    const CDisplayNode*     pDisplayNode)
{
    /* ColorimetryConversion == ((input frame colorimetry) * 1000) + (output colorimetry)  */
    /* Hence: 1001 means input is BT609 and output is BT609                                */
    /*        2003 means input is BT709 and output is BT2020                               */
    /*        3002 means input is BT2020 and output is BT709                               */
    m_Statistics.ColorimetryConversion = (COLORSPACE_FROM_FLAGS(pDisplayNode->m_bufferDesc.src.flags) * 1000) + m_outputInfo.outputColorSpace;
}


void CDisplayPlane::ResetTriplet(display_triplet_t *pTriplet)
{
    pTriplet->pPrevNode = 0;
    pTriplet->pCurNode  = 0;
    pTriplet->pNextNode = 0;
}

/*
 * Create the infrastructure needed to handle Pause.
 */
bool CDisplayPlane::PauseCreateWorkQueue()
{
    char queue_name[QUEUE_NAME_MAX_LENGTH];

    // Create the work queue for pause buffer copy
    vibe_os_snprintf (queue_name, sizeof(queue_name), "VIB-Pause-%s", m_name);
    m_PauseWorkQueue = vibe_os_create_work_queue(queue_name);
    if (!m_PauseWorkQueue)
    {
        PLANE_TRC(TRC_ID_ERROR, "Cannot create work queue %s", queue_name);
        return false;
    }

    m_pPauseCopyBufParams = (struct PauseCopyBufParams *)vibe_os_allocate_workerdata(PauseBufferCopyWorkFunction, sizeof(struct PauseCopyBufParams));
    if (!m_pPauseCopyBufParams)
    {
        PLANE_TRC(TRC_ID_ERROR, "Cannot allocate worker data for %s", queue_name);
        return false;
    }
    return true;
}


void CDisplayPlane::HandlePause(CDisplayNode       **ppPrevNode,
                                CDisplayNode       **ppCurrNode,
                                CDisplayNode       **ppNextNode)
{
    bool r = false;

    switch(m_PauseState)
    {
    case PAUSESTATE_DISABLED:
        // No current request to pause this plane
        break;
    case PAUSESTATE_PAUSE_REQUESTED:
        PLANE_TRC(TRC_ID_MAIN_INFO, "PAUSESTATE_PAUSE_REQUESTED");
        if (!ppCurrNode)
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "nothing to display - don't start the pause");
            m_PauseState = PAUSESTATE_DISABLED;
            m_status  &= ~STM_STATUS_PLANE_PAUSED;
            break;
        }
        /* starting the copy work queue */
        PLANE_TRC(TRC_ID_MAIN_INFO, "starting the copy work");
        PLANE_TRC(TRC_ID_MAIN_INFO, "*ppCurrNode=%p", *ppCurrNode);
        r = PauseBufferCopyStart(*ppCurrNode, &m_pPauseNode, m_pPauseCopyBufParams);
        if (!r)
        {
            PLANE_TRCOUT(TRC_ID_MAIN_INFO, "error starting copy work");
            m_PauseState = PAUSESTATE_DISABLED;
            break;
        }
        PLANE_TRC(TRC_ID_MAIN_INFO, "copy work started");
        m_PauseState = PAUSESTATE_ENABLED;
        m_bPauseDisplayed = false;
        m_PauseMissingNodeCount = 0;
        break;
    case PAUSESTATE_ENABLED:
        if (m_pPauseNode)
        {
            /* use the copied buffer */
            *ppCurrNode = m_pPauseNode;
            *ppPrevNode = NULL;
            *ppNextNode = NULL;
            if(!m_bPauseDisplayed)
            {
                PLANE_TRC(TRC_ID_MAIN_INFO, "PAUSESTATE_ENABLED - use m_pPauseNode=%p", m_pPauseNode);
                m_bPauseDisplayed = true;
            }
            PLANE_TRC( TRC_ID_PICT_QUEUE_RELEASE, "current node replaced by copy of %d%c (%p)",
                       (*ppCurrNode)->m_pictureId,
                       (*ppCurrNode)->m_srcPictureTypeChar,
                       (*ppCurrNode) );
        } else {
            m_PauseMissingNodeCount++;
            PLANE_TRC(TRC_ID_ERROR, "in PAUSESTATE_ENABLED but copy not finished (count=%d)", m_PauseMissingNodeCount);
            if (m_PauseMissingNodeCount > 4)
            {
                /* there was a problem to allocate m_pPauseNode so exit from pause state */
                m_PauseState = PAUSESTATE_UNPAUSE_REQUESTED;
            }
        }
        break;
    case PAUSESTATE_UNPAUSE_REQUESTED:
        /* don't replace ppCurrBuf with m_pPauseNode */
        /* m_pPauseNode will be freed at next Vsync */
        PLANE_TRC(TRC_ID_MAIN_INFO, "PAUSESTATE_UNPAUSE_REQUESTED - use real node %p to display", *ppCurrNode);
        m_PauseState = PAUSESTATE_DISABLING;
        break;
    case PAUSESTATE_DISABLING:
        /* m_pPauseNode is no more displayed. */
        /* free m_pPauseNode and the allocated buffer */
        PLANE_TRC(TRC_ID_MAIN_INFO, "PAUSESTATE_DISABLING");
        PauseBufferCopyStop();
        m_PauseState = PAUSESTATE_DISABLED;
        m_status  &= ~STM_STATUS_PLANE_PAUSED;
        break;
    }
}

/*
 * pause work queue function.
 * perform the actual copy of current frame.
 */
void PauseBufferCopyWorkFunction(void * data)
{
    struct PauseCopyBufParams * params = (struct PauseCopyBufParams *)data;
    int r = 0;

    TRCIN(TRC_ID_MAIN_INFO, "");
    if(!params)
    {
        TRCOUT(TRC_ID_ERROR, "NULL params");
        return;
    }
    if(!params->SrcNode.IsNodeValid())
    {
        TRCOUT(TRC_ID_ERROR, "%s - invalid node to copy", params->plane->GetName());
        return;
    }

    TRC(TRC_ID_MAIN_INFO, "%s - Get copy buffer", params->plane->GetName());
    r = CBufferCopy::GetCopiedPictBuffers( params->SrcNode.m_bufferDesc.src.primary_picture.video_buffer_size,
                                           params->SrcNode.m_bufferDesc.src.secondary_picture.video_buffer_size,
                                          &params->pPauseCopiedPictBuffers);
    TRC(TRC_ID_MAIN_INFO, "%s - end of get copy buffer (%d)", params->plane->GetName(), r);
    if(!r)
    {
        TRCOUT(TRC_ID_ERROR, "%s - CBufferCopy::GetCopiedPictBuffers() fails", params->plane->GetName());
        return;
    }
    /* copy the buffer */
    TRC(TRC_ID_MAIN_INFO, "%s - copying buffer", params->plane->GetName());
    r = CBufferCopy::PerformCopiedPictBuffers(&params->SrcNode, params->pPauseCopiedPictBuffers);
    TRC(TRC_ID_MAIN_INFO, "%s - end of copying buffer (%d)", params->plane->GetName(), r);
    if(!r)
    {
        TRCOUT(TRC_ID_ERROR, "%s- CBufferCopy::PerformCopiedPictBuffers() fails", params->plane->GetName());
        return;
    }
    /* allocate the display node */
    if(params->device)
    {
        params->device->VSyncLock();
    }
    TRC(TRC_ID_MAIN_INFO, "%s - allocating node", params->plane->GetName());
    r = CBufferCopy::AllocateCopiedNode(params->ppDestNode, &params->SrcNode, params->pPauseCopiedPictBuffers);
    TRC(TRC_ID_MAIN_INFO, "%s - end of allocating node (%d)", params->plane->GetName(), r);
    if(params->device)
    {
        params->device->VSyncUnlock();
    }
    if(!r)
    {
        TRCOUT(TRC_ID_ERROR, "%s - CBufferCopy::AllocatePauseNode() fails", params->plane->GetName());
        return;
    }
    TRC(TRC_ID_MAIN_INFO, "%s - *ppCopiedNode=%p", params->plane->GetName(), *params->ppDestNode);
    TRCOUT(TRC_ID_MAIN_INFO, "%s - r=%d", params->plane->GetName(), r);
    return;
}

/*
 * prepare and queue the work item for Pause buffer copy.
 */
bool CDisplayPlane::PauseBufferCopyStart(CDisplayNode *pSrcNode,
        CDisplayNode ** ppDestNode,
        struct PauseCopyBufParams * pPauseCopyBufParams)
{
    bool r = false;
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    if(!pPauseCopyBufParams)
    {
        PLANE_TRCOUT(TRC_ID_ERROR, "pPauseCopyBufParams is NULL");
        return false;
    }
    if(!pSrcNode)
    {
        PLANE_TRCOUT(TRC_ID_ERROR, "pNode is NULL");
        return false;
    }
    if(!ppDestNode)
    {
        PLANE_TRCOUT(TRC_ID_ERROR, "ppPauseNode is NULL");
        return false;
    }

    pPauseCopyBufParams->plane = this;
    pPauseCopyBufParams->device = (CDisplayDevice*)GetParentDevice();
    pPauseCopyBufParams->SrcNode = *pSrcNode;
    pPauseCopyBufParams->ppDestNode = ppDestNode;
    pPauseCopyBufParams->pPauseCopiedPictBuffers = NULL;

    r = vibe_os_queue_workerdata(m_PauseWorkQueue, (void*)pPauseCopyBufParams);
    if (!r)
    {
        PLANE_TRC(TRC_ID_ERROR, "error in vibe_os_queue_workerdata() r=%d", r);
    }

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "r=%d", r);
    return r;
}

/*
 * clean up pause.
 */
void CDisplayPlane::PauseBufferCopyStop()
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");
    if (m_pPauseNode && m_bPauseDisplayed)
    {
        m_bPauseDisplayed = false;
    }
    if (m_pPauseCopyBufParams)
    {
        if (m_pPauseCopyBufParams->pPauseCopiedPictBuffers)
        {
            CBufferCopy::ReleaseCopiedPictBuffers(m_pPauseCopyBufParams->pPauseCopiedPictBuffers);
            m_pPauseCopyBufParams->pPauseCopiedPictBuffers = NULL;
        }
    }
    if (m_pPauseNode)
    {
        delete m_pPauseNode;
        m_pPauseNode = NULL;
    }
    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
}

////////////////////////////////////////////////////////////////////////////////
// C Display plane interface
//

extern "C" {

int stm_display_plane_get_name(stm_display_plane_h plane, const char **name)
{
  CDisplayPlane* pDP = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

// We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDP = plane->handle;

  if(!CHECK_ADDRESS(name))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  *name = pDP->GetName();

  STKPI_UNLOCK(plane->pDev);

  return 0;
}

int stm_display_plane_get_device_id(stm_display_plane_h plane, uint32_t *id)
{
  CDisplayPlane* pDP = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  *id = pDP->GetParentDevice()->GetID();

  STKPI_UNLOCK(plane->pDev);

  return 0;
}

int stm_display_plane_get_id(stm_display_plane_h plane, uint32_t* id)
{
  CDisplayPlane* pDP = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(id))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  *id = pDP->GetID();

  STKPI_UNLOCK(plane->pDev);

  return 0;
}


int stm_display_plane_connect_to_output(stm_display_plane_h plane, stm_display_output_h output)
{
  CDisplayPlane * pDP  = NULL;
  COutput       * pOut = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!IS_HANDLE_VALID(output, VALID_OUTPUT_HANDLE))
    return -EINVAL;

  pOut = output->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());

  switch( pDP->ConnectToOutput(pOut))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
      ret = -ENOTSUP;
      break;
    case STM_PLANE_ALREADY_CONNECTED:
      ret = -EALREADY;
      break;
    case STM_PLANE_CANNOT_CONNECT:
      ret = -EBUSY;
      break;
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_disconnect_from_output(stm_display_plane_h plane, stm_display_output_h output)
{
  CDisplayPlane * pDP  = NULL;
  COutput       * pOut = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!IS_HANDLE_VALID(output, VALID_OUTPUT_HANDLE))
    return -EINVAL;

  pOut = output->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  pDP->DisconnectFromOutput(pOut);

  STKPI_UNLOCK(plane->pDev);

  return 0;
}


int stm_display_plane_get_connected_output_id(stm_display_plane_h plane, uint32_t *id, uint32_t number )
{
  CDisplayPlane* pDP = NULL;
  int res = -1;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  res = pDP->GetConnectedOutputID(id, number);

  STKPI_UNLOCK(plane->pDev);

  return res;
}



int stm_display_plane_get_available_source_id(stm_display_plane_h plane, uint32_t *id, uint32_t number )
{
  CDisplayPlane* pDP = NULL;
  int res = -1;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  res = pDP->GetAvailableSourceID(id, number);

  STKPI_UNLOCK(plane->pDev);

  return res;
}

int stm_display_plane_pause(stm_display_plane_h plane)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->Pause()?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_resume(stm_display_plane_h plane)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->UnPause()?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_show(stm_display_plane_h plane)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->ShowRequestedByTheApplication()?0:-EALREADY;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_hide(stm_display_plane_h plane)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->HideRequestedByTheApplication()?0:-EALREADY;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_get_capabilities(stm_display_plane_h plane, stm_plane_capabilities_t *caps)
{
  CDisplayPlane* pDP  = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  // We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  *caps = pDP->GetCapabilities();
  TRC(TRC_ID_API_PLANE, "plane : %s, caps : 0x%x", pDP->GetName(), *caps);

  STKPI_UNLOCK(plane->pDev);

  return 0;
}


int stm_display_plane_get_image_formats(stm_display_plane_h plane, const stm_pixel_format_t** formats)
{
  CDisplayPlane* pDP  = NULL;
  int n;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  n = pDP->GetFormats(formats);

  STKPI_UNLOCK(plane->pDev);

  return n;
}

int stm_display_plane_get_list_of_features(stm_display_plane_h plane, stm_plane_feature_t* list, uint32_t number )
{
  CDisplayPlane* pDP  = NULL;
  int n;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  // We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  n = pDP->GetListOfFeatures(list, number);

  STKPI_UNLOCK(plane->pDev);

  return n;
}

int stm_display_plane_is_feature_applicable(stm_display_plane_h plane, stm_plane_feature_t feature, bool *applicable )
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->IsFeatureApplicable( feature, applicable) ? 0 : -ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_set_control(stm_display_plane_h plane, stm_display_plane_control_t control, uint32_t value)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s control: %d value: %d", pDP->GetName(), control, value);

  switch( pDP->SetControl(control,value))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
      ret = -ENOTSUP;
      break;
    case STM_PLANE_INVALID_VALUE:
      ret = -ERANGE;
      break;
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_get_control(stm_display_plane_h plane, stm_display_plane_control_t control, uint32_t *current_val)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!CHECK_ADDRESS(current_val))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());

  switch( pDP->GetControl(control,(uint32_t*)current_val))
  {
    case STM_PLANE_OK:
      TRC(TRC_ID_API_PLANE, "control: %d value: %d", control, *current_val);
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
      ret = -ENOTSUP;
      break;
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_set_depth(stm_display_plane_h plane, stm_display_output_h output, int32_t depth, int32_t activate)
{
  CDisplayPlane * pDP  = NULL;
  COutput       * pOut = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!IS_HANDLE_VALID(output, VALID_OUTPUT_HANDLE))
    return -EINVAL;

  pOut = output->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->SetDepth(pOut,depth,(activate != 0))?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_get_connected_source_id(stm_display_plane_h plane, uint32_t *id)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(id))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->GetConnectedSourceID(id)?0:-ENOTCONN;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_set_mixer_bypass(stm_display_plane_h  plane,
                                       stm_display_output_h output,
                                       int32_t              exclusive)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!IS_HANDLE_VALID(output, VALID_OUTPUT_HANDLE))
    return -EINVAL;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  // By waiting futur implementation
  ret = -ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_connect_to_source(stm_display_plane_h plane, stm_display_source_h source)
{
  CDisplayPlane  *pDP = NULL;
  CDisplaySource *pDS = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!IS_HANDLE_VALID(source, VALID_SOURCE_HANDLE))
    return -EINVAL;

  pDS = source->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s source: %s", pDP->GetName(), pDS->GetName() );

  switch(pDP->ConnectToSourceProtected(pDS))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
      ret = -ENOTSUP;
      break;
    case STM_PLANE_ALREADY_CONNECTED:
      ret = -EALREADY;
      break;
    case STM_PLANE_CANNOT_CONNECT:
      ret = -EBUSY;
      break;
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_disconnect_from_source(stm_display_plane_h plane, stm_display_source_h source)
{
  CDisplayPlane  *pDP = NULL;
  CDisplaySource *pDS = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!IS_HANDLE_VALID(source, VALID_SOURCE_HANDLE))
    return -EINVAL;

  pDS = source->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s source: %s", pDP->GetName(), pDS->GetName() );

 /* Do not test the returned error on purpose to skip error if plane is not */
 /* currently connected to the given source */
  pDP->DisconnectFromSourceProtected(pDS);

  STKPI_UNLOCK(plane->pDev);

  return 0;
}


int stm_display_plane_get_depth(stm_display_plane_h plane, stm_display_output_h output, int32_t *depth)
{
  CDisplayPlane * pDP  = NULL;
  COutput       * pOut = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  /* We are not allowing this call while driver is suspended as this will be accessing hardware ressource in order to retreive the plane depth value. */
  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!IS_HANDLE_VALID(output, VALID_OUTPUT_HANDLE))
    return -EINVAL;

  pOut = output->handle;

  if(!CHECK_ADDRESS(depth))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->GetDepth(pOut,(int *) depth)?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}


int stm_display_plane_get_status(stm_display_plane_h plane, uint32_t *status)
{
  CDisplayPlane *pDP = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  // We allow this function to be called even when the driver is suspended (so IS_DEVICE_SUSPENDED() is not called)

  pDP = plane->handle;
  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());

  if(!CHECK_ADDRESS(status))
    return -EFAULT;

  *status = pDP->GetStatus();

  return 0;
}

int stm_display_plane_get_control_range(stm_display_plane_h plane, stm_display_plane_control_t selector, stm_display_plane_control_range_t *range)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(range))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->GetControlRange(selector, range)?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_get_compound_control (stm_display_plane_h plane, stm_display_plane_control_t  ctrl, void * current_val)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!CHECK_ADDRESS(current_val))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s, ctrl : %d", pDP->GetName(), ctrl);

  switch(pDP->GetCompoundControl(ctrl, current_val))
  {
    case STM_PLANE_OK:
      ret = 0;
      switch ( ctrl ) {
      case PLANE_CTRL_MIN_VIDEO_LATENCY:
      case PLANE_CTRL_MAX_VIDEO_LATENCY:
        TRC(TRC_ID_API_PLANE, "nb_input_periods : %u, nb_output_periods : %u", ((stm_compound_control_latency_t *)current_val)->nb_input_periods, ((stm_compound_control_latency_t *)current_val)->nb_output_periods);
        break;
      default:
        break;
      }
      break;
    case STM_PLANE_INVALID_VALUE:
      ret = -EINVAL;
      break;
    case STM_PLANE_NOT_SUPPORTED:
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_set_compound_control(stm_display_plane_h plane, stm_display_plane_control_t  ctrl, void * new_val)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!CHECK_ADDRESS(new_val))
    return -ERANGE;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s control: %d", pDP->GetName(), ctrl);

  switch(pDP->SetCompoundControl(ctrl, new_val))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_INVALID_VALUE:
      ret = -ERANGE;
      break;
    case STM_PLANE_NOT_SUPPORTED:
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_get_compound_control_range(stm_display_plane_h plane, stm_display_plane_control_t selector,
                                                 stm_compound_control_range_t * range)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(range))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s, selector : %d", pDP->GetName(), selector);
  ret = pDP->GetCompoundControlRange(selector, range)?0:-ENOTSUP;

  if ( ret == 0 ) {
    TRC(TRC_ID_API_PLANE, "range.min_val.x : %d, range.min_val.y : %d, range.min_val.width : %u, range.min_val.height : %u", range->min_val.x, range->min_val.y, range->min_val.width, range->min_val.height);
    TRC(TRC_ID_API_PLANE, "range.max_val.x : %d, range.max_val.y : %d, range.max_val.width : %u, range.max_val.height : %u", range->max_val.x, range->max_val.y, range->max_val.width, range->max_val.height);
    TRC(TRC_ID_API_PLANE, "range.default_val.x : %d, range.default_val.y : %d, range.default_val.width : %u, range.default_val.height : %u", range->default_val.x, range->default_val.y, range->default_val.width, range->default_val.height);
    TRC(TRC_ID_API_PLANE, "range.step.x : %d, range.step.y : %d, range.step.width : %u, range.step.height : %u", range->step.x, range->step.y, range->step.width, range->step.height);
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_get_timing_identifier(stm_display_plane_h plane, uint32_t *timingID)
{
  CDisplayPlane *pDP = NULL;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(timingID))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName() );

  *timingID = pDP->GetTimingID();

  STKPI_UNLOCK(plane->pDev);

  return (*timingID == 0)?-ENOTSUP:0;
}

int stm_display_plane_get_tuning_data_revision (stm_display_plane_h  plane,
                                                stm_display_plane_tuning_data_control_t ctrl,
                                                uint32_t * revision)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(revision))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->GetTuningDataRevision(ctrl, revision)?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_get_tuning_data_control (stm_display_plane_h  plane,
                                               stm_display_plane_tuning_data_control_t ctrl,
                                               stm_tuning_data_t * currentVal )
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(currentVal))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());

  switch(pDP->GetTuningDataControl(ctrl, currentVal))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_set_tuning_data_control (stm_display_plane_h  plane,
                                               stm_display_plane_tuning_data_control_t ctrl,
                                               stm_tuning_data_t * newVal)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  if(!CHECK_ADDRESS(newVal))
    return -ERANGE;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());

  switch(pDP->SetTuningDataControl(ctrl, newVal))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_INVALID_VALUE:
      ret = -ERANGE;
      break;
    case STM_PLANE_NOT_SUPPORTED:
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_get_control_mode (stm_display_plane_h  plane, stm_display_control_mode_t *mode)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return -EAGAIN;
  }

  pDP = plane->handle;

  if(!CHECK_ADDRESS(mode))
    return -EFAULT;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->GetControlMode(mode)?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_set_control_mode (stm_display_plane_h  plane, stm_display_control_mode_t mode)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->SetControlMode(mode)?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_apply_sync_controls (stm_display_plane_h  plane)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE)) {
    return -EINVAL;
  }

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  ret = pDP->ApplySyncControls()?0:-ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_apply_modesetup (stm_display_plane_h  plane)
{
  CDisplayPlane *pDP = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());
  // By waiting futur implementation
  ret = -ENOTSUP;

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_set_asynch_ctrl_listener(stm_display_plane_h plane, const stm_ctrl_listener_t *listener, int *listener_id)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());

  switch( pDP->RegisterAsynchCtrlListener(listener, listener_id))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
      ret = -ENOTSUP;
      break;
    case STM_PLANE_INVALID_VALUE:
      ret = -ERANGE;
      break;
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

int stm_display_plane_unset_asynch_ctrl_listener(stm_display_plane_h plane, int listener_id)
{
  CDisplayPlane* pDP  = NULL;
  int ret;

  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return -EINVAL;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
    return -EAGAIN;

  pDP = plane->handle;

  STKPI_LOCK(plane->pDev);

  TRC(TRC_ID_API_PLANE, "plane : %s", pDP->GetName());

  switch( pDP->UnregisterAsynchCtrlListener(listener_id))
  {
    case STM_PLANE_OK:
      ret = 0;
      break;
    case STM_PLANE_NOT_SUPPORTED:
      ret = -ENOTSUP;
      break;
    case STM_PLANE_INVALID_VALUE:
      ret = -ERANGE;
      break;
    default:
      ret = -ENOTSUP;
      break;
  }

  STKPI_UNLOCK(plane->pDev);

  return ret;
}

void stm_display_plane_close(stm_display_plane_h plane)
{
  if(!IS_HANDLE_VALID(plane, VALID_PLANE_HANDLE))
    return;

  if(IS_DEVICE_SUSPENDED(plane->pDev))
  {
    //For the moment we only print an error (this is done by IS_DEVICE_SUSPENDED)
    //return;
  }

  TRC(TRC_ID_HANDLE_OPEN_CLOSE, "Close plane 0x%p", plane);
  TRC(TRC_ID_API_PLANE, "plane : %s", plane->handle->GetName());
  stm_coredisplay_magic_clear(plane);

  /*
   * Remove object instance from the registry before exiting.
   */
  if(stm_registry_remove_object(plane) < 0)
    TRC( TRC_ID_ERROR, "failed to remove plane instance from the registry" );

  delete plane;
}

} // extern "C"

