/***********************************************************************
 *
 * File: display/generic/Node.cpp
 * Copyright (c) 2013 by STMicroelectronics. All rights reserved.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/
#include <vibe_os.h>
#include <vibe_debug.h>

#include "Node.h"

CNode::CNode(void)
{
    m_pNextNode = 0;
    m_magic_number = NODE_MAGIC_NUMBER;
}

CNode*  CNode::GetNextNode(void) const
{
    if( m_pNextNode && !m_pNextNode->IsNodeValid() )
    {
        TRC( TRC_ID_ERROR, "Get a corrupted node %p in the queue!!!", m_pNextNode);
        return 0;
    }
    return m_pNextNode;
}


CNode::~CNode(void)
{
    m_magic_number = 0;
}

