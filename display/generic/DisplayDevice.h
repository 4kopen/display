/***********************************************************************
 *
 * File: display/generic/DisplayDevice.h
 * Copyright (c) 2000, 2004, 2005 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/
#ifndef DISPLAYDEVICE_H
#define DISPLAYDEVICE_H

#include <vibe_os.h>
#include <display_device_priv.h>
#include "stm_display_plane.h"
#include "stm_display_test_tuning.h"

#define VALID_DEVICE_HANDLE	       0x01230123

class CDisplayDevice;
class CDisplaySource;
class CDisplayPlane;
class COutput;


enum TuningResults {
  TUNING_OK,
  TUNING_SERVICE_NOT_SUPPORTED,
  TUNING_SERVICE_NOT_AVAILABLE,
  TUNING_INVALID_PARAMETER,
  TUNING_NO_DATA_AVAILABLE
};


enum DevicePowerState {
  DEVICE_ACTIVE,
  DEVICE_SUSPENDED,
  DEVICE_FROZEN,
  DEVICE_SHUTTING_DOWN
};

struct stm_display_device_pm_s
{
  DevicePowerState  state;
  void *            spinLock;    /* SpinLock dedicated exclusively to the protection of pm.state */

  int (*runtime_get)(const uint32_t pid);
  int (*runtime_put)(const uint32_t pid);
};

struct stm_display_device_s
{
  CDisplayDevice * handle;
  uint32_t         magic;       /* Used to check handle validity */
};


class CDisplayDevice
{
public:
  CDisplayDevice(uint32_t id, int nOutputs, int nPlanes, int nSources);
  virtual ~CDisplayDevice();

  // Create Device specific resources after initial mappings have
  // been completed.
  virtual bool Create();

  uint32_t    GetID(void)            const { return m_ID; }

  // Access to outputs
  uint32_t GetNumberOfOutputs(void)  const { return m_numOutputs; }
  COutput* GetOutput(uint32_t index) const { return (index<m_numOutputs)?m_pOutputs[index]:0; }

  int FindOutputsWithCapabilities(
                               stm_display_output_capabilities_t  caps_match,
                               stm_display_output_capabilities_t  caps_mask,
                               uint32_t*                          id,
                               uint32_t                           max_ids);
  int FindPlanesWithCapabilities(
                               stm_plane_capabilities_t  caps_match,
                               stm_plane_capabilities_t  caps_mask,
                               uint32_t*                 id,
                               uint32_t                  max_ids);

  // Control registers information
  void* GetCtrlRegisterBase() const { return m_pReg; }

  // Request to use a device specific plane
  uint32_t        GetNumberOfPlanes(void)  const { return m_numPlanes; }
  CDisplayPlane*  GetPlane(uint32_t index) const { return (index<m_numPlanes)?m_pPlanes[index]:0; }
  void            SetMasterVideoPlane(CDisplayPlane *plane);

  // Access to sources
  uint32_t        GetNumberOfSources(void)  const { return m_numSources; }
  CDisplaySource* GetSource(uint32_t index) const { return (index<m_numSources)?m_pSources[index]:0; }

  uint32_t        GetUseCount(void) const { return m_use_count; }
  void            IncrementUseCount(void) { m_use_count++; }
  void            DecrementUseCount(void);

  uint16_t GetNumTuningServices(void) const { return m_nNumTuningServices; }
  const stm_device_tuning_caps_t *GetTuningCaps(void) const { return m_pTuningCaps; }

  void PlaneNeedsPrioHwUpdate(CDisplayPlane* plane);

  virtual TuningResults SetTuning(uint16_t service,
                                  void *inputList,
                                  uint32_t inputListSize,
                                  void *outputList,
                                  uint32_t outputListSize);

  /*
   * Called after VSYNC interrupt to update the display for the specified output.
   * This is done on CDisplayDevice so that the correct processing can be done
   * for all hardware clocked by a specific timing generator.
   */
  void OutputVSyncIrqUpdateHw(uint32_t timingID);
  void OutputVSyncThreadedIrqUpdateHw(uint32_t timingID);

  void UpdateSource(uint32_t timingID);

  void RemoveOutputsCapabilities(uint32_t caps);
  /*
   * Power Management stuff.
   */
  void        PowerDownVideoDacs(void);
  virtual int Freeze(void);
  virtual int Suspend(void);
  virtual int Resume(void);

  bool        PmGetState        (DevicePowerState *currentState);
  bool        PmSetState        (DevicePowerState newState);
  void        PmGetDevice       (void);
  void        PmPutDevice       (void);
  void        PmSetRuntimeHooks (int (*get)(const uint32_t id),
                                 int (*put)(const uint32_t id) );
  bool        PmIsSuspended     (const char *fct_name);

  void        VSyncLock(void) const;
  void        VSyncUnlock(void) const;
  void        DebugCheckVSyncLockTaken(const char *function_name) const;

  int         StkpiUnlock(const char *fct_name);
  void        StkpiLock  (const char *fct_name);

  const bool  BypassHwInitialization(void) const { return m_devConfig.no_hw_init; }

protected:

  uint32_t               m_ID;
  COutput              **m_pOutputs;    // Available device outputs
  uint32_t               m_numOutputs;  // Number of outputs
  CDisplayPlane        **m_pPlanes;     // Available Planes
  uint32_t               m_numPlanes;   // Number of Planes
  CDisplayPlane        **m_pPlanesPrio; // Planes ordered by update priority
  CDisplaySource       **m_pSources;    // Available Sources
  uint32_t               m_numSources;  // Number of Sources

  uint32_t               m_use_count;   // Indicate how many DisplayDevice handles have been opened
                                        // The handle obtained through a call to stm_display_device_create() is not counted
  void                  *m_stkpi_mutex_lock;
  vibe_time64_t          m_stkpi_lock_time;
  vibe_time64_t          m_stkpi_unlock_time;

  // Device power management structure
  struct  stm_display_device_pm_s m_pm;

  uint16_t                  m_nNumTuningServices;
  stm_device_tuning_caps_t *m_pTuningCaps;

  volatile uint32_t m_CurrentVTGSync;

  // IO mapped pointer to the start of the register block
  uint32_t* m_pReg;

  // Power Managment state
  bool               m_bIsSuspended; // Has the HW been powered off
  bool               m_bIsFrozen;

  void              *m_vsyncLock;   // semaphore for protecting sources and planes shared datas from stkpi and threaded_irq context

  stm_device_configuration_t m_devConfig;

  CDisplayPlane *m_MasterVideoPlane;

  void WriteDevReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pReg, reg, val); }
  uint32_t ReadDevReg(uint32_t reg) { return vibe_os_read_register(m_pReg, reg); }

  uint32_t FindPlanePrio(CDisplayPlane* plane) const;

  /*
   * Helper for device creation code to add an output to the device's list of
   * outputs. However it does a little more than that, calling the output's
   * create function and if there is any failure destroying the object for you.
   *
   * This means you can use the form if(!AddOutput(new MyOutputClass(...))) {}
   */
  bool AddOutput(COutput *);
  bool AddPlane(CDisplayPlane *);
  bool AddSource(CDisplaySource *);

  void RemoveSources(void);
  void RemovePlanes(void);
  void RemoveOutputs(void);

  /*
   * Helper for stm_registry management code to add coredisplay objects to the
   * registry's database.
   */
  void RegistryInit(void);
  void RegistryTerm(void);

}; /* CDisplayDevice */


/*
 * There will be one device specific implementation of this function in each
 * valid driver configuration. This is how you get hold of the device object
 */
extern CDisplayDevice* AnonymousCreateDevice(stm_device_configuration_t *DevConfig);

#endif /* DISPLAYDEVICE_H */
