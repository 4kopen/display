/***********************************************************************
 *
 * File: display/gamma/GenericGammaReg.h
 * Copyright (c) 2004, 2005 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef GENERICGAMMAREG_H
#define GENERICGAMMAREG_H

//MIXER Regs
#define MXn_CTL_REG_OFFSET   0x00
#define MXn_BKC_REG_OFFSET   0x04
#define MXn_MISC_REG_OFFSET  0x08
#define MXn_BCO_REG_OFFSET   0x0C
#define MXn_BCS_REG_OFFSET   0x10
#define MXn_AVO_REG_OFFSET   0x28
#define MXn_AVS_REG_OFFSET   0x2C
#define MXn_CRB2_REG_OFFSET  0x30
#define MXn_CRB_REG_OFFSET   0x34
#define MXn_ACT_REG_OFFSET   0x38
#define MXn_MST_REG_OFFSET   0x40

//MIXER MISR Regs
#define MXn_MISR_CTL_REG_OFFSET  0xA0
#define MXn_MISR_STA_REG_OFFSET  0xA4
#define MXn_MISR_AVO_REG_OFFSET  0xB4
#define MXn_MISR_AVS_REG_OFFSET  0xB8

//MIXER Signatures Regs
#define MXn_SIG1_REG_OFFSET 0xA8
#define MXn_SIG2_REG_OFFSET 0xAC
#define MXn_SIG3_REG_OFFSET 0xB0

#endif //GENERICGAMMAREG_H
