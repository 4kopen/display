/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2000-2014 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2014-05-30
***************************************************************************/

#ifndef _GAMMA_MIXER_V2_H
#define _GAMMA_MIXER_V2_H

#include <display/generic/DisplayMixer.h>


#define DEBUGMIXER(pMixer,with_crb2)\
{   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_CTL  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_CTL_REG_OFFSET));   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_BKC  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_BKC_REG_OFFSET));   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_MISC = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_MISC_REG_OFFSET));  \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_BCO  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_BCO_REG_OFFSET));   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_BCS  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_BCS_REG_OFFSET));   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_AVO  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_AVO_REG_OFFSET));   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_AVS  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_AVS_REG_OFFSET));   \
  if(with_crb2)                                                                                                                   \
    TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_CRB2 = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_CRB2_REG_OFFSET));\
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_CRB  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_CRB_REG_OFFSET));   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_ACT  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_ACT_REG_OFFSET));   \
  TRC( TRC_ID_MIXER_REG, "[%#.8x]\t :  MXn_MST  = %#.8x", (uint32_t)(m_ulRegOffset), (pMixer)->ReadMixReg(MXn_MST_REG_OFFSET));   \
}

/* Public trace macros: exposed here so that derived classes can use them */
/* These macros add the Mixer name at the beginning of all traces        */
#define MIXER_TRC(id,fmt,args...)       TRC   (id, "%s - " fmt, GetName(), ##args)
#define MIXER_TRCIN(id,fmt,args...)     TRCIN (id, "%s - " fmt, GetName(), ##args)
#define MIXER_TRCOUT(id,fmt,args...)    TRCOUT(id, "%s - " fmt, GetName(), ##args)
#define MIXER_TRCBL(id)                 TRCBL (id, "%s", GetName())


typedef struct stm_zorder_item_s
{
  uint32_t planeId;
  bool     bEnabled;
} stm_zorder_item_t;


class CGammaMixer: public CDisplayMixer
{
public:
  CGammaMixer(const char   *name,
              CDisplayDevice *pDev,
              uint32_t        ulRegOffset,
              stm_mixer_id_t  ulVBILinkedPlane = MIXER_ID_NONE,
              const char     *procClockName = NULL);

  ~CGammaMixer();

  const char *GetName(void) const { return m_name; }

  bool Start(const stm_display_mode_t *);
  void Stop(void);

  void Suspend(void);
  void Resume(void);

  stm_display_mode_t GetCurrentMode(void) const { return m_CurrentMode; }
  bool IsStarted(void) { return m_CurrentMode.mode_id != STM_TIMING_MODE_RESERVED; }

  bool PlaneValid(const CDisplayPlane *) const;

  uint32_t GetCapabilities(void) const { return m_ulCapabilities; }

  uint32_t SetControl(stm_output_control_t, uint32_t newVal);
  uint32_t GetControl(stm_output_control_t, uint32_t *val) const;

  bool EnablePlane(const CDisplayPlane *, stm_mixer_activation_t act = STM_MA_NONE);
  bool DisablePlane(const CDisplayPlane *);

  bool UpdateFromOutput(COutput* pOutput, stm_plane_update_reason_t update_reason);

  bool HasEnabledPlanes(void) const;

  bool SetPlaneDepth(const CDisplayPlane *, int depth, bool activate);
  bool GetPlaneDepth(const CDisplayPlane *, int *depth) const;

  virtual bool SetMixerMap(const CDisplayPlane * plane, uint32_t mixerIDs);
  virtual bool GetLastVsyncStatus(uint32_t * status);

  virtual bool DisableProcClock(bool force_disable);  // Stop Processing clock pacing this Mixer
  virtual bool EnableProcClock(bool force_enable);    // Start Processing clock pacing this Mixer
  virtual bool EnableProcClock(void) { return EnableProcClock(false); }

protected:
  CDisplayDevice       * m_pDev;                 // Display Device pointer
  uint32_t             * m_pGammaReg;            // Memory mapped registers
  uint32_t               m_ulRegOffset;          // Mixer address register offset
  void                 * m_lock;                 // Resource lock
  uint32_t               m_ulCapabilities;       // Output capabilities provided by this mixer

  uint32_t               m_validPlanes;          // OR'd mixer IDs supported on this mixer
  uint32_t               m_ulBackground;         // Background colour
  bool                   m_bBackgroundVisible;   // Background visibility

  stm_display_mode_t     m_CurrentMode;          // Current display mode
  bool                   m_bIsSuspended;

  int                    m_firstcrossbarSize;     // The number of plane entries in the first crossbar
  int                    m_secondcrossbarSize;    // The number of plane entries in the second crossbar
  int                    m_crossbarEntryShift;    // The number of bits for each level in CRB register.
  int                    m_crossbarEntryMask;     // The bit mask for each level in CRB register.
  bool                   m_bHasHwYCbCrMatrix;     // Does the hardware include an in-built selectable RGB->YCbCr conversion matrix
  stm_ycbcr_colorspace_t m_colorspaceMode;        // Required output colorspace

  stm_mixer_id_t         m_ulVBILinkedPlane;      // The real plane ID for VBI

  bool                   m_bHasVTGCounter;        // VTG counter available on Mixer HW

  bool                   m_bHasCursorPlane;       // CURSOR plane available on Mixer HW

  uint32_t               m_planeCount;            // The maximum value for the planeID
  stm_zorder_item_t    * m_pZorder;               // Array containing the planeID is depth order
  static uint32_t      * pMixerMap;               // Array containing association between a planeID and one or several stm_mixer_id_t
                                                  // This variable is shared by all the CGammaMixer instances
  uint32_t               m_nbConnectedPlane;      // The number of planes connected to this mixer
  bool                   m_bHasMSTRegister;       // Has the hardware a MST register


  virtual bool CanEnablePlane(const CDisplayPlane *);

  uint32_t GetColorspaceCTL(const stm_display_mode_t *pModeLine) const;
  virtual void UpdateColorspace(const stm_display_mode_t *pModeLine);

  uint32_t ReadMixReg(uint32_t reg) const { return vibe_os_read_register(m_pGammaReg, (m_ulRegOffset+reg)); }
  void  WriteMixReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pGammaReg, (m_ulRegOffset+reg), val); }

private:
  struct vibe_clk  m_procClock;                   // Processing clock associated to this Mixer

  /*
   * Power management suspend saved state
   */
  uint32_t m_SavedFirstCrossbarRegister;
  uint32_t m_SavedSecondCrossbarRegister;

  CGammaMixer(const CGammaMixer&);
  CGammaMixer& operator=(const CGammaMixer&);

  bool HasBackground(void) const { return ((m_validPlanes & MIXER_ID_BKG) != 0); }
  int FindPlaneInZorder(const CDisplayPlane * plane) const;
  void SetControlAndCrossbar(void);

  const char * m_name;
};

#endif /* _GAMMA_MIXER_V2_H */
