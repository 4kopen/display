/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2000-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-06-02
***************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplayDevice.h>
#include <display/generic/DisplayPlane.h>
#include <display/generic/Output.h>

#include <display/ip/stmviewport.h>

#include "GenericGammaReg.h"
#include "GenericGammaDefs.h"
#include "GammaMixer.h"

// Default Background RGB888 color
#define DEFAULT_BKG_COLOR 0x101010

// Initialize static variable
uint32_t* CGammaMixer::pMixerMap = NULL;

CGammaMixer::CGammaMixer(const char * name,
                         CDisplayDevice* pDev,
                         uint32_t        ulRegOffset,
                         stm_mixer_id_t  ulVBILinkedPlane,
                         const char *procClockName): CDisplayMixer()
{
  MIXER_TRCIN(TRC_ID_MIXER, "");

  m_name        = name;
  m_pDev        = pDev;
  m_pGammaReg   = (uint32_t*)m_pDev->GetCtrlRegisterBase();
  m_ulRegOffset = ulRegOffset;
  m_lock        = vibe_os_create_resource_lock();

  m_ulCapabilities = OUTPUT_CAPS_PLANE_MIXER | OUTPUT_CAPS_MIXER_BACKGROUND;

  m_validPlanes        = 0;
  m_ulBackground       = DEFAULT_BKG_COLOR;
  m_bBackgroundVisible = true;

  vibe_os_zero_memory( &m_CurrentMode, sizeof( m_CurrentMode ));
  m_CurrentMode.mode_id = STM_TIMING_MODE_RESERVED;
  m_bIsSuspended        = false;

  m_firstcrossbarSize   = 0;
  m_secondcrossbarSize  = 0;
  m_crossbarEntryShift  = 3;    // default is 3 for OLD SoC
  m_crossbarEntryMask   = 0x7;  // default is 0x7 for OLD SoC

  m_bHasHwYCbCrMatrix  = true;
  m_colorspaceMode  = STM_YCBCR_COLORSPACE_AUTO_SELECT;

  m_ulVBILinkedPlane = ulVBILinkedPlane;

  m_bHasVTGCounter   = false;
  m_bHasCursorPlane  = true; // by default Mixer support cursor planes

  m_SavedFirstCrossbarRegister = 0;
  m_SavedSecondCrossbarRegister = 0;

  m_planeCount = 0;
  m_pZorder = NULL;
  m_nbConnectedPlane = 0;
  m_bHasMSTRegister = false;

  m_procClock.name[0] ='\0';
  m_procClock.clk     = 0;
  m_procClock.enabled = false;

  // Get the processing clock
  if (procClockName)
  {
    if (vibe_os_clk_get(procClockName, &m_procClock))
    {
      MIXER_TRC(TRC_ID_MIXER, "%s : Invalid processing clock (%s)", name, procClockName );
    }
    else
    {
      /*
       * When Splash Screen is Enabled, we need to make sure to keep
       * the clk_proc clock enabled until the first start of the mixer
       */
      if (m_pDev->BypassHwInitialization())
      {
        EnableProcClock(true);
      }
    }
  }

  MIXER_TRCOUT(TRC_ID_MIXER, "");
}


CGammaMixer::~CGammaMixer()
{
  if(m_lock)
  {
    vibe_os_delete_resource_lock(m_lock);
    m_lock = 0;
  }

  if (m_procClock.clk != 0)
  {
    vibe_os_clk_put(&m_procClock);
  }
}


bool CGammaMixer::PlaneValid(const CDisplayPlane *p) const
{
  return (pMixerMap[p->GetID()] & m_validPlanes) == pMixerMap[p->GetID()];
}


/* This can be overriden to provide more restrictions, e.g. mutually exclusive
 * planes on the 5528 aux mixer. Hence the result can be dynamic depending
 * on what other planes are enabled.
 */
bool CGammaMixer::CanEnablePlane(const CDisplayPlane *p)
{
  if((pMixerMap[p->GetID()] & MIXER_ID_VBI) && (m_ulVBILinkedPlane == MIXER_ID_NONE))
    return false;

  return PlaneValid(p);
}


bool CGammaMixer::UpdateFromOutput(COutput* pOutput, stm_plane_update_reason_t update_reason)
{
  for(uint32_t i=0; i < m_pDev->GetNumberOfPlanes(); i++)
  {
    CDisplayPlane* plane = m_pDev->GetPlane(i);
    if(plane)
      plane->UpdateFromMixer(pOutput, update_reason);
  }

  switch(update_reason)
  {
    case STM_PLANE_OUTPUT_STOPPED:
    {
      DisableProcClock(false);
    }
    break;

    case STM_PLANE_OUTPUT_STARTED:
    {
      EnableProcClock();
    }
    break;

    case STM_PLANE_OUTPUT_UPDATED:
    default:
      MIXER_TRC( TRC_ID_MIXER, "No clock update required (update_reason = %d)", update_reason);
      break;
  }

  return true;
}

int CGammaMixer::FindPlaneInZorder(const CDisplayPlane * plane) const
{
  uint32_t planeId = plane->GetID();
  uint32_t i = 0;

  for (i=0; i<m_planeCount; i++)
  {
    if (m_pZorder[i].planeId == planeId)
    {
      break;
    }
  }

  return i;
}

bool CGammaMixer::EnablePlane(const CDisplayPlane *plane, stm_mixer_activation_t act)
{
  uint32_t i = 0;

  MIXER_TRCIN(TRC_ID_MIXER, "");

  if(!IsStarted())
  {
    MIXER_TRC( TRC_ID_ERROR, "no video mode active (plane = %s)",plane->GetName());
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  if(!CanEnablePlane(plane))
  {
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  vibe_os_lock_resource(m_lock);

  i = FindPlaneInZorder(plane);

  if (i >= m_planeCount)
  {
    vibe_os_unlock_resource(m_lock);
    MIXER_TRC(TRC_ID_ERROR, "Plane %d not connected to mixer",  plane->GetID());
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  if (m_pZorder[i].bEnabled == TRUE)
  {
    vibe_os_unlock_resource(m_lock);
    MIXER_TRC(TRC_ID_MIXER, "Plane %d already enabled",  plane->GetID());
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return true;
  }

  m_pZorder[i].bEnabled = TRUE;
  SetControlAndCrossbar();
  vibe_os_unlock_resource(m_lock);
  MIXER_TRCOUT(TRC_ID_MIXER, "");
  return true;
}


bool CGammaMixer::DisablePlane(const CDisplayPlane *plane)
{
  uint32_t i = 0;

  MIXER_TRCIN(TRC_ID_MIXER, "");
  if (!CanEnablePlane(plane))
  {
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  vibe_os_lock_resource(m_lock);

  i = FindPlaneInZorder(plane);

  if (i >= m_planeCount)
  {
    vibe_os_unlock_resource(m_lock);
    MIXER_TRC(TRC_ID_ERROR, "Plane %d not connected to mixer",  plane->GetID());
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  if (m_pZorder[i].bEnabled == FALSE)
  {
    vibe_os_unlock_resource(m_lock);
    MIXER_TRC(TRC_ID_MIXER, "Plane %d already disabled",  plane->GetID());
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return true;
  }

  m_pZorder[i].bEnabled = FALSE;
  SetControlAndCrossbar();
  vibe_os_unlock_resource(m_lock);
  MIXER_TRCOUT(TRC_ID_MIXER, "");
  return true;
}


bool CGammaMixer::HasEnabledPlanes(void) const
{
  uint32_t i = 0;

  MIXER_TRCIN(TRC_ID_MIXER, "");
  vibe_os_lock_resource(m_lock);
  for (i=0; i<m_planeCount; i++)
  {
    if (m_pZorder[i].bEnabled == TRUE)
    {
      vibe_os_unlock_resource(m_lock);
      MIXER_TRCOUT(TRC_ID_MIXER, "");
      return true;
    }
  }

  vibe_os_unlock_resource(m_lock);
  MIXER_TRCOUT(TRC_ID_MIXER, "");
  return false;
}

uint32_t CGammaMixer::SetControl(stm_output_control_t ctrl, uint32_t newVal)
{
  switch(ctrl)
  {
    case OUTPUT_CTRL_BACKGROUND_ARGB:
    {
      if(HasBackground())
      {
        m_ulBackground = newVal;
        MIXER_TRC( TRC_ID_MIXER, "new colour = 0x%08x", m_ulBackground );
        WriteMixReg(MXn_BKC_REG_OFFSET, m_ulBackground);
      }
      else
        return STM_OUT_NO_CTRL;

      break;
    }
    case OUTPUT_CTRL_BACKGROUND_VISIBILITY:
    {
      if(HasBackground())
      {
        m_bBackgroundVisible = (newVal != 0);
        if(IsStarted())
          SetControlAndCrossbar();
      }
      else
        return STM_OUT_NO_CTRL;

      break;
    }
    case OUTPUT_CTRL_YCBCR_COLORSPACE:
    {
      if(newVal > STM_COLORSPACE_BT2020)
        return STM_OUT_INVALID_VALUE;

      if(m_colorspaceMode != (static_cast<stm_ycbcr_colorspace_t>(newVal)))
      {
        m_colorspaceMode = static_cast<stm_ycbcr_colorspace_t>(newVal);

        MIXER_TRC( TRC_ID_MIXER, "new colourspace mode = %d", (int)m_colorspaceMode );

        if(IsStarted())
          UpdateColorspace(&m_CurrentMode);
      }
      break;
    }
    default:
      return STM_OUT_NO_CTRL;
  }

  DEBUGMIXER(this, (m_secondcrossbarSize > 0)?1:0);

  return STM_OUT_OK;
}

uint32_t CGammaMixer::GetControl(stm_output_control_t ctrl, uint32_t *val) const
{
  switch(ctrl)
  {
    case OUTPUT_CTRL_BACKGROUND_ARGB:
    {
      if(HasBackground())
      {
        *val = m_ulBackground;
      }
      else
      {
        *val = 0;
        return STM_OUT_NO_CTRL;
      }

      break;
    }
    case OUTPUT_CTRL_BACKGROUND_VISIBILITY:
    {
      if(HasBackground())
      {
        *val = m_bBackgroundVisible?1:0;
      }
      else
      {
        *val = 0;
        return STM_OUT_NO_CTRL;
      }

      break;
    }
    case OUTPUT_CTRL_YCBCR_COLORSPACE:
      *val = m_colorspaceMode;
      break;
    default:
      return STM_OUT_NO_CTRL;
  }

  return STM_OUT_OK;
}

uint32_t CGammaMixer::GetColorspaceCTL(const stm_display_mode_t *pModeLine) const
{
  uint32_t  regval = 0;

  switch(m_colorspaceMode)
  {
    case STM_YCBCR_COLORSPACE_AUTO_SELECT:
      if(pModeLine)
      {
        if((pModeLine->mode_params.output_standards & (STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861))
        || (pModeLine->mode_params.output_standards & STM_OUTPUT_STD_HD_MASK))
          regval = MIX_CTL_709_NOT_601;
      }
      break;
    case STM_COLORSPACE_BT2020:
    case STM_YCBCR_COLORSPACE_709:
      regval = MIX_CTL_709_NOT_601;
      break;
    case STM_YCBCR_COLORSPACE_601:
    default:
      break;
  }

  return regval;
}

void CGammaMixer::UpdateColorspace(const stm_display_mode_t *pModeLine)
{
  MIXER_TRCIN( TRC_ID_MIXER, "" );

  if(!m_bHasHwYCbCrMatrix)
  {
    MIXER_TRCOUT( TRC_ID_MIXER, "No HW YCbCr Matrix" );
    return;
  }

  uint32_t tmp = ReadMixReg(MXn_CTL_REG_OFFSET) & ~MIX_CTL_709_NOT_601;
  tmp |= GetColorspaceCTL(pModeLine);
  WriteMixReg(MXn_CTL_REG_OFFSET, tmp);

  DEBUGMIXER(this, (m_secondcrossbarSize > 0)?1:0);

  MIXER_TRCOUT( TRC_ID_MIXER, "" );
}


bool CGammaMixer::Start(const stm_display_mode_t *pModeLine)
{
  uint32_t ystart, ystop, xstart, xstop;
  uint32_t uAVO, uAVS, uBCO, uBCS;

  MIXER_TRCIN(TRC_ID_MIXER, "");

  if(m_bIsSuspended)
    return false;

  if(m_bHasVTGCounter)
  {
    /*
     * Enable the VTG counter into the compo.
     */
    WriteMixReg(MXn_MISC_REG_OFFSET, (1 << MIX_MISC_ENA_CNT_SHIFT));
  }

  UpdateColorspace(pModeLine);

  /*
   * Set Mixer viewport start, start on the first blanking line (negative offset
   * from the start of the active area).
   */
  xstart = STCalculateViewportPixel(pModeLine, 0);

  int blankline = pModeLine->mode_timing.vsync_width - (pModeLine->mode_params.active_area_start_line-1);
  if(pModeLine->mode_params.scan_type == STM_INTERLACED_SCAN)
    blankline *= 2;

  ystart = STCalculateViewportLine(pModeLine, blankline);

  uAVO = xstart | (ystart << 16);

  //Set Mixer background to start at the first active video line
  ystart = STCalculateViewportLine(pModeLine, 0);

  uBCO = xstart | (ystart << 16);

  WriteMixReg(MXn_AVO_REG_OFFSET, uAVO);

  MIXER_TRC( TRC_ID_MIXER, "AVO = %#08x", uAVO );

  /*
   * Set Active video stop, this is also where the background stop as well
   */
  xstop = STCalculateViewportPixel(pModeLine, pModeLine->mode_params.active_area_width - 1);
  ystop = STCalculateViewportLine(pModeLine, pModeLine->mode_params.active_area_height - 1);

  uAVS = xstop | (ystop << 16);
  uBCS = xstop | (ystop << 16);

  WriteMixReg(MXn_AVS_REG_OFFSET, uAVS);

  MIXER_TRC( TRC_ID_MIXER, "AVS = %#08x", uAVS );

  /*
   * Set current mode now so that enabling the background plane will succeed.
   */
  m_CurrentMode = *pModeLine;

  if(HasBackground())
  {
    MIXER_TRC( TRC_ID_MIXER, "Enabling Background Colour" );
    MIXER_TRC( TRC_ID_MIXER, "BCO = %#08x", uBCO );
    MIXER_TRC( TRC_ID_MIXER, "BCS = %#08x", uBCS );
    MIXER_TRC( TRC_ID_MIXER, "BKC = %#08x", m_ulBackground );
    WriteMixReg(MXn_BKC_REG_OFFSET, m_ulBackground);
    WriteMixReg(MXn_BCO_REG_OFFSET, uBCO);
    WriteMixReg(MXn_BCS_REG_OFFSET, uBCS);

    SetControlAndCrossbar();
  }

  /*
   * The output pixels sent to the capture can be decimated by two
   * (useful in 2160@60Hz mode).
   *
   * This decimation consists on a basic average calculation of two
   * adjacent pixels : out_pix = (in1_pix + in2_pix)/2.
   *
   * The decimation can be set by GAM_MIXER0_CTL.cap_dec_en bitfield.
   * The capture programming should be aligned with this feature : the
   * Mixer active window is divided by 2 (in horizontal).
   *
   * For example :
   *   In 2160P@60Hz, the Mixer active window is 3840x2160.
   *   After decimation, the active window is 1920x2160.
   *
   * So the programming of the capture window should take into account
   * the active window size AFTER decimation.
   *
   * For now Capture decimation feature is going to be enabled when
   * capturing inputs with :
   * - 4K2K resolution (width > 1920).
   * - High frame rate (refresh_rate > 30Hz).
   */
  if((pModeLine->mode_params.active_area_width > 1920)
  && (pModeLine->mode_params.vertical_refresh_rate > 30000))
  {
    /*
     * Ultra High Definition display modes with high framerate are only
     * supported by the Mixer UHD on H418 SoC. For older SoC (i.e H407)
     * the Mixer start isn't reached while trying to setup such display
     * mode.
     *
     * In the future we can have a Capture hardware which allow capturing
     * 2160@60Hz input resolution without the need to decimation at Mixer
     * level. Then we should deal with such situation at this time.
     */
    uint32_t tmp = ReadMixReg(MXn_CTL_REG_OFFSET) | MIX_CTL_CAP_DEC_ENA;
    WriteMixReg(MXn_CTL_REG_OFFSET, tmp);
  }

  DEBUGMIXER(this, (m_secondcrossbarSize > 0)?1:0);

  MIXER_TRCOUT(TRC_ID_MIXER, "");

  return true;
}

void CGammaMixer::Stop(void)
{
  uint32_t i = 0;

  MIXER_TRCIN(TRC_ID_MIXER, "");

  vibe_os_lock_resource(m_lock);

  // Disable all planes
  for (i=0; i<m_planeCount; i++)
  {
    m_pZorder[i].bEnabled = FALSE;
  }
  WriteMixReg(MXn_CTL_REG_OFFSET, 0);
  WriteMixReg(MXn_ACT_REG_OFFSET, 0);

  m_CurrentMode.mode_id = STM_TIMING_MODE_RESERVED;

  DEBUGMIXER(this, (m_secondcrossbarSize > 0)?1:0);

  vibe_os_unlock_resource(m_lock);

  MIXER_TRCOUT(TRC_ID_MIXER, "");
}

void CGammaMixer::Suspend(void)
{
  MIXER_TRCIN(TRC_ID_MIXER, "");

  if(m_bIsSuspended)
    return;

  vibe_os_lock_resource(m_lock);
  {
    m_SavedFirstCrossbarRegister = ReadMixReg(MXn_CRB_REG_OFFSET);
    if (m_secondcrossbarSize > 0)
    {
      m_SavedSecondCrossbarRegister = ReadMixReg(MXn_CRB2_REG_OFFSET);
    }
    m_bIsSuspended = true;
  }
  vibe_os_unlock_resource(m_lock);

  DisableProcClock(true);

  MIXER_TRCOUT(TRC_ID_MIXER, "");
}

void CGammaMixer::Resume(void)
{
  MIXER_TRCIN(TRC_ID_MIXER, "");

  if(m_bIsSuspended)
  {
    m_bIsSuspended = false; // First so we can call Start() to but the main configuration back

    EnableProcClock();

    if(m_CurrentMode.mode_id != STM_TIMING_MODE_RESERVED)
      Start(&m_CurrentMode);

    /*
     * Restore crossbar settings.
     */
    WriteMixReg(MXn_CRB_REG_OFFSET, m_SavedFirstCrossbarRegister);
    if (m_secondcrossbarSize > 0)
    {
      WriteMixReg(MXn_CRB2_REG_OFFSET, m_SavedSecondCrossbarRegister);
    }
  }

  MIXER_TRCOUT(TRC_ID_MIXER, "");
}

bool CGammaMixer::SetPlaneDepth(const CDisplayPlane *plane, int depth, bool activate)
{
  int prevDepth = 0;
  int increment = 0;
  int emptyDepth = 0;
  int i = 0;
  bool bEnabled = false;

  MIXER_TRCIN(TRC_ID_MIXER, "");
  MIXER_TRC(TRC_ID_MIXER, "plane \"%s\" ID:%d depth = %d", plane->GetName(), plane->GetID(), depth);

  if(depth < 0)
  {
    depth = 0;
  }

  if (depth >= (int)m_planeCount)
  {
    depth = (int)m_planeCount-1;
  }

  vibe_os_lock_resource(m_lock);
  prevDepth = FindPlaneInZorder(plane);
  if (prevDepth >= (int)m_planeCount)
  {
    vibe_os_unlock_resource(m_lock);
    MIXER_TRC(TRC_ID_ERROR, "Plane %d not connected to mixer",  plane->GetID());
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  if ((activate == false) || (prevDepth == depth))
  {
    // Depth did not change: nothing to do
    vibe_os_unlock_resource(m_lock);
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return true;
  }

  // Set increment. Increment is postive if the new depth is bigger than the previous depth
  increment = (prevDepth < depth) ? 1 : -1;
  bEnabled = m_pZorder[prevDepth].bEnabled;
  m_pZorder[prevDepth].bEnabled = FALSE;
  m_pZorder[prevDepth].planeId  = m_planeCount; // ensure the next loop is exiting

  // find empty slot nearest to depth
  emptyDepth = depth;
  while (m_pZorder[emptyDepth].planeId != m_planeCount)
  {
    emptyDepth -= increment;
  }

  // move planes between depth (included) and empty depth
  // if emptyDepth == depth, does nothing
  for (i=emptyDepth; i!=depth; i+=increment)
  {
    m_pZorder[i].planeId  = m_pZorder[i+increment].planeId;
    m_pZorder[i].bEnabled = m_pZorder[i+increment].bEnabled;
  }

  m_pZorder[depth].planeId  = plane->GetID();
  m_pZorder[depth].bEnabled = bEnabled;

  SetControlAndCrossbar();

  vibe_os_unlock_resource(m_lock);
  MIXER_TRCOUT(TRC_ID_MIXER, "");
  return true;
}

bool CGammaMixer::GetPlaneDepth(const CDisplayPlane *plane, int *depth) const
{
  uint32_t i = 0;

  MIXER_TRCIN(TRC_ID_MIXER, "");

  vibe_os_lock_resource(m_lock);
  i = FindPlaneInZorder(plane);
  vibe_os_unlock_resource(m_lock);

  if (i >= m_planeCount)
  {
    MIXER_TRC(TRC_ID_ERROR, "Plane %d not connected to mixer",  plane->GetID());
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  *depth = i;
  MIXER_TRC(TRC_ID_MIXER, "plane \"%s\" at depth %d", plane->GetName(), i);
  MIXER_TRCOUT(TRC_ID_MIXER, "");
  return true;
}

bool CGammaMixer::SetMixerMap(const CDisplayPlane* plane, uint32_t mixerIDs)
{
  uint32_t planeId = plane->GetID();

  MIXER_TRCIN(TRC_ID_MIXER, "");

  if (planeId >= m_planeCount)
  {
    MIXER_TRC(TRC_ID_ERROR, "Invalid plane_id %d",  planeId);
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  if ((mixerIDs & ~MIXER_ID_MASK) != 0)
  {
    MIXER_TRC(TRC_ID_ERROR, "Invalid mixerIDs %d",  mixerIDs);
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return false;
  }

  if(pMixerMap[planeId] == mixerIDs)
  {
    MIXER_TRC(TRC_ID_MIXER, "Using same mixerIDs %d - No update required",  mixerIDs);
    MIXER_TRCOUT(TRC_ID_MIXER, "");
    return true;
  }

  pMixerMap[planeId] = mixerIDs;
  SetControlAndCrossbar();

  MIXER_TRCOUT(TRC_ID_MIXER, "");
  return true;
}


void CGammaMixer::SetControlAndCrossbar(void)
{
  uint32_t i, j = 0;
  uint32_t planeEnables = 0;
  uint32_t tmp = 0;
  uint32_t entry_shift = 0;
  uint32_t mixerId = 0;
  uint32_t planeId = 0;
  uint64_t crossbar_setup  = 0;
  uint32_t firstcrossbar_setup  = 0;
  uint32_t secondcrossbar_setup = 0;

  tmp = ReadMixReg(MXn_CTL_REG_OFFSET) & ~MIXER_ID_MASK;

  // Loop on all the plane Z-order array
  for (i=0; i<m_planeCount; i++)
  {
    if (m_pZorder[i].bEnabled == TRUE)
    {
      planeId = m_pZorder[i].planeId;
      planeEnables |= pMixerMap[planeId];

      // Loop on all the stm_mixer_id_t values
      for (j=MIX_CRB_MIN_VALUE; j<=MIX_CRB_MAX_VALUE; j++)
      {
        mixerId = (0x01<<j);
        if (pMixerMap[planeId] & mixerId)
        {
          // Update the crossbar setup
          crossbar_setup |= ((uint64_t)(j & m_crossbarEntryMask)) << entry_shift;

          // Update shift for next depth
          entry_shift += m_crossbarEntryShift;
        }
      }
    }
  }

  // Manage background visibility
  if (m_bBackgroundVisible == TRUE)
  {
    planeEnables |= MIXER_ID_BKG;
  }

  MIXER_TRC(TRC_ID_MIXER, "planeEnables = 0x%x", planeEnables);

  // Write CTL mixer register
  WriteMixReg(MXn_CTL_REG_OFFSET, (tmp | planeEnables));

  MIXER_TRC(TRC_ID_MIXER, "new crossbar_setup = 0x%016llx", crossbar_setup);
  firstcrossbar_setup  = (crossbar_setup & MIX_CRB_MASK);
  secondcrossbar_setup = ((crossbar_setup >> 32) & MIX_CRB_MASK);

  // Write CRB mixer registers
  WriteMixReg(MXn_CRB_REG_OFFSET, firstcrossbar_setup);
  if (m_secondcrossbarSize > 0)
  {
    WriteMixReg(MXn_CRB2_REG_OFFSET, secondcrossbar_setup);
  }
  else if (secondcrossbar_setup != 0)
  {
    MIXER_TRC(TRC_ID_ERROR, "Too many Z-order for this hardware");
  }

  DEBUGMIXER(this, (m_secondcrossbarSize > 0)?1:0);
}

bool CGammaMixer::GetLastVsyncStatus(uint32_t * status)
{
  uint32_t planes_enabled = ReadMixReg(MXn_CTL_REG_OFFSET) &  (MIX_CTL_VID1_DISPLAYENABLE |
                                                               MIX_CTL_VID2_DISPLAYENABLE |
                                                               MIX_CTL_GDP1_DISPLAYENABLE |
                                                               MIX_CTL_GDP2_DISPLAYENABLE |
                                                               MIX_CTL_GDP3_DISPLAYENABLE |
                                                               MIX_CTL_GDP4_DISPLAYENABLE |
                                                               MIX_CTL_GDP5_DISPLAYENABLE |
                                                               MIX_CTL_GDP6_DISPLAYENABLE |
                                                               MIX_CTL_VID3_DISPLAYENABLE);

  MIXER_TRCIN(TRC_ID_MIXER, "");

  if (m_bHasMSTRegister)
  {
    *status = ReadMixReg(MXn_MST_REG_OFFSET);
    // clear status by writing 1:
    WriteMixReg(MXn_MST_REG_OFFSET, MIX_MST_BUFF_UNDERFLOW);

    // Status is checked only if some planes are enabled
    if ( (*status & MIX_MST_BUFF_UNDERFLOW) && (planes_enabled != 0) )
    {
#ifdef CONFIG_STM_VIRTUAL_PLATFORM /* VSOC HCE WA: Avoid print overhead on uart, to be debugged, model issue ? */
      MIXER_TRC(TRC_ID_MIXER, "%s ### pixel FIFO underflow !!! ###", m_name);
#else
      MIXER_TRC(TRC_ID_ERROR, "%s ### pixel FIFO underflow !!! ###", m_name);
#endif
    }
    else
    {
      MIXER_TRC(TRC_ID_MIXER, "%s pixel FIFO OK", m_name);
    }

    MIXER_TRCOUT(TRC_ID_MIXER, "returning: 0x%08X", *status);
    return true;
  }
  else
  {
    // Default implementation without MST register
    *status = 0;
    MIXER_TRCOUT(TRC_ID_MIXER, "returning: 0x%08X", *status);
    return false;
  }
}

bool CGammaMixer::EnableProcClock(bool force_enable)
{
  int ret = 0;
  unsigned long rate = 0;

  /* Start processing clock only if BKG is enabled or farce_enable is set */
  bool enable = (force_enable || m_bBackgroundVisible);

  if(m_procClock.clk && (!m_procClock.enabled && enable))
  {
    ret = vibe_os_clk_enable(&m_procClock);
    if (ret)
    {
      MIXER_TRC(TRC_ID_ERROR, "Proc clock %s enable failed", m_procClock.name );
      goto error;
    }
    else
    {
      rate = vibe_os_clk_get_rate(&m_procClock);
      MIXER_TRC(TRC_ID_MIXER, "Proc clock %s enabled at %lu Hz", m_procClock.name, rate);
    }
  }
  return true;

error:
  return false;
}

bool CGammaMixer::DisableProcClock(bool force_disable)
{
  int ret = 0;

  if(m_procClock.clk && m_procClock.enabled)
  {
    /* Stop processing clock only if BKG is disabled or when suspending */
    if(!m_bBackgroundVisible || force_disable)
    {
      ret = vibe_os_clk_disable(&m_procClock);
      if (ret)
      {
        MIXER_TRC(TRC_ID_ERROR, "Proc clock %s disable failed", m_procClock.name );
        goto error;
      }
      else
      {
        MIXER_TRC(TRC_ID_MIXER, "Proc clock %s disable", m_procClock.name );
      }
    }
  }
  return true;

error:
  return false;
}
