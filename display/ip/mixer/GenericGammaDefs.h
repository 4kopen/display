/***********************************************************************
 *
 * File: display/gamma/GenericGammaDefs.h
 * Copyright (c) 2000, 2004, 2005 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef GENERICGAMMADEFS_H
#define GENERICGAMMADEFS_H

// Mixer crossbar register definitions
#define MIX_CRB_NOTHING      0x00
#define MIX_CRB_VID1         0x01
#define MIX_CRB_VID2         0x02
#define MIX_CRB_GDP1         0x03
#define MIX_CRB_GDP2         0x04
#define MIX_CRB_GDP3         0x05
#define MIX_CRB_GDP4         0x06
#define MIX_CRB_GDP5         0x07
#define MIX_CRB_GDP6         0x08
#define MIX_CRB_VID3         0x09
#define MIX_CRB_GDP7         0x0A
#define MIX_CRB_GDP8         0x0B
#define MIX_CRB_MASK         0xFFFFFFFF

#define MIX_CRB_MIN_VALUE MIX_CRB_VID1
#define MIX_CRB_MAX_VALUE MIX_CRB_GDP8


//Mixer Misc register definitions
#define MIX_MISC_VREF_SEL_SHIFT 0x0
#define MIX_MISC_ENA_CNT_SHIFT  0x1

// Mixer Control register definitions
#define MIX_CTL_BKC_DISPLAYENABLE   (1L<<0)
#define MIX_CTL_VID1_DISPLAYENABLE  (1L<<1)
#define MIX_CTL_VID2_DISPLAYENABLE  (1L<<2)
#define MIX_CTL_GDP1_DISPLAYENABLE  (1L<<3)
#define MIX_CTL_GDP2_DISPLAYENABLE  (1L<<4)
#define MIX_CTL_GDP3_DISPLAYENABLE  (1L<<5)
#define MIX_CTL_GDP4_DISPLAYENABLE  (1L<<6)
#define MIX_CTL_GDP5_DISPLAYENABLE  (1L<<7)
#define MIX_CTL_GDP6_DISPLAYENABLE  (1L<<8)
#define MIX_CTL_VID3_DISPLAYENABLE  (1L<<9)
#define MIX_CTL_709_NOT_601         (1L<<25)
#define MIX_CTL_CAP_DEC_ENA         (1L<<27)

// Mixer "Memory Status" register definitions
#define MIX_MST_BUFF_UNDERFLOW  0x1

#endif //GENERICGAMMADEFS_H
