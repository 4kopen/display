/***********************************************************************
 *
 * File: display/ip/gdpplus/GdpPlus.cpp
 * Copyright (c) 2014 by STMicroelectronics. All rights reserved.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <display/ip/gdpplus/GdpPlus.h>

/*
 * Support multiple instances of LLD driver (GDP and VBI planes are using
 * same GDP+ hardware ressources).
 */
#define GDP_PLUS_INVALID_PLANE_ID   0xFFFFFFFF
/* Max number of plane's instance can't reach the number of available
 * hardware instances */
#define MAX_GDP_PLUS_INSTANCE_NB    GDPGQR_LLD_MAX_HW_NUMBER

struct GdpPlusInstance_s
{
    uint32_t            plane_id;
    uint32_t            user_count;
    gdpgqr_lld_hdl_t    lldHandle;
};

static struct GdpPlusInstance_s gdpplus_plane_instances[MAX_GDP_PLUS_INSTANCE_NB] = { 0 };

/*******************************************************************************
Name        : Init
Description : Performs the GDP Plus subsystem initializations
Parameters  : pDisplayDevBase - [in] Logical address of Display device hardware
              gdpHwNb         - [in] Number of GDP Plus hardware instances
              gdpHwInfo       - [in] GDP Plus hardware instance info
Assumptions : Must be called only once, prior to any plane instantiation
Limitations :
Returns     : true if init passed, false else
*******************************************************************************/
bool CGDPPlus::Init(
                    const void*             pDisplayDevBase,
                    int                     gdpHwNb,
                    const GdpPlusHwInfo_s   gdpHwInfo[])
{
    TRCIN(TRC_ID_MAIN_INFO);

    enum gdpgqr_lld_error      err = GDPGQR_LLD_NO_ERR;
    struct gdpgqr_lld_init_s   gdpInit[GDPGQR_LLD_MAX_HW_NUMBER];

    // Check that number of HQVDP HW instances is supported by LLD
    if(gdpHwNb > GDPGQR_LLD_MAX_HW_NUMBER)
    {
        TRC(TRC_ID_ERROR, "Bad argument");
        TRCOUT(TRC_ID_MAIN_INFO);
        return false;
    }

    // Prepare LLD init params
    for(int i = 0; i < gdpHwNb; i++)
    {
        gdpInit[i].base_addr        = (const void*)((const uint8_t*)pDisplayDevBase + gdpHwInfo[i].offset);
        gdpInit[i].mixer_bit        = gdpHwInfo[i].mixerBit;
        gdpInit[i].private_data     = NULL;
    }

    // Initialize LLD
    err = gdpgqr_lld_init(gdpHwNb, gdpInit);
    if(err != GDPGQR_LLD_NO_ERR)
    {
        TRC(TRC_ID_ERROR, "gdpgqr_lld_init() failed, err=%d", err);
        TRCOUT(TRC_ID_MAIN_INFO);
        return false;
    }

    TRC(TRC_ID_MAIN_INFO, "GDP Plus LLD initialized (API version %u)", gdpgqr_lld_get_api_version());

    TRCOUT(TRC_ID_MAIN_INFO);
    return true;
}

/*******************************************************************************
Name        : DeInit
Description : Performs the GDP Plus deinitialization
Parameters  :
Assumptions : Must be called only once, once all planes are deleted
Limitations :
Returns     : true if deinit passed, false else
*******************************************************************************/
bool CGDPPlus::DeInit()
{
    TRCIN(TRC_ID_MAIN_INFO);

    enum gdpgqr_lld_error lldErr = GDPGQR_LLD_NO_ERR;

    // Terminate LLD
    lldErr = gdpgqr_lld_terminate();
    if(lldErr != GDPGQR_LLD_NO_ERR)
    {
        TRC(TRC_ID_ERROR, "gdpgqr_lld_terminate() failed, err=%d", lldErr);
        TRCOUT(TRC_ID_MAIN_INFO);
        return false;
    }

    TRC(TRC_ID_MAIN_INFO, "GDP Plus LLD terminated");

    TRCOUT(TRC_ID_MAIN_INFO);
    return true;
}

/*
 * Support multiple instances of LLD driver (GDP and VBI planes are using
 * same GDP+ hardware ressources).
 */
/*******************************************************************************
Name        : OpenLldSession
Description : Opens a GDP+ LLD session
               - Open the GDP Plus LLD session if there is no opened handle.
               - Retreive LLD handle used by the plane if there is/are opened handle(s).
Parameters  : plane_id                 - [in]  Plane Id
              profile                  - [in]  Plane profile (Full HD, 4Kp30, 4Kp60)
              mem_for_cmd              - [in]  Memory description allocated for LLD
              hw_used_bit_mask         - [out] ORed bit mask for the maximum number of GDPGQR HW IPs used for the requested profile
              lldHandle                - [out] LLD handle identifying the GDP+. NULL in case of failure
Assumptions : LLD memory has been allocated
Limitations :
Returns     : true if session successfully opened, false else
*******************************************************************************/
bool CGDPPlus::OpenLldSession(uint32_t                           plane_id,
                              enum gdpgqr_lld_profile_e          profile,
                              const gdpgqr_lld_mem_desc_s *      mem_for_cmd,
                              uint32_t                         * hw_used_bit_mask,
                              gdpgqr_lld_hdl_t                 * lldHandle)
{
    int i = 0;
    struct GdpPlusInstance_s   *gdpplus_plane_instance = 0;

    TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

    // check if the plane instance is already there in our list
    for(i = 0; i < MAX_GDP_PLUS_INSTANCE_NB; i++)
    {
        if(gdpplus_plane_instances[i].plane_id == plane_id)
        {
            gdpplus_plane_instance = &gdpplus_plane_instances[i];
            break;
        }
    }

    if(!gdpplus_plane_instance)
    {
        // find first available plane instance
        for(i = 0; i < MAX_GDP_PLUS_INSTANCE_NB; i++)
        {
            if(gdpplus_plane_instances[i].plane_id == GDP_PLUS_INVALID_PLANE_ID)
            {
                gdpplus_plane_instance = &gdpplus_plane_instances[i];
                break;
            }
        }
    }

    if(!gdpplus_plane_instance)
    {
        TRC(TRC_ID_ERROR, "Invalid GDP+ plane!");
        return false;
    }

    if(gdpplus_plane_instance->user_count == 0) // first instance
    {
        // Open LLD session
        enum gdpgqr_lld_error err = gdpgqr_lld_open( profile,           // [in]
                                                     mem_for_cmd,       // [in]
                                                     hw_used_bit_mask,  // [out]
                                                     &gdpplus_plane_instance->lldHandle);        // [out]

        if(err != GDPGQR_LLD_NO_ERR)
        {
          TRC( TRC_ID_ERROR, "gdpgqr_lld_open() failed, err=%d", err );
          return false;
        }
        gdpplus_plane_instance->plane_id = plane_id;

        TRC(TRC_ID_GDP_PLUS_PLANE, "GDP Plus LLD session opened with profile %s",
                                        profile == GDPGQR_PROFILE_FULL_HD ? "FULL HD" :
                                        profile == GDPGQR_PROFILE_4KP30   ? "4KP30"   :
                                        profile == GDPGQR_PROFILE_4KP60   ? "4KP60"   :
                                        "unrecognized");
    }

    *lldHandle = gdpplus_plane_instance->lldHandle;
    gdpplus_plane_instance->user_count++;

    TRCOUT( TRC_ID_GDP_PLUS_PLANE, "%d opened handles", gdpplus_plane_instance->user_count);
    return true;
}

/*******************************************************************************
Name        : CloseLldSession
Description : Close the GDP Plus LLD session
               - Close the GDP Plus LLD session if there is no more opened handles.
               - Invalidate LLD handle if there is/are remaining opened handle(s).
Parameters  : plane_id                 - [in]  Plane id
              lldHandle                - [out] Plane Id
              nb_remaining_handles     - [out] Number of remaining opened handles
Assumptions : LLD session is open (gdpgqr_lld_open called)
Limitations :
Returns     : true if session successfully closed, false else
*******************************************************************************/
bool CGDPPlus::CloseLldSession(uint32_t                           plane_id,
                              gdpgqr_lld_hdl_t                 * lldHandle,
                              uint32_t                         * nb_remaining_handles)
{
    struct GdpPlusInstance_s     *gdpplus_plane_instance = 0;

    TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

    if((!lldHandle) || (!nb_remaining_handles))
    {
        TRC(TRC_ID_ERROR, "Invalid arguments!");
        return false;
    }

    // find plane instance
    for(int i = 0; i < MAX_GDP_PLUS_INSTANCE_NB; i++)
    {
        if(gdpplus_plane_instances[i].plane_id == plane_id)
        {
            gdpplus_plane_instance = &gdpplus_plane_instances[i];
            break;
        }
    }

    if(!gdpplus_plane_instance)
    {
        TRC(TRC_ID_ERROR, "Invalid GDP+ plane!");
        return false;
    }

    if(gdpplus_plane_instance->user_count == 1) // last instance
    {
        enum gdpgqr_lld_error err = gdpgqr_lld_close(gdpplus_plane_instance->lldHandle);

        if(err != GDPGQR_LLD_NO_ERR)
        {
            TRC(TRC_ID_ERROR, "gdpgqr_lld_close() failed, err=%d", err);
            return false;
        }
        gdpplus_plane_instance->lldHandle = 0;
        gdpplus_plane_instance->plane_id  = GDP_PLUS_INVALID_PLANE_ID;
        TRC(TRC_ID_GDP_PLUS_PLANE, "GDP Plus LLD session closed");
    }

    gdpplus_plane_instance->user_count--;
    *lldHandle = 0;
    *nb_remaining_handles = gdpplus_plane_instance->user_count;

    TRCOUT( TRC_ID_GDP_PLUS_PLANE, "still %d opend handles", *nb_remaining_handles);

    return true;
}
