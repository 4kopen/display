/***********************************************************************
 *
 * File: gdpgqr_lld_api.c
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include "gdpgqr_lld_api.h"
#include "gdpgqr_split.h"
#include "gdpgqr_ip_mapping.h"
#include "c8gdpgqr_hw_lld.h"
#include "c8gdpgqr_plug.h"

#ifdef VERIFICATION_PLATFORM

#endif /* VERIFICATION_PLATFORM */

/*----------------------------------------------------------------------------*/
/** @defgroup lld_local_function LLD local functions */
/*----------------------------------------------------------------------------*/

/** @brief struct to store information specific to a given gdp hardware
    @ingroup lld_priv_struct
*/
struct gdpgqr_hw_s {
        void *base_addr; /**< @brief table containing base address of each GDPGQR */
        bool boot_done; /**< @brief table containing boot_done by GDPGQR */
        gdpgqr_lld_hdl_t resource_reserved; /**< @brief handle by which hw instance has been reserved */
        uint32_t mixer_bit;
};

/** @brief struct to save information relative to driver status
    @ingroup lld_priv_struct
*/
struct gdpgqr_global_info_s {
        bool init_done;  /**< @brief initialize has bee done */
        int hw_nb;       /**< @brief number of gdp available (info provided by driver) */
        int available_resources;
        bool cpu_bignotlittlendian; /**< @brief CPU Endianness */
        struct gdpgqr_hw_s *hw;
};

static struct gdpgqr_global_info_s  *gdpgqr_global_info = NULL;

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
/** @ingroup  lld_local_function
    @brief Handle correspond to a valid handle

    @retval 1 handle is valid
    @retval 0 handle is not valid

*/
/* TODO check if we would like to inline function */
static bool gdpgqr_is_handle_valid(gdpgqr_lld_hdl_t hdl)
{
        if ( hdl->magic == GDPGQR_MAGIC_NUMBER )
                return 1;
        else
                return 0;
}


/** @ingroup  lld_local_function
    @brief Reset driver status global variable
    @param [out] global_info Global status
*/
static void gdpgqr_init_hw(struct gdpgqr_global_info_s *global_info )
{
        int i;

        /* clean all previous status */
        gdpgqr_global_info->init_done = FALSE;
        /* default value for endianness; */
        gdpgqr_global_info->cpu_bignotlittlendian = FALSE;

        for ( i = 0 ; i < gdpgqr_global_info->hw_nb ; i++ ) {
                gdpgqr_global_info->hw[i].base_addr = 0;
                gdpgqr_global_info->hw[i].boot_done = 0;
                gdpgqr_global_info->hw[i].resource_reserved = NULL;
        }
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
uint8_t gdpgqr_lld_mixerbit2crb(uint32_t mixerbit) {
        uint8_t crb;
        TRCIN(TRC_ID_GDPGQR_LLD,"*** INFO : mixerbit=0x%.8x\n",mixerbit);
        switch(mixerbit) {
        case GDP1_DISPLAYENABLE_MASK:
                crb=GDPGQR1_CRB;
                break;
        case GDP2_DISPLAYENABLE_MASK:
                crb=GDPGQR2_CRB;
                break;
        case GDP3_DISPLAYENABLE_MASK:
                crb=GDPGQR3_CRB;
                break;
        case GDP4_DISPLAYENABLE_MASK:
                crb=GDPGQR4_CRB;
                break;
        default:
                crb=0;
                break;
        }
        return crb;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void gdpgqr_lld_set_cpu_endianness(bool bignotlittle) {
  gdpgqr_global_info->cpu_bignotlittlendian = bignotlittle;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum gdpgqr_lld_error gdpgqr_lld_init (int hw_nb,
                                       const struct gdpgqr_lld_init_s init_param[]
                                       )
{
        int i;

        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        if((NULL != gdpgqr_global_info) && (gdpgqr_global_info->init_done == TRUE)) {
                TRC(TRC_ID_GDPGQR_LLD, "GDP LLD Already initialized\n");
                return GDPGQR_LLD_NO_ERR;
        }

        /* allocate global info and init status */
        gdpgqr_global_info = (struct gdpgqr_global_info_s *)GDP_GQR_ALLOCATE_LLD_MEM(sizeof(struct gdpgqr_global_info_s));
        if(NULL == gdpgqr_global_info) {
                TRC(TRC_ID_ERROR, "failed to initialize GDP LLD module\n");
                return GDPGQR_LLD_ERR_INIT_NOT_DONE;
        }
        gdpgqr_global_info->hw = (struct gdpgqr_hw_s*)GDP_GQR_ALLOCATE_LLD_MEM(sizeof(struct gdpgqr_hw_s)*hw_nb);
        if(NULL == gdpgqr_global_info->hw ) {
              GDP_GQR_FREE_LLD_MEM(gdpgqr_global_info);
              TRC(TRC_ID_ERROR, "failed to initialize GDP LLD module with %d hw\n",hw_nb);
              return GDPGQR_LLD_ERR_INIT_NOT_DONE;
        }
        gdpgqr_global_info->hw_nb = hw_nb;
        gdpgqr_init_hw(gdpgqr_global_info);

        for ( i = 0 ; i < hw_nb; i++ ) {
                gdpgqr_global_info->hw[i].base_addr = (void *)(init_param[i].base_addr);
                gdpgqr_global_info->hw[i].mixer_bit = init_param[i].mixer_bit;
        }


        gdpgqr_global_info->available_resources = hw_nb;
        gdpgqr_global_info->init_done = TRUE;

        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");

        /* no way to detect error in this type of function */
        return GDPGQR_LLD_NO_ERR;
}

int gdpgqr_get_config_size(void)
{
        /*
         * We just need to allocate physical (DMA) memory for nodes.
         */
        return sizeof(struct gdpgqr_lld_hw_viewport_s);
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/

enum gdpgqr_lld_error gdpgqr_lld_get_memory_needs(enum gdpgqr_lld_profile_e profile,
                                                  uint32_t                  max_node,
                                                  struct gdpgqr_lld_mem_need_s *memory_needs )
{
        int hw_used;
        enum gdpgqr_lld_error result = GDPGQR_LLD_NO_ERR;

        switch (profile) {
        case GDPGQR_PROFILE_4KP60:
                hw_used = 2;
                break;
        case GDPGQR_PROFILE_4KP30:
        case GDPGQR_PROFILE_FULL_HD:
                hw_used = 1;
                break;
        default:
                result = GDPGQR_LLD_ERR_INVALID_PARAM;
        }

        if(result == GDPGQR_LLD_NO_ERR) {
                /* size is depending on viewports numbers configs */
                memory_needs->size = gdpgqr_get_config_size() * hw_used * max_node;

                /* ping pong size is 2 commands */
                memory_needs->size *= 2;

                /* 128 bits aligned as SoC interconnect is 128 bits */
                memory_needs->alignment = 16;
        }
        else {
                memory_needs->size      = 0;
                memory_needs->alignment = 0;
        }

        TRC(TRC_ID_GDPGQR_LLD, "mem : size = %d - alignment = %d\n", memory_needs->size, memory_needs->alignment);

        return result;
}

/** @ingroup  lld_local_function
    @brief initialize the command location in handler
    @param[in] mem_for_cmd: provide information about memory reserved for config
    @param[in] hw_inst: define which hardware instance is selected
    @param[in] side: define which gdp side have to be consider.
                     0 left
                     1 right
    @param[out] hdl: handle
*/
static void gdpgqr_init_ctxt_config(const struct gdpgqr_lld_mem_desc_s   *mem_for_cmd,
                                    int hw_inst, int side, gdpgqr_lld_hdl_t hdl )
{
        int gdpgqr_cmd_size = gdpgqr_get_config_size();

        /* hw instance specific info */
        hdl->base_addr[side] = gdpgqr_global_info->hw[hw_inst].base_addr;
        hdl->mixer_bit[side] = gdpgqr_global_info->hw[hw_inst].mixer_bit;

        /* side specific */
        /* do not initialized ping pong buffer content */
        hdl->ctxt_cmd[side].cmd_used.logical = (void *)
                ((uint8_t *)mem_for_cmd->logical + (2 * side + 1) * gdpgqr_cmd_size);
        hdl->ctxt_cmd[side].cmd_used.physical =
                ((dma_addr32_t)mem_for_cmd->physical + (2 * side + 1) * gdpgqr_cmd_size);
        hdl->ctxt_cmd[side].cmd_used.size = gdpgqr_cmd_size;

        hdl->ctxt_cmd[side].cmd_next_vsync.logical = (void *)
                ((uint8_t *)mem_for_cmd->logical + (2 * side) * gdpgqr_cmd_size);
        hdl->ctxt_cmd[side].cmd_next_vsync.physical =
                ((dma_addr32_t)mem_for_cmd->physical + (2 * side) * gdpgqr_cmd_size);
        hdl->ctxt_cmd[side].cmd_next_vsync.size = gdpgqr_cmd_size;

        /* we don't care about alignment in this context */
        hdl->ctxt_cmd[side].cmd_used.alignment = 0;
        hdl->ctxt_cmd[side].cmd_next_vsync.alignment = 0;
}

/** @brief Initialize STBus plug
    @ingroup lld_internal_function
    @param[in] hdl : handle
    @param[in] gdpgqr_plug_conf : configuration of read plug
*/
enum gdpgqr_lld_error gdpgqr_lld_set_plug_reg_from_handle(gdpgqr_lld_hdl_t                hdl,
                                                          const struct gdpgqr_lld_plug_s * stbp_setup)
{
        int i;
        TRCIN(TRC_ID_GDPGQR_LLD, "\n");
        /* check handle is valid */
        if ( gdpgqr_is_handle_valid(hdl) == 0 ) {
                return GDPGQR_LLD_ERR_INVALID_PARAM;
        }
        for (i=0;i<hdl->reserved_resources;i++) {
               if (gdpgqr_lld_set_plug_reg_from_address(hdl->base_addr[i], stbp_setup) == GDPGQR_LLD_ERR_INVALID_PARAM) {
                      return GDPGQR_LLD_ERR_INVALID_PARAM;
               }
        }
        return GDPGQR_LLD_NO_ERR;
}

/** @brief get STBus plug config
    @ingroup lld_internal_function
    @param[in] hdl : handle
    @param[in] gdpgqr_plug_conf : configuration of read plug
*/
enum gdpgqr_lld_error gdpgqr_lld_get_plug_reg_from_handle(gdpgqr_lld_hdl_t                hdl,
                                                          struct gdpgqr_lld_plug_s * stbp_setup)
{
        int i;
        TRCIN(TRC_ID_GDPGQR_LLD, "\n");
  /* check handle is valid */
        if ( gdpgqr_is_handle_valid(hdl) == 0 ) {
                return GDPGQR_LLD_ERR_INVALID_PARAM;
        }
        for (i=0;i<hdl->reserved_resources;i++) {
               if (gdpgqr_lld_get_plug_reg_from_address(hdl->base_addr[i], &stbp_setup[i]) == GDPGQR_LLD_ERR_INVALID_PARAM) {
                      return GDPGQR_LLD_ERR_INVALID_PARAM;
               }
        }
        return GDPGQR_LLD_NO_ERR;
}

static void c8gdpgqr_set_endianness(void *base_addr, bool bignotlittle) {
        TRCIN(TRC_ID_GDPGQR_LLD, "(GDPGQR %p) GDPGQR command endianness = 0x%08X\n",
              base_addr, bignotlittle);
        GDP_GQR_REG32_WRITE(base_addr, GAM_GDP_GQR_PAS , bignotlittle? GDP_GQR1_GAM_GDP_GQR_PAS_BIGNOTLITTLE_MASK:0);
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum gdpgqr_lld_error gdpgqr_lld_open (enum gdpgqr_lld_profile_e profile,
                                       const struct gdpgqr_lld_mem_desc_s *mem_for_cmd,
                                       uint32_t *hw_used_bit_mask,
                                       gdpgqr_lld_hdl_t *hdl_p)
{
        gdpgqr_lld_hdl_t gdpgqr_ctxt = NULL;
        enum gdpgqr_lld_error result = GDPGQR_LLD_NO_ERR;
        int gdpgqr_to_use;
        int i;
        int nb_hw_inst;
        int side;

        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        /* init has to be done previously */
        if (gdpgqr_global_info->init_done == FALSE) {
                result = GDPGQR_LLD_ERR_INIT_NOT_DONE;
                goto out;
        }
        /* determine how many GDPGQR are required */
        switch (profile) {
        case GDPGQR_PROFILE_4KP60:
                gdpgqr_to_use = 2;
                break;
        case GDPGQR_PROFILE_4KP30:
        case GDPGQR_PROFILE_FULL_HD:
                gdpgqr_to_use = 1;
                break;
        default:
                gdpgqr_to_use = -1;
                result = GDPGQR_LLD_ERR_INVALID_PARAM;
                goto out;
        }

        /* exit if no enough resources are available */
        if ( gdpgqr_to_use > gdpgqr_global_info->available_resources ) {
                result = GDPGQR_LLD_ERR_NO_RESOURCE_AVAILABLE;
                goto out;
        }

        gdpgqr_ctxt = (gdpgqr_lld_hdl_t)GDP_GQR_ALLOCATE_LLD_MEM(sizeof(struct gdpgqr_lld_ctxt));
        gdpgqr_ctxt->profile = profile;

        /* defined which GDPGQR to use */
        nb_hw_inst = 0;
        side = 0; /* 0 : left, 1: right */
        *hw_used_bit_mask = 0;

        for ( i = 0; i < gdpgqr_global_info->hw_nb; i ++ ) {
                if ( gdpgqr_global_info->hw[i].resource_reserved == 0 ) {
                        /* tag resources as reserved */
                        gdpgqr_global_info->hw[i].resource_reserved = gdpgqr_ctxt;

                        /* init handle with global_info */
                        gdpgqr_init_ctxt_config(mem_for_cmd, i, side, gdpgqr_ctxt);

                        TRC(TRC_ID_GDPGQR_LLD, "reserved hw %d side[%d] addr=%p\n",i,side,gdpgqr_ctxt->base_addr[side]);

                        /* provide info to easily configure crossbar */
                        *hw_used_bit_mask |= gdpgqr_ctxt->mixer_bit[side];
                        /* update global status */
                        gdpgqr_global_info->hw[i].boot_done = TRUE;
                        gdpgqr_global_info->available_resources--;

                        c8gdpgqr_set_endianness(gdpgqr_ctxt->base_addr[side],gdpgqr_global_info->cpu_bignotlittlendian);

                        nb_hw_inst++;
                        side++;

                        if ( nb_hw_inst == gdpgqr_to_use ) {
                          /* do post increment to know how many hw resources
                           * has been reserved
                           */
                          /* Memorize how many gdp are used for this plan */
                          gdpgqr_ctxt->reserved_resources = gdpgqr_to_use;
                          break;
                        }
                }
        }
        /* not enough hw resources are available */
        if ( gdpgqr_to_use > nb_hw_inst ) {
                result = GDPGQR_LLD_ERR_UNDEFINED;
                goto out;
        }

        /* initialize control handler */
        gdpgqr_ctxt->state = gdpgqr_open_done;
        gdpgqr_ctxt->magic = GDPGQR_MAGIC_NUMBER;

        /* return handler */
        *hdl_p = gdpgqr_ctxt;

        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");

        return result;

out:
        if (gdpgqr_ctxt) {
                GDP_GQR_FREE_LLD_MEM(gdpgqr_ctxt);
        }
        return result;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
uint32_t gdpgqr_lld_get_api_version( void )
{

        uint32_t api_version = 1;

        TRC(TRC_ID_GDPGQR_LLD, "API version : v%3d\n", api_version);

        /* return api revision */
        return api_version;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void gdpgqr_lld_set_lut ( enum gdpgqr_lut_config_e    lut_config,
                          uint32_t                    size,
                          uint32_t                    gdpgqr_lld_lut[],
                          uint8_t                     *max_coeff)
{
        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        TRC(TRC_ID_GDPGQR_LLD,
                "(TO BE IMPLEMENTED) GDPGQR set lut\n");

        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void gdpgqr_lld_set_zoom_param( const struct gdpgqr_lld_size_s *input_size,
                                const struct gdpgqr_lld_size_s *output_size,
                                const enum gdpgqr_lut_config_e *strength,
                                struct gdpgqr_lld_hw_viewport_s *link_list_node,
                                struct gdpgqr_lut_table_s lut_conf)
{
        uint8_t hmaxay = 0,hmaxuv = 0,vmaxay = 0,vmaxuv = 0;

        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        TRC(TRC_ID_GDPGQR_LLD, "(TO BE IMPLEMENTED) GDPGQR set zoom param\n");
        link_list_node->HSRC = (input_size->width  * (1<<GDP_GQR_FIXPOINT_INCREMENT)) / output_size->width;
        link_list_node->VSRC = (input_size->height * (1<<GDP_GQR_FIXPOINT_INCREMENT)) / output_size->height;

        GDPGQR_REPLACE_FIELD(link_list_node->HP2,hmaxay,GDPGQR_HP2_MAXAY);
        GDPGQR_REPLACE_FIELD(link_list_node->HP2,hmaxuv,GDPGQR_HP2_MAXUV);

        GDPGQR_REPLACE_FIELD(link_list_node->VP2,vmaxay,GDPGQR_VP2_MAXAY);
        GDPGQR_REPLACE_FIELD(link_list_node->VP2,vmaxuv,GDPGQR_VP2_MAXUV);


        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
/** @ingroup  lld_local_function
    @brief determined if split is required or not
    @retval TRUE:  config has to be split
            FALSE: not split is required
*/
static bool gdpgqr_split_required(enum gdpgqr_lld_profile_e profile,
                                  const struct gdpgqr_lld_output_param_s * output_param)
{
  bool ret = FALSE;
       if (profile == GDPGQR_PROFILE_4KP60) {
               if ( output_param->output_vtg_fps > 30000 ) {
                       if ((output_param->width * output_param->height) > (1920 * 1080)) {
                               ret = TRUE;
                       }
               }
       }
  return ret;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static void gdpgqr_switch_configs(gdpgqr_lld_hdl_t hdl)
{
        int i;
        struct gdpgqr_lld_mem_desc_s tmp;

        for (i = 0; i < hdl->reserved_resources; i++ ) {
                tmp = hdl->ctxt_cmd[i].cmd_next_vsync;
                hdl->ctxt_cmd[i].cmd_next_vsync = hdl->ctxt_cmd[i].cmd_used;
                hdl->ctxt_cmd[i].cmd_used = tmp;
        }
}

enum gdpgqr_lld_error gdpgqr_lld_set_conf(gdpgqr_lld_hdl_t hdl,
                                          const struct gdpgqr_lld_output_param_s * output_param,
                                          const struct gdpgqr_lld_conf_s *conf,
                                          int *setup_nb,
                                          struct gdpgqr_lld_mixer_setup_s mixer_setup[GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE]
                                          )
{
        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        /* check handle is valid */
        if ( gdpgqr_is_handle_valid(hdl) == 0 ) {
                return GDPGQR_LLD_ERR_INVALID_PARAM;
        }
        /* update status in case a stop had been called before */
        if ( hdl->state == gdpgqr_stop_done ) {
                hdl->state = gdpgqr_open_done;
        }

        if ( gdpgqr_split_required(hdl->profile, output_param) ) {
                /* do split */
                *setup_nb = 2;
        } else {
                *setup_nb = 1;
        }

        hdl->used_resources = *setup_nb;

        /* write config in memory using one or 2 gdp */
        gdpgqr_write_config(hdl, conf, mixer_setup, *setup_nb);

        /* update pointer to be ready for next time */
        gdpgqr_switch_configs(hdl);

        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");

        return GDPGQR_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum gdpgqr_lld_error gdpgqr_lld_stop (gdpgqr_lld_hdl_t hdl)
{
        int i;

        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        if ( gdpgqr_is_handle_valid(hdl) == 0 ) {
                return GDPGQR_LLD_ERR_INVALID_PARAM;
        }

        /* stop processing of all reserved gdp */
        for (i = 0; i < hdl->reserved_resources; i++ ) {
          /* TBD */
        }
        hdl->state = gdpgqr_stop_done;

        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");

        return GDPGQR_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum gdpgqr_lld_error gdpgqr_lld_update_irr_params(gdpgqr_lld_hdl_t hdl,
                const struct gdpgqr_lld_irr_params_s *irr_params)
{
        int i = 0;

        if ( gdpgqr_is_handle_valid(hdl) == 0 ) {
                return GDPGQR_LLD_ERR_INVALID_PARAM;
        }

        for (i = 0; i < hdl->used_resources; i++) {
                TRC( TRC_ID_GDPGQR_LLD, "****** writing RBW=%d BWE=%d *****\n",
                                irr_params->RBW, irr_params->BWE );
                c8gdpgqr_config_update_irr_params( hdl->base_addr[i], irr_params);
        }
        return GDPGQR_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum gdpgqr_lld_error gdpgqr_lld_set_irr(gdpgqr_lld_hdl_t hdl,
                const struct gdpgqr_lld_irr_params_s * irr_params)
{
        /* check handle is valid */
        if ( gdpgqr_is_handle_valid(hdl) == 0 ) {
               return GDPGQR_LLD_ERR_INVALID_PARAM;
        }

        gdpgqr_lld_update_irr_params(hdl, irr_params);

        return GDPGQR_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum gdpgqr_lld_error gdpgqr_lld_close (gdpgqr_lld_hdl_t hdl)
{
        int i;

        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        /* check previous condition */
        if (hdl->state != gdpgqr_stop_done ) {
                return GDPGQR_LLD_ERROR_NO_STOP_DONE;
        }

        /* clear the magic flag to detect incorrect handle usage */
        hdl->magic = 0;

        /* update available resourced */
        gdpgqr_global_info->available_resources +=
                hdl->reserved_resources;

        /* free previously reserved resource */
        for (i = 0; i < gdpgqr_global_info->hw_nb; i++ ) {
                if ( gdpgqr_global_info->hw[i].resource_reserved == hdl ) {
                        gdpgqr_global_info->hw[i].resource_reserved = NULL;
                }
        }

        /* free handle */
        GDP_GQR_FREE_LLD_MEM(hdl);

        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");

        return GDPGQR_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum gdpgqr_lld_error gdpgqr_lld_terminate(void)
{
        TRCIN(TRC_ID_GDPGQR_LLD, "\n");

        /* free structure used to store lld specific info */
        GDP_GQR_FREE_LLD_MEM(gdpgqr_global_info);

        TRCOUT(TRC_ID_GDPGQR_LLD, "\n");

        return GDPGQR_LLD_NO_ERR;
}

