/***********************************************************************
 *
 * File: gdpgqr_split.c
 * Copyright (c) 2000-2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

 /**@file gdpgqr_split.c
    @brief low level driver internal function

    This file content function used internally by lld.
 */


#include "gdpgqr_ip_mapping.h"
#include "gdpgqr_split.h"

static int gdpgqr_getbyteperpixel(uint32_t format){
        int bpp;
        switch (format) {
        case GDPGQR_FORMAT_CLUT8:
                bpp = 1;
                break;
        case GDPGQR_FORMAT_RGB565:
        case GDPGQR_FORMAT_RGB1555:
        case GDPGQR_FORMAT_RGB4444:
        case GDPGQR_FORMAT_YCbCr422:
                bpp = 2;
                break;
        case GDPGQR_FORMAT_RGB888:
        case GDPGQR_FORMAT_RGB8565:
        case GDPGQR_FORMAT_YCbCr888:
                bpp = 3;
                break;
        default :
                bpp = 4;
                break;
        }
        return bpp;
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

void dumpvp(const struct gdpgqr_lld_hw_viewport_s* viewport_p) {
        TRC(TRC_ID_GDPGQR_LLD, "(vp %p)\n",viewport_p);
        TRC(TRC_ID_GDPGQR_LLD, "vpo=%.8x\n",viewport_p->VPO);
        TRC(TRC_ID_GDPGQR_LLD, "vps=%.8x\n",viewport_p->VPS);
        TRC(TRC_ID_GDPGQR_LLD, "pml=%.8x\n",viewport_p->PML);
        TRC(TRC_ID_GDPGQR_LLD, "hip=%.8x\n",viewport_p->HIP);
        TRC(TRC_ID_GDPGQR_LLD, "hsrc=%.8x\n",viewport_p->HSRC);
        TRC(TRC_ID_GDPGQR_LLD, "size=%.8x\n",viewport_p->SIZE);
        TRC(TRC_ID_GDPGQR_LLD, "crop=%.8x\n",viewport_p->CROP);
}

/** @brief update command according to new_param
    @ingroup  lld_local_function

    @param[in] new_param : parameter to use to update command
    @param[out] viewport_p : viewport to be updated
*/

static void gdpgqr_update_viewport(const struct cmd_split_param *new_param,
                                   struct gdpgqr_lld_hw_viewport_s* viewport_p) {

        TRCIN(TRC_ID_GDPGQR_LLD, "before up\n"); dumpvp(viewport_p);

        GDPGQR_REPLACE_FIELD(viewport_p->VPO, new_param->xdo,GDPGQR_VPO_XDO);
        GDPGQR_REPLACE_FIELD(viewport_p->VPS, new_param->xds,GDPGQR_VPS_XDS);
        GDPGQR_REPLACE_FIELD(viewport_p->HIP, new_param->init_phase,GDPGQR_HIP_INIT_PHASE);
        GDPGQR_REPLACE_FIELD(viewport_p->SIZE,new_param->input_width,GDPGQR_SIZE_WIDTH);

        viewport_p->PML = new_param->pixmap_addr;

        GDPGQR_REPLACE_FIELD(viewport_p->CROP,new_param->crop_xdo,GDPGQR_OUTPUT_PICTURE_CROP_XD0);
        GDPGQR_REPLACE_FIELD(viewport_p->CROP,new_param->crop_xds,GDPGQR_OUTPUT_PICTURE_CROP_XDS);

        TRCOUT(TRC_ID_GDPGQR_LLD, "after up\n"); dumpvp(viewport_p);

}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static void c8gdpgqr_SetNVNAddress(void *base_addr, uint32_t cmd_address) {
        TRCIN(TRC_ID_GDPGQR_LLD, "(GDPGQR %p) GDPGQR command physical address = 0x%08X\n",
              base_addr, cmd_address);
        GDP_GQR_REG32_WRITE(base_addr, GAM_GDP_GQR_NVN , cmd_address);
}


/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void gdpgqr_prepare_split(const struct cmd_origi_param *full_param_p,
                          struct cmd_split_param *side_param,
                          uint32_t ctl)
{
        uint32_t format = (ctl & GDPGQR_CTL_FORMAT_MASK) >> GDPGQR_CTL_FORMAT_SHIFT;
        int bpp = gdpgqr_getbyteperpixel(format);
        bool hsrc_enable =(ctl & GDPGQR_ENABLE_HORIZONTAL_RESCALE_MASK) ? TRUE : FALSE;
        bool is422 = format == GDPGQR_FORMAT_YCbCr422 ? TRUE : FALSE;

        TRCIN(TRC_ID_GDPGQR_LLD,"byte per pixel = %d  hsrc_enable=%d incr=%d phase=%d is422=%d\n",bpp,hsrc_enable,full_param_p->hsrc_incr,full_param_p->init_phase,is422);

        GDP_GQRSplitProgrammation(full_param_p,side_param,hsrc_enable,is422,bpp);
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void gdpgqr_write_config(gdpgqr_lld_hdl_t hdl,
                         const struct gdpgqr_lld_conf_s *expected_conf,
                         struct gdpgqr_lld_mixer_setup_s mixer_setup[],
                         uint8_t hw_to_use_cnt
                         )
{
        struct cmd_origi_param full_param;
        struct cmd_split_param side_param[2];
        const struct gdpgqr_lld_conf_s *current_conf_p = expected_conf;
        int nodeoffset = 0;

        //        uint16_t output_width;
        int i;

        uint32_t ctl;
        struct gdpgqr_lld_hw_viewport_s* viewport_p;
        bool end_of_viewport = FALSE;

        /* init mixer bits */
        GDP_GQR_MEMSET(mixer_setup,hw_to_use_cnt*sizeof(struct gdpgqr_lld_mixer_setup_s));

        /* loop on current_conf_p until end of link list */
        do {
                ctl = current_conf_p->viewport.CTL;

                if (ctl & GDPGQR_WAIT_NEXT_VSYNC_MASK) {
                        end_of_viewport = TRUE;
                }

                TRC(TRC_ID_GDPGQR_LLD, "CTL=0x%.8x\n",ctl);

                if ( hw_to_use_cnt > 1 ) {
                        TRC(TRC_ID_GDPGQR_LLD, "SPLIT ACTIVATED %.8x %.8x %.8x\n",current_conf_p->viewport.HIP,GDPGQR_HIP_INIT_PHASE_MASK,GDPGQR_HIP_INIT_PHASE_SHIFT);

                        /* pick input from original command */
                        full_param.xdo         = GDPGQR_GET_FIELD(current_conf_p->viewport.VPO ,GDPGQR_VPO_XDO);
                        full_param.xds         = GDPGQR_GET_FIELD(current_conf_p->viewport.VPS ,GDPGQR_VPS_XDS);
                        full_param.input_width = GDPGQR_GET_FIELD(current_conf_p->viewport.SIZE,GDPGQR_SIZE_WIDTH);
                        full_param.init_phase  = GDPGQR_GET_FIELDSIGNED(current_conf_p->viewport.HIP ,GDPGQR_HIP_INIT_PHASE);
                        full_param.hsrc_incr   = GDPGQR_GET_FIELD(current_conf_p->viewport.HSRC ,GDPGQR_HSRC_INCREMENT);
                        full_param.pixmap_addr = current_conf_p->viewport.PML;

                        gdpgqr_prepare_split(&full_param, side_param, ctl);

                        TRC(TRC_ID_GDPGQR_LLD, "full  init_phase = %d hsrc_incr = %d\n", full_param.init_phase,full_param.hsrc_incr);
                        TRC(TRC_ID_GDPGQR_LLD, "full  xdo = %d, xds = %d outwidth = %d\n", full_param.xdo, full_param.xds, full_param.xds - full_param.xdo + 1);
                        TRC(TRC_ID_GDPGQR_LLD, "full  pml = %.8x, inwidth = %d\n", full_param.pixmap_addr, full_param.input_width);

                        TRC(TRC_ID_GDPGQR_LLD, "left  init_phase = %d\n",side_param[0].init_phase);
                        TRC(TRC_ID_GDPGQR_LLD, "right init_phase = %d\n",side_param[1].init_phase);

                        TRC(TRC_ID_GDPGQR_LLD, "left  xdo = %d, xds = %d outwidth = %d\n", side_param[0].xdo, side_param[0].xds, side_param[0].xds - side_param[0].xdo + 1);
                        TRC(TRC_ID_GDPGQR_LLD, "right xdo = %d, xds = %d outwidth = %d\n", side_param[1].xdo, side_param[1].xds, side_param[1].xds - side_param[1].xdo + 1);
                        TRC(TRC_ID_GDPGQR_LLD, "left  pml = %.8x, inwidth = %d\n", side_param[0].pixmap_addr, side_param[0].input_width);
                        TRC(TRC_ID_GDPGQR_LLD, "right pml = %.8x, inwidth = %d\n", side_param[1].pixmap_addr, side_param[1].input_width);

                }

                /* copy coef once and only if rescale on */
                if (ctl & GDPGQR_ENABLE_HFILTER_UPDATE_MASK) {
                        TRC(TRC_ID_GDPGQR_LLD, "Write H Coef at logical address 0x%p\n",
                              ((char*)hdl->ctxt_cmd[0].cmd_next_vsync.logical + nodeoffset) + GDPGQR_HFP_OFFSET);

                        GDP_GQR_MEMCPY(((char*)hdl->ctxt_cmd[0].cmd_next_vsync.logical + nodeoffset) + GDPGQR_HFP_OFFSET,
                               ((char*)(&(current_conf_p->viewport))) + GDPGQR_HFP_OFFSET,
                               GDPGQR_H_COEFF_SIZE );
                } else {
                        TRC(TRC_ID_GDPGQR_LLD, "no H filter\n");
                }
                if (ctl & GDPGQR_ENABLE_VFILTER_UPDATE_MASK) {
                        TRC(TRC_ID_GDPGQR_LLD, "Write V Coef at logical address 0x%p\n",
                              ((char*)hdl->ctxt_cmd[0].cmd_next_vsync.logical + nodeoffset) + GDPGQR_VFP_OFFSET);

                        GDP_GQR_MEMCPY(((char*)hdl->ctxt_cmd[0].cmd_next_vsync.logical + nodeoffset) + GDPGQR_VFP_OFFSET,
                               ((char*)(&(current_conf_p->viewport))) + GDPGQR_VFP_OFFSET,
                               GDPGQR_V_COEFF_SIZE );
                } else {
                        TRC(TRC_ID_GDPGQR_LLD, "no V filter\n");
                }
                /* TODO first implementation where parameters to update are updated
                   in a second time */

                for (i = 0; i < hw_to_use_cnt; i++) {
                        /*
                         * i = 0 => gdpgqr left
                         * i = 1 => gdpgqr right
                         * */
                        viewport_p = (struct gdpgqr_lld_hw_viewport_s*)(((char*)hdl->ctxt_cmd[i].cmd_next_vsync.logical) + nodeoffset);


                        TRC(TRC_ID_GDPGQR_LLD, "(GDPGQR %p) Write GDPGQR command at logical address %p offset=%d\n",
                              hdl->base_addr[i], viewport_p, nodeoffset);
                        GDP_GQR_MEMCPY(viewport_p, &(current_conf_p->viewport),
                               GDPGQR_CONF_SIZE);

                        /* set NVN on next node or first node if last node of the list */
                        viewport_p->NVN = ((uint32_t)hdl->ctxt_cmd[i].cmd_next_vsync.physical) +
                                (current_conf_p->next != NULL ?
                                 (nodeoffset + sizeof(struct gdpgqr_lld_hw_viewport_s) * hw_to_use_cnt * 2) : 0);


                        TRC(TRC_ID_GDPGQR_LLD, "Next vp %.8x\n",viewport_p->NVN);

                        /* reference coef on command 0 for both cmd */
                        viewport_p->HFP = (uint32_t)hdl->ctxt_cmd[0].cmd_next_vsync.physical + nodeoffset + GDPGQR_HFP_OFFSET;

                        viewport_p->VFP = (uint32_t)hdl->ctxt_cmd[0].cmd_next_vsync.physical + nodeoffset + GDPGQR_VFP_OFFSET;


                        /* patch command only in case of split */
                        if ( hw_to_use_cnt > 1 ) {
                                gdpgqr_update_viewport(&side_param[i], viewport_p);
                        }

                        /* build video parameters to export */
                        mixer_setup[i].is_active = TRUE;
                        mixer_setup[i].mixer_bit = hdl->mixer_bit[i];
                        TRC(TRC_ID_GDPGQR_LLD,"mixerbit=0x%x\n",mixer_setup[i].mixer_bit);
                }

                nodeoffset += sizeof(struct gdpgqr_lld_hw_viewport_s) * hw_to_use_cnt * 2;

                TRC(TRC_ID_GDPGQR_LLD, "current conf-> next = %p\n",current_conf_p->next);

                current_conf_p = current_conf_p->next;


                /* loop until last node of linklist */
        } while (current_conf_p != NULL);

        if (end_of_viewport == FALSE) {
                TRCOUT(TRC_ID_GDPGQR_LLD,"***************** End of link list reached without last node in vsync\n");
        }

        /* ensure that command register access is not done before end of
         * write in memory
         */
        GDP_GQR_WRITE_MB();

        for (i  = 0; i < hw_to_use_cnt; i++) {
                /* write command address on side  */
                c8gdpgqr_SetNVNAddress(hdl->base_addr[i],
                                       (uint32_t)hdl->ctxt_cmd[i].cmd_next_vsync.physical);
        }
}
