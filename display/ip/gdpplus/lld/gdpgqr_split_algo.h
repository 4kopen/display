/***********************************************************************
 *
 * File: gdpgqr_split_algo.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef GDPGQR_SPLIT_ALGO_H_
#define GDPGQR_SPLIT_ALGO_H_
#include "gdpgqr_lld_platform.h"

struct cmd_origi_param {
        uint16_t xdo;          /**< @brief x coordonnate of the first pixel of the viewport in the output image */
        uint16_t xds;          /**< @brief x coordonnate of the last pixel of the viewport in the output image */
        int32_t  init_phase;   /**< @brief horizontal init phase */
        uint32_t hsrc_incr;    /**< @brief horizontal increment */
        uint16_t input_width;  /**< @brief input width  */
        uint32_t pixmap_addr;  /**< @brief pixmap address */
};

struct cmd_split_param {
        uint16_t xdo;          /**< @brief x coordonnate of the first pixel of the viewport in the output image */
        uint16_t xds;          /**< @brief x coordonnate of the last pixel of the viewport in the output image */
        int32_t  init_phase;   /**< @brief horizontal init phase */
        uint16_t input_width;  /**< @brief input width  */
        uint32_t pixmap_addr;  /**< @brief pixmap address */
        uint16_t crop_xdo;     /**< @brief crop value to use to program VideoPlug */
        uint16_t crop_xds;     /**< @brief crop value to use to program VideoPlug */
};

enum side_e {
        LEFT = 0,
        RIGHT = 1
};

/*!
 * /brief Configure parameter of dual GDP_GQR
 * @param[in]  full_param_p original parameters without split
 * @param[out] side_param output parameters (left and right)
 * @param[in] hsrc_enable  scaler enable
 * @param[in] is422 for split position of right side, the first pixel read in memory must be even in 422
 * @param[in] bpp byte per pixel for address computation
 */
void GDP_GQRSplitProgrammation(const struct cmd_origi_param *full_param_p,
                               struct cmd_split_param *side_param,
                               bool hsrc_enable,
                               bool is422,
                               int bpp);

#endif /* GDP_SPLIT_ALGO_H_ */
