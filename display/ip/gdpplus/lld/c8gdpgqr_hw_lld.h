/***********************************************************************
 *
 * File: c8gdpgqr_hw_lld.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/


 /**@file c8gdpgqr_hw_lld.h
    @brief boot GDPGQR hardware and check its status
 */

#ifndef C8GDPGQR_HW_LLD_H_
#define C8GDPGQR_HW_LLD_H_

#include "c8gdpgqr_stddefs.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief Do initialization of the IP
 *
 * \param[in] base_addr base address of the IP
 */
void c8gdpgqr_BootIp(void *base_addr);

/*!
 * \brief to wait until reset is done
 *
 * \param[in] base_address base address of the IP
 */
bool c8gdpgqr_IsBootIpDone (void *base_address);

/*!
 * \brief return true when FW ready to process a command
 *
 * \param[in] base_addr base address of the IP
 */
bool c8gdpgqr_IsReady(void *base_addr);

/* C++ support */
#ifdef __cplusplus
}
#endif

#endif /* C8GDPGQR_HW_LLD_H_ */
