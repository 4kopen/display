/***********************************************************************
 *
 * File: c8gdpgqr_plug.c
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include "gdpgqr_lld_api.h"
#include "c8gdpgqr_plug.h"
#include "gdpgqr_ip_mapping.h"
#include "c8gdpgqr_stddefs.h"

enum gdpgqr_lld_error c8gdpgqr_config_check(const struct gdpgqr_lld_plug_s *gdpgqr_plug_conf) {

  // chunk size must be equal or smaller than memory page size
        if (gdpgqr_plug_conf->chunksize > gdpgqr_plug_conf->pagesize+1) {
                return GDPGQR_LLD_ERR_INVALID_PARAM;
        }
        return GDPGQR_LLD_NO_ERR;
}
enum gdpgqr_lld_error gdpgqr_lld_set_plug_reg_from_address(void *base_addr,
                                                           const struct gdpgqr_lld_plug_s *gdpgqr_plug_conf)

{
        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_G2_OFFSET, GDP_GQR1_GAM_GDP_GQR_PLUG_G2_P_START_MASK);

        /* Status Polling */
        do {
                yield();
        } while (!(GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_G3_OFFSET) & GDP_GQR1_GAM_GDP_GQR_PLUG_G3_IDLE_MASK));

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_G1_OFFSET,
                            GDPGQR_SET_FIELD(gdpgqr_plug_conf->pagesize,GDP_GQR1_GAM_GDP_GQR_PLUG_G1_MEMORY_PAGE_SIZE));

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_OFFSET,
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->chunksize, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_CHUNK_SIZE))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->maxopc,    GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_OPCODE_SIZE))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->pktsnb,    GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_PKTS_NB)));

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_2_OFFSET,
                            GDPGQR_SET_FIELD(gdpgqr_plug_conf->svc,   GDP_GQR1_GAM_GDP_GQR_PLUG_C1_2_SVC));

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_3_OFFSET,
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->sadd, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_3_SADD))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->eadd, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_3_EADD)));

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_OFFSET,
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->bwe_srst_sel, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_BWE_SRST_SEL))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->bwe_srst_sat, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_BWE_SRST_SAT))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->rate,         GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RATE))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->rbw,          GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RBW))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->dpr_step,     GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_DPR_STEP)));


        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_OFFSET,
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->irr_ext_panic_en, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_EXT_PANIC_EN))|
                            (GDPGQR_SET_FIELD(gdpgqr_plug_conf->irr_max_bwe,      GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_MAX_BWE)));

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_G2_OFFSET, 0);

        return c8gdpgqr_config_check(gdpgqr_plug_conf);
}

enum gdpgqr_lld_error gdpgqr_lld_get_plug_reg_from_address(void *base_addr,
                                                           struct gdpgqr_lld_plug_s *gdpgqr_plug_conf)

{
        uint32_t Val;

        Val = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_G1_OFFSET);
        gdpgqr_plug_conf->pagesize = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_G1_MEMORY_PAGE_SIZE);

        Val = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_OFFSET);
        gdpgqr_plug_conf->chunksize = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_CHUNK_SIZE);
        gdpgqr_plug_conf->maxopc    = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_OPCODE_SIZE);
        gdpgqr_plug_conf->pktsnb    = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_C1_1_PKTS_NB);

        Val = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_2_OFFSET);
        gdpgqr_plug_conf->svc   = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_C1_2_SVC);

        Val = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_C1_3_OFFSET);
        gdpgqr_plug_conf->sadd = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_C1_3_SADD);
        gdpgqr_plug_conf->eadd = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_C1_3_EADD);

        Val = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_OFFSET);
        gdpgqr_plug_conf->bwe_srst_sel = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_BWE_SRST_SEL);
        gdpgqr_plug_conf->bwe_srst_sat = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_BWE_SRST_SAT);
        gdpgqr_plug_conf->rate         = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RATE);
        gdpgqr_plug_conf->rbw          = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RBW);
        gdpgqr_plug_conf->dpr_step     = GDPGQR_GET_FIELD(Val,GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_DPR_STEP);

        Val = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_OFFSET);
        gdpgqr_plug_conf->irr_ext_panic_en = GDPGQR_GET_FIELD(Val, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_EXT_PANIC_EN);
        gdpgqr_plug_conf->irr_max_bwe      = GDPGQR_GET_FIELD(Val, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_MAX_BWE);

        return  c8gdpgqr_config_check(gdpgqr_plug_conf);
}

void c8gdpgqr_config_update_irr_params(void *base_addr,
                                const struct gdpgqr_lld_irr_params_s *irr_params)
{
        uint32_t IVC1_1 = 0;
        uint32_t IVC1_2 = 0;
        uint32_t BWE = 0;
        uint32_t RBW = 0;

        IVC1_1 = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_OFFSET);

        IVC1_1 &= ~GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RBW_MASK;

        RBW = IVC1_1;

        RBW >>= GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RBW_SHIFT;

        IVC1_2 = GDP_GQR_REG32_READ(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_OFFSET);
        IVC1_2 &= ~GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_MAX_BWE_MASK;

        BWE = IVC1_2;

        BWE >>= GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_MAX_BWE_SHIFT;

        if ((BWE == irr_params->BWE) && (RBW == irr_params->RBW )) { return; }

        IVC1_2 |= ((irr_params->BWE << GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_MAX_BWE_SHIFT)
                        & GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_IRR_MAX_BWE_MASK);

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_2_OFFSET,IVC1_2);

        IVC1_1 |= ((irr_params->RBW << GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RBW_SHIFT)
                        & GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_RBW_MASK);

        GDP_GQR_REG32_WRITE(base_addr, GDP_GQR1_GAM_GDP_GQR_PLUG_IVC1_1_OFFSET,IVC1_1);

	return;
}

