/***********************************************************************
 *
 * File: gdpgqr_lld_api.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

 /**@file gdpgqr_lld_api.h
    @brief api file for gdpgqr low level driver

    This files exposes functions (@ref lld_function), structures
    (@ref lld_struct) and enums (@ref lld_enum) of the lld API.

 */

#ifndef __GDPGQR_LLD_API__
#define __GDPGQR_LLD_API__

#include "gdpgqr_lld_platform.h" /* basic types definition per target platform */


/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/*----------------------------------------------------------------------------*/
/** @defgroup lld_define LLD API exported define */
/*----------------------------------------------------------------------------*/
#define GDPGQR_LLD_MAX_HW_NUMBER        4 /**< @brief maximum of GDPGQR hardware resources supported by LLD */
#define GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE 2 /**< @brief maximum number of hardware resources which can be used by one plane */

#define GDPGQR_NBTAP_H    (8)
#define GDPGQR_NBPHASE_H (32)
#define GDPGQR_NBTAP_V    (5)
#define GDPGQR_NBPHASE_V (32)
#define GDPGQR_NBCOEF_H ((GDPGQR_NBPHASE_H*GDPGQR_NBTAP_H)/2)
#define GDPGQR_NBCOEF_V ((GDPGQR_NBPHASE_V*GDPGQR_NBTAP_V)/2)

#define GDPGQR_MAGIC_NUMBER (0x56781234)

/*----------------------------------------------------------------------------*/
/** @defgroup lld_enum LLD API exported enums */
/*----------------------------------------------------------------------------*/

/** @brief list of all possible LUT configuration
    @ingroup lld_enum
    @todo list all possible LUT table
*/
enum gdpgqr_lut_config_e {
    GDPGQR_LUTA,
    GDPGQR_LUTB
};


/** @brief error values for lld function calls return
    @ingroup lld_enum                                 */
enum gdpgqr_lld_error {
    GDPGQR_LLD_NO_ERR = 0,                 /**< @brief No error */
    GDPGQR_LLD_ERR_INVALID_PARAM,          /**< @brief invalid parameters passed during the function call. */
    GDPGQR_LLD_ERR_NO_PROFILE,             /**< @brief profile requested is not supported or already open*/
    GDPGQR_LLD_ERROR_NO_STOP_DONE,         /**< @brief precondition gdpgqr_lld_stop has not be done before */
    GDPGQR_LLD_ERR_INIT_NOT_DONE,          /**< @brief Init has not been done when requiered */
    GDPGQR_LLD_ERR_NO_RESOURCE_AVAILABLE,  /**< @brief no hardware resource available */
    GDPGQR_LLD_ERR_UNDEFINED,              /**< @brief Other type of error */
};


/** @brief profile used for initialyzing the GDPGQR lld
    @ingroup lld_enum

    - The profile information is used at open time (#gdpgqr_lld_open) so that the lld
    can report its memory needs in terms of storage for motion buffer and
    xp70 commands as well as reserve the required HW resources.                      */
enum gdpgqr_lld_profile_e {
    GDPGQR_PROFILE_FULL_HD,   /**< 1920x1080*/
    GDPGQR_PROFILE_4KP30,     /**< 3840x2160 30 fps (requires 1 GDPGQR HW)*/
    GDPGQR_PROFILE_4KP60,     /**< 3840x2160 60 fps  (requires 2 GDPGQR HW)*/
};


/*----------------------------------------------------------------------------*/
/** @defgroup lld_struct LLD API exported structures */
/*----------------------------------------------------------------------------*/

/** @brief forward declaration for gdpgqr session hanlde */
typedef struct gdpgqr_lld_ctxt * gdpgqr_lld_hdl_t;


/** @brief forward declaration for gdpgqr configuration structure */
typedef struct gdpgqr_lld_conf_s gdpgqr_lld_conf_t;


/** @brief forward declaration for viewport structure */
typedef struct gdpgqr_lld_hw_viewport_s gdpgqr_lld_hw_viewport_t;

/** @brief forward declaration for irr params structure */
typedef struct gdpgqr_lld_irr_params_s gdpgqr_lld_irr_params_t;

/** @brief Memory range descriptor
    @ingroup lld_struct
 */
struct gdpgqr_lld_mem_desc_s
{
    int          size;       /**< @brief byte size */
    int          alignment;  /**< @brief byte alignment */
    void       * logical;    /**< @brief logical address */
    dma_addr32_t physical;   /**< @brief physical address */
};


struct gdpgqr_lld_irr_params_s
{
        uint32_t BWE; /**< @Bandwidth max error*/
        uint32_t RBW; /**< @Requested Bandwidth*/
};

/** @brief Memory needs descriptor
    @ingroup lld_struct
*/
struct gdpgqr_lld_mem_need_s
{
    int size;          /**< @brief byte size for hardware viewport linked list(s) */
    int alignment;     /**< @brief byte alignment for hardware viewport linked list(s) area */
};


/** @brief lld api viewport node structure
    @ingroup lld_struct
    @note Alignment Constraints for addresses:
    - #gdpgqr_lld_hw_viewport_s.PML: no alignment constraint
    - #gdpgqr_lld_hw_viewport_s.HFP: must be 128-bit (16-byte) aligned
    - #gdpgqr_lld_hw_viewport_s.VFP: must be 128-bit (16-byte) aligned
    - #gdpgqr_lld_hw_viewport_s.CML: ???
    @note The following HW parameters are handled by the LLD itself:
    - NVN
    - CROP (XDS & XDO) in case of 4Kp60
    @todo what is the alignment constraint for CLUT memory location?
    @todo in future evolution, BT2020 coefficient should be handled by the LLD itself.
    The driver should have only to take care of BT2020 conversion controls (enable and 10/12 bits)
*/
struct gdpgqr_lld_hw_viewport_s
{
    uint32_t         CTL;      /**< @brief control */
    uint32_t         AGC;      /**< @brief alpha gain constant */
    uint32_t         VPO;      /**< @brief viewport offset */
    uint32_t         VPS;      /**< @brief viewport stop */

    dma_addr32_t     PML;      /**< @brief pixmap memory location */
    uint32_t         PMP;      /**< @brief pixmap memory pitch */
    uint32_t         SIZE;     /**< @brief pixmap size */
    dma_addr32_t     NVN;      /**< @brief next viewport node
                                    @note  This struct member is handled internally by the LLD */

    uint32_t         KEY1;     /**< @brief color keying - lower limit */
    uint32_t         KEY2;     /**< @brief color keying - upper limit */
    dma_addr32_t     HFP;      /**< @brief horizontal filter pointer */
    uint32_t         PPT;      /**< @brief properties */

    dma_addr32_t     VFP;      /**< @brief vertical filter pointer */
    dma_addr32_t     CML;      /**< @brief CLUT memory location */
    uint32_t         CROP;     /**< @brief viewport crop
                                    @note  This struct member is handled internally by the LLD */
    uint32_t         BT0;      /**< @brief BT2020_0 register (coefficient) */

    uint32_t         BT1;      /**< @brief BT2020_1 register (coefficient) */
    uint32_t         BT2;      /**< @brief BT2020_2 register (coefficient) */
    uint32_t         BT3;      /**< @brief BT2020_3 register (coefficient) */
    uint32_t         BT4;      /**< @brief BT2020_4 register (coefficient and control) */

    uint32_t         HSRC;     /**< @brief H sample rate converter */
    uint32_t         HIP;      /**< @brief horizontal initial phase */
    uint32_t         HP1;      /**< @brief horizontal param1 */
    uint32_t         HP2;      /**< @brief horizontal param2 */

    uint32_t         VSRC;     /**< @brief V sample rate converter */
    uint32_t         VIP;      /**< @brief vertical initial phase */
    uint32_t         VP1;      /**< @brief vertical param1 */
    uint32_t         VP2;      /**< @brief vertical param2 */

    uint32_t         HFCAY[GDPGQR_NBCOEF_H];    /**< @brief HF Alpha/Luma Coefficient */
    uint32_t         HFCUV[GDPGQR_NBCOEF_H];    /**< @brief HF Chroma Coefficient */
    uint32_t         VFCAY[GDPGQR_NBCOEF_V];    /**< @brief VF Alpha/Luma Coefficient */
    uint32_t         VFCUV[GDPGQR_NBCOEF_V];    /**< @brief VF Chroma Coefficient */
};

/** @brief image size definition
    @ingroup lld_struct
**/
struct gdpgqr_lld_size_s
{
   uint16_t width;    /**< @brief width in pixel */
   uint16_t height;    /**< @brief height in pixel */
};


/** @brief LUT configuration to use
    @ingroup lld_struct
**/
struct gdpgqr_lut_table_s
{
    enum gdpgqr_lut_config_e v_luma_or_alpha;   /**< @brief vertical LUT config to apply on luma or alpha */
    enum gdpgqr_lut_config_e v_chroma;          /**< @brief vertical LUT config to apply chroma */
    enum gdpgqr_lut_config_e h_luma_or_alpha;   /**< @brief horizontal LUT config to apply on luma or alpha */
    enum gdpgqr_lut_config_e h_chroma;          /**< @brief horizontal LUT config to apply on chroma */
};


/** @brief lld api configuration structure
    @ingroup lld_struct
    @note This is the viewport linked list from the driver (software) point of view.
    The LLD is responsible for handling the viewport linked list for the hardware (NVN register)
*/
struct gdpgqr_lld_conf_s
{
    gdpgqr_lld_conf_t       * next;        /**< @brief next node in software linked list (NULL in last item) */
    gdpgqr_lld_hw_viewport_t  viewport;    /**< @brief hardware viewport node */
};

/** @brief lld api output parameters structure
    @ingroup lld_struct
    @note This is the output configuration used to select one or two gdp mode
*/
struct gdpgqr_lld_output_param_s
{
    int output_vtg_fps;  /**< @brief vtg fps in mHz (example : p30 =>29976mHz) */
    uint16_t width;      /**< @brief output width  */
    uint16_t height;     /**< @brief output height */
};

/** @brief lld api init configuration structure
    @ingroup lld_struct
*/
struct gdpgqr_lld_init_s
{
    const void   * base_addr;      /**< @brief base address of an GDPGQR HW instance */
    uint32_t       mixer_bit;      /**< @brief bit of the GAM_MIXERn_CTL associated to the GDP */
    void         * private_data;   /**< @brief init opaque parameter used for lld validation/testing. */
};


/** @brief structure describing GDPGQR IP-Plug Setup
    @ingroup lld_struct
   - note : see ipplug documentation for full descriptions of these parameters
*/
struct gdpgqr_lld_plug_s
{
  uint8_t pagesize;         /**< @brief memory page size = 2(^6+n) */
  uint8_t chunksize;        /**< @brief chunk size = 2(^5+n) must be lesser or equal to memory page size*/
  uint8_t maxopc;           /**< @brief max opcode = 2^(3+n) */
  uint8_t pktsnb;           /**< @brief max number of outstanding packets, (0=disable) */
  uint8_t svc;              /**< @brief QoS channel Id */
  uint16_t sadd;            /**< @brief Start Address in local mem or IpPlug for Client 1 */
  uint16_t eadd;            /**< @brief End Address in local mem or IpPlug for Client 1 */
  uint8_t bwe_srst_sel;     /**< @brief select source of Bandwidth gauge reset */
  uint8_t bwe_srst_sat;     /**< @brief saturation value of Bandwidth error */
  uint8_t rate;             /**< @brief sample rate of hardware bandwidth measerement */
  uint8_t rbw;              /**< @brief required bandwidth as a fractionnal part of stbus bandwidth */
  uint8_t dpr_step;         /**< @brief select dpri dynamic (0=disable) */
  uint8_t irr_ext_panic_en; /**< @brief enable the external to make the plug enter panic mode */
  uint8_t irr_max_bwe;      /**< @brief internal panic source threshold */
};


/** @brief #gdpgqr_lld_set_conf output parameters
    @ingroup lld_struct
*/
struct gdpgqr_lld_mixer_setup_s
{
    bool     is_active;     /**< @brief state if the HW GDPGQR instance is used (MIXER configuration) */
    uint32_t mixer_bit;     /**< @brief #gdpgqr_lld_init_s.mixer_bit of the active/ inactive gdpgqr hw */
};

/** @brief helper function to set the crb value according to its mixerbit
    @ingroup  lld_helper_function
*/
uint8_t gdpgqr_lld_mixerbit2crb(uint32_t mixerbit);

/*----------------------------------------------------------------------------*/
/** @defgroup lld_helper_function LLD API exported functions */
/*----------------------------------------------------------------------------*/

/** @brief helper function to set the gqr ARGB LUTs in the gdpgqr_lld configuration structure
    with respect to provided strength
    @ingroup  lld_helper_function
    @post gdpgqr_lld_set_conf
    @todo What (input) parameters are actually needed, shouldn't we define a specific structure?
    @todo What parameters are actually returned, shouldn't we define a specific structure?
    @todo If we don't want to change the profile of this function, then we should explain/document why.
    @todo Define helper gdpgqr_lld_set_lut
*/
void gdpgqr_lld_set_lut (
    enum gdpgqr_lut_config_e    lut_config,         /**< [in]     : lut config needed    */
    uint32_t                    size,               /**< [out]    : size in byte of LUT (only for checks) */
    uint32_t                    gdpgqr_lld_lut[],   /**< [in,out] : memory pointer to contiguous non cachable physical memory  */
    uint8_t                     *max_coeff          /**< [out] : normalisation coefficient */
);


/** @brief helper function to set increment according to zoom
    with respect to provided strength
    @ingroup  lld_helper_function
    @post update in link_list_node HFP and VFP to use LUT table according to
    lut_conf output
    @post gdpgqr_lld_set_conf
    @todo What (input) parameters are actually needed, shouldn't we define a specific structure?
    @todo What parameters are actually returned, shouldn't we define a specific structure?
    @todo If we don't want to change the profile of this function, then we should explain/document why.
    @todo Do we need to create a dedicated structure for all input?
*/
void gdpgqr_lld_set_zoom_param(
   const struct gdpgqr_lld_size_s  *input_size,       /**< [in] : define input picure size */
   const struct gdpgqr_lld_size_s  *output_size,      /**< [in] : define output size */
   const enum gdpgqr_lut_config_e  *strength,         /**< [in] : alpha  vertical strength    */
   struct gdpgqr_lld_hw_viewport_s *link_list_node,   /**< [out]: hw command to update */
   struct gdpgqr_lut_table_s lut_conf                 /**< [out]: define which LUT to use for each component */
);


/*----------------------------------------------------------------------------*/
/** @defgroup lld_function LLD API exported functions */
/*----------------------------------------------------------------------------*/

/** @brief report 3 digit API version.
    @ingroup  lld_function

    @retval API version
    @pre no pre condition
    @note this function is a provision.
*/
uint32_t gdpgqr_lld_get_api_version ( void );


/** @brief initialyze the gdpgqr lld ...
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INIT_NOT_DONE if no memory.

    @warning This function must be called only once. Depending on the SOC HW and
    parameters passed, 2 GDPGQR logical instances supporting up to 4Kp30 or one supporting up to
    4Kp60 will be available.
    @post #gdpgqr_lld_get_memory_needs call can be made.
    @todo Clarify what means :
            Depending on the SOC HW and
            parameters passed, 2 GDPGQR logical instances supporting up to 4Kp30 or one supporting up to
            4Kp60 will be available.
*/
enum gdpgqr_lld_error gdpgqr_lld_init (
    int                            hw_nb,         /**< [in] : number of GDPGQR HW instances on the SOC. */
    const struct gdpgqr_lld_init_s init_param[]   /**< [in] : initialization struct. */
);

/** @brief Set Cpu Endianness, only for backward compatibility of BigEndian Cpu support
    Default is littleEndian like most cpus.

    @ingroup  lld_function
    @pre none
    @post #gdpgqr_lld_init
*/
void gdpgqr_lld_set_cpu_endianness(
                                   bool bignotlittle /**< [in] : Cpu Endianness > */
);

/** @brief get memory needs for a GDPGQR plane (whether it uses 1 or 2 GDPGQR HW Ips) according to profile
    @ingroup  lld_function
    @pre none
    @post #gdpgqr_lld_open

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM if memory_needs is null.
    @retval GDPGQR_LLD_ERR_NO_PROFILE in case profile requested is not supported or
    not enough free HW resources to enable it.
*/
enum gdpgqr_lld_error gdpgqr_lld_get_memory_needs (
    enum gdpgqr_lld_profile_e       profile,        /**< [in] : worst case (fps/resolution) intented to be used with the session. */
    uint32_t                        max_node,       /**< [in] : maximum number of node in linked list */
    struct gdpgqr_lld_mem_need_s  * memory_needs    /**< [in] : memory required for hardware viewport linked list */
);


/** @brief Set Ip Plug param for gdpgqr plugs related to the given handle.
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in plug parameters

    @pre #gdpgqr_lld_open  has been called
*/

enum gdpgqr_lld_error gdpgqr_lld_set_plug_reg_from_handle (
    gdpgqr_lld_hdl_t                 hdl,       /**< [in] : pointer to handle identifying the gdpgqr. */
    const struct gdpgqr_lld_plug_s * stbp_setup /**< [in] : pointer to setup configuration for the IP plug of a given GDPGQR HW IP.
                                                 - Only one configuration is passed by the user whether the lld use one or 2 HW instances as same IP plug*/
);

/** @brief Get Ip Plug param for gdpgqr plugs related to the given handle.
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in plug parameters

    @pre #gdpgqr_lld_open  has been called
*/

enum gdpgqr_lld_error gdpgqr_lld_get_plug_reg_from_handle (
     gdpgqr_lld_hdl_t           hdl,       /**< [in] : pointer to handle identifying the gdpgqr. */
     struct gdpgqr_lld_plug_s * stbp_setup /**< [out]  : pointer to setup configuration for the IP plug of a given GDPGQR HW IP.
                                            - One or Two configuration are returned whether the lld use one or 2 HW instances as same IP plug
                                              make sure that stdp_setup points to 2 allocated struct if necessary */
);

/** @brief Set Ip Plug param for gdpgqr plugs related to the given handle.
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in plug parameters
*/

enum gdpgqr_lld_error gdpgqr_lld_set_plug_reg_from_address (
    void *                           base_address, /**< [in] : gdpgqr base address. */
    const struct gdpgqr_lld_plug_s * stbp_setup    /**< [in] : pointer to setup configuration for the IP plug of a given GDPGQR HW IP.
                                                 - Only one configuration is passed by the user whether the lld use one or 2 HW instances as same IP plug*/
);

/** @brief Get Ip Plug param for gdpgqr plugs related to the given handle.
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in plug parameters
*/

enum gdpgqr_lld_error gdpgqr_lld_get_plug_reg_from_address (
     void *                     base_address, /**< [in] : gdpgqr base address. */
     struct gdpgqr_lld_plug_s * stbp_setup    /**< [out]  : pointer to setup configuration for the IP plug of a given GDPGQR HW IP.
                                            - One or Two configuration are returned whether the lld use one or 2 HW instances as same IP plug
                                              make sure that stdp_setup points to 2 allocated struct if necessary */
);



/** @brief Initialize a graphical plane in LLD, configure GDPGQR STBus R&W Plugs
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in mem_for_cmd is null.
    @retval GDPGQR_LLD_ERR_NO_PROFILE in case profile requested is not supported or
    not enough free HW resources to enable it.

    @pre #gdpgqr_lld_init, #gdpgqr_lld_get_memory_needs  has been called
    @warning mem_for_cmd must be contiguous non cachable physical memory.
    @note
        - *hw_used_bit_mask is used to statically configure the dataflow from GDPGQR HW
    to mixers.
        - *mixer_bit_mask (from #gdpgqr_lld_set_conf) is used to know which plane to
        enable at MIXER input for each lld frame operation as lld implementation may
        use only one GDPGQR HW instance when VTG is lower than 4k60 even if 2 HW instances
        have been reserved at open time.
*/
enum gdpgqr_lld_error gdpgqr_lld_open (
    enum gdpgqr_lld_profile_e              profile,          /**< [in]  : worst case (fps/resolution) intended to be used with this session.     */
    const struct gdpgqr_lld_mem_desc_s   * mem_for_cmd,      /**< [in]  : pointer to descriptor of memory allocated by caller to store XP70 cmd. */
    uint32_t                             * hw_used_bit_mask, /**< [out] : pointer to ORed bit mask for the maximum number of GDPGQR HW IPs used for the requested profile.
                                                                   - This is the ORed result of the #gdpgqr_lld_init_s.mixer_bit of the GDPGQR HW instance selected
                                                                     by the LLD. This information is used statically by the driver to program the plane routing
                                                                     to the MIXER (aka cross bar). */
    gdpgqr_lld_hdl_t                     * hdl_p             /**< [out] : pointer to handle identifying the gdpgqr. NULL in case of failure   */
);


/** @brief configure the GDPGQR for the next Vsync operation
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @note depending on the init performed (#gdpgqr_lld_init) the command may be
    split on 2 or 1 GDPGQR HW IPs.
    @note lld handles on it owns the bufferization of the cmd into pending and
    active.
    @note upon call completion caller is free to modify *cmd
    @note user of lld must allocate struct mixer_setup[GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE]
*/
enum gdpgqr_lld_error gdpgqr_lld_set_conf (
    gdpgqr_lld_hdl_t                 hdl,                  /**< [in] : handle of the session. */
    const struct gdpgqr_lld_output_param_s * output_param, /**< [in] : output parameters. */
    const struct gdpgqr_lld_conf_s * conf,                 /**< [in] : pointer to configuration processing command. */
    int                            * setup_nb,             /**< [out]: pointer to number of mixer video plug to configure
                                                          - For 4K60 profile setup_nb is always 2 as lld needs to state for each gdpgqr_lld_set_conf call
                                                          which GDPGQR HW id is used so that mixer is reprogrammed accordingly. */
    struct gdpgqr_lld_mixer_setup_s  mixer_setup [GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE]     /**< [out]: hw setup mixer to be applied outside of the lld scope. */
);

/** @brief configure Issue Rate Regulation params for GDP
    @ingroup lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @note depending on the init performed (#gdpgqr_lld_init) setting for 1 or 2 GDPGQR HW IPs.
    @note Setting of Irr is immediate (R/W registers) driver should make sure to
    @node properly synchronize Irr params value with currently executed node.
*/
enum gdpgqr_lld_error gdpgqr_lld_update_irr_params (
    gdpgqr_lld_hdl_t hdl, /**< [in] : handle of the session. */
    const struct gdpgqr_lld_irr_params_s * irr_params /**< [in] : pointer to irr_params. */
);


/** @brief Stop the GDPGQR processing
    @ingroup  lld_function
    @retval GDPGQR_LLD_NO_ERR in case hdl is not valid
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @note this call stop GDP data bus requests
    @todo HW/SW check In which cases we actually need to stop.
    @todo define if we would like to return mixer_setup as it is done in #gdpgqr_lld_set_conf
*/
enum gdpgqr_lld_error gdpgqr_lld_stop ( gdpgqr_lld_hdl_t hdl     /**< [in] : handle of the session. */);

/** @brief close an gdpgqr session.
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success,
    @retval GDPGQR_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @pre #gdpgqr_lld_stop has been stopped.
    @post a new #gdpgqr_lld_open call can be done
*/
enum gdpgqr_lld_error gdpgqr_lld_close ( gdpgqr_lld_hdl_t hdl     /**< [in] : handle of the session. */);


/** @brief delete gdpgqr_lld
    @ingroup  lld_function

    @retval GDPGQR_LLD_NO_ERR on success
    @note this function cannot fail. It deallocates any logical memory
    allocated by the lld at #gdpgqr_lld_init time. All IP clocks that are
    under lld control are stopped.
    @note this function is typically called when gdpgqr driver is unloaded.
*/
enum gdpgqr_lld_error gdpgqr_lld_terminate ( void );


/* C++ support */
#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GDPGQR_LLD_API__ */

/* End of C stuff, following stuff is purely for documentation of the API. */

/**

@mainpage Orly3/Cannes 2.5 GDPGQR Lite LLD API

@tableofcontents



References {#S1}
================

| ref                     | author                           | title                                                      | Link                          |
|:------------------------|:---------------------------------|:-----------------------------------------------------------|-------------------------------|
|<a id="DAJL1">DAJL1</a>  | Didier Aladenise / Jeremy Lepecq | COMPOSITOR Functional Specification V3 (v2.1.1 Orly3)      |[c8compo3_orly3.pdf](http://gnx5572.gnb.st.com:8080/hdmsdb/C8COMPO3/c8compo3_orly3_lib/funcspec_doc/v5.2/c8compo3_orly3.pdf)|
|<a id="DAJL2">DAJL2</a>  | Didier Aladenise / Jeremy Lepecq | COMPOSITOR Functional Specification V3 (v3.0.0 Cannes2.5)  |[c8compo3_cannes2_5.pdf](http://gnx5572.gnb.st.com:8080/hdmsdb/C8COMPO3/c8compo3_cannes2_5_lib/funcspec_doc/v1.0/c8compo3_cannes2_5.pdf)|

Assumptions {#S2}
===========

Part of this API is relying on the fact that all mixers in Compo are using the same bit mapping for their inputs.
For example, in Orly3, VID1_DISPLAYENABLE is bit 1 of GAM_MIXER<n>_CTL (for n in 0..3)

Open points & Todos {#S3}
===================
[todo](./todo.html)


Types {#S4}
=====
@ref lld_enum

@ref lld_struct

Functions {#S5}
=========
@ref lld_function

Macros {#S6}
======

Macro are used to remap HW access/ libC calls to the actual target implementation.

@ref macro_hw

@ref macro_mem

Usage {#S7}
=====
The bellow sequence diagram show 2 intended usage of the LLD API.
- One "4KP60 STB" intended usage where both GDPGQR HW resources are seen as one 4KP60 capable plane.
- One "dual 4KP30 IVI " intended usage where both GDPGQR HW resources are used as 2 independant planes

4KP60 usage {#S71}
-----------

![GDPGQR usage for 4K STB ](gdpgqr_lld_4KP60.png)

2x 4KP30 usage {#S72}
--------------
![GDPGQR usage for IVI ](gdpgqr_lld_2X4KP30.png)

Example of motion buffer sizing for dual GDPGQR mode {#S73}
--------------
@code

#define OVERLAP_WIDTH 100
#define MOTION_BUFFER_WIDTH (1920+2*OVERLAP_WIDTH)
#define MOTION_BUFFER_HEIGHT (544)

@endcode

Revisions {#S8}
=========

@version 0.1.2 2014/08/25
             - added question about alpha plane split

@version 0.1.1 2014/08/22
    - gdpgqr_lld_init
       - update return value in description
    - gdpgqr_lld_get_memory_needs
       - add missing coma
       - memory_needs is an output and not an input
    - gdpgqr_lld_open
       - no firmware is present in gdp_gqr
       - update return param
    - propose an initial definition for helper for coeff
    - gdpgqr_lld_stop
       - no null command concept in GDP
    - rename compo_setup to mixer_setup
    - fix typo in  gdpgqr_lld_conf_s
    - start proposition on helper for programming GQR

@version 0.1.0 2014/08/19 First shared version

*/
