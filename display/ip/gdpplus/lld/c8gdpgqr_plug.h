/***********************************************************************
 *
 * File: c8gdpgqr_plug.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/


 /**@file c8gdpgqr_plug.h
    @brief macro definition files to write/read GDPGQR Plugs registers
 */

#ifndef __C8GDPGQR_PLUG_H_
#define __C8GDPGQR_PLUG_H_

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** @brief memory page size = 2(^6+n)
    @ingroup lld_enum
*/
enum  gdpgqr_lld_plug_pagesize_e {
  GDPGQR_MAXSIZE_64   = 0,
  GDPGQR_MAXSIZE_128  = 1,
  GDPGQR_MAXSIZE_256  = 2,
  GDPGQR_MAXSIZE_512  = 3,
  GDPGQR_MAXSIZE_1024 = 4,
  GDPGQR_MAXSIZE_2048 = 5,
  GDPGQR_MAXSIZE_4096 = 6,
  GDPGQR_MAXSIZE_8192 = 7,
};

/** @brief chunk size = 2(^5+n)
    @ingroup lld_enum
*/
enum  gdpgqr_lld_plug_chunksize_e {
  GDPGQR_CHUNKSIZE_32  = 0,
  GDPGQR_CHUNKSIZE_64  = 1,
  GDPGQR_CHUNKSIZE_128 = 2,
  GDPGQR_CHUNKSIZE_256 = 3
};


/** @brief max opcode = 2^(3+n)
    @ingroup lld_enum
*/
enum  gdpgqr_lld_plug_maxopc_e {
  GDPGQR_MAXOPC_8  = 0,
  GDPGQR_MAXOPC_16 = 1,
  GDPGQR_MAXOPC_32 = 2,
  GDPGQR_MAXOPC_64 = 3
};

/*!
 * @brief Update Issue rate regulation params
 *
 * @param[in] base_addr base address of GDPGQR
 * @param[in]
 */
void c8gdpgqr_config_update_irr_params(void *base_addr,
          const struct gdpgqr_lld_irr_params_s *irr_params);

/* C++ support */
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __C8GDPGQR_PLUG_H_ */
