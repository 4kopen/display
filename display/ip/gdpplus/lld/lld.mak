# Classes required for SoCs containing a GDP Plus with LLD display
ifneq ($(CONFIG_GDP_PLUS),)

EXTRA_CFLAGS += -DGDPGQR_API_FOR_STAPI
ifneq ($(CONFIG_STM_VIRTUAL_PLATFORM),)
EXTRA_CFLAGS += -DVIRTUAL_PLATFORM_TLM
endif

STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/gdpplus/lld/,  \
                        gdpgqr_lld_api.c                             \
                        c8gdpgqr_plug.c                              \
                        gdpgqr_split.c                               \
                        c8gdpgqr_hw_lld.c                            \
                        gdpgqr_split_algo.c)

endif




