/***********************************************************************
 *
 * File: gdpgqr_split.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

 /**@file gdpgqr_split.h
    @brief function definition files to enable splitting configurations
    for dual GDPGQR usage.

    @warning any call to the libc function shall be done through a macro so that
    call can be redirected when driver is from inside a linux kernel context.
 */

#ifndef __GDPGQR_SPLIT_H__
#define __GDPGQR_SPLIT_H__

#include "gdpgqr_handle.h"
#include "c8compo4_define.h"
#include "gdpgqr_split_algo.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/** @brief generate 2 GDPGQR configurations from one original config
    @ingroup  lld_internal_function
    @param[in] hdl : handle to session
    @param[in] expected_conf : ideal configuration
    @param[out] mixer_setup[] : mixer setup to be applied
    @param[in] hw_to_use_cnt : number of gdp to use
*/
void gdpgqr_write_config(gdpgqr_lld_hdl_t hdl,
                         const struct gdpgqr_lld_conf_s *expected_conf,
                         struct gdpgqr_lld_mixer_setup_s mixer_setup[],
                         uint8_t hw_to_use_cnt
                         );

/** @brief
*/
void gdpgqr_prepare_split(const struct cmd_origi_param *full_param,
                          struct cmd_split_param *side_param,
                          uint32_t ctl);


/******************************************************************************/
/******************************************************************************/
/* C++ support */
#ifdef __cplusplus
}
#endif

#endif /* __GDPGQR_SPLIT_H__ */
