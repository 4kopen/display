#include "c8compo3_generic.h"

/* keep compatibility with HQVDP test written by sam */
#define c8compo3_generic_MIXER1_GAM_MIXER1_CTL_OFFSET MIXER1_GAM_MIXER_CTL_OFFSET
/* end */

#define GAM_MIX_CTL (MIXER0_GAM_MIXER0_CTL_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_MISC (MIXER0_GAM_MIXER0_MISC_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_CRB (MIXER0_GAM_MIXER0_CRB_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_CRB2 (MIXER0_GAM_MIXER0_CRB2_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_ACT (MIXER0_GAM_MIXER0_ACT_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_BKC (MIXER0_GAM_MIXER0_BKC_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_BCO (MIXER0_GAM_MIXER0_BCO_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_BCS (MIXER0_GAM_MIXER0_BCS_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_AVO (MIXER0_GAM_MIXER0_AVO_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_AVS (MIXER0_GAM_MIXER0_AVS_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_MBP (MIXER0_GAM_MIXER0_MBP_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)

#define GAM_MIX_MISR_CTL (MIXER1_GAM_MIXER_MISR_CTL_OFFSET - c8compo3_generic_MIXER1_BASE_ADDR)
#define GAM_MIX_MISR_AVO (MIXER1_GAM_MIXER_MISR_AVO_OFFSET - c8compo3_generic_MIXER1_BASE_ADDR)
#define GAM_MIX_MISR_AVS (MIXER1_GAM_MIXER_MISR_AVS_OFFSET - c8compo3_generic_MIXER1_BASE_ADDR)

#define GAM_MIX_SIGN1 (MIXER0_GAM_MIXER0_SIGN1_OFFSET - c8compo3_generic_MIXER1_BASE_ADDR)
#define GAM_MIX_SIGN2 (MIXER0_GAM_MIXER0_SIGN2_OFFSET - c8compo3_generic_MIXER1_BASE_ADDR)
#define GAM_MIX_SIGN3 (MIXER0_GAM_MIXER0_SIGN3_OFFSET - c8compo3_generic_MIXER1_BASE_ADDR)
#define GAM_MIX_MISR_SIGN1 GAM_MIX_SIGN1
#define GAM_MIX_MISR_SIGN2 GAM_MIX_SIGN2
#define GAM_MIX_MISR_SIGN3 GAM_MIX_SIGN3

#define GAM_VID_CTL  (VIDEO1_GAM_VIDEO_CTL_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_ALP  (VIDEO1_GAM_VIDEO_ALP_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_CLF  (VIDEO1_GAM_VIDEO_CLF_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_VPO  (VIDEO1_GAM_VIDEO_VPO_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_VPS  (VIDEO1_GAM_VIDEO_VPS_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_KEY1 (VIDEO1_GAM_VIDEO_KEY1_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_KEY2 (VIDEO1_GAM_VIDEO_KEY2_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_MPR0 (VIDEO1_GAM_VIDEO_MPR0_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_MPR1 (VIDEO1_GAM_VIDEO_MPR1_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_MPR2 (VIDEO1_GAM_VIDEO_MPR2_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_MPR3 (VIDEO1_GAM_VIDEO_MPR3_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_CROP (VIDEO1_GAM_VIDEO_CROP_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_BC   (VIDEO1_GAM_VIDEO_BC_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_TINT (VIDEO1_GAM_VIDEO_TINT_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)
#define GAM_VID_CSAT (VIDEO1_GAM_VIDEO_CSAT_OFFSET - c8compo3_generic_VIDEO1_BASE_ADDR)

#define GAM_GDP_NVN      (GDP5_GAM_GDP_NVN_OFFSET - c8compo3_generic_GDP5_BASE_ADDR)
#define GAM_GDP_GQR_NVN  (GDP_GQR1_GAM_GDP_GQR_NVN_OFFSET - c8compo3_generic_GDP_GQR1_BASE_ADDR)
#define GAM_ALP_NVN      (ALP1_GAM_ALP_NVN_OFFSET - c8compo3_generic_ALP1_BASE_ADDR)
#define GAM_GDP_PAS      (GDP5_GAM_GDP_PAS_OFFSET - c8compo3_generic_GDP5_BASE_ADDR)
#define GAM_GDP_GQR_PAS  (GDP_GQR1_GAM_GDP_GQR_PAS_OFFSET - c8compo3_generic_GDP_GQR1_BASE_ADDR)
#define GAM_ALP_PAS      (ALP1_GAM_ALP_PAS_OFFSET - c8compo3_generic_ALP1_BASE_ADDR)

#define GAM_CAP_CTL    (CAP1_GAM_CAP_CTL_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_CWO    (CAP1_GAM_CAP_CWO_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_CWS    (CAP1_GAM_CAP_CWS_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VTP    (CAP1_GAM_CAP_VTP_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VBP    (CAP1_GAM_CAP_VBP_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_PMP    (CAP1_GAM_CAP_PMP_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_CMW    (CAP1_GAM_CAP_CMW_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HSRC   (CAP1_GAM_CAP_HSRC_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VSRC   (CAP1_GAM_CAP_VSRC_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC0   (CAP1_GAM_CAP_HFC0_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC1   (CAP1_GAM_CAP_HFC1_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC2   (CAP1_GAM_CAP_HFC2_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC3   (CAP1_GAM_CAP_HFC3_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC4   (CAP1_GAM_CAP_HFC4_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC5   (CAP1_GAM_CAP_HFC5_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC6   (CAP1_GAM_CAP_HFC6_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC7   (CAP1_GAM_CAP_HFC7_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC8   (CAP1_GAM_CAP_HFC8_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_HFC9   (CAP1_GAM_CAP_HFC9_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VFC0   (CAP1_GAM_CAP_VFC0_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VFC1   (CAP1_GAM_CAP_VFC1_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VFC2   (CAP1_GAM_CAP_VFC2_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VFC3   (CAP1_GAM_CAP_VFC3_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VFC4   (CAP1_GAM_CAP_VFC4_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_VFC5   (CAP1_GAM_CAP_VFC5_OFFSET - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX0    (CAP1_GAM_CAP_MX0_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX1    (CAP1_GAM_CAP_MX1_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX2    (CAP1_GAM_CAP_MX2_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX3    (CAP1_GAM_CAP_MX3_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX4    (CAP1_GAM_CAP_MX4_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX5    (CAP1_GAM_CAP_MX5_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX6    (CAP1_GAM_CAP_MX5_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)
#define GAM_CAP_MX7    (CAP1_GAM_CAP_MX7_OFFSET  - c8compo3_generic_CAP1_BASE_ADDR)


// Cannes2.5_cut specif part
////////////////////////////////////////////////////////////////////////////////

// copy by hand
#define MIXER0_GAM_MIXER0_CTL_TEE_OFFSET                            (c8compo3_generic_MIXER0_BASE_ADDR + 0x50)
#define MIXER0_GAM_MIXER0_CRB2_TEE_OFFSET                           (c8compo3_generic_MIXER0_BASE_ADDR + 0x80)
#define MIXER0_GAM_MIXER0_CRB_TEE_OFFSET                            (c8compo3_generic_MIXER0_BASE_ADDR + 0x84)
// end pick by hand

#define GAM_MIX_CTL_TEE (MIXER0_GAM_MIXER0_CTL_TEE_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_CRB_TEE (MIXER0_GAM_MIXER0_CRB_TEE_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)
#define GAM_MIX_CRB2_TEE (MIXER0_GAM_MIXER0_CRB2_TEE_OFFSET - c8compo3_generic_MIXER0_BASE_ADDR)


#ifdef c8compo3_generic_GDP_LIT1_BASE_ADDR
  #define  c8compo3_generic_GDP1_BASE_ADDR c8compo3_generic_GDP_LIT1_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_LIT2_BASE_ADDR
  #define  c8compo3_generic_GDP2_BASE_ADDR c8compo3_generic_GDP_LIT2_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_LIT3_BASE_ADDR
  #define  c8compo3_generic_GDP3_BASE_ADDR c8compo3_generic_GDP_LIT3_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_LIT4_BASE_ADDR
  #define  c8compo3_generic_GDP4_BASE_ADDR c8compo3_generic_GDP_LIT4_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_LIT5_BASE_ADDR
  #define  c8compo3_generic_GDP5_BASE_ADDR c8compo3_generic_GDP_LIT5_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_LIT6_BASE_ADDR
  #define  c8compo3_generic_GDP6_BASE_ADDR c8compo3_generic_GDP_LIT6_BASE_ADDR
#endif

#ifdef c8compo3_generic_GDP_GQR1_BASE_ADDR
  #define  c8compo3_generic_GDP1_BASE_ADDR c8compo3_generic_GDP_GQR1_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_GQR2_BASE_ADDR
  #define  c8compo3_generic_GDP2_BASE_ADDR c8compo3_generic_GDP_GQR2_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_GQR3_BASE_ADDR
  #define  c8compo3_generic_GDP3_BASE_ADDR c8compo3_generic_GDP_GQR3_BASE_ADDR
#endif
#ifdef c8compo3_generic_GDP_GQR4_BASE_ADDR
  #define  c8compo3_generic_GDP4_BASE_ADDR c8compo3_generic_GDP_GQR4_BASE_ADDR
#endif
