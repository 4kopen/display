/***********************************************************************
*
* Copyright (c) 2014 STMicroelectronics Limited.
*
* This file is subject to the terms and conditions of the GNU General Public
* License version 2.  See the file COPYING in the main directory of this archive for
* more details.
*
************************************************************************/


 /**@file gdpgqr_lld_platform.h
    @brief macro definition files to enable execution on different targets

    @warning any call to the libc function shall be done through a macro so that
    call can be redirected when driver is from inside a linux kernel context.
 */

#ifndef __LLD_PLATFORM_H__
#define __LLD_PLATFORM_H__

/*----------------------------------------------------------------------------*/
#ifdef VERIFICATION_PLATFORM /**< @todo: change for actual define from Yvan's team*/
/*----------------------------------------------------------------------------*/

#include <stdint.h>     /**< @brief definition of  uint32_t and so on */
#include <stdbool.h>    /**< @brief definition of  bool */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

/* get definition */
#include "global_verif_hal_stddefs.h"
#include "vibe_os.h"

/** @todo Add all verification platform build specific includes here */

/** @defgroup macro_hw verification platform HW registers write macros
    @warning all base address parameters must be of type uint32_t*
    which means that there is a byte address increase of 4 bytes for each
    offset increment.
    @{
*/

/** @brief write a 32 bits register
    @param[in] base : base address of the IP
    @param[in] offset: offset of the register from the base address
    @param[in] val: 32 bits value to write.
*/

#define GDP_GQR_REG32_WRITE(base, offset, val) vibe_os_write_register((volatile uint32_t*)(base), (offset), (val))
#define GDP_GQR_REG32_READ(base, offset) (uint32_t)(vibe_os_read_register((volatile uint32_t*)(base), (offset)))

/** @} register access macros */

/** @defgroup macro_mem macro related to memory management
    @{  */

/** @brief write memory barrier
    @note most probably not needed on verification platform as long as code does
    not use an ARM cycle accurate emulator.
 */
#define GDP_GQR_WRITE_MB()

/** @brief memory allocator for sw related content only memory cannot be shared with HW
    @param[in] size in sizeof units
    @retval void* pointer on allocated memory.
    @note most probably a simple malloc on verification platform
 */

#define GDP_GQR_ALLOCATE_LLD_MEM(size) malloc(size)


/** @brief memory deallocator for sw related content only
    @param[in] mem: pointer returned by ALLOCATE_LLD_MEM
    @note most probably a simple free on verification platform
 */
#define GDP_GQR_FREE_LLD_MEM(mem) free(mem)


/** @brief memcpy macro
    @param[in] d: (void*) pointer on destination address
    @param[in] s: (void*) pointer on source address
    @param[in] n: memory transfer size is size_t units

    @note This is a macro. It might mapped on standard memcpy or not
 */

#define GDP_GQR_MEMCPY(d,s,n) memcpy(d,s,n)


/** @brief memset macro
    @param[in] d: (void*) pointer on destination address
    @param[in] n: memory transfer size is size_t units

    @note This is a macro. It might mapped on standard memcpy or not
*/
#define GDP_GQR_MEMSET(d,n) memset(d,0,n)


/** @} macro_mem */

/** @defgroup macro_assert macro related to condition checking
    @{  */

/** @brief assert macro
 */
#define GDP_GQR_ASSERT(cond) assert(cond)

/** @} macro_assert */

/** @warning any libc call must be discussed and accessed through a macro as libc is not
accessible from code executing in linux kernel */


/** @defgroup trace_func trace generation macros.
    @note to be compliant with the embedded code tracing framework the following
    function must be used for tracing (if needed). A dedicated trace ID is allocated
    to gdp lld implementation: id = TRC_ID_GDPGQR_LLD

    @{
*/


enum trc_id_gdpgqr {            /* [family -] trace description */
    TRC_ID_GDPGQR_LLD = 1,      /*            GDPGQR LLD     */
    TRC_ID_GDPGQR_ERROR         /* dummy Error value */
};


/** @brief generic trace macro
    @note do not use directly, use #TRCIN, #TRCOUT or #TRC instead
*/

#if 0
#define TRCGEN(id,c,...)                                                \
    do                                                                  \
    {                                                                   \
        int check_id = id; /* ensures that id is defined */             \
        check_id = check_id; /* prevents 'unused variable' warning */   \
        printf(ANSI_COLOR_GREEN "[GDPGQR_LLD] %c %s " ANSI_COLOR_RESET, c, __PRETTY_FUNCTION__);  \
        printf("" __VA_ARGS__);                                         \
    } while(0)


/** @brief trace macro to log entry in a function */
#define TRCIN(id,args...)   TRCGEN(id, '>', ##args)

/** @brief trace macro to log exit of a function */
#define TRCOUT(id,args...)  TRCGEN(id, '<', ##args)

/** @brief trace macro to log anything but exit/entry. */
#define TRC(id,args...)     TRCGEN(id, '.', ##args)
#endif

/** @} trace_func */


/*----------------------------------------------------------------------------*/
#else /*  Linux Kernel build with VibeOs*/
/*----------------------------------------------------------------------------*/


/* Type definition */
#ifndef __cplusplus
/* do not redefine bool if we are compiling in c++ */
typedef unsigned char  bool;
#endif /* n __cplusplus */
typedef int            int32_t;
typedef short          int16_t;

/* define int8_t and prevent redefinition by types.h include by stdlib.h */
typedef char           int8_t;
#define __int8_t_defined

typedef unsigned int   uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char  uint8_t;
__extension__ typedef unsigned long long uint64_t;

typedef uint32_t uintptr_t;

/*
 * Some good old style C definitions for where we have imported code
 * from elsewhere.
*/
#define FALSE (1==2)
#define TRUE  (!FALSE)
#undef NULL
#if defined(__cplusplus)
  #define NULL 0
#else
  #define NULL ((void *)0)
#endif

#define false FALSE
#define true TRUE

#include "vibe_os.h"    /* header file for vibe osal */
#include "vibe_debug.h" /* definition of TRCIN, TRCOUT, TRC */

 /** @defgroup macro_hw linux kernel HW registers write macros
    @{
*/

#define GDP_GQR_REG32_WRITE(base, offset, val)            vibe_os_write_register((volatile uint32_t*)(base), (offset), (val))
#define GDP_GQR_REG32_READ( base, offset     ) ((uint32_t)vibe_os_read_register ((volatile uint32_t*)(base), (offset)))

/** @} */


/** @defgroup macro_mem macro related to memory management
    @{  */

/** @brief write memory barrier
 */
#define GDP_GQR_WRITE_MB() vibe_os_write_memory_barrier()


/** @brief memory allocator for sw related content only memory cannot be shared with HW
    @param[in] size in sizeof units
    @retval void* pointer on allocated memory.
 */
#define GDP_GQR_ALLOCATE_LLD_MEM(size)  vibe_os_allocate_memory(size)


/** @brief memory deallocator for sw related content only
    @param[in] mem: pointer returned by ALLOCATE_LLD_MEM
 */
#define GDP_GQR_FREE_LLD_MEM(mem)       vibe_os_free_memory(mem)


/** @brief memcpy macro
    @param[in] d: (void*) pointer on destination address
    @param[in] s: (void*) pointer on source address
    @param[in] n: memory transfer size is size_t units

    @note This is a macro. It might mapped on standard memcpy or not
*/
#define GDP_GQR_MEMCPY(d,s,n) vibe_os_memcpy(d,s,n)

/** @brief memset macro
    @param[in] d: (void*) pointer on destination address
    @param[in] n: memory transfer size is size_t units

    @note This is a macro. It might mapped on standard memcpy or not
*/
#define GDP_GQR_MEMSET(d,n) vibe_os_zero_memory(d,n)

/** @} macro_mem */

/** @defgroup macro_assert macro related to condition checking
    @{  */

/** @brief assert macro
 */
#define GDP_GQR_ASSERT(cond) { if (!(cond)) { vibe_break(#cond, __FILE__, __LINE__); } }

/** @} macro_assert */

#endif /*VERIFICATION_PLATFORM */

typedef uint32_t dma_addr32_t;

#endif /* __LLD_PLATFORM_H__ */
