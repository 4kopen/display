/***********************************************************************
*
* Copyright (c) 2014 STMicroelectronics Limited.
*
* This file is subject to the terms and conditions of the GNU General Public
* License version 2.  See the file COPYING in the main directory of this archive for
* more details.
*
************************************************************************/


 /**@file gdpgqr_handle.h
    @brief internal gdp handle data file for gdp low level driver

    This files exposes internal functions (@ref lld_function), structures
    (@ref lld_struct) and enums (@ref lld_enum) of the lld API.

 */

#ifndef __GDPGQR_HANDLE_H__
#define __GDPGQR_HANDLE_H__

/******************************************************************************/
/* Handle definition                                                          */
/******************************************************************************/
#include "gdpgqr_lld_api.h"


/** @brief struct to store where to pput cmd
    @ingroup lld_priv_struct
*/
struct gdpgqr_ctxt_cmd {
        struct gdpgqr_lld_mem_desc_s cmd_used;
        struct gdpgqr_lld_mem_desc_s cmd_next_vsync;
};

/** @brief status of session
    @ingroup lld_priv_enum
*/
enum gdpgqr_state_e {
        gdpgqr_open_done, /**< @brief opened has been done */
        gdpgqr_stop_done,/**< @brief NULL command has been sent to GDPGQR */
        gdpgqr_close_done,/**< @brief session has been finished or not properly initialised */
};

/** @brief struct to define handle
    @ingroup lld_priv_struct
*/
struct gdpgqr_lld_ctxt {
        /* hw specific */
        void *base_addr[GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE];
        uint32_t mixer_bit[GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE];
        /* command */
        struct gdpgqr_ctxt_cmd ctxt_cmd[GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE];
        /* status */
        int reserved_resources;
        int used_resources;
        enum gdpgqr_lld_profile_e profile;
        /* check */
        enum gdpgqr_state_e state;
        uint32_t magic;
};

#endif /* __GDPGQR_HANDLE_H__ */
