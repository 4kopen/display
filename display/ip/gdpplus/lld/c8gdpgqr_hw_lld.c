/***********************************************************************
 *
 * File: c8gdpgqr_hw_lld.c
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include "gdpgqr_lld_api.h"
#include "gdpgqr_lld_platform.h"
#include "c8gdpgqr_hw_lld.h"

bool c8gdpgqr_IsReady(void *base_addr)
{
        yield();
        /* Assuming GDP is always ready for new programming request! */
        return TRUE;
}

void c8gdpgqr_BootIp(void *base_addr)
{
        yield();
        /* Assuming GDP is always ready for new programming request! */
        return;
}

bool c8gdpgqr_IsBootIpDone (void *base_addr)
{
        /* wait for reset done */
        yield();
        /* Assuming GDP is always ready for new programming request! */
        return TRUE;
}
