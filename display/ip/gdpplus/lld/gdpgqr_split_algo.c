/***********************************************************************
 *
 * File: gdpgqr_split_algo.c
 * Copyright (c) 2000-2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include "gdpgqr_split_algo.h"

#define GDP_GQR_FIXPOINT_INCREMENT 13
#define GDP_GQR_FIXPOINT_PHASE 13
#define GDP_GQR_HSRC_NB_TAP 8

void GDP_GQRSplitProgrammation(const struct cmd_origi_param *full_param_p,
                               struct cmd_split_param *side_param,
                               bool hsrc_enable,
                               bool is422,
                               int bpp) {

        if (hsrc_enable) {

                /* local definition to pass argument to functiom GDPSplitProgrammation
                   which requesting int*
                */

                int widthOut;
                int InitialphaseRight;
                int offset,n;
                n=0;
                widthOut = full_param_p->xds - full_param_p->xdo + 1;

                /* get input width from output width, increment and init phase. what about rounding ? */

                side_param[LEFT].xdo = full_param_p->xdo;
                side_param[LEFT].xds = full_param_p->xdo + widthOut / 2;
                side_param[LEFT].init_phase = full_param_p->init_phase;

                do {
                        InitialphaseRight = (((widthOut) / 2 - 1 - n) * full_param_p->hsrc_incr) + full_param_p->init_phase;
                        offset = (InitialphaseRight >> GDP_GQR_FIXPOINT_INCREMENT) - (GDP_GQR_HSRC_NB_TAP / 2);
                        n++;
                } while ((offset % 2 != 0) && (n < 9) && is422);

                side_param[RIGHT].xdo = side_param[LEFT].xds - n;
                side_param[RIGHT].xds = full_param_p->xds;
                side_param[RIGHT].init_phase = InitialphaseRight - ((InitialphaseRight >> GDP_GQR_FIXPOINT_INCREMENT) << GDP_GQR_FIXPOINT_INCREMENT) +
                        ((GDP_GQR_HSRC_NB_TAP / 2) << GDP_GQR_FIXPOINT_INCREMENT);

                GDP_GQR_ASSERT(side_param[RIGHT].init_phase>=-65636 && side_param[RIGHT].init_phase<65536);

                side_param[LEFT].input_width = ((widthOut * full_param_p->hsrc_incr / 2  +
                                                 //                                                 full_param_p->init_phase + (1<<(GDP_GQR_FIXPOINT_INCREMENT-1))) >>
                                                 //                                                 full_param_p->init_phase) >>
                                                 full_param_p->init_phase + (1<<GDP_GQR_FIXPOINT_INCREMENT)-1) >>
                                                GDP_GQR_FIXPOINT_INCREMENT) + GDP_GQR_HSRC_NB_TAP / 2;
                if (is422 && ( side_param[LEFT].input_width & 1 )) {
                        side_param[LEFT].input_width++;
                }

                TRC(TRC_ID_GDPGQR_LLD, "offset=%d\n",offset);

                if (offset < 0) {
                        side_param[RIGHT].init_phase += (offset<<GDP_GQR_FIXPOINT_INCREMENT);
                        offset = 0;
                }

                if (offset < full_param_p->input_width) {
                  side_param[RIGHT].input_width = full_param_p->input_width - offset;
                } else {
                  offset = full_param_p->input_width - 1;
                  side_param[LEFT].input_width = full_param_p->input_width;
                  side_param[RIGHT].input_width = 1;
                }

                side_param[LEFT].crop_xdo = 0;
                side_param[LEFT].crop_xds = 1;
                side_param[RIGHT].crop_xdo = n;
                side_param[RIGHT].crop_xds = 0;

                side_param[LEFT].pixmap_addr = full_param_p->pixmap_addr;
                side_param[RIGHT].pixmap_addr = full_param_p->pixmap_addr + offset * bpp;

        } else {
                /* need crop of 1 pixel on last col of left side and first col of right side because of AlphaBorder */
                side_param[LEFT].input_width = full_param_p->input_width/2 + 1;
                side_param[LEFT].init_phase = 0;
                side_param[LEFT].pixmap_addr = full_param_p->pixmap_addr;
                side_param[LEFT].xdo = full_param_p->xdo;
                side_param[LEFT].xds = full_param_p->xdo + side_param[LEFT].input_width - 1;
                side_param[LEFT].crop_xdo = 0;
                side_param[LEFT].crop_xds = 1;

                side_param[RIGHT].input_width = full_param_p->input_width + 2 - side_param[LEFT].input_width;
                side_param[RIGHT].init_phase = 0;
                side_param[RIGHT].pixmap_addr = full_param_p->pixmap_addr + ((side_param[LEFT].input_width - 2) * bpp);
                side_param[RIGHT].xdo = side_param[LEFT].xds - 1;
                side_param[RIGHT].xds = full_param_p->xds;
                side_param[RIGHT].crop_xdo = 1;
                side_param[RIGHT].crop_xds = 0;

                /* in 422 single buffer, map on luma */
                if (is422) {
                        if (side_param[RIGHT].pixmap_addr & 2) {
                                side_param[RIGHT].pixmap_addr -= 2;
                                side_param[RIGHT].crop_xdo++;
                                side_param[RIGHT].input_width++;
                                side_param[RIGHT].xdo--;
                        }
                }
        }
}
