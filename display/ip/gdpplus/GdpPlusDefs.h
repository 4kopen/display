/***********************************************************************
 *
 * File: display/ip/gdpplus/GdpPlusDefs.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _GDP_PLUS_DEFS_H
#define _GDP_PLUS_DEFS_H

/***********************************************************************/
/** Generic GDP Plus Register Definitions.                             **/
/***********************************************************************/

#define GDP_PLUS_CTL_RGB_565           0L
#define GDP_PLUS_CTL_RGB_888           0x01L
#define GDP_PLUS_CTL_RGB_32            0x02L
#define GDP_PLUS_CTL_ARGB_8565         0x04L
#define GDP_PLUS_CTL_ARGB_8888         0x05L
#define GDP_PLUS_CTL_ARGB_1555         0x06L
#define GDP_PLUS_CTL_ARGB_4444         0x07L
#define GDP_PLUS_CTL_CLUT8             0x0BL
#define GDP_PLUS_CTL_YCbCr888          0x10L
#define GDP_PLUS_CTL_YCbCr422R         0x12L
#define GDP_PLUS_CTL_AYCbCr8888        0x15L
#define GDP_PLUS_CTL_FORMAT_MASK       0x1FL

#define GDP_PLUS_CTL_ALPHA_RANGE       (1L<<5)
#define GDP_PLUS_CTL_ALPHA_SWITCH      (1L<<6)
#define GDP_PLUS_CTL_PIXEL_REPEAT      (1L<<7)
#define GDP_PLUS_CTL_COLOUR_FILL       (1L<<8)
#define GDP_PLUS_CTL_EN_H_RESIZE       (1L<<10)
#define GDP_PLUS_CTL_EN_V_RESIZE       (1L<<11)
#define GDP_PLUS_CTL_EN_ALPHA_HBOR     (1L<<12)
#define GDP_PLUS_CTL_EN_ALPHA_VBOR     (1L<<13)
#define GDP_PLUS_CTL_EN_COLOR_KEY      (1L<<14)
#define GDP_PLUS_CTL_BCB_COL_KEY_1     (1L<<16)
#define GDP_PLUS_CTL_BCB_COL_KEY_3     (3L<<16)
#define GDP_PLUS_CTL_GY_COL_KEY_1      (1L<<18)
#define GDP_PLUS_CTL_GY_COL_KEY_3      (3L<<18)
#define GDP_PLUS_CTL_RCR_COL_KEY_1     (1L<<20)
#define GDP_PLUS_CTL_RCR_COL_KEY_3     (3L<<20)
#define GDP_PLUS_CTL_BIGENDIAN         (1L<<23)
#define GDP_PLUS_CTL_PREMULT_FORMAT    (1L<<24)
#define GDP_PLUS_CTL_709_SELECT        (1L<<25)
#define GDP_PLUS_CTL_CHROMA_SIGNED     (1L<<26)
#define GDP_PLUS_CTL_EN_CLUT_UPDATE    (1L<<27)
#define GDP_PLUS_CTL_EN_VFILTER_UPD    (1L<<28)
#define GDP_PLUS_CTL_LSB_STUFF         (1L<<29)
#define GDP_PLUS_CTL_EN_HFILTER_UPD    (1L<<30)
#define GDP_PLUS_CTL_WAIT_NEXT_VSYNC   (1L<<31)

#define GDP_PLUS_AGC_GAIN_SHIFT        (16)
#define GDP_PLUS_AGC_GAIN_MASK         (0xFF<<GDP_PLUS_AGC_GAIN_SHIFT)
#define GDP_PLUS_AGC_CONSTANT_SHIFT    (24)
#define GDP_PLUS_AGC_CONSTANT_MASK     (0xFF<<GDP_PLUS_AGC_CONSTANT_SHIFT)

#define GDP_PLUS_HSRC_INCREMENT_MASK   (0x1FFFF)
#define GDP_PLUS_VSRC_INCREMENT_MASK   (0x1FFFF)

#define GDP_PLUS_HIP_INIT_PHASE_MASK   (0x1FFF)
#define GDP_PLUS_VIP_INIT_PHASE_MASK   (0x1FFF)

#define GDP_PLUS_VPS_XDS_MASK          (0x7FFF)
#define GDP_PLUS_VPS_YDS_MASK          (0x7FFF<<16)

#define GDP_PLUS_PMP_PITCH_VALUE_MASK  (0xFFFF)
#define GDP_PLUS_PMP_VSO_SHIFT         (31)
#define GDP_PLUS_PMP_VSO_VALUE_MASK    (0x1)

#define GDP_PLUS_SIZE_WIDTH_MASK       (0x0FFF)
#define GDP_PLUS_SIZE_HEIGHT_MASK      (0x0FFF<<16)

#define GDP_PLUS_PPT_FORCE_ON_MIX0     (1L<<0)
#define GDP_PLUS_PPT_FORCE_ON_MIX1     (1L<<1)
#define GDP_PLUS_PPT_FORCE_ON_MIX2     (1L<<2)
#define GDP_PLUS_PPT_ALPHA_PAT_ENA     (1L<<4)
#define GDP_PLUS_PPT_ALPHA_PAT_MODE    (1L<<5)

#define GDP_PLUS_HVP1_KRING_MASK       (0xF)
#define GDP_PLUS_HVP1_KOVS_MASK        (0xF)
#define GDP_PLUS_HVP1_KRING_V_SHIFT    (0)
#define GDP_PLUS_HVP1_KRING_Y_SHIFT    (4)
#define GDP_PLUS_HVP1_KRING_A_SHIFT    (8)
#define GDP_PLUS_HVP1_KOVS_V_SHIFT     (12)
#define GDP_PLUS_HVP1_KOVS_Y_SHIFT     (16)
#define GDP_PLUS_HVP1_KOVS_A_SHIFT     (20)

#define GDP_PLUS_HVP2_ATHR_SHIFT        (0)
#define GDP_PLUS_HVP2_ATHR_MASK         (0x7)
#define GDP_PLUS_HVP2_DIS_ALPHA_SHIFT   (3)
#define GDP_PLUS_HVP2_MAXUV_SHIFT       (4)
#define GDP_PLUS_HVP2_MAXUV_MASK        (0xF)
#define GDP_PLUS_HVP2_MAXAY_SHIFT       (8)
#define GDP_PLUS_HVP2_MAXAY_MASK        (0xF)

#define GDP_PLUS_HVP1_Y_KRING_DISABLE   (0x8)
#define GDP_PLUS_HVP1_Y_KRING_DEFAULT   (0x4)
#define GDP_PLUS_HVP1_Y_KRING_MAX       (0x1)

#define GDP_PLUS_HVP1_Y_KOVS_DISABLE    (0x8)
#define GDP_PLUS_HVP1_Y_KOVS_DEFAULT    (0x4)
#define GDP_PLUS_HVP1_Y_KOVS_MAX        (0x0)

#define GDP_PLUS_HVP1_UV_KRING_DISABLE  (0x8)
#define GDP_PLUS_HVP1_UV_KRING_DEFAULT  (0x5)
#define GDP_PLUS_HVP1_UV_KRING_MAX      (0x1)

#define GDP_PLUS_HVP1_UV_KOVS_DISABLE   (0x8)
#define GDP_PLUS_HVP1_UV_KOVS_DEFAULT   (0x4)
#define GDP_PLUS_HVP1_UV_KOVS_MAX       (0x0)

#define GDP_PLUS_HVP1_A_KRING_DISABLE   (0x8)
#define GDP_PLUS_HVP1_A_KRING_DEFAULT   (0x4)
#define GDP_PLUS_HVP1_A_KRING_MAX       (0x1)

#define GDP_PLUS_HVP1_A_KOVS_DISABLE    (0x8)
#define GDP_PLUS_HVP1_A_KOVS_DEFAULT    (0x4)
#define GDP_PLUS_HVP1_A_KOVS_MAX        (0x0)

#define GDP_PLUS_HVP1_A_THR_DISABLE     (0x8)
#define GDP_PLUS_HVP1_A_THR_DEFAULT     (0x3)

/* BT.2020 management */
#define GDP_PLUS_BT4_BT2020_EN          (1L<<16)
#define GDP_PLUS_BT4_BT2020_CLAMP       (1L<<17)
/**
* Bit-field : UV_KRING
* Ringing control parameter for chroma
*/

#define GDP_PLUS_HVP1_UV_KRING_SHIFT    (0)
#define GDP_PLUS_HVP1_UV_KRING_WIDTH    (4)
#define GDP_PLUS_HVP1_UV_KRING_MASK     (0x0000000F)

/**
* Bit-field : Y_KRING
* Ringing control parameter for luma
*/

#define GDP_PLUS_HVP1_Y_KRING_SHIFT     (4)
#define GDP_PLUS_HVP1_Y_KRING_WIDTH     (4)
#define GDP_PLUS_HVP1_Y_KRING_MASK      (0x000000F0)

/**
* Bit-field : A_KRING
* Ringing control parameter for alpha
*/

#define GDP_PLUS_HVP1_A_KRING_SHIFT     (8)
#define GDP_PLUS_HVP1_A_KRING_WIDTH     (4)
#define GDP_PLUS_HVP1_A_KRING_MASK      (0x00000F00)


/**
* Bit-field : UV_KOVS
* Overshoot control parameter for chroma
*/

#define GDP_PLUS_HVP1_UV_KOVS_SHIFT     (12)
#define GDP_PLUS_HVP1_UV_KOVS_WIDTH     (4)
#define GDP_PLUS_HVP1_UV_KOVS_MASK      (0x0000F000)

/**
* Bit-field : Y_KOVS
* Overshoot control parameter for luma
*/

#define GDP_PLUS_HVP1_Y_KOVS_SHIFT      (16)
#define GDP_PLUS_HVP1_Y_KOVS_WIDTH      (4)
#define GDP_PLUS_HVP1_Y_KOVS_MASK       (0x000F0000)

/**
* Bit-field : A_KOVS
* Overshoot control parameter for alpha
*/

#define GDP_PLUS_HVP1_A_KOVS_SHIFT      (20)
#define GDP_PLUS_HVP1_A_KOVS_WIDTH      (4)
#define GDP_PLUS_HVP1_A_KOVS_MASK       (0x00F00000)

/*
* The bandwidth error threshold that can be specified is power of 2 and can be in
* the range from 2KB to 1024KB (1MB).
*/
#define GDP_PLUG_IRR_MAX_BWE               (1) /* 2KB */
#endif /* _GDP_PLUS_DEFS_H */
