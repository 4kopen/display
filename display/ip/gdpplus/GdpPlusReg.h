/***********************************************************************
 *
 * File: display/ip/gdpplus/GdpPlusReg.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _GENERIC_GDP_PLUS_REG_H
#define _GENERIC_GDP_PLUS_REG_H

//GDP Plus - Only some of the registers are defined as common
#define GDP_PLUS_NVN_OFFSET    0x1c

#endif /* _GENERIC_GDP_PLUS_REG_H */
