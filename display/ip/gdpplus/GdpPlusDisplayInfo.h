
/***********************************************************************
 *
 * File: display/ip/gdpplus/GdpPlusDisplayInfo.h
 * Copyright (c) 2014 by STMicroelectronics. All rights reserved.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _GDP_PLUS_DISPLAY_INFO_H
#define _GDP_PLUS_DISPLAY_INFO_H


#include "display/generic/DisplayInfo.h"
#include <display/ip/stmviewport.h>

class CDisplayInfo;



class CGdpPlusDisplayInfo: public CDisplayInfo
{
  public:
    CGdpPlusDisplayInfo(void):CDisplayInfo()
    {
        Reset();
    }

    // Reset every CGdpPlusDisplayInfo
    virtual void Reset(void)
    {
        CDisplayInfo::Reset();

        vibe_os_zero_memory (&m_viewport, sizeof(m_viewport) );
    }

  protected:
    /*
     * Basic information from system and buffer presentation flags
     */
    bool                      m_isDisplayInterlaced;

    /*
     * Scaling information conversion increments
     */
    int32_t                   m_srcFrameRectFixedPointX;  //srcFrame x and y values in n.13 notation
    int32_t                   m_srcFrameRectFixedPointY;

    uint32_t                  m_verticalFilterInputSamples;
    uint32_t                  m_verticalFilterOutputSamples;
    uint32_t                  m_horizontalFilterOutputSamples;

    uint32_t                  m_hsrcinc;
    uint32_t                  m_vsrcinc;
    uint32_t                  m_line_step;

    /*
     * Destination Viewport
     */
    stm_viewport_t            m_viewport;

  // Only CGdpPlusPlane is allowed to access CGdpPlusDisplayInfo
  friend class CGdpPlusPlane;
};

#endif /* _GDP_PLUS_DISPLAY_INFO_H */
