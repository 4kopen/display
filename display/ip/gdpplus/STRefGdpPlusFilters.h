/***********************************************************************
 *
 * File: display/ip/gdpplus/STRefGdpPlusFilters.h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STREF_GDP_PLUS_FILTER_H
#define _STREF_GDP_PLUS_FILTER_H


#define GDP_PLUS_HFC_NB_COEFS (32 * 8)
#define GDP_PLUS_VFC_NB_COEFS (32 * 5)

typedef struct {
  int16_t  LUT[GDP_PLUS_VFC_NB_COEFS];
  uint32_t Normalization;
} VFilterSingleCoeffs_t;

typedef struct {
  int16_t  LUT[GDP_PLUS_HFC_NB_COEFS];
  uint32_t Normalization;
} HFilterSingleCoeffs_t;

#include "GdpPlusFilter_legacy.h"
#include "GdpPlusFilter_medium.h"
#include "GdpPlusFilter_sharp.h"
#include "GdpPlusFilter_smooth.h"

/***********************************************************/
/*                                                         */
/*                 HORIZONTAL COEFFICIENTS                 */
/*         ----------------------------------------        */
/*         1      < Zoom < 10     -> use coeffs 'A'        */
/*                  Zoom = 1      -> use coeffs 'B'        */
/*         0.80   < Zoom < 1      -> use coeffs 'C'        */
/*         0.60   < Zoom < 0.80   -> use coeffs 'D'        */
/*         0.50   < Zoom < 0.60   -> use coeffs 'E'        */
/*         0.25   < Zoom < 0.50   -> use coeffs 'F'        */
/*                                                         */
/***********************************************************/

/* Filter coeffs available for Horizontal zoom 1 (no zoom) */
/*---------------------------------------------------------*/
static const HFilterSingleCoeffs_t HorizontalCoefficientsB =
{{
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
}, 8};


/***********************************************************/
/*                                                         */
/*                 VERTICAL COEFFICIENTS                   */
/*         ----------------------------------------        */
/*         1      < Zoom < 10     -> use coeffs 'A'        */
/*                  Zoom = 1      -> use coeffs 'B'        */
/*         0.80   < Zoom < 1      -> use coeffs 'C'        */
/*         0.60   < Zoom < 0.80   -> use coeffs 'D'        */
/*         0.50   < Zoom < 0.60   -> use coeffs 'E'        */
/*         0.25   < Zoom < 0.50   -> use coeffs 'F'        */
/*                                                         */
/***********************************************************/

/* Filter coeffs available for Vertical zoom 1 (no zoom) */
/*-------------------------------------------------------*/
static const VFilterSingleCoeffs_t VerticalCoefficientsB =
{{
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
}, 8};

#endif /* _STREF_GDP_PLUS_FILTER_H */
