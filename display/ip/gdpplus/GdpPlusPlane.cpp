/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2014-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-10-08
***************************************************************************/


#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/Output.h>

#include "display/ip/gdpplus/GdpPlus.h"
#include "display/ip/gdpplus/GdpPlusReg.h"
#include "display/ip/gdpplus/GdpPlusDefs.h"
#include "display/ip/gdpplus/STRefGdpPlusFilters.h"

#include "display/ip/gdpplus/GdpPlusPlane.h"

#define AGC_FULL_RANGE_GAIN       128
/*
 * Map 0-255 to 16-235, 6.25% offset and 85.88% gain, approximately.
 */
#define AGC_CONSTANT_BLACK_LEVEL  64
#define AGC_VIDEO_GAIN            110

// Misc constants
#define GDPGQR_MAX_PITCH_IN_BYTES                        65535
#define GDPGQR_MAX_INPUT_WIDTH                           2048 // Max input line with supported by the GDP+
#define GDPGQR_MAX_UHD_OUTPUT_WIDTH                      2620 // Pixel repeat should be enabled if dest width is up to this value in UHD display mode
#define GDPGQR_DEFAULT_CLOCK_FREQUENCY_MHz               400

/*
 * For GDPGQR plane, there is a first hardware constraint linked to the minimum input viewport/window:
 * - if resize is performed, there is a constraints depending on the H=8/V=5 filters so width >= 8 and height >= 5.
 * The second hardware constraint is linked to the minimum output viewport/window:
 * - width * height >= 30 pixels (30 pixels is the size of an output pixel fifo).
 * For simplyfing the code, we decide to apply both contraints for defining the minimum input/output window so:
 * - min width = 8 and min height = 5 for GDPGQR plane
 * because an input/output window below that coordinate has certainly no interest.
 */
#define GDPGQR_MIN_INPUT_OUTPUT_WIDTH           8
#define GDPGQR_MIN_INPUT_OUTPUT_HEIGHT          5

// ------------------------------------------------
//  Macro definitions
// ------------------------------------------------

#define GdpPlusSetBitField(var, field, val)         \
do                                                  \
{                                                   \
    var &= ~(field##_MASK);                         \
    var |= ((val) << field##_SHIFT) & field##_MASK; \
} while(0)

#define INT_VALf8(x)           (((int64_t) ((x) >> 8)) & 0x1f), (((((int64_t) x) & 0xff) * 100000000) / 256)

static const char *
GetCurrentUseCaseString(stm_display_use_cases_t use_case)
{
#define PRINT_USE_CASE_STRING(use_case) case use_case: return #use_case
  switch (use_case)
  {
    PRINT_USE_CASE_STRING(TOP_TO_TOP);
    PRINT_USE_CASE_STRING(TOP_TO_BOTTOM);
    PRINT_USE_CASE_STRING(TOP_TO_FRAME);
    PRINT_USE_CASE_STRING(BOTTOM_TO_TOP);
    PRINT_USE_CASE_STRING(BOTTOM_TO_BOTTOM);
    PRINT_USE_CASE_STRING(BOTTOM_TO_FRAME);
    PRINT_USE_CASE_STRING(FRAME_TO_TOP);
    PRINT_USE_CASE_STRING(FRAME_TO_BOTTOM);
    PRINT_USE_CASE_STRING(FRAME_TO_FRAME);

    case MAX_USE_CASES:
    default:
      break;
  }

  return "invalid";
#undef PRINT_USE_CASE_STRING
}

static const stm_pixel_format_t g_surfaceFormats[] = {
  SURF_RGB565,
  SURF_ARGB1555,
  SURF_ARGB4444,
  SURF_ARGB8888,
  SURF_BGRA8888,
  SURF_RGB888,
  SURF_ARGB8565,
  SURF_YCBCR422R,
  SURF_CRYCB888,
  SURF_ACRYCB8888
};


static const stm_pixel_format_t g_surfaceFormatsWithClut[] = {
  SURF_RGB565,
  SURF_ARGB1555,
  SURF_ARGB4444,
  SURF_ARGB8888,
  SURF_BGRA8888,
  SURF_RGB888,
  SURF_ARGB8565,
  SURF_YCBCR422R,
  SURF_CRYCB888,
  SURF_ACRYCB8888,
  SURF_CLUT8
};

static const stm_plane_feature_t g_gdpPlaneFeatures[] = {
   PLANE_FEAT_VIDEO_SCALING,
   PLANE_FEAT_SRC_COLOR_KEY,
   PLANE_FEAT_TRANSPARENCY,
   PLANE_FEAT_WINDOW_MODE,
   PLANE_FEAT_GLOBAL_COLOR
};


CGdpPlusPlane::CGdpPlusPlane(const char     *name,
                     uint32_t        id,
                     const CDisplayDevice *pDev,
                     const stm_plane_capabilities_t caps,
                     gdpgqr_lld_profile_e           profile,
                     const char     *pixClockName,
                     const char     *procClockName,
                     uint32_t        max_viewports,
                     bool            bHasClut
                     ):CDisplayPlane(name, id, pDev, caps, pixClockName, procClockName)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "id = %u", id );

  InitializeState(bHasClut, profile, max_viewports);

  m_bHasIrrSupported = false;

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}


void CGdpPlusPlane::InitializeState(bool bHasClut, gdpgqr_lld_profile_e profile, uint32_t max_viewports)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  m_bHasClut    = bHasClut;
  if(!m_bHasClut)
  {
    m_pSurfaceFormats = g_surfaceFormats;
    m_nFormats = N_ELEMENTS (g_surfaceFormats);
  }
  else
  {
    m_pSurfaceFormats = g_surfaceFormatsWithClut;
    m_nFormats = N_ELEMENTS (g_surfaceFormatsWithClut);
  }

  m_pFeatures = g_gdpPlaneFeatures;
  m_nFeatures = N_ELEMENTS (g_gdpPlaneFeatures);

  ResetGdpSetup();

  m_bHasVFilter = false;

  /* Dynamic GDP Plus Setup */
  m_IsColorKeyChanged      = false;
  m_IsGainChanged          = true;
  m_IsTransparencyChanged  = true;

  /*
   * The GDPPlus sample rate converters have an n.13 fixed point format.
   */
  m_fixedpointONE     = 1<<13;

  /*
   * Do not assume scaling is available, SoC specific subclasses will
   * override this in their constructors.
   */
  m_ulMaxHSrcInc   = m_fixedpointONE;
  m_ulMinHSrcInc   = m_fixedpointONE;
  m_ulMaxVSrcInc   = m_fixedpointONE;
  m_ulMinVSrcInc   = m_fixedpointONE;

  m_ulGain           = 255;
  m_ulAlphaRamp      = 0x0000ff00;
  m_ulStaticAlpha[0] = 0;
  m_ulStaticAlpha[1] = 0x80;
  m_ulHDRGain        = AGC_FULL_RANGE_GAIN;
  m_ulHDROffset      = 0;

  vibe_os_zero_memory( &m_HDRGainOffset, sizeof( m_HDRGainOffset ));
  m_HDRGainOffset.hdr_hlg_gain  = AGC_HDR_HLG_VIDEO_GAIN;
  m_HDRGainOffset.hdr_st2084_gain = AGC_HDR_ST2084_VIDEO_GAIN;
  m_HDRGainOffset.sdr_gain = AGC_SDR_FULL_RANGE_GAIN;
  m_HDRGainOffset.hdr_hlg_offset = AGC_HDR_HLG_CONSTANT_BLACK_LEVEL;
  m_HDRGainOffset.hdr_st2084_offset = AGC_HDR_ST2084_CONSTANT_BLACK_LEVEL;
  m_HDRGainOffset.sdr_offset = 0;

  m_ulDirectBaseAddress = 0;
  m_ulQueueBaseAddress  = 0;

  m_FirstGDPPlusConfigOwner = 0;
  m_NextGDPPlusConfigOwner  = 0;

  /* The maximum line step depends on the source pitch and width. The maximum
     pitch the hardware will accept is 65535, whereas the maximum source size
     is 2047. Assuming a source width of 1 pixel in CLUT8 format, we get 1024
     as maximum useful line step; for any other format this will be different.
     Therefore we do additional checks when a buffer is queued, and we adjust
     m_ulMaxLineStep dynamically, as other code uses that as basis for some
     calculation/checks ... */
  m_ulMaxLineStep = 1024;

  /*
   * Note: the scaling capabilities are calculated in the Create method as
   *       the sample rate limits may be overriden.
   */

  /*
   *  Default Input and Output Window Modes.
   */
  m_InputWindowMode  = MANUAL_MODE;
  m_OutputWindowMode = MANUAL_MODE;

  /*
   *  Default AR Conversion Mode is 'IGNORE' for GDP Plus plane.
   */
  m_AspectRatioConversionMode = ASPECT_RATIO_CONV_IGNORE;

  m_wasEnabled = false;
  m_b4k2k      = false;

  m_gdpDisplayInfo.Reset();
  m_pNodeToDisplay = 0;

  /* Dynamic GDP Plus Setup*/
  m_IsGainChanged                     = false;
  m_IsColorKeyChanged                 = false;
  m_IsTransparencyChanged             = false;

  /*
   * LLD part.
   */
  vibe_os_zero_memory(&m_lldCmdBuffer, sizeof(m_lldCmdBuffer));
  vibe_os_zero_memory(&m_lldMemDesc,      sizeof(m_lldMemDesc));
  vibe_os_zero_memory(&m_gdpConf,         sizeof(m_gdpConf));
  m_lldHandle         = 0;
  m_lldProfile        = profile;
  m_MaxViewPorts      = max_viewports;

  m_clockFreqInMHz = GDPGQR_DEFAULT_CLOCK_FREQUENCY_MHz;
  m_procClockToleranceInNs  = 0;

  m_CurrentRBW = 0;
  m_PrevRBW    = 0;
  m_NextRBW    = 0;

  m_pGamutMatrix = 0;

  /*
   * Pixel Repeat is supported by GDP+ hardware.
   */
  m_bHasPixelRepeat = true;

  m_currentDisplayUseCase     = MAX_USE_CASES;
  m_lastProgrammedUseCase     = MAX_USE_CASES;
  m_unexpectedFieldInversion  = false;

  m_bHasIrrSupported = false;

  m_fifo_size = 0;
  m_RequesteBW = 0;
  m_IrrLinkCapacity = 0;

  m_LinkToGDPPlus = this;

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}

bool CGdpPlusPlane::InitIrrParams(void)
{
  uint32_t plug_fifo_size = 0;
  uint32_t plug_bus_width = 0;
  uint64_t plug_bus_freq  = 0;
  const char* plug_bus_clk_name = NULL;
  vibe_clk plug_bus_clk;
  int      err = 0;

  if (!m_bHasIrrSupported)
  {
      return false;
  }

  err = vibe_os_of_property_read_u32("plug_fifo_size", &plug_fifo_size);
  if (err || (plug_fifo_size == 0))
  {
      TRC( TRC_ID_ERROR, "cannot get IRR params ");
      return false;
  }

  m_fifo_size = plug_fifo_size;

  err = vibe_os_of_property_read_u32("plug_bus_width", &plug_bus_width );
  if (err || (plug_bus_width == 0))
  {
      TRC( TRC_ID_ERROR, "cannot get IRR params ");
      return false;
  }

  err = vibe_os_of_property_read_string("plug_bus_clk_name",
                                         &plug_bus_clk_name);

  if (err)
  {
      TRC( TRC_ID_ERROR, "cannot get IRR params ");
      return false;
  }

  err = vibe_os_clk_get(plug_bus_clk_name, &plug_bus_clk);
  if (err)
  {
      TRC( TRC_ID_ERROR, "cannot get IRR params ");
      return false;
  }

  plug_bus_freq = vibe_os_clk_get_rate(&plug_bus_clk);
  vibe_os_clk_put(&plug_bus_clk);

  m_IrrLinkCapacity = plug_bus_freq * plug_bus_width;

  return true;
}

/* Should be called with valid output and GDP setup */
bool CGdpPlusPlane::UpdateIrrSetup(void)
{
  if (m_bHasIrrSupported)
  {
    uint32_t Bpp   = m_gdpDisplayInfo.m_selectedPicture.pixelDepth>>3;
    uint32_t Xin   = m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.width;
    uint32_t Xout  = GetIrrXout();
    uint32_t Yin   = m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.height;
    uint32_t Yout  = m_gdpDisplayInfo.m_dstHeight;
    uint64_t FpixDisp  = m_outputInfo.currentMode.mode_timing.pixel_clock_freq;
    uint32_t Qin = (Xin * Bpp) / GetNumberOfHwRequired();
    uint64_t RBW = 0; /*Requested Bandwidth*/
    uint32_t RoundedVSF = 0;

    if (Qin > m_fifo_size )
    {
      if (Yin >= Yout)
      {
          RoundedVSF = (63 * Yin)/Yout;
      }
      else
      {
          RoundedVSF = 63 * 1;
      }
    }
    else
    {
      RoundedVSF = (63 * Yin)/Yout;
    }

    RBW = vibe_os_div64 ((uint64_t)(FpixDisp * Qin  * RoundedVSF) ,(uint64_t) Xout);


    /* rounding UP to always have a margin vs real IRR*/
    m_NextRBW = vibe_os_div64((RBW + (m_IrrLinkCapacity -1)) , m_IrrLinkCapacity);

    TRC( TRC_ID_GDP_PLUS_PLANE, "Requested BW = %llu ", m_NextRBW);

  }

  return m_bHasIrrSupported;
}

bool CGdpPlusPlane::ApplyIrrSetup()
{
  gdpgqr_lld_irr_params_t irr_params;

  if (!m_bHasIrrSupported)
  {
    irr_params.RBW = 0;
    irr_params.BWE = 0;
    return false;
  }

  irr_params.BWE = GDP_PLUG_IRR_MAX_BWE;
  if (m_NextRBW > m_CurrentRBW)
  {
    /*
     *  Low to High transition : Immediate programming of next RBW.
     */
    irr_params.RBW = m_NextRBW;
    gdpgqr_lld_update_irr_params (m_lldHandle, &irr_params);
    m_CurrentRBW = m_NextRBW;
  }
  else if (m_PrevRBW < m_CurrentRBW)
  {
    /*
     * High to Low transition : Immediate programming of current RBW  (equivalent
     * to delayed programming of next RBW).
     */
    irr_params.RBW = m_PrevRBW;
    gdpgqr_lld_update_irr_params (m_lldHandle, &irr_params);
    m_CurrentRBW = m_NextRBW;
  }

  m_PrevRBW = m_NextRBW;

  return true;
}

uint32_t CGdpPlusPlane::GetIrrXout(void)
{
   return m_outputInfo.currentMode.mode_params.active_area_width;

}

bool CGdpPlusPlane::SetFirstGDPPlusConfigOwner(CGdpPlusPlane *gdpplus)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "Setting %s first ", gdpplus->GetName() );

  if(m_NextGDPPlusConfigOwner)
  {
    if(!m_NextGDPPlusConfigOwner->SetFirstGDPPlusConfigOwner(gdpplus))
      return false;
  }

  m_FirstGDPPlusConfigOwner = gdpplus;

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
  return true;
}


CGdpPlusPlane::~CGdpPlusPlane(void)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  CloseLldSession();
  FreeLldMemory(&m_lldCmdBuffer);

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}


bool CGdpPlusPlane::GetCompoundControlRange(stm_display_plane_control_t selector, stm_compound_control_range_t *range)
{
  range->min_val.x = 0;
  range->min_val.y = 0;
  range->min_val.width  = GDPGQR_MIN_INPUT_OUTPUT_WIDTH;
  range->min_val.height = GDPGQR_MIN_INPUT_OUTPUT_HEIGHT;

  range->max_val.x = 0;
  range->max_val.y = 0;
  if(m_b4k2k == false)
  {
    range->max_val.width = 1920;
    range->max_val.height = 2047;
  }
  else
  {
    range->max_val.width = 65535;
    range->max_val.height = 65535;
  }

  range->default_val.x = 0;
  range->default_val.y = 0;
  range->default_val.width = 0;
  range->default_val.height = 0;

  range->step.x = 1;
  range->step.y = 1;
  range->step.width = 1;
  range->step.height = 1;

  if(m_pOutput && (selector == PLANE_CTRL_OUTPUT_WINDOW_VALUE))
  {
    if(m_outputInfo.isOutputStarted)
    {
      if( (m_b4k2k == true)
       || ((m_outputInfo.currentMode.mode_id != STM_TIMING_MODE_4K2K29970_296703)
        && (m_outputInfo.currentMode.mode_id != STM_TIMING_MODE_4K2K25000_297000)
        && (m_outputInfo.currentMode.mode_id != STM_TIMING_MODE_4K2K24000_297000)
        && (m_outputInfo.currentMode.mode_id != STM_TIMING_MODE_4K2K24000_297000_WIDE)
        && (m_outputInfo.currentMode.mode_id != STM_TIMING_MODE_4K2K23980_296703)))
      {
        range->max_val.width  = m_outputInfo.currentMode.mode_params.active_area_width - range->min_val.x;
        range->max_val.height = m_outputInfo.currentMode.mode_params.active_area_height - range->min_val.y;
      }
    }
  }

  return true;
}


bool CGdpPlusPlane::IsFeatureApplicable( stm_plane_feature_t feature, bool *applicable) const
{
  bool is_supported = false;
  switch(feature)
  {
    case PLANE_FEAT_WINDOW_MODE:
        is_supported = m_bHasVFilter;
        if(applicable)
            *applicable = m_bHasVFilter;
    break;

    default:
        is_supported =  CDisplayPlane::IsFeatureApplicable(feature, applicable);
  }
  return is_supported;
}


bool CGdpPlusPlane::Create(void)
{
  gdpgqr_lld_mem_need_s    lldMemNeeds;

  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  if(!CDisplayPlane::Create())
    return false;

  // Get LLD internal memory needs (each viewports needs two nodes TOP and BOTTOM)
  gdpgqr_lld_get_memory_needs(m_lldProfile, 2*m_MaxViewPorts, &lldMemNeeds);

  // Allocate memory for LLD internal needs
  if(!AllocateLldMemory(lldMemNeeds, &m_lldCmdBuffer))
  {
    PLANE_TRC( TRC_ID_ERROR, "AllocateLldMemory failed" );
    return false;
  }

  // Prepare LLD memory descriptor
  m_lldMemDesc.size       = lldMemNeeds.size;
  m_lldMemDesc.alignment  = lldMemNeeds.alignment;
  m_lldMemDesc.logical    = m_lldCmdBuffer.pData;
  m_lldMemDesc.physical   = (dma_addr32_t)m_lldCmdBuffer.ulPhysical;

  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "GDP Plus LLD mem allocated at %p (phys = 0x%08X) : size = 0x%08X - alignement = %d",
            m_lldMemDesc.logical, m_lldMemDesc.physical, m_lldMemDesc.size, m_lldMemDesc.alignment);

  // Open LLD session
  if(!OpenLldSession(m_lldMemDesc))
  {
    PLANE_TRC(TRC_ID_ERROR, "Failed to open LLD session");
    return false;
  }

  // Now we know which hardware(s) has(have) been "allocated" to this plane by the LLD
  // => we can initialize clock infos

  if (!m_Filter.Create())
  {
    PLANE_TRC( TRC_ID_ERROR, "m_Filter.Create returns error" );
    return false;
  }

  SetScalingCapabilities(&m_rescale_caps);

  if (m_procClockName[0] != '\0')
  {
    const uint32_t freqMhz = vibe_os_clk_get_rate(&m_procClock) / 1000000;
    m_clockFreqInMHz = freqMhz !=0 ? freqMhz : GDPGQR_DEFAULT_CLOCK_FREQUENCY_MHz;
    PLANE_TRC(TRC_ID_GDP_PLUS_PLANE, "GDP+ clock rate is %u MHz!", m_clockFreqInMHz);
  }

  /*
   * Calculate the Processing clock tolerance value (+/-50 ppm).
   */
  uint32_t ToleranceInKHz = (uint32_t)vibe_os_div64( 50 * (uint64_t)m_clockFreqInMHz,1000);
  /* Multiplied by 1000 to get a result in Ns */
  m_procClockToleranceInNs  = (ToleranceInKHz!=0) ? vibe_os_div64( 1000 ,(uint64_t)ToleranceInKHz) : 0;

  m_bHasIrrSupported = InitIrrParams();

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
  return true;
}


void CGdpPlusPlane::CalculateHorizontalScaling()
{
  /*
   * Calculate the scaling factors, with one extra bit of precision so we can
   * round the result.
   */
  m_gdpDisplayInfo.m_hsrcinc = (m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.width * m_fixedpointONE * 2) / m_gdpDisplayInfo.m_dstFrameRect.width;

  if(m_gdpDisplayInfo.m_selectedPicture.isSrc420 || m_gdpDisplayInfo.m_selectedPicture.isSrc422)
  {
    /*
     * For formats with half chroma, we have to round up or down to an even
     * number, so that the chroma value which is half this value cannot lose
     * precision.
     */
    m_gdpDisplayInfo.m_hsrcinc += 1L<<1;
    m_gdpDisplayInfo.m_hsrcinc &= ~0x3;
    m_gdpDisplayInfo.m_hsrcinc >>= 1;
  }
  else
  {
    /*
     * As chroma is not an issue here just round the result and convert to
     * the correct fixed point format.
     */
    m_gdpDisplayInfo.m_hsrcinc += 1;
    m_gdpDisplayInfo.m_hsrcinc >>= 1;
  }

  bool bRecalculateDstWidth = false;

  if(m_gdpDisplayInfo.m_hsrcinc < m_ulMinHSrcInc)
  {
      m_gdpDisplayInfo.m_hsrcinc = m_ulMinHSrcInc;
      bRecalculateDstWidth = true;
  }

  if(m_gdpDisplayInfo.m_hsrcinc > m_ulMaxHSrcInc)
  {
      m_gdpDisplayInfo.m_hsrcinc = m_ulMaxHSrcInc;
      bRecalculateDstWidth = true;
  }

  if(bRecalculateDstWidth)
    m_gdpDisplayInfo.m_dstFrameRect.width  = (m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.width  * m_fixedpointONE) / m_gdpDisplayInfo.m_hsrcinc;

}


static inline uint32_t
__attribute__((const))
_MIN (uint32_t x, uint32_t y)
{
  return (x < y) ? x : y;
}

static inline uint32_t
__attribute__((const))
_MAX (uint32_t x, uint32_t y)
{
  return (x > y) ? x : y;
}


unsigned int CGdpPlusPlane::GetNumberOfHwRequired(void) const
{
  unsigned int              numberOfHw = 1;
  gdpgqr_lld_output_param_s output_params;

  output_params.output_vtg_fps = m_outputInfo.currentMode.mode_params.vertical_refresh_rate;
  output_params.width          = m_outputInfo.currentMode.mode_params.active_area_width;
  output_params.height         = m_outputInfo.currentMode.mode_params.active_area_height;

  if (m_lldProfile == GDPGQR_PROFILE_4KP60)
  {
    if ( output_params.output_vtg_fps > 30000 )
    {
      if ((output_params.width * output_params.height) > (1920 * 1080))
      {
        PLANE_TRC(TRC_ID_GDP_PLUS_PLANE, "Will use 2 GDP+ hardware instances");
        numberOfHw = 2;
      }
    }
  }

  return numberOfHw;
}


unsigned int CGdpPlusPlane::GetMaxLinesSkippableByHw(unsigned int srcPicturePitch) const
{
  // Address(Line N+1) = Address(Line N) + Pitch
  // Max number of lines skippable = (Max pitch supported by GDP+ HW) / (Input picture pitch)
  return GDPGQR_MAX_PITCH_IN_BYTES / srcPicturePitch;
}


void CGdpPlusPlane::TruncateSourceToHWLimits(CDisplayInfo* pDisplayInfo)
{
  if((m_capabilities & PLANE_CAPS_GRAPHICS_BEST_QUALITY) != 0)
  {
    /*
     * The GDP+ hardware isn't able to process a source width up to 2048
     * pixels when the vertical scaling is enabled.
     */
    if(pDisplayInfo->m_selectedPicture.srcFrameRect.height != pDisplayInfo->m_dstFrameRect.height)
    {
      const unsigned int numberOfHw = GetNumberOfHwRequired();

      /* Get minimal value of input_width and max_input_width value */
      uint32_t width = _MIN(pDisplayInfo->m_selectedPicture.srcFrameRect.width, (GDPGQR_MAX_INPUT_WIDTH * numberOfHw));

      /* Truncate input width to min value */
      if(pDisplayInfo->m_selectedPicture.srcFrameRect.width > width)
      {
        pDisplayInfo->m_selectedPicture.srcFrameRect.width = width;
        PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Truncate input width to Max Input value (%d)).", width);
      }
    }
  }

  /* Workaround to not read outside of input picture image due to filter */
  /* For example: source picture 1920x1080 progressive, output 1080i => if line 1081 is red, */
  /* it is visible on the screen, seems that vertical tap is using it.                       */
  if(!m_gdpDisplayInfo.m_isSrcInterlaced && m_gdpDisplayInfo.m_isDisplayInterlaced) {
    if (m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.y/16 + m_gdpDisplayInfo.m_selectedPicture.srcHeight ==
        m_gdpDisplayInfo.m_selectedPicture.height) {
        m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.height -= 1;
        m_gdpDisplayInfo.m_selectedPicture.srcHeight -= 1;
    }
  }
}


bool CGdpPlusPlane::IsHwProcessingTimeOk(
                                uint32_t            vhsrcInputWidth,
                                uint32_t            vhsrcInputHeight,
                                uint32_t            vhsrcOutputWidth,
                                uint32_t            vhsrcOutputHeight) const
{
  /*
   *  The downscale constraint is:
   *  GDPGQR_processing_time < Output_line_time
   *
   *  * GDPGQR_processing_time is the time necessarry to generate a line.
   *  * Output_line_time is the time to display a line (total VTG width).
   */

  /*
   * Calculation of GDPGQR_processing_time
   *
   * GDPGQR_processing_time = Tproc x Rv x max(IN_width,OUT_width)
   *
   *  Where:
   *    - Tproc is the processing clock period
   *    - Rv is the Vertical downsize ratio = IN_height / OUT_height
   *    - IN_width is the input width of the line
   *    - OUT_width is the output width of the line
   */
  /* Multiplied by 1000 to get a result in Ns */
  const uint64_t ProcessingTimeInNs = vibe_os_div64( 1000 * (uint64_t)vhsrcInputHeight * (uint64_t)_MAX(vhsrcInputWidth,vhsrcOutputWidth),
                                                                 (uint64_t)vhsrcOutputHeight * (uint64_t)m_clockFreqInMHz) + m_procClockToleranceInNs;

  /*
   * Calculation of Output_line_time
   *
   * Output_line_time = Tpix x VTG_width
   *
   *  Where:
   *    - Tpix is the pixel clock period
   *    - VTG_width is the witdh of a VTG line (including blanking)
   */
  const uint32_t pixelsPerLine     = m_outputInfo.currentMode.mode_timing.pixels_per_line;
  uint32_t outputPixelClockInHz    = m_outputInfo.currentMode.mode_timing.pixel_clock_freq;
  uint64_t PixelClockToleranceInNs = m_pOutput->GetPixelClockToleranceInNs();

  /* Multiplied by 1000000000 to get a result in Ns */
  const uint64_t outputVideoLineDurationInNs = vibe_os_div64(1000000000 * (uint64_t)pixelsPerLine, (uint64_t)outputPixelClockInHz) - PixelClockToleranceInNs;
  const bool bIsHwProcessingTimeOk = ProcessingTimeInNs < outputVideoLineDurationInNs ? true : false;

  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "procClockToleranceInNs      = %llu", m_procClockToleranceInNs);
  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "pixelClockToleranceInNs     = %llu", PixelClockToleranceInNs);
  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "ProcessingTimeInNs          = %llu", ProcessingTimeInNs);
  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "Tpix (MHz)                  = %u", outputPixelClockInHz/1000000);
  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "VTG_width (pixels)          = %u", pixelsPerLine);
  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "outputVideoLineDurationInNs = %llu", outputVideoLineDurationInNs);
  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "Processing is %s", bIsHwProcessingTimeOk ? "achievable" : "not achievable");

  return bIsHwProcessingTimeOk;
}


bool CGdpPlusPlane::IsScalingPossibleBySkippingLines(CDisplayNode*        pCurrNode,
                                    CDisplayInfo*        pDisplayInfo)
{
  const unsigned int maxNbOfLinesSkippable = GetMaxLinesSkippableByHw(pDisplayInfo->m_selectedPicture.pitch);

  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "Max number of lines skippable by GDP+ %u", maxNbOfLinesSkippable);

  // Artificially reduce picture height incrementally by skipping lines and check if scaling is possible
  for(unsigned int nbOfLinesToSkip = 1; nbOfLinesToSkip < maxNbOfLinesSkippable; nbOfLinesToSkip++)
  {
    PLANE_TRC(TRC_ID_GDP_PLUS_IT, "## Checking if scaling possible with %s picture by skipping %u lines out of %u",
                                pDisplayInfo->m_isSecondaryPictureSelected ? "Secondary" : "Primary",
                                nbOfLinesToSkip, maxNbOfLinesSkippable);

    pDisplayInfo->m_srcLinesSkipped = nbOfLinesToSkip;
    FillSelectedPictureDisplayInfo(pCurrNode, pDisplayInfo);

    if(IsScalingPossibleByHw(pDisplayInfo))
    {
      // Scaling possible
      PLANE_TRC(TRC_ID_GDP_PLUS_IT, "## Scaling possible with %s picture by skipping %u lines out of %u",
                                  pDisplayInfo->m_isSecondaryPictureSelected ? "Secondary" : "Primary",
                                  nbOfLinesToSkip, maxNbOfLinesSkippable);
      return true;
    }
    /* Reset m_srcLinesSkipped before going on */
    pDisplayInfo->m_srcLinesSkipped = 0;
  }

  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "## Scaling NOT possible by skipping lines");
  return false;
}


bool CGdpPlusPlane::IsScalingPossibleByHw(CDisplayInfo*    pDisplayInfo)
{
  const uint32_t     srcRectWidth      = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
  const uint32_t     srcRectHeight     = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
  const uint32_t     dstRectWidth      = pDisplayInfo->m_dstFrameRect.width;
  const uint32_t     dstRectHeight     = pDisplayInfo->m_dstFrameRect.height;
  const unsigned int numberOfHw        = GetNumberOfHwRequired();
  uint32_t           srcRectWidthOneHw = srcRectWidth / numberOfHw;
  uint32_t           dstRectWidthOneHw = dstRectWidth / numberOfHw;

  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "Trying: IsSrcInterlaced = %d, src width = %u, src height = %u, dst width = %u, dst height = %u",
                              pDisplayInfo->m_isSrcInterlaced, srcRectWidth, srcRectHeight, dstRectWidth, dstRectHeight);

  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "Number of HW %u, Input width with overlap %u, Output width with overlap %u", numberOfHw, srcRectWidthOneHw, dstRectWidthOneHw);

  if(IsHwProcessingTimeOk(srcRectWidthOneHw, srcRectHeight, dstRectWidthOneHw, dstRectHeight))
  {
    // All the conditions are OK. This scaling is possible
    PLANE_TRC(TRC_ID_GDP_PLUS_PLANE, "Scaling is possible");
    this->TruncateSourceToHWLimits(pDisplayInfo);
    return true;
  }

  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "HwProcessingTime NOK: scaling not possible");
  return false;
}


bool CGdpPlusPlane::AdjustIOWindowsForHWConstraints(CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo) const
{
  if(m_b4k2k == false)
  {
    /* Adjust the source rectangle */
    if(pDisplayInfo->m_selectedPicture.srcFrameRect.width > 1920)
      pDisplayInfo->m_selectedPicture.srcFrameRect.width = 1920;
    if(pDisplayInfo->m_selectedPicture.srcFrameRect.height > 2047)
      pDisplayInfo->m_selectedPicture.srcFrameRect.height = 2047;
  }

  if((m_capabilities & PLANE_CAPS_GRAPHICS_BEST_QUALITY) == 0)
  {
    /* Get minimal value of input and output rectangle */
    uint32_t width, height;

    width  = _MIN(pDisplayInfo->m_selectedPicture.srcFrameRect.width, pDisplayInfo->m_dstFrameRect.width);
    height = _MIN(pDisplayInfo->m_selectedPicture.srcFrameRect.height, pDisplayInfo->m_dstFrameRect.height);

    /* Truncate input and output to min value */
    pDisplayInfo->m_selectedPicture.srcFrameRect.width  = pDisplayInfo->m_dstFrameRect.width  = width;
    pDisplayInfo->m_selectedPicture.srcFrameRect.height = pDisplayInfo->m_dstFrameRect.height = height;
  }

  return true;
}


bool CGdpPlusPlane::IsScalingPossible(CDisplayNode* pCurrNode, CDisplayInfo* pDisplayInfo)
{
  PLANE_TRC(TRC_ID_GDP_PLUS_IT, "## Checking if scaling possible with Primary picture");

  pDisplayInfo->m_isSecondaryPictureSelected = false;
  FillSelectedPictureDisplayInfo(pCurrNode, pDisplayInfo);

  if(IsScalingPossibleByHw(pDisplayInfo))
  {
    // Scaling possible with the Primary picture
    PLANE_TRC(TRC_ID_GDP_PLUS_IT, "## Scaling possible with Primary picture");
    return true;
  }

  // No decimation available for GFX planes
  // In case of Progressive source a bigger downscaling can be achieved by skipping lines from source picture
  if(!pDisplayInfo->m_isSrcInterlaced)
  {
    if(IsScalingPossibleBySkippingLines(pCurrNode, pDisplayInfo))
    {
      // Scaling possible with Primary picture by skipping lines
      PLANE_TRC(TRC_ID_GDP_PLUS_IT, "## Scaling possible with Primary picture by skipping %d lines", pDisplayInfo->m_srcLinesSkipped);
      return true;
    }
  }

  PLANE_TRC(TRC_ID_ERROR, "Scaling not possible!");
  return false;
}


void CGdpPlusPlane::CalculateVerticalScaling()
{
  unsigned long srcHeight;
  bool bRecalculateDstHeight = false;

  m_gdpDisplayInfo.m_line_step = 1;
  m_gdpDisplayInfo.m_verticalFilterOutputSamples = m_gdpDisplayInfo.m_dstHeight;

  do
  {
    srcHeight = m_gdpDisplayInfo.m_selectedPicture.srcHeight / m_gdpDisplayInfo.m_line_step;
    m_gdpDisplayInfo.m_verticalFilterInputSamples  = srcHeight;

    /*
     * Calculate the scaling factors, with one extra bit of precision so we can
     * round the result.
     */
    m_gdpDisplayInfo.m_vsrcinc = (m_gdpDisplayInfo.m_verticalFilterInputSamples * m_fixedpointONE * 2) / m_gdpDisplayInfo.m_verticalFilterOutputSamples;

    if(m_gdpDisplayInfo.m_selectedPicture.isSrc420)
    {
      /*
       * For formats with half vertical chroma, we have to round up or down to
       * an even number, so that the chroma value which is half this value
       * cannot lose precision.
       */
      m_gdpDisplayInfo.m_vsrcinc += 1L<<1;
      m_gdpDisplayInfo.m_vsrcinc &= ~0x3;
      m_gdpDisplayInfo.m_vsrcinc >>= 1;
    }
    else
    {
      /*
       * As chroma is not an issue here just round the result and convert to
       * the correct fixed point format.
       */
      m_gdpDisplayInfo.m_vsrcinc += 1;
      m_gdpDisplayInfo.m_vsrcinc >>= 1;
    }

    if(m_gdpDisplayInfo.m_vsrcinc < m_ulMinVSrcInc)
    {
      m_gdpDisplayInfo.m_vsrcinc = m_ulMinVSrcInc;
      bRecalculateDstHeight = true;
    }

    if(m_gdpDisplayInfo.m_vsrcinc > m_ulMaxVSrcInc)
    {
      if(m_gdpDisplayInfo.m_line_step < m_ulMaxLineStep)
      {
        /*
         * Perform a software Line Skipping and invalidate increment value
         * then we restart its calculation for the begin.
         */
        ++m_gdpDisplayInfo.m_line_step;
        m_gdpDisplayInfo.m_vsrcinc = 0;
      }
      else
      {
        m_gdpDisplayInfo.m_vsrcinc = m_ulMaxVSrcInc;
        bRecalculateDstHeight = true;
      }
    }
  } while ( m_gdpDisplayInfo.m_vsrcinc == 0 );

  if(bRecalculateDstHeight)
  {
    m_gdpDisplayInfo.m_dstHeight = (m_gdpDisplayInfo.m_verticalFilterInputSamples * m_fixedpointONE) / m_gdpDisplayInfo.m_vsrcinc;
    m_gdpDisplayInfo.m_dstFrameRect.height = m_gdpDisplayInfo.m_dstHeight;
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Max/Min Vertical Upscale is exceeded, new Output Window Height is %4d", m_gdpDisplayInfo.m_dstFrameRect.height);
  }
}


void CGdpPlusPlane::CalculateViewport()
{
  /*
   * Now we know the destination viewport extents for the
   * compositor/mixer, which may get clipped by the active video area of
   * the display mode.
   */
  m_gdpDisplayInfo.m_viewport.startPixel = STCalculateViewportPixel(&m_outputInfo.currentMode, m_gdpDisplayInfo.m_dstFrameRect.x);
  m_gdpDisplayInfo.m_viewport.stopPixel  = STCalculateViewportPixel(&m_outputInfo.currentMode, m_gdpDisplayInfo.m_dstFrameRect.x + m_gdpDisplayInfo.m_dstFrameRect.width - 1);

  /*
   * We need to limit the number of output samples generated to the
   * (possibly clipped) viewport width.
   */
  m_gdpDisplayInfo.m_horizontalFilterOutputSamples = (m_gdpDisplayInfo.m_viewport.stopPixel - m_gdpDisplayInfo.m_viewport.startPixel + 1);

    m_gdpDisplayInfo.m_viewport.startLine  = STCalculateViewportLine(&m_outputInfo.currentMode,  m_gdpDisplayInfo.m_dstFrameRect.y);

  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "samples = %u startpixel = %u stoppixel = %u",m_gdpDisplayInfo.m_horizontalFilterOutputSamples, m_gdpDisplayInfo.m_viewport.startPixel, m_gdpDisplayInfo.m_viewport.stopPixel );

  /*
   * The viewport line numbering is always frame based, even on
   * an interlaced display
   */
  m_gdpDisplayInfo.m_viewport.stopLine = STCalculateViewportLine(&m_outputInfo.currentMode, m_gdpDisplayInfo.m_dstFrameRect.y + m_gdpDisplayInfo.m_dstFrameRect.height - 1);

  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "startline = %u stopline = %u",m_gdpDisplayInfo.m_viewport.startLine,m_gdpDisplayInfo.m_viewport.stopLine );
}


void CGdpPlusPlane::AdjustBufferInfoForScaling(gdpgqr_lld_hw_viewport_s       &topNode,
                                                 gdpgqr_lld_hw_viewport_s       &botNode,
                                                 bool isDisplayInterlaced)
{
  PLANE_TRCIN(TRC_ID_GDP_PLUS_IT, "");

  if(!m_gdpDisplayInfo.m_isSrcInterlaced && isDisplayInterlaced)
  {
    if (m_ulMaxLineStep > 1)
    {
      PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "using line skip (max %u)\n", m_ulMaxLineStep );
    }
    else
    {
      /*
       * we have to convert to interlaced using a
       * 2x downscale. But, if the hardware cannot do that or the overall
       * scale then goes outside the hardware capabilities, we treat the
       * source as interlaced instead.
       */

      // We convert a progressive Frame into an Interlaced Field so we should consider
      // the Src Frame Height and the dst Field Height
      bool convert_to_interlaced = (m_gdpDisplayInfo.m_dstHeight < (m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.height * m_fixedpointONE / m_ulMaxVSrcInc));

      if(convert_to_interlaced)
      {
        PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, " converting source to interlaced for downscaling \n " );
        m_gdpDisplayInfo.m_isSrcInterlaced           = true;
        m_gdpDisplayInfo.m_selectedPicture.srcHeight = m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.height/2;
        m_pNodeToDisplay->m_firstFieldType           = GNODE_TOP_FIELD;
      }
    }
  }

  if(m_gdpDisplayInfo.m_isSrcInterlaced)
  {
    /*
     * Change the vertical start position from frame to field coordinates
     *
     * Remember that this value is in the fixed point format.
     */
    m_gdpDisplayInfo.m_srcFrameRectFixedPointY /= 2;
  }

  CalculateHorizontalScaling();
  CalculateVerticalScaling();

  /*
   * Now adjust the source coordinate system to take into account line skipping
   */
  m_gdpDisplayInfo.m_srcFrameRectFixedPointY /= m_gdpDisplayInfo.m_line_step;

  CalculateViewport();

  PLANE_TRCOUT(TRC_ID_GDP_PLUS_IT, "");
}


/* Called on Vsync */
void CGdpPlusPlane::PresentDisplayNode( CDisplayNode * pPrevNode,
                                          CDisplayNode * pCurrNode,
                                          CDisplayNode * pNextNode,
                                          bool isPictureRepeated,
                                          bool isDisplayInterlaced,
                                          bool isTopFieldOnDisplay,
                                          const stm_time64_t &vsyncTime )
{
  bool isAddrChanged = false;
  bool procClkTemporaryEnabled = false;

  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "new node %p",(void *)pCurrNode );

  // Check that all the conditions are met to allow a display on this plane
  if (!isDisplayPossible(pCurrNode))
  {
    // One or more condition is not met to allow the display on this plane
    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Invalid Node to display !!!" );
    return;
  }

  // Check that VSyncLock is already taken before accessing to shared variables
  DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

  /*If a buffer has been queued to just do a flip*/
  if ( m_ulQueueBaseAddress != (pCurrNode->m_bufferDesc.src.primary_picture.video_buffer_addr) )
  {
    isAddrChanged = true;
    /* invalidate GDP conf so it will be generated again later */
    m_gdpConf.isValid = false;
    m_ulQueueBaseAddress = pCurrNode->m_bufferDesc.src.primary_picture.video_buffer_addr;
    PLANE_TRC(TRC_ID_GDP_PLUS_IT, "Context changed (new video_buffer_addr)");
  }

  m_pNodeToDisplay = pCurrNode;

  if (!isPictureRepeated || isAddrChanged)
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "presentation_time : %lld, PTS : %lld, nfields : %d",
         pCurrNode->m_bufferDesc.info.presentation_time, pCurrNode->m_bufferDesc.info.PTS, pCurrNode->m_bufferDesc.info.nfields );
    m_Statistics.PicDisplayed++;
  }
  else
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "node %p : previous picture is repeated!",(void *)m_pNodeToDisplay );
    m_Statistics.PicRepeated++;
  }

  if (!m_outputInfo.isOutputStarted)
  {
    /*Nothing to display*/
    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Nothing to display !!!" );
    return;
  }

  /*
   * The Proc Clock should be enabled BEFORE sending new configuration
   * to the GDP+ HW.
   */
  if(!m_isProcClkEnabled)
  {
    EnableProcClock();
    procClkTemporaryEnabled = true;
  }

  if (FillDisplayInfo())
  {
    /* Only called if we have new changes */
    if(IsContextChanged(pCurrNode, isPictureRepeated))
    {
      ClearContextFlags();
      ResetGdpSetup();
      if(!PrepareGdpSetup(isDisplayInterlaced))
      {
        m_GdpPlusSetup.isValid = false;
        PLANE_TRC( TRC_ID_GDP_PLUS_IT ,"PrepareGdpSetup failed !!\n");
      }
      else
      {
        /* Invalidate GDP conf so it will be generated again later */
        m_gdpConf.isValid = false;
      }
    }

    if(m_GdpPlusSetup.isValid)
    {
      if(!SetupDynamicGdpSetup())
      {
        m_GdpPlusSetup.isValid = false;
        PLANE_TRC( TRC_ID_GDP_PLUS_IT ,"SetupDynamicGdpsetup() failed !!\n");
      }
    }
  }

  CheckForUnexpectedFieldInversion(isDisplayInterlaced, isTopFieldOnDisplay);

  /* Should be called every OutputVsync */
  if(ApplyGdpSetup(isDisplayInterlaced, isTopFieldOnDisplay, vsyncTime))
  {
    /*
     * Last we build GDP Plus configuration.
     */
    if((!m_gdpConf.isValid) || (m_unexpectedFieldInversion))
    {
      gdpgqr_lld_hw_viewport_s *topNode = &m_GdpPlusSetup.topNode;
      gdpgqr_lld_hw_viewport_s *botNode = &m_GdpPlusSetup.botNode;

      PrepareConf(topNode, botNode, isTopFieldOnDisplay, isDisplayInterlaced);
      ApplyConf(&m_gdpConf);
    }

    ApplyIrrSetup();

    // Save the reference of the picture(s) presented for next VSync
    m_picturesPreparedForNextVSync.pCurNode  = pCurrNode;
    m_picturesPreparedForNextVSync.pPrevNode = 0;
    m_picturesPreparedForNextVSync.pNextNode = 0;

    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "new node %p - node is valid : %d",(void *)pCurrNode, (int)m_GdpPlusSetup.isValid );
  }

  /*
   * Keep Processing clock enabled if everything is OK to display the
   * frame otherwise we should disable it.
   */
  if(!m_gdpConf.isValid && !m_GdpPlusSetup.isValid && procClkTemporaryEnabled)
  {
    DisableProcClock();
  }

}


void CGdpPlusPlane::CheckForUnexpectedFieldInversion(bool isDisplayInterlaced,
                                                           bool isTopFieldOnDisplay)
{
  /* Clear 'm_unexpectedFieldInversion' value. */
  m_unexpectedFieldInversion = false;

  /* Get the current use case (this cannot fail) */
  m_currentDisplayUseCase = GetCurrentUseCase(m_pNodeToDisplay->m_firstFieldType, isDisplayInterlaced, isTopFieldOnDisplay);

  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "********************************************");
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Current  Use Case    : %s", GetCurrentUseCaseString(m_currentDisplayUseCase));
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Previous Use Case    : %s", GetCurrentUseCaseString(m_lastProgrammedUseCase));
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "GDP+ Configuration   : %s", (m_gdpConf.isValid & m_GdpPlusSetup.isValid) ? "Valid" : "Invalid");
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Display Polarity     : %s", isDisplayInterlaced ? "Interlaced" : "Progressive");
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Source Polarity      : %s", isTopFieldOnDisplay ? "Top" : "Bottom");
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "********************************************");

  /*
   * In interlaced mode : check if current use case is same as before.
   * If current and previous cases matches and the GDP+ hardware was
   * already programmed then hardware is no more sync'ed to correct
   * display field polarity which will end up with inverted fields on
   * display!
   */
  if((m_gdpConf.isValid) && (m_GdpPlusSetup.isValid) && (isDisplayInterlaced))
  {
    if(m_currentDisplayUseCase == m_lastProgrammedUseCase)
    {
      /*
       * Sync GDP+ hardware against current field on display by rebuilding
       * new LLD configuration.
       */
      PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Sync GDP+ hw on current display field polarity");
      m_unexpectedFieldInversion = false;
      m_Statistics.UnexpectedFieldInversions++;
    }
  }
  m_lastProgrammedUseCase = m_currentDisplayUseCase;
}

void CGdpPlusPlane::ClearContextFlags(void)
{
    /* Trigger dynamic gdp setup*/
    m_IsGainChanged                     = true;
    m_IsColorKeyChanged                 = true;
    m_IsTransparencyChanged             = true;
}

bool CGdpPlusPlane::FillDisplayInfo()
{
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "node %p",(void *)m_pNodeToDisplay );

  m_gdpDisplayInfo.Reset();

  CDisplayPlane::FillDisplayInfo(m_pNodeToDisplay, &m_gdpDisplayInfo);

  if(m_outputInfo.currentMode.mode_id == STM_TIMING_MODE_RESERVED)
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_IT, "failed! No valid display mode!" );
    return false;
  }

  m_gdpDisplayInfo.m_isDisplayInterlaced = (m_outputInfo.currentMode.mode_params.scan_type == STM_INTERLACED_SCAN);

  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "done OK" );
  return true;
}


bool CGdpPlusPlane::SetupProgressiveNode(void)
{

  m_GdpPlusSetup.info              = m_pNodeToDisplay->m_bufferDesc.info;
  m_GdpPlusSetup.nodeType          = GNODE_PROGRESSIVE;

  m_GdpPlusSetup.isValid = true;
  return true;
}

bool CGdpPlusPlane::SetupSimpleInterlacedNode(void)

{
  /*
   * This method queues either a pair of fields in an interlaced buffer or
   * a single field in that buffer using a single display node. This covers
   * a number of different use cases:
   *
   * 1. An interlaced field pair on an interlaced display, being displayed at
   *    normal speed without any intra-field interpolation. The fields will
   *    be displayed alternately for as long as the display node is valid.
   *
   * 2. A single interlaced field on an interlaced display. The specified
   *    field will get displayed on the first valid display field, usually for
   *    just one field (it is expected that another node with the next field
   *    will be queued behind it during normal speed playback). If the display
   *    node is maintained on the display for more than one field then the
   *    behaviour depends on the interpolateFields flag. If true (i.e. for
   *    slowmotion) then an interpolated other field is displayed, otherwise
   *    the data from the other field in the buffer is displayed (this is the
   *    best choice of a bad lot, because this should only happen if the
   *    queue has starved or some correction for AV sync is being applied).
   *    This use case is intented to support per-field pan and scan vectors,
   *    where the hardware setup needs to be different for each field,
   *    including the repeated first field when doing 3/2 pulldown (eeek).
   *
   * 3. A single interlaced field on a progressive display. The specified
   *    field will be displayed for as long as the display node is valid.
   *
   */
  m_GdpPlusSetup.info              = m_pNodeToDisplay->m_bufferDesc.info;
  m_GdpPlusSetup.nodeType          = m_pNodeToDisplay->m_firstFieldType;

  if(m_pNodeToDisplay->m_repeatFirstField && !m_pNodeToDisplay->m_firstFieldOnly)
      m_pNodeToDisplay->m_bufferDesc.info.nfields++;

  m_GdpPlusSetup.isValid = true;

  return true;
}

bool CGdpPlusPlane::PrepareGdpSetup(bool isDisplayInterlaced)
{
  gdpgqr_lld_hw_viewport_s    &topNode  = m_GdpPlusSetup.topNode;
  gdpgqr_lld_hw_viewport_s    &botNode  = m_GdpPlusSetup.botNode;

  if(!PrepareIOWindows(m_pNodeToDisplay, &m_gdpDisplayInfo))
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_IT,"failed !! Invalid IOWindows\n");
    return false;
  }

  if(!m_gdpDisplayInfo.m_selectedPicture.pitch)
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_IT,"failed !! Invalid pitch value\n");
    return false;
  }

  /*
   * Convert the source origin to fixed point format ready for setting up
   * the resize filters. Note that the incoming coordinates are in
   * multiples of a 16th of a pixel/scanline.
   */
  m_gdpDisplayInfo.m_srcFrameRectFixedPointX = ValToFixedPoint(m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.x, 16);
  m_gdpDisplayInfo.m_srcFrameRectFixedPointY = ValToFixedPoint(m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.y, 16);

  /* it is totally ok to dynamically modify m_ulMaxLineStep here, as the only
     place where it is used to calculate information that can be exported to
     the outside is in Create(), which has happened long ago. The max line
     step needs to correctly reflect the current pitch and is used here
     AdjustBufferInfoForScaling(). */
  m_ulMaxLineStep = GetMaxLinesSkippableByHw(m_gdpDisplayInfo.m_selectedPicture.pitch);
  if (m_gdpDisplayInfo.m_isSrcInterlaced)
    m_ulMaxLineStep /= 2;

  /*
   * Wait for Next VSync before loading the bottom node
   */
  topNode.CTL = botNode.CTL = GDP_PLUS_CTL_WAIT_NEXT_VSYNC;

  /*
   * Configure the Pixel Repeat if it is required and hardware support
   * it.
   */
  if(!setNodePixelRepeat(topNode, botNode))
    return false;

  AdjustBufferInfoForScaling(topNode, botNode, isDisplayInterlaced);

  /*
   * Note that the resize and filter setup, may set state in the nodes which
   * is inspected by the following calls to setOutputViewport and
   * setMemoryAddressing.
   */
  if(!setNodeResizeAndFilters(topNode, botNode, isDisplayInterlaced))
    return false;

  if(!setMemoryAddressing(topNode, botNode, isDisplayInterlaced))
    return false;

  if(!setOutputViewport(topNode, botNode, isDisplayInterlaced))
    return false;

  if(!setNodeColourFmt(topNode, botNode))
    return false;

  if(!setNodeColourKeys(topNode, botNode))
    return false;

  UpdateIrrSetup();

  if(!setNodeGamutMatrix(topNode, botNode))
    return false;

  if (!m_gdpDisplayInfo.m_isSrcInterlaced)
    return SetupProgressiveNode();
  else
    return SetupSimpleInterlacedNode();
}

bool CGdpPlusPlane::SetupDynamicGdpSetup(void)
{
  gdpgqr_lld_hw_viewport_s    &topNode  = m_GdpPlusSetup.topNode;
  gdpgqr_lld_hw_viewport_s    &botNode  = m_GdpPlusSetup.botNode;

  if(m_updateHDROutFormat || m_IsColorKeyChanged || m_IsTransparencyChanged || m_IsGainChanged)
  {
    /*
     * Invalidate GDP conf so it will be generated again after applying
     * The GDP setup.
     */
    m_gdpConf.isValid = false;
  }

  if(m_updateHDROutFormat)
  {
    m_updateHDROutFormat = false;
    if(!SetupHDROutFormat())
      return false;
  }

  if(m_IsColorKeyChanged || m_IsTransparencyChanged || m_IsGainChanged)
  {
    /*
     * Invalidate GDP conf so it will be generated again after applying
     * The GDP setup.
     */
    m_gdpConf.isValid = false;
  }

  if(m_IsColorKeyChanged)
  {
    m_IsColorKeyChanged = false;
    if(!setNodeColourKeys(topNode, botNode))
      return false;
  }

  if (m_IsTransparencyChanged || m_IsGainChanged)
  {
    m_IsTransparencyChanged = false;
    m_IsGainChanged         = false;
    if(!setNodeAlphaGain(topNode, botNode))
      return false;
  }

  return true;
}

// This function returns "true" if a config is written for next vsync
bool CGdpPlusPlane::ApplyGdpSetup(bool isDisplayInterlaced,
                              bool isTopFieldOnDisplay,
                              const stm_time64_t &vsyncTime)
{
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "node %p - isDisplayInterlaced = %d - isTopFieldOnDisplay %d",(void *)m_pNodeToDisplay, (int)isDisplayInterlaced, (int)isTopFieldOnDisplay);

  if(!m_GdpPlusSetup.isValid)
  {
    /* Do not update HW */
    return false;
  }

  if(isDisplayInterlaced)
  {
    /*
     * If we are being asked to present graphics on an interlaced display
     * then only allow changes on the top field. This is to prevent visual
     * artifacts when animating vertical movement.
     */
    if(isTopFieldOnDisplay && (m_GdpPlusSetup.info.ulFlags & STM_BUFFER_PRESENTATION_GRAPHICS))
    {
      EnableGdpPlusHW();
      WriteConfigForNextVsync(isDisplayInterlaced);
      return true;
    }

    if((isTopFieldOnDisplay  && m_GdpPlusSetup.nodeType == GNODE_TOP_FIELD) ||
       (!isTopFieldOnDisplay && m_GdpPlusSetup.nodeType == GNODE_BOTTOM_FIELD))
    {
      PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Waiting for correct field" );
      PLANE_TRC( TRC_ID_GDP_PLUS_IT, "isTopFieldOnDisplay = %s", isTopFieldOnDisplay?"true":"false" );
      PLANE_TRC( TRC_ID_GDP_PLUS_IT, "nodeType            = %s", (m_GdpPlusSetup.nodeType==GNODE_TOP_FIELD)?"top":"bottom" );
      return false;
    }
  }

  EnableGdpPlusHW();
  WriteConfigForNextVsync(isDisplayInterlaced);
  return true;
}

void CGdpPlusPlane::WriteConfigForNextVsync(bool isDisplayInterlaced)
{
  gdpgqr_lld_hw_viewport_s *topNode = &m_GdpPlusSetup.topNode;
  gdpgqr_lld_hw_viewport_s *botNode = &m_GdpPlusSetup.botNode;

  uint32_t                     nextflags = m_GdpPlusSetup.info.ulFlags;

  PLANE_TRCIN( TRC_ID_GDP_PLUS_IT, "" );

  if((nextflags & STM_BUFFER_PRESENTATION_DIRECT_BUFFER_ADDR) && (m_ulDirectBaseAddress == 0))
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Direct buffer address is NULL" );
    return;
  }

  if(!(nextflags & STM_BUFFER_PRESENTATION_DIRECT_BUFFER_ADDR) && (m_ulQueueBaseAddress == 0))
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Queue buffer address is NULL" );
    return;
  }

  if(nextflags & STM_BUFFER_PRESENTATION_DIRECT_BUFFER_ADDR)
  {
    /* Write the main configuration */
    m_gdpConf.conf[0].viewport.PML = topNode->PML + m_ulDirectBaseAddress;
    m_gdpConf.conf[1].viewport.PML = botNode->PML + m_ulDirectBaseAddress;
  }
  else
  {
    /* Write the main configuration */
    m_gdpConf.conf[0].viewport.PML = topNode->PML + m_ulQueueBaseAddress;
    m_gdpConf.conf[1].viewport.PML = botNode->PML + m_ulQueueBaseAddress;
  }
  PLANE_TRC( TRC_ID_GDP_PLUS_IT, "  -> top PML = %#08x bot PML = %#08x.",  m_gdpConf.conf[0].viewport.PML, m_gdpConf.conf[1].viewport.PML );

  /* Make new node visible */
  this->setNodeVisbility(topNode, false);
  this->setNodeVisbility(botNode, false);

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_IT, "" );
}


void CGdpPlusPlane::updateBaseAddress(void)
{
  gdpgqr_lld_hw_viewport_s *topNode = &m_GdpPlusSetup.topNode;
  gdpgqr_lld_hw_viewport_s *botNode = &m_GdpPlusSetup.botNode;

  PLANE_TRCIN( TRC_ID_GDP_PLUS_IT, "" );

  if(!m_GdpPlusSetup.isValid)
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "GDP Plus setup is invalid" );
    return;
  }

  if((m_GdpPlusSetup.info.ulFlags & STM_BUFFER_PRESENTATION_DIRECT_BUFFER_ADDR) && (m_ulDirectBaseAddress == 0))
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Direct buffer address is NULL" );
    return;
  }

  if(!(m_GdpPlusSetup.info.ulFlags & STM_BUFFER_PRESENTATION_DIRECT_BUFFER_ADDR) && (m_ulQueueBaseAddress == 0))
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Queue buffer address is NULL" );
    return;
  }

  uint32_t topbaseaddr;
  uint32_t botbaseaddr;

  if(m_GdpPlusSetup.info.ulFlags & STM_BUFFER_PRESENTATION_DIRECT_BUFFER_ADDR)
  {
    topbaseaddr = topNode->PML + m_ulDirectBaseAddress;
    botbaseaddr = botNode->PML + m_ulDirectBaseAddress;
  }
  else
  {
    topbaseaddr = topNode->PML + m_ulQueueBaseAddress;
    botbaseaddr = botNode->PML + m_ulQueueBaseAddress;
  }

  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Direct buffer address is 0x%x (top) and 0x%x (bot)",topbaseaddr ,botbaseaddr );

  m_gdpConf.conf[0].viewport.PML = topbaseaddr;
  m_gdpConf.conf[1].viewport.PML = botbaseaddr;

  /*
   * Apply new setting if we do have a valid configuration. Otherwise
   * new setting will be applyed during next VSync after building a new
   * valid GDP Plus configuration.
   */
  if(m_gdpConf.isValid)
  {
    /*
     * Build new Config according to current displayed field polartity
     * and the display mode polarity.
     */
    bool isDisplayInterlaced = (m_outputInfo.currentMode.mode_params.scan_type == STM_INTERLACED_SCAN);
    bool isTopFieldOnDisplay = (m_pOutput->GetCurrentVTGEvent() & STM_TIMING_EVENT_TOP_FIELD) != 0;

    PrepareConf(topNode, botNode, isTopFieldOnDisplay, isDisplayInterlaced);

    /*
     * This update shouldn't update the GDP plane visibility on Mixer.
     * It is just pushing new configuration.
     */
    UpdateConf(&m_gdpConf);
  }
  PLANE_TRCOUT( TRC_ID_GDP_PLUS_IT, "" );
}


bool CGdpPlusPlane::setNodePixelRepeat(gdpgqr_lld_hw_viewport_s       &topNode,
                                 gdpgqr_lld_hw_viewport_s       &botNode)
{
  if((m_outputInfo.currentMode.mode_params.active_area_width > 1920)
  && (m_outputInfo.currentMode.mode_params.vertical_refresh_rate > 30000)
  && (m_gdpDisplayInfo.m_dstFrameRect.width > GDPGQR_MAX_UHD_OUTPUT_WIDTH)
  && (m_lldProfile != GDPGQR_PROFILE_4KP60))
  {
    /*
     * We are going to display an HD picture on 2160@60Hz display mode
     * using one GDP hardware instance. The hardware will not be able to
     * display the picture (BW issue) unless it support the pixel repeat
     * (pixel doubling) feature.
     */
    if(m_bHasPixelRepeat)
    {
      /*
       * The Pixel Repeat feature is only available for Mixer0 which support
       * 2160@60Hz display mode.
       *
       * It consists of doubling each pixels of a plane, if the feature is
       * enabled. Each hardware pipeline has its own bitfield pixel_repeat
       * (GDPn_CTL[7]).
       *
       * The programming should be align with this feature :
       *   - the plane provides N pixels.
       *   - the Mixer displays 2*N pixels of this layer, xdo and xds should
       *     be coherent with this value.
       */
      topNode.CTL = botNode.CTL |= GDP_PLUS_CTL_PIXEL_REPEAT;
      m_gdpDisplayInfo.m_dstFrameRect.width /= 2;
      PLANE_TRC( TRC_ID_GDP_PLUS_IT, "Pixel Repeat Enabled (destination width = %d).", m_gdpDisplayInfo.m_dstFrameRect.width);
    }
    else
    {
      /* We can't display the picture through GDP plane! */
      TRC( TRC_ID_ERROR, "Can't display the picture using GDP hardware (pixel repeat NOT SUPPORTED)!" );
      return false;
    }
  }

  return true;
}


bool CGdpPlusPlane::setNodeColourFmt(gdpgqr_lld_hw_viewport_s       &topNode,
                                 gdpgqr_lld_hw_viewport_s       &botNode)
{
  uint32_t ulCtrl = 0;
  uint32_t alphaRange = (m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_LIMITED_RANGE_ALPHA)?0:GDP_PLUS_CTL_ALPHA_RANGE;

  switch(m_gdpDisplayInfo.m_selectedPicture.colorFmt)
  {
    case SURF_RGB565:
      ulCtrl = GDP_PLUS_CTL_RGB_565;
      break;

    case SURF_RGB888:
      ulCtrl = GDP_PLUS_CTL_RGB_888;
      break;

    case SURF_ARGB8565:
      ulCtrl = GDP_PLUS_CTL_ARGB_8565 | alphaRange;
      break;

    case SURF_ARGB8888:
      ulCtrl = GDP_PLUS_CTL_ARGB_8888 | alphaRange;
      break;

    case SURF_ARGB1555:
      ulCtrl = GDP_PLUS_CTL_ARGB_1555;
      break;

    case SURF_ARGB4444:
      ulCtrl = GDP_PLUS_CTL_ARGB_4444;
      break;

    case SURF_YCBCR422R:
      ulCtrl = GDP_PLUS_CTL_YCbCr422R;

      if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_COLORSPACE_709)
        ulCtrl |= GDP_PLUS_CTL_709_SELECT;

      break;

    case SURF_CRYCB888:
      ulCtrl = GDP_PLUS_CTL_YCbCr888;

      if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_COLORSPACE_709)
        ulCtrl |= GDP_PLUS_CTL_709_SELECT;

      break;

    case SURF_ACRYCB8888:
      ulCtrl = GDP_PLUS_CTL_AYCbCr8888 | alphaRange;

      if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_COLORSPACE_709)
        ulCtrl |= GDP_PLUS_CTL_709_SELECT;

      break;

    case SURF_CLUT8:
      if(m_bHasClut)
      {
        ulCtrl |= GDP_PLUS_CTL_CLUT8 | GDP_PLUS_CTL_EN_CLUT_UPDATE;
      }
      else
      {
        PLANE_TRC( TRC_ID_ERROR, "Clut not supported on hardware." );
        return false;
      }
      break;

    case SURF_BGRA8888:
      ulCtrl = GDP_PLUS_CTL_ARGB_8888 | GDP_PLUS_CTL_BIGENDIAN | alphaRange;
      break;

    default:
      PLANE_TRC( TRC_ID_ERROR, "Unknown colour format." );
      return false;
  }

  if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Setting Premultiplied Alpha." );
    ulCtrl |= GDP_PLUS_CTL_PREMULT_FORMAT;
  }

  topNode.CTL |= ulCtrl;
  botNode.CTL |= ulCtrl;

  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "Setting Clut Address = 0x%08x.", m_pNodeToDisplay->m_bufferDesc.src.clut_bus_address);
  topNode.CML  = botNode.CML = m_pNodeToDisplay->m_bufferDesc.src.clut_bus_address;

  return true;
}


bool CGdpPlusPlane::setNodeColourKeys(gdpgqr_lld_hw_viewport_s       &topNode,
                                  gdpgqr_lld_hw_viewport_s       &botNode)
{
  uint8_t ucRCR = 0;
  uint8_t ucGY  = 0;
  uint8_t ucBCB = 0;

  // GDPs do not support destination colour keying
  if((m_pNodeToDisplay->m_bufferDesc.dst.ulFlags & (STM_BUFFER_DST_COLOR_KEY | STM_BUFFER_DST_COLOR_KEY_INV)) != 0)
    return false;

  if ((m_ColorKeyState==CONTROL_OFF) && (m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_COLOR_KEY ))
  {
    FillColorKeyConfig (&m_ColorKeyConfig, &m_pNodeToDisplay->m_bufferDesc.src.ColorKey);
  }

  // If colour keying not required, do nothing.
  if (m_ColorKeyConfig.enable == 0)
  {
    // Disable CKEY before going on
    topNode.CTL &= ~(GDP_PLUS_CTL_EN_COLOR_KEY    |
                     GDP_PLUS_CTL_RCR_COL_KEY_1   |
                     GDP_PLUS_CTL_RCR_COL_KEY_3   |
                     GDP_PLUS_CTL_GY_COL_KEY_1    |
                     GDP_PLUS_CTL_GY_COL_KEY_3    |
                     GDP_PLUS_CTL_BCB_COL_KEY_1   |
                     GDP_PLUS_CTL_BCB_COL_KEY_3);
    botNode.CTL &= ~(GDP_PLUS_CTL_EN_COLOR_KEY    |
                     GDP_PLUS_CTL_RCR_COL_KEY_1   |
                     GDP_PLUS_CTL_RCR_COL_KEY_3   |
                     GDP_PLUS_CTL_GY_COL_KEY_1    |
                     GDP_PLUS_CTL_GY_COL_KEY_3    |
                     GDP_PLUS_CTL_BCB_COL_KEY_1   |
                     GDP_PLUS_CTL_BCB_COL_KEY_3);
    botNode.KEY1 = topNode.KEY1 = 0;
    botNode.KEY2 = topNode.KEY2 = 0;
    return true;
  }

  //Get Min Key value
  if (!(m_ColorKeyConfig.flags & SCKCF_MINVAL)
      || !GetRGBYCbCrKey (ucRCR, ucGY, ucBCB, m_ColorKeyConfig.minval,
                          m_gdpDisplayInfo.m_selectedPicture.colorFmt,
                          m_ColorKeyConfig.format == SCKCVF_RGB))
  {
    PLANE_TRC( TRC_ID_ERROR, "Min key value not obtained." );
    return false;
  }
  botNode.KEY1 = topNode.KEY1 = (ucBCB | (ucGY<<8) | (ucRCR<<16));

  //Get Max Key value
  if (!(m_ColorKeyConfig.flags & SCKCF_MAXVAL)
      || !GetRGBYCbCrKey (ucRCR, ucGY, ucBCB, m_ColorKeyConfig.maxval,
                          m_gdpDisplayInfo.m_selectedPicture.colorFmt,
                          m_ColorKeyConfig.format == SCKCVF_RGB))
  {
    PLANE_TRC( TRC_ID_ERROR, "Max key value not obtained" );
    return false;
  }
  botNode.KEY2 = topNode.KEY2 = (ucBCB | (ucGY<<8) | (ucRCR<<16));

  uint32_t ulCtrl = GDP_PLUS_CTL_EN_COLOR_KEY;

  switch (m_ColorKeyConfig.r_info) {
  case SCKCCM_DISABLED: break;
  case SCKCCM_ENABLED: ulCtrl |= GDP_PLUS_CTL_RCR_COL_KEY_1; break;
  case SCKCCM_INVERSE: ulCtrl |= GDP_PLUS_CTL_RCR_COL_KEY_3; break;
  default: return false;
  }

  switch (m_ColorKeyConfig.g_info) {
  case SCKCCM_DISABLED: break;
  case SCKCCM_ENABLED: ulCtrl |= GDP_PLUS_CTL_GY_COL_KEY_1; break;
  case SCKCCM_INVERSE: ulCtrl |= GDP_PLUS_CTL_GY_COL_KEY_3; break;
  default: return false;
  }

  switch (m_ColorKeyConfig.b_info) {
  case SCKCCM_DISABLED: break;
  case SCKCCM_ENABLED: ulCtrl |= GDP_PLUS_CTL_BCB_COL_KEY_1; break;
  case SCKCCM_INVERSE: ulCtrl |= GDP_PLUS_CTL_BCB_COL_KEY_3; break;
  default: return false;
  }

  topNode.CTL |= ulCtrl;
  botNode.CTL |= ulCtrl;

  return true;
}


int CGdpPlusPlane::ScaleVerticalSamplePosition(int pos, CDisplayInfo DisplayInfo)
{
  /*
   * This function scales a source vertical sample position by the true
   * vertical scale factor between source and destination, ignoring any
   * scaling to convert between interlaced and progressive sources and
   * destinations. It is used to work out the starting sample position for
   * the first line on the destination, particularly for the start of a
   * bottom display field.
   */
  return (pos * DisplayInfo.m_selectedPicture.srcFrameRect.height) / DisplayInfo.m_dstFrameRect.height;
}


bool CGdpPlusPlane::setNodeResizeAndFilters(gdpgqr_lld_hw_viewport_s       &topNode,
                                              gdpgqr_lld_hw_viewport_s       &botNode,
                                              bool isDisplayInterlaced)
{
  uint32_t        ulCtrl  = 0;
  const uint32_t *fluma   = 0;
  const uint32_t *fchroma = 0;
  stm_rect_t      inputRectangle;         /* InputRectangle  in Frame notation */
  stm_rect_t      outputRectangle;        /* OutputRectangle in Frame notation */
  uint32_t        chromaInputWidth  = 0;
  uint32_t        chromaInputHeight = 0;
  uint8_t         luma_normalization   = 8;
  uint8_t         chroma_normalization = 8;
  uint8_t         DisableAlphaChannel  = 1;
  uint8_t         HorAlphaThreshold    = GDP_PLUS_HVP1_A_THR_DISABLE;
  uint8_t         VerAlphaThreshold    = GDP_PLUS_HVP1_A_THR_DISABLE;

  inputRectangle  = m_gdpDisplayInfo.m_selectedPicture.srcFrameRect;
  outputRectangle = m_gdpDisplayInfo.m_dstFrameRect;

  /* Calculate input chroma width base on the color format */
  if(m_gdpDisplayInfo.m_selectedPicture.isSrc420)
  {
    chromaInputWidth  = inputRectangle.width / 2;
    chromaInputHeight = inputRectangle.height / 2;
  }
  else if(m_gdpDisplayInfo.m_selectedPicture.isSrc422)
  {

    chromaInputWidth  = inputRectangle.width / 2;
    chromaInputHeight = inputRectangle.height;
  }
  else
  {
    chromaInputWidth  = inputRectangle.width;
    chromaInputHeight = inputRectangle.height;
  }

  /* The general Overshooting and Anti-Ringing tuning is as follow:
   *
   * * For Luma and Alpha (Y/A) :
   *   - No scaling   : kring default, kovs disable, alpha disabled
   *   - Down scaling : kring max, kovs disable, alpha disabled
   *   - Up scaling   : - kring max, kovs disable, alpha default (for zoom > 2)
   *                    - kring max, kovs max, alpha disabled (for zoom <= 2)
   *
   * * For Chroma (UV) :
   *   - output = chromasize : kring default, kovs disable
   *   - output < chromasize : kring disable, kovs disable
   *   - output > chromasize : - kring max, kovs disable, alpha default (for zoom > 2)
   *                           - kring max, kovs max, alpha disabled (for zoom <= 2)
   */

  /*
   * Luma Settings
   */
  if(outputRectangle.width == inputRectangle.width)
  {
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_Y_KRING,  GDP_PLUS_HVP1_Y_KRING_DEFAULT);
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_Y_KOVS,   GDP_PLUS_HVP1_Y_KOVS_DISABLE );
  }
  else if(outputRectangle.width < inputRectangle.width)
  {
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_Y_KRING,  GDP_PLUS_HVP1_Y_KRING_MAX);
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_Y_KOVS,   GDP_PLUS_HVP1_Y_KOVS_DISABLE );
  }
  else
  {
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_Y_KRING,  GDP_PLUS_HVP1_Y_KRING_MAX);
    if(m_gdpDisplayInfo.m_hsrcinc < ZOOMx2) /* (zoom > 2) */
    {
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_Y_KOVS, GDP_PLUS_HVP1_Y_KOVS_DISABLE);
    }
    else /* (zoom <= 2) */
    {
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_Y_KOVS, GDP_PLUS_HVP1_Y_KOVS_MAX);
    }
  }

  if(outputRectangle.height == inputRectangle.height)
  {
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_Y_KRING,  GDP_PLUS_HVP1_Y_KRING_DEFAULT);
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_Y_KOVS,   GDP_PLUS_HVP1_Y_KOVS_DISABLE );
  }
  else if(outputRectangle.height < inputRectangle.height)
  {
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_Y_KRING,  GDP_PLUS_HVP1_Y_KRING_MAX);
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_Y_KOVS,   GDP_PLUS_HVP1_Y_KOVS_DISABLE );
  }
  else
  {
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_Y_KRING,  GDP_PLUS_HVP1_Y_KRING_MAX);
    if(m_gdpDisplayInfo.m_vsrcinc < ZOOMx2) /* (zoom > 2) */
    {
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_Y_KOVS, GDP_PLUS_HVP1_Y_KOVS_DISABLE);
    }
    else /* (zoom <= 2) */
    {
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_Y_KOVS, GDP_PLUS_HVP1_Y_KOVS_MAX);
    }
  }

  /*
   * Chroma Settings
   */
  if(outputRectangle.width == chromaInputWidth)
  {
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_UV_KRING,  GDP_PLUS_HVP1_UV_KRING_DEFAULT);
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_UV_KOVS,   GDP_PLUS_HVP1_UV_KOVS_DISABLE );
  }
  else if(outputRectangle.width < chromaInputWidth)
  {
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_UV_KRING,  GDP_PLUS_HVP1_UV_KRING_MAX);
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_UV_KOVS,   GDP_PLUS_HVP1_UV_KOVS_DISABLE );
  }
  else
  {
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_UV_KRING,  GDP_PLUS_HVP1_UV_KRING_MAX);
    if(m_gdpDisplayInfo.m_hsrcinc < ZOOMx2) /* (zoom > 2) */
    {
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_UV_KOVS, GDP_PLUS_HVP1_UV_KOVS_DISABLE);
    }
    else /* (zoom <= 2) */
    {
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_UV_KOVS, GDP_PLUS_HVP1_UV_KOVS_MAX);
    }
  }

  if(outputRectangle.height == chromaInputHeight)
  {
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_UV_KRING,  GDP_PLUS_HVP1_UV_KRING_DEFAULT);
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_UV_KOVS,   GDP_PLUS_HVP1_UV_KOVS_DISABLE );
  }
  else if(outputRectangle.height < chromaInputHeight)
  {
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_UV_KRING,  GDP_PLUS_HVP1_UV_KRING_MAX);
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_UV_KOVS,   GDP_PLUS_HVP1_UV_KOVS_DISABLE );
  }
  else
  {
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_UV_KRING,  GDP_PLUS_HVP1_UV_KRING_MAX);
    if(m_gdpDisplayInfo.m_vsrcinc < ZOOMx2) /* (zoom > 2) */
    {
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_UV_KOVS, GDP_PLUS_HVP1_UV_KOVS_DISABLE);
    }
    else /* (zoom <= 2) */
    {
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_UV_KOVS, GDP_PLUS_HVP1_UV_KOVS_MAX);
    }
  }

  /*
   * Alpha Settings
   */
  if((m_gdpDisplayInfo.m_selectedPicture.colorFmt == SURF_ARGB8565) ||
     (m_gdpDisplayInfo.m_selectedPicture.colorFmt == SURF_ARGB8888) ||
     (m_gdpDisplayInfo.m_selectedPicture.colorFmt == SURF_ARGB1555) ||
     (m_gdpDisplayInfo.m_selectedPicture.colorFmt == SURF_ARGB4444) ||
     (m_gdpDisplayInfo.m_selectedPicture.colorFmt == SURF_ACRYCB8888))
  {
    DisableAlphaChannel = 0;

    if(outputRectangle.width == inputRectangle.width)
    {
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_DEFAULT);
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KOVS,   GDP_PLUS_HVP1_A_KOVS_DISABLE );
    }
    else if(outputRectangle.width < inputRectangle.width)
    {
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_MAX);
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KOVS,   GDP_PLUS_HVP1_A_KOVS_DISABLE );
    }
    else
    {
      GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_MAX);
      if(m_gdpDisplayInfo.m_hsrcinc < ZOOMx2) /* (zoom > 2) */
      {
        GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KOVS, GDP_PLUS_HVP1_A_KOVS_DISABLE);
        HorAlphaThreshold = GDP_PLUS_HVP1_A_THR_DEFAULT;
      }
      else /* (zoom <= 2) */
      {
        GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KOVS, GDP_PLUS_HVP1_A_KOVS_MAX);
      }
    }

    if(outputRectangle.height == inputRectangle.height)
    {
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_DEFAULT);
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KOVS,   GDP_PLUS_HVP1_A_KOVS_DISABLE );
    }
    else if(outputRectangle.height < inputRectangle.height)
    {
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_MAX);
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KOVS,   GDP_PLUS_HVP1_A_KOVS_DISABLE );
    }
    else
    {
      GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_MAX);
      if(m_gdpDisplayInfo.m_vsrcinc < ZOOMx2) /* (zoom > 2) */
      {
        GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KOVS, GDP_PLUS_HVP1_A_KOVS_DISABLE);
        VerAlphaThreshold = GDP_PLUS_HVP1_A_THR_DEFAULT;
      }
      else /* (zoom <= 2) */
      {
        GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KOVS, GDP_PLUS_HVP1_A_KOVS_MAX);
      }
    }
  }
  else
  {
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_DISABLE);
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KRING,  GDP_PLUS_HVP1_A_KRING_DISABLE);
    GdpPlusSetBitField(topNode.HP1, GDP_PLUS_HVP1_A_KOVS,   GDP_PLUS_HVP1_A_KOVS_DISABLE );
    GdpPlusSetBitField(topNode.VP1, GDP_PLUS_HVP1_A_KOVS,   GDP_PLUS_HVP1_A_KOVS_DISABLE );
  }
  botNode.HP1 = topNode.HP1;
  botNode.VP1 = topNode.VP1;

  if(m_gdpDisplayInfo.m_hsrcinc != (uint32_t)m_fixedpointONE)
  {
    int subpixelpos = 0;
    int32_t horz_phase = 0;

    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "H Resize Enabled (HSRC 4.13: %x)", m_gdpDisplayInfo.m_hsrcinc);

    topNode.HSRC
      = botNode.HSRC
      = m_gdpDisplayInfo.m_hsrcinc;

    ulCtrl |= GDP_PLUS_CTL_EN_H_RESIZE;

    /* Horizontal filter setup is reasonably simple and is the same for
     * both fields when dealing with the source as two interlaced fields.
     *
     * First of all get the start position in fixed point format.
     */
    FixedPointToInteger( m_gdpDisplayInfo.m_srcFrameRectFixedPointX, &subpixelpos );

    /* Second get the horizontal phase in 4.13 format. */
    horz_phase = ( ( subpixelpos * m_fixedpointONE ) & GDP_PLUS_HIP_INIT_PHASE_MASK );

    topNode.HIP
      = botNode.HIP
      = (horz_phase & GDP_PLUS_HIP_INIT_PHASE_MASK);

    /* Last calculate horizontal luma and chroma filters. */
    fluma   = m_Filter.SelectHorizontalLumaFilter( m_gdpDisplayInfo.m_hsrcinc, horz_phase, &luma_normalization );
    fchroma = m_Filter.SelectHorizontalChromaFilter( m_gdpDisplayInfo.m_hsrcinc, horz_phase, &chroma_normalization );

    /*
     * Do not update filters if context doesn't changed.
     *
     * Since the LLD isn't keeping reference to previous valid filtering
     * table than we should be filling all the time tables in the GDP+
     * configuration.
     */
    //if((m_GdpPlusSetup.hfluma != fluma) || (m_GdpPlusSetup.hfchroma != fchroma))
    {
      m_GdpPlusSetup.hfluma    = fluma;
      m_GdpPlusSetup.hfchroma  = fchroma;
      fluma                     = 0;
      fchroma                   = 0;

      // Copy the required filter coefficients to the hardware filter tables
      for (uint32_t i=0; i < GDPGQR_NBCOEF_H; i++)
      {
        topNode.HFCAY[i] = botNode.HFCAY[i] = *(m_GdpPlusSetup.hfluma+i);
        topNode.HFCUV[i] = botNode.HFCUV[i] = *(m_GdpPlusSetup.hfchroma+i);
      }
    }

    topNode.HP2
    = botNode.HP2
    = (HorAlphaThreshold << GDP_PLUS_HVP2_ATHR_SHIFT)           |
      (DisableAlphaChannel << GDP_PLUS_HVP2_DIS_ALPHA_SHIFT)    |
      (luma_normalization   << GDP_PLUS_HVP2_MAXAY_SHIFT)       |
      (chroma_normalization << GDP_PLUS_HVP2_MAXUV_SHIFT);

    ulCtrl |= GDP_PLUS_CTL_EN_HFILTER_UPD;
  }

  if((m_gdpDisplayInfo.m_vsrcinc != (uint32_t)m_fixedpointONE)
     || (!m_gdpDisplayInfo.m_isSrcInterlaced &&  isDisplayInterlaced))
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "V Resize Enabled (VSRC 4.13: %x)", m_gdpDisplayInfo.m_vsrcinc);

    topNode.VSRC
      = botNode.VSRC
      = m_gdpDisplayInfo.m_vsrcinc;

    ulCtrl |= GDP_PLUS_CTL_EN_V_RESIZE;

    if(isDisplayInterlaced && !m_gdpDisplayInfo.m_isSrcInterlaced)
    {
      /*
      * When putting progressive content on an interlaced display
      * we adjust the filter phase of the bottom field to start
      * an appropriate distance lower in the source bitmap, based on the
      * scaling factor. If the scale means that the bottom field
      * starts >1 source bitmap line lower then this will get dealt
      * with in the memory setup by adjusting the source bitmap address.
      */
      uint32_t bottomfieldinc   = ScaleVerticalSamplePosition(m_fixedpointONE,
                                                             m_gdpDisplayInfo);
      uint32_t integerinc       = bottomfieldinc / m_fixedpointONE;
      uint32_t fractionalinc    = bottomfieldinc - (integerinc
                                                   * m_fixedpointONE);
      uint32_t bottomfieldphase = 256 * ((((fractionalinc * 32) + (m_fixedpointONE / 2))
                                  / m_fixedpointONE) % 32);

      PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "V Initial Phase  (VIP  5.8 : %x  - %lld.%02lld)", bottomfieldphase, INT_VALf8(bottomfieldphase));
      botNode.VIP |= (bottomfieldphase & GDP_PLUS_VIP_INIT_PHASE_MASK);
    }

    if(m_bHasVFilter)
    {
      const int32_t vert_phase = ( botNode.VIP & GDP_PLUS_VIP_INIT_PHASE_MASK );

      fluma   = m_Filter.SelectVerticalLumaFilter( m_gdpDisplayInfo.m_vsrcinc, vert_phase, &luma_normalization );
      fchroma = m_Filter.SelectVerticalChromaFilter( m_gdpDisplayInfo.m_vsrcinc, vert_phase, &chroma_normalization );

      /*
       * Do not update filters if context doesn't changed.
       *
       * Since the LLD isn't keeping reference to previous valid filtering
       * table than we should be filling all the time tables in the GDP+
       * configuration.
       */
      //if((m_GdpPlusSetup.vfluma != fluma) || (m_GdpPlusSetup.vfchroma != fchroma))
      {
        m_GdpPlusSetup.vfluma    = fluma;
        m_GdpPlusSetup.vfchroma  = fchroma;

        // Copy the required filter coefficients to the hardware filter tables
        for (uint32_t i=0; i < GDPGQR_NBCOEF_V; i++)
        {
          topNode.VFCAY[i] = botNode.VFCAY[i] = *(m_GdpPlusSetup.vfluma+i);
          topNode.VFCUV[i] = botNode.VFCUV[i] = *(m_GdpPlusSetup.vfchroma+i);
        }
      }

      topNode.VP2
      = botNode.VP2
      = (VerAlphaThreshold << GDP_PLUS_HVP2_ATHR_SHIFT)           |
        (DisableAlphaChannel << GDP_PLUS_HVP2_DIS_ALPHA_SHIFT)    |
        (luma_normalization   << GDP_PLUS_HVP2_MAXAY_SHIFT)       |
        (chroma_normalization << GDP_PLUS_HVP2_MAXUV_SHIFT);

      ulCtrl |= GDP_PLUS_CTL_EN_VFILTER_UPD;
    }
  }

  topNode.CTL |= ulCtrl;
  botNode.CTL |= ulCtrl;

  return true;
}


bool CGdpPlusPlane::setOutputViewport(gdpgqr_lld_hw_viewport_s    &topNode,
                                  gdpgqr_lld_hw_viewport_s    &botNode,
                                  bool isDisplayInterlaced)
{
  uint32_t horizontalInputSamples = (m_gdpDisplayInfo.m_horizontalFilterOutputSamples * m_gdpDisplayInfo.m_hsrcinc) / m_fixedpointONE;
  uint32_t xds                    = m_gdpDisplayInfo.m_viewport.stopPixel;
  uint32_t yds                    = m_gdpDisplayInfo.m_viewport.stopLine;

  /*
   * The number of pixels per line to read is recalculated from the number of
   * output samples required to fill the viewport, which may have been clipped
   * by the active video area.
   */
  if(horizontalInputSamples > (uint32_t)m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.width)
    horizontalInputSamples = m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.width;

  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "H input samples = %u V input samples = %u",horizontalInputSamples, m_gdpDisplayInfo.m_verticalFilterInputSamples );

  if(topNode.CTL & GDP_PLUS_CTL_PIXEL_REPEAT)
  {
    uint32_t xdo = m_gdpDisplayInfo.m_viewport.startPixel;
    xds = (((xds - xdo + 1) * 2) + xdo - 1);
  }

  /*
   * Set the destination viewport, which is in terms of frame line numbering,
   * regardless of interlaced/progressive and pixel repeat enabled/disabled.
   */
  topNode.VPO = PackRegister(m_gdpDisplayInfo.m_viewport.startLine, m_gdpDisplayInfo.m_viewport.startPixel);
  botNode.VPO = topNode.VPO;

  topNode.VPS = PackRegister(yds, xds);
  botNode.VPS = topNode.VPS;

  topNode.SIZE = PackRegister(m_gdpDisplayInfo.m_verticalFilterInputSamples, horizontalInputSamples);
  botNode.SIZE = topNode.SIZE;

  /*
   * Actually skip CROP setting. This configuration will be relevant
   * when supporting Dual GDP planes.
   */
  topNode.CROP = 0;
  botNode.CROP = 0;

  return true;
}

bool CGdpPlusPlane::SetupHDROutFormat(void)
{
  if(m_capabilities & PLANE_CAPS_HDR_FORMAT)
  {
    PLANE_TRC(TRC_ID_ERROR, "IP/SOC/Product specific HDROut Format method not implemented!");
  }
  else
  {
    PLANE_TRC( TRC_ID_ERROR, "HDR Output Format is NOT supported!");
  }

  return false;
}


bool CGdpPlusPlane::setMemoryAddressing(gdpgqr_lld_hw_viewport_s       &topNode,
                                    gdpgqr_lld_hw_viewport_s       &botNode,
                                    bool isDisplayInterlaced)
{
  uint32_t pitch = m_gdpDisplayInfo.m_selectedPicture.srcFramePitch;

  topNode.PMP = pitch * m_gdpDisplayInfo.m_line_step;
  if(m_gdpDisplayInfo.m_isSrcInterlaced)
  {
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> interlaced source" );
    topNode.PMP *= 2;
  }
  else
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> progressive source" );

  botNode.PMP = topNode.PMP;

  // Set up the pixmap memory
  uint32_t ulXStart        = m_gdpDisplayInfo.m_srcFrameRectFixedPointX/m_fixedpointONE;
  uint32_t ulYStart        = m_gdpDisplayInfo.m_srcFrameRectFixedPointY/m_fixedpointONE;
  uint32_t ulBytesPerPixel = m_gdpDisplayInfo.m_selectedPicture.pixelDepth>>3;
  uint32_t ulScanLine      = topNode.PMP * ulYStart;

  topNode.PML = ulScanLine + (ulBytesPerPixel * ulXStart);

  if(m_gdpDisplayInfo.m_isSrcInterlaced)
  {
    /*
     * When accessing the buffer content as interlaced fields the bottom
     * field start pointer must be one line down the buffer.
     */
    botNode.PML = topNode.PML + pitch;
  }
  else if(isDisplayInterlaced)
  {
    /*
     * Progressive on interlaced display, the start position of the
     * bottom field depends on the scaling factor involved. Note that
     * we round to the next line if the sample position is in the last 1/16th.
     * The filter phase (which has 8 positions) will have been rounded up
     * and then wrapped to 0 in the filter setup.
     *
     * Note: this could potentially cause us to read beyond the end of the
     * image buffer.
     */
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> interlaced output (height src %u -> dst %u)", m_gdpDisplayInfo.m_selectedPicture.srcFrameRect.height, m_gdpDisplayInfo.m_dstHeight);
    uint32_t linestobottomfield = ((ScaleVerticalSamplePosition(m_fixedpointONE,
                                                                m_gdpDisplayInfo)
                                    + (m_fixedpointONE / 16))
                                    / m_fixedpointONE);
    PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> linestobottomfield %u", linestobottomfield );
    botNode.PML = topNode.PML + (pitch*linestobottomfield);
  }
  else
  {
     /*
       * Progressive content on a progressive display has the same start pointer
       * for both nodes, i.e. the content is repeated exactly.
       */
      PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> progressive output" );
      botNode.PML = topNode.PML;
  }

  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> top PML = %#08x bot PML = %#08x.",  topNode.PML, botNode.PML );

  return true;
}


bool CGdpPlusPlane::setNodeGamutMatrix(gdpgqr_lld_hw_viewport_s       &topNode,
                                 gdpgqr_lld_hw_viewport_s       &botNode)
{
  if((m_pGamutMatrix) && (m_pOutput))
  {
    ClearColorimetryConversionStatistics();

    // Set color space conversion table based on frame vs. output color space
    // If no color space conversion is needed then Gamut conversion is bypassed.
    // NB: BT601 color space is handled as BT709 color space
    switch(m_outputInfo.outputColorSpace)
    {
      case STM_YCBCR_COLORSPACE_601:
      case STM_YCBCR_COLORSPACE_709:
      {
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_COLORSPACE_BT2020)
        {
          /*
           * Frame is BT2020 and output is BT601 or BT709, conversion needed
           * but hardware doesn't support it!
           */
          PLANE_TRC( TRC_ID_ERROR, "Color space conversion BT.2020 -> BT.709/BT.601 not supported!" );
          return false;
        }
        else
        {
          /*
           * Both frame and output are BT601 or BT709, no conversion needed.
           * Gamut conversion is bypassed.
           */
          PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> Gamut bypassed" );
          topNode.BT4 = botNode.BT4 = 0;
        }
      }
      break;

      case STM_COLORSPACE_BT2020:
      {
        if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_COLORSPACE_BT2020)
        {
          /*
           * Both frame and output are BT2020, no conversion needed.
           * Gamut conversion is bypassed.
           */
          PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "  -> Gamut bypassed" );
          topNode.BT4 = botNode.BT4 = 0;
        }
        else
        {
          /*
           * Frame is BT601 or BT709 and output is BT2020, conversion needed.
           */
          PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "   -> Gamut conversion BT.601/BT.709 -> BT.2020" );
          topNode.BT0 = botNode.BT0 = m_pGamutMatrix[0];
          topNode.BT1 = botNode.BT1 = m_pGamutMatrix[1];
          topNode.BT2 = botNode.BT2 = m_pGamutMatrix[2];
          topNode.BT3 = botNode.BT3 = m_pGamutMatrix[3];
          topNode.BT4 = botNode.BT4 = m_pGamutMatrix[4] | GDP_PLUS_BT4_BT2020_EN;
        }
      }
      break;
      default:
      {
        PLANE_TRC( TRC_ID_ERROR, "Unsupported output color space %d", m_outputInfo.outputColorSpace);
        return false;
      }
    }
    UpdateColorimetryConversionStatistics(m_pNodeToDisplay);
  }

  return true;
}


bool CGdpPlusPlane::setNodeAlphaGain(gdpgqr_lld_hw_viewport_s       &topNode,
                                 gdpgqr_lld_hw_viewport_s       &botNode)
{
  uint32_t transparency   = STM_PLANE_TRANSPARENCY_OPAQUE;
  uint32_t ulAlpha        = ((m_Transparency + 1) >> 1) & 0xFF;
  uint32_t staticgain     = (m_ulGain+1)/2; // convert 0-255 to 0-128 (note not 127)
  uint32_t ulGain         = 0;
  uint32_t ulOffset       = 0;
  uint32_t ulAGC          = 0;
  bool     bIsVideoRange  = !!(m_pNodeToDisplay->m_bufferDesc.dst.ulFlags & STM_BUFFER_DST_RESCALE_TO_VIDEO_RANGE);

  if (m_TransparencyState==CONTROL_ON)
  {
    transparency = m_Transparency;
  }
  else
  {
    if(m_pNodeToDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_CONST_ALPHA )
    {
      transparency = m_pNodeToDisplay->m_bufferDesc.src.ulConstAlpha;
    }
  }

  if(transparency == 0)
  {
    /* The GDP+ plane isn't going to be used for GFX display as it will be
     * fully transparent. In that case we can completly stop the hardware
     * For the moment there is no VBI plane being using the hardware.
     */
    return false;
  }

  // The input range is 0-255, this must be scaled to 0-128 for the hardware
  ulAlpha = ((transparency + 1) >> 1) & 0xFF;

  if(m_gdpDisplayInfo.m_selectedPicture.colorFmt == SURF_ARGB1555)
  {
    /*
     * Scale the alpha ramp map by the requested buffer alpha
     */
    ulAGC |= ((m_ulStaticAlpha[0] * ulAlpha)  + 63)/AGC_FULL_RANGE_GAIN;
    ulAGC |= (((m_ulStaticAlpha[1] * ulAlpha) + 63)/AGC_FULL_RANGE_GAIN)<<8;
  }
  else
  {
    ulAGC |= ulAlpha;
  }

  // HDR is restricted for Video Range
  if(m_outputInfo.outputHDRFormat.eotf_type == STM_EOTF_GAMMA_HDR)
  {
    bIsVideoRange = true;
  }

  if (bIsVideoRange)
  {
    ulGain    = ((AGC_VIDEO_GAIN * staticgain) + 63) / 128;
    ulOffset  = AGC_CONSTANT_BLACK_LEVEL;
  }
  else
  {
    ulGain    = ((AGC_FULL_RANGE_GAIN * staticgain) + 63)/ 128;
    ulOffset  = 0;
  }

  ulGain    = ((m_ulHDRGain * ulGain) + 63)/ 128;
  ulOffset  = m_ulHDROffset + ulOffset;

  ulAGC |= (ulGain   << GDP_PLUS_AGC_GAIN_SHIFT);
  ulAGC |= (ulOffset << GDP_PLUS_AGC_CONSTANT_SHIFT);

  topNode.AGC = botNode.AGC = ulAGC;

  return true;
}


DisplayPlaneResults CGdpPlusPlane::SetControl(stm_display_plane_control_t control, uint32_t value)
{
  DisplayPlaneResults result = STM_PLANE_OK;

  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "control = %d ulNewVal = %u (0x%08x)",(int)control,value,value );

  switch(control)
  {
    case PLANE_CTRL_GLOBAL_GAIN_VALUE:
      if(value>255)
        return STM_PLANE_INVALID_VALUE;
      m_pDisplayDevice->VSyncLock();
      m_ulGain = value;
      m_IsGainChanged = true;
      m_pDisplayDevice->VSyncUnlock();
      break;

    case PLANE_CTRL_ALPHA_RAMP:
      m_pDisplayDevice->VSyncLock();
      m_ulAlphaRamp = value;
      m_ulStaticAlpha[0] = ((m_ulAlphaRamp & 0xff)+1)/2;
      m_ulStaticAlpha[1] = (((m_ulAlphaRamp>>8) & 0xff)+1)/2;
      m_IsTransparencyChanged = true;
      m_pDisplayDevice->VSyncUnlock();
      break;

    /*
     * This Plane control is going to be removed from next releases!
     */
    case PLANE_CTRL_COLOR_KEY:
      {
        const stm_color_key_config_t * const config = reinterpret_cast<stm_color_key_config_t *> (value);
        if (!config)
          return STM_PLANE_INVALID_VALUE;
        FillColorKeyConfig (&m_ColorKeyConfig, config);
        m_IsColorKeyChanged = true;
        PLANE_TRC(TRC_ID_ERROR, "Obsolete Plane's control! Use 'PLANE_CTRL_SRC_COLOR_XXXX' instead of this one!");
      }
      break;

    case PLANE_CTRL_BUFFER_ADDRESS:
      if(m_ulDirectBaseAddress != value)
      {
        m_pDisplayDevice->VSyncLock();
        m_ulDirectBaseAddress = value;
        updateBaseAddress();
        m_pDisplayDevice->VSyncUnlock();
      }
      break;

    case PLANE_CTRL_FILTER_SET :
      if (!m_Filter.SelectFilterSet( (stm_plane_filter_set_t)value ))
      {
        PLANE_TRC( TRC_ID_ERROR, "IOW control=PLANE_CTRL_FILTER_SET err=NO MEMORY" );
        result = STM_PLANE_NO_MEMORY;
      }
      else
      {
        m_pDisplayDevice->VSyncLock();
        m_ContextChanged = true; // to force context change
        m_pDisplayDevice->VSyncUnlock();
      }
      break;

    default:
      return CDisplayPlane::SetControl(control,value);
  }

  return result;
}


DisplayPlaneResults CGdpPlusPlane::GetControl(stm_display_plane_control_t control, uint32_t *value) const
{
  PLANE_TRC( TRC_ID_GDP_PLUS_PLANE, "control = %d",(int)control );

  switch(control)
  {
    case PLANE_CTRL_TRANSPARENCY_VALUE:
      *value = m_Transparency;
      break;
    case PLANE_CTRL_GLOBAL_GAIN_VALUE:
      *value = m_ulGain;
      break;

    case PLANE_CTRL_ALPHA_RAMP:
      *value = m_ulAlphaRamp;
      break;

    /*
     * This Plane control is going to be removed from next releases!
     */
    case PLANE_CTRL_COLOR_KEY:
      {
        stm_color_key_config_t * const config = reinterpret_cast<stm_color_key_config_t *> (value);
        if (!config)
          return STM_PLANE_INVALID_VALUE;
        FillColorKeyConfig (config, &m_ColorKeyConfig);
        PLANE_TRC(TRC_ID_ERROR, "Obsolete Plane's control! Use 'PLANE_CTRL_SRC_COLOR_XXXX' instead of this one!");
      }
      break;

    case PLANE_CTRL_BUFFER_ADDRESS:
      *value = m_ulDirectBaseAddress;
      break;

    default:
      return CDisplayPlane::GetControl(control,value);
  }

  return STM_PLANE_OK;
}

DisplayPlaneResults CGdpPlusPlane::SetCompoundControl(stm_display_plane_control_t ctrl, void * newVal)
{
  DisplayPlaneResults result = STM_PLANE_NOT_SUPPORTED;
  switch(ctrl)
  {
    case PLANE_CTRL_HDR_GAIN_VALUE:
        m_pDisplayDevice->VSyncLock();
        FillHDRGainOffset(&m_HDRGainOffset, (stm_hdr_gain_offset_t *)newVal);
        m_updateHDROutFormat= true;
        m_pDisplayDevice->VSyncUnlock();

        PLANE_TRC(TRC_ID_CONTEXT_CHANGE, "HDR gain & offset changed");
        result = STM_PLANE_OK;
        break;

    default:
        result = CDisplayPlane::SetCompoundControl(ctrl,newVal);
  }

  return result;
}

DisplayPlaneResults CGdpPlusPlane::GetCompoundControl(stm_display_plane_control_t ctrl, void * currentVal)
{
  DisplayPlaneResults result = STM_PLANE_NOT_SUPPORTED;

  switch(ctrl)
  {
    case PLANE_CTRL_HDR_GAIN_VALUE:
        FillHDRGainOffset((stm_hdr_gain_offset_t *)currentVal, &m_HDRGainOffset);
        result = STM_PLANE_OK;
        break;

    default:
        result = CDisplayPlane::GetCompoundControl(ctrl,currentVal);
  }

  return result;
}

void CGdpPlusPlane::EnableGdpPlusHW(void)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_IT, "" );

  /*
   * It is up to the LLD part to manage the NVN regsiter writing
   * hence the enable and disable of the hardware!
   *
   * The LLD at this time should be running dummy nodes then within
   * next set_conf call it will update the configuration.
   */

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_IT, "" );
}


void CGdpPlusPlane::DisableHW(void)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  PLANE_TRC(TRC_ID_MAIN_INFO, "Disable %s HW", GetName() );

  if(isEnabled())
  {
    StopGdpPlus(); // will start when pushing new configuration.
    CDisplayPlane::DisableHW();
  }

  /* Invalidate current GDP+ configuration */
  m_gdpConf.isValid = false;

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}

void CGdpPlusPlane::ResetGdpSetup(void)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  m_GdpPlusSetup.nodeType          = GNODE_PROGRESSIVE;
  m_GdpPlusSetup.pictureId         = 0;
  m_GdpPlusSetup.isValid           = false;

  vibe_os_zero_memory(&m_GdpPlusSetup.topNode, sizeof(gdpgqr_lld_hw_viewport_s));
  vibe_os_zero_memory(&m_GdpPlusSetup.botNode, sizeof(gdpgqr_lld_hw_viewport_s));

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}


void CGdpPlusPlane::setNodeVisbility(gdpgqr_lld_hw_viewport_s *node, bool bForceOnMixer)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_IT, "bForceOnMixer = %d", (int)bForceOnMixer);

  /* Force node on Mixer */
  if(bForceOnMixer)
  {
    node->PPT |= m_ForceOnMixerMask;
  }
  else
  {
    /*
     * Clear Force on Mixer bits. This should update visibility of the
     * node according to the Mixer control configuration.
     */
    node->PPT &= ~m_ForceOnMixerMask;
  }

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_IT, "" );
}


void CGdpPlusPlane::Freeze(void)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  /* Backup current visibility state */
  m_wasEnabled = isEnabled();

  if(m_wasEnabled)
  {
    /*
     * For GFX planes we only need to disable HW and keep
     * previous node on the list so we will have previous
     * picture displayed on the screen when resuming.
     */
    DisableHW();
  }

  CDisplayPlane::Freeze();

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}


void CGdpPlusPlane::Resume(void)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  if(m_wasEnabled && m_bIsFrozen && m_pOutput)
  {
    bool isDisplayInterlaced = (m_outputInfo.currentMode.mode_params.scan_type == STM_INTERLACED_SCAN);
    bool isTopFieldOnDisplay = (m_pOutput->GetCurrentVTGEvent() & STM_TIMING_EVENT_TOP_FIELD) != 0;

    /* Check for unexpected field inversion */
    CheckForUnexpectedFieldInversion(isDisplayInterlaced, isTopFieldOnDisplay);

    /*
     * Rebuild new GDP+ configuration if an unexpected field inversion
     * is detected.
     */
    if(m_unexpectedFieldInversion)
    {
      gdpgqr_lld_hw_viewport_s *topNode = &m_GdpPlusSetup.topNode;
      gdpgqr_lld_hw_viewport_s *botNode = &m_GdpPlusSetup.botNode;

      PrepareConf(topNode, botNode, isTopFieldOnDisplay, isDisplayInterlaced);
    }

    /*
     * Apply command.
     */
    ApplyConf(&m_gdpConf);
  }

  CDisplayPlane::Resume();

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}


/*******************************************************************************
Name        : AllocateLldMemory
Description : Allocate memory for LLD internal needs
Parameters  : lldMemNeeds      - [in]  LLD memory needs
              pLldConfigBuffer - [out] LLD config buffers descriptor
Assumptions : LLD memory not yet allocated
Limitations :
Returns     : true if memory successfully allocated, false else
*******************************************************************************/
bool CGdpPlusPlane::AllocateLldMemory(
                                const gdpgqr_lld_mem_need_s&  lldMemNeeds,
                                DMA_Area*                     pLldConfigBuffer) const
{
    PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

    // Allocate DMA area for LLD internal configuration
    // SDAAF_UNCACHED flag allows to bypass the CPU cache and thus write
    // directly to physical memory. This way LLD does not need to flush
    // the cache once commands for HW are ready.
    vibe_os_allocate_dma_area( pLldConfigBuffer,
                               lldMemNeeds.size,
                               lldMemNeeds.alignment,
                               SDAAF_UNCACHED);
    if(!pLldConfigBuffer->pMemory)
    {
      PLANE_TRC( TRC_ID_ERROR, "Failed to allocate LLD internal configuration buffer" );
      return false;
    }
    vibe_os_zero_memory(pLldConfigBuffer->pData, pLldConfigBuffer->ulDataSize);

    PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
    return true;
}

/*******************************************************************************
Name        : FreeLldMemory
Description : Release the memory allocated for LLD internal needs
Parameters  : pLldCmdMem   - [in/out] LLD/FW commands buffer descriptor to be freed
Assumptions : LLD memory has been already allocated
Limitations :
Returns     : void
*******************************************************************************/
void CGdpPlusPlane::FreeLldMemory( DMA_Area*   pLldConfigBuffer) const
{
    PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

    if(pLldConfigBuffer->pData != 0)
    {
      vibe_os_free_dma_area(pLldConfigBuffer);
    }

    PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}

/*******************************************************************************
Name        : OpenLldSession
Description : Opens a HQVDP LLD session
Parameters  : lldMemDesc - [in] Memory description allocated for LLD
Assumptions : LLD memory has been allocated
Limitations :
Returns     : true if session successfully opened, false else
*******************************************************************************/
bool CGdpPlusPlane::OpenLldSession(const gdpgqr_lld_mem_desc_s& lldMemDesc)
{
    PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

    enum gdpgqr_lld_error   err = GDPGQR_LLD_NO_ERR;
    uint32_t                hwUsedBitMask = 0;

    // Open LLD session
    err = gdpgqr_lld_open( m_lldProfile,     // [in]
                        &lldMemDesc,         // [in]
                        &hwUsedBitMask,      // [out]
                        &m_lldHandle);       // [out]

    if(err != GDPGQR_LLD_NO_ERR)
    {
      PLANE_TRC( TRC_ID_ERROR, "gdpgqr_lld_open() failed, err=%d", err );
      return false;
    }

    PLANE_TRC(TRC_ID_GDP_PLUS_PLANE, "GDP Plus LLD session opened with profile %s",
                                    m_lldProfile == GDPGQR_PROFILE_FULL_HD ? "FULL HD" :
                                    m_lldProfile == GDPGQR_PROFILE_4KP30   ? "4KP30"   :
                                    m_lldProfile == GDPGQR_PROFILE_4KP60   ? "4KP60"   :
                                    "unrecognized");

    PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
    return true;
}

/*******************************************************************************
Name        : CloseLldSession
Description : Close the GDP Plus LLD session
Parameters  : None
Assumptions : LLD session is open
Limitations :
Returns     : void
*******************************************************************************/
void CGdpPlusPlane::CloseLldSession()
{
    PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

    StopGdpPlus();

    if(m_lldHandle != 0)
    {
      const enum gdpgqr_lld_error err = gdpgqr_lld_close(m_lldHandle);

      if(err != GDPGQR_LLD_NO_ERR)
      {
        PLANE_TRC(TRC_ID_ERROR, "gdpgqr_lld_close() failed, err=%d", err);
        return;
      }

      PLANE_TRC(TRC_ID_MAIN_INFO, "GDP Plus LLD session closed");
      m_lldHandle = 0;
    }

    PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}

/*******************************************************************************
Name        : ApplyConf
Description : Apply a GDP Plus configuration to the LLD
Parameters  : pGdpPlusConf   - [in] LLD-ready GDP Plus configuration
Assumptions : LLD session is open and pGdpPlusConf is already filled accordingly
Limitations :
Returns     : void
*******************************************************************************/
void CGdpPlusPlane::ApplyConf(const GdpPlusConf_s*          pGdpPlusConf)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_IT, "" );

  gdpgqr_lld_mixer_setup_s  mixerSetup[GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE];
  gdpgqr_lld_error          err = GDPGQR_LLD_NO_ERR;
  gdpgqr_lld_output_param_s output_params;
  uint32_t                  EnabledBitMask = 0;
  int                       nbHwUsed = 0;

#if defined(FORCE_DUAL_GDP_PLUS_HW_PER_GFX_PLANE)
  output_params.output_vtg_fps = 60000;
  output_params.width          = 3840;
  output_params.height         = 2160;
#else
  output_params.output_vtg_fps = m_outputInfo.currentMode.mode_params.vertical_refresh_rate;
  output_params.width          = m_outputInfo.currentMode.mode_params.active_area_width;
  output_params.height         = m_outputInfo.currentMode.mode_params.active_area_height;
#endif

  // Apply conf to GDP Plus HW
  err = gdpgqr_lld_set_conf( m_lldHandle,           // [in]
                            &output_params,         // [in]
                            pGdpPlusConf->conf,     // [in]
                            &nbHwUsed,              // [out]
                            mixerSetup);            // [out]

  if(err != GDPGQR_LLD_NO_ERR)
  {
    PLANE_TRC(TRC_ID_ERROR, "gdpgqr_lld_set_conf() failed, err=%d", err);
    return;
  }

  // Apply the video plugs setups
  for(int i = 0; i < nbHwUsed; i++)
  {
    if(mixerSetup[i].is_active)
    {
      // This GDP Plus instance is used => set corresponding bit
      EnabledBitMask |= mixerSetup[i].mixer_bit;
    }
    else
    {
      // This GDP Plus instance is not used => clear corresponding bit
      EnabledBitMask &= ~mixerSetup[i].mixer_bit;
    }
  } // for(nbHwUsed)

  if(EnabledBitMask != 0)
  {
    EnableGdpPlusHW();
  }

  if (m_pOutput != NULL)
  {
    m_pOutput->SetMixerMap(this, EnabledBitMask);
  }

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_IT, "" );
}


/*******************************************************************************
Name        : StopGdpPlus
Description : Instructs GDP Plus LLD to move to "stopped" state.
Parameters  : None
Assumptions : LLD session is open
Limitations :
Returns     : void
*******************************************************************************/
void CGdpPlusPlane::StopGdpPlus(void)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  if(m_lldHandle != 0)
  {
    const gdpgqr_lld_error err = gdpgqr_lld_stop(m_lldHandle);

    if(err != GDPGQR_LLD_NO_ERR)
    {
      PLANE_TRC(TRC_ID_ERROR, "gdpgqr_lld_stop() failed, err=%d", err);
      return;
    }

    PLANE_TRC(TRC_ID_GDP_PLUS_PLANE, "GDP Plus HW stopped");
  }

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
}

/*******************************************************************************
Name        : PrepareConf
Description : Prepare the GDP Plus configuration to be applied to LLD.
Parameters  : topNode             - [in]  top node info
              botNode             - [in]  bottom node info
              isTopFieldOnDisplay - [in]  Current polarity of displayed field
              isDisplayInterlaced - [in]  Current display scan type
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CGdpPlusPlane::PrepareConf(gdpgqr_lld_hw_viewport_s       *topNode,
                                  gdpgqr_lld_hw_viewport_s       *botNode,
                                  bool                            isTopFieldOnDisplay,
                                  bool                            isDisplayInterlaced)
{
  gdpgqr_lld_hw_viewport_s *currentfieldsetup = 0;
  gdpgqr_lld_hw_viewport_s *nextfieldsetup    = 0;

  PLANE_TRCIN(TRC_ID_GDP_PLUS_IT, "");

  /*
   * Setup GDP Plus LLD Configuration:
   * - Link Top and Bottom nodes (if display is interlaced)
   * - Generate LLD configuration from Top and Bottom nodes setup
   */
  /* Backup original Top and Bottom PML addresses */
  uint32_t savedtopbaseaddr = topNode->PML;
  uint32_t savedbotbaseaddr = botNode->PML;

  /*
   * Patch Top and Bottom PML addresses. Change the PML values in the
   * GDP+ configurations.
   */
  topNode->PML = m_gdpConf.conf[0].viewport.PML;
  botNode->PML = m_gdpConf.conf[1].viewport.PML;

  if(isDisplayInterlaced)
  {
    /*
     * This apply for both P2I and I2I use case as the driver is always
     * building a couple of nodes (top and bottom) to present the source
     * picture.
     *
     * For I2I use case we should make sure to push nodes with correct
     * order whitch is respect to current displayed field polarity.
     */
    if(isTopFieldOnDisplay)
    {
      currentfieldsetup = topNode;
      nextfieldsetup    = botNode;
    }
    else
    {
      currentfieldsetup = botNode;
      nextfieldsetup    = topNode;
    }
  }
  else
  {
    if (m_GdpPlusSetup.nodeType == GNODE_PROGRESSIVE)
    {
      /*
       * This apply for P2P use case where driver should use only one node
       * (TOP which have full source SIZE value) for display presentation.
       */
      currentfieldsetup = topNode;
      nextfieldsetup    = topNode;
    }
    else
    {
      /*
       * This apply for I2P use case where driver should respect the source
       * proprieties and push appropriate data field on the correct display
       * field.
       */
      if (m_GdpPlusSetup.nodeType == GNODE_TOP_FIELD)
      {
        currentfieldsetup = botNode;
        nextfieldsetup    = topNode;
      }
      else
      {
        currentfieldsetup = topNode;
        nextfieldsetup    = botNode;
      }
    }
  }

  /*
   * We are always pushing two linked nodes even for prgressive display.
   */
  m_gdpConf.isValid = true;
  m_gdpConf.conf[0].viewport = *nextfieldsetup;
  m_gdpConf.conf[0].next     = &m_gdpConf.conf[1];
  m_gdpConf.conf[1].viewport = *currentfieldsetup;
  m_gdpConf.conf[1].next     = 0;

  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "------------------------- %s Node -----------------------", (isTopFieldOnDisplay ? "Bottom":"Top"));
  DEBUGGDPPLUS(((gdpgqr_lld_hw_viewport_s *)&m_gdpConf.conf[0].viewport));
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "------------------------- %s Node -----------------------", (isTopFieldOnDisplay ? "Top":"Bottom"));
  DEBUGGDPPLUS(((gdpgqr_lld_hw_viewport_s *)&m_gdpConf.conf[1].viewport));

  /* Restore original Top and Bottom PML values */
  topNode->PML = savedtopbaseaddr;
  botNode->PML = savedbotbaseaddr;

  PLANE_TRCOUT(TRC_ID_GDP_PLUS_IT, "");
  return true;
}


/*******************************************************************************
Name        : UpdateConf
Description : Update the GDP Plus configuration with respect to current setup.
Parameters  : pGdpPlusConf        - [out] GDP Plus conf to be updated
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CGdpPlusPlane::UpdateConf(const GdpPlusConf_s* pGdpPlusConf)
{
  PLANE_TRCIN( TRC_ID_GDP_PLUS_PLANE, "" );

  gdpgqr_lld_mixer_setup_s mixerSetup[GDPGQR_LLD_MAX_HW_FOR_ONE_PLANE];
  gdpgqr_lld_error         err = GDPGQR_LLD_NO_ERR;
  gdpgqr_lld_output_param_s output_params;
  int                       nbHwUsed = 0;

  output_params.output_vtg_fps = m_outputInfo.currentMode.mode_params.vertical_refresh_rate;
  output_params.width          = m_outputInfo.currentMode.mode_params.active_area_width;
  output_params.height         = m_outputInfo.currentMode.mode_params.active_area_height;

  // Apply conf to GDP Plus HW
  err = gdpgqr_lld_set_conf( m_lldHandle,           // [in]
                            &output_params,         // [in]
                            pGdpPlusConf->conf,     // [in]
                            &nbHwUsed,              // [out]
                            mixerSetup);            // [out]

  if(err != GDPGQR_LLD_NO_ERR)
  {
    PLANE_TRC(TRC_ID_ERROR, "gdpgqr_lld_set_conf() failed, err=%d", err);
    return false;
  }

  PLANE_TRCOUT( TRC_ID_GDP_PLUS_PLANE, "" );
  return true;
}

void CGdpPlusPlane::ProcessLastVsyncStatus(const stm_time64_t &vsyncTime, CDisplayNode *pNodeDisplayed)
{
  // No status currently collected on GDP
}

void CGdpPlusPlane::FillHDRGainOffset (stm_hdr_gain_offset_t       * const dst,
                                      const stm_hdr_gain_offset_t * const src) const
{
  dst->hdr_hlg_gain      = src->hdr_hlg_gain;
  dst->hdr_st2084_gain   = src->hdr_st2084_gain;
  dst->sdr_gain          = src->sdr_gain;
  dst->hdr_hlg_offset    = src->hdr_hlg_offset;
  dst->hdr_st2084_offset = src->hdr_st2084_offset;
  dst->sdr_offset        = src->sdr_offset;
}

