/*******************************************************************************
 *
 * File: display/ip/gdpplus/GdpPlusFilter.cpp
 * Copyright (c) 2005-2007,2008 by STMicroelectronics. All rights reserved.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 ******************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include "STRefGdpPlusFilters.h"
#include "GdpPlusFilter.h"


CGdpPlusFilter::CGdpPlusFilter(void)
{
  vibe_os_zero_memory( m_horizontalLumaChroma, sizeof( m_horizontalLumaChroma ));
  vibe_os_zero_memory( m_verticalLumaChroma, sizeof( m_verticalLumaChroma ));
  m_filterSet = FILTER_SET_LEGACY; // by default, use legacy filters
  m_maxVerticalLumaCoef = COEF_L;
  m_horizontalBoundaries = 0;
  m_verticalBoundaries = 0;
  m_nbLumaVerticalTables = 0;
  m_nbChromaVerticalTables = 0;
}


CGdpPlusFilter::~CGdpPlusFilter(void)
{

}


bool CGdpPlusFilter::Create(void)
{
  /*
   * LEGACY LUT is not optimal but it is historical LUT we should take
   * SHARP or MEDIUM for best quality.
   */
  SelectFilterSet( /*FILTER_SET_MEDIUM*/ FILTER_SET_SHARP );
  return true;
}


bool CGdpPlusFilter::SelectFilterSet( stm_plane_filter_set_t filterSet )
{
  bool retval = true;

  switch ( filterSet )
    {
    case FILTER_SET_LEGACY :
      TRC( TRC_ID_GDP_PLUS_PLANE, "LEGACY filter set selected" );
      /*
       * Horizontal LUTs and Normalization values.
       */
      m_horizontalBoundaries = LegacyHorizontalBoundaries;
      m_horizontalLumaChroma[COEF_X2].chroma = LegacyHorizontalCoefficientsChromaZoomX2.LUT;
      m_horizontalLumaChroma[COEF_X2].chroma_normalization = LegacyHorizontalCoefficientsChromaZoomX2.Normalization;

      m_horizontalLumaChroma[COEF_A].luma = LegacyHorizontalCoefficientsA.LUT;
      m_horizontalLumaChroma[COEF_A].luma_normalization = LegacyHorizontalCoefficientsA.Normalization;
      m_horizontalLumaChroma[COEF_A].chroma = LegacyHorizontalCoefficientsA.LUT;
      m_horizontalLumaChroma[COEF_A].chroma_normalization = LegacyHorizontalCoefficientsA.Normalization;

      m_horizontalLumaChroma[COEF_B].luma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].luma_normalization = HorizontalCoefficientsB.Normalization;
      m_horizontalLumaChroma[COEF_B].chroma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].chroma_normalization = HorizontalCoefficientsB.Normalization;

      m_horizontalLumaChroma[COEF_C].luma = LegacyHorizontalCoefficientsLumaC.LUT;
      m_horizontalLumaChroma[COEF_C].luma_normalization = LegacyHorizontalCoefficientsLumaC.Normalization;
      m_horizontalLumaChroma[COEF_C].chroma = LegacyHorizontalCoefficientsChromaC.LUT;
      m_horizontalLumaChroma[COEF_C].chroma_normalization = LegacyHorizontalCoefficientsChromaC.Normalization;

      m_horizontalLumaChroma[COEF_D].luma = LegacyHorizontalCoefficientsLumaD.LUT;
      m_horizontalLumaChroma[COEF_D].luma_normalization = LegacyHorizontalCoefficientsLumaD.Normalization;
      m_horizontalLumaChroma[COEF_D].chroma = LegacyHorizontalCoefficientsChromaD.LUT;
      m_horizontalLumaChroma[COEF_D].chroma_normalization = LegacyHorizontalCoefficientsChromaD.Normalization;

      m_horizontalLumaChroma[COEF_E].luma = LegacyHorizontalCoefficientsLumaE.LUT;
      m_horizontalLumaChroma[COEF_E].luma_normalization = LegacyHorizontalCoefficientsLumaE.Normalization;
      m_horizontalLumaChroma[COEF_E].chroma = LegacyHorizontalCoefficientsChromaE.LUT;
      m_horizontalLumaChroma[COEF_E].chroma_normalization = LegacyHorizontalCoefficientsChromaE.Normalization;

      m_horizontalLumaChroma[COEF_F].luma = LegacyHorizontalCoefficientsLumaF.LUT;
      m_horizontalLumaChroma[COEF_F].luma_normalization = LegacyHorizontalCoefficientsLumaF.Normalization;
      m_horizontalLumaChroma[COEF_F].chroma = LegacyHorizontalCoefficientsChromaF.LUT;
      m_horizontalLumaChroma[COEF_F].chroma_normalization = LegacyHorizontalCoefficientsChromaF.Normalization;

      /*
       * Vertical LUTs and Normalization values.
       */
      m_verticalBoundaries = LegacyVerticalBoundaries;
      m_verticalLumaChroma[COEF_A].luma = LegacyVerticalCoefficientsA.LUT;
      m_verticalLumaChroma[COEF_A].luma_normalization = LegacyVerticalCoefficientsA.Normalization;
      m_verticalLumaChroma[COEF_A].chroma = LegacyVerticalCoefficientsA.LUT;
      m_verticalLumaChroma[COEF_A].chroma_normalization = LegacyVerticalCoefficientsA.Normalization;

      m_verticalLumaChroma[COEF_B].luma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].luma_normalization = VerticalCoefficientsB.Normalization;
      m_verticalLumaChroma[COEF_B].chroma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].chroma_normalization = VerticalCoefficientsB.Normalization;

      m_verticalLumaChroma[COEF_C].luma = LegacyVerticalCoefficientsLumaC.LUT;
      m_verticalLumaChroma[COEF_C].luma_normalization = LegacyVerticalCoefficientsLumaC.Normalization;
      m_verticalLumaChroma[COEF_C].chroma = LegacyVerticalCoefficientsChromaC.LUT;
      m_verticalLumaChroma[COEF_C].chroma_normalization = LegacyVerticalCoefficientsChromaC.Normalization;

      m_verticalLumaChroma[COEF_D].luma = LegacyVerticalCoefficientsLumaD.LUT;
      m_verticalLumaChroma[COEF_D].luma_normalization = LegacyVerticalCoefficientsLumaD.Normalization;
      m_verticalLumaChroma[COEF_D].chroma = LegacyVerticalCoefficientsChromaD.LUT;
      m_verticalLumaChroma[COEF_D].chroma_normalization = LegacyVerticalCoefficientsChromaD.Normalization;

      m_verticalLumaChroma[COEF_E].luma = LegacyVerticalCoefficientsLumaE.LUT;
      m_verticalLumaChroma[COEF_E].luma_normalization = LegacyVerticalCoefficientsLumaE.Normalization;
      m_verticalLumaChroma[COEF_E].chroma = LegacyVerticalCoefficientsChromaE.LUT;
      m_verticalLumaChroma[COEF_E].chroma_normalization = LegacyVerticalCoefficientsChromaE.Normalization;

      m_verticalLumaChroma[COEF_F].luma = LegacyVerticalCoefficientsLumaF.LUT;
      m_verticalLumaChroma[COEF_F].luma_normalization = LegacyVerticalCoefficientsLumaF.Normalization;
      m_verticalLumaChroma[COEF_F].chroma = LegacyVerticalCoefficientsChromaF.LUT;
      m_verticalLumaChroma[COEF_F].chroma_normalization = LegacyVerticalCoefficientsChromaF.Normalization;

      m_maxVerticalLumaCoef = COEF_F;
      break;

    case FILTER_SET_MEDIUM :
      TRC( TRC_ID_GDP_PLUS_PLANE, "MEDIUM filter set selected" );
      /*
       * Horizontal LUTs and Normalization values.
       */
      m_horizontalBoundaries = MediumHorizontalBoundaries;
      m_horizontalLumaChroma[COEF_X2].chroma = 0;
      m_horizontalLumaChroma[COEF_X2].chroma_normalization = 0;

      m_horizontalLumaChroma[COEF_A].luma = MediumHorizontalCoefficientsLumaA.LUT;
      m_horizontalLumaChroma[COEF_A].luma_normalization = MediumHorizontalCoefficientsLumaA.Normalization;
      m_horizontalLumaChroma[COEF_A].chroma = MediumHorizontalCoefficientsChromaA.LUT;
      m_horizontalLumaChroma[COEF_A].chroma_normalization = MediumHorizontalCoefficientsChromaA.Normalization;

      m_horizontalLumaChroma[COEF_B].luma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].luma_normalization = HorizontalCoefficientsB.Normalization;
      m_horizontalLumaChroma[COEF_B].chroma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].chroma_normalization = HorizontalCoefficientsB.Normalization;

      m_horizontalLumaChroma[COEF_C].luma = MediumHorizontalCoefficientsLumaC.LUT;
      m_horizontalLumaChroma[COEF_C].luma_normalization = MediumHorizontalCoefficientsLumaC.Normalization;
      m_horizontalLumaChroma[COEF_C].chroma = MediumHorizontalCoefficientsChromaC.LUT;
      m_horizontalLumaChroma[COEF_C].chroma_normalization = MediumHorizontalCoefficientsChromaC.Normalization;

      m_horizontalLumaChroma[COEF_D].luma = MediumHorizontalCoefficientsLumaD.LUT;
      m_horizontalLumaChroma[COEF_D].luma_normalization = MediumHorizontalCoefficientsLumaD.Normalization;
      m_horizontalLumaChroma[COEF_D].chroma = MediumHorizontalCoefficientsChromaD.LUT;
      m_horizontalLumaChroma[COEF_D].chroma_normalization = MediumHorizontalCoefficientsChromaD.Normalization;

      m_horizontalLumaChroma[COEF_E].luma = MediumHorizontalCoefficientsLumaE.LUT;
      m_horizontalLumaChroma[COEF_E].luma_normalization = MediumHorizontalCoefficientsLumaE.Normalization;
      m_horizontalLumaChroma[COEF_E].chroma = MediumHorizontalCoefficientsChromaE.LUT;
      m_horizontalLumaChroma[COEF_E].chroma_normalization = MediumHorizontalCoefficientsChromaE.Normalization;

      m_horizontalLumaChroma[COEF_F].luma = MediumHorizontalCoefficientsLumaF.LUT;
      m_horizontalLumaChroma[COEF_F].luma_normalization = MediumHorizontalCoefficientsLumaF.Normalization;
      m_horizontalLumaChroma[COEF_F].chroma = MediumHorizontalCoefficientsChromaF.LUT;
      m_horizontalLumaChroma[COEF_F].chroma_normalization = MediumHorizontalCoefficientsChromaF.Normalization;

      /*
       * Vertical LUTs and Normalization values.
       */
      m_verticalLumaChroma[COEF_X2].chroma = 0;
      m_verticalLumaChroma[COEF_X2].chroma_normalization = 0;

      m_verticalBoundaries = MediumVerticalBoundaries;
      m_verticalLumaChroma[COEF_A].luma = MediumVerticalCoefficientsLumaA.LUT;
      m_verticalLumaChroma[COEF_A].luma_normalization = MediumVerticalCoefficientsLumaA.Normalization;
      m_verticalLumaChroma[COEF_A].chroma = MediumVerticalCoefficientsChromaA.LUT;
      m_verticalLumaChroma[COEF_A].chroma_normalization = MediumVerticalCoefficientsChromaA.Normalization;

      m_verticalLumaChroma[COEF_B].luma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].luma_normalization = VerticalCoefficientsB.Normalization;
      m_verticalLumaChroma[COEF_B].chroma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].chroma_normalization = VerticalCoefficientsB.Normalization;

      m_verticalLumaChroma[COEF_C].luma = MediumVerticalCoefficientsLumaC.LUT;
      m_verticalLumaChroma[COEF_C].luma_normalization = MediumVerticalCoefficientsLumaC.Normalization;
      m_verticalLumaChroma[COEF_C].chroma = MediumVerticalCoefficientsChromaC.LUT;
      m_verticalLumaChroma[COEF_C].chroma_normalization = MediumVerticalCoefficientsChromaC.Normalization;

      m_verticalLumaChroma[COEF_D].luma = MediumVerticalCoefficientsLumaD.LUT;
      m_verticalLumaChroma[COEF_D].luma_normalization = MediumVerticalCoefficientsLumaD.Normalization;
      m_verticalLumaChroma[COEF_D].chroma = MediumVerticalCoefficientsChromaD.LUT;
      m_verticalLumaChroma[COEF_D].chroma_normalization = MediumVerticalCoefficientsChromaD.Normalization;

      m_verticalLumaChroma[COEF_D].luma = MediumVerticalCoefficientsLumaD.LUT;
      m_verticalLumaChroma[COEF_D].luma_normalization = MediumVerticalCoefficientsLumaD.Normalization;
      m_verticalLumaChroma[COEF_D].chroma = MediumVerticalCoefficientsChromaD.LUT;
      m_verticalLumaChroma[COEF_D].chroma_normalization = MediumVerticalCoefficientsChromaD.Normalization;

      m_verticalLumaChroma[COEF_E].luma = MediumVerticalCoefficientsLumaE.LUT;
      m_verticalLumaChroma[COEF_E].luma_normalization = MediumVerticalCoefficientsLumaE.Normalization;
      m_verticalLumaChroma[COEF_E].chroma = MediumVerticalCoefficientsChromaE.LUT;
      m_verticalLumaChroma[COEF_E].chroma_normalization = MediumVerticalCoefficientsChromaE.Normalization;

      m_verticalLumaChroma[COEF_F].luma = MediumVerticalCoefficientsLumaF.LUT;
      m_verticalLumaChroma[COEF_F].luma_normalization = MediumVerticalCoefficientsLumaF.Normalization;
      m_verticalLumaChroma[COEF_F].chroma = MediumVerticalCoefficientsChromaF.LUT;
      m_verticalLumaChroma[COEF_F].chroma_normalization = MediumVerticalCoefficientsChromaF.Normalization;

      m_verticalLumaChroma[COEF_G].luma = MediumVerticalCoefficientsLumaG.LUT;
      m_verticalLumaChroma[COEF_G].luma_normalization = MediumVerticalCoefficientsLumaG.Normalization;
      m_verticalLumaChroma[COEF_G].chroma = MediumVerticalCoefficientsChromaG.LUT;
      m_verticalLumaChroma[COEF_G].chroma_normalization = MediumVerticalCoefficientsChromaG.Normalization;

      m_verticalLumaChroma[COEF_H].luma = MediumVerticalCoefficientsLumaH.LUT;
      m_verticalLumaChroma[COEF_H].luma_normalization = MediumVerticalCoefficientsLumaH.Normalization;
      m_verticalLumaChroma[COEF_H].chroma = MediumVerticalCoefficientsChromaH.LUT;
      m_verticalLumaChroma[COEF_H].chroma_normalization = MediumVerticalCoefficientsChromaH.Normalization;

      m_verticalLumaChroma[COEF_I].luma = MediumVerticalCoefficientsLumaI.LUT;
      m_verticalLumaChroma[COEF_I].luma_normalization = MediumVerticalCoefficientsLumaI.Normalization;
      m_verticalLumaChroma[COEF_I].chroma = MediumVerticalCoefficientsChromaI.LUT;
      m_verticalLumaChroma[COEF_I].chroma_normalization = MediumVerticalCoefficientsChromaI.Normalization;

      m_verticalLumaChroma[COEF_J].luma = MediumVerticalCoefficientsLumaJ.LUT;
      m_verticalLumaChroma[COEF_J].luma_normalization = MediumVerticalCoefficientsLumaJ.Normalization;
      m_verticalLumaChroma[COEF_J].chroma = MediumVerticalCoefficientsChromaJ.LUT;
      m_verticalLumaChroma[COEF_J].chroma_normalization = MediumVerticalCoefficientsChromaJ.Normalization;

      m_verticalLumaChroma[COEF_K].luma = MediumVerticalCoefficientsLumaK.LUT;
      m_verticalLumaChroma[COEF_K].luma_normalization = MediumVerticalCoefficientsLumaK.Normalization;
      m_verticalLumaChroma[COEF_K].chroma = MediumVerticalCoefficientsChromaK.LUT;
      m_verticalLumaChroma[COEF_K].chroma_normalization = MediumVerticalCoefficientsChromaK.Normalization;

      m_verticalLumaChroma[COEF_L].luma = MediumVerticalCoefficientsLumaL.LUT;
      m_verticalLumaChroma[COEF_L].luma_normalization = MediumVerticalCoefficientsLumaL.Normalization;
      m_verticalLumaChroma[COEF_L].chroma = MediumVerticalCoefficientsChromaL.LUT;
      m_verticalLumaChroma[COEF_L].chroma_normalization = MediumVerticalCoefficientsChromaL.Normalization;

      m_maxVerticalLumaCoef = COEF_L;
      break;

    case FILTER_SET_SHARP :
      TRC( TRC_ID_GDP_PLUS_PLANE, "SHARP filter set selected" );
      /*
       * Horizontal LUTs and Normalization values.
       */
      m_horizontalBoundaries = SharpHorizontalBoundaries;
      m_horizontalLumaChroma[COEF_X2].chroma = 0;
      m_horizontalLumaChroma[COEF_X2].chroma_normalization = 0;

      m_horizontalLumaChroma[COEF_A].luma = SharpHorizontalCoefficientsLumaA.LUT;
      m_horizontalLumaChroma[COEF_A].luma_normalization = SharpHorizontalCoefficientsLumaA.Normalization;
      m_horizontalLumaChroma[COEF_A].chroma = SharpHorizontalCoefficientsChromaA.LUT;
      m_horizontalLumaChroma[COEF_A].chroma_normalization = SharpHorizontalCoefficientsChromaA.Normalization;

      m_horizontalLumaChroma[COEF_B].luma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].luma_normalization = HorizontalCoefficientsB.Normalization;
      m_horizontalLumaChroma[COEF_B].chroma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].chroma_normalization = HorizontalCoefficientsB.Normalization;

      m_horizontalLumaChroma[COEF_C].luma = SharpHorizontalCoefficientsLumaC.LUT;
      m_horizontalLumaChroma[COEF_C].luma_normalization = SharpHorizontalCoefficientsLumaC.Normalization;
      m_horizontalLumaChroma[COEF_C].chroma = SharpHorizontalCoefficientsChromaC.LUT;
      m_horizontalLumaChroma[COEF_C].chroma_normalization = SharpHorizontalCoefficientsChromaC.Normalization;

      m_horizontalLumaChroma[COEF_D].luma = SharpHorizontalCoefficientsLumaD.LUT;
      m_horizontalLumaChroma[COEF_D].luma_normalization = SharpHorizontalCoefficientsLumaD.Normalization;
      m_horizontalLumaChroma[COEF_D].chroma = SharpHorizontalCoefficientsChromaD.LUT;
      m_horizontalLumaChroma[COEF_D].chroma_normalization = SharpHorizontalCoefficientsChromaD.Normalization;

      m_horizontalLumaChroma[COEF_E].luma = SharpHorizontalCoefficientsLumaE.LUT;
      m_horizontalLumaChroma[COEF_E].luma_normalization = SharpHorizontalCoefficientsLumaE.Normalization;
      m_horizontalLumaChroma[COEF_E].chroma = SharpHorizontalCoefficientsChromaE.LUT;
      m_horizontalLumaChroma[COEF_E].chroma_normalization = SharpHorizontalCoefficientsChromaE.Normalization;

      m_horizontalLumaChroma[COEF_F].luma = SharpHorizontalCoefficientsLumaF.LUT;
      m_horizontalLumaChroma[COEF_F].luma_normalization = SharpHorizontalCoefficientsLumaF.Normalization;
      m_horizontalLumaChroma[COEF_F].chroma = SharpHorizontalCoefficientsChromaF.LUT;
      m_horizontalLumaChroma[COEF_F].chroma_normalization = SharpHorizontalCoefficientsChromaF.Normalization;

      /*
       * Vertical LUTs and Normalization values.
       */
      m_verticalBoundaries = SharpVerticalBoundaries;
      m_verticalLumaChroma[COEF_X2].chroma = 0;
      m_verticalLumaChroma[COEF_X2].chroma_normalization = 0;

      m_verticalLumaChroma[COEF_A].luma = SharpVerticalCoefficientsLumaA.LUT;
      m_verticalLumaChroma[COEF_A].luma_normalization = SharpVerticalCoefficientsLumaA.Normalization;
      m_verticalLumaChroma[COEF_A].chroma = SharpVerticalCoefficientsChromaA.LUT;
      m_verticalLumaChroma[COEF_A].chroma_normalization = SharpVerticalCoefficientsChromaA.Normalization;

      m_verticalLumaChroma[COEF_B].luma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].luma_normalization = VerticalCoefficientsB.Normalization;
      m_verticalLumaChroma[COEF_B].chroma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].chroma_normalization = VerticalCoefficientsB.Normalization;

      m_verticalLumaChroma[COEF_C].luma = SharpVerticalCoefficientsLumaC.LUT;
      m_verticalLumaChroma[COEF_C].luma_normalization = SharpVerticalCoefficientsLumaC.Normalization;
      m_verticalLumaChroma[COEF_C].chroma = SharpVerticalCoefficientsChromaC.LUT;
      m_verticalLumaChroma[COEF_C].chroma_normalization = SharpVerticalCoefficientsChromaC.Normalization;

      m_verticalLumaChroma[COEF_D].luma = SharpVerticalCoefficientsLumaD.LUT;
      m_verticalLumaChroma[COEF_D].luma_normalization = SharpVerticalCoefficientsLumaD.Normalization;
      m_verticalLumaChroma[COEF_D].chroma = SharpVerticalCoefficientsChromaD.LUT;
      m_verticalLumaChroma[COEF_D].chroma_normalization = SharpVerticalCoefficientsChromaD.Normalization;

      m_verticalLumaChroma[COEF_E].luma = SharpVerticalCoefficientsLumaE.LUT;
      m_verticalLumaChroma[COEF_E].luma_normalization = SharpVerticalCoefficientsLumaE.Normalization;
      m_verticalLumaChroma[COEF_E].chroma = SharpVerticalCoefficientsChromaE.LUT;
      m_verticalLumaChroma[COEF_E].chroma_normalization = SharpVerticalCoefficientsChromaE.Normalization;

      m_verticalLumaChroma[COEF_F].luma = SharpVerticalCoefficientsLumaF.LUT;
      m_verticalLumaChroma[COEF_F].luma_normalization = SharpVerticalCoefficientsLumaF.Normalization;
      m_verticalLumaChroma[COEF_F].chroma = SharpVerticalCoefficientsChromaF.LUT;
      m_verticalLumaChroma[COEF_F].chroma_normalization = SharpVerticalCoefficientsChromaF.Normalization;

      m_verticalLumaChroma[COEF_G].luma = SharpVerticalCoefficientsLumaG.LUT;
      m_verticalLumaChroma[COEF_G].luma_normalization = SharpVerticalCoefficientsLumaG.Normalization;
      m_verticalLumaChroma[COEF_G].chroma = SharpVerticalCoefficientsChromaG.LUT;
      m_verticalLumaChroma[COEF_G].chroma_normalization = SharpVerticalCoefficientsChromaG.Normalization;

      m_verticalLumaChroma[COEF_H].luma = SharpVerticalCoefficientsLumaH.LUT;
      m_verticalLumaChroma[COEF_H].luma_normalization = SharpVerticalCoefficientsLumaH.Normalization;
      m_verticalLumaChroma[COEF_H].chroma = SharpVerticalCoefficientsChromaH.LUT;
      m_verticalLumaChroma[COEF_H].chroma_normalization = SharpVerticalCoefficientsChromaH.Normalization;

      m_verticalLumaChroma[COEF_I].luma = SharpVerticalCoefficientsLumaI.LUT;
      m_verticalLumaChroma[COEF_I].luma_normalization = SharpVerticalCoefficientsLumaI.Normalization;
      m_verticalLumaChroma[COEF_I].chroma = SharpVerticalCoefficientsChromaI.LUT;
      m_verticalLumaChroma[COEF_I].chroma_normalization = SharpVerticalCoefficientsChromaI.Normalization;

      m_verticalLumaChroma[COEF_J].luma = SharpVerticalCoefficientsLumaJ.LUT;
      m_verticalLumaChroma[COEF_J].luma_normalization = SharpVerticalCoefficientsLumaJ.Normalization;
      m_verticalLumaChroma[COEF_J].chroma = SharpVerticalCoefficientsChromaJ.LUT;
      m_verticalLumaChroma[COEF_J].chroma_normalization = SharpVerticalCoefficientsChromaJ.Normalization;

      m_verticalLumaChroma[COEF_K].luma = SharpVerticalCoefficientsLumaK.LUT;
      m_verticalLumaChroma[COEF_K].luma_normalization = SharpVerticalCoefficientsLumaK.Normalization;
      m_verticalLumaChroma[COEF_K].chroma = SharpVerticalCoefficientsChromaK.LUT;
      m_verticalLumaChroma[COEF_K].chroma_normalization = SharpVerticalCoefficientsChromaK.Normalization;

      m_verticalLumaChroma[COEF_L].luma = SharpVerticalCoefficientsLumaL.LUT;
      m_verticalLumaChroma[COEF_L].luma_normalization = SharpVerticalCoefficientsLumaL.Normalization;
      m_verticalLumaChroma[COEF_L].chroma = SharpVerticalCoefficientsChromaL.LUT;
      m_verticalLumaChroma[COEF_L].chroma_normalization = SharpVerticalCoefficientsChromaL.Normalization;

      m_maxVerticalLumaCoef = COEF_L;
      break;

    case FILTER_SET_SMOOTH :
      TRC( TRC_ID_GDP_PLUS_PLANE, "SMOOTH filter set selected" );
      /*
       * Horizontal LUTs and Normalization values.
       */
      m_horizontalBoundaries = SmoothHorizontalBoundaries;
      m_horizontalLumaChroma[COEF_X2].chroma = 0;
      m_horizontalLumaChroma[COEF_X2].chroma_normalization = 0;

      m_horizontalLumaChroma[COEF_A].luma = SmoothHorizontalCoefficientsLumaA.LUT;
      m_horizontalLumaChroma[COEF_A].luma_normalization = SmoothHorizontalCoefficientsLumaA.Normalization;
      m_horizontalLumaChroma[COEF_A].chroma = SmoothHorizontalCoefficientsChromaA.LUT;
      m_horizontalLumaChroma[COEF_A].chroma_normalization = SmoothHorizontalCoefficientsChromaA.Normalization;

      m_horizontalLumaChroma[COEF_B].luma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].luma_normalization = HorizontalCoefficientsB.Normalization;
      m_horizontalLumaChroma[COEF_B].chroma = HorizontalCoefficientsB.LUT;
      m_horizontalLumaChroma[COEF_B].chroma_normalization = HorizontalCoefficientsB.Normalization;

      m_horizontalLumaChroma[COEF_C].luma = SmoothHorizontalCoefficientsLumaC.LUT;
      m_horizontalLumaChroma[COEF_C].luma_normalization = SmoothHorizontalCoefficientsLumaC.Normalization;
      m_horizontalLumaChroma[COEF_C].chroma = SmoothHorizontalCoefficientsChromaC.LUT;
      m_horizontalLumaChroma[COEF_C].chroma_normalization = SmoothHorizontalCoefficientsChromaC.Normalization;

      m_horizontalLumaChroma[COEF_D].luma = SmoothHorizontalCoefficientsLumaD.LUT;
      m_horizontalLumaChroma[COEF_D].luma_normalization = SmoothHorizontalCoefficientsLumaD.Normalization;
      m_horizontalLumaChroma[COEF_D].chroma = SmoothHorizontalCoefficientsChromaD.LUT;
      m_horizontalLumaChroma[COEF_D].chroma_normalization = SmoothHorizontalCoefficientsChromaD.Normalization;

      m_horizontalLumaChroma[COEF_E].luma = SmoothHorizontalCoefficientsLumaE.LUT;
      m_horizontalLumaChroma[COEF_E].luma_normalization = SmoothHorizontalCoefficientsLumaE.Normalization;
      m_horizontalLumaChroma[COEF_E].chroma = SmoothHorizontalCoefficientsChromaE.LUT;
      m_horizontalLumaChroma[COEF_E].chroma_normalization = SmoothHorizontalCoefficientsChromaE.Normalization;

      m_horizontalLumaChroma[COEF_F].luma = SmoothHorizontalCoefficientsLumaF.LUT;
      m_horizontalLumaChroma[COEF_F].luma_normalization = SmoothHorizontalCoefficientsLumaF.Normalization;
      m_horizontalLumaChroma[COEF_F].chroma = SmoothHorizontalCoefficientsChromaF.LUT;
      m_horizontalLumaChroma[COEF_F].chroma_normalization = SmoothHorizontalCoefficientsChromaF.Normalization;

      /*
       * Vertical LUTs and Normalization values.
       */
      m_verticalBoundaries = SmoothVerticalBoundaries;
      m_verticalLumaChroma[COEF_X2].chroma = 0;
      m_verticalLumaChroma[COEF_X2].chroma_normalization = 0;

      m_verticalLumaChroma[COEF_A].luma = SmoothVerticalCoefficientsLumaA.LUT;
      m_verticalLumaChroma[COEF_A].luma_normalization = SmoothVerticalCoefficientsLumaA.Normalization;
      m_verticalLumaChroma[COEF_A].chroma = SmoothVerticalCoefficientsChromaA.LUT;
      m_verticalLumaChroma[COEF_A].chroma_normalization = SmoothVerticalCoefficientsChromaA.Normalization;

      m_verticalLumaChroma[COEF_B].luma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].luma_normalization = VerticalCoefficientsB.Normalization;
      m_verticalLumaChroma[COEF_B].chroma = VerticalCoefficientsB.LUT;
      m_verticalLumaChroma[COEF_B].chroma_normalization = VerticalCoefficientsB.Normalization;

      m_verticalLumaChroma[COEF_C].luma = SmoothVerticalCoefficientsLumaC.LUT;
      m_verticalLumaChroma[COEF_C].luma_normalization = SmoothVerticalCoefficientsLumaC.Normalization;
      m_verticalLumaChroma[COEF_C].chroma = SmoothVerticalCoefficientsChromaC.LUT;
      m_verticalLumaChroma[COEF_C].chroma_normalization = SmoothVerticalCoefficientsChromaC.Normalization;

      m_verticalLumaChroma[COEF_D].luma = SmoothVerticalCoefficientsLumaD.LUT;
      m_verticalLumaChroma[COEF_D].luma_normalization = SmoothVerticalCoefficientsLumaD.Normalization;
      m_verticalLumaChroma[COEF_D].chroma = SmoothVerticalCoefficientsChromaD.LUT;
      m_verticalLumaChroma[COEF_D].chroma_normalization = SmoothVerticalCoefficientsChromaD.Normalization;

      m_verticalLumaChroma[COEF_E].luma = SmoothVerticalCoefficientsLumaE.LUT;
      m_verticalLumaChroma[COEF_E].luma_normalization = SmoothVerticalCoefficientsLumaE.Normalization;
      m_verticalLumaChroma[COEF_E].chroma = SmoothVerticalCoefficientsChromaE.LUT;
      m_verticalLumaChroma[COEF_E].chroma_normalization = SmoothVerticalCoefficientsChromaE.Normalization;

      m_verticalLumaChroma[COEF_F].luma = SmoothVerticalCoefficientsLumaF.LUT;
      m_verticalLumaChroma[COEF_F].luma_normalization = SmoothVerticalCoefficientsLumaF.Normalization;
      m_verticalLumaChroma[COEF_F].chroma = SmoothVerticalCoefficientsChromaF.LUT;
      m_verticalLumaChroma[COEF_F].chroma_normalization = SmoothVerticalCoefficientsChromaF.Normalization;

      m_verticalLumaChroma[COEF_G].luma = SmoothVerticalCoefficientsLumaG.LUT;
      m_verticalLumaChroma[COEF_G].luma_normalization = SmoothVerticalCoefficientsLumaG.Normalization;
      m_verticalLumaChroma[COEF_G].chroma = SmoothVerticalCoefficientsChromaG.LUT;
      m_verticalLumaChroma[COEF_G].chroma_normalization = SmoothVerticalCoefficientsChromaG.Normalization;

      m_verticalLumaChroma[COEF_H].luma = SmoothVerticalCoefficientsLumaH.LUT;
      m_verticalLumaChroma[COEF_H].luma_normalization = SmoothVerticalCoefficientsLumaH.Normalization;
      m_verticalLumaChroma[COEF_H].chroma = SmoothVerticalCoefficientsChromaH.LUT;
      m_verticalLumaChroma[COEF_H].chroma_normalization = SmoothVerticalCoefficientsChromaH.Normalization;

      m_verticalLumaChroma[COEF_I].luma = SmoothVerticalCoefficientsLumaI.LUT;
      m_verticalLumaChroma[COEF_I].luma_normalization = SmoothVerticalCoefficientsLumaI.Normalization;
      m_verticalLumaChroma[COEF_I].chroma = SmoothVerticalCoefficientsChromaI.LUT;
      m_verticalLumaChroma[COEF_I].chroma_normalization = SmoothVerticalCoefficientsChromaI.Normalization;

      m_verticalLumaChroma[COEF_J].luma = SmoothVerticalCoefficientsLumaJ.LUT;
      m_verticalLumaChroma[COEF_J].luma_normalization = SmoothVerticalCoefficientsLumaJ.Normalization;
      m_verticalLumaChroma[COEF_J].chroma = SmoothVerticalCoefficientsChromaJ.LUT;
      m_verticalLumaChroma[COEF_J].chroma_normalization = SmoothVerticalCoefficientsChromaJ.Normalization;

      m_verticalLumaChroma[COEF_K].luma = SmoothVerticalCoefficientsLumaK.LUT;
      m_verticalLumaChroma[COEF_K].luma_normalization = SmoothVerticalCoefficientsLumaK.Normalization;
      m_verticalLumaChroma[COEF_K].chroma = SmoothVerticalCoefficientsChromaK.LUT;
      m_verticalLumaChroma[COEF_K].chroma_normalization = SmoothVerticalCoefficientsChromaK.Normalization;

      m_verticalLumaChroma[COEF_L].luma = SmoothVerticalCoefficientsLumaL.LUT;
      m_verticalLumaChroma[COEF_L].luma_normalization = SmoothVerticalCoefficientsLumaL.Normalization;
      m_verticalLumaChroma[COEF_L].chroma = SmoothVerticalCoefficientsChromaL.LUT;
      m_verticalLumaChroma[COEF_L].chroma_normalization = SmoothVerticalCoefficientsChromaL.Normalization;
      m_maxVerticalLumaCoef = COEF_L;
      break;
    }

  m_filterSet = filterSet;

  TRC( TRC_ID_GDP_PLUS_PLANE, "SelectFilterSet %d OK", filterSet );
  return retval;
}


bool CGdpPlusFilter::GetFilterSet( stm_plane_filter_set_t * filterSet ) const
{
  *filterSet = m_filterSet;
  return true;
}


const uint32_t *CGdpPlusFilter::SelectHorizontalLumaFilter(uint32_t scaleFactor, uint32_t phase, uint8_t *normalization)
{
  const void *luma;
  uint8_t norm = 0;

  if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_E_F] )
    {
      TRC( TRC_ID_MAIN_INFO, "HY filter: COEF_F");
      luma = m_horizontalLumaChroma[COEF_F].luma;
      norm = m_horizontalLumaChroma[COEF_F].luma_normalization;
    }
  else if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_D_E] )
    {
      TRC( TRC_ID_MAIN_INFO, "HY filter: COEF_E");
      luma = m_horizontalLumaChroma[COEF_E].luma;
      norm = m_horizontalLumaChroma[COEF_E].luma_normalization;
    }
  else if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_C_D] )
    {
      TRC( TRC_ID_MAIN_INFO, "HY filter: COEF_D");
      luma = m_horizontalLumaChroma[COEF_D].luma;
      norm = m_horizontalLumaChroma[COEF_D].luma_normalization;
    }
  else if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_A_C] )
    {
      TRC( TRC_ID_MAIN_INFO, "HY filter: COEF_C");
      luma = m_horizontalLumaChroma[COEF_C].luma;
      norm = m_horizontalLumaChroma[COEF_C].luma_normalization;
    }
  else if (( scaleFactor == m_horizontalBoundaries[BOUNDARY_A_C] ) && ( phase == 0 )) // No scaling or subpixel pan/scan
    {
      TRC( TRC_ID_MAIN_INFO, "HY filter: COEF_B");
      luma = m_horizontalLumaChroma[COEF_B].luma;
      norm = m_horizontalLumaChroma[COEF_B].luma_normalization;
    }
  else
    {
      TRC( TRC_ID_MAIN_INFO, "HY filter: COEF_A");
      luma = m_horizontalLumaChroma[COEF_A].luma;       // zoom in or subpixel pan/scan at 1x
      norm = m_horizontalLumaChroma[COEF_A].luma_normalization;
    }

  /*
   * Fill normalization value if requested.
   */
  if(normalization != NULL)
  {
    *normalization = norm;
  }

  return (const uint32_t *)luma;
}


const uint32_t *CGdpPlusFilter::SelectHorizontalChromaFilter(uint32_t scaleFactor, uint32_t phase, uint8_t *normalization)
{
  const void *chroma;
  uint8_t norm = 0;

  if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_E_F] )
    {
      TRC( TRC_ID_MAIN_INFO, "HC filter: COEF_F");
      chroma = m_horizontalLumaChroma[COEF_F].chroma;
      norm   = m_horizontalLumaChroma[COEF_F].chroma_normalization;
    }
  else if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_D_E] )
    {
      TRC( TRC_ID_MAIN_INFO, "HC filter: COEF_E");
      chroma = m_horizontalLumaChroma[COEF_E].chroma;
      norm   = m_horizontalLumaChroma[COEF_E].chroma_normalization;
    }
  else if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_C_D] )
    {
      TRC( TRC_ID_MAIN_INFO, "HC filter: COEF_D");
      chroma = m_horizontalLumaChroma[COEF_D].chroma;
      norm   = m_horizontalLumaChroma[COEF_D].chroma_normalization;
    }
  else if ( scaleFactor > m_horizontalBoundaries[BOUNDARY_A_C] )
    {
      TRC( TRC_ID_MAIN_INFO, "HC filter: COEF_C");
      chroma = m_horizontalLumaChroma[COEF_C].chroma;
      norm   = m_horizontalLumaChroma[COEF_C].chroma_normalization;
    }
  else if (( scaleFactor == m_horizontalBoundaries[BOUNDARY_A_C] ) && ( phase == 0 )) // No scaling or subpixel pan/scan
    {
      TRC( TRC_ID_MAIN_INFO, "HC filter: COEF_B");
      chroma = m_horizontalLumaChroma[COEF_B].chroma;
      norm   = m_horizontalLumaChroma[COEF_B].chroma_normalization;
    }
  else
    {
      TRC( TRC_ID_MAIN_INFO, "HC filter: COEF_A");
      chroma = m_horizontalLumaChroma[COEF_A].chroma;       // zoom in or subpixel pan/scan at 1x
      norm   = m_horizontalLumaChroma[COEF_A].chroma_normalization;
    }

  // specific to legacy filter set
  if (( m_filterSet == FILTER_SET_LEGACY ) and ( scaleFactor == ZOOMx2 ))
    {
      TRC( TRC_ID_MAIN_INFO, "HC filter: COEF_X2");
      chroma = m_horizontalLumaChroma[COEF_X2].chroma;
      norm   = m_horizontalLumaChroma[COEF_X2].chroma_normalization;
    }

  /*
   * Fill normalization value if requested.
   */
  if(normalization != NULL)
  {
    *normalization = norm;
  }

  return (const uint32_t *)chroma;
}


const uint32_t *CGdpPlusFilter::SelectVerticalLumaFilter(uint32_t scaleFactor, uint32_t phase, uint8_t *normalization)
{
  const void *luma = 0;
  uint8_t norm = 0;

  if ( m_maxVerticalLumaCoef == COEF_L ) // only for medium, sharp or smooth
    {
      if ( scaleFactor > m_verticalBoundaries[BOUNDARY_K_L] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_L");
          luma = m_verticalLumaChroma[COEF_L].luma;
          norm = m_verticalLumaChroma[COEF_L].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_J_K] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_K");
          luma = m_verticalLumaChroma[COEF_K].luma;
          norm = m_verticalLumaChroma[COEF_K].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_I_J] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_J");
          luma = m_verticalLumaChroma[COEF_J].luma;
          norm = m_verticalLumaChroma[COEF_J].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_H_I] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_I");
          luma = m_verticalLumaChroma[COEF_I].luma;
          norm = m_verticalLumaChroma[COEF_I].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_G_H] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_H");
          luma = m_verticalLumaChroma[COEF_H].luma;
          norm = m_verticalLumaChroma[COEF_H].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_F_G] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_G");
          luma = m_verticalLumaChroma[COEF_G].luma;
          norm = m_verticalLumaChroma[COEF_G].luma_normalization;
        }
    }

  if ( luma == 0 ) // not set before
    {
      if ( scaleFactor > m_verticalBoundaries[BOUNDARY_E_F] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_F");
          luma = m_verticalLumaChroma[COEF_F].luma;
          norm = m_verticalLumaChroma[COEF_F].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_D_E] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_E");
          luma = m_verticalLumaChroma[COEF_E].luma;
          norm = m_verticalLumaChroma[COEF_E].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_C_D] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_D");
          luma = m_verticalLumaChroma[COEF_D].luma;
          norm = m_verticalLumaChroma[COEF_D].luma_normalization;
        }
      else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_A_C] )
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_C");
          luma = m_verticalLumaChroma[COEF_C].luma;
          norm = m_verticalLumaChroma[COEF_C].luma_normalization;
        }
      else if (( scaleFactor == m_verticalBoundaries[BOUNDARY_A_C] ) && ( phase == 0x1000 )) // No scaling or subpixel pan/scan
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_B");
          luma = m_verticalLumaChroma[COEF_B].luma;
          norm = m_verticalLumaChroma[COEF_B].luma_normalization;
        }
      else
        {
          TRC( TRC_ID_MAIN_INFO, "VY filter: COEF_A");
          luma = m_verticalLumaChroma[COEF_A].luma; // zoom in or subpixel pan/scan at 1x
          norm = m_verticalLumaChroma[COEF_A].luma_normalization;
        }
    }

  /*
   * Fill normalization value if requested.
   */
  if(normalization != NULL)
  {
    *normalization = norm;
  }

  return (const uint32_t *)luma;
}


const uint32_t *CGdpPlusFilter::SelectVerticalChromaFilter(uint32_t scaleFactor, uint32_t phase, uint8_t *normalization)
{
  const void *chroma;
  uint8_t norm = 0;

  if ( scaleFactor > m_verticalBoundaries[BOUNDARY_E_F] )
    {
      TRC( TRC_ID_MAIN_INFO, "VC filter: COEF_F");
      chroma = m_verticalLumaChroma[COEF_F].chroma;
      norm   = m_verticalLumaChroma[COEF_F].chroma_normalization;
    }
  else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_D_E] )
    {
      TRC( TRC_ID_MAIN_INFO, "VC filter: COEF_E");
      chroma = m_verticalLumaChroma[COEF_E].chroma;
      norm   = m_verticalLumaChroma[COEF_E].chroma_normalization;
    }
  else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_C_D] )
    {
      TRC( TRC_ID_MAIN_INFO, "VC filter: COEF_D");
      chroma = m_verticalLumaChroma[COEF_D].chroma;
      norm   = m_verticalLumaChroma[COEF_D].chroma_normalization;
    }
  else if ( scaleFactor > m_verticalBoundaries[BOUNDARY_A_C] )
    {
      TRC( TRC_ID_MAIN_INFO, "VC filter: COEF_C");
      chroma = m_verticalLumaChroma[COEF_C].chroma;
      norm   = m_verticalLumaChroma[COEF_C].chroma_normalization;
    }
  else if (( scaleFactor == m_verticalBoundaries[BOUNDARY_A_C] ) && ( phase == 0x1000 )) // No scaling or subpixel pan/scan
    {
      TRC( TRC_ID_MAIN_INFO, "VC filter: COEF_B");
      chroma = m_verticalLumaChroma[COEF_B].chroma;
      norm   = m_verticalLumaChroma[COEF_B].chroma_normalization;
    }
  else
    {
      TRC( TRC_ID_MAIN_INFO, "VC filter: COEF_A");
      chroma = m_verticalLumaChroma[COEF_A].chroma;       // zoom in or subpixel pan/scan at 1x
      norm   = m_verticalLumaChroma[COEF_A].chroma_normalization;
    }

  // specific to legacy filter set
  if (( m_filterSet == FILTER_SET_LEGACY ) and ( scaleFactor == ZOOMx2 ))
    {
      TRC( TRC_ID_MAIN_INFO, "VC filter: COEF_X2");
      chroma = m_verticalLumaChroma[COEF_X2].chroma;
      norm   = m_verticalLumaChroma[COEF_X2].chroma_normalization;
    }

  /*
   * Fill normalization value if requested.
   */
  if(normalization != NULL)
  {
    *normalization = norm;
  }

  return (const uint32_t *)chroma;
}
