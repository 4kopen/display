/*
* This file is part of Display Engine
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: GPLv2
*
* Display Engine is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* Display Engine is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Display Engine. If not, see
* <http://www.gnu.org/licenses/>.
*/

#ifndef _GDP_PLUS_PLANE_H
#define _GDP_PLUS_PLANE_H

#include <display/generic/DisplayPlane.h>
#include <display/ip/gdpplus/GdpPlusReg.h>
#include <display/ip/gdpplus/GdpPlusDisplayInfo.h>
#include "display/ip/gdpplus/GdpPlusFilter.h"
#include <display/ip/gdpplus/lld/gdpgqr_lld_api.h>

/*
 * No VBI support for the moment.
 */
#define GDP_GQR_MAX_VIEWPORTS_NUMBER        1

struct GdpPlusConf_s
{
  bool                isValid;
  gdpgqr_lld_conf_t   conf[2*GDP_GQR_MAX_VIEWPORTS_NUMBER]; // used for TOP and BOTTOM nodes for each viewport
};

#define DEBUGGDPPLUS(pGDP_Setup)\
{   \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_CTL  = %#.8x  GDPn_AGC  = %#.8x", (pGDP_Setup)->CTL,  (pGDP_Setup)->AGC ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_VPO  = %#.8x  GDPn_VPS  = %#.8x", (pGDP_Setup)->VPO,  (pGDP_Setup)->VPS ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_PML  = %#.8x  GDPn_PMP  = %#.8x", (pGDP_Setup)->PML,  (pGDP_Setup)->PMP ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_SIZE = %#.8x  GDPn_NVN  = %#.8x", (pGDP_Setup)->SIZE, (pGDP_Setup)->NVN ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_KEY1 = %#.8x  GDPn_KEY2 = %#.8x", (pGDP_Setup)->KEY1, (pGDP_Setup)->KEY2); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_HFP  = %#.8x  GDPn_PPT  = %#.8x", (pGDP_Setup)->HFP,  (pGDP_Setup)->PPT ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_VFP  = %#.8x  GDPn_CML  = %#.8x", (pGDP_Setup)->VFP,  (pGDP_Setup)->CML ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_CROP = %#.8x  GDPn_BT0  = %#.8x", (pGDP_Setup)->CROP, (pGDP_Setup)->BT0 ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_BT1  = %#.8x  GDPn_BT2  = %#.8x", (pGDP_Setup)->BT1,  (pGDP_Setup)->BT2 ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_BT3  = %#.8x  GDPn_BT4  = %#.8x", (pGDP_Setup)->BT3,  (pGDP_Setup)->BT4 ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_HSRC = %#.8x  GDPn_HIP  = %#.8x", (pGDP_Setup)->HSRC, (pGDP_Setup)->HIP ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_HP1  = %#.8x  GDPn_HP2  = %#.8x", (pGDP_Setup)->HP1,  (pGDP_Setup)->HP2 ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_VSRC = %#.8x  GDPn_VIP  = %#.8x", (pGDP_Setup)->VSRC, (pGDP_Setup)->VIP ); \
  PLANE_TRC( TRC_ID_GDP_PLUS_REG, "GDPn_VP1  = %#.8x  GDPn_VP2  = %#.8x", (pGDP_Setup)->VP1,  (pGDP_Setup)->VP2 ); \
}

typedef struct
{
  bool                      isValid;
  gdpgqr_lld_hw_viewport_s  topNode;
  gdpgqr_lld_hw_viewport_s  botNode;
  stm_buffer_presentation_t info;
  BufferNodeType            nodeType;
  uint32_t                  pictureId;
  bool                      areIOWindowsValid;
  // Filter coefficients
  const uint32_t            * hfluma;
  const uint32_t            * hfchroma;
  const uint32_t            * vfluma;
  const uint32_t            * vfchroma;
} GdpPlusSetup_t;

class CGdpPlusPlane: public CDisplayPlane
{
  public:
    CGdpPlusPlane(const char     *name,
              uint32_t        id,
              const CDisplayDevice *pDev,
              const stm_plane_capabilities_t caps,
              gdpgqr_lld_profile_e           profile,
              const char     *pixClockName,
              const char     *procClockName,
              uint32_t        max_viewports = 1,
              bool            bHasClut = true);

    ~CGdpPlusPlane();

    virtual bool GetCompoundControlRange(stm_display_plane_control_t selector, stm_compound_control_range_t *range);
    virtual bool IsFeatureApplicable( stm_plane_feature_t feature, bool *applicable) const;

    virtual bool Create(void);

    virtual DisplayPlaneResults SetControl(stm_display_plane_control_t control, uint32_t value);
    virtual DisplayPlaneResults GetControl(stm_display_plane_control_t control, uint32_t *value) const;

    virtual DisplayPlaneResults SetCompoundControl(stm_display_plane_control_t  ctrl, void * newVal);
    virtual DisplayPlaneResults GetCompoundControl(stm_display_plane_control_t  ctrl, void * currentVal);

    virtual void ClearContextFlags                   (void);
    virtual void PresentDisplayNode(CDisplayNode *pPrevNode,
                                    CDisplayNode *pCurrNode,
                                    CDisplayNode *pNextNode,
                                    bool isPictureRepeated,
                                    bool isInterlaced,
                                    bool isTopFieldOnDisplay,
                                    const stm_time64_t &vsyncTime);
    bool SetFirstGDPPlusConfigOwner(CGdpPlusPlane *);
    CGdpPlusPlane *GetFirstGDPPlusConfigOwner(void) const { return m_FirstGDPPlusConfigOwner; }
    CGdpPlusPlane *GetNextGDPPlusConfigOwner(void) const { return m_NextGDPPlusConfigOwner; }
    /*
     * Power Managment stuff
     */
    virtual void Freeze(void);
    virtual void Resume(void);

    virtual uint32_t GetIrrXout(void);

  protected:
    bool setNodeColourFmt(gdpgqr_lld_hw_viewport_s       &topNode,
                          gdpgqr_lld_hw_viewport_s       &botNode);

    virtual bool setNodeColourKeys(gdpgqr_lld_hw_viewport_s       &topNode,
                                   gdpgqr_lld_hw_viewport_s       &botNode);

    bool setNodeAlphaGain(gdpgqr_lld_hw_viewport_s       &topNode,
                          gdpgqr_lld_hw_viewport_s       &botNode);

    virtual bool setNodeResizeAndFilters(gdpgqr_lld_hw_viewport_s     &topNode,
                                         gdpgqr_lld_hw_viewport_s     &botNode,
                                         bool isDisplayInterlaced);

    virtual bool setOutputViewport(gdpgqr_lld_hw_viewport_s           &topNode,
                           gdpgqr_lld_hw_viewport_s                   &botNode,
                           bool isDisplayInterlaced);

    bool setNodePixelRepeat(gdpgqr_lld_hw_viewport_s                  &topNode,
                            gdpgqr_lld_hw_viewport_s                  &botNode);

    bool setMemoryAddressing(gdpgqr_lld_hw_viewport_s                 &topNode,
                             gdpgqr_lld_hw_viewport_s                 &botNode,
                             bool isDisplayInterlaced);

    bool setNodeGamutMatrix(gdpgqr_lld_hw_viewport_s                  &topNode,
                            gdpgqr_lld_hw_viewport_s                  &botNode);

    virtual bool SetupHDROutFormat(void);

    void updateBaseAddress(void);

    void WriteConfigForNextVsync(bool isDisplayInterlaced);

    void CalculateViewport(void);

    void CalculateHorizontalScaling(void);

    void CalculateVerticalScaling(void);

    void AdjustBufferInfoForScaling(gdpgqr_lld_hw_viewport_s                 &topNode,
                                    gdpgqr_lld_hw_viewport_s                 &botNode,
                                    bool isDisplayInterlaced);

    virtual bool   AdjustIOWindowsForHWConstraints      (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo) const;
    virtual bool   IsScalingPossible(CDisplayNode* pCurrNode, CDisplayInfo* pDisplayInfo);

    bool FillDisplayInfo(void);

    /*
     * GDP Plus Setup management. This is current setup which we are building
     * for next VSync.
     */
    void ResetGdpSetup(void);
    bool PrepareGdpSetup(bool isDisplayInterlaced);
    bool SetupDynamicGdpSetup(void);
    bool ApplyGdpSetup(bool isDisplayInterlaced,
                       bool isTopFieldOnDisplay,
                 const stm_time64_t &vsyncTime);

    bool SetupProgressiveNode(void);
    bool SetupSimpleInterlacedNode(void);

    void ProcessLastVsyncStatus(const stm_time64_t &vsyncTime, CDisplayNode *pNodeDisplayed);

    uint32_t PackRegister(uint16_t hi,uint16_t lo) const { return (hi<<16) | lo; }

    virtual void setNodeVisbility(gdpgqr_lld_hw_viewport_s *node, bool bForceOnMixer);

    // Manage HDR Gain and Offset
    void FillHDRGainOffset (stm_hdr_gain_offset_t       * const dst,
                             const stm_hdr_gain_offset_t * const src) const;

    /* VBI support */
    CGdpPlusPlane *m_LinkToGDPPlus;
    CGdpPlusPlane *m_FirstGDPPlusConfigOwner;
    CGdpPlusPlane *m_NextGDPPlusConfigOwner;

    /* Filtering */
    bool  m_bHasVFilter;
    CGdpPlusFilter m_Filter;

    bool  m_bHasClut;
    bool  m_b4k2k;

    uint32_t m_ulGain;
    uint32_t m_ulAlphaRamp;
    uint32_t m_ulStaticAlpha[2];

    uint32_t m_ulDirectBaseAddress;
    uint32_t m_ulQueueBaseAddress;

    /* Retreive previous plane visibility state when Plane is frozen */
    bool            m_wasEnabled;

    CDisplayNode           *m_pNodeToDisplay;
    CGdpPlusDisplayInfo     m_gdpDisplayInfo;
    GdpPlusSetup_t          m_GdpPlusSetup;

    bool  m_IsTransparencyChanged;
    bool  m_IsGainChanged;
    bool  m_IsColorKeyChanged;

    /* Hardware Gamut LUT support for BT709 -> BT2020 conversion */
    const uint32_t             *m_pGamutMatrix;

    /* HDROut support through Gain/Offset adjustments */
    uint32_t    m_ulHDRGain;
    uint32_t    m_ulHDROffset;
    stm_hdr_gain_offset_t m_HDRGainOffset;

    /* Pixel Repeat (Pixel Doubling) */
    bool  m_bHasPixelRepeat;

    /* Is Issue Rate Regulation supported by plane */
    bool     m_bHasIrrSupported;

  private:
    /* Specific LLD part */
    gdpgqr_lld_hdl_t            m_lldHandle;
    gdpgqr_lld_mem_desc_s       m_lldMemDesc;
    gdpgqr_lld_profile_e        m_lldProfile;
    DMA_Area                    m_lldCmdBuffer;
    GdpPlusConf_s               m_gdpConf;
    uint32_t                    m_MaxViewPorts;

    /* GDP+ Processing clock rate in MHz */
    uint32_t                    m_clockFreqInMHz;

    /* GDP Processing and Pixel clock tolerances in Ns (+/-50 ppm) */
    uint64_t                    m_procClockToleranceInNs;

    /*
     * Detect unexpected field inversion by checking current display
     * use case versus last programmed use case.
     */
    stm_display_use_cases_t m_currentDisplayUseCase;
    stm_display_use_cases_t m_lastProgrammedUseCase;
    bool m_unexpectedFieldInversion;

    CGdpPlusPlane(const CGdpPlusPlane&);
    CGdpPlusPlane& operator=(const CGdpPlusPlane&);
    void EnableGdpPlusHW(void);
    void DisableHW(void);
    int ScaleVerticalSamplePosition(int pos, CDisplayInfo DisplayInfo);

    void InitializeState(bool bHasClut, gdpgqr_lld_profile_e profile, uint32_t max_viewports);

    /* Specific LLD part */
    bool AllocateLldMemory(
                    const gdpgqr_lld_mem_need_s&  lldMemNeeds,
                    DMA_Area*                     pLldConfigBuffer) const;
    void FreeLldMemory(DMA_Area*                  pLldConfigBuffer) const;

    bool OpenLldSession(
                    const gdpgqr_lld_mem_desc_s&  lldMemDesc);
    void CloseLldSession();

    bool PrepareConf(
                    gdpgqr_lld_hw_viewport_s       *topNode,
                    gdpgqr_lld_hw_viewport_s       *botNode,
                    bool                            isTopFieldOnDisplay,
                    bool                            isDisplayInterlaced);
    bool UpdateConf(const GdpPlusConf_s            *pGdpPlusConf);
    void ApplyConf(const GdpPlusConf_s             *pGdpPlusConf);

    void StopGdpPlus(void);

    unsigned int GetNumberOfHwRequired(void) const;
    unsigned int GetMaxLinesSkippableByHw(unsigned int srcPicturePitch) const;

    bool IsScalingPossibleByHw(CDisplayInfo*        pDisplayInfo);
    bool IsScalingPossibleBySkippingLines(
                    CDisplayNode*                   pCurrNode,
                    CDisplayInfo*                   pDisplayInfo);
    bool IsHwProcessingTimeOk(
                    uint32_t                        vhsrcInputWidth,
                    uint32_t                        vhsrcInputHeight,
                    uint32_t                        vhsrcOutputWidth,
                    uint32_t                        vhsrcOutputHeight) const;

    void TruncateSourceToHWLimits(CDisplayInfo* pDisplayInfo);

    void CheckForUnexpectedFieldInversion(bool isDisplayInterlaced,
                                                bool isTopFieldOnDisplay);

 /* Irr support */
    bool InitIrrParams(void);
    bool ApplyIrrSetup(void);
    bool UpdateIrrSetup(void);

    uint32_t m_fifo_size;
    uint64_t m_RequesteBW;
    uint64_t m_IrrLinkCapacity;
    uint64_t m_NextRBW;
    uint64_t m_CurrentRBW;
    uint64_t m_PrevRBW;
};


#endif /* _GDP_PLUS_PLANE_H */
