/***********************************************************************
 *
 * File: display/ip/gdpplus/GdpPlus..h
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/
#ifndef _GDP_PLUS_H_
#define _GDP_PLUS_H_

#include <display/ip/gdpplus/lld/gdpgqr_lld_api.h>

struct GdpPlusHwInfo_s
{
    unsigned int        offset;         // Offset of GDP Plus instance in display device
    uint32_t            mixerBit;       // Mixer bit associated with GDP Plus instance
};

class CGDPPlus
{
    // Only CGdpPlusPlane is allowed to access private methods
    friend class CGdpPlusPlane;
public:
    static bool Init(
                    const void*             pDisplayDevBase,
                    int                     gdpHwNb,
                    const GdpPlusHwInfo_s   gdpHwInfo[]);

    static bool DeInit();

protected:

private:
    static bool     OpenLldSession(uint32_t                          plane_id,
                              enum gdpgqr_lld_profile_e          profile,
                              const gdpgqr_lld_mem_desc_s *      mem_for_cmd,
                              uint32_t                         * hw_used_bit_mask,
                              gdpgqr_lld_hdl_t                 * lldHandle);

    static bool     CloseLldSession(uint32_t                         plane_id,
                              gdpgqr_lld_hdl_t                  * lldHandle,
                              uint32_t                          * nb_remaining_handles);

    CGDPPlus(const CGDPPlus&);
    CGDPPlus& operator=(const CGDPPlus&);
};

#endif /* _GDP_PLUS_H_ */
