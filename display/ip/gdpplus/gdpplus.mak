ifneq ($(CONFIG_GDP_PLUS),)
include $(STG_TOPDIR)/display/ip/gdpplus/lld/lld.mak

STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/gdpplus/,      \
                        GdpPlusPlane.cpp                             \
                        GdpPlusFilter.cpp                            \
                        GdpPlus.cpp)

endif
