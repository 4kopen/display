/***********************************************************************
 *
 * File: display/ip/displaytiming/stmclocklla.cpp
 * Copyright (c) 2013 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/
#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include "stmclocklla.h"

CSTmClockLLA::CSTmClockLLA( stm_clock_t *clk_src_map, uint32_t clk_src_mapsize
                          , stm_clock_t *clk_out_map, uint32_t clk_out_mapsize
                          , CDisplayDevice *pDev)
{
  TRCIN( TRC_ID_CLOCK, "" );

  uint32_t i;

  m_sourceMap   = clk_src_map;
  m_nSrcMapSize = clk_src_mapsize;
  m_outputMap   = clk_out_map;
  m_nOutMapSize = clk_out_mapsize;
  m_pDev        = pDev;

  m_bIsSuspended = false;

  ASSERTF(m_sourceMap,("m_sourceMap should be valid at creation time!'\n"));
  ASSERTF(m_outputMap,("m_outputMap should be valid at creation time!'\n"));

  TRC( TRC_ID_CLOCK, "SRC[%p:%d] - OUT[%p:%d].", m_sourceMap, m_nSrcMapSize, m_outputMap, m_nOutMapSize );

  /* Get SRC clocks */
  for(i=0; i<m_nSrcMapSize; i++)
  {
    if(!vibe_os_clk_get(m_sourceMap[i].name, &(m_sourceMap[i].clock)) == 0)
    {
      TRC( TRC_ID_ERROR, "Failed to get clock %s", m_sourceMap[i].name );
    }
  }

  /* Get OUT clocks */
  for(i=0; i<m_nOutMapSize; i++)
  {
    if(vibe_os_clk_get(m_outputMap[i].name, &(m_outputMap[i].clock)) == 0)
    {
      if(!m_pDev->BypassHwInitialization())
      {
        TRC( TRC_ID_CLOCK, "Enabling Clock %s", m_outputMap[i].name );
        vibe_os_clk_enable(&(m_outputMap[i].clock));
      }
    }
  }

  TRCOUT( TRC_ID_CLOCK, "" );
}


CSTmClockLLA::~CSTmClockLLA(void)
{
  uint32_t i;

  /*
   * Warn if still enabled clocks while destroying this object
   */
  for(i=0; i<m_nOutMapSize; i++)
  {
    if(m_outputMap[i].clock.enabled)
      TRC( TRC_ID_ERROR, "Clock %s is still enabled", m_outputMap[i].name );
  }

  /* Put OUT clocks */
  for(i=0; i<m_nOutMapSize; i++)
    vibe_os_clk_put(&(m_outputMap[i].clock));

  /* Put SRC clocks */
  for(i=0; i<m_nSrcMapSize; i++)
    vibe_os_clk_put(&(m_sourceMap[i].clock));
}


bool CSTmClockLLA::Enable(stm_clk_divider_output_name_t   name,
                                stm_clk_divider_output_source_t src,
                                stm_clk_divider_output_divide_t div)
{
  struct vibe_clk *source=0;
  struct vibe_clk *output=0;
  struct vibe_clk current_parent;
  uint32_t rate;

  if(!lookupOutput(name, &output))
  {
    TRC( TRC_ID_ERROR, "OUT Clock %d NOT FOUND!!", name );
    return false;
  }

  TRC( TRC_ID_CLOCK, "Enabling Clock %s", output->name );
  if(!output->enabled)
    vibe_os_clk_enable(output);

  if(src == STM_CLK_SRC_SPARE)
    return true;

  if(!lookupSource(src, &source))
  {
    TRC( TRC_ID_ERROR, "SRC Clock %d NOT FOUND!!", src );
    return false;
  }

  rate =  vibe_os_clk_get_rate(source) >> div;

  TRC( TRC_ID_CLOCK, "Updating Clock %s (src is %s, div = %d, rate = %d)", output->name, source->name, (1 << div), rate );

  /*
   * Check if the new clock parent is the same as the old one
   * No need to re-parent if it's the same
   */
  vibe_os_clk_get_parent(output, &current_parent);

  if (current_parent.clk != source->clk)
    vibe_os_clk_set_parent(output, source);
  else
    TRC(TRC_ID_MAIN_INFO, "No need to re-parent %s", output->name );

  vibe_os_clk_set_rate(output, rate);

  return true;
}


bool CSTmClockLLA::Disable(stm_clk_divider_output_name_t name)
{
  struct vibe_clk *output=0;

  if(!lookupOutput(name, &output))
  {
    TRC( TRC_ID_ERROR, "Clock %d NOT FOUND!!", name );
    return false;
  }

  TRC( TRC_ID_CLOCK, "Disabling Clock %s", output->name );
  vibe_os_clk_disable(output);

  return true;
}


bool CSTmClockLLA::SetRate(stm_clk_divider_output_name_t name, uint32_t rate)
{
  struct vibe_clk *output=0;

  if(!lookupOutput(name, &output))
  {
    TRC( TRC_ID_ERROR, "OUT Clock %d NOT FOUND!!", name );
    return false;
  }

  TRC( TRC_ID_CLOCK, "Setting Clock %s rate to %d", output->name, rate );

  vibe_os_clk_set_rate(output, rate);

  return true;
}


uint32_t CSTmClockLLA::GetRate(stm_clk_divider_output_name_t name)
{
  struct vibe_clk *output=0;

  if(!lookupOutput(name, &output))
  {
    TRC( TRC_ID_ERROR, "OUT Clock %d NOT FOUND!!", name );
    return 0;
  }

  uint32_t rate = vibe_os_clk_get_rate(output);

  TRC( TRC_ID_CLOCK, "Clock %s rate is %d", output->name, rate );

  return rate;
}


bool CSTmClockLLA::SetParent(stm_clk_divider_output_name_t name, stm_clk_divider_output_source_t src)
{
  struct vibe_clk *source=0;
  struct vibe_clk *output=0;
  struct vibe_clk current_parent;

  if(!lookupSource(src, &source))
  {
    TRC( TRC_ID_ERROR, "SRC Clock %d NOT FOUND!!", src );
    return false;
  }

  if(!lookupOutput(name, &output))
  {
    TRC( TRC_ID_ERROR, "OUT Clock %d NOT FOUND!!", name );
    return false;
  }

  TRC( TRC_ID_CLOCK, "Setting Clock %s parent to %s", output->name, source->name );

  /*
   * Check if the new clock parent is the same as the old one
   * No need to re-parent if it's the same
   */
  vibe_os_clk_get_parent(output, &current_parent);

  if (current_parent.clk != source->clk)
    vibe_os_clk_set_parent(output, source);
  else
    TRC(TRC_ID_MAIN_INFO, "No need to re-parent %s", output->name );

  return true;
}


bool CSTmClockLLA::isEnabled(stm_clk_divider_output_name_t name) const
{
  struct vibe_clk *output=0;
  TRCIN( TRC_ID_CLOCK, "" );

  if(!lookupOutput(name, &output))
  {
    TRC( TRC_ID_ERROR, "Clock %d NOT FOUND!!", name );
    return false;
  }

  bool enabled = output->enabled == 0 ? false : true;

  TRCOUT( TRC_ID_CLOCK, "" );
  return enabled;
}


bool CSTmClockLLA::getDivide(stm_clk_divider_output_name_t    name,
                               stm_clk_divider_output_divide_t *div) const
{
  bool ret = true;
  struct vibe_clk *output=0;
  struct vibe_clk parent;
  TRCIN( TRC_ID_CLOCK, "" );

  if(!lookupOutput(name, &output))
  {
    TRC( TRC_ID_ERROR, "Clock %d NOT FOUND!!", name );
    return false;
  }

  if(vibe_os_clk_get_parent(output, &parent))
  {
    TRC( TRC_ID_ERROR, "Failed to get clock parent for %s !!", output->name );
    return false;
  }

  uint32_t d = vibe_os_clk_get_rate(&parent)/vibe_os_clk_get_rate(output);

  switch(d)
    {
    case 1:
      *div = STM_CLK_DIV_1;
      break;
    case 2:
      *div = STM_CLK_DIV_2;
      break;
    case 4:
      *div = STM_CLK_DIV_4;
      break;
    case 8:
      *div = STM_CLK_DIV_8;
      break;
    default:
      ret = false; // Impossible but keep the compiler quiet.
  }

  TRCOUT( TRC_ID_CLOCK, "" );
  return ret;
}


int CSTmClockLLA::Suspend(void)
{
  uint32_t i;

  if(m_bIsSuspended)
    return 0;

  TRCIN( TRC_ID_CLOCK, "" );

  /* Suspend OUT */
  for(i=0; i<m_nOutMapSize; i++)
    vibe_os_clk_suspend(&(m_outputMap[i].clock));

  m_bIsSuspended = true;

  TRCOUT( TRC_ID_CLOCK, "" );
  return 0;
}


int CSTmClockLLA::Resume(void)
{
  uint32_t i;

  if(!m_bIsSuspended)
    return 0;

  TRCIN( TRC_ID_CLOCK, "" );

  /* Resume OUT */
  for(i=0; i<m_nOutMapSize; i++)
    vibe_os_clk_resume(&(m_outputMap[i].clock));

  m_bIsSuspended = false;

  TRCOUT( TRC_ID_CLOCK, "" );
  return 0;
}
