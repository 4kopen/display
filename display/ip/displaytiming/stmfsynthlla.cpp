/***********************************************************************
 *
 * File: display/ip/displaytiming/stmfsynthlla.cpp
 * Copyright (c) 2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/
#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/generic/DisplayDevice.h>

#include "stmfsynthlla.h"

CSTmFSynthLLA::CSTmFSynthLLA( const char *fsClockName,
                              const char *pixelClockName): CSTmFSynth()
{
  int ret = 0;
  TRCIN( TRC_ID_FSYNTH, "FS Clock is '%s' - Pixel clock is '%s'", fsClockName, pixelClockName);

  ASSERTF( ((fsClockName[0] != '\0') && (pixelClockName[0] != '\0'))
         , ("clock names should be valid at creation time!'\n"));

  vibe_os_snprintf (m_fsClockName, sizeof(m_fsClockName), "%s",
                    fsClockName);
  vibe_os_snprintf (m_pixelClockName, sizeof(m_pixelClockName), "%s",
                    pixelClockName);

  // Get the clocks
  m_clk_fs.clk     = 0;
  ret = vibe_os_clk_get(m_fsClockName, &m_clk_fs);
  if (ret)
  {
    TRC(TRC_ID_ERROR, "Failed to get clock %s", m_fsClockName );
    goto exit;
  }

  m_clk_pix.clk     = 0;
  ret = vibe_os_clk_get(m_pixelClockName, &m_clk_pix);
  if (ret)
  {
    TRC(TRC_ID_ERROR, "Failed to get clock %s", m_pixelClockName );
    goto exit;
  }

  m_IsStarted = false;

exit:
  ASSERTF((!ret), ("failed to create Clock LLA module'\n"));
  TRCOUT( TRC_ID_FSYNTH, "" );
}


CSTmFSynthLLA::~CSTmFSynthLLA()
{
  TRCIN( TRC_ID_FSYNTH, "" );

  vibe_os_clk_put(&m_clk_fs);
  vibe_os_clk_put(&m_clk_pix);

  TRCOUT( TRC_ID_FSYNTH, "" );
}


bool CSTmFSynthLLA::Start(uint32_t ulFrequency)
{
  TRCIN( TRC_ID_UNCLASSIFIED, "" );

  if(!CSTmFSynth::Start(ulFrequency))
    return false;

  TRC( TRC_ID_FSYNTH, "Starting Clock %s (rate = %d)", m_fsClockName, m_CurrentTiming.fout );

  if(vibe_os_clk_set_rate(&m_clk_fs, m_CurrentTiming.fout) < 0)
  {
    TRC( TRC_ID_ERROR, "Failed to set clock rate for %s (rate = %d)", m_fsClockName, m_CurrentTiming.fout );
    return false;
  }

  if(!m_IsStarted)
  {
    TRC( TRC_ID_FSYNTH, "Enabling Clock %s", m_pixelClockName );
    vibe_os_clk_enable(&m_clk_pix);
    m_IsStarted = true;
  }

  TRCOUT( TRC_ID_UNCLASSIFIED, "" );
  return true;
}


void CSTmFSynthLLA::Stop(void)
{
  TRCIN( TRC_ID_UNCLASSIFIED, "" );

  CSTmFSynth::Stop();

  if(m_IsStarted)
  {
    TRC( TRC_ID_FSYNTH, "Disabling Clock %s", m_pixelClockName );
    vibe_os_clk_disable(&m_clk_pix);
    m_IsStarted = false;
  }

  TRCOUT( TRC_ID_UNCLASSIFIED, "" );
}


bool CSTmFSynthLLA::SolveFsynthEqn(uint32_t Fout, stm_clock_fsynth_timing_t *timing) const
{
  TRCIN( TRC_ID_UNCLASSIFIED, "" );

  /* update the outgoing arguments */
  timing->fout = Fout;
  timing->sdiv = 0;
  timing->md   = 0;
  timing->pe   = 0;

  TRCOUT( TRC_ID_UNCLASSIFIED, "" );
  return true;
}


bool CSTmFSynthLLA::SetAdjustment(int ppm)
{
  TRCIN( TRC_ID_UNCLASSIFIED, "" );

  if(!CSTmFSynth::SetAdjustment(ppm))
    return false;

  TRC( TRC_ID_FSYNTH, "Adjusting Clock %s to %d", m_fsClockName, m_CurrentTiming.fout );
  if(vibe_os_clk_set_rate(&m_clk_fs, m_CurrentTiming.fout) < 0)
  {
    TRC( TRC_ID_ERROR, "Failed to set clock rate for %s (rate = %d)", m_fsClockName, m_CurrentTiming.fout );
    return false;
  }

  TRCOUT( TRC_ID_UNCLASSIFIED, "" );
  return true;
}
