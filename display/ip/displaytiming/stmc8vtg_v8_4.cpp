/***********************************************************************
 *
 * File: display/ip/displaytiming/stmc8vtg_v8_4.cpp
 * Copyright (c) 2011 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/generic/DisplayDevice.h>

#include "stmc8vtg_v8_4.h"


/* VTG offset registers ---------------------------------------------------*/
#define VTGn_VTGMOD           0x0000
#define VTGn_INV_BNOTTOP      0x0004                                    // New register
#define VTGn_CLKLN            0x0008
#define VTGn_WINDOW_OFFSET    0x0040
#define VTGn_H_HD_1           0x00C0
#define VTGn_TOP_V_VD_1       0x00C4
#define VTGn_BOT_V_VD_1       0x00C8
#define VTGn_TOP_V_HD_1       0x00CC
#define VTGn_BOT_V_HD_1       0x00D0
#define VTGn_H_HD_2           0x00E0
#define VTGn_TOP_V_VD_2       0x00E4
#define VTGn_BOT_V_VD_2       0x00E8
#define VTGn_TOP_V_HD_2       0x00EC
#define VTGn_BOT_V_HD_2       0x00F0
#define VTGn_H_HD_3           0x0100
#define VTGn_TOP_V_VD_3       0x0104
#define VTGn_BOT_V_VD_3       0x0108
#define VTGn_TOP_V_HD_3       0x010C
#define VTGn_BOT_V_HD_3       0x0110
#define VTGn_H_HD_4           0x0120
#define VTGn_TOP_V_VD_4       0x0124
#define VTGn_BOT_V_VD_4       0x0128
#define VTGn_TOP_V_HD_4       0x012C
#define VTGn_BOT_V_HD_4       0x0130
#define VTGn_H_HD_5           0x0140
#define VTGn_TOP_V_VD_5       0x0144
#define VTGn_BOT_V_VD_5       0x0148
#define VTGn_TOP_V_HD_5       0x014C
#define VTGn_BOT_V_HD_5       0x0150
#define VTGn_H_HD_6           0x0160
#define VTGn_TOP_V_VD_6       0x0164
#define VTGn_BOT_V_VD_6       0x0168
#define VTGn_TOP_V_HD_6       0x016C
#define VTGn_BOT_V_HD_6       0x0170

#define VTGn_HLFLN            0x000C
#define VTGn_DRST             0x0010
#define VTGn_LN_INT           0x0014
#define VTGn_LN_STAT          0x0018
#define VTGn_DFV              0x0070
#define VTGn_HOST_STA         0x0074
#define VTGn_HOST_ITS         0x0078
#define VTGn_HOST_ITS_BCLR    0x007C
#define VTGn_HOST_ITS_BSET    0x0080
#define VTGn_HOST_ITM         0x0084
#define VTGn_HOST_ITM_BCLR    0x0088
#define VTGn_HOST_ITM_BSET    0x008C
#define VTGn_RT_STA           0x00A4
#define VTGn_RT_ITS           0x00A8
#define VTGn_RT_ITS_BCLR      0x00AC
#define VTGn_RT_ITS_BSET      0x00B0
#define VTGn_RT_ITM           0x00B4
#define VTGn_RT_ITM_BCLR      0x00B8
#define VTGn_RT_ITM_BSET      0x00BC

#if defined(CONFIG_STM_HOST_CPU)
static const int VTGn_STA       =       VTGn_HOST_STA;       // HOST
static const int VTGn_ITM       =       VTGn_HOST_ITM;       // HOST
static const int VTGn_ITM_BCLR  =       VTGn_HOST_ITM_BCLR;  // HOST
static const int VTGn_ITM_BSET  =       VTGn_HOST_ITM_BSET;  // HOST
static const int VTGn_ITS       =       VTGn_HOST_ITS;       // HOST
static const int VTGn_ITS_BCLR  =       VTGn_HOST_ITS_BCLR;  // HOST
static const int VTGn_ITS_BSET  =       VTGn_HOST_ITS_BSET;  // HOST
#else
static const int VTGn_STA       =       VTGn_RT_STA;         // RT
static const int VTGn_ITM       =       VTGn_RT_ITM;         // RT
static const int VTGn_ITM_BCLR  =       VTGn_RT_ITM_BCLR;    // RT
static const int VTGn_ITM_BSET  =       VTGn_RT_ITM_BSET;    // RT
static const int VTGn_ITS       =       VTGn_RT_ITS;         // RT
static const int VTGn_ITS_BCLR  =       VTGn_RT_ITS_BCLR;    // RT
static const int VTGn_ITS_BSET  =       VTGn_RT_ITS_BSET;    // RT
#endif

#define VTG_ITS_VSB (1L<<0)
#define VTG_ITS_VST (1L<<1)
#define VTG_ITS_LNB (1L<<4)
#define VTG_ITS_LNT (1L<<5)

#define STM_C8VTG_MAX_SYNC_OUTPUTS 6


static const stm_vtg_reg_offsets_t vtg_regs[STM_C8VTG_MAX_SYNC_OUTPUTS] = {
  { VTGn_H_HD_1, VTGn_TOP_V_VD_1, VTGn_BOT_V_VD_1, VTGn_TOP_V_HD_1, VTGn_BOT_V_HD_1 },
  { VTGn_H_HD_2, VTGn_TOP_V_VD_2, VTGn_BOT_V_VD_2, VTGn_TOP_V_HD_2, VTGn_BOT_V_HD_2 },
  { VTGn_H_HD_3, VTGn_TOP_V_VD_3, VTGn_BOT_V_VD_3, VTGn_TOP_V_HD_3, VTGn_BOT_V_HD_3 },
  { VTGn_H_HD_4, VTGn_TOP_V_VD_4, VTGn_BOT_V_VD_4, VTGn_TOP_V_HD_4, VTGn_BOT_V_HD_4 },
  { VTGn_H_HD_5, VTGn_TOP_V_VD_5, VTGn_BOT_V_VD_5, VTGn_TOP_V_HD_5, VTGn_BOT_V_HD_5 },
  { VTGn_H_HD_6, VTGn_TOP_V_VD_6, VTGn_BOT_V_VD_6, VTGn_TOP_V_HD_6, VTGn_BOT_V_HD_6 }
};


CSTmC8VTG_V8_4::CSTmC8VTG_V8_4(
    const char          *name,
    CDisplayDevice      *pDev,
    uint32_t             uRegOffset,
    uint32_t             uNumSyncOutputs,
    CSTmFSynth          *pFSynth,
    bool                 bDoubleClocked,
    stm_vtg_sync_type_t  refpol,
    bool                 bDisableSyncsOnStop,
    bool                 bUseSlaveInterrupts): CSTmC8VTG(name,
                                                         pDev,
                                                         uRegOffset,
                                                         uNumSyncOutputs,
                                                         pFSynth,
                                                         bDoubleClocked,
                                                         refpol,
                                                         bDisableSyncsOnStop,
                                                         bUseSlaveInterrupts)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  m_syncRegOffsets = vtg_regs;
  m_uWindowRegistersOffset = VTGn_WINDOW_OFFSET;

  /*
   * This is a temporary override of the default behaviour, for the current
   * dual die configuration of the V8.4 VTG used on the STiH416/STiH407.
   */
  m_bUseOddEvenHSyncFreerun = false;

  m_dbg_count = 0;
  m_mismatched_sync_dbg_count = 5;

  /*
   * Set interrupt configuration
   */
  m_InterruptMask = (VTG_ITS_VST | VTG_ITS_VSB);

  m_lastvsync = 0ULL;

  m_bHwInitDone = false;

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


CSTmC8VTG_V8_4::~CSTmC8VTG_V8_4() {}


void CSTmC8VTG_V8_4::InitializeHardware()
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  /* This is will be done only once at the VTG first start */
  if(!m_bHwInitDone)
  {
    /*
     * Ensure the VTG is initially disabled, for instance in case this driver
     * is being loaded after a boot loader has brought up a splash screen.
     *
     * Note: This is slight overkill, at least for current use on STiH416/STiH407, as
     *       the requirements of the dual VTG slaving means we have to do
     *       a hard reset of the blocks in the device class to put the VTGs back
     *       into a "clean" state to cope with the splash screen use case.
     */

    WriteVTGReg(VTGn_ITM_BCLR, (VTG_ITS_VST | VTG_ITS_VSB | VTG_ITS_LNT | VTG_ITS_LNB));

    CSTmC8VTG_V8_4::DisableSyncs();

    m_bHwInitDone = true;
  }

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


void CSTmC8VTG_V8_4::ProgramVTGTimings(const stm_display_mode_t* pModeLine)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  stm_display_mode_timing_t TimingParams;

  // Get corresponding register parameters in that line
  TimingParams = pModeLine->mode_timing;

  uint32_t ulMOD = m_uVTGStartModeConfig;
  uint32_t clocksperline;

  if(m_bDoubleClocked)
    clocksperline = TimingParams.pixels_per_line*2;
  else
    clocksperline = TimingParams.pixels_per_line;

  // Set the number of clock cycles per line
  WriteVTGReg(VTGn_CLKLN, clocksperline);

  // Set Half Line Per Field
  if(pModeLine->mode_params.scan_type == STM_PROGRESSIVE_SCAN)
  { // Progressive
    WriteVTGReg(VTGn_HLFLN, 2*TimingParams.lines_per_frame);
  }
  else
  { // Interlaced
    WriteVTGReg(VTGn_HLFLN, TimingParams.lines_per_frame);
  }

  /*
   * Set the HRef pulse programming.
   */
  if ((m_syncParams[STM_SYNC_SEL_REF].m_syncType == STVTG_SYNC_NEGATIVE) ||
      (m_syncParams[STM_SYNC_SEL_REF].m_syncType == STVTG_SYNC_TIMING_MODE && !TimingParams.hsync_polarity))
  { // HRef is requested to be negative
      VTG_TRC( TRC_ID_UNCLASSIFIED, "Inverting HRef polarity" );
      ulMOD |= VTG_MOD_HREF_INV_POLARITY;
  }

  /*
   * Set VRef pulse programming
   */
  if ((m_syncParams[STM_SYNC_SEL_REF].m_syncType == STVTG_SYNC_NEGATIVE) ||
      (m_syncParams[STM_SYNC_SEL_REF].m_syncType == STVTG_SYNC_TIMING_MODE && !TimingParams.vsync_polarity))
  { // VRef is requested to be negative
    VTG_TRC( TRC_ID_UNCLASSIFIED, "Inverting VRef polarity" );
    ulMOD |= VTG_MOD_VREF_INV_POLARITY;
  }

  if (m_CurrentMode.mode_id == STM_TIMING_MODE_RESERVED)
    m_pendingSync = 0xFFFFFFFF;

  ProgramSyncOutputs(pModeLine);

  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTG_VTGMOD     %#.8x", ulMOD );
  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTG_CLKLN      %#.8x", ReadVTGReg(VTGn_CLKLN) );
  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTG_HLFLN      %#.8x", ReadVTGReg(VTGn_HLFLN) );
  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTGn_VID_TFO   %#.8x", ReadVTGReg(VTGn_VID_TFO) );
  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTGn_VID_TFS   %#.8x", ReadVTGReg(VTGn_VID_TFS) );
  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTGn_VID_BFO   %#.8x", ReadVTGReg(VTGn_VID_BFO) );
  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTGn_VID_BFS   %#.8x", ReadVTGReg(VTGn_VID_BFS) );

  WriteVTGReg(VTGn_VTGMOD, ulMOD);
  m_CurrentMode = *pModeLine;

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


void CSTmC8VTG_V8_4::EnableInterrupts(void)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  // Enable interrupts on VTG, clear any old pending status bits first.
  WriteVTGReg(VTGn_ITS_BCLR, 0xFF);
  WriteVTGReg(VTGn_ITM_BSET, m_InterruptMask);

  VTG_TRC( TRC_ID_UNCLASSIFIED, "VTG_ITS %#.8x", ReadVTGReg(VTGn_ITS) );

  /*
   * Reset interrupt debug to show the first 20 interrupts immediately after the
   * enable.
   */
  m_dbg_count = 20;

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


void CSTmC8VTG_V8_4::DisableInterrupts(void)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  // Disable interrupts on VTG
  vibe_os_lock_resource(m_lock);
  {
    WriteVTGReg(VTGn_ITM_BCLR, m_InterruptMask);
    m_lastvsync = 0ULL;
  }
  vibe_os_unlock_resource(m_lock);

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


void CSTmC8VTG_V8_4::ResetCounters(void)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  /*
   * Only reset the counters on a master VTG.
   */
  if(!m_bIsSlaveVTG)
  {
    if(m_bDisabled)
      WriteVTGReg(VTGn_DRST, 1);
    else
      m_bDoSoftResetInInterrupt = true;
  }

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


void CSTmC8VTG_V8_4::DisableSyncs(void)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  uint32_t var = ReadVTGReg(VTGn_VTGMOD) | VTG_MOD_DISABLE;
  WriteVTGReg(VTGn_VTGMOD, var);
  VTG_TRC( TRC_ID_VTG, "Disabling syncs expected value = %x VTGMOD = %x", var, ReadVTGReg(VTGn_VTGMOD) );
  m_bDisabled = true;

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


void CSTmC8VTG_V8_4::EnableSyncs(void)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  uint32_t var = ReadVTGReg(VTGn_VTGMOD) & ~VTG_MOD_DISABLE;
  WriteVTGReg(VTGn_VTGMOD, var);
  VTG_TRC( TRC_ID_VTG, "Enabling syncs expected value = %x VTGMOD = %x", var, ReadVTGReg(VTGn_VTGMOD) );
  m_bDisabled = false;

  VTG_TRCOUT( TRC_ID_VTG, "" );
}


bool CSTmC8VTG_V8_4::SetBnottopType(stm_display_sync_id_t sync, stm_vtg_sync_Bnottop_type_t type)
{
  VTG_TRCIN( TRC_ID_VTG, "" );

  uint32_t val;

  if(sync == STM_SYNC_SEL_REF)
    return false;

  val =  ReadVTGReg(VTGn_INV_BNOTTOP) & ~(1 << (sync - 1));
  if(type == STVTG_SYNC_BNOTTOP_INVERTED)
    val |=  (1 << (sync - 1));
  WriteVTGReg(VTGn_INV_BNOTTOP, val);

  VTG_TRCOUT( TRC_ID_VTG, "" );
  return true;
}


uint32_t CSTmC8VTG_V8_4::ReadHWInterruptStatus(void)
{
  uint32_t intStatus;

  intStatus =  ReadVTGReg(VTGn_ITS);

  if(intStatus != 0)
  {
    stm_time64_t now = vibe_os_get_system_time();

    WriteVTGReg(VTGn_ITS_BCLR, intStatus);
    (void)ReadVTGReg(VTGn_ITS); // sync bus write

    if(m_dbg_count > 0)
    {
      VTG_TRC( TRC_ID_UNCLASSIFIED, "ITS = 0x%x now = %lld elapsed = %lld", intStatus, now, (m_lastvsync == 0ULL)?0ULL:(now - m_lastvsync) );
      m_dbg_count--;
    }

    if((intStatus & (VTG_ITS_VST | VTG_ITS_VSB)) == (VTG_ITS_VST | VTG_ITS_VSB))
    {
      if (!m_bSuppressMissedInterruptMessage)
      {
        m_bSuppressMissedInterruptMessage = true;
        VTG_TRC( TRC_ID_UNCLASSIFIED, "TOP and BOTTOM flags asserted now = %lld elapsed = %lld", now,(m_lastvsync == 0ULL)?0ULL:(now - m_lastvsync) );
      }
    }
    else
    {
      m_bSuppressMissedInterruptMessage = false;
    }

    m_lastvsync = now;
  }

  return intStatus;
}


uint32_t CSTmC8VTG_V8_4::GetInterruptStatus(void)
{
  uint32_t intStatus,masterIntStatus,slaveIntStatus=0;
  uint32_t event = STM_TIMING_EVENT_NONE;

  vibe_os_lock_resource(m_lock);

  masterIntStatus =  ReadHWInterruptStatus();
  if(m_pSlavedVTG)
  {
    slaveIntStatus = m_pSlavedVTG->ReadHWInterruptStatus();
    if((slaveIntStatus & (VTG_ITS_VST | VTG_ITS_VSB)) != (masterIntStatus & (VTG_ITS_VST | VTG_ITS_VSB)))
    {
      if(m_mismatched_sync_dbg_count > 0)
      {
        VTG_TRC( TRC_ID_UNCLASSIFIED, "Master(0x%x)/Slave(0x%x) ITS Mismatch now = %lld", masterIntStatus, slaveIntStatus, vibe_os_get_system_time() );

        m_mismatched_sync_dbg_count--;

        if(m_mismatched_sync_dbg_count == 0)
          VTG_TRC( TRC_ID_UNCLASSIFIED, "Further ITS Mismatch messages suppressed");
      }
    }
    else
      m_mismatched_sync_dbg_count = 5; // Reset debug if we recover from error
  }

  intStatus = m_bUseSlaveInterrupts?slaveIntStatus:masterIntStatus;

  DASSERTF2((intStatus != 0),("CSTmC8VTG_V8_4::GetInterruptStatus: NULL interrupt status\n"), m_lock, STM_TIMING_EVENT_NONE);

  if((intStatus & (VTG_ITS_VST | VTG_ITS_VSB)) == (VTG_ITS_VST | VTG_ITS_VSB))
  {
    /*
     * We cannot risk accidentally resetting or re-programming the VTG when
     * we are not absolutely certain which state the VTG is in, otherwise we
     * might break it in an unrecoverable way. So simply return with something
     * appropriate and delay any other work until we have a known good state.
     */
    if(IsStarted())
      event = (m_CurrentMode.mode_params.scan_type == STM_INTERLACED_SCAN)?STM_TIMING_EVENT_BOTTOM_FIELD:STM_TIMING_EVENT_FRAME;
    else
      event = STM_TIMING_EVENT_NONE;

    goto unlock_and_exit;
  }

  /*
   * Defensive coding against catching an interrupt unexpectedly when the
   * VTG is configured to not be disabled when "stopped".
   */
  if(!IsStarted() && !IsPending() )
    goto unlock_and_exit;


  if(intStatus & VTG_ITS_VST)
  {
    if(IsPending() && m_bUpdateOnlyClockFrequency)
    {
      VTG_TRC( TRC_ID_UNCLASSIFIED, "Updating pending clock frequency = %uHz", m_PendingMode.mode_timing.pixel_clock_freq );
      m_pFSynth->Start(m_PendingMode.mode_timing.pixel_clock_freq);
      m_bUpdateOnlyClockFrequency = false;
      m_CurrentMode = m_PendingMode;
      m_PendingMode.mode_id = STM_TIMING_MODE_RESERVED;
    }

    if(IsStarted())
    {
      event |= STM_TIMING_EVENT_FRAME;
      if(m_CurrentMode.mode_params.scan_type == STM_INTERLACED_SCAN)
        event |= STM_TIMING_EVENT_TOP_FIELD;
    }
  }
  else if(intStatus & VTG_ITS_VSB)
  {
    if(IsStarted() && m_UpdateOutputRect)
    {
      ProgramOutputWindow(&m_CurrentMode);
      if(m_pSlavedVTG)
        m_pSlavedVTG->ProgramOutputWindow(&m_CurrentMode);
    }

    if(IsStarted())
    {
      if(m_pSlavedVTG && m_pSlavedVTG->m_pendingSync)
        {
          ((CSTmC8VTG_V8_4*)m_pSlavedVTG)->ProgramSyncOutputs(&m_CurrentMode);
          m_pSlavedVTG->m_pendingSync = 0;
        }
      if(m_pendingSync)
        ProgramSyncOutputs(&m_CurrentMode);
    }

    if(IsPending() && !m_bUpdateOnlyClockFrequency)
    {
      VTG_TRC( TRC_ID_UNCLASSIFIED, "Doing Mode Change");

      m_pFSynth->Start(m_PendingMode.mode_timing.pixel_clock_freq);

      ProgramOutputWindow(&m_PendingMode);
      if(m_pSlavedVTG)
        m_pSlavedVTG->ProgramOutputWindow(&m_PendingMode);

      if(m_pSlavedVTG)
        m_pSlavedVTG->ProgramVTGTimings(&m_PendingMode);

      ProgramVTGTimings(&m_PendingMode); // Note: this sets m_CurrentMode at the end

      if(ReadHWInterruptStatus() != 0)
        VTG_TRC( TRC_ID_ERROR, "Possibly failed to reprogram before vsync");

      m_bDoSoftResetInInterrupt   = true; // Force a reset to switch double buffered registers
    }

    if(m_bDoSoftResetInInterrupt)
    {
      m_mismatched_sync_dbg_count = 5; // Reset sync mismatch debug before reset.

      WriteVTGReg(VTGn_DRST, 1);
      m_bDoSoftResetInInterrupt = false;

      VTG_TRC( TRC_ID_UNCLASSIFIED, "SoftReset");
      /*
       * We have just issued a reset which has immediately raised a new top
       * field/frame, so there is no point in returning the original cause of
       * this interrupt. We are going to be taking a new interrupt as soon as
       * we return.
       */
      event = STM_TIMING_EVENT_NONE;
      goto unlock_and_exit;
    }

    if(IsStarted())
    {
      event |= (m_CurrentMode.mode_params.scan_type == STM_INTERLACED_SCAN)?STM_TIMING_EVENT_BOTTOM_FIELD:STM_TIMING_EVENT_FRAME;
    }
  }

  if(intStatus & (VTG_ITS_LNB | VTG_ITS_LNT))
    event |= STM_TIMING_EVENT_LINE;

unlock_and_exit:
  vibe_os_unlock_resource(m_lock);
  return event;
}
