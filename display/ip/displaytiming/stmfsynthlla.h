/***********************************************************************
 *
 * File: display/ip/displaytiming/stmfsynthlla.h
 * Copyright (c) 2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STM_CLK_FSYNTH_H
#define _STM_CLK_FSYNTH_H

#include "stmfsynth.h"
#include "stmclocklla.h" /* Needed only for stm_clock_t type */

typedef enum {
  STM_CLK_FS_CH_0,
  STM_CLK_FS_CH_1,
  STM_CLK_FS_CH_2,
  STM_CLK_FS_CH_3,
  STM_CLK_FS_CH_4,
  STM_CLK_FS_CH_5,
  STM_CLK_FS_CH_6,
  STM_CLK_FS_CH_7,
  STM_CLK_FS_CH_8,

  STM_CLK_FS_HD = STM_CLK_FS_CH_0,
  STM_CLK_FS_SD

} stm_clk_fsynth_channel_t;


class CSTmFSynthLLA: public CSTmFSynth
{
public:
  CSTmFSynthLLA( const char *fsClockName,
                 const char *pixelClockName);
  virtual ~CSTmFSynthLLA(void);

  bool Start(uint32_t ulFrequency);
  void Stop(void);
  bool SetAdjustment(int ppm);

protected:
  char   m_fsClockName[VIBE_OS_CLK_MAX_NAME_LENGTH];
  char   m_pixelClockName[VIBE_OS_CLK_MAX_NAME_LENGTH];
  struct vibe_clk m_clk_fs;
  struct vibe_clk m_clk_pix;

  bool            m_IsStarted;

  bool SolveFsynthEqn(uint32_t Fout, stm_clock_fsynth_timing_t *timing) const;
  void ProgramClock(void) {}

private:
  CSTmFSynthLLA(const CSTmFSynthLLA&);
  CSTmFSynthLLA& operator=(const CSTmFSynthLLA&);
};

#endif // _STM_CLK_FSYNTH_H
