# Classes for display timing control, including frequency synthesizers and
# video timing generators.

STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/displaytiming/,         \
                   stmvtg.cpp                                                 \
                   stmfsynth.cpp)

ifneq ($(CONFIG_C8VTG),)
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/displaytiming/,         \
                        stmc8vtg.cpp                                          \
                        stmc8vtg_v8_4.cpp)
endif

ifneq ($(CONFIG_CLOCK_LLA),)

STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/displaytiming/,         \
                        stmfsynthlla.cpp                                      \
                        stmclocklla.cpp)
endif
