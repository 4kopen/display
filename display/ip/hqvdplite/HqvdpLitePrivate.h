/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015.07.30
***************************************************************************/

#ifndef _HQVDPLITE_PLANE_PRIVATE_H_
#define _HQVDPLITE_PLANE_PRIVATE_H_

#define GetBitField(var,field)  (((var) & field##_MASK) >> field##_SHIFT)

#define SetBitField(var,field,val)                  \
do                                                  \
{                                                   \
    var &= ~(field##_MASK);                         \
    var |= ((val) << field##_SHIFT) & field##_MASK; \
} while(0)


#endif // _HQVDPLITE_PLANE_PRIVATE_H_
