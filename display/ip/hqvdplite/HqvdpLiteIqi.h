/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#ifndef _HQVDPLITE_IQI_H_
#define _HQVDPLITE_IQI_H_

#include <stm_display.h>
#include <stm_display_plane.h>
#include <vibe_debug.h>

#define IQI_TRC(id,fmt,args...)       TRC   (id, "%s - " fmt, GetPlaneName(), ##args)
#define IQI_TRCIN(id,fmt,args...)     TRCIN (id, "%s - " fmt, GetPlaneName(), ##args)
#define IQI_TRCOUT(id,fmt,args...)    TRCOUT(id, "%s - " fmt, GetPlaneName(), ##args)

class CHqvdpLiteIqi
{
public:
    CHqvdpLiteIqi(void)
    {
        m_plane_name = "";
        m_algo_name  = "";
        m_preset     = PCIQIC_BYPASS;
    }
    virtual ~CHqvdpLiteIqi(void)
    {
    }

    void SetPlaneName(const char* planeName)
    {
        m_plane_name = planeName;
    }

    inline const char* GetPlaneName() const
    {
        return m_plane_name;
    }

    void SetAlgoName(const char* algoName)
    {
        m_algo_name = algoName;
    }

    inline const char* GetAlgoName() const
    {
        return m_algo_name;
    }

    virtual inline bool SetPreset(stm_plane_ctrl_iqi_configuration_e preset)
    {
        if (preset >=  PCIQIC_COUNT)
        {
            return false;
        }
        IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Setting %s preset to %s", m_algo_name, PresetStr(preset));
        m_preset = preset;
        return true;
    }

    inline stm_plane_ctrl_iqi_configuration_e GetPreset() const
    {
        return m_preset;
    }

    static inline const char* PresetStr(stm_plane_ctrl_iqi_configuration_e preset)
    {
        return preset == PCIQIC_BYPASS    ? "BYPASS" :
               preset == PCIQIC_ST_SOFT   ? "SOFT"   :
               preset == PCIQIC_ST_MEDIUM ? "MEDIUM" :
               preset == PCIQIC_ST_STRONG ? "STRONG" :
               "UNRECOGNIZED";
    }

private:
    const char*                        m_plane_name;
    const char*                        m_algo_name;
    stm_plane_ctrl_iqi_configuration_e m_preset;
};

#endif /* _HQVDPLITE_IQI_H_ */
