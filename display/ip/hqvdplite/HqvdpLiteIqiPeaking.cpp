/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#include "HqvdpLiteIqiPeaking.h"
#include "HqvdpLitePrivate.h"
#include <display/generic/DisplayInfo.h>


#define PEAKING_COEFF_MASK          0x000003ff
#define FREQUENCY_FROM_IQIPFF(x)    (100 * (100 + 25 * (x)) / 675)
#define CLIP_PEAKING_COEFF(c)       ((c) & PEAKING_COEFF_MASK)


static const uint8_t IQI_Peaking_HorGain[IQIPHVG_LAST+1] =
{
    [IQIPHVG_N6_0DB]  =  0,
    [IQIPHVG_N5_5DB]  =  1,
    [IQIPHVG_N5_0DB]  =  2,
    [IQIPHVG_N4_5DB]  =  3,
    [IQIPHVG_N4_0DB]  =  3,
    [IQIPHVG_N3_5DB]  =  4,
    [IQIPHVG_N3_0DB]  =  4,
    [IQIPHVG_N2_5DB]  =  5,
    [IQIPHVG_N2_0DB]  =  5,
    [IQIPHVG_N1_5DB]  =  6,
    [IQIPHVG_N1_0DB]  =  6,
    [IQIPHVG_N0_5DB]  =  7,
    [IQIPHVG_0DB   ]  =  8,
    [IQIPHVG_P0_5DB]  =  9,
    [IQIPHVG_P1_0DB]  = 10,
    [IQIPHVG_P1_5DB]  = 11,
    [IQIPHVG_P2_0DB]  = 12,
    [IQIPHVG_P2_5DB]  = 13,
    [IQIPHVG_P3_0DB]  = 14,
    [IQIPHVG_P3_5DB]  = 15,
    [IQIPHVG_P4_0DB]  = 16,
    [IQIPHVG_P4_5DB]  = 17,
    [IQIPHVG_P5_0DB]  = 18,
    [IQIPHVG_P5_5DB]  = 19,
    [IQIPHVG_P6_0DB]  = 20,
    [IQIPHVG_P6_5DB]  = 21,
    [IQIPHVG_P7_0DB]  = 22,
    [IQIPHVG_P7_5DB]  = 23,
    [IQIPHVG_P8_0DB]  = 24,
    [IQIPHVG_P8_5DB]  = 25,
    [IQIPHVG_P9_0DB]  = 26,
    [IQIPHVG_P9_5DB]  = 27,
    [IQIPHVG_P10_0DB] = 28,
    [IQIPHVG_P10_5DB] = 29,
    [IQIPHVG_P11_0DB] = 30,
    [IQIPHVG_P11_5DB] = 31,
    [IQIPHVG_P12_0DB] = 31
};

static const uint8_t IQI_Peaking_VertGain[IQIPHVG_LAST+1] =
{
    [IQIPHVG_N6_0DB]  =  0,
    [IQIPHVG_N5_5DB]  =  0,
    [IQIPHVG_N5_0DB]  =  0,
    [IQIPHVG_N4_5DB]  =  0,
    [IQIPHVG_N4_0DB]  =  0,
    [IQIPHVG_N3_5DB]  =  0,
    [IQIPHVG_N3_0DB]  =  0,
    [IQIPHVG_N2_5DB]  =  0,
    [IQIPHVG_N2_0DB]  =  1,
    [IQIPHVG_N1_5DB]  =  2,
    [IQIPHVG_N1_0DB]  =  3,
    [IQIPHVG_N0_5DB]  =  4,
    [IQIPHVG_0DB   ]  =  4,
    [IQIPHVG_P0_5DB]  =  4,
    [IQIPHVG_P1_0DB]  =  5,
    [IQIPHVG_P1_5DB]  =  5,
    [IQIPHVG_P2_0DB]  =  6,
    [IQIPHVG_P2_5DB]  =  6,
    [IQIPHVG_P3_0DB]  =  7,
    [IQIPHVG_P3_5DB]  =  7,
    [IQIPHVG_P4_0DB]  =  8,
    [IQIPHVG_P4_5DB]  =  9,
    [IQIPHVG_P5_0DB]  = 10,
    [IQIPHVG_P5_5DB]  = 11,
    [IQIPHVG_P6_0DB]  = 12,
    [IQIPHVG_P6_5DB]  = 13,
    [IQIPHVG_P7_0DB]  = 14,
    [IQIPHVG_P7_5DB]  = 15,
    [IQIPHVG_P8_0DB]  = 15,
    [IQIPHVG_P8_5DB]  = 15,
    [IQIPHVG_P9_0DB]  = 15,
    [IQIPHVG_P9_5DB]  = 15,
    [IQIPHVG_P10_0DB] = 15,
    [IQIPHVG_P10_5DB] = 15,
    [IQIPHVG_P11_0DB] = 15,
    [IQIPHVG_P11_5DB] = 15,
    [IQIPHVG_P12_0DB] = 15
};

/* highpass filter coefficient for 9 taps peaking filter */
static const short IQI_Peaking_HFilterCoefficientHP[IQIPFF_COUNT][5]=
{
    /* Filter coefficients to give 0.8 gain */
    [IQIPFF_0_15_FsDiv2] = { 288,  -96, -44,  -7,   3  },  /* Correspond to 1.0MHZ   for 1H signal   (1.0/6.75  = 0.15) */
    [IQIPFF_0_18_FsDiv2] = { 260, -107, -31,   5,   3  },  /* Correspond to 1.25MHZ  for 1H signal   (1.25/6.75 = 0.18) */
    [IQIPFF_0_22_FsDiv2] = { 230, -119, -16,  16,   4  },  /* Correspond to 1.5MHZ   for 1H signal   (1.5/6.75  = 0.22) */
    [IQIPFF_0_26_FsDiv2] = { 198, -119,   5,  16,  -1  },  /* Correspond to 1.75MHZ  for 1H signal   (1.75/6.75 = 0.26) */
    /* extended size limit */
    [IQIPFF_0_30_FsDiv2] = { 288,  -96, -44,  -7,   3  },  /* Correspond to 2.0MHZ   for 1H signal   (2.0/6.75  = 0.30) */
    [IQIPFF_0_33_FsDiv2] = { 274, -102, -38,   0,   3  },  /* Correspond to 2.25MHZ  for 1H signal   (2.25/6.75 = 0.33) */
    [IQIPFF_0_37_FsDiv2] = { 260, -107, -31,   5,   3  },  /* Correspond to 2.5MHZ   for 1H signal   (2.5/6.75  = 0.37) */
    [IQIPFF_0_41_FsDiv2] = { 244, -116, -27,  14,   7  },  /* Correspond to 2.75MHZ  for 1H signal   (2.75/6.75 = 0.40) */
    [IQIPFF_0_44_FsDiv2] = { 230, -119, -16,  16,   4  },  /* Correspond to 3.0MHZ   for 1H signal   (3.0/6.75  = 0.44) */
    [IQIPFF_0_48_FsDiv2] = { 214, -120,  -6,  18,   1  },  /* Correspond to 3.25MHZ  for 1H signal   (3.25/6.75 = 0.48) */
    [IQIPFF_0_52_FsDiv2] = { 198, -119,   5,  16,  -1  },  /* Correspond to 3.5MHZ   for 1H signal   (3.5/6.75  = 0.51) */
    [IQIPFF_0_56_FsDiv2] = { 182, -118,  15,  16,  -4  },  /* Correspond to 3.75MHZ  for 1H signal   (3.75/6.75 = 0.55} */
    [IQIPFF_0_59_FsDiv2] = { 166, -114,  25,  12,  -6  },  /* Correspond to 4.0MHZ   for 1H signal   (4.0/6.75  = 0.58) */
    [IQIPFF_0_63_FsDiv2] = { 150, -109,  33,   7,  -6  },  /* Correspond to 4.25MHZ  for 1H signal   (4.25/6.75 = 0.63) */
};

/* bandpass filter coefficient for 9 taps peaking filter */
static const short IQI_Peaking_HFilterCoefficientBP[IQIPFF_COUNT][5]=
{
    /* Filter coefficients to give 0.8 gain in passband */
    [IQIPFF_0_15_FsDiv2] = { 124,  70,  -28, -68, -36 },  /* Correspond to 1.0MHZ   for 1H signal   (1.0/6.75  = 0.15) */
    [IQIPFF_0_18_FsDiv2] = { 154,  56,  -74, -58,  -1 },  /* Correspond to 1.25MHZ  for 1H signal   (1.25/6.75 = 0.18) */
    [IQIPFF_0_22_FsDiv2] = { 178,  27,  -99, -24,   7 },  /* Correspond to 1.5MHZ   for 1H signal   (1.5/6.75  = 0.22) */
    [IQIPFF_0_26_FsDiv2] = { 190,  -9, -101,   7,   8 },  /* Correspond to 1.75MHZ  for 1H signal   (1.75/6.75 = 0.26) */
    /* extended size limit */
    [IQIPFF_0_30_FsDiv2] = { 124,  70,  -28, -68, -36 },  /* Correspond to 2.0MHZ   for 1H signal   (2.0/6.75  = 0.30) */
    [IQIPFF_0_33_FsDiv2] = { 140,  65,  -52, -68, -15 },  /* Correspond to 2.25MHZ  for 1H signal   (2.25/6.75 = 0.33) */
    [IQIPFF_0_37_FsDiv2] = { 154,  56,  -74, -58,  -1 },  /* Correspond to 2.5MHZ   for 1H signal   (2.5/6.75  = 0.37) */
    [IQIPFF_0_41_FsDiv2] = { 166,  43,  -89, -42,   5 },  /* Correspond to 2.75MHZ  for 1H signal   (2.75/6.75 = 0.40) */
    [IQIPFF_0_44_FsDiv2] = { 178,  27,  -99, -24,   7 },  /* Correspond to 3.0MHZ   for 1H signal   (3.0/6.75  = 0.44) */
    [IQIPFF_0_48_FsDiv2] = { 188,   9, -103,  -7,   7 },  /* Correspond to 3.25MHZ  for 1H signal   (3.25/6.75 = 0.48) */
    [IQIPFF_0_52_FsDiv2] = { 190,  -9, -101,   7,   8 },  /* Correspond to 3.5MHZ   for 1H signal   (3.5/6.75  = 0.51) */
    [IQIPFF_0_56_FsDiv2] = { 172, -26,  -98,  26,  12 },  /* Correspond to 3.75MHZ  for 1H signal   (3.75/6.75 = 0.55) */
    [IQIPFF_0_59_FsDiv2] = { 176, -44,  -87,  38,   5 },  /* Correspond to 4.0MHZ   for 1H signal   (4.0/6.75  = 0.58) */
    [IQIPFF_0_63_FsDiv2] = { 174, -61,  -72,  47,  -1 },  /* Correspond to 4.25MHZ  for 1H signal   (4.25/6.75 = 0.63) */
};


static const stm_iqi_peaking_conf_t IQI_PK_Config[PCIQIC_COUNT] =
{
    [PCIQIC_BYPASS] =
    {
        undershoot                 : IQIPOUF_100,
        overshoot                  : IQIPOUF_100,
        coring_mode                : false,
        coring_level               : 0,
        vertical_peaking           : false,
        ver_gain                   : IQIPHVG_0DB,
        clipping_mode              : IQISTRENGTH_NONE,
        highpass_filter_cutofffreq : IQIPFF_0_48_FsDiv2,
        bandpass_filter_centerfreq : IQIPFF_0_30_FsDiv2,
        highpassgain               : IQIPHVG_0DB,
        bandpassgain               : IQIPHVG_0DB
    },

    [PCIQIC_ST_SOFT] =
    {
        undershoot                 : IQIPOUF_100,
        overshoot                  : IQIPOUF_050,
        coring_mode                : false,
        coring_level               : 20,
        vertical_peaking           : true,
        ver_gain                   : IQIPHVG_P6_5DB,
        clipping_mode              : IQISTRENGTH_WEAK,
        highpass_filter_cutofffreq : IQIPFF_0_48_FsDiv2,
        bandpass_filter_centerfreq : IQIPFF_0_30_FsDiv2,
        highpassgain               : IQIPHVG_P1_0DB,
        bandpassgain               : IQIPHVG_P1_0DB
    },

    [PCIQIC_ST_MEDIUM] =
    {
        undershoot                 : IQIPOUF_100,
        overshoot                  : IQIPOUF_050,
        coring_mode                : false,
        coring_level               : 20,
        vertical_peaking           : true,
        ver_gain                   : IQIPHVG_P6_5DB,
        clipping_mode              : IQISTRENGTH_WEAK,
        highpass_filter_cutofffreq : IQIPFF_0_48_FsDiv2,
        bandpass_filter_centerfreq : IQIPFF_0_30_FsDiv2,
        highpassgain               : IQIPHVG_P3_0DB,
        bandpassgain               : IQIPHVG_P3_0DB
    },

    [PCIQIC_ST_STRONG] =
    {
        undershoot                 : IQIPOUF_100,
        overshoot                  : IQIPOUF_050,
        coring_mode                : false,
        coring_level               : 20,
        vertical_peaking           : true,
        ver_gain                   : IQIPHVG_P6_5DB,
        clipping_mode              : IQISTRENGTH_WEAK,
        highpass_filter_cutofffreq : IQIPFF_0_48_FsDiv2,
        bandpass_filter_centerfreq : IQIPFF_0_30_FsDiv2,
        highpassgain               : IQIPHVG_P5_0DB,
        bandpassgain               : IQIPHVG_P5_0DB
    }
};


CHqvdpLiteIqiPeaking::CHqvdpLiteIqiPeaking(void)
{
    CHqvdpLiteIqi::SetAlgoName("Peaking");
    CHqvdpLiteIqi::SetPreset(PCIQIC_BYPASS);
    m_current_config = IQI_PK_Config[PCIQIC_BYPASS];
}


CHqvdpLiteIqiPeaking::~CHqvdpLiteIqiPeaking(void)
{
}


void CHqvdpLiteIqiPeaking::ComputeGainAndCoeffs(
                                stm_iqi_peaking_hor_vert_gain_e     horGainHighPass,
                                stm_iqi_peaking_hor_vert_gain_e     horGainBandPass,
                                stm_iqi_peaking_filter_frequency_e  highPassFilterCutoffFrequency,
                                stm_iqi_peaking_filter_frequency_e  bandPassFilterCenterFrequency,
                                HqvdpLiteIqiPeakingSetup_s*         pSetup) const
{
    int32_t coeff[5];

    /* If both are positive */
    if (   horGainHighPass >= IQIPHVG_0DB
        && horGainBandPass >= IQIPHVG_0DB)
    {
        uint32_t gain, gain_divider;

        /* Find max gain from HP & BP */
        if (horGainHighPass >= horGainBandPass)
        {
            /* Calculate the relative gain */
            gain         = 16 * (horGainBandPass - IQIPHVG_0DB);
            gain_divider = horGainHighPass - IQIPHVG_0DB;
            gain         = gain_divider != 0 ? gain/gain_divider : 0;

            pSetup->horizontal_gain = IQI_Peaking_HorGain[horGainHighPass];

            for(unsigned int i=0; i<5; i++)
            {
                coeff[i] =    IQI_Peaking_HFilterCoefficientHP[highPassFilterCutoffFrequency][i]
                          + ((IQI_Peaking_HFilterCoefficientBP[bandPassFilterCenterFrequency][i] * (short) gain) / 16);
            }
        }
        else
        {
            /* Calculate relative gain */
            gain         = 16 * (horGainHighPass - IQIPHVG_0DB);
            gain_divider = horGainBandPass - IQIPHVG_0DB;
            gain        /= gain_divider;

            pSetup->horizontal_gain = IQI_Peaking_HorGain[horGainBandPass];

            for(unsigned int i=0; i<5; i++)
            {
                coeff[i] =    IQI_Peaking_HFilterCoefficientBP[bandPassFilterCenterFrequency][i]
                          + ((IQI_Peaking_HFilterCoefficientHP[highPassFilterCutoffFrequency][i] * (short) gain) / 16);
            }
        }
    }
    else
    {
        /* At least one of them is negative */
        if (horGainHighPass < horGainBandPass)
        {
            pSetup->horizontal_gain = IQI_Peaking_HorGain[horGainHighPass];

            for(unsigned int i=0; i<5; i++)
            {
                coeff[i] = IQI_Peaking_HFilterCoefficientHP[highPassFilterCutoffFrequency][i];
            }
        }
        else
        {
            pSetup->horizontal_gain = IQI_Peaking_HorGain[horGainBandPass];

            for(unsigned int i=0; i<5; i++)
            {
                coeff[i] = IQI_Peaking_HFilterCoefficientBP[bandPassFilterCenterFrequency][i];
            }
        }
    }

    for(unsigned int i=0; i<5; i++)
    {
        pSetup->coeff[i] = CLIP_PEAKING_COEFF(coeff[i]);
    }

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking horizontal_gain = %d", pSetup->horizontal_gain);
    for(unsigned int i=0; i<5; i++)
    {
        IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking coeff[i]        = %d", pSetup->coeff[i]);
    }
}


stm_iqi_peaking_filter_frequency_e CHqvdpLiteIqiPeaking::GetZoomCorrectedFrequency(
                                        uint32_t                            srcFrameRectWidth,
                                        uint32_t                            dstFrameRectWidth,
                                        stm_iqi_peaking_filter_frequency_e  peakingFrequency) const
{
    stm_iqi_peaking_filter_frequency_e correctedPeakingFrequency = IQIPFF_LAST;

    /* We need to do divide the peaking freq by the zoom ratio: F2 = F1/z   */
    /* As we use an enum indexed by n specifying the frequency, we have     */
    /*   F1 = (n * 0.25 + 1) / 6.75                                         */
    /* When developing F2 = F1/z this evaluates to n2 = (n1 + 4 - 4*z) / z  */
    /* To work in fixed point: z = Z/100 => n2 = (100 * n1 + 400 - 4*Z) / Z */
    const int32_t zoomInRatioBy100 = (dstFrameRectWidth * 100) / srcFrameRectWidth;

    /* Use 100*Index for rounding purposes                                            */
    /* IndexBy100 = (100 * (100 * (S32) PeakingFrequency + 400 - 4*ZoomInRatioBy100)) */
    /*              / ZoomInRatioBy100;                                               */
    int32_t indexBy100 = ((100 * (100 * (int32_t) peakingFrequency  + 400 - 4 * zoomInRatioBy100)) / zoomInRatioBy100);

    if (indexBy100 < 0)
    {
        IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking zoom correction: clipping peaking frequency from 0.%d to min 0.%d",
                                       FREQUENCY_FROM_IQIPFF(peakingFrequency),
                                       FREQUENCY_FROM_IQIPFF(0));
        indexBy100 = 0;
    }

    /* Round correctly */
    correctedPeakingFrequency = (stm_iqi_peaking_filter_frequency_e)((indexBy100 / 100) + (((indexBy100 % 100) >= 50) ? 1 : 0));

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking zoom correction: Freq 0.%d has been changed to 0.%d for a zoom-in ratio of %d.%d",
                                   FREQUENCY_FROM_IQIPFF(peakingFrequency),
                                   FREQUENCY_FROM_IQIPFF(correctedPeakingFrequency),
                                   zoomInRatioBy100 / 100, zoomInRatioBy100 % 100);

    if (correctedPeakingFrequency > IQIPFF_LAST)
    {
        IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking zoom correction: clipping peaking frequency from 0.%d to max 0.%d",
                                       FREQUENCY_FROM_IQIPFF(peakingFrequency),
                                       FREQUENCY_FROM_IQIPFF(IQIPFF_LAST));
        correctedPeakingFrequency = IQIPFF_LAST;
    }

    return correctedPeakingFrequency;
}


void CHqvdpLiteIqiPeaking::CalculateSetup(
                            const CDisplayInfo*             pDisplayInfo,
                            bool                            isDisplayInterlaced,
                            HqvdpLiteIqiPeakingSetup_s*     pSetup) const
{
    const uint32_t                      srcFrameRectWidth               = pDisplayInfo->m_primarySrcFrameRect.width;
    const uint32_t                      dstFrameRectWidth               = pDisplayInfo->m_dstFrameRect.width;
    stm_iqi_peaking_filter_frequency_e  highPassFilterCutoffFrequency   = IQIPFF_LAST;
    stm_iqi_peaking_filter_frequency_e  bandPassFilterCenterFrequency   = IQIPFF_LAST;

    /* We need to disable these if the output is interlaced to avoid artifacts */
    pSetup->env_detect_3_lines       = isDisplayInterlaced ? false : true;
    pSetup->vertical_peaking_allowed = isDisplayInterlaced ? false : true;

    /* Peaking cutoff */
    highPassFilterCutoffFrequency = GetZoomCorrectedFrequency(srcFrameRectWidth,
                                                              dstFrameRectWidth,
                                                              m_current_config.highpass_filter_cutofffreq);

    bandPassFilterCenterFrequency = GetZoomCorrectedFrequency(srcFrameRectWidth,
                                                              dstFrameRectWidth,
                                                              m_current_config.bandpass_filter_centerfreq);

    /* If extended size is required to reach required frequencies... */
    if (   highPassFilterCutoffFrequency <= IQIPFF_EXTENDED_SIZE_LIMIT
        || bandPassFilterCenterFrequency <= IQIPFF_EXTENDED_SIZE_LIMIT)
    {
        /* ...then we have to multiply the frequency by 2 because extended size on means all frequencies are divided by 2 */
        /* Compensate HighPass filter for extended mode if extended was not required for it */
        if (highPassFilterCutoffFrequency > IQIPFF_EXTENDED_SIZE_LIMIT)
        {
            /* HighPass is at least 0.30 if not extended so with extended on it is at least 0.60 */
            highPassFilterCutoffFrequency = IQIPFF_0_63_FsDiv2;
        }

        /* Note that the BandPass frequency is always lower than the HighPass      */
        /* frequency so HighPass extended and BandPass not extended makes no sense */

        /* Extended size is not allowed for zoom-in < 2.0 */
        if (((100 * dstFrameRectWidth) / srcFrameRectWidth) < 200)
        {
            IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Warning! Peaking: extended size needed but zoom-in is < 2.0!");

            if (bandPassFilterCenterFrequency <= IQIPFF_EXTENDED_SIZE_LIMIT)
            {
                /* Clip BandPass to minimum value without extended size */
                IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Warning! Peaking: Clipping BandPass filter freq 0.%d to minimum freq non extended 0.%d\n",
                                                FREQUENCY_FROM_IQIPFF(bandPassFilterCenterFrequency),
                                                FREQUENCY_FROM_IQIPFF(IQIPFF_EXTENDED_SIZE_LIMIT + 1));
                bandPassFilterCenterFrequency = static_cast <stm_iqi_peaking_filter_frequency_e> ((static_cast <unsigned int> (IQIPFF_EXTENDED_SIZE_LIMIT)) + 1);
            }

            if (highPassFilterCutoffFrequency <= IQIPFF_EXTENDED_SIZE_LIMIT)
            {
                /* Clip HighPass to minimum value without extended size */
                IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Warning! Peaking: Clipping HighPass filter freq 0.%d to minimum freq non extended 0.%d\n",
                                                FREQUENCY_FROM_IQIPFF(highPassFilterCutoffFrequency),
                                                FREQUENCY_FROM_IQIPFF(IQIPFF_EXTENDED_SIZE_LIMIT + 1));
                highPassFilterCutoffFrequency = static_cast <stm_iqi_peaking_filter_frequency_e> ((static_cast <unsigned int> (IQIPFF_EXTENDED_SIZE_LIMIT)) + 1);
            }

            pSetup->extended_size = false;
        }
        else
        {
            pSetup->extended_size = true;
        }
    }
    else
    {
        pSetup->extended_size = false;
    }

    if (bandPassFilterCenterFrequency > highPassFilterCutoffFrequency)
    {
        IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Warning! Peaking: BandPass freq 0.%d is above HighPass freq. 0.%d which is forbidden! Forcing Bandpass = Highpass\n",
                                       FREQUENCY_FROM_IQIPFF(bandPassFilterCenterFrequency),
                                       FREQUENCY_FROM_IQIPFF(highPassFilterCutoffFrequency));
        bandPassFilterCenterFrequency = highPassFilterCutoffFrequency;
    }

    pSetup->highpass_filter_cutoff_freq = highPassFilterCutoffFrequency;
    pSetup->bandpass_filter_center_freq = bandPassFilterCenterFrequency;

    if (pSetup->vertical_peaking_allowed)
    {
        pSetup->vertical_gain = IQI_Peaking_VertGain[m_current_config.ver_gain];
    }

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking env_detect_3_lines          = %d", pSetup->env_detect_3_lines);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking vertical_peaking_allowed    = %d", pSetup->vertical_peaking_allowed);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking extended_size               = %d", pSetup->extended_size);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking highpass_filter_cutoff_freq = %d", pSetup->highpass_filter_cutoff_freq);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking bandpass_filter_center_freq = %d", pSetup->bandpass_filter_center_freq);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking vertical_gain               = %d", pSetup->vertical_gain);

    ComputeGainAndCoeffs(m_current_config.highpassgain,
                         m_current_config.bandpassgain,
                         highPassFilterCutoffFrequency,
                         bandPassFilterCenterFrequency,
                         pSetup);
}


void CHqvdpLiteIqiPeaking::CalculateParams(
                            const CDisplayInfo*         pDisplayInfo,
                            bool                        isDisplayInterlaced,
                            HQVDPLITE_IQI_Params_t*     pParams) const
{
    HqvdpLiteIqiPeakingSetup_s setup;

    CalculateSetup(pDisplayInfo, isDisplayInterlaced, &setup);

    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_EXTENDED_SIZE,       setup.extended_size           );
    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_ENV_DETECT,          setup.env_detect_3_lines      );
    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_CORING_MODE,         m_current_config.coring_mode  );
    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_V_PK_EN,             setup.vertical_peaking_allowed);
    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_RANGE_GAIN_LUT_INIT, 1                             );
    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_OVERSHOOT,           m_current_config.overshoot    );
    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_UNDERSHOOT,          m_current_config.undershoot   );
    SetBitField(pParams->PkConfig, IQI_PK_CONFIG_RANGE_MAX,           4                             );

    SetBitField(pParams->Coeff0Coeff1, IQI_COEFF0_COEFF1_PK_COEFF0, setup.coeff[0]);
    SetBitField(pParams->Coeff0Coeff1, IQI_COEFF0_COEFF1_PK_COEFF1, setup.coeff[1]);
    SetBitField(pParams->Coeff2Coeff3, IQI_COEFF2_COEFF3_PK_COEFF2, setup.coeff[2]);
    SetBitField(pParams->Coeff2Coeff3, IQI_COEFF2_COEFF3_PK_COEFF3, setup.coeff[3]);
    SetBitField(pParams->Coeff4,       IQI_COEFF4_PK_COEFF4,        setup.coeff[4]);
    SetBitField(pParams->PkLut,        IQI_PK_LUT_SELECT,           m_current_config.clipping_mode);
    SetBitField(pParams->PkGain,       IQI_PK_GAIN_PK_HOR_GAIN,     setup.horizontal_gain);
    SetBitField(pParams->PkGain,       IQI_PK_GAIN_PK_VERT_GAIN,    setup.vertical_gain);

    SetBitField(pParams->PkCoringLevel, IQI_PK_CORING_LEVEL_PK_CORING_LEVEL, m_current_config.coring_level);

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking PkConfig      = 0x%x", pParams->PkConfig);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking Coeff0Coeff1  = 0x%x", pParams->Coeff0Coeff1);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking Coeff2Coeff3  = 0x%x", pParams->Coeff2Coeff3);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking Coeff4        = 0x%x", pParams->Coeff4);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking PkLut         = 0x%x", pParams->PkLut);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking PkGain        = 0x%x", pParams->PkGain);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking PkCoringLevel = 0x%x", pParams->PkCoringLevel);
}


bool CHqvdpLiteIqiPeaking::SetConf(const stm_iqi_peaking_conf_t* pConf)
{
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking bandpassgain               = %d", pConf->bandpassgain );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking highpassgain               = %d", pConf->highpassgain );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking ver_gain                   = %d", pConf->ver_gain );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking coring_level               = %d", pConf->coring_level );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking overshoot                  = %d", pConf->overshoot );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking undershoot                 = %d", pConf->undershoot );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking clipping_mode              = %d", pConf->clipping_mode );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking bandpass_filter_centerfreq = %d", pConf->bandpass_filter_centerfreq );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking highpass_filter_cutofffreq = %d", pConf->highpass_filter_cutofffreq );

    if (   pConf->bandpassgain               >  IQIPHVG_LAST
        || pConf->highpassgain               >  IQIPHVG_LAST
        || pConf->ver_gain                   <  IQIPHVG_N6_0DB
        || pConf->ver_gain                   >  IQIPHVG_P12_0DB
        || pConf->coring_level               >  63
        || pConf->overshoot                  >  IQIPOUF_LAST
        || pConf->undershoot                 >  IQIPOUF_LAST
        || pConf->clipping_mode              >  IQISTRENGTH_LAST
        || pConf->bandpass_filter_centerfreq >= IQIPFF_COUNT
        || pConf->highpass_filter_cutofffreq >= IQIPFF_COUNT
        || pConf->bandpass_filter_centerfreq >  pConf->highpass_filter_cutofffreq)
    {
        IQI_TRC( TRC_ID_ERROR, "Invalid Peaking parameters" );
        return false;
    }

    m_current_config = *pConf;
    return true;
}


void CHqvdpLiteIqiPeaking::GetConf(stm_iqi_peaking_conf_t* pConf) const
{
    *pConf = m_current_config;

    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking undershoot                 = %d", pConf->undershoot);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking overshoot                  = %d", pConf->overshoot);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking coring_mode                = %d", pConf->coring_mode);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking coring_level               = %d", pConf->coring_level);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking vertical_peaking           = %d", pConf->vertical_peaking);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking clipping_mode              = %d", pConf->clipping_mode);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking bandpass_filter_centerfreq = %d", pConf->bandpass_filter_centerfreq);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking highpass_filter_cutofffreq = %d", pConf->highpass_filter_cutofffreq);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking bandpassgain               = %d", pConf->bandpassgain);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking highpassgain               = %d", pConf->highpassgain);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI,"Peaking ver_gain                   = %d", pConf->ver_gain);
}


bool CHqvdpLiteIqiPeaking::SetPreset(stm_plane_ctrl_iqi_configuration_e preset)
{
    if (!CHqvdpLiteIqi::SetPreset(preset))
    {
      IQI_TRC( TRC_ID_ERROR, "Failed to set preset %s (%d)", PresetStr(preset), preset );
      return false;
    }

    m_current_config =  IQI_PK_Config[preset];

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Preset = %s (%d)", PresetStr(preset), preset );

    return true;
}
