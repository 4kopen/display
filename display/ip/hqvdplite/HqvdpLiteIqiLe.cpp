/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#include "HqvdpLiteIqiLe.h"
#include "HqvdpLitePrivate.h"
#include <display/generic/DisplayNode.h>

#define IQI_LE_BLACKD1                            64  /* Default value for AutoCurve LE algo */
#define IQI_LE_WHITED1                           940  /* Default value for AutoCurve LE algo */
#define IQI_LE_LUT_10BITS_SIZE                  1024  /* Compute LUT on 10 bits       */
#define IQI_LE_LUT_7BITS_SIZE                    128  /* Compute LUT on 7 bits (for HW) */
#define IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR        8  /* Scale from IQI_LE_LUT_10BITS_SIZE to IQI_LE_LUT_7BITS_SIZE */


static const int16_t IQI_LE_CustomCurveGamma_Medium[LUMA_LUT_SIZE] =
{
    0,   0,   0,   0,   0,   0,   0,   0,
    0,  -1,  -1,  -2,  -2,  -2,  -3,  -3,
   -3,  -4,  -4,  -4,  -4,  -4,  -5,  -5,
   -5,  -5,  -5,  -5,  -5,  -5,  -6,  -6,
   -6,  -6,  -6,  -6,  -6,  -6,  -6,  -6,
   -6,  -6,  -6,  -6,  -6,  -6,  -6,  -6,
   -6,  -6,  -6,  -6,  -6,  -6,  -6,  -6,
   -6,  -6,  -6,  -6,  -6,  -6,  -6,  -6,
   -6,  -6,  -6,  -6,  -6,  -6,  -6,  -6,
   -5,  -5,  -5,  -5,  -5,  -5,  -5,  -5,
   -5,  -5,  -5,  -4,  -4,  -4,  -4,  -4,
   -4,  -4,  -4,  -4,  -3,  -3,  -3,  -3,
   -3,  -3,  -3,  -3,  -2,  -2,  -2,  -2,
   -2,  -2,  -2,  -1,  -1,  -1,  -1,  -1,
   -1,  -1,   0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0
};

static const int16_t IQI_LE_CustomCurveGamma_Strong[LUMA_LUT_SIZE] =
{
    0,   0,   0,   0,   0,   0,   0,   0,
    0,  -1,  -2,  -3,  -3,  -4,  -4,  -4,
   -5,  -5,  -6,  -6,  -6,  -6,  -7,  -7,
   -7,  -7,  -8,  -8,  -8,  -8,  -8,  -8,
   -9,  -9,  -9,  -9,  -9,  -9,  -9,  -9,
   -9,  -9,  -9,  -9,  -9,  -9, -10, -10,
  -10, -10, -10, -10,  -9,  -9,  -9,  -9,
   -9,  -9,  -9,  -9,  -9,  -9,  -9,  -9,
   -9,  -9,  -9,  -9,  -9,  -8,  -8,  -8,
   -8,  -8,  -8,  -8,  -8,  -8,  -7,  -7,
   -7,  -7,  -7,  -7,  -7,  -6,  -6,  -6,
   -6,  -6,  -6,  -5,  -5,  -5,  -5,  -5,
   -4,  -4,  -4,  -4,  -4,  -3,  -3,  -3,
   -3,  -3,  -2,  -2,  -2,  -2,  -2,  -1,
   -1,  -1,  -1,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0
};

static const int16_t IQI_LE_CustomCurveGamma_Unused[LUMA_LUT_SIZE] =
{
    0,   0,   0,   0,   0,   0,   0,   0,
    0,  -1,  -2,  -3,  -4,  -5,  -5,  -6,
   -6,  -7,  -7,  -8,  -8,  -9,  -9,  -9,
  -10, -10, -10, -10, -11, -11, -11, -11,
  -11, -12, -12, -12, -12, -12, -12, -12,
  -12, -12, -12, -13, -13, -13, -13, -13,
  -13, -13, -13, -13, -13, -13, -13, -12,
  -12, -12, -12, -12, -12, -12, -12, -12,
  -12, -12, -12, -11, -11, -11, -11, -11,
  -11, -11, -11, -10, -10, -10, -10, -10,
   -9,  -9,  -9,  -9,  -9,  -8,  -8,  -8,
   -8,  -8,  -7,  -7,  -7,  -7,  -6,  -6,
   -6,  -6,  -5,  -5,  -5,  -5,  -4,  -4,
   -4,  -4,  -3,  -3,  -3,  -2,  -2,  -2,
   -1,  -1,  -1,  -1,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0,   0,   0
};


static const stm_iqi_le_conf_t IQI_LE_Config[PCIQIC_COUNT] = {
    [PCIQIC_BYPASS] =
    {
        csc_gain           : 0,
        fixed_curve        : false,
        fixed_curve_params : {
                                 BlackStretchInflexionPoint : 0,
                                 BlackStretchLimitPoint     : 0,
                                 WhiteStretchInflexionPoint : 0,
                                 WhiteStretchLimitPoint     : 0,
                                 BlackStretchGain           : 0,
                                 WhiteStretchGain           : 0
                             },
        custom_curve       : 0
    },

    [PCIQIC_ST_SOFT] =
    {
        csc_gain           : 8,
        fixed_curve        : true,
        fixed_curve_params : {
                                 BlackStretchInflexionPoint : 344,
                                 BlackStretchLimitPoint     : 128,
                                 WhiteStretchInflexionPoint : 360,
                                 WhiteStretchLimitPoint     : 704,
                                 BlackStretchGain           :  10,
                                 WhiteStretchGain           :  10
                             },
        custom_curve       : 0
    },

    [PCIQIC_ST_MEDIUM] =
    {
        csc_gain           : 8,
        fixed_curve        : true,
        fixed_curve_params : {
                                 BlackStretchInflexionPoint : 344,
                                 BlackStretchLimitPoint     : 128,
                                 WhiteStretchInflexionPoint : 360,
                                 WhiteStretchLimitPoint     : 704,
                                 BlackStretchGain           :  15,
                                 WhiteStretchGain           :  15
                             },
        custom_curve       : IQI_LE_CustomCurveGamma_Medium
    },

    [PCIQIC_ST_STRONG] =
    {
        csc_gain           : 8,
        fixed_curve        : true,
        fixed_curve_params : {
                                 BlackStretchInflexionPoint : 344,
                                 BlackStretchLimitPoint     : 128,
                                 WhiteStretchInflexionPoint : 360,
                                 WhiteStretchLimitPoint     : 704,
                                 BlackStretchGain           :  20,
                                 WhiteStretchGain           :  20
                             },
        custom_curve       : IQI_LE_CustomCurveGamma_Strong
    }
};


CHqvdpLiteIqiLe::CHqvdpLiteIqiLe(void)
{
    CHqvdpLiteIqi::SetAlgoName("LE");
    CHqvdpLiteIqi::SetPreset(PCIQIC_BYPASS);
    m_current_config = IQI_LE_Config[PCIQIC_BYPASS];
    GenerateLumaLUT(&m_current_config, m_LumaLUT);
}


CHqvdpLiteIqiLe::~CHqvdpLiteIqiLe(void)
{
}


bool CHqvdpLiteIqiLe::GenerateFixedCurveTable(
                            const stm_iqi_le_fixed_curve_params_t* const pParams,
                            int16_t*                               const pCurve) const
{
    /* B&W Limits */
    uint32_t BSLim;
    uint32_t WSLim;
    uint32_t BlackD1;
    uint32_t WhiteD1;
    /* Black Clip setup */
    uint32_t xClipBS;
    uint32_t yClipBS;
    /* Black Limit */
    int32_t  SlopeBSLim;
    int32_t  OriginBSLim;
    /* White X&Y Clip */
    uint32_t xClipWS;
    int32_t  yClipWS;
    /* White Limit */
    int32_t  SlopeWSLim;

    /* From Black to Black inflection */
    /* new algo from application team (CR5)
     values like 128, 880, etc. are not replaced by defines (to keep the
     algo clear) */

    /* B & W limits */
    BSLim   = pParams->BlackStretchLimitPoint;
    WSLim   = pParams->WhiteStretchLimitPoint;
    BlackD1 = IQI_LE_BLACKD1;
    WhiteD1 = IQI_LE_WHITED1;

    { /* black clip setup */
        uint32_t xClipBS1 = (uint32_t) (BSLim
                                       + (int32_t) (((uint32_t) (pParams->BlackStretchGain * 8)
                                                   * (uint32_t) pParams->BlackStretchInflexionPoint)
                                                   / 1024));
        uint32_t xClipBS2 = (uint32_t) ((1024 + (uint32_t) (pParams->BlackStretchGain * 8)));

        xClipBS = (uint32_t) (xClipBS1 * 1024) / xClipBS2;
        yClipBS = xClipBS - BSLim;
    }

    { /* Black Limit */
        uint32_t SlopeBSLim1 = (uint32_t) (yClipBS * 1024);
        int32_t  SlopeBSLim2 = (int32_t) (BlackD1 - xClipBS);
        SlopeBSLim = ((SlopeBSLim2 == 0) ? 1 : (int32_t) (SlopeBSLim1) / SlopeBSLim2);
        OriginBSLim = (-BlackD1 * SlopeBSLim / 1024);
    }

    { /* White X&Y Clip */
        uint32_t xClipWS1 = (uint32_t) (WSLim
                                        + (int32_t) (((uint32_t) (pParams->WhiteStretchGain * 8)
                                                    * (uint32_t) pParams->WhiteStretchInflexionPoint)
                                                    / 1024));
        uint32_t xClipWS2 = (uint32_t) ((1024 + (uint32_t) (pParams->WhiteStretchGain * 8)));

        xClipWS = (uint32_t) (xClipWS1 * 1024) / xClipWS2;
        yClipWS = (int32_t) (WSLim - xClipWS);
    }

    { /* White Limit */
        uint32_t SlopeWSLim1 = (uint32_t) (yClipWS * 1024);
        int32_t  SlopeWSLim2 = (int32_t) (xClipWS - WhiteD1);

        SlopeWSLim = ((SlopeWSLim2 == 0) ? 1 : (int32_t) (SlopeWSLim1) / SlopeWSLim2);
    }

    /* First generate Fixed curve */

    /* --- Check parameters...  */
    if (   BlackD1 <= xClipBS
        && xClipBS <= pParams->BlackStretchInflexionPoint
        && pParams->BlackStretchInflexionPoint <= pParams->WhiteStretchInflexionPoint
        && pParams->WhiteStretchInflexionPoint <= xClipWS
        && xClipWS <= WhiteD1
        && WhiteD1 <= IQI_LE_LUT_10BITS_SIZE)
    {
        /* A loop on all values*/
        for (uint32_t Loop = 0;
             Loop < IQI_LE_LUT_10BITS_SIZE;
             Loop += IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR)
        {
            int32_t Point;

            if (Loop <= BlackD1)
            {
                /* From 0 to BlackD1 => Linear LUT*/
                pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = 0;
            }
            else if (Loop < xClipBS)
            {
                /* From BlackD1 to xClipBS */
                Point = (int32_t) ((int32_t) Loop * (int32_t) SlopeBSLim / 1024 + (int32_t) OriginBSLim);
                pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = (int16_t)Point;
            }
            else if (Loop < pParams->BlackStretchInflexionPoint)
            {
                /* From xClipBS to Black inflection */
                Point = (int32_t) ((((int32_t) Loop
                                      - (int32_t) pParams->BlackStretchInflexionPoint)
                                      * (int32_t) (pParams->BlackStretchGain * 8)
                                      + 512) / 1024);
                pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = (int16_t)Point;
            }
            else if (Loop < pParams->WhiteStretchInflexionPoint)
            {
                /* From Black inflection to White inflection */
                pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = 0;
            }
            else if (Loop < xClipWS)
            {
                /* From White inflection to xClipWS */
                Point = (int32_t) ((((int32_t) Loop
                                      - (int32_t) pParams->WhiteStretchInflexionPoint)
                                      * (int32_t) (pParams->WhiteStretchGain * 8)
                                      + 512) / 1024);
                pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = (int16_t)Point;
            }
            else if (Loop < WhiteD1)
            {
                /* From xClipWS to WhiteD1 */
                Point = (int32_t) (yClipWS + SlopeWSLim * ((int32_t) Loop - (int32_t) xClipWS) / 1024);
                pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = (int16_t)Point;
            }
            else
            {
                /* From WhiteD1 to full white */
                pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = 0;
            }
        }
    }
    else
    {
        /* Clean LUT table (which must be done to avoid visual effects when values are wrong) */
        for (uint32_t Loop = 0;
             Loop < IQI_LE_LUT_10BITS_SIZE;
             Loop += IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR)
        {
            pCurve[Loop / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR] = 0;
        }

        return false;
    }

  return true;
}

bool CHqvdpLiteIqiLe::GenerateLumaLUT(
                            const stm_iqi_le_conf_t * const pConf,
                            uint16_t                *       pLUT) const
{
    bool     bIsOk = true;
    int16_t  lutGainHL[LUMA_LUT_SIZE];
    uint8_t  index;
    uint16_t identityValue;

    /* First generate fixed curve table if needed */
    if (pConf->fixed_curve)
    {
        bIsOk = GenerateFixedCurveTable (&pConf->fixed_curve_params, lutGainHL);
    }

    /* Now, sum fixed and gamma curves to the identity curve */
    if (bIsOk)
    {
        for (unsigned int i = 0;
             i < IQI_LE_LUT_10BITS_SIZE;
             i += IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR)
        {
            index = i / IQI_LE_LUT_10BITS_7BITS_SCALEFACTOR;
            /* Ranges are on 10 bits (1024) */
            identityValue = (uint16_t) ((index * (IQI_LE_LUT_10BITS_SIZE - 1)) / (IQI_LE_LUT_7BITS_SIZE - 1));

            /* identity curve */
            pLUT[index] = identityValue;

            if (pConf->fixed_curve)
            {
                /* Fixed curve is centered aroud zero */
                pLUT[index] += lutGainHL[index];
            }

            if (pConf->custom_curve)
            {
                /* custom curve is also centered around zero */
                pLUT[index] += pConf->custom_curve[index];
            }

            /* Manage clipping */
            if (pLUT[index] >= IQI_LE_LUT_10BITS_SIZE)
            {
                pLUT[index] = IQI_LE_LUT_10BITS_SIZE - 1;
            }
        }
    }

    return bIsOk;
}


void CHqvdpLiteIqiLe::CalculateParams(
                            const CDisplayNode*         pDisplayNode,
                            HQVDPLITE_IQI_Params_t*     pParams,
                            bool*                       pEnableCsc) const
{
    bool bIsBt601 = (pDisplayNode->m_bufferDesc.src.flags & (STM_BUFFER_SRC_COLORSPACE_709|STM_BUFFER_SRC_COLORSPACE_BT2020)) ? false : true;

    SetBitField(pParams->LeConfig, IQI_LE_CONFIG_D1_601_NOT_709, bIsBt601                 );
    SetBitField(pParams->LeConfig, IQI_LE_CONFIG_LCE_LUT_INIT,   true                     );
    SetBitField(pParams->LeConfig, IQI_LE_CONFIG_WEIGHT_GAIN,    m_current_config.csc_gain);

    IQI_TRC(TRC_ID_HQVDPLITE_IQI,"LE LeConfig = 0x%x", pParams->LeConfig);

    for(unsigned int j=0; j<N_ELEMENTS(pParams->LeLut); j++)
    {
        SetBitField(pParams->LeLut[j], IQI_LE_LUT_LUT_COEF_EVEN, m_LumaLUT[2*j  ]);
        SetBitField(pParams->LeLut[j], IQI_LE_LUT_LUT_COEF_ODD,  m_LumaLUT[2*j+1]);

        IQI_TRC(TRC_ID_HQVDPLITE_IQI,"LeLut[%.2d] = 0x%x", j, pParams->LeLut[j]);
    }

    *pEnableCsc = m_current_config.csc_gain ? true : false;
}


bool CHqvdpLiteIqiLe::SetConf(const stm_iqi_le_conf_t* pConf)
{
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE csc_gain                                      = %d", pConf->csc_gain );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE fixed_curve                                   = %d", pConf->fixed_curve );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE fixed_curve_params.BlackStretchInflexionPoint = %d", pConf->fixed_curve_params.BlackStretchInflexionPoint );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE fixed_curve_params.BlackStretchGain           = %d", pConf->fixed_curve_params.BlackStretchGain );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE fixed_curve_params.BlackStretchLimitPoint     = %d", pConf->fixed_curve_params.BlackStretchLimitPoint );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE fixed_curve_params.WhiteStretchInflexionPoint = %d", pConf->fixed_curve_params.WhiteStretchInflexionPoint );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE fixed_curve_params.WhiteStretchGain           = %d", pConf->fixed_curve_params.WhiteStretchGain );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "LE fixed_curve_params.WhiteStretchLimitPoint     = %d", pConf->fixed_curve_params.WhiteStretchLimitPoint);

    if (   !pConf
        || pConf->csc_gain > 31
        || (pConf->fixed_curve
            && (   pConf->fixed_curve_params.BlackStretchInflexionPoint > (IQI_LE_LUT_10BITS_SIZE-1)
                || pConf->fixed_curve_params.BlackStretchGain           > 100
                || pConf->fixed_curve_params.BlackStretchLimitPoint     > (IQI_LE_LUT_10BITS_SIZE-1)
                || pConf->fixed_curve_params.WhiteStretchInflexionPoint > (IQI_LE_LUT_10BITS_SIZE-1)
                || pConf->fixed_curve_params.WhiteStretchGain           > 100
                || pConf->fixed_curve_params.WhiteStretchLimitPoint     > (IQI_LE_LUT_10BITS_SIZE-1))))
    {
        IQI_TRC( TRC_ID_ERROR, "Invalid LE conf");
        return false;
    }

    if (!GenerateLumaLUT(pConf, m_LumaLUT))
    {
        IQI_TRC( TRC_ID_ERROR, "Failed to generate LUT");
        return false;
    }

    m_current_config = *pConf;
    return true;
}


void CHqvdpLiteIqiLe::GetConf(stm_iqi_le_conf_t* pConf) const
{
    *pConf = m_current_config;
}


bool CHqvdpLiteIqiLe::SetPreset(stm_plane_ctrl_iqi_configuration_e preset)
{
    if (!CHqvdpLiteIqi::SetPreset(preset))
    {
      IQI_TRC( TRC_ID_ERROR, "Failed to set preset %s (%d)", PresetStr(preset), preset );
      return false;
    }

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Preset = %s (%d)", PresetStr(preset), preset );

    if(!GenerateLumaLUT(&IQI_LE_Config[preset], m_LumaLUT))
    {
      IQI_TRC( TRC_ID_ERROR, "Failed to generate luma LUT");
      return false;
    }

    m_current_config = IQI_LE_Config[preset];

    return true;
}
