/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#ifndef _HQVDPLITE_IQI_PEAKING_H_
#define _HQVDPLITE_IQI_PEAKING_H_

#include "HqvdpLiteIqi.h"
#include "lld_V2/c8fvp3_stddefs.h"
#include "lld_V2/c8fvp3_hqvdplite_api_IQI.h"

class CDisplayInfo;

class CHqvdpLiteIqiPeaking: public CHqvdpLiteIqi
{
public:
    CHqvdpLiteIqiPeaking(void);
    virtual ~CHqvdpLiteIqiPeaking(void);

    void CalculateParams(
                  const CDisplayInfo*       pDisplayInfo,
                  bool                      isDisplayInterlaced,
                  HQVDPLITE_IQI_Params_t*   pParams) const;

    bool SetConf(const stm_iqi_peaking_conf_t* pConf) __attribute__((nonnull,warn_unused_result));

    void GetConf(stm_iqi_peaking_conf_t* pConf) const;

    bool SetPreset(stm_plane_ctrl_iqi_configuration_e preset);

private:
    struct HqvdpLiteIqiPeakingSetup_s
    {
        bool                                env_detect_3_lines;
        bool                                vertical_peaking_allowed;
        bool                                extended_size;
        stm_iqi_peaking_filter_frequency_e  highpass_filter_cutoff_freq;
        stm_iqi_peaking_filter_frequency_e  bandpass_filter_center_freq;
        uint32_t                            vertical_gain;
        uint32_t                            horizontal_gain;
        uint32_t                            coeff[5];
    };

    stm_iqi_peaking_conf_t m_current_config;

    void CalculateSetup(
                    const CDisplayInfo*                 pDisplayInfo,
                    bool                                isDisplayInterlaced,
                    HqvdpLiteIqiPeakingSetup_s*         pSetup) const __attribute__((nonnull(1)));

    void ComputeGainAndCoeffs(
                    stm_iqi_peaking_hor_vert_gain_e     horGainHighPass,
                    stm_iqi_peaking_hor_vert_gain_e     horGainBandPass,
                    stm_iqi_peaking_filter_frequency_e  highPassFilterCutoffFrequency,
                    stm_iqi_peaking_filter_frequency_e  bandPassFilterCenterFrequency,
                    HqvdpLiteIqiPeakingSetup_s*         pSetup) const;

    stm_iqi_peaking_filter_frequency_e GetZoomCorrectedFrequency(
                    uint32_t                            srcFrameRectWidth,
                    uint32_t                            dstFrameRectWidth,
                    stm_iqi_peaking_filter_frequency_e  peakingFrequency) const __attribute__((nonnull(1)));
};

#endif /* _HQVDPLITE_IQI_PEAKING_H_ */
