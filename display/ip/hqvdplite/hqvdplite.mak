# Classes required for SoCs containing a HQVDP Lite display
ifneq ($(CONFIG_HQVDPLITE),)

SRC_FILES_NAMES :=
SRC_FILES_NAMES += HqvdpLitePlaneV2.cpp
SRC_FILES_NAMES += HqvdpLiteIqiCti.cpp
SRC_FILES_NAMES += HqvdpLiteIqiLe.cpp
SRC_FILES_NAMES += HqvdpLiteIqiPeaking.cpp


STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/hqvdplite/,$(SRC_FILES_NAMES))


include $(STG_TOPDIR)/display/ip/hqvdplite/lld_V2/lld.mak

endif

