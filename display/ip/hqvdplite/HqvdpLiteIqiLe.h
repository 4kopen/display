/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#ifndef _HQVDPLITE_IQI_LE_H_
#define _HQVDPLITE_IQI_LE_H_

#include "HqvdpLiteIqi.h"
#include "lld_V2/c8fvp3_stddefs.h"
#include "lld_V2/c8fvp3_hqvdplite_api_IQI.h"

class CDisplayNode;

class CHqvdpLiteIqiLe: public CHqvdpLiteIqi
{
public:
    CHqvdpLiteIqiLe (void);
    virtual ~CHqvdpLiteIqiLe (void);

    void CalculateParams(
                  const CDisplayNode*       pDisplayNode,
                  HQVDPLITE_IQI_Params_t*   pParams,
                  bool*                     pEnableCsc) const;

    bool SetConf(const stm_iqi_le_conf_t* pConf) __attribute__((nonnull,warn_unused_result));

    void GetConf(stm_iqi_le_conf_t* pConf) const;

    bool SetPreset(stm_plane_ctrl_iqi_configuration_e preset);

private:

    #define LUMA_LUT_SIZE 128

    stm_iqi_le_conf_t m_current_config;
    uint16_t m_LumaLUT[LUMA_LUT_SIZE];

    bool GenerateLumaLUT(
                  const stm_iqi_le_conf_t*                  const pConf,
                  uint16_t*                                 pLUT) const;

    bool GenerateFixedCurveTable(
                  const stm_iqi_le_fixed_curve_params_t*    const pParams,
                  int16_t*                                  const pCurve) const;
};

#endif /* _HQVDPLITE_IQI_LE_H_ */
