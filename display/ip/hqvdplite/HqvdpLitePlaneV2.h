/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2014-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#ifndef _HQVDPLITE_PLANE_V2_H_
#define _HQVDPLITE_PLANE_V2_H_

#include <display/generic/DisplayPlane.h>
#include "HqvdpLiteDisplayInfoV2.h"
#include "lld_V2/hqvdp_lld_api.h"
#include "HqvdpLiteIqiPeaking.h"
#include "HqvdpLiteIqiLe.h"
#include "HqvdpLiteIqiCti.h"

struct HqvdpLiteHwInfo_s
{
    unsigned int    offset;     // Offset of HQVDP instance in display device
    CVideoPlug*     pVideoPlug; // Video plug associated to HQVDP instance
    uint32_t        mixerBit;   // Mixer bit associated with HQVDP instance
};

enum HqvdpLiteProfile_e
{
    HQVDPLITE_PROFILE_FULL_HD,              // 1920x1080
    HQVDPLITE_PROFILE_4KP30,                // 3840x2160 30 fps
    HQVDPLITE_PROFILE_4KP60,                // 3840x2160 60 fps, standard quality
    HQVDPLITE_PROFILE_4KP60_PIXEL_REPEAT    // 3840x2160 60 fps, low quality
};

class CHqvdpLitePlaneV2: public CDisplayPlane
{
public:

    static bool Initialize(
                    const void*                     pDisplayDevBase,
                    int                             hqvdpHwNb,
                    const HqvdpLiteHwInfo_s         hqvdpHwInfo[]);

    static bool Terminate();

    CHqvdpLitePlaneV2(
                    const char*                     pName,
                    uint32_t                        planeId,
                    const CDisplayDevice*           pDisplayDev,
                    const HqvdpLiteProfile_e        profile,
                    bool                            isTemporalDeiAllowed,
                    const stm_plane_capabilities_t  caps,
                    const char                     *pixClockName,
                    const char                     *procClockName);

    virtual ~CHqvdpLitePlaneV2(void);

    virtual bool Create(void);

    virtual void PresentDisplayNode(
                    CDisplayNode*                   pPrevNode,
                    CDisplayNode*                   pCurrNode,
                    CDisplayNode*                   pNextNode,
                    bool                            isPictureRepeated,
                    bool                            isInterlaced,
                    bool                            isTopFieldOnDisplay,
                    const stm_time64_t&             vSyncTime);

    virtual void ProcessLastVsyncStatus(
                    const stm_time64_t&             vSyncTime,
                    CDisplayNode*                   pNodeDisplayed);

    virtual DisplayPlaneResults SetControl(
                    stm_display_plane_control_t     control,
                    uint32_t                        value);

    virtual DisplayPlaneResults GetControl(
                    stm_display_plane_control_t     control,
                    uint32_t*                       pValue) const;

    virtual bool GetControlRange(
                    stm_display_plane_control_t             control,
                    stm_display_plane_control_range_t*      pRange);

    virtual bool GetTuningDataRevision(
                    stm_display_plane_tuning_data_control_t ctrl,
                    uint32_t*                               pRevision);

    virtual DisplayPlaneResults GetTuningDataControl(
                    stm_display_plane_tuning_data_control_t ctrl,
                    stm_tuning_data_t*                      pTuningData);

    virtual DisplayPlaneResults SetTuningDataControl(
                    stm_display_plane_tuning_data_control_t ctrl,
                    stm_tuning_data_t*                      pTuningData);

    virtual bool IsFeatureApplicable(
                    stm_plane_feature_t             feature,
                    bool*                           pApplicable) const;

    virtual bool GetCompoundControlRange(
                    stm_display_plane_control_t     selector,
                    stm_compound_control_range_t*   pRange);

    virtual DisplayPlaneResults SetCompoundControl(
                    stm_display_plane_control_t     ctrl,
                    void*                           pNewVal);

    virtual DisplayPlaneResults GetCompoundControl(
                    stm_display_plane_control_t     ctrl,
                    void*                           currentVal);

    virtual TuningResults SetTuning(
                    uint16_t                        service,
                    void*                           pInputList,
                    uint32_t                        inputListSize,
                    void*                           pOutputList,
                    uint32_t                        outputListSize);

    /*
     * Power Management stuff
     */
    virtual void Freeze(void);
    virtual void Resume(void);

protected:

    // -------------------------------------------------------------------
    // Protected data
    // -------------------------------------------------------------------

    bool   m_bUse8BitsSrcForRefUseCase;

private:

    // -------------------------------------------------------------------
    // Private types and constants
    // -------------------------------------------------------------------

    enum
    {
        HQVDPLITE_DEI_MOTION_BUFFERS_NB = 3
    };

    enum HqvdpLiteDeiMotionState_e
    {
        // Mapped on HW values, do not change
        HQVDPLITE_DEI_MOTION_OFF        = 0,
        HQVDPLITE_DEI_MOTION_INIT       = 1,
        HQVDPLITE_DEI_MOTION_LOW_CONF   = 2,
        HQVDPLITE_DEI_MOTION_FULL_CONF  = 3
    };

    enum HqvdpLiteDeiMode_e
    {
        HQVDPLITE_DEI_MODE_OFF,
        HQVDPLITE_DEI_MODE_VI,
        HQVDPLITE_DEI_MODE_DI,
        HQVDPLITE_DEI_MODE_MEDIAN,
        HQVDPLITE_DEI_MODE_3D,
        HQVDPLITE_MAX_DEI_MODES
    };

    struct HqvdpLiteConf_s
    {
        bool                isValid;
        hqvdp_lld_conf_s    conf;
    };

    // -------------------------------------------------------------------
    // Static private data
    // -------------------------------------------------------------------

    static int                  m_hqvdpHwNb;
    static HqvdpLiteHwInfo_s*   m_pHqvdpHwInfo;

    // -------------------------------------------------------------------
    // Private data
    // -------------------------------------------------------------------

    hqvdp_lld_profile_e         m_lldProfile;
    DMA_Area                    m_lldCmdBufferArea;
    hqvdp_lld_mem_desc_s        m_lldMemDesc;
    hqvdp_lld_hdl_t             m_lldHandle;
    bool                        m_bIsPixelRepeatAllowed;
    DMA_Area                    m_motionBuffersArea;
    uint32_t                    m_prevMotionBufferPhysAddr;
    uint32_t                    m_currMotionBufferPhysAddr;
    uint32_t                    m_nextMotionBufferPhysAddr;
    HqvdpLiteConf_s             m_hqvdpConf[MAX_USE_CASES];
    CVideoPlug*                 m_videoPlug[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE];
    uint32_t                    m_clockFreqInMHz;
    CHqvdpLiteIqiPeaking        m_iqiPeaking;
    CHqvdpLiteIqiLe             m_iqiLe;
    CHqvdpLiteIqiCti            m_iqiCti;
    bool                        m_bEndOfProcessingError;
    bool                        m_bUseFMD;
    Crc_t                       m_crcData;
    bool                        m_bHasProgressive2InterlacedHW;
    CHqvdpLiteDisplayInfoV2     m_hqvdpDisplayInfo;
    HqvdpLiteDeiMotionState_e   m_motionState;
    bool                        m_bIsTemporalDeiAllowed;

    // -------------------------------------------------------------------
    // Private methods
    // -------------------------------------------------------------------

    bool AllocateLldMemory(
                    const hqvdp_lld_mem_need_s&     lldMemNeeds,
                    DMA_Area*                       pLldCmdBuffer,
                    DMA_Area*                       pMotionBuffersArea,
                    uint32_t                        motionBuffersPhysAddr[HQVDPLITE_DEI_MOTION_BUFFERS_NB]) const;

    void FreeLldMemory(
                    DMA_Area*                       pLldCmdBufferArea,
                    DMA_Area*                       pMotionBuffersArea) const;

    bool OpenLldSession(
                    const hqvdp_lld_mem_desc_s&     lldMemDesc);

    void CloseLldSession();

    void ResetEveryUseCases();

    virtual bool IsScalingPossible(
                    CDisplayNode*                   pCurrNode,
                    CDisplayInfo*                   pDisplayInfo);

    virtual bool AdjustIOWindowsForHWConstraints(
                    CDisplayNode*                   pCurrNode,
                    CDisplayInfo*                   pDisplayInfo) const;

    bool IsScalingPossibleByHw(
                    CHqvdpLiteDisplayInfoV2*        pDisplayInfo);

    unsigned int MaxLinesSkippable(
                    unsigned int                    srcPicturePitch) const;

    int GetNumberOfHwRequired(
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo) const;

    bool GetIOWidthsOneHw(
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    uint32_t                        inputWidth,
                    uint32_t                        outputWidth,
                    uint32_t*                       pInputWidthOneHw,
                    uint32_t*                       pOutputWidthOneHw) const;

    bool IsScalingPossibleBySkippingLines(
                    CDisplayNode*                   pCurrNode,
                    CHqvdpLiteDisplayInfoV2*        pDisplayInfo);

    bool AreZoomConstraintsOk(
                    uint32_t                        vhsrcInputWidth,
                    uint32_t                        vhsrcInputHeight,
                    uint32_t                        vhsrcOutputWidth,
                    uint32_t                        vhsrcOutputHeight) const;

    bool IsHwProcessingTimeOk(
                    uint32_t                        vhsrcInputWidth,
                    uint32_t                        vhsrcInputHeight,
                    uint32_t                        vhsrcOutputWidth,
                    uint32_t                        vhsrcOutputHeight,
                    HqvdpLiteDeiMode_e              deiMode) const;

    bool IsSTBusDataRateOk(
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    HqvdpLiteDeiMode_e              deiMode);

    uint32_t GetSTBusDataRateTolerancePercent(
                    uint32_t                        fullOutputHeight,
                    uint32_t                        outputRefreshRateMilliHz) const;

    stm_rational_t GetNbrBytesPerPixel(
                    bool                            is420,
                    bool                            is422,
                    bool                            isOn10Bits,
                    bool                            isInterlaced,
                    HqvdpLiteDeiMode_e              deiMode) const;

    uint32_t ComputeSTBusDataRate(
                    uint32_t                        srcFrameWidth,
                    uint32_t                        srcFrameHeight,
                    bool                            isSrc420,
                    bool                            isSrc422,
                    bool                            isSrcOn10Bits,
                    bool                            isSrcInterlaced,
                    uint32_t                        dstFrameWidth,
                    uint32_t                        dstFrameHeight,
                    HqvdpLiteDeiMode_e              deiMode,
                    uint32_t                        verticalRefreshRate) const;

    bool FillHqvdpLiteDisplayInfo(
                    CDisplayNode*                   pCurrNode,
                    CHqvdpLiteDisplayInfoV2*        pDisplayInfo);

    bool UpdateDynamicConf(
                    const CDisplayNode*             pPrevNode,
                    const CDisplayNode*             pCurrNode,
                    const CDisplayNode*             pNextNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    bool                            isPictureRepeated,
                    bool                            isTopFieldOnDisplay,
                    bool                            isDisplayInterlaced,
                    hqvdp_lld_conf_s*               pHqvdpConf);

    void SetHwDisplayInfos(
                    CHqvdpLiteDisplayInfoV2*        pDisplayInfo) const;

    bool PrepareConf(
                    const CDisplayNode*             pCurrNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    hqvdp_lld_conf_s*               pHqvdpConf) const;

    bool PrepareTopParams(
                    const CDisplayNode*             pCurrNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    HQVDPLITE_TOP_Params_t*         pTopParams) const;

    bool PrepareVc1Params(
                    HQVDPLITE_VC1RE_Params_t*       pVC1ReParams) const;

    bool PrepareFmdParams(
                    HQVDPLITE_FMD_Params_t*         pFmdParams) const;

    bool PrepareCsdiParams(
                    HQVDPLITE_CSDI_Params_t*        pCsdiParams) const;

    bool PrepareHvsrcParams(
                    const CHqvdpLiteDisplayInfoV2*  pCurrNode,
                    HQVDPLITE_HVSRC_Params_t*       pHvsrcParams,
                    HQVDPLITE_LUT_HVSRC_Params_t*   pHvsrcLutParams) const;

    bool PrepareOutParams(
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    HQVDPLITE_OUT_Params_t*         pOutParams) const;

    bool SetPictureBaseAddresses(
                    const CDisplayNode*             pCurrNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    uint32_t*                       pLumaBaseAddress,
                    uint32_t*                       pChromaBaseAddress) const;

    bool SetCsdiParams(
                    const CDisplayNode*             pPrevNode,
                    const CDisplayNode*             pCurrNode,
                    const CDisplayNode*             pNextNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    const HQVDPLITE_TOP_Params_t*   pTopParams,
                    bool                            isPictureRepeated,
                    HQVDPLITE_CSDI_Params_t*        pCsdiParams);

    bool SetIqiParams(
                    const CDisplayNode*             pCurrNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    bool                            isTopFieldOnDisplay,
                    bool                            isDisplayInterlaced,
                    HQVDPLITE_IQI_Params_t*         pIqiParams);

    bool SetVideoPlugParams(
                    HQVDPLITE_VIDEOPLUG_Params_t*   pVideoPlugParams);

    void SetDeiNextFieldAddresses(
                    const CDisplayNode*             pCurrNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    HQVDPLITE_CSDI_Params_t*        pCsdiParams) const;

    void SetDeiPreviousFieldAddresses(
                    const CDisplayNode*             pCurrNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    HQVDPLITE_CSDI_Params_t*        pCsdiParams) const;

    void ApplyConf( const CDisplayNode*             pCurrNode,
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    const hqvdp_lld_conf_s*         pHqvdpConf);

    void RecoverFromEndOfProcessingError(
                    const CDisplayNode*             pAValidNode);

    void DisableHW(void);

    void StopHqvdp(void);

    void RotateMotionBuffers(void);

    void UpdateMotionState(void);

    CHqvdpLitePlaneV2::HqvdpLiteDeiMode_e SelectDeiMode(
                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                    const CDisplayNode*             pPrevNode,
                    const CDisplayNode*             pCurNode,
                    const CDisplayNode*             pNextNode) const;

    inline void SetDeiMotionState(
                    uint32_t&                       deiCtl,
                    HqvdpLiteDeiMotionState_e       motionState) const;

    inline void SetDeiMainMode(
                    uint32_t&                       deiCtl,
                    uint32_t                        mode) const;

    inline void SetDeiInterpolation(
                    uint32_t&                       deiCtl,
                    uint32_t                        lumaMode,
                    uint32_t                        chromaMode) const;

    inline void Set3dDeiConfiguration(
                    uint32_t&                       deiCtl) const;

    bool IsSecondaryPictureAvailable(
                    const CDisplayNode*             pDisplayNode) const;

    bool CheckInputPicture(
                    const CDisplayNode*             pCurrNode,
                    CHqvdpLiteDisplayInfoV2*        pDisplayInfo);

    bool IsColorFormatSupported(
                    stm_pixel_format_t              colorFormat,
                    bool                            bIsInterlaced) const;

    void DumpConf(  const hqvdp_lld_conf_s*         pConf) const;

    void DumpConfField(
                    const char*                     pStructName,
                    const char*                     pFieldName,
                    const uint32_t*                 pField,
                    unsigned int                    numFieldElts) const;

    CHqvdpLitePlaneV2(const CHqvdpLitePlaneV2&);
    CHqvdpLitePlaneV2& operator=(const CHqvdpLitePlaneV2&);
};

#endif // _HQVDPLITE_PLANE_V2_H_
