/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2014-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#ifndef _HQVDP_LITE_DISPLAY_INFO_V2_H_
#define _HQVDP_LITE_DISPLAY_INFO_V2_H_

#include "display/generic/DisplayInfo.h"

class CHqvdpLiteDisplayInfoV2: public CDisplayInfo
{
public:
    CHqvdpLiteDisplayInfoV2(void):CDisplayInfo()
    {
        Reset();
    }

    // Reset every HqvdpLiteDisplayInfoV2
    virtual void Reset(void)
    {
        CDisplayInfo::Reset();

        m_isPrimaryPictureSupported       = false;
        m_isSecondaryPictureSupported     = false;
        m_isUsingProgressive2InterlacedHW = false;
        m_is3DDeinterlacingPossible       = false;
    }

protected:
    // Information specific to HQVDP_Lite
    bool    m_isPrimaryPictureSupported;
    bool    m_isSecondaryPictureSupported;
    bool    m_isUsingProgressive2InterlacedHW;
    bool    m_is3DDeinterlacingPossible;

    // Only CHqvdpLitePlaneV2 is allowed to access to the hqvdpInfo
    friend class CHqvdpLitePlaneV2;
};

#endif /* _HQVDP_LITE_DISPLAY_INFO_V2_H_ */

