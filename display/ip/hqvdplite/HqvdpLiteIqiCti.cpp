/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-30
***************************************************************************/

#include "HqvdpLiteIqiCti.h"
#include "HqvdpLitePrivate.h"


#define IQI_CTIr2_DEFAULT_COR   0x01
#define IQI_CTIr2_DEFAULT_MED   0x1C
#define IQI_CTIr2_DEFAULT_MON   0x04


static const stm_iqi_cti_conf_t IQI_CTI_Config[PCIQIC_COUNT] =
{
    [PCIQIC_BYPASS] =
    {
        strength1 : IQICS_NONE,
        strength2 : IQICS_NONE
    },

    [PCIQIC_ST_SOFT] =
    {
        strength1 : IQICS_NONE,
        strength2 : IQICS_MIN
    },

    [PCIQIC_ST_MEDIUM] =
    {
        strength1 : IQICS_NONE,
        strength2 : IQICS_STRONG
    },

    [PCIQIC_ST_STRONG] =
    {
        strength1 : IQICS_MIN,
        strength2 : IQICS_MEDIUM
    }
};


CHqvdpLiteIqiCti::CHqvdpLiteIqiCti(void)
{
    CHqvdpLiteIqi::SetAlgoName("CTI");
    CHqvdpLiteIqi::SetPreset(PCIQIC_BYPASS);
    m_current_config = IQI_CTI_Config[PCIQIC_BYPASS];
}


CHqvdpLiteIqiCti::~CHqvdpLiteIqiCti(void)
{
}

void CHqvdpLiteIqiCti::CalculateParams(HQVDPLITE_IQI_Params_t* pParams) const
{
    SetBitField(pParams->CtiConfig, IQI_CTI_CONFIG_CTI_T_MON,     IQI_CTIr2_DEFAULT_MON);
    SetBitField(pParams->CtiConfig, IQI_CTI_CONFIG_CTI_T_MED,     IQI_CTIr2_DEFAULT_MED);
    SetBitField(pParams->CtiConfig, IQI_CTI_CONFIG_CTI_STRENGTH1, m_current_config.strength1);
    SetBitField(pParams->CtiConfig, IQI_CTI_CONFIG_CTI_STRENGTH2, m_current_config.strength2);
    SetBitField(pParams->CtiConfig, IQI_CTI_CONFIG_EXTENDED_CTI,  0);
    SetBitField(pParams->CtiConfig, IQI_CTI_CONFIG_CTI_COR,       IQI_CTIr2_DEFAULT_COR);

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "CTI CtiConfig = 0x%x", pParams->CtiConfig);
}


bool CHqvdpLiteIqiCti::SetConf(const stm_iqi_cti_conf_t* pConf)
{
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "CTI strength1 = %d", pConf->strength1 );
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "CTI strength2 = %d", pConf->strength2 );

    if (  !pConf
        || pConf->strength1 >= IQICS_COUNT
        || pConf->strength2 >= IQICS_COUNT)
    {
      IQI_TRC( TRC_ID_ERROR," Invalid CTI conf");
      return false;
    }

    m_current_config = *pConf;
    return true;
}


void CHqvdpLiteIqiCti::GetConf(stm_iqi_cti_conf_t* pConf) const
{
    *pConf = m_current_config;

    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "CTI strength1 = %d", pConf->strength1);
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "CTI strength2 = %d", pConf->strength2);
}


bool CHqvdpLiteIqiCti::SetPreset(stm_plane_ctrl_iqi_configuration_e preset)
{
    if (!CHqvdpLiteIqi::SetPreset(preset))
    {
        IQI_TRC( TRC_ID_ERROR, "Failed to set preset %s (%d)", PresetStr(preset), preset );
        return false;
    }

    m_current_config =  IQI_CTI_Config[preset];
    IQI_TRC( TRC_ID_HQVDPLITE_IQI, "Preset = %s (%d)", PresetStr(preset), preset );

    return true;
}
