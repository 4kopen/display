/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2014-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-09-30
***************************************************************************/

#include <stm_display.h>
#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/Output.h>
#include <display/ip/videoPlug/VideoPlug.h>
#include "HqvdpLitePlaneV2.h"
#include "HqvdpLitePrivate.h"

// ------------------------------------------------
// Static variables
// ------------------------------------------------
static const stm_pixel_format_t g_hqvdpLiteSurfaceFormats[] =
{
    SURF_YCBCR420MB,
    SURF_YCbCr420R2B,
    SURF_YCbCr422R2B,
    SURF_YCbCr422R2B10,
    SURF_CRYCB888,          /* For this format, only Progressive sources are accepted */
    SURF_CRYCB101010,       /* For this format, only Progressive sources are accepted */
    SURF_ACRYCB8888,        /* For this format, only Progressive sources are accepted */
    SURF_YCbCr420R2B10
};

static const stm_plane_feature_t g_hqvdpLitePlaneFeatures[] =
{
    PLANE_FEAT_VIDEO_SCALING,
    PLANE_FEAT_VIDEO_MADI,
    PLANE_FEAT_WINDOW_MODE,
    PLANE_FEAT_SRC_COLOR_KEY,
    PLANE_FEAT_TRANSPARENCY,
    PLANE_FEAT_GLOBAL_COLOR,
    PLANE_FEAT_VIDEO_FMD,
    PLANE_FEAT_VIDEO_IQI_PEAKING,
    PLANE_FEAT_VIDEO_IQI_LE,
    PLANE_FEAT_VIDEO_IQI_CTI
};

int                 CHqvdpLitePlaneV2::m_hqvdpHwNb     = 0;
HqvdpLiteHwInfo_s*  CHqvdpLitePlaneV2::m_pHqvdpHwInfo  = NULL;

// ------------------------------------------------
//  Constant definitions
// ------------------------------------------------

// Debug flags
#define HQVDPLITE_DEBUG_FORCE_SPATIAL_DEINTERLACING         0
#define HQVDPLITE_DEBUG_DUMP_HQVDP_COEFS                    0

// Misc constants
#define HQVDPLITE_SCALING_FACTOR_HARDWARE_LIMIT             16
#define HQVDPLITE_MAX_VERTICAL_SCALING_FACTOR               8
#define HQVDPLITE_MAX_HORIZONTAL_SCALING_FACTOR             16
#define HQVDPLITE_MAX_PITCH_IN_BYTES                        16383
#define HQVDPLITE_MIN_INPUT_WIDTH                           48
#define HQVDPLITE_MAX_INPUT_WIDTH                           4096
#define HQVDPLITE_MIN_INPUT_HEIGHT                          16
#define HQVDPLITE_MAX_INPUT_HEIGHT                          2160
#define HQVDPLITE_MIN_OUTPUT_WIDTH                          48
#define HQVDPLITE_MAX_OUTPUT_WIDTH                          4096
#define HQVDPLITE_MIN_OUTPUT_HEIGHT                         16
#define HQVDPLITE_MAX_OUTPUT_HEIGHT                         2160
#define HQVDPLITE_DEFAULT_CLOCK_FREQUENCY_MHz               400
#define HQVDPLITE_VERTICAL_REFRESH_RATE_30Hz                30
#define HQVDPLITE_VERTICAL_REFRESH_RATE_50Hz                50
#define HQVDPLITE_VERTICAL_REFRESH_RATE_60Hz                60
#define HQVDPLITE_3D_DEI_PICTURES_NB                        3
#define HQVDPLITE_MAX_FIRMWARE_CYCLES                       1000

// DEI modes defined in string, to be printed out
#define HQVDPLITE_DEI_MODE_STR(m)                   ((m) == HQVDPLITE_DEI_MODE_OFF      ? "OFF"      : \
                                                     (m) == HQVDPLITE_DEI_MODE_VI       ? "VI"       : \
                                                     (m) == HQVDPLITE_DEI_MODE_DI       ? "DI"       : \
                                                     (m) == HQVDPLITE_DEI_MODE_MEDIAN   ? "MEDIAN"   : \
                                                     (m) == HQVDPLITE_DEI_MODE_3D       ? "3D"       : \
                                                     "Unrecognized DEI mode")


// In Secure Datapath context, alignment constraint is given by
// T3CAF configuration inside the MES hardware
#define HQVDPLITE_SDP_ALIGNMENT                             64

// For a given use case, the STBus data rate can be higher than the data
// rate of the reference use case of below percentages
#define HQVDPLITE_STBUS_DATARATE_TOLERANCE_LOW_PC           25
#define HQVDPLITE_STBUS_DATARATE_TOLERANCE_HIGH_PC          40

// Display standard definitions
#define HQVDPLITE_HD_720P_WIDTH                             1280
#define HQVDPLITE_HD_720P_HEIGHT                            720
#define HQVDPLITE_FULL_HD_WIDTH                             1920
#define HQVDPLITE_FULL_HD_HEIGHT                            1080
#define HQVDPLITE_ULTRA_HD_WIDTH                            3840
#define HQVDPLITE_ULTRA_HD_HEIGHT                           2160
#define HQVDPLITE_4K2K_WIDTH                                4096
#define HQVDPLITE_4K2K_HEIGHT                               2160

// Decoders output macro-block aligned Full HD pictures
#define HQVDPLITE_MB_ALIGNED_FULL_HD_HEIGHT                 (HQVDPLITE_FULL_HD_HEIGHT+8)

/* TOP CONFIG*/
#define HQVDPLITE_CONFIG_PROGRESSIVE                        ((1 << TOP_CONFIG_PROGRESSIVE_SHIFT  ) & TOP_CONFIG_PROGRESSIVE_MASK  )
#define HQVDPLITE_CONFIG_TOP_NOT_BOT                        ((1 << TOP_CONFIG_TOP_NOT_BOT_SHIFT  ) & TOP_CONFIG_TOP_NOT_BOT_MASK  )
#define HQVDPLITE_CONFIG_IT_EOP                             ((1 << TOP_CONFIG_IT_ENABLE_EOP_SHIFT) & TOP_CONFIG_IT_ENABLE_EOP_MASK)
#define HQVDPLITE_CONFIG_PASS_THRU                          ((1 << TOP_CONFIG_PASS_THRU_SHIFT    ) & TOP_CONFIG_PASS_THRU_MASK    )

#define HQVDPLITE_CONFIG_NB_OF_FIELD_3                      ((TOP_CONFIG_NB_OF_FIELD_3 << TOP_CONFIG_NB_OF_FIELD_SHIFT) & TOP_CONFIG_NB_OF_FIELD_MASK)
#define HQVDPLITE_CONFIG_NB_OF_FIELD_5                      ((TOP_CONFIG_NB_OF_FIELD_5 << TOP_CONFIG_NB_OF_FIELD_SHIFT) & TOP_CONFIG_NB_OF_FIELD_MASK)

/* TOP MEM FORMAT */
#define HQVDPLITE_INPUT_FORMAT_420_RASTER_DUAL_BUFFER       0x00
#define HQVDPLITE_INPUT_FORMAT_422_RASTER_DUAL_BUFFER       0x01
#define HQVDPLITE_INPUT_FORMAT_422_RASTER_SINGLE_BUFFER     0x05
#define HQVDPLITE_INPUT_FORMAT_444_RSB_WITH_ALPHA           0x06
#define HQVDPLITE_INPUT_FORMAT_444_RASTER_SINGLE_BUFFER     0x07
#define HQVDPLITE_INPUT_FORMAT_420_MB_DUAL_BUFFER           0x08
#define HQVDPLITE_MEM_FORMAT_420_RASTER_DUAL_BUFFER         ((HQVDPLITE_INPUT_FORMAT_420_RASTER_DUAL_BUFFER   << TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_422_RASTER_SINGLE_BUFFER       ((HQVDPLITE_INPUT_FORMAT_422_RASTER_SINGLE_BUFFER << TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_420_MACROBLOCK                 ((HQVDPLITE_INPUT_FORMAT_420_MB_DUAL_BUFFER       << TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_444_RASTER_SINGLE_BUFFER       ((HQVDPLITE_INPUT_FORMAT_444_RASTER_SINGLE_BUFFER << TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_444_RSB_WITH_ALPHA             ((HQVDPLITE_INPUT_FORMAT_444_RSB_WITH_ALPHA       << TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_422_RASTER_DUAL_BUFFER         ((HQVDPLITE_INPUT_FORMAT_422_RASTER_DUAL_BUFFER   << TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_FORMAT_MASK)

#define HQVDPLITE_INPUT_PIX10BIT_NOT_8BIT                   0x1
#define HQVDPLITE_MEM_FORMAT_PIX10BIT_NOT_8BIT              ((HQVDPLITE_INPUT_PIX10BIT_NOT_8BIT << TOP_MEM_FORMAT_PIX10BIT_NOT_8BIT_SHIFT) & TOP_MEM_FORMAT_PIX10BIT_NOT_8BIT_MASK)

#define HQVDPLITE_OUTPUT_FORMAT_420_RASTER_DUAL_BUFFER      0x0
#define HQVDPLITE_OUTPUT_FORMAT_422_RASTER_SINGLE_BUFFER    0x5
#define HQVDPLITE_OUTPUT_FORMAT_444_RASTER_SINGLE_BUFFER    0x6
#define HQVDPLITE_MEM_FORMAT_OUTPUTFORMAT_420               ((HQVDPLITE_OUTPUT_FORMAT_420_RASTER_DUAL_BUFFER   << TOP_MEM_FORMAT_OUTPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_OUTPUTFORMAT_422               ((HQVDPLITE_OUTPUT_FORMAT_422_RASTER_SINGLE_BUFFER << TOP_MEM_FORMAT_OUTPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_OUTPUTFORMAT_444               ((HQVDPLITE_OUTPUT_FORMAT_444_RASTER_SINGLE_BUFFER << TOP_MEM_FORMAT_OUTPUT_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_FORMAT_MASK)

#define HQVDPLITE_OUTPUT_ALPHA                              0x80
#define HQVDPLITE_MEM_FORMAT_OUTPUTALPHA                    ((HQVDPLITE_OUTPUT_ALPHA << TOP_MEM_FORMAT_OUTPUT_ALPHA_SHIFT) & TOP_MEM_FORMAT_OUTPUT_ALPHA_MASK)

#define HQVDPLITE_MEM_FORMAT_MEM_TO_TV                      0x1
#define HQVDPLITE_MEM_FORMAT_MEM_TO_MEM                     0x0
#define HQVDPLITE_MEM_FORMAT_MEMTOTV                        ((HQVDPLITE_MEM_FORMAT_MEM_TO_TV  << TOP_MEM_FORMAT_MEM_TO_TV_SHIFT ) & TOP_MEM_FORMAT_MEM_TO_TV_MASK)
#define HQVDPLITE_MEM_FORMAT_MEMTOMEM                       ((~HQVDPLITE_MEM_FORMAT_MEM_TO_TV << TOP_MEM_FORMAT_MEM_TO_MEM_SHIFT) & TOP_MEM_FORMAT_MEM_TO_MEM_MASK)

#define HQVDPLITE_FLAT_3D                                   0x1
#define HQVDPLITE_FLAT_3D_NONE                              0x0
#define HQVDPLITE_MEM_FORMAT_FLAT3D                         ((HQVDPLITE_FLAT_3D      << TOP_MEM_FORMAT_FLAT3D_SHIFT) & TOP_MEM_FORMAT_FLAT3D_MASK)
#define HQVDPLITE_MEM_FORMAT_FLAT3D_NONE                    ((HQVDPLITE_FLAT_3D_NONE << TOP_MEM_FORMAT_FLAT3D_SHIFT) & TOP_MEM_FORMAT_FLAT3D_MASK)

#define HQVDPLITE_VSYNC_LEFT                                0x0
#define HQVDPLITE_VSYNC_RIGHT                               0x1
#define HQVDPLITE_MEM_FORMAT_RIGHT                          ((HQVDPLITE_VSYNC_RIGHT << TOP_MEM_FORMAT_RIGHTNOTLEFT_SHIFT) & TOP_MEM_FORMAT_RIGHTNOTLEFT_MASK)
#define HQVDPLITE_MEM_FORMAT_LEFT                           ((HQVDPLITE_VSYNC_LEFT  << TOP_MEM_FORMAT_RIGHTNOTLEFT_SHIFT) & TOP_MEM_FORMAT_RIGHTNOTLEFT_MASK)

#define HQVDPLITE_MEM_FORMAT_OUTPUT_2D                      0x0
#define HQVDPLITE_MEM_FORMAT_OUTPUT_3D_SBS                  0x1
#define HQVDPLITE_MEM_FORMAT_OUTPUT_3D_TAB                  0x2
#define HQVDPLITE_MEM_FORMAT_OUTPUT_3D_FP                   0x3
#define HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_2D             ((HQVDPLITE_MEM_FORMAT_OUTPUT_2D     << TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_SBS            ((HQVDPLITE_MEM_FORMAT_OUTPUT_3D_SBS << TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_TAB            ((HQVDPLITE_MEM_FORMAT_OUTPUT_3D_TAB << TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_FP             ((HQVDPLITE_MEM_FORMAT_OUTPUT_3D_FP  << TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK)

#define HQVDPLITE_MEM_FORMAT_PIX8TO10_Y                     0x2
#define HQVDPLITE_MEM_FORMAT_PIX8TO10LSB_Y                  ((HQVDPLITE_MEM_FORMAT_PIX8TO10_Y << TOP_MEM_FORMAT_PIX8TO10_LSB_Y_SHIFT) & TOP_MEM_FORMAT_PIX8TO10_LSB_Y_MASK)

#define HQVDPLITE_MEM_FORMAT_PIX8TO10_C                     0x0
#define HQVDPLITE_MEM_FORMAT_PIX8TO10LSB_C                  ((HQVDPLITE_MEM_FORMAT_PIX8TO10_C << TOP_MEM_FORMAT_PIX8TO10_LSB_C_SHIFT) & TOP_MEM_FORMAT_PIX8TO10_LSB_C_MASK)

#define HQVDPLITE_MEM_FORMAT_INPUT_2D                       0x0
#define HQVDPLITE_MEM_FORMAT_INPUT_3D_SBS                   0x1
#define HQVDPLITE_MEM_FORMAT_INPUT_3D_TAB                   0x2
#define HQVDPLITE_MEM_FORMAT_INPUT_3D_FP                    0x3
#define HQVDPLITE_MEM_FORMAT_INPUT_DOLBY_BL                 0x4
#define HQVDPLITE_MEM_FORMAT_INPUT_DOLBY_BL_EL              0x5
#define HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_2D              ((HQVDPLITE_MEM_FORMAT_INPUT_2D     << TOP_MEM_FORMAT_INPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_3D_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_SBS             ((HQVDPLITE_MEM_FORMAT_INPUT_3D_SBS << TOP_MEM_FORMAT_INPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_3D_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_TAB             ((HQVDPLITE_MEM_FORMAT_INPUT_3D_TAB << TOP_MEM_FORMAT_INPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_3D_FORMAT_MASK)
#define HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_FP              ((HQVDPLITE_MEM_FORMAT_INPUT_3D_FP  << TOP_MEM_FORMAT_INPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_INPUT_3D_FORMAT_MASK)

#define HQVDPLITE_CRC_RESET_ALL                             TOP_CRC_RESET_CTRL_MASK

/* DEI */
/* NEXT_NOT_PREV: */
#define HQVDPLITE_DEI_CTL_NEXT_NOT_PREV_USE_PREV            0
#define HQVDPLITE_DEI_CTL_NEXT_NOT_PREV_USE_NEXT            1
/* DIR_REC_EN: */
#define HQVDPLITE_DEI_CTL_DIR_RECURSIVITY_OFF               0
#define HQVDPLITE_DEI_CTL_DIR_RECURSIVITY_ON                1
/* KMOV_FACTOR : Defines the motion factor. */
#define HQVDPLITE_DEI_CTL_KMOV_14_16                        14
#define HQVDPLITE_DEI_CTL_KMOV_12_16                        12
#define HQVDPLITE_DEI_CTL_KMOV_10_16                        10
#define HQVDPLITE_DEI_CTL_KMOV_8_16                         8
/* Reduce Motion */
#define HQVDPLITE_DEI_CTL_8_BITS_MOTION                     0
#define HQVDPLITE_DEI_CTL_4_BITS_MOTION                     1
/* MD: Motion Detector */
#define HQVDPLITE_DEI_CTL_MD_MODE_OFF                       0x00
#define HQVDPLITE_DEI_CTL_MD_MODE_INIT                      0x01
#define HQVDPLITE_DEI_CTL_MD_MODE_LOW                       0x02
#define HQVDPLITE_DEI_CTL_MD_MODE_FULL                      0x03
#define HQVDPLITE_DEI_CTL_MD_MODE_NO_RECURSIVE              0x04
/* CHROMA_INTERP : Defines the interpolation algorithm used for chroma */
#define HQVDPLITE_DEI_CTL_CHROMA_VI                         0x00
#define HQVDPLITE_DEI_CTL_CHROMA_DI                         0x01
#define HQVDPLITE_DEI_CTL_CHROMA_MEDIAN                     0x02
#define HQVDPLITE_DEI_CTL_CHROMA_3D                         0x03
/* LUMA_INTERP : Defines the interpolation algorithm used for luma */
#define HQVDPLITE_DEI_CTL_LUMA_VI                           0x00
#define HQVDPLITE_DEI_CTL_LUMA_DI                           0x01
#define HQVDPLITE_DEI_CTL_LUMA_MEDIAN                       0x02
#define HQVDPLITE_DEI_CTL_LUMA_3D                           0x03
/* MAIN_MODE : Selects a group of configurations for the deinterlacer */
#define HQVDPLITE_DEI_CTL_MAIN_MODE_BYPASS_ON               0x00
#define HQVDPLITE_DEI_CTL_MAIN_MODE_FIELD_MERGING           0x01
#define HQVDPLITE_DEI_CTL_MAIN_MODE_DEINTERLACING           0x02
/* Median or DMDI mode */
#define HQVDPLITE_DEI_CTL_MEDIAN_DMDI_MASK                  0x1
#define HQVDPLITE_DEI_CTL_DMDI                              0x01
#define HQVDPLITE_DEI_CTL_MEDIAN_DMDI_SHIFT                 10

/* 3D DEI recommended values */
#define HQVDPLITE_DEI_CTL_BEST_KCOR_VALUE                   12  /* recommended value for KCOR */
#define HQVDPLITE_DEI_CTL_BEST_DETAIL_VALUE                 15  /* T_DETAIL has been increased in order to reduce the noise sensibility */
#define HQVDPLITE_DEI_CTL_BEST_KMOV_VALUE                   HQVDPLITE_DEI_CTL_KMOV_12_16
#define HQVDPLITE_CSDI_CONFIG2_INFLEXION_MOTION             0x1
#define HQVDPLITE_CSDI_CONFIG2_FIR_LENGTH                   0x2
#define HQVDPLITE_CSDI_CONFIG2_MOTION_FIR                   0x1
#define HQVDPLITE_CSDI_CONFIG2_MOTION_CLOSING               0x1
#define HQVDPLITE_CSDI_CONFIG2_DETAILS                      0x0
#define HQVDPLITE_CSDI_CONFIG2_CHROMA_MOTION                0x1
#define HQVDPLITE_CSDI_CONFIG2_ADAPTIVE_FADING              0x1
#define HQVDPLITE_CSDI_DCDI_CONFIG_ALPHA_LIMIT              0x20
#define HQVDPLITE_CSDI_DCDI_CONFIG_MEDIAN_MODE              0x3
#define HQVDPLITE_CSDI_DCDI_CONFIG_WIN_CLAMP_SIZE           0x8
#define HQVDPLITE_CSDI_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP   0x1
#define HQVDPLITE_CSDI_DCDI_CONFIG_ALPHA_MEDIAN_EN          0x1

/* HVSRC FILTERING_CTRL */

/* HVSRC PARAM_CTRL */
#define HQVDPLITE_HVSRC_V_AUTOKOVS_DEFAULT                  0x0  /* Disable Auto Control as it doesn't work well */
#define HQVDPLITE_HVSRC_H_AUTOKOVS_DEFAULT                  0x0  /* Disable Auto Control as it doesn't work well */
#define HQVDPLITE_HVSRC_YH_KOVS_DISABLE                     0x8  /* Overshoot disabled in downscaling (8=off) */
#define HQVDPLITE_HVSRC_YH_KOVS_MAX                         0x0  /* Overshoot enabled in upscaling (0=max)*/
#define HQVDPLITE_HVSRC_YV_KOVS_DISABLE                     0x8
#define HQVDPLITE_HVSRC_YV_KOVS_MAX                         0x0
#define HQVDPLITE_HVSRC_CH_KOVS_DISABLE                     0x8
#define HQVDPLITE_HVSRC_CH_KOVS_MAX                         0x0
#define HQVDPLITE_HVSRC_CV_KOVS_DISABLE                     0x8
#define HQVDPLITE_HVSRC_CV_KOVS_MAX                         0x0
#define HQVDPLITE_HVSRC_H_ACON_DEFAULT                      0x1  /* Antiringing enabled in upscaling */
#define HQVDPLITE_HVSRC_H_ACON_DISABLE                      0x0  /* Antiringing disabled in downscaling */
#define HQVDPLITE_HVSRC_V_ACON_DEFAULT                      0x1
#define HQVDPLITE_HVSRC_V_ACON_DISABLE                      0x0

/* IQI */
#define HQVDPLITE_IQI_CONTRAST_BRIGHTNESS_GAIN_DEFAULT      (1*256)
#define HQVDPLITE_IQI_CONTRAST_BRIGHTNESS_OFFSET_DEFAULT    0
#define HQVDPLITE_IQI_SATURATION_GAIN_DEFAULT               (1*256)

// ------------------------------------------------
//  Macro definitions
// ------------------------------------------------

#define DUMP_CONF_FIELD(pConf,Struct,Field)                                                                                 \
do                                                                                                                          \
{                                                                                                                           \
    DumpConfField( #Struct, #Field, (const uint32_t*)&pConf->Struct.Field, sizeof(pConf->Struct.Field) / sizeof(uint32_t)); \
                                                                                                                            \
} while(0)


#define CheckProcClockActivation()                                                                  \
do                                                                                                  \
{                                                                                                   \
    if(!m_isProcClkEnabled)                                                                         \
    {                                                                                               \
       PLANE_TRC( TRC_ID_ERROR, "Communicating with LLD driver while HQVDP Clock not started!");    \
    }                                                                                               \
} while(0)


/*******************************************************************************
Name        : Initialize
Description : Performs the HQVDP subsystem initializations
Parameters  : pDisplayDevBase - [in] Logical address of Display device hardware
              hqvdpHwNb       - [in] Number of HQVDP hardware instances
              hqvdpHwInfo     - [in] HQVDP hardware instances info
Assumptions : Must be called only once, prior to any plane instantiation
Limitations :
Returns     : true if init passed, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::Initialize(
                    const void*             pDisplayDevBase,
                    int                     hqvdpHwNb,
                    const HqvdpLiteHwInfo_s hqvdpHwInfo[])
{
    TRCIN(TRC_ID_MAIN_INFO, "");

    // Check that Init is not already done
    if(m_pHqvdpHwInfo != NULL)
    {
        TRC(TRC_ID_ERROR, "Previous session has not been terminated, cannot initialize");
        TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    enum hqvdp_lld_error    err = HQVDP_LLD_NO_ERR;
    hqvdp_lld_init_s*       pLldInit = (hqvdp_lld_init_s*)vibe_os_allocate_memory(hqvdpHwNb * sizeof(hqvdp_lld_init_s));

    // Check that init table could be allocated
    if(pLldInit == NULL)
    {
        TRC(TRC_ID_ERROR, "Failed to allocate LLD init table");
        TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    // Prepare LLD init params
    for(int i = 0; i < hqvdpHwNb; i++)
    {
        pLldInit[i].fw.mode            = FW_LOAD_T1;
        pLldInit[i].fw.pmem.size       = HQVDPLITE_PMEM_SIZE;
        pLldInit[i].fw.pmem.alignment  = 1;
        pLldInit[i].fw.pmem.logical    = (void*)HQVDPLITE_pmem;
        pLldInit[i].fw.pmem.physical   = NULL;
        pLldInit[i].fw.dmem.size       = HQVDPLITE_DMEM_SIZE;
        pLldInit[i].fw.dmem.alignment  = 1;
        pLldInit[i].fw.dmem.logical    = (void*)HQVDPLITE_dmem;
        pLldInit[i].fw.dmem.physical   = NULL;
        pLldInit[i].hqvdp_base_addr    = (const void*)((const uint8_t*)pDisplayDevBase + hqvdpHwInfo[i].offset);
        pLldInit[i].video_plug_handle  = hqvdpHwInfo[i].pVideoPlug;
        pLldInit[i].mixer_bit          = hqvdpHwInfo[i].mixerBit;
        pLldInit[i].debug_settings     = NULL;
    }

    // Initialize LLD
    err = hqvdp_lld_init(hqvdpHwNb, pLldInit);
    if(err != HQVDP_LLD_NO_ERR)
    {
        vibe_os_free_memory(pLldInit);
        pLldInit = 0;
        TRC(TRC_ID_ERROR, "hqvdp_lld_init() failed, err=%d", err);
        TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    // LLD init done
    TRC(TRC_ID_MAIN_INFO, "HQVDP LLD initialized (FW package version %s)", hqvdp_lld_get_firmware_package_version());
    vibe_os_free_memory(pLldInit);
    pLldInit = 0;

    // Backup HQVDP HW info
    m_pHqvdpHwInfo = new HqvdpLiteHwInfo_s[hqvdpHwNb];
    if(m_pHqvdpHwInfo == NULL)
    {
        hqvdp_lld_terminate();
        TRC(TRC_ID_ERROR, "Failed to allocate HQVDP HW info table");
        TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }
    for(int i=0; i<hqvdpHwNb; i++)
    {
        m_pHqvdpHwInfo[i] = hqvdpHwInfo[i];
    }
    m_hqvdpHwNb = hqvdpHwNb;

    // HQVDP subsystem init done
    TRCOUT(TRC_ID_MAIN_INFO, "");
    return true;
}


/*******************************************************************************
Name        : Terminate
Description : Performs the HQVDP subsystem deinitialization
Parameters  :
Assumptions : Must be called only once, once all planes are deleted
Limitations :
Returns     : true if deinit passed, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::Terminate()
{
    TRCIN(TRC_ID_MAIN_INFO, "");

    // Check that Init is done
    if(m_pHqvdpHwInfo == NULL)
    {
        TRC(TRC_ID_ERROR, "HQVDP subsytem is not initialized, cannot terminate");
        TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    // Terminate LLD
    enum hqvdp_lld_error lldErr = hqvdp_lld_terminate();
    if(lldErr != HQVDP_LLD_NO_ERR)
    {
        TRC(TRC_ID_ERROR, "hqvdp_lld_terminate() failed, err=%d", lldErr);
        TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }
    TRC(TRC_ID_MAIN_INFO, "HQVDP LLD terminated");

    // Release HQVDP HW info table
    delete [] m_pHqvdpHwInfo;
    m_pHqvdpHwInfo = NULL;
    m_hqvdpHwNb    = 0;

    TRCOUT(TRC_ID_MAIN_INFO, "");
    return true;
}


/*******************************************************************************
Name        : CHqvdpLitePlaneV2
Description : HQVDP Lite plane instance constructor
Parameters  : pName                    - [in] Plane Name
              planeId                  - [in] Plane Id
              pDisplayDev              - [in] Associated display device
              profile                  - [in] Plane profile (Full HD, 4Kp30, 4Kp60)
              isTemporalDeiAllowed     - [in] Allow/disallow temporal deinterlacing
              caps                     - [in] Plane capabilities
              pixClockName             - [in] Pixel clock name
              procClockName            - [in] Processing clock name
Assumptions : LLD has been initialized (hqvdp_lld_init called)
Limitations :
Returns     : void
*******************************************************************************/
CHqvdpLitePlaneV2::CHqvdpLitePlaneV2(
                                const char*                       pName,
                                uint32_t                          planeId,
                                const CDisplayDevice*             pDisplayDev,
                                const HqvdpLiteProfile_e          profile,
                                bool                              isTemporalDeiAllowed,
                                const stm_plane_capabilities_t    caps,
                                const char                      * pixClockName,
                                const char                      * procClockName): CDisplayPlane(pName,
                                                                                                planeId,
                                                                                                pDisplayDev,
                                                                                                caps,
                                                                                                pixClockName,
                                                                                                procClockName)
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "self: %p, id: %u", this, planeId);

    // Init base class members
    m_fixedpointONE         = 1 << 13;              /* n.13 fixed point for DISP rescaling */
    m_ulMaxHSrcInc          = 4 * m_fixedpointONE;  /* downscale 4x */
    m_ulMinHSrcInc          = m_fixedpointONE / 32; /* upscale 32x */
    m_ulMaxVSrcInc          = 4 * m_fixedpointONE;
    m_ulMinVSrcInc          = m_fixedpointONE / 32;
    m_pSurfaceFormats       = g_hqvdpLiteSurfaceFormats;
    m_nFormats              = N_ELEMENTS(g_hqvdpLiteSurfaceFormats);
    m_pFeatures             = g_hqvdpLitePlaneFeatures;
    m_nFeatures             = N_ELEMENTS(g_hqvdpLitePlaneFeatures);
    m_hasADeinterlacer      = true;

    SetScalingCapabilities(&m_rescale_caps);

    // Init class members
    vibe_os_zero_memory(&m_crcData,             sizeof(m_crcData));
    vibe_os_zero_memory(&m_lldCmdBufferArea,    sizeof(m_lldCmdBufferArea));
    vibe_os_zero_memory(&m_motionBuffersArea,   sizeof(m_motionBuffersArea));
    vibe_os_zero_memory(&m_lldMemDesc,          sizeof(m_lldMemDesc));
    vibe_os_zero_memory(&m_hqvdpConf,           sizeof(m_hqvdpConf));
    vibe_os_zero_memory(&m_videoPlug,           sizeof(m_videoPlug));

    m_lldHandle                     = 0;
    m_bIsPixelRepeatAllowed         = false;
    m_clockFreqInMHz                = HQVDPLITE_DEFAULT_CLOCK_FREQUENCY_MHz;
    m_motionState                   = HQVDPLITE_DEI_MOTION_INIT;
    m_bEndOfProcessingError         = false;
    m_bUseFMD                       = false;
    m_bHasProgressive2InterlacedHW  = true;
    m_prevMotionBufferPhysAddr      = 0;
    m_currMotionBufferPhysAddr      = 0;
    m_nextMotionBufferPhysAddr      = 0;
    m_bIsTemporalDeiAllowed         = isTemporalDeiAllowed;
    m_bUse8BitsSrcForRefUseCase     = false;

    switch(profile)
    {
        case HQVDPLITE_PROFILE_FULL_HD:
        {
            m_lldProfile = HQVDP_PROFILE_FULL_HD;
            break;
        }
        case HQVDPLITE_PROFILE_4KP30:
        {
            m_lldProfile = HQVDP_PROFILE_4KP30;
            break;
        }
        case HQVDPLITE_PROFILE_4KP60:
        {
            m_lldProfile = HQVDP_PROFILE_4KP60;
            break;
        }
        case HQVDPLITE_PROFILE_4KP60_PIXEL_REPEAT:
        {
            m_lldProfile = HQVDP_PROFILE_4KP60_PR;
            m_bIsPixelRepeatAllowed = true;
            break;
        }
        default:
        {
            PLANE_TRC(TRC_ID_ERROR, "Unrecognized profile %d, defaulting to FULL HD", profile);
            m_lldProfile = HQVDP_PROFILE_FULL_HD;
            break;
        }
    }

    m_hqvdpDisplayInfo.Reset();
    m_iqiPeaking.SetPlaneName(pName);
    m_iqiLe.SetPlaneName(pName);
    m_iqiCti.SetPlaneName(pName);

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "self: %p", this);
}

/*******************************************************************************
Name        : ~CHqvdpLitePlaneV2
Description : HQVDP Lite plane instance destructor
Parameters  : None
Assumptions : Called prior to LLD termination (hqvdp_lld_terminate)
Limitations :
Returns     : void
*******************************************************************************/
CHqvdpLitePlaneV2::~CHqvdpLitePlaneV2(void)
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    CloseLldSession();
    FreeLldMemory(&m_lldCmdBufferArea, &m_motionBuffersArea);

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
}

/*******************************************************************************
Name        : Create
Description : HQVDP Lite actual instance constructor
Parameters  : None
Assumptions :
Limitations :
Returns     : true if successfully constructed, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::Create(void)
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    bool                    bOk = true;
    hqvdp_lld_mem_need_s    lldMemNeeds;
    uint32_t                motionBuffersPhysAddr[HQVDPLITE_DEI_MOTION_BUFFERS_NB];
    hqvdp_lld_error         err = HQVDP_LLD_NO_ERR;

    // Check that Init is done
    if(m_pHqvdpHwInfo == NULL)
    {
        PLANE_TRC(TRC_ID_ERROR, "HQVDP subsytem is not initialized, cannot create plane");
        PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    // Create base class
    bOk = CDisplayPlane::Create();
    if(!bOk)
    {
        PLANE_TRC(TRC_ID_ERROR, "CDisplayPlane::Create failed");
        PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    // Get LLD internal memory needs
    err = hqvdp_lld_get_memory_needs(m_lldProfile, &lldMemNeeds);
    if(err != HQVDP_LLD_NO_ERR)
    {
        PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_get_memory_needs() failed, err=%d", err);
        PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    // Allocate memory for LLD internal needs
    bOk = AllocateLldMemory(lldMemNeeds, &m_lldCmdBufferArea, &m_motionBuffersArea, motionBuffersPhysAddr);
    if(!bOk)
    {
        PLANE_TRC(TRC_ID_ERROR, "AllocateLldMemory failed");
        PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    // Backup motion buffers stuff
    m_prevMotionBufferPhysAddr = motionBuffersPhysAddr[0];
    m_currMotionBufferPhysAddr = motionBuffersPhysAddr[1];
    m_nextMotionBufferPhysAddr = motionBuffersPhysAddr[2];

    // Prepare LLD memory descriptor
    m_lldMemDesc.size       = lldMemNeeds.cmd_size;
    m_lldMemDesc.alignment  = lldMemNeeds.cmd_alignment;
    m_lldMemDesc.logical    = m_lldCmdBufferArea.pData;
    m_lldMemDesc.physical   = (void*)m_lldCmdBufferArea.ulPhysical;

    // Open LLD session
    bOk = OpenLldSession(m_lldMemDesc);
    if(!bOk)
    {
        FreeLldMemory(&m_lldCmdBufferArea, &m_motionBuffersArea);
        PLANE_TRC(TRC_ID_ERROR, "Failed to open LLD session");
        PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }

    if (m_procClockName[0] != '\0')
    {
        const uint32_t freqMhz = vibe_os_clk_get_rate(&m_procClock) / 1000000;
        m_clockFreqInMHz = freqMhz !=0 ? freqMhz : HQVDPLITE_DEFAULT_CLOCK_FREQUENCY_MHz;
        PLANE_TRC(TRC_ID_MAIN_INFO, "HQVDP clock rate is %u MHz!", m_clockFreqInMHz);
    }

    if (!PauseCreateWorkQueue() )
    {
        PLANE_TRC( TRC_ID_ERROR, "PauseCreate() returns error" );
        return false;
    }

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
    return true;
}

/*******************************************************************************
Name        : AllocateLldMemory
Description : Allocate memory for LLD internal needs.
               - One area for LLD/FW commands.
               - One area for motion buffers.
Parameters  : lldMemNeeds           - [in]  LLD memory needs
              pLldCmdBufferArea     - [out] LLD/FW commands buffers area descriptor
              pMotionBuffersArea    - [out] Motion buffers area descriptor
              motionBuffersPhysAddr - [out] The physical addresses of individual motions buffers
Assumptions : LLD memory not yet allocated
Limitations :
Returns     : true if memory successfully allocated, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::AllocateLldMemory(
                                const hqvdp_lld_mem_need_s&     lldMemNeeds,
                                DMA_Area*                       pLldCmdBufferArea,
                                DMA_Area*                       pMotionBuffersArea,
                                uint32_t                        motionBuffersPhysAddr[HQVDPLITE_DEI_MOTION_BUFFERS_NB]) const
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    // Allocate DMA area for LLD/FW internal communication
    // SDAAF_UNCACHED flag allows to bypass the CPU cache and thus write
    // directly to physical memory. This way LLD does not need to flush
    // the cache once commands for FW are ready.
    vibe_os_allocate_dma_area( pLldCmdBufferArea,
                               lldMemNeeds.cmd_size,
                               lldMemNeeds.cmd_alignment,
                               SDAAF_UNCACHED);
    if(!pLldCmdBufferArea->pMemory)
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to allocate LLD internal commands buffer");
        PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
        return false;
    }
    vibe_os_zero_memory(pLldCmdBufferArea->pData, pLldCmdBufferArea->ulDataSize);

    // Motion buffers allocation
    //  * As far as possible, in SDP context, regions having the same permission should be allocated in one shot.
    //    => We allocate one area and we split it into HQVDPLITE_DEI_MOTION_BUFFERS_NB motion buffers
    //  * Each motion buffer must meet the HQVDP alignment constraint requested by the LLD
    //  * Secure regions must be HQVDPLITE_SDP_ALIGNMENT bytes aligned
    //    => The allocated region must meet both HQVDP and SDP alignment constraint
    //  * Size of secure regions must be HQVDPLITE_SDP_ALIGNMENT bytes aligned
    const uint32_t motionBufferAlignedSize      = _ALIGN_UP(lldMemNeeds.motion_buffer_size, lldMemNeeds.motion_buffer_alignment);
    const uint32_t motionBuffersAreaAlignment   = MAX(lldMemNeeds.motion_buffer_alignment, HQVDPLITE_SDP_ALIGNMENT);
    const uint32_t motionBuffersAreaAlignedSize = _ALIGN_UP(motionBufferAlignedSize * HQVDPLITE_DEI_MOTION_BUFFERS_NB, HQVDPLITE_SDP_ALIGNMENT);

    // Allocate a secure motion buffers area
    vibe_os_allocate_dma_area(pMotionBuffersArea,
                              motionBuffersAreaAlignedSize,
                              motionBuffersAreaAlignment,
                              SDAAF_SECURE);
    if(!pMotionBuffersArea->pMemory)
    {
        PLANE_TRC( TRC_ID_ERROR, "Could not allocate DMA memory for Motion Buffer");
        return false;
    }

    // Split the motion buffers area into HQVDPLITE_DEI_MOTION_BUFFERS_NB motion buffers.
    for(unsigned int i=0; i<HQVDPLITE_DEI_MOTION_BUFFERS_NB; i++)
    {
        motionBuffersPhysAddr[i] = pMotionBuffersArea->ulPhysical + (i * motionBufferAlignedSize);
        PLANE_TRC(TRC_ID_HQVDPLITE, "motionBuffersPhysAddr[%d] = 0x%08X, size = %d, aligned size = %u",
                                    i, motionBuffersPhysAddr[i], lldMemNeeds.motion_buffer_size, motionBufferAlignedSize);
    }

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
    return true;
}

/*******************************************************************************
Name        : FreeLldMemory
Description : Release the memory allocated for LLD internal needs
Parameters  : pLldCmdBufferArea  - [in/out] LLD/FW commands buffer area to be freed
              pMotionBuffersArea - [in/out] LLD motion buffers area to be freed
Assumptions : LLD memory has been already allocated
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::FreeLldMemory(
                            DMA_Area*   pLldCmdBufferArea,
                            DMA_Area*   pMotionBuffersArea) const
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    // Release LLD cmd buffer
    if(pLldCmdBufferArea->pData != 0)
    {
        vibe_os_free_dma_area(pLldCmdBufferArea);
    }

    // Release motion buffers
    if(pMotionBuffersArea->pData)
    {
        vibe_os_free_dma_area (pMotionBuffersArea);
    }

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
}

/*******************************************************************************
Name        : OpenLldSession
Description : Opens a HQVDP LLD session and links the associated video plugs
Parameters  : lldMemDesc - [in] Memory description allocated for LLD
Assumptions : LLD memory has been allocated
Limitations :
Returns     : true if session successfully opened, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::OpenLldSession(const hqvdp_lld_mem_desc_s& lldMemDesc)
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    hqvdp_lld_error         err = HQVDP_LLD_NO_ERR;
    unsigned int            plugsCount = 0;
    uint32_t                hwUsedBitMask = 0;

    // The Proc Clock should be enabled so LLD read/write ops becomes working
    EnableProcClock();

    // Make sure that Proc Clock is really enabled before any call to a LLD function
    CheckProcClockActivation();

    PLANE_TRC(TRC_ID_HQVDPLITE, "Opening HQVDP LLD session with profile %s",
                                m_lldProfile == HQVDP_PROFILE_FULL_HD  ? "FULL HD"  :
                                m_lldProfile == HQVDP_PROFILE_4KP30    ? "4KP30"    :
                                m_lldProfile == HQVDP_PROFILE_4KP60    ? "4KP60"    :
                                m_lldProfile == HQVDP_PROFILE_4KP60_PR ? "4KP60 PR" :
                                "unrecognized");

    // Open LLD session
    err = hqvdp_lld_open( m_lldProfile,         // [in]
#ifdef CONFIG_STM_VIRTUAL_PLATFORM
                          HQVDP_SOFT_VSYNC,     // [in]
#else
                          HQVDP_HW_VSYNC,       // [in]
#endif
                          &lldMemDesc,          // [in]
                          NULL,                 // [in]
                          &hwUsedBitMask,       // [out]
                          &m_lldHandle);        // [out]

    if(err != HQVDP_LLD_NO_ERR)
    {
        PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_open() failed, err=%d", err);
        goto failure;
    }

    // Parse HW info and find the video plugs that the LLD intents to use for this plane.
    // We can have up to HQVDP_LLD_MAX_HW_FOR_ONE_PLANE plugs for one plane.
    // In case hwUsedBitMask has (erroneously) more than HQVDP_LLD_MAX_HW_FOR_ONE_PLANE bits
    // enabled, then the extra-bits will not be taken into account.
    for(int hwInfoIdx=0; (hwInfoIdx < m_hqvdpHwNb) && (plugsCount < HQVDP_LLD_MAX_HW_FOR_ONE_PLANE); hwInfoIdx++)
    {
        if(m_pHqvdpHwInfo[hwInfoIdx].mixerBit & hwUsedBitMask)
        {
            /* This video plug will be used by the LLD */
            m_videoPlug[plugsCount] = m_pHqvdpHwInfo[hwInfoIdx].pVideoPlug;
            m_videoPlug[plugsCount]->SetPlaneName(GetName());
            PLANE_TRC(TRC_ID_HQVDPLITE, "Video Plug %u: mixer bit 0x%X", plugsCount, m_pHqvdpHwInfo[hwInfoIdx].mixerBit);
            plugsCount++;
        }
    }
    PLANE_TRC(TRC_ID_HQVDPLITE, "%d video plugs attached to this plane", plugsCount);

    // Check if video plugs have been found
    if(plugsCount == 0)
    {
        // This should never happen: either HW info table is not
        // correctly filled or LLD returned a wrong bit mask
        PLANE_TRC(TRC_ID_ERROR, "Could not find any matching video plug");
        hqvdp_lld_close(m_lldHandle);
        m_lldHandle = 0;
        goto failure;
    }

    // Done
    DisableProcClock();
    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
    return true;

failure:
    DisableProcClock();
    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
    return false;
}

/*******************************************************************************
Name        : CloseLldSession
Description : Close the HQVDP LLD session and unlinks the associated video plugs
Parameters  : None
Assumptions : LLD session is open and HQVDP HW is stopped
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::CloseLldSession()
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    if(m_lldHandle != 0)
    {
        // NB hqvdp_lld_close() does not send any command to the HW, so having clocks
        // enabled here is not required => no need to call CheckProcClockActivation()
        // By the way, when the plane closes LLD session, clocks have likely been
        // previously disabled.

        // Do close LLD session
        const hqvdp_lld_error err = hqvdp_lld_close(m_lldHandle);

        if(err != HQVDP_LLD_NO_ERR)
        {
            PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_close() failed, err=%d", err);
            PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
            return;
        }

        PLANE_TRC(TRC_ID_MAIN_INFO, "HQVDP LLD session closed");
        m_lldHandle = 0;

        // Unlink the associated video plugs
        for(unsigned int i=0; i < N_ELEMENTS(m_videoPlug); i++)
        {
            m_videoPlug[i] = NULL;
        }
    }

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
}


/*******************************************************************************
Name        : SetCompoundControl
Description :
Parameters  : ctrl    - [in] plane control
              pNewVal - [in] new value te set
Assumptions : pNewVal not null
Limitations :
Returns     : DisplayPlaneResults
*******************************************************************************/
DisplayPlaneResults CHqvdpLitePlaneV2::SetCompoundControl(
                                stm_display_plane_control_t ctrl,
                                void*                       pNewVal)
{
    PLANE_TRCIN( TRC_ID_HQVDPLITE, "");

    DisplayPlaneResults res = STM_PLANE_NOT_SUPPORTED;

    res = CDisplayPlane::SetCompoundControl(ctrl, pNewVal);

    if(res == STM_PLANE_OK)
    {
        switch(ctrl)
        {
            case PLANE_CTRL_INPUT_WINDOW_VALUE:
            {
                m_pDisplayDevice->VSyncLock();
                m_motionState = HQVDPLITE_DEI_MOTION_OFF;
                m_pDisplayDevice->VSyncUnlock();
                break;
            }
            default:
                break;
        }
    }

    PLANE_TRCOUT( TRC_ID_HQVDPLITE, "");
    return res;
}

/*******************************************************************************
Name        : GetCompoundControl
Description :
Parameters  : ctrl       - [in]  plane control selector
              currentVal - [out] control value
Assumptions : currentVal not null
Limitations :
Returns     : DisplayPlaneResults
*******************************************************************************/
DisplayPlaneResults CHqvdpLitePlaneV2::GetCompoundControl(stm_display_plane_control_t ctrl, void * currentVal)
{
    DisplayPlaneResults res = STM_PLANE_NOT_SUPPORTED;

    res = CDisplayPlane::GetCompoundControl(ctrl, currentVal);

    return res;
}

/*******************************************************************************
Name        : GetCompoundControlRange
Description :
Parameters  : selector - [in]  plane control selector
              pRange   - [out] control range values
Assumptions : pRange not null
Limitations :
Returns     : true
*******************************************************************************/
bool CHqvdpLitePlaneV2::GetCompoundControlRange(
                                    stm_display_plane_control_t     selector,
                                    stm_compound_control_range_t*   pRange)
{

    if((selector == PLANE_CTRL_INPUT_WINDOW_VALUE) || (selector == PLANE_CTRL_OUTPUT_WINDOW_VALUE))
    {
        pRange->min_val.x           = 0;
        pRange->min_val.y           = 0;
        pRange->min_val.width       = 48;
        pRange->min_val.height      = 16;

        pRange->max_val.x           = 0;
        pRange->max_val.y           = 0;
        pRange->max_val.width       = 4096;
        pRange->max_val.height      = 2160;

        pRange->default_val.x       = 0;
        pRange->default_val.y       = 0;
        pRange->default_val.width   = 0;
        pRange->default_val.height  = 0;

        pRange->step.x              = 1;
        pRange->step.y              = 1;
        pRange->step.width          = 1;
        pRange->step.height         = 1;
        return true;
    }
    else
    {
        return CDisplayPlane::GetCompoundControlRange( selector, pRange );
    }
}

/*******************************************************************************
Name        : SetControl
Description : Set plane control value
Parameters  : control - [in] control to be set
              value   - [in] value to be set
Assumptions :
Limitations :
Returns     : DisplayPlaneResults
*******************************************************************************/
DisplayPlaneResults CHqvdpLitePlaneV2::SetControl(
                                            stm_display_plane_control_t  control,
                                            uint32_t                     value)
{
    DisplayPlaneResults res = STM_PLANE_NOT_SUPPORTED;

    PLANE_TRCIN(TRC_ID_HQVDPLITE, "");

    switch(control)
    {
        case PLANE_CTRL_BRIGHTNESS:
        case PLANE_CTRL_SATURATION:
        case PLANE_CTRL_CONTRAST:
        case PLANE_CTRL_TINT:
        case PLANE_CTRL_COLOR_FILL_STATE:
        case PLANE_CTRL_COLOR_FILL_VALUE:
        case PLANE_CTRL_COLOR_FILL_MODE:
        {
            m_pDisplayDevice->VSyncLock();
            /* Apply settings to all video plugs attached to the plane */
            for(unsigned int i=0; i < N_ELEMENTS(m_videoPlug); i++)
            {
                if(m_videoPlug[i] != NULL)
                {
                    res = m_videoPlug[i]->SetControl(control, value) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set video plug control %d", control);
                    }
                }
            }
            m_pDisplayDevice->VSyncUnlock();
            break;
        }
        case PLANE_CTRL_INPUT_WINDOW_MODE:
        {
            res = CDisplayPlane::SetControl(control, value);
            if(res == STM_PLANE_OK)
            {
                m_pDisplayDevice->VSyncLock();
                m_motionState = HQVDPLITE_DEI_MOTION_OFF;
                m_pDisplayDevice->VSyncUnlock();
            }
            break;
        }
        case PLANE_CTRL_HIDE_MODE_POLICY:
        {
            switch(static_cast<stm_plane_ctrl_hide_mode_policy_t>(value))
            {
                case PLANE_HIDE_BY_MASKING:
                case PLANE_HIDE_BY_DISABLING:
                {
                    m_pDisplayDevice->VSyncLock();
                    m_eHideMode = static_cast<stm_plane_ctrl_hide_mode_policy_t>(value);
                    m_pDisplayDevice->VSyncUnlock();
                    res = STM_PLANE_OK;
                    break;
                }
                default:
                {
                    PLANE_TRC( TRC_ID_ERROR, "Invalid hiding policy %u", value);
                    res = STM_PLANE_INVALID_VALUE;
                    break;
                }
            }
            break;
        }
        case PLANE_CTRL_VIDEO_MADI_STATE:
        {
            switch(static_cast<stm_plane_control_state_t>(value))
            {
                case CONTROL_ON:
                {
                    res = STM_PLANE_OK;
                    break;
                }
                case CONTROL_OFF:
                case CONTROL_ONLY_LEFT_ON:
                case CONTROL_ONLY_RIGHT_ON:
                {
                    /* DEI should always be on so off setting is not supported */
                    PLANE_TRC( TRC_ID_ERROR, "DEI must not be disabled");
                    res = STM_PLANE_NOT_SUPPORTED;
                    break;
                }
                default:
                {
                    PLANE_TRC( TRC_ID_ERROR, "Invalid DEI control %u", value);
                    res = STM_PLANE_INVALID_VALUE;
                    break;
                }
            }
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_PEAKING_STATE:
        {
            switch(static_cast<stm_plane_control_state_t>(value))
            {
                case CONTROL_ON:
                {
                    PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking control ON, setting peaking preset to DEFAULT");
                    res = m_iqiPeaking.SetPreset(PCIQIC_ST_DEFAULT) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set Peaking preset");
                    }
                    break;
                }
                case CONTROL_OFF:
                {
                    PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking control OFF, setting peaking preset to BYPASS");
                    res = m_iqiPeaking.SetPreset(PCIQIC_BYPASS) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set Peaking preset");
                    }
                    break;
                }
                case CONTROL_SMOOTH:
                {
                    PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking control SMOOTH, setting peaking preset to SOFT");
                    res = m_iqiPeaking.SetPreset(PCIQIC_ST_SOFT) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set Peaking preset");
                    }
                    break;
                }
                case CONTROL_MEDIUM:
                {
                    PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking control MEDIUM, setting peaking preset to MEDIUM");
                    res = m_iqiPeaking.SetPreset(PCIQIC_ST_MEDIUM) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set Peaking preset");
                    }
                    break;
                }
                case CONTROL_SHARP:
                {
                    PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking control SHARP, setting peaking preset to STRONG");
                    res = m_iqiPeaking.SetPreset(PCIQIC_ST_STRONG) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set Peaking preset");
                    }
                    break;
                }
                default:
                {
                    PLANE_TRC( TRC_ID_ERROR, "Unsupported Peaking preset %u", value);
                    res = STM_PLANE_NOT_SUPPORTED;
                    break;
                }
            }
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_CTI_STATE:
        {
            switch (static_cast<stm_plane_control_state_t>(value))
            {
                case CONTROL_OFF:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "CTI control OFF, setting CTI preset to BYPASS");
                    res = m_iqiCti.SetPreset(PCIQIC_BYPASS) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set CTI preset");
                    }
                    break;
                }
                case CONTROL_ON:
                case CONTROL_SMOOTH:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "CTI control ON/SMOOTH, setting CTI preset to SOFT");
                    res = m_iqiCti.SetPreset(PCIQIC_ST_SOFT) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set CTI preset");
                    }
                    break;
                }
                case CONTROL_MEDIUM:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "CTI control MEDIUM, setting CTI preset to MEDIUM");
                    res = m_iqiCti.SetPreset(PCIQIC_ST_MEDIUM) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set CTI preset");
                    }
                    break;
                }
                case CONTROL_SHARP:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "CTI control SHARP, setting CTI preset to STRONG");
                    res = m_iqiCti.SetPreset(PCIQIC_ST_STRONG) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set CTI preset");
                    }
                    break;
                }
                default:
                {
                    PLANE_TRC( TRC_ID_ERROR, "Unsupported CTI preset %u", value);
                    res = STM_PLANE_NOT_SUPPORTED;
                    break;
                }
            }
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_LE_STATE:
        {
            switch (static_cast<stm_plane_control_state_t>(value))
            {
                case CONTROL_ON:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "LE control ON, setting LE preset to DEFAULT");
                    res = m_iqiLe.SetPreset(PCIQIC_ST_DEFAULT) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set LE preset");
                    }
                    break;
                }
                case CONTROL_OFF:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "LE control OFF, setting LE preset to BYPASS");
                    res = m_iqiLe.SetPreset(PCIQIC_BYPASS) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set LE preset");
                    }
                    break;
                }
                case CONTROL_SMOOTH:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "LE control SMOOTH, setting LE preset to SOFT");
                    res = m_iqiLe.SetPreset(PCIQIC_ST_SOFT) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set LE preset");
                    }
                    break;
                }
                case CONTROL_MEDIUM:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "LE control MEDIUM, setting LE preset to MEDIUM");
                    res = m_iqiLe.SetPreset(PCIQIC_ST_MEDIUM) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set LE preset");
                    }
                    break;
                }
                case CONTROL_SHARP:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "LE control SHARP, setting LE preset to STRONG");
                    res = m_iqiLe.SetPreset(PCIQIC_ST_STRONG) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
                    if(res != STM_PLANE_OK)
                    {
                        PLANE_TRC( TRC_ID_ERROR, "Failed to set LE preset");
                    }
                    break;
                }
                default:
                {
                    PLANE_TRC( TRC_ID_ERROR, "Unsupported LE preset %u", value);
                    res = STM_PLANE_NOT_SUPPORTED;
                    break;
                }
            }
            break;
        }
        default:
        {
            res = CDisplayPlane::SetControl(control, value);
            break;
        }
    }

    PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
    return res;
}


/*******************************************************************************
Name        : GetControl
Description : Get control
Parameters  : control - [in]  control to get
              pValue  - [out] corresponding control value
Assumptions : pValue not null
Limitations :
Returns     : DisplayPlaneResults
*******************************************************************************/
DisplayPlaneResults CHqvdpLitePlaneV2::GetControl(
                                            stm_display_plane_control_t control,
                                            uint32_t*                   pValue) const
{
    DisplayPlaneResults res = STM_PLANE_NOT_SUPPORTED;

    PLANE_TRCIN(TRC_ID_HQVDPLITE, "");

    switch(control)
    {
        case PLANE_CTRL_BRIGHTNESS:
        case PLANE_CTRL_SATURATION:
        case PLANE_CTRL_CONTRAST:
        case PLANE_CTRL_TINT:
        case PLANE_CTRL_COLOR_FILL_STATE:
        case PLANE_CTRL_COLOR_FILL_VALUE:
        case PLANE_CTRL_COLOR_FILL_MODE:
        {
            /* All video plugs have same settings, so we can query whatever instance */
            res = m_videoPlug[0]->GetControl(control, pValue) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
            if(res != STM_PLANE_OK)
            {
                PLANE_TRC( TRC_ID_ERROR, "Failed to get video plug control %d", control);
            }
            break;
        }
        case PLANE_CTRL_HIDE_MODE_POLICY:
        {
            *pValue = (uint32_t)m_eHideMode;
            res = STM_PLANE_OK;
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_PEAKING_STATE:
        {
            *pValue = static_cast<uint32_t>(m_iqiPeaking.GetPreset());
            PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "Peaking preset = %s", CHqvdpLiteIqi::PresetStr(static_cast<stm_plane_ctrl_iqi_configuration_e>(*pValue)));
            res = STM_PLANE_OK;
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_CTI_STATE:
        {
            *pValue = static_cast<uint32_t>(m_iqiCti.GetPreset());
            PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "CTI preset = %s", CHqvdpLiteIqi::PresetStr(static_cast<stm_plane_ctrl_iqi_configuration_e>(*pValue)));
            res = STM_PLANE_OK;
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_LE_STATE:
        {
            *pValue = static_cast<uint32_t>(m_iqiLe.GetPreset());
            PLANE_TRC( TRC_ID_HQVDPLITE_IQI, "LE preset = %s", CHqvdpLiteIqi::PresetStr(static_cast<stm_plane_ctrl_iqi_configuration_e>(*pValue)));
            res = STM_PLANE_OK;
            break;
        }
        default:
        {
            res = CDisplayPlane::GetControl(control, pValue);
            break;
        }
    }

    PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
    return res;
}

/*******************************************************************************
Name        : GetControlRange
Description : Get control ranges
Parameters  : control - [in] control to get
              pRange  - [in] range for this control
Assumptions : pRange not null
Limitations :
Returns     : true if control is supported, false else.
*******************************************************************************/
bool CHqvdpLitePlaneV2::GetControlRange(
                                    stm_display_plane_control_t           control,
                                    stm_display_plane_control_range_t*    pRange)
{
    bool bIsOk = false;

    switch(control)
    {
        case PLANE_CTRL_BRIGHTNESS:
        case PLANE_CTRL_SATURATION:
        case PLANE_CTRL_CONTRAST:
        case PLANE_CTRL_TINT:
        {
            /* All video plugs have same settings, so we can query whatever instance */
            bIsOk = m_videoPlug[0]->GetControlRange(control, pRange);
            break;
        }
        default:
        {
            bIsOk = CDisplayPlane::GetControlRange(control, pRange);
            break;
        }
    }

    return bIsOk;
}

/*******************************************************************************
Name        : SetTuningDataControl
Description : Set tuning data control value
Parameters  : ctrl        - [in] tuning control
              pTuningData - [in] new tuning value to be applied
Assumptions : pTuningData not null
Limitations :
Returns     : DisplayPlaneResults
*******************************************************************************/
DisplayPlaneResults CHqvdpLitePlaneV2::SetTuningDataControl(
                                                stm_display_plane_tuning_data_control_t ctrl,
                                                stm_tuning_data_t*                      pTuningData)
{
    PLANE_TRCIN(TRC_ID_HQVDPLITE,"");

    DisplayPlaneResults res = STM_PLANE_NOT_SUPPORTED;

    switch(ctrl)
    {
        case PLANE_CTRL_VIDEO_IQI_PEAKING_TUNING_DATA:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "Setting Peaking conf");
            const stm_iqi_peaking_conf_t* pPeakingConf = &((const stm_iqi_peaking_tuning_data_t*)GET_PAYLOAD_POINTER(pTuningData))->conf;
            res = m_iqiPeaking.SetConf(pPeakingConf) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
            if(res != STM_PLANE_OK)
            {
                PLANE_TRC(TRC_ID_ERROR,"Failed to set Peaking conf");
            }
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_LE_TUNING_DATA:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "Setting LE conf");
            const stm_iqi_le_conf_t* pLeConf = &((const stm_iqi_le_tuning_data_t*)GET_PAYLOAD_POINTER(pTuningData))->conf;
            res = m_iqiLe.SetConf(pLeConf) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
            if(res != STM_PLANE_OK)
            {
                PLANE_TRC(TRC_ID_ERROR,"Failed to set LE conf");
            }
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_CTI_TUNING_DATA:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_IQI, "Setting CTI conf");
            const stm_iqi_cti_conf_t* pCtiConf = &((const stm_iqi_cti_tuning_data_t*)GET_PAYLOAD_POINTER(pTuningData))->conf;
            res =  m_iqiCti.SetConf(pCtiConf) ? STM_PLANE_OK : STM_PLANE_INVALID_VALUE;
            if(res != STM_PLANE_OK)
            {
                PLANE_TRC(TRC_ID_ERROR,"Failed to set CTI conf");
            }
            break;
        }
        default:
        {
            res = STM_PLANE_NOT_SUPPORTED;
            break;
        }
    }

    PLANE_TRCOUT(TRC_ID_HQVDPLITE,"");
    return res;
}


/*******************************************************************************
Name        : GetTuningDataControl
Description : Get tuning data control value
Parameters  : ctrl        - [in]  tuning control
              pTuningData - [out] current tuning value
Assumptions : pTuningData not null
Limitations :
Returns     : DisplayPlaneResults
*******************************************************************************/
DisplayPlaneResults CHqvdpLitePlaneV2::GetTuningDataControl(
                                                stm_display_plane_tuning_data_control_t ctrl,
                                                stm_tuning_data_t*                      pTuningData)
{
    PLANE_TRCIN(TRC_ID_HQVDPLITE,"");

    DisplayPlaneResults res = STM_PLANE_NOT_SUPPORTED;

    switch(ctrl)
    {
        case PLANE_CTRL_VIDEO_IQI_PEAKING_TUNING_DATA:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_IQI,"Getting Peaking conf");
            stm_iqi_peaking_conf_t* pPeakingConf = &((stm_iqi_peaking_tuning_data_t*)GET_PAYLOAD_POINTER(pTuningData))->conf;
            m_iqiPeaking.GetConf(pPeakingConf);
            res = STM_PLANE_OK;
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_LE_TUNING_DATA:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_IQI,"Getting LE conf");
            stm_iqi_le_conf_t* pLeConf = &((stm_iqi_le_tuning_data_t*)GET_PAYLOAD_POINTER(pTuningData))->conf;
            m_iqiLe.GetConf(pLeConf);
            res = STM_PLANE_OK;
            break;
        }
        case PLANE_CTRL_VIDEO_IQI_CTI_TUNING_DATA:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_IQI,"Getting CTI conf");
            stm_iqi_cti_conf_t* pCtiConf = &((stm_iqi_cti_tuning_data_t*)GET_PAYLOAD_POINTER(pTuningData))->conf;
            m_iqiCti.GetConf(pCtiConf);
            res = STM_PLANE_OK;
            break;
        }
        default:
        {
            res = STM_PLANE_NOT_SUPPORTED;
            break;
        }
    }

    PLANE_TRCOUT(TRC_ID_HQVDPLITE,"");
    return res;
}

/*******************************************************************************
Name        : GetTuningDataRevision
Description : Get tuning data revision
Parameters  : ctrl      - [in]  tuning control to get revision
              pRevision - [out] revision
Assumptions : pRevision not null
Limitations :
Returns     : true if tuning control is supported
*******************************************************************************/
bool CHqvdpLitePlaneV2::GetTuningDataRevision(
                                    stm_display_plane_tuning_data_control_t ctrl,
                                    uint32_t*                               pRevision)
{
    bool bIsSupported = false;

    if(!pRevision)
        return false;

    switch(ctrl)
    {
        case PLANE_CTRL_VIDEO_FMD_TUNING_DATA:
        case PLANE_CTRL_VIDEO_MADI_TUNING_DATA:
        case PLANE_CTRL_VIDEO_IQI_PEAKING_TUNING_DATA:
        case PLANE_CTRL_VIDEO_IQI_LE_TUNING_DATA:
        case PLANE_CTRL_VIDEO_IQI_CTI_TUNING_DATA:
        {
            *pRevision = 1;
            bIsSupported = true;
            break;
        }
        default:
        {
            *pRevision = 0;
            bIsSupported = false;
            break;
        }
    }
    return bIsSupported;
}


/*******************************************************************************
Name        : IsFeatureApplicable
Description : Tells if plane feature is applicable
Parameters  : feature     - [in]  plane feature
              pApplicable - [out] "is applicable" result
Assumptions : pApplicable is not null
Limitations :
Returns     : true if supported, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsFeatureApplicable(
                                    stm_plane_feature_t feature,
                                    bool*               pApplicable) const
{
    bool bIsSupported = false;

    switch(feature)
    {
        case PLANE_FEAT_VIDEO_MADI:
        {
            bIsSupported = true;
            if(pApplicable)
            {
                *pApplicable = true;
            }
            break;
        }
        case PLANE_FEAT_VIDEO_FMD:
        {
            bIsSupported = m_bUseFMD;
            if(pApplicable)
            {
                *pApplicable = bIsSupported;
            }
            break;
        }
        default:
        {
            bIsSupported =  CDisplayPlane::IsFeatureApplicable(feature, pApplicable);
            break;
        }
    }
    return bIsSupported;
}


/*******************************************************************************
Name        : PresentDisplayNode
Description : Present the new node to the display
Parameters  : pPrevNode           - [in] Previous displayed node
              pCurrNode           - [in] Current Node to display
              pNextNode           - [in] Pending Node to display
              isPictureRepeated   - [in] True if picture is repeated
              isDisplayInterlaced - [in] True if display is interlaced
              isTopFieldOnDisplay - [in] True if top field is currently on the display
              vsyncTime           - [in] Current VSync time
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::PresentDisplayNode(
                                CDisplayNode*       pPrevNode,
                                CDisplayNode*       pCurrNode,
                                CDisplayNode*       pNextNode,
                                bool                isPictureRepeated,
                                bool                isDisplayInterlaced,
                                bool                isTopFieldOnDisplay,
                                const stm_time64_t& vSyncTime)
{
    stm_display_use_cases_t useCase;
    bool                    procClkTemporaryEnabled = false;

    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
    // On VSOC, VSYNC interrupt is not connected to HQVDP model
    // => we need to simulate the VSYNC by software
    hqvdp_lld_gen_soft_vsync(m_lldHandle);
#endif

    // Check that all the conditions are met to allow a display on this plane
    if(!isDisplayPossible(pCurrNode))
    {
        PLANE_TRC(TRC_ID_MAIN_INFO, "Display conditions not met, so displaying nothing");
        return;
    }

    // Check node validity (Previous and Next nodes are optional so if they are null, this is not an error)
    if( (pPrevNode && !pPrevNode->IsNodeValid() ) ||
        (pCurrNode && !pCurrNode->IsNodeValid() ) ||
        (pNextNode && !pNextNode->IsNodeValid() ) )
    {
        PLANE_TRC(TRC_ID_ERROR, "Invalid node! P:0x%p C:0x%p N:0x%p",
            pPrevNode,
            pCurrNode,
            pNextNode);
        return;
    }

    PLANE_TRC(TRC_ID_HQVDPLITE, "isPictureRepeated : %u, isDisplayInterlaced : %u, isTopFieldOnDisplay : %u, vSyncTime : %lld", isPictureRepeated, isDisplayInterlaced, isTopFieldOnDisplay, vSyncTime);
    PLANE_TRC(TRC_ID_PICT_QUEUE_RELEASE, "%d%c (%p)",  pCurrNode->m_pictureId, pCurrNode->m_srcPictureTypeChar, pCurrNode);

    HandlePause(&pPrevNode, &pCurrNode, &pNextNode);

    // For HQVDP, the Proc Clock should be enabled BEFORE sending a command to the HW
    if(!m_isProcClkEnabled)
    {
        EnableProcClock();
        procClkTemporaryEnabled = true;
    }

    // If previous frame could not be processed in the VSYNC time frame then skip the current frame
    if(m_bEndOfProcessingError)
    {
        PLANE_TRC(TRC_ID_ERROR, "'End Of Processing' error detected, skipping frame for error recovery");
        RecoverFromEndOfProcessingError(pCurrNode);
        m_bEndOfProcessingError = false;
        goto error;
    }

    /* Get the current use case (this cannot fail) */
    useCase = GetCurrentUseCase(pCurrNode->m_srcPictureType, isDisplayInterlaced, isTopFieldOnDisplay);

    /*
     * Check whether the context is changed
     * If yes, reset and recompute everything
     */
    if(IsContextChanged(pCurrNode, isPictureRepeated))
    {
        PLANE_TRC(TRC_ID_HQVDPLITE,"### Display context has changed");

        /* Reset all use cases configurations */
        ResetEveryUseCases();

        /* Recompute the Display Info */
        if(!FillHqvdpLiteDisplayInfo(pCurrNode, &m_hqvdpDisplayInfo))
        {
            PLANE_TRC(TRC_ID_ERROR, "Failed to fill HQVDP display info!");
            goto error;
        }
    }

    /* Exit if IO Windows are not valid */
    if(!m_hqvdpDisplayInfo.m_areIOWindowsValid)
    {
        PLANE_TRC(TRC_ID_ERROR, "IO windows are not valid");
        goto error;
    }

    /* Check whether a valid conf is available for this use case. If not, compute it */
    if(!m_hqvdpConf[useCase].isValid)
    {
        PLANE_TRC(TRC_ID_HQVDPLITE, "No valid configuration available for this use case: recompute the configuration");

        /* Prepare the "static" part of this conf           */
        /* i.e. parameters that don't change at every frame */
        if(!PrepareConf(pCurrNode,
                        &m_hqvdpDisplayInfo,
                        &m_hqvdpConf[useCase].conf))
        {
            PLANE_TRC(TRC_ID_ERROR, "Failed to prepare HQVDP configuration!");
            goto error;
        }
        m_hqvdpConf[useCase].isValid = true;
    }

    /* Update the "dynamic" part of this conf         */
    /* i.e. parameters that may change at every frame */
    if(!UpdateDynamicConf(pPrevNode,
                          pCurrNode,
                          pNextNode,
                          &m_hqvdpDisplayInfo,
                          isPictureRepeated,
                          isTopFieldOnDisplay,
                          isDisplayInterlaced,
                          &m_hqvdpConf[useCase].conf))
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to update HQVDP dynamic configuration");
        goto error;
    }

    /* Apply the HQVDP configuration */
    ApplyConf(pCurrNode, &m_hqvdpDisplayInfo, &m_hqvdpConf[useCase].conf);

    UpdatePictureDisplayedStatistics(pCurrNode, isPictureRepeated, isDisplayInterlaced, isTopFieldOnDisplay);
    UpdateColorimetryConversionStatistics(pCurrNode);

    // Picture presentation was successful. Save the reference of the pictures presented for next VSync
    m_picturesPreparedForNextVSync.pCurNode  = pCurrNode;
    m_picturesPreparedForNextVSync.pPrevNode = pPrevNode;
    m_picturesPreparedForNextVSync.pNextNode = pNextNode;

    /* Done */
    return;

error:
    if(procClkTemporaryEnabled)
    {
        DisableProcClock();
    }
}


/*******************************************************************************
Name        : Freeze
Description : Freezes plane processing
              This function is called when entering Low Power mode.
Parameters  : None
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::Freeze(void)
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    DisableHW();
    // HQVDP power is going to be turned off, so HQVDP firmware is implicitly going to be shutdown.
    // => close LLD session so that LLD is cleanly paused.
    CloseLldSession();
    CDisplayPlane::Freeze();

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
}


/*******************************************************************************
Name        : Resume
Description : Resumes (restarts plane processing if it was frozen)
              This function is called when exiting Low Power mode.
Parameters  : None
Assumptions : Plane is suspended or frozen
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::Resume(void)
{
  PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

  if(m_bIsFrozen)
  {
      // Power is back
      // => reopen LLD session so that HQVDP firmware is reloaded
      OpenLldSession(m_lldMemDesc);
  }
  CDisplayPlane::Resume();

  PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
}


/*******************************************************************************
Name        : IsColorFormatSupported
Description : Check if image color format is supported
Parameters  : colorFormat   - [in] color format to be checked
              bIsInterlaced - [in] true if picture is interlaced, false else
Assumptions :
Limitations :
Returns     : true if format is supported, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsColorFormatSupported(
                                    stm_pixel_format_t  colorFormat,
                                    bool                bIsInterlaced) const
{
    bool bIsSupported = false;

    switch(colorFormat)
    {
        case SURF_YCBCR420MB:
        case SURF_YCbCr420R2B:
        case SURF_YCbCr422R2B:
        case SURF_YCbCr420R2B10:
        case SURF_YCbCr422R2B10:
        {
            // Formats supported in both interlaced and progressive
            bIsSupported = true;
            break;
        }
        case SURF_CRYCB888:
        case SURF_ACRYCB8888:
        case SURF_CRYCB101010:
        {
            // Formats supported only in progressive
            bIsSupported = bIsInterlaced ? false : true;
            break;
        }
        default:
        {
            // Unsupported format
            bIsSupported = false;
            break;
        }
    }

    if(!bIsSupported)
    {
        PLANE_TRC(TRC_ID_HQVDPLITE, "Unsupported color format %d (%s source)", colorFormat, bIsInterlaced ? "interlaced" : "progressive");
    }

    return bIsSupported;
}


/*******************************************************************************
Name        : IsSecondaryPictureAvailable
Description : Tells if node conveys a secondary picture
Parameters  : pDisplayNode - [in] Display Node
Assumptions :
Limitations :
Returns     : true if picture is supported, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsSecondaryPictureAvailable(
                                const CDisplayNode* pDisplayNode) const
{
    bool bIsAvailable =    pDisplayNode->m_bufferDesc.src.horizontal_decimation_factor != STM_NO_DECIMATION
                        || pDisplayNode->m_bufferDesc.src.vertical_decimation_factor   != STM_NO_DECIMATION;

    return bIsAvailable;
}

/*******************************************************************************
Name        : CheckInputPicture
Description : Check if input picture is supported and updates the related display
              info flags.
Parameters  : pDisplayNode - [in]     Node to be checked
              pDisplayInfo - [in/out] HQVDP display info to be updated
Assumptions :
Limitations :
Returns     : true if picture is supported, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::CheckInputPicture(
                                    const CDisplayNode*         pDisplayNode,
                                    CHqvdpLiteDisplayInfoV2*    pDisplayInfo)
{
    const bool bIsPictureInterlaced = (pDisplayNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) != 0;

    pDisplayInfo->m_isPrimaryPictureSupported = IsColorFormatSupported(pDisplayNode->m_bufferDesc.src.primary_picture.color_fmt, bIsPictureInterlaced);

    if(!pDisplayInfo->m_isPrimaryPictureSupported)
    {
        PLANE_TRC(TRC_ID_HQVDPLITE, "Warning: primary picture not supported");
    }

    if(IsSecondaryPictureAvailable(pDisplayNode))
    {
        pDisplayInfo->m_isSecondaryPictureSupported = IsColorFormatSupported(pDisplayNode->m_bufferDesc.src.secondary_picture.color_fmt, bIsPictureInterlaced);

        if(!pDisplayInfo->m_isSecondaryPictureSupported)
        {
            PLANE_TRC(TRC_ID_HQVDPLITE, "Warning: secondary picture not supported");
        }
    }

    if(   !pDisplayInfo->m_isPrimaryPictureSupported
       && !pDisplayInfo->m_isSecondaryPictureSupported)
    {
        PLANE_TRC(TRC_ID_ERROR, "Neither primary nor secondary pictures are supported");
        return false;;
    }

    return true;
}

/*******************************************************************************
Name        : FillHqvdpLiteDisplayInfo
Description : Fills HQVDP Lite display info
Parameters  : pCurrNode     - [in]  Current node to display
              pDisplayInfo  - [out] HQVDP display info to be updated
Assumptions :
Limitations :
Returns     : true if updated successfully, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::FillHqvdpLiteDisplayInfo(
                                    CDisplayNode*               pCurrNode,
                                    CHqvdpLiteDisplayInfoV2*    pDisplayInfo)
{
    /* Reset the DisplayInfo before recomputing them */
    pDisplayInfo->Reset();

    // Check input picture
    if(!CheckInputPicture(pCurrNode, pDisplayInfo))
    {
        PLANE_TRC(TRC_ID_ERROR, "CheckInputPicture() failed !!");
        goto reset_and_exit;
    }

    // Fill the information about the output display
    if(!FillDisplayInfo(pCurrNode, pDisplayInfo))
    {
        PLANE_TRC(TRC_ID_ERROR, "FillDisplayInfo() failed !!");
        goto reset_and_exit;
    }

    // Compute IO window
    if(!PrepareIOWindows(pCurrNode, pDisplayInfo))
    {
        PLANE_TRC(TRC_ID_ERROR, "PrepareIOWindows() failed !!");
        goto reset_and_exit;
    }

    // Clear colorimetry stats
    ClearColorimetryConversionStatistics();

    /* Set display hw information */
    SetHwDisplayInfos(pDisplayInfo);

    if(!pDisplayInfo->m_areIOWindowsValid)
    {
        goto reset_and_exit;
    }

    return true;

reset_and_exit:
    pDisplayInfo->Reset();
    return false;
}

/*******************************************************************************
Name        : UpdateDynamicConf
Description : Update the HQVDP conf params that may change at every frame
Parameters  : pPrevNode             - [in]  Previous node displayed
              pCurrNode             - [in]  Current node to display
              pNextNode             - [in]  Next node to display
              pDisplayInfo          - [in]  Display Info
              isPictureRepeated     - [in]  True if picture is repeated
              isTopFieldOnDisplay   - [in]  True if top field is on display
              isDisplayInterlaced   - [in]  True if display is interlaced
              pHqvdpConf            - [out] HQVDP configuration to be updated
Assumptions :
Limitations :
Returns     : true if conf updated successfully, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::UpdateDynamicConf(
                                    const CDisplayNode*             pPrevNode,
                                    const CDisplayNode*             pCurrNode,
                                    const CDisplayNode*             pNextNode,
                                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                    bool                            isPictureRepeated,
                                    bool                            isTopFieldOnDisplay,
                                    bool                            isDisplayInterlaced,
                                    hqvdp_lld_conf_s*               pHqvdpConf)
{
    /* Set luma and chroma buffer address in Top parameters */
    if(!SetPictureBaseAddresses(pCurrNode,
                                pDisplayInfo,
                                &pHqvdpConf->Top.CurrentLuma,
                                &pHqvdpConf->Top.CurrentChroma))
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to set luma and chroma addresses!");
        return false;
    }

    /* Set CSDI params */
    if(!SetCsdiParams(pPrevNode, pCurrNode, pNextNode, pDisplayInfo, &pHqvdpConf->Top, isPictureRepeated, &pHqvdpConf->Csdi))
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to set the CSDI params");
        return false;
    }

    /* Set IQI parameters */
    if(!SetIqiParams(pCurrNode, pDisplayInfo, isTopFieldOnDisplay, isDisplayInterlaced, &pHqvdpConf->Iqi))
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to set the IQI params");
        return false;
    }

    /* Set Video Plug parameters */
    if(!SetVideoPlugParams(&pHqvdpConf->VideoPlug))
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to set the Video Plug params");
        return false;
    }

    return true;
}

/*******************************************************************************
Name        : PrepareConf
Description : Prepare the HQVDP configuration to be applied to LLD.
Parameters  : pCurrNode     - [in]  Current node info
              pDisplayInfo  - [in]  Current display info
              pHqvdpConf    - [out] HQVDP conf to be prepared
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::PrepareConf(
                            const CDisplayNode*             pCurrNode,
                            const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                            hqvdp_lld_conf_s*               pHqvdpConf) const
{
    if(PrepareTopParams(pCurrNode, pDisplayInfo, &pHqvdpConf->Top) == false)
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to prepare Top params");
        goto failed;
    }

    if(PrepareVc1Params(&pHqvdpConf->Vc1re) == false)
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to prepare VC1 params");
        goto failed;
    }

    if(PrepareFmdParams(&pHqvdpConf->Fmd) == false)
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to prepare FMD params");
        goto failed;
    }

    if(PrepareCsdiParams(&pHqvdpConf->Csdi) == false)
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to prepare CSDI params");
        goto failed;
    }

    if(PrepareHvsrcParams(pDisplayInfo, &pHqvdpConf->Hvsrc, &pHqvdpConf->LutHvsrc) == false)
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to prepare HVSRC params");
        goto failed;
    }

    if(PrepareOutParams(pDisplayInfo, &pHqvdpConf->Out) == false)
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to prepare OUT params");
        goto failed;
    }

    return true;

failed:
    return false;
}


/*******************************************************************************
Name        : SetHwDisplayInfos
Description : Set HW display info
Parameters  : pDisplayInfo - [out] HQVDP Lite display info to be updated
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::SetHwDisplayInfos(CHqvdpLiteDisplayInfoV2* pDisplayInfo) const
{
    if(m_bHasProgressive2InterlacedHW && m_outputInfo.isDisplayInterlaced)
    {
        PLANE_TRC(TRC_ID_HQVDPLITE, "Using P2I");
        pDisplayInfo->m_isUsingProgressive2InterlacedHW = true;
    }
}


/*******************************************************************************
Name        : AdjustIOWindowsForHWConstraints
Description : Adjust the IO windows to meet HW constraints
Parameters  : pCurrNode    - [in]  Current node to display
              pDisplayInfo - [out] HQVDP Lite display info to be updated
Assumptions :
Limitations :
Returns     : true if adjustment done successfully
*******************************************************************************/
bool CHqvdpLitePlaneV2::AdjustIOWindowsForHWConstraints(
                                    CDisplayNode*   pCurrNode,
                                    CDisplayInfo*   pDisplayInfo) const
{
    // HQVDP input width and height must be even
    // Low value are taken to prevent taking pixels that may be outside the picture
    pDisplayInfo->m_selectedPicture.srcFrameRect.height &= ~0x1;
    pDisplayInfo->m_selectedPicture.srcFrameRect.width  &= ~0x1;

    // HQVDP output width and height adjustment
    // Hereafter, the nearest high value (and not the nearest low value) are taken to avoid
    // increasing the scaling factor as it has already been checked supported by the HW.

    // HQVDP output height must be even
    pDisplayInfo->m_dstFrameRect.height = (pDisplayInfo->m_dstFrameRect.height + 1) & (~(0x1));

    if(m_bIsPixelRepeatAllowed)
    {
        // Pixel repeat is allowed on this plane, so the LLD can decide to enable the pixel repeat feature
        // that consist in horizontal downscale by 2 by the HQVDP followed by pixel repeat by video plug.
        // In that case, the HQVDP output width must be multiple of 4 so that the intermediate width
        // (due to downscale by 2) is even.
        pDisplayInfo->m_dstFrameRect.width = (pDisplayInfo->m_dstFrameRect.width + 3) & (~(0x3));
    }
    else
    {
        // HQVDP output width must be even
        pDisplayInfo->m_dstFrameRect.width  = (pDisplayInfo->m_dstFrameRect.width + 1) & (~(0x1));
    }

    // Move destination rectangle horizontally in case destination rectangle overlaps the visible area
    if ( (pDisplayInfo->m_dstFrameRect.x + pDisplayInfo->m_dstFrameRect.width) > m_outputInfo.displayVisibleArea.width )
    {
        pDisplayInfo->m_dstFrameRect.x = m_outputInfo.displayVisibleArea.width - pDisplayInfo->m_dstFrameRect.width;
    }

    // Move destination rectangle vertically in case destination rectangle overlaps the visible area
    if ( (pDisplayInfo->m_dstFrameRect.y + pDisplayInfo->m_dstFrameRect.height) > m_outputInfo.displayVisibleArea.height )
    {
        pDisplayInfo->m_dstFrameRect.y = m_outputInfo.displayVisibleArea.height - pDisplayInfo->m_dstFrameRect.height;
    }

    return true;
 }

/*******************************************************************************
Name        : IsScalingPossible
Description : Determines if scaling is possible based on HW resources
Parameters  : pCurrNode     - [in]     Current node
              pDisplayInfo  - [in/out] Display info
Assumptions : Primary or secondary (or both) picture is supported
Limitations :
Returns     : true if scaling is possible, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsScalingPossible(
                                CDisplayNode*   pCurrNode,
                                CDisplayInfo*   pDisplayInfo)
{
    CHqvdpLiteDisplayInfoV2* pHqvdpLiteDisplayInfo = static_cast<CHqvdpLiteDisplayInfoV2*>(pDisplayInfo);

    // If primary picture is supported, then check that scaling is achievable using HW only
    if(pHqvdpLiteDisplayInfo->m_isPrimaryPictureSupported)
    {
        PLANE_TRCBL(TRC_ID_MAIN_INFO);
        PLANE_TRC(TRC_ID_MAIN_INFO, "## Checking if scaling possible with Primary picture");

        pHqvdpLiteDisplayInfo->m_isSecondaryPictureSelected = false;
        FillSelectedPictureDisplayInfo(pCurrNode, pHqvdpLiteDisplayInfo);

        if(IsScalingPossibleByHw(pHqvdpLiteDisplayInfo))
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "## Scaling possible with Primary picture");
            return true;
        }
    }

    // If secondary picture is supported, then check that scaling is achievable using HW only
    if(   IsSecondaryPictureAvailable(pCurrNode)
       && pHqvdpLiteDisplayInfo->m_isSecondaryPictureSupported)
    {
        PLANE_TRCBL(TRC_ID_MAIN_INFO);
        PLANE_TRC(TRC_ID_MAIN_INFO, "## Checking if scaling possible with Secondary picture");

        pHqvdpLiteDisplayInfo->m_isSecondaryPictureSelected = true;
        FillSelectedPictureDisplayInfo(pCurrNode, pHqvdpLiteDisplayInfo);

        if(IsScalingPossibleByHw(pHqvdpLiteDisplayInfo))
        {
            PLANE_TRC(TRC_ID_MAIN_INFO, "## Scaling possible with Secondary picture");
            return true;
        }
    }

    // Scaling not achievable using HW only scaling feature
    // => Check if skipping lines from input picture allows HW to achieve scaling
    // NB We are sure that primary or secondary (or both) picture is supported (otherwise IsScalingPossible wouldn't have been called)
    // So above code has selected one of them and filled display info accordingly.
    if(!pHqvdpLiteDisplayInfo->m_isSrcInterlaced)
    {
        if(IsScalingPossibleBySkippingLines(pCurrNode, pHqvdpLiteDisplayInfo))
        {
            return true;
        }
    }

    PLANE_TRC(TRC_ID_ERROR, "!!! Scaling NOT possible !!!");
    return false;
}


/*******************************************************************************
Name        : IsScalingPossibleByHw
Description : Determines if scaling is possible by HW according to 3 criteria:
               1. Zoom constraints (including max zoom ratio)
               2. Processing time (line processing must be achievable in a HSync timeframe)
               3. STBus traffic (should not exceed a threshold)
Parameters  : pDisplayInfo - [in/out] Display info
Assumptions :
Limitations :
Returns     : True if scaling is possible, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsScalingPossibleByHw(
                                    CHqvdpLiteDisplayInfoV2*    pDisplayInfo)
{
    // With current code:
    // - The DEI is always ON with Interlaced inputs so the VHSRC is always receiving a Progressive picture (so input height  = src frame height)
    // - The P2I is always ON with Interlaced output so the VHSRC is always producing a Progressive picture (so output height = dst frame height)
    const uint32_t     srcRectWidth      = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
    const uint32_t     srcRectHeight     = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
    const uint32_t     dstRectWidth      = pDisplayInfo->m_dstFrameRect.width;
    const uint32_t     dstRectHeight     = pDisplayInfo->m_dstFrameRect.height;
    uint32_t           srcRectWidthOneHw = 0;
    uint32_t           dstRectWidthOneHw = 0;

    PLANE_TRC(TRC_ID_MAIN_INFO, "Trying: IsSrcInterlaced = %d, src width = %u, src height = %u, dst width = %u, dst height = %u",
                                pDisplayInfo->m_isSrcInterlaced, srcRectWidth, srcRectHeight, dstRectWidth, dstRectHeight);

    // Check that zoom is achievable
    if(!AreZoomConstraintsOk(srcRectWidth, srcRectHeight, dstRectWidth, dstRectHeight))
    {
        PLANE_TRC(TRC_ID_MAIN_INFO, "Zoom constraints NOK!");
        return false;
    }

    // Get IO widths processed by one hardware
    if(!GetIOWidthsOneHw(pDisplayInfo, srcRectWidth, dstRectWidth, &srcRectWidthOneHw, &dstRectWidthOneHw))
    {
        PLANE_TRC(TRC_ID_MAIN_INFO, "Failed to compute IO widths for one HW");
        return false;
    }

    PLANE_TRC(TRC_ID_HQVDPLITE, "One HW src width = %u, one HW dst width = %u", srcRectWidthOneHw, dstRectWidthOneHw);

    if(pDisplayInfo->m_isSrcInterlaced)
    {
        // Try with 3D DEI mode only if 3D DEI is allowed on this plane
        if(m_bIsTemporalDeiAllowed)
        {
            PLANE_TRC(TRC_ID_HQVDPLITE, "Trying if scaling possible with 3D DEI mode");

            if(   IsSTBusDataRateOk(pDisplayInfo, HQVDPLITE_DEI_MODE_3D)
               && IsHwProcessingTimeOk(srcRectWidthOneHw, srcRectHeight, dstRectWidthOneHw, dstRectHeight, HQVDPLITE_DEI_MODE_3D))
            {
                // All the conditions are OK. This scaling is possible and the maxDeiMode is HQVDPLITE_DEI_MODE_3D
                PLANE_TRC(TRC_ID_HQVDPLITE, "Scaling is possible with 3D DEI mode");
                pDisplayInfo->m_is3DDeinterlacingPossible = true;
                return true;
            }
            PLANE_TRC(TRC_ID_HQVDPLITE, "STBusDataRate or HwProcessingTime NOK: scaling not possible with 3D DEI mode");
        }

        PLANE_TRC(TRC_ID_HQVDPLITE, "Trying if scaling possible with DI DEI mode");

        // Try with DI DEI mode
        if(   IsSTBusDataRateOk(pDisplayInfo, HQVDPLITE_DEI_MODE_DI)
           && IsHwProcessingTimeOk(srcRectWidthOneHw, srcRectHeight, dstRectWidthOneHw, dstRectHeight, HQVDPLITE_DEI_MODE_DI))
        {
            // All the conditions are OK. This scaling is possible and the maxDeiMode is HQVDPLITE_DEI_MODE_DI
            PLANE_TRC(TRC_ID_HQVDPLITE, "Scaling is possible with DI DEI mode");
            pDisplayInfo->m_is3DDeinterlacingPossible = false;
            return true;
        }
        PLANE_TRC(TRC_ID_HQVDPLITE, "STBusDataRate or HwProcessingTime NOK: scaling not possible with DI DEI mode");
    }
    else
    {
        PLANE_TRC(TRC_ID_HQVDPLITE, "Trying if scaling possible with DEI OFF");

        if(   IsSTBusDataRateOk(pDisplayInfo, HQVDPLITE_DEI_MODE_OFF)
           && IsHwProcessingTimeOk(srcRectWidthOneHw, srcRectHeight, dstRectWidthOneHw, dstRectHeight, HQVDPLITE_DEI_MODE_OFF))
        {
            // All the conditions are OK. This scaling is possible and the maxDeiMode is HQVDPLITE_DEI_MODE_OFF
            PLANE_TRC(TRC_ID_HQVDPLITE, "Scaling is possible with DEI OFF");
            pDisplayInfo->m_is3DDeinterlacingPossible = false;
            return true;
        }
        PLANE_TRC(TRC_ID_HQVDPLITE, "STBusDataRate or HwProcessingTime NOK: scaling not possible");
    }

    return false;
}


/*******************************************************************************
Name        : MaxLinesSkippable
Description : Return the max number of lines that we can skip based on HW capability.
              This function can return 0 in case the HW does not allow to skip
              lines (this can happen if max pitch is 1 line).
Parameters  : srcPicturePitch - [in] Source picture pitch in number of bytes
Assumptions :
Limitations :
Returns     : max number of lines skippable (>=0)
*******************************************************************************/
unsigned int CHqvdpLitePlaneV2::MaxLinesSkippable(
                                            unsigned int srcPicturePitch) const
{
    // Address(Line N+1) = Address(Line N) + Pitch
    const unsigned int maxPitchInNbOfLines = HQVDPLITE_MAX_PITCH_IN_BYTES / srcPicturePitch;
    const unsigned int maxLinesSkippable   = maxPitchInNbOfLines > 0 ? maxPitchInNbOfLines - 1 : 0;
    return maxLinesSkippable;
}


/*******************************************************************************
Name        : GetNumberOfHwRequired
Description : Gets the number of HW instances required for current display context
Parameters  : pDisplayInfo - [in] Display info
Assumptions :
Limitations :
Returns     : number of HW required, 0 if error occurred
*******************************************************************************/
int CHqvdpLitePlaneV2::GetNumberOfHwRequired(
                            const CHqvdpLiteDisplayInfoV2*  pDisplayInfo) const
{
    int                     numberOfHw = 0;
    HQVDPLITE_OUT_Params_t  outParams;
    hqvdp_lld_error         err = HQVDP_LLD_NO_ERR;

    if(!PrepareOutParams(pDisplayInfo, &outParams))
    {
        PLANE_TRC(TRC_ID_ERROR, "Failed to prepare out params");
        return 0;
    }

    err = hqvdp_lld_get_number_of_hardware(m_lldHandle, pDisplayInfo->m_selectedPicture.srcFrameRect.height, &outParams, &numberOfHw);
    if(err != HQVDP_LLD_NO_ERR)
    {
        PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_get_number_of_hardware() failed, err=%d", err);
        return 0;
    }

    return numberOfHw;
}


/*******************************************************************************
Name        : GetIOWidthsOneHw
Description : Get I/O widths handled by one hardware instance.
              If multiple hardwares are required, every single HW will process
              a vertical stripe of the picture whose width is known by LLD.
Parameters  : pDisplayInfo       - [in]  Display info
              inputWidth         - [in]  Input width to be processed by HW
              outputWidth        - [in]  Output width to be produced by HW
              pInputWidthOneHw   - [out] Actual input width to be processed by one HW
              pOutputWidthOneHw  - [out] Actual output width to be produced by one HW
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
bool CHqvdpLitePlaneV2::GetIOWidthsOneHw(
                            const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                            uint32_t                        inputWidth,
                            uint32_t                        outputWidth,
                            uint32_t*                       pInputWidthOneHw,
                            uint32_t*                       pOutputWidthOneHw) const
{
    const int numberOfHw = GetNumberOfHwRequired(pDisplayInfo);

    if(numberOfHw <= 0)
    {
        PLANE_TRC(TRC_ID_ERROR, "Invalid number of HW: %d", numberOfHw);
        return false;
    }

    PLANE_TRC(TRC_ID_HQVDPLITE, "Number of HW required %d", numberOfHw);

    if(numberOfHw == 1)
    {
        *pInputWidthOneHw  = inputWidth;
        *pOutputWidthOneHw = outputWidth;

        /* Workaround for Pixel repeat until fix is provided at LLD level */
        if(   m_lldProfile == HQVDP_PROFILE_4KP60_PR
           && m_outputInfo.currentMode.mode_timing.pixels_per_line > 2640
           && m_outputInfo.currentMode.mode_params.vertical_refresh_rate > 30000
           && outputWidth > 1920)
        {
            /* LLD will enable pixel repeat and thus will program horizontal downscale */
            *pOutputWidthOneHw /= 2;
        }
    }
    else
    {
        hqvdp_lld_error err              = HQVDP_LLD_NO_ERR;
        uint16_t        inputWidthSplit  = 0;
        uint16_t        outputWidthSplit = 0;

        err = hqvdp_lld_get_split_width(m_lldHandle, (uint16_t)inputWidth, (uint16_t)outputWidth, &inputWidthSplit, &outputWidthSplit);
        if(err != HQVDP_LLD_NO_ERR)
        {
            PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_get_split_width() failed, err=%d", err);
            return false;
        }

        *pInputWidthOneHw  = (uint32_t)inputWidthSplit;
        *pOutputWidthOneHw = (uint32_t)outputWidthSplit;
    }

    return true;
}


/*******************************************************************************
Name        : IsScalingPossibleBySkippingLines
Description : The intent of this function is to check if downscale is achievable
              by skipping lines of the input picture i.e. by feeding the hardware
              with fewer lines than actual picture height.
              Here skipping lines means:
                - skip 1 line  out of 2,
                - skip 2 lines out of 3,
                - etc.
Parameters  : pCurrNode     - [in]     Current node
              pDisplayInfo  - [in/out] Display info
Assumptions :
Limitations :
Returns     : True if scaling is possible, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsScalingPossibleBySkippingLines(
                                    CDisplayNode*                   pCurrNode,
                                    CHqvdpLiteDisplayInfoV2*        pDisplayInfo)
{
    const unsigned int maxNbOfLinesSkippable = MaxLinesSkippable(pDisplayInfo->m_selectedPicture.pitch);

    PLANE_TRC(TRC_ID_HQVDPLITE, "Max number of lines skippable by HQVDP %u", maxNbOfLinesSkippable);

    // Artificially reduce picture height incrementally by skipping lines and check if scaling is possible
    for(unsigned int nbOfLinesToSkip = 1; nbOfLinesToSkip <= maxNbOfLinesSkippable; nbOfLinesToSkip++)
    {
        PLANE_TRCBL(TRC_ID_MAIN_INFO);
        PLANE_TRC(TRC_ID_MAIN_INFO, "## Checking if scaling possible with %s picture by skipping %u lines out of %u",
                                    pDisplayInfo->m_isSecondaryPictureSelected ? "Secondary" : "Primary",
                                    nbOfLinesToSkip, nbOfLinesToSkip + 1);

        pDisplayInfo->m_srcLinesSkipped = nbOfLinesToSkip;
        FillSelectedPictureDisplayInfo(pCurrNode, pDisplayInfo);

        if(IsScalingPossibleByHw(pDisplayInfo))
        {
            // Scaling possible
            PLANE_TRC(TRC_ID_MAIN_INFO, "## Scaling possible with %s picture by skipping %u lines out of %u",
                                        pDisplayInfo->m_isSecondaryPictureSelected ? "Secondary" : "Primary",
                                        nbOfLinesToSkip, nbOfLinesToSkip + 1);
            return true;
        }
    }

    PLANE_TRC(TRC_ID_MAIN_INFO, "## Scaling NOT possible by skipping lines");
    return false;
}


/*******************************************************************************
Name        : AreZoomConstraintsOk
Description : Tells if zoom is possible based on HW capabilities
Parameters  : vhsrcInputWidth   - [in] Input frame width
              vhsrcInputHeight  - [in] Input frame height
              vhsrcOutputWidth  - [in] Output frame width
              vhsrcOutputHeight - [in] Output frame height
Assumptions :
Limitations :
Returns     : true if zoom constraints are met, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::AreZoomConstraintsOk(
                                uint32_t    vhsrcInputWidth,
                                uint32_t    vhsrcInputHeight,
                                uint32_t    vhsrcOutputWidth,
                                uint32_t    vhsrcOutputHeight) const
{
    bool bOk = true;

    PLANE_TRC(TRC_ID_HQVDPLITE, "Scaling (%ux%u) => (%ux%u)", vhsrcInputWidth, vhsrcInputHeight, vhsrcOutputWidth, vhsrcOutputHeight);

    // Check absolute input and output geometries
    bOk =    (vhsrcInputWidth   >= HQVDPLITE_MIN_INPUT_WIDTH  ) && (vhsrcInputWidth   <= HQVDPLITE_MAX_INPUT_WIDTH  )
          && (vhsrcInputHeight  >= HQVDPLITE_MIN_INPUT_HEIGHT ) && (vhsrcInputHeight  <= HQVDPLITE_MAX_INPUT_HEIGHT )
          && (vhsrcOutputWidth  >= HQVDPLITE_MIN_OUTPUT_WIDTH ) && (vhsrcOutputWidth  <= HQVDPLITE_MAX_OUTPUT_WIDTH )
          && (vhsrcOutputHeight >= HQVDPLITE_MIN_OUTPUT_HEIGHT) && (vhsrcOutputHeight <= HQVDPLITE_MAX_OUTPUT_HEIGHT);

    if(!bOk)
    {
        PLANE_TRC(TRC_ID_ERROR, "I/O windows dimensions out of HW limits");
        return false;
    }

    // Check vertical downscale factor (Take care to not reach HW limit, hence '<')
    bOk =    (vhsrcInputHeight <= (vhsrcOutputHeight * HQVDPLITE_MAX_VERTICAL_SCALING_FACTOR))
          && (vhsrcInputHeight <  (vhsrcOutputHeight * HQVDPLITE_SCALING_FACTOR_HARDWARE_LIMIT));

    if(!bOk)
    {
        // Do not issue an error log while we are searching a supported scaling factor
        PLANE_TRC(TRC_ID_HQVDPLITE, "Vertical downscale too big (max sw %d, max hw %d)",
                  HQVDPLITE_MAX_VERTICAL_SCALING_FACTOR, HQVDPLITE_SCALING_FACTOR_HARDWARE_LIMIT);
        return false;
    }

    // Check horizontal downscale factor (Take care to not reach HW limit, hence '<')
    bOk =    (vhsrcInputWidth <= (vhsrcOutputWidth * HQVDPLITE_MAX_HORIZONTAL_SCALING_FACTOR))
          && (vhsrcInputWidth <  (vhsrcOutputWidth * HQVDPLITE_SCALING_FACTOR_HARDWARE_LIMIT));

    if(!bOk)
    {
        // Do not issue an error log while we are searching a supported scaling factor
        PLANE_TRC(TRC_ID_HQVDPLITE, "Horizontal downscale too big (max sw %d, max hw %d)",
                  HQVDPLITE_MAX_VERTICAL_SCALING_FACTOR, HQVDPLITE_SCALING_FACTOR_HARDWARE_LIMIT);
        return false;
    }

    PLANE_TRC(TRC_ID_HQVDPLITE, "Zoom constraints OK");

    return true;
}

/*******************************************************************************
Name        : IsHwProcessingTimeOk
Description : This function checks that the HW processing time to perform this
              scaling is achievable
Parameters  : vhsrcInputWidth   - [in] Input frame width
              vhsrcInputHeight  - [in] Input frame height
              vhsrcOutputWidth  - [in] Output frame width
              vhsrcOutputHeight - [in] Output frame height
              deiMode           - [in] DEI mode
Assumptions :
Limitations :
Returns     : true if possible to achieve scaling, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsHwProcessingTimeOk(
                                uint32_t            vhsrcInputWidth,
                                uint32_t            vhsrcInputHeight,
                                uint32_t            vhsrcOutputWidth,
                                uint32_t            vhsrcOutputHeight,
                                HqvdpLiteDeiMode_e  deiMode) const
{
    const uint32_t              hqvdp_clock_freq_MHz  = m_clockFreqInMHz;
    const uint32_t              vtg_full_width        = m_outputInfo.currentMode.mode_timing.pixels_per_line;
    const uint32_t              output_pixel_clock_Hz = m_outputInfo.currentMode.mode_timing.pixel_clock_freq;
    const bool                  is_display_interlaced = m_outputInfo.isDisplayInterlaced;

    const uint32_t input_overhead_factor            = (deiMode==HQVDPLITE_DEI_MODE_OFF) ? 10 : 20;
    const uint32_t output_overhead_factor           = 5;
    const uint32_t hqvdp_clock_freq_Hz              = 1000000 * hqvdp_clock_freq_MHz;
    const uint32_t p2i_factor                       = is_display_interlaced ? 2 : 1;
    const uint32_t lines_processed                  = MAX(vhsrcInputHeight, vhsrcOutputHeight);
    const uint32_t max_fw_cycles_with_overhead      = (HQVDPLITE_MAX_FIRMWARE_CYCLES * (100 + input_overhead_factor)) / 100;
    const uint32_t vhsrc_input_width_with_overhead  = (vhsrcInputWidth  * (100 + input_overhead_factor )) / 100;
    const uint32_t vhsrc_output_width_with_overhead = (vhsrcOutputWidth * (100 + output_overhead_factor)) / 100;
    const uint32_t width_processed_with_overhead    = MAX(vhsrc_input_width_with_overhead, vhsrc_output_width_with_overhead);
    bool           res                              = false;
    uint64_t       left_value                       = 0;
    uint64_t       right_value                      = 0;

    TRC(TRC_ID_HQVDPLITE, "Raw parameters  ---------------------------------------");
    TRC(TRC_ID_HQVDPLITE, "vhsrcInputWidth                = %u",   vhsrcInputWidth);
    TRC(TRC_ID_HQVDPLITE, "vhsrcInputHeight               = %u",   vhsrcInputHeight);
    TRC(TRC_ID_HQVDPLITE, "vhsrcOutputWidth               = %u",   vhsrcOutputWidth);
    TRC(TRC_ID_HQVDPLITE, "vhsrcOutputHeight              = %u",   vhsrcOutputHeight);
    TRC(TRC_ID_HQVDPLITE, "hqvdp_clock_freq_MHz           = %u",   hqvdp_clock_freq_MHz);
    TRC(TRC_ID_HQVDPLITE, "vtg_full_width                 = %u",   vtg_full_width);
    TRC(TRC_ID_HQVDPLITE, "output_pixel_clock_Hz          = %u",   output_pixel_clock_Hz);
    TRC(TRC_ID_HQVDPLITE, "is_display_interlaced          = %s",   is_display_interlaced ? "true" : "false");
    TRC(TRC_ID_HQVDPLITE, "deiMode                        = %s",   HQVDPLITE_DEI_MODE_STR(deiMode));

    TRC(TRC_ID_HQVDPLITE, "Internal variables ------------------------------------");
    TRC(TRC_ID_HQVDPLITE, "input_overhead_factor            = %u%%", input_overhead_factor);
    TRC(TRC_ID_HQVDPLITE, "output_overhead_factor           = %u%%", output_overhead_factor);
    TRC(TRC_ID_HQVDPLITE, "hqvdp_clock_freq_Hz              = %u",   hqvdp_clock_freq_Hz);
    TRC(TRC_ID_HQVDPLITE, "p2i_factor                       = %u",   p2i_factor);
    TRC(TRC_ID_HQVDPLITE, "lines_processed                  = %u",   lines_processed);
    TRC(TRC_ID_HQVDPLITE, "max_fw_cycles_with_overhead      = %u",   max_fw_cycles_with_overhead);
    TRC(TRC_ID_HQVDPLITE, "vhsrc_input_width_with_overhead  = %u",   vhsrc_input_width_with_overhead);
    TRC(TRC_ID_HQVDPLITE, "vhsrc_output_width_with_overhead = %u",   vhsrc_output_width_with_overhead);
    TRC(TRC_ID_HQVDPLITE, "width_processed_with_overhead    = %u",   width_processed_with_overhead);

     /*
      * HW processing time must meet 2 constraints:
      *
      * 1. Firmware processing time constraint
      *
      *                                    lines_processed      vtg_full_width x hqvdp_clock_freq_Hz
      *    max_fw_cycles_with_overhead x ------------------- <= ------------------------------------
      *                                   vhsrcOutputHeight      p2i_factor x output_pixel_clock_Hz
      *
      * 2. Scaler processing time constraint
      *
      *                                      lines_processed      vtg_full_width x hqvdp_clock_freq_Hz
      *    width_processed_with_overhead x ------------------- <= ------------------------------------
      *                                    vhsrcOutputHeight      p2i_factor x output_pixel_clock_Hz
      *
      * This can be simplified in a single constraint:
      *                                                                       lines_processed      vtg_full_width x hqvdp_clock_freq_Hz
      *    MAX(max_fw_cycles_with_overhead,width_processed_with_overhead) x ------------------- <= ------------------------------------
      *                                                                      vhsrcOutputHeight      p2i_factor x output_pixel_clock_Hz
      *
      * To avoid 64-bits division, it is implemented as follows:
      *
      *    left_value  = MAX(max_fw_cycles_with_overhead,width_processed_with_overhead) x lines_processed x p2i_factor x output_pixel_clock_Hz
      *
      *    right_value = vtg_full_width x hqvdp_clock_freq_Hz x vhsrcOutputHeight
      *
      * So constraint to be met is:
      *
      *    left_value <= right_value
      */

     left_value  = (uint64_t) MAX(max_fw_cycles_with_overhead, width_processed_with_overhead)
                 * (uint64_t) lines_processed
                 * (uint64_t) p2i_factor
                 * (uint64_t) output_pixel_clock_Hz;

     right_value = (uint64_t) vtg_full_width
                 * (uint64_t) hqvdp_clock_freq_Hz
                 * (uint64_t) vhsrcOutputHeight;

     res = (left_value < right_value);

     TRC(TRC_ID_HQVDPLITE, "Result ------------------------------------------------");
     TRC(TRC_ID_HQVDPLITE, "left_value                       = %llu", left_value);
     TRC(TRC_ID_HQVDPLITE, "right_value                      = %llu", right_value);
     TRC(TRC_ID_HQVDPLITE, "Processing achievable            = %s",   res ? "yes" : "no");

     return res;
}


/*******************************************************************************
Name        : GetNbrBytesPerPixel
Description : Get the number of bytes per pixel for the selected picture.
Parameters  : is420        - [in]  true if picture is 420 encoded
              is422        - [in]  true if picture is 422 encoded
              isOn10Bits   - [in]  True if picture pixel is on 10 bits
              isInterlaced - [in]  true if picture is interlaced
              deiMode      - [in]  DEI mode
Assumptions :
Limitations :
Returns     : Number of bytes per pixel
*******************************************************************************/
stm_rational_t CHqvdpLitePlaneV2::GetNbrBytesPerPixel(
                                    bool                    is420,
                                    bool                    is422,
                                    bool                    isOn10Bits,
                                    bool                    isInterlaced,
                                    HqvdpLiteDeiMode_e      deiMode) const
{
    stm_rational_t nbBytesPerPixel = { .numerator = 0, .denominator = 1 };

    // Compute number of bytes for DEI OFF
    if(is422)
    {
        // 4:2:2 image encoding
        // 4 pixels require : 4Y + 2Cb + 2Cr samples
        // 1 pixel requires : (4 + 2 + 2) / 4  = 2 samples
        nbBytesPerPixel.numerator   = 2;
        nbBytesPerPixel.denominator = 1;
    }
    else if(is420)
    {
        // 4:2:0 image encoding
        // 4 pixels require : 4Y + 1Cb + 1Cr samples
        // 1 pixel requires : (4 + 1 + 1) / 4  = 1.5 samples
        nbBytesPerPixel.numerator   = 3;
        nbBytesPerPixel.denominator = 2;
    }
    else
    {
        // 4:4:4 image encoding
        // 4 pixels require : 4Y + 4Cb + 4Cr samples
        // 1 pixel requires : (4 + 4 + 4) / 4  = 3 samples
        nbBytesPerPixel.numerator   = 3;
        nbBytesPerPixel.denominator = 1;
    }

    // Adjust against pixel bit depth
    if(isOn10Bits)
    {
        // NB 5/4 == 10/8
        nbBytesPerPixel.numerator   *= 5;
        nbBytesPerPixel.denominator *= 4;
    }

    // Adjust number of bytes in case 3D DEI is used
    if(isInterlaced && deiMode == HQVDPLITE_DEI_MODE_3D)
    {
        // 1 pixel requires : (Number of bytes per pixels (DEI OFF) x 3 pictures) + (1 byte x 3 motion buffers)
        // => numerator   = numerator (DEI OFF) * 3 + denominator (DEI OFF) x 3
        //    denominator = denominator (DEI OFF)
        nbBytesPerPixel.numerator =   (nbBytesPerPixel.numerator   * HQVDPLITE_3D_DEI_PICTURES_NB)
                                    + (nbBytesPerPixel.denominator * HQVDPLITE_DEI_MOTION_BUFFERS_NB);
    }

    return nbBytesPerPixel;
}


/*******************************************************************************
Name        : ComputeSTBusDataRate
Description : Computes the required STBus rate for a given scaling ratio
Parameters  : srcFrameWidth                - [in]  Source frames width
              srcFrameHeight               - [in]  Source frames height
              isSrc420                     - [in]  True if source is 420 encoded
              isSrc422                     - [in]  True if source is 422 encoded
              isSrcOn10Bits                - [in]  True if source bit depth is 10
              isSrcInterlaced              - [in]  True if source is interlaced
              dstFrameWidth                - [in]  Destination frames width
              dstFrameHeight               - [in]  Destination frames height
              deiMode                      - [in]  DEI mode
              verticalRefreshRateInMilliHz - [in]  Vertical refresh rate in milli-Hz
Assumptions :
Limitations :
Returns     : STBus data rate in MBytes per second
*******************************************************************************/
uint32_t CHqvdpLitePlaneV2::ComputeSTBusDataRate(
                                    uint32_t                srcFrameWidth,
                                    uint32_t                srcFrameHeight,
                                    bool                    isSrc420,
                                    bool                    isSrc422,
                                    bool                    isSrcOn10Bits,
                                    bool                    isSrcInterlaced,
                                    uint32_t                dstFrameWidth,
                                    uint32_t                dstFrameHeight,
                                    HqvdpLiteDeiMode_e      deiMode,
                                    uint32_t                verticalRefreshRateInMilliHz) const
{
    const stm_rational_t nbBytesPerPixel = GetNbrBytesPerPixel(isSrc420, isSrc422, isSrcOn10Bits, isSrcInterlaced, deiMode);

    const uint32_t srcHeight = isSrcInterlaced ? srcFrameHeight / 2 : srcFrameHeight;

    // If the output is Interlaced, no need to calculate the "field" size for OutputHeight and OutputTotalHeight
    // because the result will be the same when doing "OutputHeight / OutputTotalHeight"
    const uint32_t outputTotalHeight = m_outputInfo.currentMode.mode_timing.lines_per_frame;

    /*
     *  STBus data rate estimation:
     *
     *  STBus data rate = (Size of data read-written in memory) / (time available to perform the operation)
     *
     *  Size of data read-written in memory = (src_width * src_height) * NbrBytesPerPixel
     *
     *  Time available to perform the operation = OutputHeight / (VSyncFreq * OutputTotalHeight)
     *
     *  So we get:
     *  STBus data rate = NbrBytesPerPixel * (src_width * src_height) / (OutputHeight / (VSyncFreq * OutputTotalHeight))
     *                  = NbrBytesPerPixel * (src_width * src_height) * (VSyncFreq * OutputTotalHeight) / OutputHeight
     *
     *  In the info about the current VTG mode, we have the "vertical_refresh_rate".
     *  "vertical_refresh_rate" is in mHz so it should be divided by 1000 to get the VSyncFreq.
     *
     *  STBus data rate = NbrBytesPerPixel * (src_width * src_height) * (vertical_refresh_rate * OutputTotalHeight) / (1000 * OutputHeight)
     *
     *  This will give a data rate in Bytes/s.
     *  Divide it by 10^6 to get a result in MB/s
     *
     *  As a conclusion, the formula to use is:
     *  STBus data rate = NbrBytesPerPixel * (src_width * src_height) * (vertical_refresh_rate * OutputTotalHeight) / (1000 * 1000000 * OutputHeight)
     */

    const uint64_t num =   (uint64_t)nbBytesPerPixel.numerator
                         * (uint64_t)srcFrameWidth
                         * (uint64_t)srcHeight
                         * (uint64_t)verticalRefreshRateInMilliHz
                         * (uint64_t)outputTotalHeight;

    const uint64_t den =   (uint64_t)nbBytesPerPixel.denominator
                         * (uint64_t)1000
                         * (uint64_t)1000000
                         * (uint64_t)dstFrameHeight;

    const uint32_t dataRateInMBytesPerS = (uint32_t)vibe_os_div64(num, den);

    return dataRateInMBytesPerS;
}


/*******************************************************************************
Name        : GetSTBusDataRateTolerancePercent
Description : Returns the STBus data rate tolerance to be used for a given output
Parameters  : fullOutputHeight         - [in] Full display height in nb of lines
              outputRefreshRateMilliHz - [in] Display frame rate in milliHz
Assumptions :
Limitations :
Returns     : STBus datarate tolerance in percent
*******************************************************************************/
uint32_t CHqvdpLitePlaneV2::GetSTBusDataRateTolerancePercent(
                                    uint32_t    fullOutputHeight,
                                    uint32_t    outputRefreshRateMilliHz) const
{
    uint32_t tolerance = 0;

    if(   (fullOutputHeight         >= HQVDPLITE_ULTRA_HD_HEIGHT)
       && (outputRefreshRateMilliHz >= (1000 * HQVDPLITE_VERTICAL_REFRESH_RATE_50Hz)))
    {
        tolerance = HQVDPLITE_STBUS_DATARATE_TOLERANCE_LOW_PC;
    }
    else
    {
        tolerance = HQVDPLITE_STBUS_DATARATE_TOLERANCE_HIGH_PC;
    }

    return tolerance;
}


/*******************************************************************************
Name        : IsSTBusDataRateOk
Description : This function checks that the number of MB/s required on the STBus
              by the Display IP to perform this scaling is achievable
Parameters  : pDisplayInfo    - [in]  Display info
              deiMode         - [in]  DEI mode
Assumptions :
Limitations :
Returns     : true STBus rate is achievable, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::IsSTBusDataRateOk(
                                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                    HqvdpLiteDeiMode_e              deiMode)
{
    const uint32_t fullOutputWidth              = m_outputInfo.currentMode.mode_params.active_area_width;
    const uint32_t fullOutputHeight             = m_outputInfo.currentMode.mode_params.active_area_height;
    const uint32_t outputRefreshRateMilliHz     = m_outputInfo.currentMode.mode_params.vertical_refresh_rate;
    const bool     bIsReferenceSrcOn10Bits      = m_bUse8BitsSrcForRefUseCase ? false : pDisplayInfo->m_selectedPicture.isSrcOn10bits;
    const uint32_t dataRateTolerancePercent     = GetSTBusDataRateTolerancePercent(fullOutputHeight, outputRefreshRateMilliHz);
    uint32_t       referenceDataRateInMBperS    = 0;
    uint32_t       useCaseDataRateInMBperS      = 0;

    // Compute the STBus Data Rate for a reference use case:
    // - Up to Full HD source, consider Full HD as the reference use case.
    // - If the source is greater than Full HD then consider Ultra HD as the reference use case.
    if(pDisplayInfo->m_primarySrcFrameRect.height > HQVDPLITE_MB_ALIGNED_FULL_HD_HEIGHT)
    {
        // Reference use case: Ultra HD
        const uint32_t referenceRefreshRateHz = (m_lldProfile == HQVDP_PROFILE_4KP60) ? HQVDPLITE_VERTICAL_REFRESH_RATE_60Hz : HQVDPLITE_VERTICAL_REFRESH_RATE_30Hz;

        referenceDataRateInMBperS = ComputeSTBusDataRate(
                                                HQVDPLITE_ULTRA_HD_WIDTH,
                                                HQVDPLITE_ULTRA_HD_HEIGHT,
                                                pDisplayInfo->m_selectedPicture.isSrc420,
                                                pDisplayInfo->m_selectedPicture.isSrc422,
                                                bIsReferenceSrcOn10Bits,
                                                pDisplayInfo->m_isSrcInterlaced,
                                                fullOutputWidth,
                                                fullOutputHeight,
                                                HQVDPLITE_DEI_MODE_3D,
                                                referenceRefreshRateHz * 1000);
    }
    else
    {
        // Reference use case: Full HD source displayed on full screen output (with 3D DEI mode if source is Interlaced).
        referenceDataRateInMBperS = ComputeSTBusDataRate(
                                                HQVDPLITE_FULL_HD_WIDTH,
                                                HQVDPLITE_FULL_HD_HEIGHT,
                                                pDisplayInfo->m_selectedPicture.isSrc420,
                                                pDisplayInfo->m_selectedPicture.isSrc422,
                                                bIsReferenceSrcOn10Bits,
                                                pDisplayInfo->m_isSrcInterlaced,
                                                fullOutputWidth,
                                                fullOutputHeight,
                                                HQVDPLITE_DEI_MODE_3D,
                                                HQVDPLITE_VERTICAL_REFRESH_RATE_60Hz * 1000);
    }

    // Compute the STBus Data Rate for the current use case
    useCaseDataRateInMBperS = ComputeSTBusDataRate(
                                                pDisplayInfo->m_selectedPicture.srcFrameRect.width,
                                                pDisplayInfo->m_selectedPicture.srcFrameRect.height,
                                                pDisplayInfo->m_selectedPicture.isSrc420,
                                                pDisplayInfo->m_selectedPicture.isSrc422,
                                                pDisplayInfo->m_selectedPicture.isSrcOn10bits,
                                                pDisplayInfo->m_isSrcInterlaced,
                                                pDisplayInfo->m_dstFrameRect.width,
                                                pDisplayInfo->m_dstFrameRect.height,
                                                deiMode,
                                                outputRefreshRateMilliHz);

    // Based on allowed tolerance, compute data rate threshold
    const uint32_t dataRateThresholdInMBperS = (referenceDataRateInMBperS * (100 + dataRateTolerancePercent)) / 100;

    PLANE_TRC(TRC_ID_HQVDPLITE, "DataRate Tolerance = %u%%",    dataRateTolerancePercent);
    PLANE_TRC(TRC_ID_HQVDPLITE, "DataRate Reference = %u MB/s", referenceDataRateInMBperS);
    PLANE_TRC(TRC_ID_HQVDPLITE, "DataRate Use Case  = %u MB/s", useCaseDataRateInMBperS);
    PLANE_TRC(TRC_ID_HQVDPLITE, "DataRate Threshold = %u MB/s", dataRateThresholdInMBperS);

    const bool bIsOk = useCaseDataRateInMBperS <= dataRateThresholdInMBperS ? true : false;
    PLANE_TRC(TRC_ID_MAIN_INFO, "STBus Data Rate is %s", bIsOk ? "OK" : "not OK");

    m_Statistics.DataRateUseCaseInMBperS = bIsOk ? useCaseDataRateInMBperS : 0;

    return bIsOk;
}

/*******************************************************************************
Name        : ResetEveryUseCases
Description : Reset all use case configurations
Parameters  :
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::ResetEveryUseCases()
{
    vibe_os_zero_memory(m_hqvdpConf, sizeof(m_hqvdpConf));
}


/*******************************************************************************
Name        : SetPictureBaseAddresses
Description : Set Luma/Chroma Base Addresses of a picture.
Parameters  : pCurNode           - [in]  Current node to display
              pDisplayInfo       - [in]  Display info
              pLumaBaseAddress   - [out] Base address of Luma buffer
              pChromaBaseAddress - [out] Base address of Chroma buffer
Assumptions :
Limitations :
Returns     : true if luma/chroma buffers addresses could be set, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::SetPictureBaseAddresses(
                                    const CDisplayNode*             pCurNode,
                                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                    uint32_t*                       pLumaBaseAddress,
                                    uint32_t*                       pChromaBaseAddress) const
{
    const stm_buffer_src_picture_desc_t* pSrcPicture = 0;

    if(!pCurNode)
        return false;

    if(pDisplayInfo->m_isSecondaryPictureSelected)
    {
        pSrcPicture = &pCurNode->m_bufferDesc.src.secondary_picture;
    }
    else
    {
        pSrcPicture = &pCurNode->m_bufferDesc.src.primary_picture;
    }

    BufferNodeType srcPictureType = pCurNode->m_srcPictureType;

    /* Calculate PictureBaseAddress base on the color format */
    switch (pSrcPicture->color_fmt)
    {
        case SURF_CRYCB888:
        case SURF_CRYCB101010:
        case SURF_ACRYCB8888:
        {
            // These formats are supported only with progressive pictures
            // This has been checked earlier
            *pLumaBaseAddress   = pSrcPicture->video_buffer_addr;
            *pChromaBaseAddress = 0;
            break;
        }
        case SURF_YCbCr420R2B:
        case SURF_YCbCr422R2B:
        case SURF_YCbCr420R2B10:
        case SURF_YCbCr422R2B10:
        {
            /* YcbCr 4:2:0 Raster Double Buffer */
            if(srcPictureType == GNODE_PROGRESSIVE || srcPictureType == GNODE_TOP_FIELD)
            {
                *pLumaBaseAddress   = pSrcPicture->video_buffer_addr;
                *pChromaBaseAddress = pSrcPicture->video_buffer_addr + pSrcPicture->chroma_buffer_offset;
            }
            else
            {
                /* In case of Bottom field presented, skip a line in the base address */
                *pLumaBaseAddress   = pSrcPicture->video_buffer_addr + pSrcPicture->pitch;
                *pChromaBaseAddress = pSrcPicture->video_buffer_addr + pSrcPicture->chroma_buffer_offset + pSrcPicture->pitch;
            }
            break;
        }
        case SURF_YCBCR420MB:
        {
            /* Macroblock Format (usual case) */
            *pLumaBaseAddress   = pSrcPicture->video_buffer_addr;
            *pChromaBaseAddress = pSrcPicture->video_buffer_addr + pSrcPicture->chroma_buffer_offset;
            break;
        }
        default:
        {
            // Unsupported format
            return false;
        }
    }
    return true;
}

/*******************************************************************************
Name        : RotateMotionBuffers
Description : Makes the motion ring buffer mechanism advance one step
Parameters  : None
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::RotateMotionBuffers(void)
{
    uint32_t availMotionBufferPhysAddr  = m_prevMotionBufferPhysAddr;
    m_prevMotionBufferPhysAddr          = m_currMotionBufferPhysAddr;
    m_currMotionBufferPhysAddr          = m_nextMotionBufferPhysAddr;
    m_nextMotionBufferPhysAddr          = availMotionBufferPhysAddr;
}

/*******************************************************************************
Name        : UpdateMotionState
Description : Advance the motion state one step further
Parameters  : None
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::UpdateMotionState(void)
{
    switch(m_motionState)
    {
        case HQVDPLITE_DEI_MOTION_OFF:
        {
            m_motionState = HQVDPLITE_DEI_MOTION_INIT;
            break;
        }
        case HQVDPLITE_DEI_MOTION_INIT:
        {
            m_motionState = HQVDPLITE_DEI_MOTION_LOW_CONF;
            break;
        }
        case HQVDPLITE_DEI_MOTION_LOW_CONF:
        {
            m_motionState = HQVDPLITE_DEI_MOTION_FULL_CONF;
            break;
        }
        case HQVDPLITE_DEI_MOTION_FULL_CONF:
        {
            // Motion state cannot go higher
            break;
        }
    }
}

/*******************************************************************************
Name        : ProcessLastVsyncStatus
Description : Processes the status of the configuration processed on last VSync
Parameters  : vSyncTime      - [in]     VSync time
              pNodeDisplayed - [in/out] Info on node displayed
Assumptions : LLD session is open
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::ProcessLastVsyncStatus(
                                    const stm_time64_t& vSyncTime,
                                    CDisplayNode*       pNodeDisplayed)
{
    hqvdp_lld_status_s  hqvdpStatus[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE];
    hqvdp_lld_error     err       = HQVDP_LLD_NO_ERR;
    int                 statusCnt = 0;

    // Check node validity
    if(pNodeDisplayed && !pNodeDisplayed->IsNodeValid() )
    {
        //This error can be uncommented when the blocking "flush all" gets implemented
        //PLANE_TRC( TRC_ID_ERROR, "Invalid node 0x%p", pNodeDisplayed);
        m_crcData.Status = false;
        return;
    }

    if(!pNodeDisplayed)
    {
        /* pNodeDisplayed is null so no picture was displayed during the previous VSync period and there is no result to collect */
        return;
    }

    PLANE_TRCBL(TRC_ID_PLANE_STATUS);

    if(m_currentState != STM_PLANE_HW_ENABLED)
    {
        /* The plane is currently disabled at mixer level so no processing is done and there is not status to collect */
        PLANE_TRC( TRC_ID_PLANE_STATUS, "No status because plane disabled at mixer level");
        return;
    }

    /* Exit if IO Windows was not valid */
    if(!m_hqvdpDisplayInfo.m_areIOWindowsValid)
    {
        PLANE_TRC( TRC_ID_PLANE_STATUS, "No status because IOWindows invalid");
        return;
    }

    vibe_os_zero_memory(hqvdpStatus, sizeof(hqvdpStatus));

    // Check that Proc Clock is really enabled before any call to a LLD function
    CheckProcClockActivation();

    // Ask previous conf status to LLD
    err = hqvdp_lld_get_status(m_lldHandle, &statusCnt, hqvdpStatus);

    if(err != HQVDP_LLD_NO_ERR)
    {
        PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_get_status() failed, err=%d", err);
        return;
    }

    /* Save the HQVDP CRC data collected at last VSync */
    vibe_os_zero_memory(&m_crcData, sizeof(m_crcData));
    m_crcData.LastVsyncTime  = vSyncTime;
    m_crcData.PictureID      = pNodeDisplayed->m_pictureId;
    m_crcData.PTS            = pNodeDisplayed->m_bufferDesc.info.PTS;
    m_crcData.Status         = true;

    m_Statistics.CurDispPicPTS = pNodeDisplayed->m_bufferDesc.info.PTS;

    switch(pNodeDisplayed->m_srcPictureType)
    {
        case GNODE_BOTTOM_FIELD:
        {
            m_crcData.PictureType = 'B';
            break;
        }
        case GNODE_TOP_FIELD:
        {
            m_crcData.PictureType = 'T';
            break;
        }
        case GNODE_PROGRESSIVE:
        {
            m_crcData.PictureType = 'F';
            break;
        }
        default:
        {
            m_crcData.PictureType = 'U';
            break;
        }
    }

    PLANE_TRC(TRC_ID_PLANE_STATUS, "PictureId: %d    PictureType: %c    PTS=%llx",  m_crcData.PictureID, m_crcData.PictureType, m_crcData.PTS);

    /* HQVDP_TOPStatus_Params_t */
    m_crcData.CrcValue[ 0] = hqvdpStatus[0].TopStatus.InputYCrc;
    m_crcData.CrcValue[ 1] = hqvdpStatus[0].TopStatus.InputUvCrc;
    /* HQVDP_FMDStatus_Params_t */
    m_crcData.CrcValue[ 2] = hqvdpStatus[0].FmdStatus.NextYFmdCrc;
    m_crcData.CrcValue[ 3] = hqvdpStatus[0].FmdStatus.NextNextYFmdCrc;
    m_crcData.CrcValue[ 4] = hqvdpStatus[0].FmdStatus.NextNextNextYFmdCrc;
    /* HQVDP_CSDIStatus_Params_t */
    m_crcData.CrcValue[ 5] = hqvdpStatus[0].CsdiStatus.PrevYCsdiCrc;
    m_crcData.CrcValue[ 6] = hqvdpStatus[0].CsdiStatus.CurYCsdiCrc;
    m_crcData.CrcValue[ 7] = hqvdpStatus[0].CsdiStatus.NextYCsdiCrc;
    m_crcData.CrcValue[ 8] = hqvdpStatus[0].CsdiStatus.PrevUvCsdiCrc;
    m_crcData.CrcValue[ 9] = hqvdpStatus[0].CsdiStatus.CurUvCsdiCrc;
    m_crcData.CrcValue[10] = hqvdpStatus[0].CsdiStatus.NextUvCsdiCrc;
    m_crcData.CrcValue[11] = hqvdpStatus[0].CsdiStatus.YCsdiCrc;
    m_crcData.CrcValue[12] = hqvdpStatus[0].CsdiStatus.UvCsdiCrc;
    m_crcData.CrcValue[13] = hqvdpStatus[0].CsdiStatus.UvCupCrc;
    m_crcData.CrcValue[14] = hqvdpStatus[0].CsdiStatus.MotCsdiCrc;
    m_crcData.CrcValue[15] = hqvdpStatus[0].CsdiStatus.MotCurCsdiCrc;
    m_crcData.CrcValue[16] = hqvdpStatus[0].CsdiStatus.MotPrevCsdiCrc;
    /* HQVDP_HQRStatus_Params_t */
    m_crcData.CrcValue[17] = hqvdpStatus[0].HvsrcStatus.YHvsrcCrc;
    m_crcData.CrcValue[18] = hqvdpStatus[0].HvsrcStatus.UHvsrcCrc;
    m_crcData.CrcValue[19] = hqvdpStatus[0].HvsrcStatus.VHvsrcCrc;
    /* HQVDP_IQIStatus_Params_t */
    m_crcData.CrcValue[20] = hqvdpStatus[0].IqiStatus.PxfItStatus;
    m_crcData.CrcValue[21] = hqvdpStatus[0].IqiStatus.YIqiCrc;
    m_crcData.CrcValue[22] = hqvdpStatus[0].IqiStatus.UIqiCrc;
    m_crcData.CrcValue[23] = hqvdpStatus[0].IqiStatus.VIqiCrc;

    /* Now check the status */
    for(int i=0; i<statusCnt; i++)
    {
        PLANE_TRCBL(TRC_ID_PLANE_STATUS);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] TopStatus.InputYCrc           = 0x%x", i, hqvdpStatus[i].TopStatus.InputYCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] TopStatus.InputUvCrc          = 0x%x", i, hqvdpStatus[i].TopStatus.InputUvCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] FmdStatus.NextYFmdCrc         = 0x%x", i, hqvdpStatus[i].FmdStatus.NextYFmdCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] FmdStatus.NextNextYFmdCrc     = 0x%x", i, hqvdpStatus[i].FmdStatus.NextNextYFmdCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] FmdStatus.NextNextNextYFmdCrc = 0x%x", i, hqvdpStatus[i].FmdStatus.NextNextNextYFmdCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.PrevYCsdiCrc       = 0x%x", i, hqvdpStatus[i].CsdiStatus.PrevYCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.CurYCsdiCrc        = 0x%x", i, hqvdpStatus[i].CsdiStatus.CurYCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.NextYCsdiCrc       = 0x%x", i, hqvdpStatus[i].CsdiStatus.NextYCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.PrevUvCsdiCrc      = 0x%x", i, hqvdpStatus[i].CsdiStatus.PrevUvCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.CurUvCsdiCrc       = 0x%x", i, hqvdpStatus[i].CsdiStatus.CurUvCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.NextUvCsdiCrc      = 0x%x", i, hqvdpStatus[i].CsdiStatus.NextUvCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.YCsdiCrc           = 0x%x", i, hqvdpStatus[i].CsdiStatus.YCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.UvCsdiCrc          = 0x%x", i, hqvdpStatus[i].CsdiStatus.UvCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.UvCupCrc           = 0x%x", i, hqvdpStatus[i].CsdiStatus.UvCupCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.MotCsdiCrc         = 0x%x", i, hqvdpStatus[i].CsdiStatus.MotCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.MotCurCsdiCrc      = 0x%x", i, hqvdpStatus[i].CsdiStatus.MotCurCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] CsdiStatus.MotPrevCsdiCrc     = 0x%x", i, hqvdpStatus[i].CsdiStatus.MotPrevCsdiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] HvsrcStatus.YHvsrcCrc         = 0x%x", i, hqvdpStatus[i].HvsrcStatus.YHvsrcCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] HvsrcStatus.UHvsrcCrc         = 0x%x", i, hqvdpStatus[i].HvsrcStatus.UHvsrcCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] HvsrcStatus.VHvsrcCrc         = 0x%x", i, hqvdpStatus[i].HvsrcStatus.VHvsrcCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] IqiStatus.PxfItStatus         = 0x%x", i, hqvdpStatus[i].IqiStatus.PxfItStatus);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] IqiStatus.YIqiCrc             = 0x%x", i, hqvdpStatus[i].IqiStatus.YIqiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] IqiStatus.UIqiCrc             = 0x%x", i, hqvdpStatus[i].IqiStatus.UIqiCrc);
        PLANE_TRC(TRC_ID_PLANE_STATUS, "HQVDP[%d] IqiStatus.VIqiCrc             = 0x%x", i, hqvdpStatus[i].IqiStatus.VIqiCrc);

        /* In normal condition END_PROCESSING should be equal to '1' at the end of a processing */
        if((hqvdpStatus[i].IqiStatus.PxfItStatus & IQI_STATUS_PXF_IT_STATUS_END_PROCESSING_MASK) == 0)
        {
            pNodeDisplayed->m_bufferDesc.info.stats.status |= STM_STATUS_BUF_HW_ERROR;
            PLANE_TRC(TRC_ID_ERROR, "HQVDP[%d]: Last cmd not processed correctly for pict %d%c (0x%p)",
                      i, pNodeDisplayed->m_pictureId, pNodeDisplayed->m_srcPictureTypeChar, pNodeDisplayed);
            m_bEndOfProcessingError = true;
        }
    }
}


/*******************************************************************************
Name        : DisableHW
Description : To be called when there is nothing to display
Parameters  : None
Assumptions : "m_vsyncLock" must be taken
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::DisableHW()
{
    bool procClkTemporaryEnabled = false;

    PLANE_TRC(TRC_ID_MAIN_INFO, "Disable HW");

    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    // For HQVDP, the Proc Clock should be enabled BEFORE sending a command to the HW
    if(!m_isProcClkEnabled)
    {
        EnableProcClock();
        procClkTemporaryEnabled = true;
    }

    StopHqvdp();

    if(procClkTemporaryEnabled)
    {
        DisableProcClock();
    }

    // Perform the generic stop actions
    CDisplayPlane::DisableHW();
}

/*******************************************************************************
Name        : DumpConfField
Description : Dump LLD/HQVDP conf field to traces
Parameters  : pStructName   [in] - Name of structure containing field
              pFieldName    [in] - Name of field to be dumped
              pField        [in] - Pointer to field to be dumped
              numFieldElts  [in] - Number of elements in field
Assumptions : Field to be dumped is a uint32_t (or an array of uint32_t)
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::DumpConfField(
                            const char*            pStructName,
                            const char*            pFieldName,
                            const uint32_t*        pField,
                            unsigned int           numFieldElts) const
{
    char fieldName[64] = "";

    if(numFieldElts > 1)
    {
        for(unsigned int i=0; i < numFieldElts; i++)
        {
            vibe_os_snprintf(fieldName, sizeof(fieldName), "%s.%s[%3d]", pStructName,  pFieldName, i);
            PLANE_TRC(TRC_ID_HQVDPLITE_DUMP_LLD_CMD, "%-32s = 0x%08X (%u)", fieldName, pField[i], pField[i]);
        }
    }
    else
    {
        vibe_os_snprintf(fieldName, sizeof(fieldName), "%s.%s", pStructName,  pFieldName);
        PLANE_TRC(TRC_ID_HQVDPLITE_DUMP_LLD_CMD, "%-32s = 0x%08X (%u)", fieldName, *pField, *pField);
    }
}

/*******************************************************************************
Name        : DumpConf
Description : Dump LLD/HQVDP conf to traces
Parameters  : pConf - [in] LLD/HQVDP conf
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::DumpConf(const hqvdp_lld_conf_s* pConf) const
{
    PLANE_TRCIN(TRC_ID_HQVDPLITE_DUMP_LLD_CMD, "");

    DUMP_CONF_FIELD(pConf, Top,   Config);
    DUMP_CONF_FIELD(pConf, Top,   MemFormat);
    DUMP_CONF_FIELD(pConf, Top,   CurrentLuma);
    DUMP_CONF_FIELD(pConf, Top,   CurrentEnhLuma);
    DUMP_CONF_FIELD(pConf, Top,   CurrentRightLuma);
    DUMP_CONF_FIELD(pConf, Top,   CurrentEnhRightLuma);
    DUMP_CONF_FIELD(pConf, Top,   CurrentChroma);
    DUMP_CONF_FIELD(pConf, Top,   CurrentEnhChroma);
    DUMP_CONF_FIELD(pConf, Top,   CurrentRightChroma);
    DUMP_CONF_FIELD(pConf, Top,   CurrentEnhRightChroma);
    DUMP_CONF_FIELD(pConf, Top,   OutputLuma);
    DUMP_CONF_FIELD(pConf, Top,   OutputChroma);
    DUMP_CONF_FIELD(pConf, Top,   LumaSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   LumaEnhSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   LumaRightSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   LumaEnhRightSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   ChromaSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   ChromaEnhSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   ChromaRightSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   ChromaEnhRightSrcPitch);
    DUMP_CONF_FIELD(pConf, Top,   LumaProcessedPitch);
    DUMP_CONF_FIELD(pConf, Top,   ChromaProcessedPitch);
    DUMP_CONF_FIELD(pConf, Top,   InputFrameSize);
    DUMP_CONF_FIELD(pConf, Top,   InputViewportOri);
    DUMP_CONF_FIELD(pConf, Top,   InputViewportOriRight);
    DUMP_CONF_FIELD(pConf, Top,   InputViewportSize);
    DUMP_CONF_FIELD(pConf, Top,   LeftViewBorderWidth);
    DUMP_CONF_FIELD(pConf, Top,   RightViewBorderWidth);
    DUMP_CONF_FIELD(pConf, Top,   LeftView3doffsetWidth);
    DUMP_CONF_FIELD(pConf, Top,   RightView3doffsetWidth);
    DUMP_CONF_FIELD(pConf, Top,   SideStripeColor);
    DUMP_CONF_FIELD(pConf, Top,   CrcResetCtrl);

    DUMP_CONF_FIELD(pConf, Vc1re, CtrlPrvCsdi);
    DUMP_CONF_FIELD(pConf, Vc1re, CtrlCurCsdi);
    DUMP_CONF_FIELD(pConf, Vc1re, CtrlNxtCsdi);
    DUMP_CONF_FIELD(pConf, Vc1re, CtrlCurFmd);
    DUMP_CONF_FIELD(pConf, Vc1re, CtrlNxtFmd);

    DUMP_CONF_FIELD(pConf, Fmd,   Config);
    DUMP_CONF_FIELD(pConf, Fmd,   ViewportOri);
    DUMP_CONF_FIELD(pConf, Fmd,   ViewportSize);
    DUMP_CONF_FIELD(pConf, Fmd,   NextNextLuma);
    DUMP_CONF_FIELD(pConf, Fmd,   NextNextRightLuma);
    DUMP_CONF_FIELD(pConf, Fmd,   NextNextNextLuma);
    DUMP_CONF_FIELD(pConf, Fmd,   NextNextNextRightLuma);
    DUMP_CONF_FIELD(pConf, Fmd,   ThresholdScd);
    DUMP_CONF_FIELD(pConf, Fmd,   ThresholdRfd);
    DUMP_CONF_FIELD(pConf, Fmd,   ThresholdMove);
    DUMP_CONF_FIELD(pConf, Fmd,   ThresholdCfd);

    DUMP_CONF_FIELD(pConf, Csdi,  Config);
    DUMP_CONF_FIELD(pConf, Csdi,  Config2);
    DUMP_CONF_FIELD(pConf, Csdi,  DcdiConfig);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevEnhLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevRightLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevEnhRightLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextEnhLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextRightLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextEnhRightLuma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevEnhChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevRightChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevEnhRightChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextEnhChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextRightChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  NextEnhRightChroma);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevMotion);
    DUMP_CONF_FIELD(pConf, Csdi,  PrevRightMotion);
    DUMP_CONF_FIELD(pConf, Csdi,  CurMotion);
    DUMP_CONF_FIELD(pConf, Csdi,  CurRightMotion);
    DUMP_CONF_FIELD(pConf, Csdi,  NextMotion);
    DUMP_CONF_FIELD(pConf, Csdi,  NextRightMotion);

    DUMP_CONF_FIELD(pConf, Hvsrc, HorPanoramicCtrl);
    DUMP_CONF_FIELD(pConf, Hvsrc, OutputPictureSize);
    DUMP_CONF_FIELD(pConf, Hvsrc, InitHorizontal);
    DUMP_CONF_FIELD(pConf, Hvsrc, InitVertical);
    DUMP_CONF_FIELD(pConf, Hvsrc, ParamCtrl);

    if(HQVDPLITE_DEBUG_DUMP_HQVDP_COEFS)
    {
        DUMP_CONF_FIELD(pConf, Hvsrc, YhCoef);
        DUMP_CONF_FIELD(pConf, Hvsrc, ChCoef);
        DUMP_CONF_FIELD(pConf, Hvsrc, YvCoef);
        DUMP_CONF_FIELD(pConf, Hvsrc, CvCoef);
    }
    DUMP_CONF_FIELD(pConf, Hvsrc, HoriShift);
    DUMP_CONF_FIELD(pConf, Hvsrc, VertShift);

    DUMP_CONF_FIELD(pConf, Iqi,   Config);
    DUMP_CONF_FIELD(pConf, Iqi,   DemoWindSize);
    DUMP_CONF_FIELD(pConf, Iqi,   PkConfig);
    DUMP_CONF_FIELD(pConf, Iqi,   Coeff0Coeff1);
    DUMP_CONF_FIELD(pConf, Iqi,   Coeff2Coeff3);
    DUMP_CONF_FIELD(pConf, Iqi,   Coeff4);
    DUMP_CONF_FIELD(pConf, Iqi,   PkLut);
    DUMP_CONF_FIELD(pConf, Iqi,   PkGain);
    DUMP_CONF_FIELD(pConf, Iqi,   PkCoringLevel);
    DUMP_CONF_FIELD(pConf, Iqi,   CtiConfig);
    DUMP_CONF_FIELD(pConf, Iqi,   LeConfig);
    if(HQVDPLITE_DEBUG_DUMP_HQVDP_COEFS)
    {
       DUMP_CONF_FIELD(pConf, Iqi,   LeLut);
    }
    DUMP_CONF_FIELD(pConf, Iqi,   ConBri);
    DUMP_CONF_FIELD(pConf, Iqi,   SatGain);
    DUMP_CONF_FIELD(pConf, Iqi,   PxfConf);
    DUMP_CONF_FIELD(pConf, Iqi,   DefaultColor);

    DUMP_CONF_FIELD(pConf, Out,   output_window_top_left_x);
    DUMP_CONF_FIELD(pConf, Out,   output_window_top_left_y);
    DUMP_CONF_FIELD(pConf, Out,   output_vtg_width);
    DUMP_CONF_FIELD(pConf, Out,   output_vtg_height);
    DUMP_CONF_FIELD(pConf, Out,   output_vtg_fps);

    DUMP_CONF_FIELD(pConf, VideoPlug, color_fill_enabled);

    PLANE_TRCOUT(TRC_ID_HQVDPLITE_DUMP_LLD_CMD, "");
}

/*******************************************************************************
Name        : ApplyConf
Description : Apply a HQVDP configuration to the LLD and program Video Plugs
Parameters  : pCurrNode      - [in] Node to display
              pDisplayInfo   - [in] Display info
              pHqvdpConf     - [in] LLD-ready HQVDP configuration
Assumptions : LLD session is open and pHqvdpConf is already filled accordingly
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::ApplyConf(
                            const CDisplayNode*             pCurrNode,
                            const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                            const hqvdp_lld_conf_s*         pHqvdpConf)
{
    PLANE_TRCIN(TRC_ID_HQVDPLITE, "");

    hqvdp_lld_compo_setup_s compoSetup[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE];
    hqvdp_lld_error         err              = HQVDP_LLD_NO_ERR;
    bool                    isPlaneVisible   = true;
    uint32_t                mixerBits        = 0;
    int                     nbHwUsed         = 0;

    if( IS_TRC_ID_ENABLED(TRC_ID_HQVDPLITE_DUMP_LLD_CMD) )
    {
        DumpConf(pHqvdpConf);
    }

    if (m_hideRequestedByTheApplication && (m_eHideMode == PLANE_HIDE_BY_MASKING))
    {
        // Plane hidden by masking. The plane HW will work normally but the VideoPlug will sent transparent pixels
        PLANE_TRC(TRC_ID_HQVDPLITE, "Plane hidden by masking" );
        isPlaneVisible = false;
    }

    PLANE_TRC(TRC_ID_HQVDPLITE, "Input picture: %s, %u lines skipped",
                                pDisplayInfo->m_isSecondaryPictureSelected ? "secondary" : "primary", pDisplayInfo->m_srcLinesSkipped);
    PLANE_TRC(TRC_ID_HQVDPLITE, "Input rectangle: (%d,%d) (%ux%u)",
                                pDisplayInfo->m_selectedPicture.srcFrameRect.x,     pDisplayInfo->m_selectedPicture.srcFrameRect.y,
                                pDisplayInfo->m_selectedPicture.srcFrameRect.width, pDisplayInfo->m_selectedPicture.srcFrameRect.height);
    PLANE_TRC(TRC_ID_HQVDPLITE, "Output rectangle: (%d,%d) (%ux%u)",
                                pDisplayInfo->m_dstFrameRect.x,     pDisplayInfo->m_dstFrameRect.y,
                                pDisplayInfo->m_dstFrameRect.width, pDisplayInfo->m_dstFrameRect.height);

    // Check that Proc Clock is really enabled before any call to a LLD function
    CheckProcClockActivation();

    // Apply conf to HQVDP HW
    err = hqvdp_lld_set_conf( m_lldHandle,   // [in]
                              pHqvdpConf,    // [in]
                              &nbHwUsed,     // [out]
                              compoSetup);   // [out]

    if(err != HQVDP_LLD_NO_ERR)
    {
        PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_set_conf() failed, err=%d", err);
        PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
        return;
    }

    PLANE_TRC(TRC_ID_HQVDPLITE, "Pixel bit depth = %u", pDisplayInfo->m_selectedPicture.pixelDepth);

    // Apply the video plugs setups
    for(int i = 0; i < nbHwUsed; i++)
    {
        if(compoSetup[i].is_active)
        {
            // LLD requests to program this video plug
            // Even though it is not clearly visible here, the video_plug_handle returned by the LLD is one of m_videoPlug
            CVideoPlug*     pVideoPlug = static_cast<CVideoPlug*>(compoSetup[i].plug_setup.video_plug_handle);
            uint32_t        transparency = STM_PLANE_TRANSPARENCY_OPAQUE;
            VideoPlugSetup  videoPlugSetup;
            stm_viewport_t  viewport;
            VideoPlugCrop   crop;
            bool            bOk  = true;

            // Prepare destination viewport settings
            viewport.startPixel = compoSetup[i].plug_setup.top_left_x;
            viewport.stopPixel  = compoSetup[i].plug_setup.bottom_right_x;
            viewport.startLine  = compoSetup[i].plug_setup.top_left_y;
            viewport.stopLine   = compoSetup[i].plug_setup.bottom_right_y;

            // Prepare cropping settings
            if(pDisplayInfo->m_selectedPicture.srcFrameRect.height < 1088)
            {
                crop.left  = compoSetup[i].plug_setup.crop_left;
                crop.right = compoSetup[i].plug_setup.crop_right;
            }
            else
            {
                // compoSetup[0].plug_setup.crop_right and compoSetup[1].plug_setup.crop_left are
                // predefined below for different resolutions by lld:
                //                                                    4k      1080p/720p      576p
                // crop.right = compoSetup[0].plug_setup.crop_right:   8         4             6
                // crop.left  = compoSetup[1].plug_setup.crop_left:    8         4             6
                //
                // To resolve the vertical line appeared during playing 4K interlaced stream by
                // reducing 4 pixels on both crop.right on LEFT HQVDPLITE and crop.left on right HQVDPLITE:
                //                                                         4k      1080p/720p      576p
                // crop.right = compoSetup[0].plug_setup.crop_right - 4:    4         0             2
                // crop.left  = compoSetup[1].plug_setup.crop_left - 4:     4         0             2
                //
                if(i == 0)
                {
                    crop.left  = compoSetup[i].plug_setup.crop_left;
                    crop.right = compoSetup[i].plug_setup.crop_right - 4;
                }
                else
                {
                    crop.left  = compoSetup[i].plug_setup.crop_left - 4;
                    crop.right = compoSetup[i].plug_setup.crop_right;
                }
            }

            // Compute transparency parameter
            if(m_TransparencyState == CONTROL_ON)
            {
                transparency = m_Transparency;
            }
            else if(pCurrNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_CONST_ALPHA)
            {
                transparency = pCurrNode->m_bufferDesc.src.ulConstAlpha;
            }

            PLANE_TRC(TRC_ID_HQVDPLITE, "HQVDP[%d] Destination Viewport: VPO (%d, %d) VPS (%d, %d)", i,
                                        viewport.startPixel, viewport.startLine,
                                        viewport.stopPixel,  viewport.stopLine);
            PLANE_TRC(TRC_ID_HQVDPLITE, "HQVDP[%d] Crop: Left %u , Right %u", i, crop.left, crop.right);
            PLANE_TRC(TRC_ID_HQVDPLITE, "HQVDP[%d] Transparency   %u", i, transparency);
            PLANE_TRC(TRC_ID_HQVDPLITE, "HQVDP[%d] isPlaneVisible %d", i, isPlaneVisible);
            PLANE_TRC(TRC_ID_HQVDPLITE, "HQVDP[%d] PixelRepeat    %d", i, compoSetup[i].plug_setup.pixel_repeat);
            PLANE_TRC(TRC_ID_HQVDPLITE, "HQVDP[%d] Mixer bit      0x%X", i, compoSetup[i].mixer_bit);

            // Create video plug setups
            bOk = pVideoPlug->CreatePlugSetup(videoPlugSetup,
                                              &pCurrNode->m_bufferDesc,
                                              viewport,
                                              pDisplayInfo->m_selectedPicture.colorFmt,
                                              transparency,
                                              &m_ColorKeyConfig,
                                              &crop,
                                              compoSetup[i].plug_setup.pixel_repeat,
                                              m_outputInfo.outputColorSpace);
            if(!bOk)
            {
                PLANE_TRC(TRC_ID_ERROR, "HQVDP[%d] Failed to create the PlugSetup!", i);
                PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
                return;
            }

            // Apply video plus setup
            pVideoPlug->WritePlugSetup(videoPlugSetup, isPlaneVisible);

            // This HQVDP instance is used => set corresponding mixer bit
            mixerBits |= compoSetup[i].mixer_bit;
        }
        else
        {
            // This HQVDP instance is not used => clear corresponding mixer bit
            mixerBits &= ~compoSetup[i].mixer_bit;
        }
    } // for(nbHwUsed)

    if (m_pOutput != NULL)
    {
        PLANE_TRC(TRC_ID_HQVDPLITE, "Output %s, enabling mixer bits 0x%X", m_pOutput->GetName(), mixerBits);
        m_pOutput->SetMixerMap(this, mixerBits);
    }

    PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
}


/*******************************************************************************
Name        : RecoverFromEndOfProcessingError
Description : When 'End Of Processing Error' situation occurs, next frame candidate
              must be skipped and this function must be called instead.
              In some situations, the HQVDP can fail to process a picture in the
              VSYNC time frame. When 2 concurrent HQVDPs are running, this can
              lead to an inter-blocking situation where 'End Of Processing Error'
              is raised forever by FWs. The HW/FW cannot recover on its own, thus
              the intent of this function is to recover from this situation.
Parameters  : pAValidNode - [in] A Node that contains valid data
Assumptions : LLD session is open
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::RecoverFromEndOfProcessingError(const CDisplayNode* pAValidNode)
{
    PLANE_TRCIN(TRC_ID_MAIN_INFO, "");

    // Stop the HQVDP so that nothing is processed at next VSync
    StopHqvdp();

    // Clear the viewports of all associated video plugs
    for(unsigned int i=0; i < N_ELEMENTS(m_videoPlug); i++)
    {
        if(m_videoPlug[i])
        {
            // NB: Viewport is set to zero so that _no_ pixels are pulled
            // by the video plugs. Other parameters are not relevant.
            const stm_viewport_t        viewport        = { 0, 0, 0, 0 };
            const VideoPlugCrop         crop            = { 0, 0 };
            const stm_display_buffer_t* pBufferDesc     = &pAValidNode->m_bufferDesc;
            VideoPlugSetup              videoPlugSetup;

            PLANE_TRC(TRC_ID_MAIN_INFO, "Closing VideoPlug[%d] viewport", i);
            m_videoPlug[i]->CreatePlugSetup(videoPlugSetup,
                                            pBufferDesc,
                                            viewport,
                                            SURF_YCbCr420R2B,
                                            0,
                                            NULL,
                                            &crop,
                                            false);
            m_videoPlug[i]->WritePlugSetup(videoPlugSetup, false);
        }
    }

    PLANE_TRCOUT(TRC_ID_MAIN_INFO, "");
}


/*******************************************************************************
Name        : StopHqvdp
Description : Instructs HQVDP LLD/HW to move to "stopped" state.
Parameters  : None
Assumptions : LLD session is open
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::StopHqvdp()
{
    PLANE_TRCIN(TRC_ID_HQVDPLITE, "");

    if(m_lldHandle != 0)
    {
        // Check that Proc Clock is really enabled before any call to a LLD function
        CheckProcClockActivation();

        const hqvdp_lld_error err = hqvdp_lld_stop(m_lldHandle);

        if(err != HQVDP_LLD_NO_ERR)
        {
            PLANE_TRC(TRC_ID_ERROR, "hqvdp_lld_stop() failed, err=%d", err);
            PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
            return;
        }

        PLANE_TRC(TRC_ID_MAIN_INFO, "HQVDP HW stopped");
    }

    PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
}


/*******************************************************************************
Name        : PrepareTopParams
Description : Prepare the Top part of the HQVDP conf.
Parameters  : pCurrNode     - [in]  Current node to display
              pDisplayInfo  - [in]  Display info
              pTopParams    - [out] TOP parameters to be prepared
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::PrepareTopParams(
                                    const CDisplayNode*             pCurrNode,
                                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                    HQVDPLITE_TOP_Params_t*         pTopParams) const
{
    uint32_t srcPictureHorizontalSize;
    uint32_t srcPictureVerticalSize;
    uint32_t srcPicturePitch;
    uint32_t origX;
    uint32_t origY;
    uint32_t viewportHeight;
    uint32_t viewportWidth;

    /* Clear the content of the Top params */
    vibe_os_zero_memory(pTopParams, sizeof(*pTopParams));

    /**************************/
    /* Top parameter : CONFIG */
    /**************************/
    /* Top parameter : CONFIG  : Progressive bit */
    if(!pDisplayInfo->m_isSrcInterlaced)
    {
        pTopParams->Config |= HQVDPLITE_CONFIG_PROGRESSIVE;
    }
    else
    {
        pTopParams->Config &= ~HQVDPLITE_CONFIG_PROGRESSIVE;
        /* Top parameter : CONFIG  : Top or Bottom bit */
        /* Select Field */
        if(pCurrNode->m_srcPictureType == GNODE_PROGRESSIVE || pCurrNode->m_srcPictureType == GNODE_TOP_FIELD)
        {
            pTopParams->Config |= HQVDPLITE_CONFIG_TOP_NOT_BOT;
        }
        else
        {
            pTopParams->Config &= ~HQVDPLITE_CONFIG_TOP_NOT_BOT;
        }
    }

    /* Top parameter : CONFIG  : IT EOP bit */
    pTopParams->Config &= ~HQVDPLITE_CONFIG_IT_EOP;

    /* Top parameter : CONFIG  : Pass Thru bit */
    pTopParams->Config &= ~HQVDPLITE_CONFIG_PASS_THRU;

    /* Top parameter : CONFIG  : Nb of field bits = 3 now (could be changed to 5) */
    pTopParams->Config |= HQVDPLITE_CONFIG_NB_OF_FIELD_3;

    /******************************/
    /* Top parameter : MEM_FORMAT */
    /******************************/
    pTopParams->MemFormat = 0;
    /* Top parameter : MEM_FORMAT  : Raster or macroblock Input Format */
    switch (pDisplayInfo->m_selectedPicture.colorFmt)
    {
        case SURF_YCBCR420MB:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_420_MACROBLOCK;
            break;

        case SURF_YCbCr420R2B:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_420_RASTER_DUAL_BUFFER;
            break;

        case SURF_YCbCr422R2B:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_422_RASTER_DUAL_BUFFER;
            break;

        case SURF_YCbCr422R2B10:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_422_RASTER_DUAL_BUFFER;
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_PIX10BIT_NOT_8BIT;
            break;

        case SURF_YCbCr420R2B10:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_420_RASTER_DUAL_BUFFER;
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_PIX10BIT_NOT_8BIT;
            break;

        case SURF_CRYCB888:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_444_RASTER_SINGLE_BUFFER;
            break;

        case SURF_CRYCB101010:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_444_RASTER_SINGLE_BUFFER;
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_PIX10BIT_NOT_8BIT;
            break;

        case SURF_ACRYCB8888:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_444_RSB_WITH_ALPHA;
            break;

        default:
            // Unsupported format
            return false;
    }

    /* Top parameter : MEM_FORMAT  : Output format is set Raster 444 (useful only in MEM to MEM configuration) */
    pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_OUTPUTFORMAT_444;

    /* Top parameter : MEM_FORMAT  : Output  Alpha is useful only in MEM to MEM configuration */
    pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_OUTPUTALPHA;

    /* Top parameter : MEM_FORMAT  : Setting Mem to TV (always TRUE) */
    pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_MEMTOTV;

    /* Top parameter : MEM_FORMAT  : Setting Mem to Mem (always FALSE) */
    pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_MEMTOMEM;

    /* Top parameter : MEM_FORMAT  : Pix8to10 lsb bits (10 by default) Luma */
    pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_PIX8TO10LSB_Y;

    /* Top parameter : MEM_FORMAT  : Pix8to10 lsb bits (0 by default) Chroma */
    pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_PIX8TO10LSB_C;

    /* Top parameter : MEM_FORMAT  : setting for output 3D format based on VTG mode set */
    switch (m_outputInfo.currentMode.mode_params.flags)
    {
        case STM_MODE_FLAGS_NONE:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_2D;
            break;
        case STM_MODE_FLAGS_3D_FRAME_PACKED:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_FP;
            break;
        case STM_MODE_FLAGS_3D_SBS_HALF:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_SBS;
            break;
        case STM_MODE_FLAGS_3D_TOP_BOTTOM:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_TAB;
            break;
        default:/* Unsupported formats */
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_OUTPUT_3DFORMAT_2D;
            break;
    }

    /* Top parameter : MEM_FORMAT  :  Input 3D format (taken from stream info, unless it is forced by appli) */
    switch(pCurrNode->m_bufferDesc.src.config_3D.formats)
    {
        case FORMAT_3D_NONE:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_2D;
            break;
        case FORMAT_3D_SBS_HALF:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_SBS;
            break;
        case FORMAT_3D_STACKED_HALF:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_TAB;
            break;
        case FORMAT_3D_FRAME_SEQ:
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_FP;
            break;
        default:
            /* Unsupported formats */
            pTopParams->MemFormat |= HQVDPLITE_MEM_FORMAT_INPUT_3DFORMAT_2D;
            break;
    }

    /******************************************************************/
    /* Top parameter : CURRENT_LUMA and CURRENT_CHROMA source address */
    /*                 (will be set by PresentDisplayNode())          */
    /******************************************************************/
    pTopParams->CurrentLuma   = 0;
    pTopParams->CurrentChroma = 0;

    /***************************************************************************************************/
    /* Top parameter : CURRENT_RIGHT_LUMA and CURRENT_RIGHT_CHROMA source address for 3D right support */
    /*                 (will be set by PresentDisplayNode())                                           */
    /***************************************************************************************************/
    pTopParams->CurrentRightLuma   = 0;
    pTopParams->CurrentRightChroma = 0;

    /*******************************************************/
    /* Top parameter : LUMA_SRC_PITCH and CHROMA_SRC_PITCH */
    /*******************************************************/

    /* Adapting Source picture size to stream 3D characteristics for 3D support */
    switch(pCurrNode->m_bufferDesc.src.config_3D.formats)
    {
        case FORMAT_3D_FRAME_SEQ:
            srcPictureHorizontalSize = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
            srcPictureVerticalSize   = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
            break;
        case FORMAT_3D_SBS_HALF:
            srcPictureHorizontalSize = pDisplayInfo->m_selectedPicture.srcFrameRect.width / 2;
            srcPictureVerticalSize   = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
            break;
        case FORMAT_3D_STACKED_HALF:
            srcPictureHorizontalSize = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
            srcPictureVerticalSize   = pDisplayInfo->m_selectedPicture.srcFrameRect.height / 2;
            break;
        default:
            /* Unsupported formats */
            srcPictureHorizontalSize = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
            srcPictureVerticalSize   = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
            break;
    }

    srcPicturePitch = pDisplayInfo->m_selectedPicture.srcFramePitch;

    if(pDisplayInfo->m_isSrcInterlaced)
    {
        srcPictureVerticalSize /= 2;
        srcPicturePitch        *= 2;
    }

    switch (pDisplayInfo->m_selectedPicture.colorFmt)
    {
        case SURF_CRYCB888:
        case SURF_CRYCB101010:
        case SURF_ACRYCB8888:
        {
            pTopParams->LumaSrcPitch = srcPicturePitch;
            /* ChromaSrcPitch is not needed in that use case because luma/chroma are together in a single buffer */
            pTopParams->ChromaSrcPitch = srcPicturePitch;

            /**************************************/
            /* Top parameter : INPUT_FRAME_SIZE   */
            /**************************************/
            SetBitField(pTopParams->InputFrameSize, TOP_INPUT_FRAME_SIZE_WIDTH,  srcPictureHorizontalSize);
            SetBitField(pTopParams->InputFrameSize, TOP_INPUT_FRAME_SIZE_HEIGHT, srcPictureVerticalSize);
            break;
        }
        case SURF_YCbCr420R2B:
        case SURF_YCbCr422R2B:
        case SURF_YCbCr420R2B10:
        case SURF_YCbCr422R2B10:
        {
            /* 4:2:0 Raster Dual Buffer */
            pTopParams->LumaSrcPitch   = srcPicturePitch;
            pTopParams->ChromaSrcPitch = srcPicturePitch;

            /**************************************/
            /* Top parameter : INPUT_FRAME_SIZE   */
            /**************************************/
            SetBitField(pTopParams->InputFrameSize, TOP_INPUT_FRAME_SIZE_WIDTH,  srcPictureHorizontalSize);
            SetBitField(pTopParams->InputFrameSize, TOP_INPUT_FRAME_SIZE_HEIGHT, srcPictureVerticalSize);
            break;
        }
        case SURF_YCBCR420MB:
        {
            /* Macroblock format: pitch is expressed in number of lines */
            uint32_t pitch = pDisplayInfo->m_srcLinesSkipped + 1;

            pitch *= pDisplayInfo->m_isSrcInterlaced ? 2 : 1;

            SetBitField(pTopParams->LumaSrcPitch,   TOP_LUMA_SRC_PITCH_MBSRCPITCH,   pitch);
            SetBitField(pTopParams->ChromaSrcPitch, TOP_CHROMA_SRC_PITCH_MBSRCPITCH, pitch);

            /**************************************/
            /* Top parameter : INPUT_FRAME_SIZE   */
            /**************************************/
            // With MB format, InputFrameSize corresponds to the full picture size in memory
            // whereas InputViewportOri and InputViewportSize specify the visible part
            SetBitField(pTopParams->InputFrameSize, TOP_INPUT_FRAME_SIZE_WIDTH,  pDisplayInfo->m_selectedPicture.width);
            SetBitField(pTopParams->InputFrameSize, TOP_INPUT_FRAME_SIZE_HEIGHT, pDisplayInfo->m_selectedPicture.height);
            break;
        }
        default:
        {
            // Unsupported format
            return false;
        }
    }

    /***********************************/
    /* Top parameter : RIGHT VIEW PITCH */
    /***********************************/
    /* Getting 3D Right view pitch from left view */
    pTopParams->LumaRightSrcPitch   = pTopParams->LumaSrcPitch;
    pTopParams->ChromaRightSrcPitch = pTopParams->ChromaSrcPitch;

    /***********************************/
    /* Top parameter : PROCESSED_PITCH */
    /***********************************/
    /* Only used when the IP is operating Mem to Mem so it is not used */
    pTopParams->LumaProcessedPitch = srcPicturePitch;
    /* Chroma pitch is useful only when output format is 420 R2B in this case, ChromaPitch can be equal to LumaPitch */
    pTopParams->ChromaProcessedPitch = srcPicturePitch;

    /**************************************/
    /* Top parameter : INPUT_VIEWPORT_ORI */
    /**************************************/
    origY = pDisplayInfo->m_selectedPicture.srcFrameRect.y / 16;
    origX = pDisplayInfo->m_selectedPicture.srcFrameRect.x / 16;
    pTopParams->InputViewportOri  =   ((origY << TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_SHIFT) & TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_MASK)
                                    | ((origX << TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_SHIFT) & TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_MASK);

    /********************************************/
    /* Top parameter : INPUT_VIEWPORT_ORI_RIGHT */
    /********************************************/
    /* Copying exact same parameters as Left view offset. Offset is aware about  3D type */
    pTopParams->InputViewportOriRight =  ((origY << TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_SHIFT) & TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_MASK)
                                       | ((origX << TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_SHIFT) & TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_MASK);

    /***************************************/
    /* Top parameter : INPUT_VIEWPORT_SIZE */
    /***************************************/
    switch(pCurrNode->m_bufferDesc.src.config_3D.formats)
    {
        case FORMAT_3D_NONE:
        case FORMAT_3D_FRAME_SEQ:
        {
            viewportHeight = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
            viewportWidth  = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
            break;
        }
        case FORMAT_3D_SBS_HALF:
        {
            viewportHeight = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
            viewportWidth  = pDisplayInfo->m_selectedPicture.srcFrameRect.width / 2;
            break;
        }
        case FORMAT_3D_STACKED_HALF:
        {
            viewportHeight = pDisplayInfo->m_selectedPicture.srcFrameRect.height / 2;
            viewportWidth  = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
            break;
        }
        default:
        {
            /* Unsupported formats */
            viewportHeight = pDisplayInfo->m_selectedPicture.srcFrameRect.height;
            viewportWidth  = pDisplayInfo->m_selectedPicture.srcFrameRect.width;
            break;
        }
    }
    pTopParams->InputViewportSize =   ((viewportHeight << TOP_INPUT_VIEWPORT_SIZE_HEIGHT_SHIFT) & TOP_INPUT_VIEWPORT_SIZE_HEIGHT_MASK)
                                    | ((viewportWidth  << TOP_INPUT_VIEWPORT_SIZE_WIDTH_SHIFT ) & TOP_INPUT_VIEWPORT_SIZE_WIDTH_MASK );

    /****************************/
    /* Top parameter : CRC_CTRL */
    /****************************/
    pTopParams->CrcResetCtrl |= HQVDPLITE_CRC_RESET_ALL;

    return true;
}


/*******************************************************************************
Name        : PrepareVc1Params
Description : Prepare the VC1 part of the HQVDP conf.
Parameters  : pVc1ReParams - [out] VC1 parameters to be prepared
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::PrepareVc1Params(HQVDPLITE_VC1RE_Params_t* pVc1ReParams) const
{
    /******************/
    /* VC1 parameters */
    /******************/

    /* Not implemented yet */
    SetBitField(pVc1ReParams->CtrlPrvCsdi, VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_LUMA,   0);
    SetBitField(pVc1ReParams->CtrlPrvCsdi, VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_CHROMA, 0);
    SetBitField(pVc1ReParams->CtrlPrvCsdi, VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_LUMA,     0);
    SetBitField(pVc1ReParams->CtrlPrvCsdi, VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_CHROMA,   0);

    SetBitField(pVc1ReParams->CtrlCurCsdi, VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_LUMA,   0);
    SetBitField(pVc1ReParams->CtrlCurCsdi, VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_CHROMA, 0);
    SetBitField(pVc1ReParams->CtrlCurCsdi, VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_LUMA,     0);
    SetBitField(pVc1ReParams->CtrlCurCsdi, VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_CHROMA,   0);

    SetBitField(pVc1ReParams->CtrlNxtCsdi, VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_LUMA,   0);
    SetBitField(pVc1ReParams->CtrlNxtCsdi, VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_CHROMA, 0);
    SetBitField(pVc1ReParams->CtrlNxtCsdi, VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_LUMA,     0);
    SetBitField(pVc1ReParams->CtrlNxtCsdi, VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_CHROMA,   0);

    SetBitField(pVc1ReParams->CtrlCurFmd,  VC1RE_CTRL_CUR_FMD_VC1RE_ENABLE_LUMA,    0);
    SetBitField(pVc1ReParams->CtrlCurFmd,  VC1RE_CTRL_CUR_FMD_VC1RE_COEF_LUMA,      0);

    SetBitField(pVc1ReParams->CtrlNxtFmd,  VC1RE_CTRL_NXT_FMD_VC1RE_ENABLE_LUMA,    0);
    SetBitField(pVc1ReParams->CtrlNxtFmd,  VC1RE_CTRL_NXT_FMD_VC1RE_COEF_LUMA,      0);

    return true;
}

/*******************************************************************************
Name        : PrepareFmdParams
Description : Prepare the FMD part of the HQVDP conf.
Parameters  : pFmdParams - [out] FMD parameters to be prepared
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::PrepareFmdParams(HQVDPLITE_FMD_Params_t* pFmdParams) const
{
    /* Clear the content of the FMD params */
    vibe_os_zero_memory(pFmdParams, sizeof(*pFmdParams));

    /******************/
    /* FMD parameters */
    /******************/
    if(m_bUseFMD)
    {
        /* Not yet implemented */
    }

    return true;
}

/*******************************************************************************
Name        : PrepareCsdiParams
Description : Prepare the CSDI part of the HQVDP conf.
Parameters  : pCsdiParams - [out] CSDI parameters to be prepared
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::PrepareCsdiParams(HQVDPLITE_CSDI_Params_t* pCsdiParams) const
{
    /* Clear the content of the CSDI params */
    vibe_os_zero_memory(pCsdiParams, sizeof(*pCsdiParams));

    /*****************/
    /* CSDi : CONFIG */
    /*****************/

    SetBitField(pCsdiParams->Config, CSDI_CONFIG_MOTION_REDUCED, HQVDPLITE_DEI_CTL_8_BITS_MOTION);

    SetBitField(pCsdiParams->Config2, CSDI_CONFIG2_INFLEXION_MOTION, HQVDPLITE_CSDI_CONFIG2_INFLEXION_MOTION);
    SetBitField(pCsdiParams->Config2, CSDI_CONFIG2_FIR_LENGTH,       HQVDPLITE_CSDI_CONFIG2_FIR_LENGTH);
    SetBitField(pCsdiParams->Config2, CSDI_CONFIG2_MOTION_FIR,       HQVDPLITE_CSDI_CONFIG2_MOTION_FIR);
    SetBitField(pCsdiParams->Config2, CSDI_CONFIG2_MOTION_CLOSING,   HQVDPLITE_CSDI_CONFIG2_MOTION_CLOSING);
    SetBitField(pCsdiParams->Config2, CSDI_CONFIG2_DETAILS,          HQVDPLITE_CSDI_CONFIG2_DETAILS);
    SetBitField(pCsdiParams->Config2, CSDI_CONFIG2_CHROMA_MOTION,    HQVDPLITE_CSDI_CONFIG2_CHROMA_MOTION);
    SetBitField(pCsdiParams->Config2, CSDI_CONFIG2_ADAPTIVE_FADING,  HQVDPLITE_CSDI_CONFIG2_ADAPTIVE_FADING);

    SetBitField(pCsdiParams->DcdiConfig, CSDI_DCDI_CONFIG_ALPHA_LIMIT,            HQVDPLITE_CSDI_DCDI_CONFIG_ALPHA_LIMIT);
    SetBitField(pCsdiParams->DcdiConfig, CSDI_DCDI_CONFIG_MEDIAN_MODE,            HQVDPLITE_CSDI_DCDI_CONFIG_MEDIAN_MODE);
    SetBitField(pCsdiParams->DcdiConfig, CSDI_DCDI_CONFIG_WIN_CLAMP_SIZE,         HQVDPLITE_CSDI_DCDI_CONFIG_WIN_CLAMP_SIZE);
    SetBitField(pCsdiParams->DcdiConfig, CSDI_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP, HQVDPLITE_CSDI_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP);
    SetBitField(pCsdiParams->DcdiConfig, CSDI_DCDI_CONFIG_ALPHA_MEDIAN_EN,        HQVDPLITE_CSDI_DCDI_CONFIG_ALPHA_MEDIAN_EN);

    return true;
}


/*******************************************************************************
Name        : SelectDeiMode
Description : Selects a DEI mode as follows:
              - Are Prev, and Next nodes available?  If not => DEI mode DIRECTIONAL
              - Are polarities ok?                   If not => DEI mode DIRECTIONAL
              - Are characteristics changed?         If not => DEI mode DIRECTIONAL
              - Then, DEI mode 3D
Parameters  : pDisplayInfo [in] Display info
              pPrevNode    [in] Previous Node
              pCurNode     [in] Current Node
              pNextNode    [in] Next Node
Assumptions :
Limitations :
Returns     : return HQVDPLITE_DEI_MODE_xx
*******************************************************************************/
CHqvdpLitePlaneV2::HqvdpLiteDeiMode_e CHqvdpLitePlaneV2::SelectDeiMode(
                                        const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                        const CDisplayNode*             pPrevNode,
                                        const CDisplayNode*             pCurNode,
                                        const CDisplayNode*             pNextNode) const
{
    if(!pDisplayInfo->m_isSrcInterlaced)
    {
        // Progressive source: DEI Off
        return HQVDPLITE_DEI_MODE_OFF;
    }

    if(HQVDPLITE_DEBUG_FORCE_SPATIAL_DEINTERLACING)
    {
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(!m_bIsTemporalDeiAllowed)
    {
        // Temporal DEI is not allowed on this plane, use directional
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "3D DEI mode not allowed on this plane: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(!pDisplayInfo->m_is3DDeinterlacingPossible)
    {
        /* STBus DataRate calculation have shown that we can't do more than DI mode */
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "3D DEI mode not possible due to STBus DataRate: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(pDisplayInfo->m_isSecondaryPictureSelected)
    {
        /* When Decimated picture is used, Directional Deinterlacing is recommended to minimize the STBus traffic */
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "Secondary picture selected: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(!pPrevNode || !pCurNode || !pNextNode)
    {
        /* A field is missing so temporal deinterlacing is not possible */
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "A field is missing: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(pCurNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_CHARACTERISTIC_DISCONTINUITY)
    {
        /* The source characteristics have changed so temporal deinterlacing is not possible */
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "Cur Field characteristics have changed: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(pPrevNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_CHARACTERISTIC_DISCONTINUITY)
    {
        /* The source characteristics have changed so temporal deinterlacing is not possible */
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "Prev Field characteristics have changed: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(pNextNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_CHARACTERISTIC_DISCONTINUITY)
    {
        /* The source characteristics have changed so temporal deinterlacing is not possible */
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "Next Field characteristics have changed: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(pCurNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY)
    {
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "Cur Field has a temporal discontinuity compare to Prev Field: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    if(pNextNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY)
    {
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "Next Field has a temporal discontinuity compare to Cur Field: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }

    // NB: No need to check temporal discontinuity on pPrevNode because it tells if there is a discontinuity
    //     between PreviousNode and Pre-PreviousNode. So it doesn't matter here.

    /* check if the source fields have alternate polarities (TBT or BTB) */
    if(   (pPrevNode->m_srcPictureType == GNODE_TOP_FIELD   )
       && (pCurNode->m_srcPictureType  == GNODE_BOTTOM_FIELD)
       && (pNextNode->m_srcPictureType == GNODE_TOP_FIELD   ))
    {
        return HQVDPLITE_DEI_MODE_3D;
    }
    else if(   (pPrevNode->m_srcPictureType == GNODE_BOTTOM_FIELD)
            && (pCurNode->m_srcPictureType  == GNODE_TOP_FIELD   )
            && (pNextNode->m_srcPictureType == GNODE_BOTTOM_FIELD))
    {
        return HQVDPLITE_DEI_MODE_3D;
    }
    else
    {
        /* The 3 fields don't have alternate polarities so temporal deinterlacing is not possible */
        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "The 3 fields don't have alternate polarities: Use DI mode");
        return HQVDPLITE_DEI_MODE_DI;
    }
}

/*******************************************************************************
Name        : SetDeiNextFieldAddresses
Description : Set the Luma and Chroma buffers addresses of next DEI field
Parameters  : pNextNode    - [in]  Next node
              pDisplayInfo - [in]  Display info
              pCsdiParams  - [out] CSDI parameter structure to be updated
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::SetDeiNextFieldAddresses(
                                const CDisplayNode*             pNextNode,
                                const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                HQVDPLITE_CSDI_Params_t*        pCsdiParams) const
{
    pCsdiParams->NextLuma = 0;
    pCsdiParams->NextChroma = 0;

    if(!pNextNode)
        return;

    if(!(pNextNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_CHARACTERISTIC_DISCONTINUITY))
    {
        SetPictureBaseAddresses(pNextNode,
                                pDisplayInfo,
                                &pCsdiParams->NextLuma,
                                &pCsdiParams->NextChroma);
    }
}

/*******************************************************************************
Name        : SetDeiPreviousFieldAddresses
Description : Set the Luma and Chroma buffers addresses of previous DEI field
Parameters  : pNextNode    - [in]  Next node
              pDisplayInfo - [in]  Display info
              pCsdiParams  - [out] CSDI parameter structure to be updated
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
void CHqvdpLitePlaneV2::SetDeiPreviousFieldAddresses(
                                    const CDisplayNode*             pPrevNode,
                                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                    HQVDPLITE_CSDI_Params_t*        pCsdiParams) const
{
    pCsdiParams->PrevLuma = 0;
    pCsdiParams->PrevChroma = 0;

    if(!pPrevNode)
        return;

    if(!(pPrevNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_CHARACTERISTIC_DISCONTINUITY))
    {
        SetPictureBaseAddresses(pPrevNode,
                                pDisplayInfo,
                                &pCsdiParams->PrevLuma,
                                &pCsdiParams->PrevChroma);
    }
    else
    {
        pCsdiParams->PrevLuma   = pCsdiParams->NextLuma;
        pCsdiParams->PrevChroma = pCsdiParams->NextChroma;
    }
}

/*******************************************************************************
Name        : Set3dDeiConfiguration
Description : Set 3D DEI config
Parameters  : deiCtl - [in/out] computed DEI ctrl register value
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
inline void CHqvdpLitePlaneV2::Set3dDeiConfiguration(uint32_t& deiCtl) const
{
    SetBitField(deiCtl, CSDI_CONFIG_ENABLE_RECURSIVE_DIR, HQVDPLITE_DEI_CTL_DIR_RECURSIVITY_ON);
    SetBitField(deiCtl, CSDI_CONFIG_KCOR,                 HQVDPLITE_DEI_CTL_BEST_KCOR_VALUE);
    SetBitField(deiCtl, CSDI_CONFIG_T_DETAIL,             HQVDPLITE_DEI_CTL_BEST_DETAIL_VALUE);
    SetBitField(deiCtl, CSDI_CONFIG_KMOV,                 HQVDPLITE_DEI_CTL_BEST_KMOV_VALUE);
}


/*******************************************************************************
Name        : SetDeiMainMode
Description : Compute and set main mode for DEI control
Parameters  : deiCtl - [out] DEI control control value
              mode   - [in]  DEI mode to be set
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
inline void CHqvdpLitePlaneV2::SetDeiMainMode(
                                    uint32_t&   deiCtl,
                                    uint32_t    mode) const
{
    SetBitField(deiCtl, CSDI_CONFIG_MAIN_MODE, mode);
}


/*******************************************************************************
Name        : SetDeiMotionState
Description : Compute and set motion state for DEI control
Parameters  : deiCtl      - [out] DEI control control value
              motionState - [in]  Motion state to be set
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
inline void CHqvdpLitePlaneV2::SetDeiMotionState(
                                    uint32_t&                   deiCtl,
                                    HqvdpLiteDeiMotionState_e   motionState) const
{
    /* Motion states values are the same as HQVDPLITE MD mode configuration parameters */
    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "motion state = %d", motionState);
    SetBitField(deiCtl, CSDI_CONFIG_MD_MODE, motionState);
}


/*******************************************************************************
Name        : SetDeiInterpolation
Description : Compute and set interpolation for DEI control
Parameters  : deiCtl     - [out] DEI control control value
              lumaMode   - [in]  Luma mode to be set
              chromaMode - [in]  Chroma mode to be set
Assumptions :
Limitations :
Returns     : void
*******************************************************************************/
inline void CHqvdpLitePlaneV2::SetDeiInterpolation(
                                    uint32_t&   deiCtl,
                                    uint32_t    lumaMode,
                                    uint32_t    chromaMode) const
{
    SetBitField(deiCtl, CSDI_CONFIG_LUMA_INTERP,   lumaMode);
    SetBitField(deiCtl, CSDI_CONFIG_CHROMA_INTERP, chromaMode);
}


/*******************************************************************************
Name        : SetCsdiParams
Description : Set CSDI parameters of the HQVDP conf
Parameters  : pPrevNode         - [in]  Previous node displayed
              pCurrNode         - [in]  Current node to display
              pNextNode         - [in]  Next node to display
              pTopParams        - [in]  Top parameters
              isPictureRepeated - [in]  True if picture is repeated
              pCsdiParams       - [out] CSDI parameters structure to be updated
Assumptions :
Limitations :
Returns     : true if CSDI parameters could be updated, false else
*******************************************************************************/
bool CHqvdpLitePlaneV2::SetCsdiParams(
                                const CDisplayNode*                 pPrevNode,
                                const CDisplayNode*                 pCurrNode,
                                const CDisplayNode*                 pNextNode,
                                const CHqvdpLiteDisplayInfoV2*      pDisplayInfo,
                                const HQVDPLITE_TOP_Params_t*       pTopParams,
                                bool                                isPictureRepeated,
                                HQVDPLITE_CSDI_Params_t*            pCsdiParams)
{
    PLANE_TRCBL(TRC_ID_HQVDPLITE_DEI);

    if(!pCurrNode)
    {
        PLANE_TRC(TRC_ID_ERROR, "Invalid argument");
        return false;
    }

    // Debugging trace: display previous, current and next node information
    if( IS_TRC_ID_ENABLED(TRC_ID_HQVDPLITE_DEI) )
    {
        char prevNodeText[25];
        char nextNodeText[25];

        if(pPrevNode)
        {
            vibe_os_snprintf(prevNodeText, sizeof(prevNodeText), "%d%c %s",
                             pPrevNode->m_pictureId,
                             pPrevNode->m_srcPictureTypeChar,
                             pPrevNode->m_doNotDisplay?"(DoNotDisplay)":"");
        }
        else
        {
            vibe_os_snprintf(prevNodeText, sizeof(prevNodeText), "(null)");
        }

        if(pNextNode)
        {
            vibe_os_snprintf(nextNodeText, sizeof(nextNodeText), "%d%c %s",
                             pNextNode->m_pictureId,
                             pNextNode->m_srcPictureTypeChar,
                             pNextNode->m_doNotDisplay?"(DoNotDisplay)":"");
        }
        else
        {
            vibe_os_snprintf(nextNodeText, sizeof(nextNodeText), "(null)");
        }

        PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "P: %s C: %d%c N: %s",
                                        prevNodeText,
                                        pCurrNode->m_pictureId,
                                        pCurrNode->m_srcPictureTypeChar,
                                        nextNodeText);
    }

    /* Select the DEI mode to use */
    const HqvdpLiteDeiMode_e deiMode = SelectDeiMode(pDisplayInfo, pPrevNode, pCurrNode, pNextNode);

    if((pDisplayInfo->m_isSrcInterlaced) && (deiMode == HQVDPLITE_DEI_MODE_OFF))
    {
        PLANE_TRC(TRC_ID_ERROR, "DEI should NOT be OFF with Interlaced sources!");
    }

    /*
     * rotate motion state if DEI mode is HQVDPLITE_DEI_MODE_3D
     * reset motion state to MOTION_OFF if DEI mode is HQVDPLITE_DEI_MODE_DI
     */
    switch(deiMode)
    {
        case HQVDPLITE_DEI_MODE_OFF:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "HQVDPLITE_DEI_MODE_OFF");
            m_motionState = HQVDPLITE_DEI_MOTION_OFF;
            SetDeiMainMode(pCsdiParams->Config, HQVDPLITE_DEI_CTL_MAIN_MODE_BYPASS_ON);
            break;
        }
        case HQVDPLITE_DEI_MODE_DI:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "HQVDPLITE_DEI_MODE_DI is set");
            PLANE_TRC(TRC_ID_TEST_VERDICT , "HQVDPLITE_DEI_MODE_DI is set");//trace used by test
            m_motionState = HQVDPLITE_DEI_MOTION_OFF;
            SetDeiMainMode     (pCsdiParams->Config, HQVDPLITE_DEI_CTL_MAIN_MODE_DEINTERLACING);
            SetDeiInterpolation(pCsdiParams->Config, HQVDPLITE_DEI_CTL_LUMA_DI, HQVDPLITE_DEI_CTL_CHROMA_DI);
            break;
        }
        case HQVDPLITE_DEI_MODE_3D:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "HQVDPLITE_DEI_MODE_3D is set");
            PLANE_TRC(TRC_ID_TEST_VERDICT , "HQVDPLITE_DEI_MODE_Y3D_UV3D is set");//trace used by test;

            // If the same picture is repeated:
            // - Motion Buffers should not be rotated
            // - Motion State should be not updated
            if(!isPictureRepeated)
            {
                RotateMotionBuffers();
                UpdateMotionState();
            }
            else
            {
                PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "Pict repeated: Motion state unchanged");
            }

            // Update csdi_config according to the new motion state
            switch(m_motionState)
            {
                case HQVDPLITE_DEI_MOTION_OFF:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "MOTION_OFF");
                    SetDeiMainMode     (pCsdiParams->Config, HQVDPLITE_DEI_CTL_MAIN_MODE_DEINTERLACING);
                    SetDeiInterpolation(pCsdiParams->Config, HQVDPLITE_DEI_CTL_LUMA_DI, HQVDPLITE_DEI_CTL_CHROMA_DI);
                    break;
                }
                case HQVDPLITE_DEI_MOTION_INIT:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "MOTION_INIT");
                    SetDeiMainMode     (pCsdiParams->Config, HQVDPLITE_DEI_CTL_MAIN_MODE_DEINTERLACING);
                    SetDeiInterpolation(pCsdiParams->Config, HQVDPLITE_DEI_CTL_LUMA_DI, HQVDPLITE_DEI_CTL_CHROMA_DI);
                    break;
                }
                case HQVDPLITE_DEI_MOTION_LOW_CONF:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "MOTION_LOW");
                    SetDeiMainMode       (pCsdiParams->Config, HQVDPLITE_DEI_CTL_MAIN_MODE_DEINTERLACING);
                    SetDeiInterpolation  (pCsdiParams->Config, HQVDPLITE_DEI_CTL_LUMA_3D, HQVDPLITE_DEI_CTL_CHROMA_3D);
                    Set3dDeiConfiguration(pCsdiParams->Config);
                    break;
                }
                case HQVDPLITE_DEI_MOTION_FULL_CONF:
                {
                    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "MOTION_FULL");
                    SetDeiMainMode       (pCsdiParams->Config, HQVDPLITE_DEI_CTL_MAIN_MODE_DEINTERLACING);
                    SetDeiInterpolation  (pCsdiParams->Config, HQVDPLITE_DEI_CTL_LUMA_3D, HQVDPLITE_DEI_CTL_CHROMA_3D);
                    Set3dDeiConfiguration(pCsdiParams->Config);
                    break;
                }
            }
            break;
        }
        case HQVDPLITE_DEI_MODE_VI:
        case HQVDPLITE_DEI_MODE_MEDIAN:
        default:
        {
            PLANE_TRC(TRC_ID_ERROR, "DEI Mode %d not implemented!", deiMode);
            return false;
        }
    }

    /* Set the base address of the Previous and Next Fields of the conf */
    SetDeiNextFieldAddresses    (pNextNode, pDisplayInfo, pCsdiParams);
    SetDeiPreviousFieldAddresses(pPrevNode, pDisplayInfo, pCsdiParams);

    /* Set the current motion state */
    SetDeiMotionState(pCsdiParams->Config, m_motionState);

    /* Set Motion Buffers */
    pCsdiParams->PrevMotion = m_prevMotionBufferPhysAddr;
    pCsdiParams->CurMotion  = m_currMotionBufferPhysAddr;
    pCsdiParams->NextMotion = m_nextMotionBufferPhysAddr;

    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "csdi_config: %#.8x", pCsdiParams->Config);

    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "PY: %#.8x CY: %#.8x NY: %#.8x",
                                    pCsdiParams->PrevLuma,
                                    pTopParams->CurrentLuma,
                                    pCsdiParams->NextLuma);

    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "PC: %#.8x CC: %#.8x NC: %#.8x",
                                    pCsdiParams->PrevChroma,
                                    pTopParams->CurrentChroma,
                                    pCsdiParams->NextChroma);

    PLANE_TRC(TRC_ID_HQVDPLITE_DEI, "PM: %#.8x CM: %#.8x NM: %#.8x",
                                    pCsdiParams->PrevMotion,
                                    pCsdiParams->CurMotion,
                                    pCsdiParams->NextMotion);

    return true;
}

/*******************************************************************************
Name        : PrepareHvsrcParams
Description : Prepare the HVSRC part of the HQVDP conf.
Parameters  : pDisplayInfo    - [in]  Display info
              pHvsrcParams    - [out] HVSRC parameters to be updated
              pHvsrcLutParams - [out] HVSRC LUT parameters to be updated
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::PrepareHvsrcParams(
                                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                    HQVDPLITE_HVSRC_Params_t*       pHvsrcParams,
                                    HQVDPLITE_LUT_HVSRC_Params_t*   pHvsrcLutParams) const
{
    uint32_t    chromaInputWidth    = 0;
    uint32_t    chromaInputHeight   = 0;
    stm_rect_t  inputRectangle      = pDisplayInfo->m_selectedPicture.srcFrameRect; /* InputRectangle  in Frame notation */
    stm_rect_t  outputRectangle     = pDisplayInfo->m_dstFrameRect;                 /* OutputRectangle in Frame notation */

    /* Clear the content of the HVSRC params */
    vibe_os_zero_memory(pHvsrcParams, sizeof(*pHvsrcParams));

    /* Adapting output rectangle size with respect to output mode (SbS or TaB)*/
    switch(m_outputInfo.currentMode.mode_params.flags)
    {
        case STM_MODE_FLAGS_3D_SBS_HALF:
        {
            outputRectangle.width /= 2;
            break;
        }
        case STM_MODE_FLAGS_3D_TOP_BOTTOM:
        {
            outputRectangle.height /= 2;
            break;
        }
        default:
            break;
    }

    /******************************/
    /* HVSRC : HOR_PANORAMIC_CTRL */
    /******************************/
    pHvsrcParams->HorPanoramicCtrl = 0;

    /********************************/
    /* HVSRC  : OUTPUT_PICTURE_SIZE */
    /********************************/

    /* HVSRC output should ALWAYS be a Frame */
    SetBitField(pHvsrcParams->OutputPictureSize, HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT, outputRectangle.height);
    SetBitField(pHvsrcParams->OutputPictureSize, HVSRC_OUTPUT_PICTURE_SIZE_WIDTH,  outputRectangle.width);

    /****************************/
    /* HVSRC  : INIT_HORIZONTAL */
    /****************************/
    SetBitField(pHvsrcParams->InitHorizontal, HVSRC_INIT_HORIZONTAL_CHROMA_PHASE_SHIFT, 0);
    SetBitField(pHvsrcParams->InitHorizontal, HVSRC_INIT_HORIZONTAL_LUMA_INIT_PHASE,    0);

    /**************************/
    /* HVSRC  : INIT_VERTICAL */
    /**************************/
    SetBitField(pHvsrcParams->InitVertical, HVSRC_INIT_VERTICAL_CHROMA_PHASE_SHIFT, 0);
    SetBitField(pHvsrcParams->InitVertical, HVSRC_INIT_VERTICAL_LUMA_INIT_PHASE,    0);

    /***********************/
    /* HVSRC  : PARAM_CTRL */
    /***********************/

    /* Disable Auto Control all the time because it doesn't work as expected */
    SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_V_AUTOKOVS, HQVDPLITE_HVSRC_V_AUTOKOVS_DEFAULT);
    SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_H_AUTOKOVS, HQVDPLITE_HVSRC_H_AUTOKOVS_DEFAULT);

    /*
     * Luma Settings
     */

    /* Disable Luma Horizontal Overshoting when there is no horizontal scaling */
    if(outputRectangle.width == inputRectangle.width)
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_H_ACON,  HQVDPLITE_HVSRC_H_ACON_DEFAULT);
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_YH_KOVS, HQVDPLITE_HVSRC_YH_KOVS_DISABLE);
    }
    /* Disable Horizontal Antiringing and Luma Horizontal Overshooting in width downscaling */
    else if(outputRectangle.width < inputRectangle.width)
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_H_ACON,  HQVDPLITE_HVSRC_H_ACON_DISABLE);
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_YH_KOVS, HQVDPLITE_HVSRC_YH_KOVS_DISABLE);
    }
    /* Enable Horizontal Antiringing and Luma Horizontal Overshooting in width upscaling */
    else
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_H_ACON,  HQVDPLITE_HVSRC_H_ACON_DEFAULT);
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_YH_KOVS, HQVDPLITE_HVSRC_YH_KOVS_MAX);
    }

    /* Disable Luma Vertical OverShooting when there is no vertical Scaling */
    if(outputRectangle.height == inputRectangle.height)
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_V_ACON,  HQVDPLITE_HVSRC_V_ACON_DEFAULT);
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_YV_KOVS, HQVDPLITE_HVSRC_YV_KOVS_DISABLE);
    }
    /* Disable Vertical Antiringing and Luma Vertical OverShooting in height downscaling */
    else if(outputRectangle.height < inputRectangle.height)
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_V_ACON,  HQVDPLITE_HVSRC_V_ACON_DISABLE);
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_YV_KOVS, HQVDPLITE_HVSRC_YV_KOVS_DISABLE);
    }
    /* Enable Vertical Antiringing and Luma Vertical OverShooting in height upscaling */
    else
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_V_ACON,  HQVDPLITE_HVSRC_V_ACON_DEFAULT);
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_YV_KOVS, HQVDPLITE_HVSRC_YV_KOVS_MAX);
    }

    /*
     * Chroma Settings
     */

    /* Calculate input chroma width base on the color format */
    if(pDisplayInfo->m_selectedPicture.isSrc420)
    {
        chromaInputWidth  = inputRectangle.width / 2;
        chromaInputHeight = inputRectangle.height / 2;
    }
    else if(pDisplayInfo-> m_selectedPicture.isSrc422)
    {
        chromaInputWidth  = inputRectangle.width / 2;
        chromaInputHeight = inputRectangle.height;
    }
    else
    {
        chromaInputWidth  = inputRectangle.width;
        chromaInputHeight = inputRectangle.height;
    }

    /* Disable Chroma Horizontal Overshooting when there is no Horizontal Scaling */
    if(outputRectangle.width == inputRectangle.width)
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_CH_KOVS, HQVDPLITE_HVSRC_CH_KOVS_DISABLE);
    }
    /* Set Chroma Horizontal Overshooting based on the color format */
    else
    {
        /* Disable Chroma Horizontal Overshooting in width downscaling base on the color format */
        if(outputRectangle.width < chromaInputWidth)
        {
            SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_CH_KOVS, HQVDPLITE_HVSRC_CH_KOVS_DISABLE);
        }
        /* Enable Chroma Horizontal Overshooting in width upscaling base on the color format */
        else
        {
            SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_CH_KOVS, HQVDPLITE_HVSRC_CH_KOVS_MAX);
        }
    }

    /* Disable Chroma Vertical OverShooting when there is no Vertical Scaling */
    if(outputRectangle.height == inputRectangle.height)
    {
        SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_CH_KOVS, HQVDPLITE_HVSRC_CH_KOVS_DISABLE);
    }
    /* Set Chroma Vertical OverShooting based on the color format */
    else
    {
        /* Disable Chroma Vertical OverShooting in height downscaling base on the color format */
        if(outputRectangle.height < chromaInputHeight)
        {
            SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_CV_KOVS, HQVDPLITE_HVSRC_CV_KOVS_DISABLE);
        }
        /* Enable Chroma Vertical OverShooting in height upscaling base on the color format */
        else
        {
            SetBitField(pHvsrcParams->ParamCtrl, HVSRC_PARAM_CTRL_CV_KOVS, HQVDPLITE_HVSRC_CV_KOVS_MAX);
        }
    }

    /* Clear the content of the HVSRC LUT params */
    vibe_os_zero_memory(pHvsrcLutParams, sizeof(*pHvsrcLutParams));

    /* Set HVSRC LUTs params */
    pHvsrcLutParams->custom            = false;
    pHvsrcLutParams->luma_h_strength   = HQVDP_HVSRC_LUT_Y_MEDIUM;
    pHvsrcLutParams->chroma_h_strength = HQVDP_HVSRC_LUT_C_OPT;
    pHvsrcLutParams->luma_v_strength   = HQVDP_HVSRC_LUT_Y_MEDIUM;
    pHvsrcLutParams->chroma_v_strength = HQVDP_HVSRC_LUT_C_OPT;

    return true;
}


/*******************************************************************************
Name        : PrepareOutParams
Description : Prepare the OUT part of the HQVDP conf.
Parameters  : pDisplayInfo - [in]  Display info
              pOutParams   - [out] OUT params to be prepared
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::PrepareOutParams(
                                    const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                    HQVDPLITE_OUT_Params_t*         pOutParams) const
{
    pOutParams->output_window_top_left_x = STCalculateViewportPixel(&m_outputInfo.currentMode, (int)pDisplayInfo->m_dstFrameRect.x);
    pOutParams->output_window_top_left_y = STCalculateViewportLine (&m_outputInfo.currentMode, (int)pDisplayInfo->m_dstFrameRect.y);
    pOutParams->output_vtg_width         = m_outputInfo.currentMode.mode_timing.pixels_per_line;
    pOutParams->output_vtg_height        = m_outputInfo.currentMode.mode_timing.lines_per_frame;
#if defined(FORCE_60HZ_OUTPUT_FPS)
    pOutParams->output_vtg_fps           = 60000;
#else
    pOutParams->output_vtg_fps           = m_outputInfo.currentMode.mode_params.vertical_refresh_rate;
#endif

    return true;
}


/*******************************************************************************
Name        : SetIqiParams
Description : Sets the IQI part of the HQVDP conf.
Parameters  : pCurrNode           - [in]  Current node to display info
              pDisplayInfo        - [in]  Display info
              isTopFieldOnDisplay - [in]  True if top field is on the display
              isDisplayInterlaced - [in]  True if display is interlaced
              pIqiParams          - [out] IQI parameters structure to be set
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::SetIqiParams(
                                const CDisplayNode*             pCurrNode,
                                const CHqvdpLiteDisplayInfoV2*  pDisplayInfo,
                                bool                            isTopFieldOnDisplay,
                                bool                            isDisplayInterlaced,
                                HQVDPLITE_IQI_Params_t*         pIqiParams)
{
    bool bIqiAlgoEnabled = false;

    /* Clear the content of the IQI params */
    vibe_os_zero_memory(pIqiParams, sizeof(*pIqiParams));

    SetBitField(pIqiParams->ConBri,  IQI_CON_BRI_CONTRAST_GAIN, HQVDPLITE_IQI_CONTRAST_BRIGHTNESS_GAIN_DEFAULT);
    SetBitField(pIqiParams->ConBri,  IQI_CON_BRI_CONBRI_OFFSET, HQVDPLITE_IQI_CONTRAST_BRIGHTNESS_OFFSET_DEFAULT);
    SetBitField(pIqiParams->SatGain, IQI_SAT_GAIN_SAT_GAIN,     HQVDPLITE_IQI_SATURATION_GAIN_DEFAULT);

    // Force default color to off as Video Plug Color Fill is used instead
    SetBitField(pIqiParams->DefaultColor, IQI_DEFAULT_COLOR_DEFAULT_COLOR_CONTROL, 0);

    if(!pDisplayInfo->m_isUsingProgressive2InterlacedHW)
    {
        // Bypass the IQI P2I pixel FIFO
        SetBitField(pIqiParams->PxfConf, IQI_PXF_CONF_BYPASS_ENABLE, 1);
    }
    else
    {
        // Enable the IQI P2I pixel FIFO
        SetBitField(pIqiParams->PxfConf, IQI_PXF_CONF_BYPASS_ENABLE, 0);

        // Set IQI pixel FIFO polarity
        SetBitField(pIqiParams->PxfConf, IQI_PXF_CONF_FIELD, isTopFieldOnDisplay ? 0 : 1);
    }

    if(m_iqiPeaking.GetPreset() != PCIQIC_BYPASS)
    {
        m_iqiPeaking.CalculateParams(pDisplayInfo, isDisplayInterlaced, pIqiParams);

        SetBitField(pIqiParams->Config, IQI_CONFIG_PK_ENABLE, 1);
        bIqiAlgoEnabled = true;
    }

    if(m_iqiLe.GetPreset() != PCIQIC_BYPASS)
    {
        bool bEnableCsc = false;

        m_iqiLe.CalculateParams(pCurrNode, pIqiParams, &bEnableCsc);

        SetBitField(pIqiParams->Config, IQI_CONFIG_CSC_ENABLE, bEnableCsc ? 1 : 0);
        SetBitField(pIqiParams->Config, IQI_CONFIG_LE_ENABLE,  1);
        bIqiAlgoEnabled = true;
    }

    if(m_iqiCti.GetPreset() != PCIQIC_BYPASS)
    {
        m_iqiCti.CalculateParams(pIqiParams);
        SetBitField(pIqiParams->Config, IQI_CONFIG_CTI_ENABLE, 1);
        bIqiAlgoEnabled = true;
    }

    SetBitField(pIqiParams->Config, IQI_CONFIG_MAIN_BYPASS, bIqiAlgoEnabled ? 0 : 1);

    return true;
}


/*******************************************************************************
Name        : SetVideoPlugParams
Description : Sets the Video Plug part of the HQVDP conf.
Parameters  : pVideoPlugParams - [out] Video Plug parameters structure to be set
Assumptions :
Limitations :
Returns     : true if successful, false otherwise
*******************************************************************************/
bool CHqvdpLitePlaneV2::SetVideoPlugParams(
                            HQVDPLITE_VIDEOPLUG_Params_t*   pVideoPlugParams)
{
    bool     bOk            = false;
    uint32_t colorFillState = 0;

    bOk = m_videoPlug[0]->GetControl(PLANE_CTRL_COLOR_FILL_STATE, &colorFillState);

    if(!bOk)
    {
        PLANE_TRC( TRC_ID_ERROR, "Failed to get video plug Color Fill state");
        return false;
    }

    pVideoPlugParams->color_fill_enabled = (colorFillState != 0);

    return true;
}


/*******************************************************************************
Name        : SetTuning
Description : Set tuning settings for a given tuning service
Parameters  : service        - [in]  Tuning service
              pInputList     - [in]  Input tuning list
              inputListSize  - [in]  Input tuning list size
              pOutputList    - [out] Tuning settings to be updated
              outputListSize - [in]  Size of output list parameter
Assumptions :
Limitations :
Returns     : TUNING_OK if successful
*******************************************************************************/
TuningResults CHqvdpLitePlaneV2::SetTuning(
                                        uint16_t    service,
                                        void*       pInputList,
                                        uint32_t    inputListSize,
                                        void*       pOutputList,
                                        uint32_t    outputListSize)
{
    TuningResults res = TUNING_INVALID_PARAMETER;

    PLANE_TRCIN(TRC_ID_HQVDPLITE, "");

    switch((tuning_service_type_t)service)
    {
        case CRC_CAPABILITY:
        {
            if(pOutputList != 0)
            {
                vibe_os_snprintf((char*)pOutputList, outputListSize, "HQVDP,InputY,InputUv,NextYFmd,NextNextYFmd,NextNextNextYFmd,"
                                                                     "PrevYCsdi,CurYCsdi,NextYCsdi,PrevUvCsdi,CurUvCsdi,NextUvCsdi,YCsdi,UvCsdi,"
                                                                     "UvCup,MotCsdi,MotCurCsdi,MotPrevCsdi,YHvr,UHvr,VHvr,PxfItStatus,YIqi,UIqi,VIqi");
                res = TUNING_OK;
            }
            else
            {
                PLANE_TRC( TRC_ID_ERROR, "Invalid outputList!");
                res = TUNING_INVALID_PARAMETER;
            }
            break;
        }
        case CRC_COLLECT:
        {
            PLANE_TRC(TRC_ID_HQVDPLITE, "CRC_COLLECT");

            SetTuningOutputData_t* pTuningOutputData = (SetTuningOutputData_t*)pOutputList;
            if(pTuningOutputData != 0)
            {
                m_pDisplayDevice->VSyncLock();
                if(m_crcData.Status)
                {
                    pTuningOutputData->Data.Crc = m_crcData;
                    PLANE_TRC( TRC_ID_HQVDPLITE, "VSync %lld PTS : %lld", pTuningOutputData->Data.Crc.LastVsyncTime, pTuningOutputData->Data.Crc.PTS);
                    vibe_os_zero_memory(&m_crcData, sizeof(m_crcData));
                    res = TUNING_OK;
                }
                else
                {
                    res = TUNING_NO_DATA_AVAILABLE;
                }
                m_pDisplayDevice->VSyncUnlock();
            }
            else
            {
                PLANE_TRC( TRC_ID_ERROR, "Invalid outputList!");
                res = TUNING_INVALID_PARAMETER;
            }
            break;
        }
        default:
        {
            res = CDisplayPlane::SetTuning(service, pInputList, inputListSize, pOutputList, outputListSize);
            break;
        }
    }

    PLANE_TRCOUT(TRC_ID_HQVDPLITE, "");
    return res;
}
