/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __HQVDP_SPLIT_H__
#define __HQVDP_SPLIT_H__

#include "hqvdp_lld_api.h"
#include "hqvdp_lld_platform.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

enum side_e {
        LEFT = 0,
        RIGHT = 1
};

/** @brief original command parameters used for split structure
    @ingroup lld_internal_struct
*/
struct cmd_origi_param {
        uint16_t input_viewportx;   /**< @brief position of left corner  */
        uint16_t input_viewport_width;   /**< @brief input width of LUMA or CHROMA */
        uint16_t output_width;  /**< @brief output width of LUMA or CHROMA */
        int8_t init_phase_luma;  /**< @brief LUMA horizontal initial phase */
        uint16_t xdo;           /**< @brief x coordinate of the output image */

};

/** @brief 3d command parameters structure
    @ingroup lld_internal_struct
*/
struct cmd_origi_param_3d {
        uint8_t output_3d_format;
        bool rightnotleft;
        bool flat3d;
        uint16_t left_view_border_width_left;
        uint16_t left_view_border_width_right;
        uint16_t right_view_border_width_left;
        uint16_t right_view_border_width_right;
};

/** @brief split configuration structure
    @ingroup lld_internal_struct
*/
struct cmd_split_param {
        uint16_t input_viewportx;   /**< @brief position of left corner  */
        uint16_t input_viewport_width;   /**< @brief input width of LUMA or CHROMA  */
        uint16_t output_width;  /**< @brief output width of LUMA or CHROMA */
        int8_t init_phase_luma; /**< @brief LUMA horizontal initial phase */
        uint16_t xdo;           /**< @brief x coordinate of the output image */
        uint16_t crop_xdo;      /**< @brief crop value to use to program VideoPlug */
        uint16_t crop_xds;      /**< @brief crop value to use to program VideoPlug */
};

/** @brief structure to get error by split algo
    @ingroup lld_internal_struct
*/
struct error_split_param {
		int ErrorAll;  /**< @brief sum of all error   */
		int ErrorPhaseRight; /**< @brief Error with full param on right phase  */
		int ErrorPhaseLeft; /**< @brief Error with full param on right phase */
        int ErrorIncr; /**< @brief Error with full param on increment */
};

/** @brief write command(s) in HQVDP from one original command
    @ingroup  lld_internal_function
    @param[in] hdl : handle to session
    @param[in] expected_conf : ideal configuration
    @param[in] hw_to_use_cnt : number of available hardware resources
    @param[out] compo_setup[] : video plug and mixer setup to be applied
*/
void hqvdp_write_cmd(hqvdp_lld_hdl_t hdl,
                    const struct hqvdp_lld_conf_s *expected_conf,
                    uint8_t hw_to_use_cnt,
                    struct hqvdp_lld_compo_setup_s compo_setup[]
                   );

/** @brief
*/
void hqvdp_prepare_split(const struct cmd_origi_param *full_param,
                         const struct cmd_origi_param_3d *param_3d,
                         struct cmd_split_param side_param[2]);

/******************************************************************************/
/******************************************************************************/
/* C++ support */
#ifdef __cplusplus
}
#endif

#endif
