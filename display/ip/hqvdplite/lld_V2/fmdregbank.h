/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _FMDREGBANK_H_
#define _FMDREGBANK_H_

/**
* Component section
* Purpose : defines macros for component
*           specific informations
*/

#define FMDregBank_VENDOR                               ("st.com")
#define FMDregBank_LIBRARY                              ("hqvdp")
#define FMDregBank_NAME                                 ("FMDregBank")
#define FMDregBank_VERSION                              ("1.0")

/**
* Address Block : fmd_pu_reg
*/

#define FMDregBank_fmd_pu_reg_BASE_ADDR                 (0x0)

/**
* Register : FMD_ENABLE
* Field mode detection enable
*/

#define FMDregBank_FMD_ENABLE_SIZE                      (32)
#define FMDregBank_FMD_ENABLE_OFFSET                    (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x00)
#define FMDregBank_FMD_ENABLE_RESET_VALUE               (0x00000000)
#define FMDregBank_FMD_ENABLE_BITFIELD_MASK             (0x8000000F)
#define FMDregBank_FMD_ENABLE_RWMASK                    (0x8000000F)
#define FMDregBank_FMD_ENABLE_ROMASK                    (0x00000000)
#define FMDregBank_FMD_ENABLE_WOMASK                    (0x00000000)
#define FMDregBank_FMD_ENABLE_UNUSED_MASK               (0x7FFFFFF0)

/**
* Bit-field : DISABLE_CFD
* field mode detection enable bit:
*/

#define FMDregBank_FMD_ENABLE_DISABLE_CFD_OFFSET        (0x00000000)
#define FMDregBank_FMD_ENABLE_DISABLE_CFD_WIDTH         (1)
#define FMDregBank_FMD_ENABLE_DISABLE_CFD_MASK          (0x00000001)

/**
* Value : B_0x0
* the field mode detection is enable.
*/

#define FMDregBank_FMD_ENABLE_DISABLE_CFD_B_0x0         (0x00000000)

/**
* Value : B_0x1
* the field mode detection is disable
*/

#define FMDregBank_FMD_ENABLE_DISABLE_CFD_B_0x1         (0x00000001)

/**
* Bit-field : FIRSTLINE
* first line of BBD field:
*/

#define FMDregBank_FMD_ENABLE_FIRSTLINE_OFFSET          (0x00000001)
#define FMDregBank_FMD_ENABLE_FIRSTLINE_WIDTH           (1)
#define FMDregBank_FMD_ENABLE_FIRSTLINE_MASK            (0x00000002)

/**
* Value : B_0x0
* it isn't the first line of BBD field.
*/

#define FMDregBank_FMD_ENABLE_FIRSTLINE_B_0x0           (0x00000000)

/**
* Value : B_0x1
* it is the first line of BBD field.
*/

#define FMDregBank_FMD_ENABLE_FIRSTLINE_B_0x1           (0x00000001)

/**
* Bit-field : FIRSTBLOCKLINE
* first line of BBD Block:
*/

#define FMDregBank_FMD_ENABLE_FIRSTBLOCKLINE_OFFSET     (0x00000002)
#define FMDregBank_FMD_ENABLE_FIRSTBLOCKLINE_WIDTH      (1)
#define FMDregBank_FMD_ENABLE_FIRSTBLOCKLINE_MASK       (0x00000004)

/**
* Value : B_0x0
* it isn't the first line of BBD block.
*/

#define FMDregBank_FMD_ENABLE_FIRSTBLOCKLINE_B_0x0      (0x00000000)

/**
* Value : B_0x1
* it is the first line of BBD block.
*/

#define FMDregBank_FMD_ENABLE_FIRSTBLOCKLINE_B_0x1      (0x00000001)

/**
* Bit-field : LASTBLOCKLINE
* last line of BBD Block:
*/

#define FMDregBank_FMD_ENABLE_LASTBLOCKLINE_OFFSET      (0x00000003)
#define FMDregBank_FMD_ENABLE_LASTBLOCKLINE_WIDTH       (1)
#define FMDregBank_FMD_ENABLE_LASTBLOCKLINE_MASK        (0x00000008)

/**
* Value : B_0x0
* it isn't the last line of BBD block.
*/

#define FMDregBank_FMD_ENABLE_LASTBLOCKLINE_B_0x0       (0x00000000)

/**
* Value : B_0x1
* it is the last line of BBD block.
*/

#define FMDregBank_FMD_ENABLE_LASTBLOCKLINE_B_0x1       (0x00000001)

/**
* Bit-field : FMDREQUEST
* add for IPmodelling only:
*/

#define FMDregBank_FMD_ENABLE_FMDREQUEST_OFFSET         (0x0000001f)
#define FMDregBank_FMD_ENABLE_FMDREQUEST_WIDTH          (1)
#define FMDregBank_FMD_ENABLE_FMDREQUEST_MASK           (0x80000000)

/**
* Value : B_0x0
* no line to pull by FMD.
*/

#define FMDregBank_FMD_ENABLE_FMDREQUEST_B_0x0          (0x00000000)

/**
* Value : B_0x1
* one line to pull by FMD.
*/

#define FMDregBank_FMD_ENABLE_FMDREQUEST_B_0x1          (0x00000001)

/**
* Register : FMD_WIDTH
* Field mode detection width
*/

#define FMDregBank_FMD_WIDTH_SIZE                       (32)
#define FMDregBank_FMD_WIDTH_OFFSET                     (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x04)
#define FMDregBank_FMD_WIDTH_RESET_VALUE                (0x00000000)
#define FMDregBank_FMD_WIDTH_BITFIELD_MASK              (0x000007FF)
#define FMDregBank_FMD_WIDTH_RWMASK                     (0x000007FF)
#define FMDregBank_FMD_WIDTH_ROMASK                     (0x00000000)
#define FMDregBank_FMD_WIDTH_WOMASK                     (0x00000000)
#define FMDregBank_FMD_WIDTH_UNUSED_MASK                (0xFFFFF800)

/**
* Bit-field : WIDTH
* width of FMD line
*/

#define FMDregBank_FMD_WIDTH_WIDTH_OFFSET               (0x00000000)
#define FMDregBank_FMD_WIDTH_WIDTH_WIDTH                (11)
#define FMDregBank_FMD_WIDTH_WIDTH_MASK                 (0x000007FF)

/**
* Register : FMD_DL_INIT
* DelayLine Init
*/

#define FMDregBank_FMD_DL_INIT_SIZE                     (32)
#define FMDregBank_FMD_DL_INIT_OFFSET                   (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x08)
#define FMDregBank_FMD_DL_INIT_RESET_VALUE              (0x00000000)
#define FMDregBank_FMD_DL_INIT_BITFIELD_MASK            (0x00000001)
#define FMDregBank_FMD_DL_INIT_RWMASK                   (0x00000001)
#define FMDregBank_FMD_DL_INIT_ROMASK                   (0x00000000)
#define FMDregBank_FMD_DL_INIT_WOMASK                   (0x00000000)
#define FMDregBank_FMD_DL_INIT_UNUSED_MASK              (0xFFFFFFFE)

/**
* Bit-field : INIT
*/

#define FMDregBank_FMD_DL_INIT_INIT_OFFSET              (0x00000000)
#define FMDregBank_FMD_DL_INIT_INIT_WIDTH               (1)
#define FMDregBank_FMD_DL_INIT_INIT_MASK                (0x00000001)

/**
* Register : FMD_DL_SKIP
* DelayLine Skip
*/

#define FMDregBank_FMD_DL_SKIP_SIZE                     (32)
#define FMDregBank_FMD_DL_SKIP_OFFSET                   (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x0C)
#define FMDregBank_FMD_DL_SKIP_RESET_VALUE              (0x00000000)
#define FMDregBank_FMD_DL_SKIP_BITFIELD_MASK            (0x00000001)
#define FMDregBank_FMD_DL_SKIP_RWMASK                   (0x00000001)
#define FMDregBank_FMD_DL_SKIP_ROMASK                   (0x00000000)
#define FMDregBank_FMD_DL_SKIP_WOMASK                   (0x00000000)
#define FMDregBank_FMD_DL_SKIP_UNUSED_MASK              (0xFFFFFFFE)

/**
* Bit-field : SKIP
*/

#define FMDregBank_FMD_DL_SKIP_SKIP_OFFSET              (0x00000000)
#define FMDregBank_FMD_DL_SKIP_SKIP_WIDTH               (1)
#define FMDregBank_FMD_DL_SKIP_SKIP_MASK                (0x00000001)

/**
* Register : FMD_DL_FLUSH
* DelayLine Flush
*/

#define FMDregBank_FMD_DL_FLUSH_SIZE                    (32)
#define FMDregBank_FMD_DL_FLUSH_OFFSET                  (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x10)
#define FMDregBank_FMD_DL_FLUSH_RESET_VALUE             (0x00000000)
#define FMDregBank_FMD_DL_FLUSH_BITFIELD_MASK           (0x00000001)
#define FMDregBank_FMD_DL_FLUSH_RWMASK                  (0x00000001)
#define FMDregBank_FMD_DL_FLUSH_ROMASK                  (0x00000000)
#define FMDregBank_FMD_DL_FLUSH_WOMASK                  (0x00000000)
#define FMDregBank_FMD_DL_FLUSH_UNUSED_MASK             (0xFFFFFFFE)

/**
* Bit-field : FLUSH
*/

#define FMDregBank_FMD_DL_FLUSH_FLUSH_OFFSET            (0x00000000)
#define FMDregBank_FMD_DL_FLUSH_FLUSH_WIDTH             (1)
#define FMDregBank_FMD_DL_FLUSH_FLUSH_MASK              (0x00000001)

/**
* Register : FMD_THRESHOLD_SCD
* Scene change detection threshold
*/

#define FMDregBank_FMD_THRESHOLD_SCD_SIZE               (32)
#define FMDregBank_FMD_THRESHOLD_SCD_OFFSET             (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x14)
#define FMDregBank_FMD_THRESHOLD_SCD_RESET_VALUE        (0x00000000)
#define FMDregBank_FMD_THRESHOLD_SCD_BITFIELD_MASK      (0x0003FFFF)
#define FMDregBank_FMD_THRESHOLD_SCD_RWMASK             (0x0003FFFF)
#define FMDregBank_FMD_THRESHOLD_SCD_ROMASK             (0x00000000)
#define FMDregBank_FMD_THRESHOLD_SCD_WOMASK             (0x00000000)
#define FMDregBank_FMD_THRESHOLD_SCD_UNUSED_MASK        (0xFFFC0000)

/**
* Bit-field : T_SCENE
* SCD threshold used to define scene_count. This is a threshold of blocks pixels differences sum. Above this threshold, a block is considered as moving
*/

#define FMDregBank_FMD_THRESHOLD_SCD_T_SCENE_OFFSET     (0x00000000)
#define FMDregBank_FMD_THRESHOLD_SCD_T_SCENE_WIDTH      (18)
#define FMDregBank_FMD_THRESHOLD_SCD_T_SCENE_MASK       (0x0003FFFF)

/**
* Register : FMD_THRESHOLD_RFD
* Repeat field detection threshold
*/

#define FMDregBank_FMD_THRESHOLD_RFD_SIZE               (32)
#define FMDregBank_FMD_THRESHOLD_RFD_OFFSET             (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x18)
#define FMDregBank_FMD_THRESHOLD_RFD_RESET_VALUE        (0x00000000)
#define FMDregBank_FMD_THRESHOLD_RFD_BITFIELD_MASK      (0x0003FFFF)
#define FMDregBank_FMD_THRESHOLD_RFD_RWMASK             (0x0003FFFF)
#define FMDregBank_FMD_THRESHOLD_RFD_ROMASK             (0x00000000)
#define FMDregBank_FMD_THRESHOLD_RFD_WOMASK             (0x00000000)
#define FMDregBank_FMD_THRESHOLD_RFD_UNUSED_MASK        (0xFFFC0000)

/**
* Bit-field : T_REPEAT
* RFD threshold used to define scene_status. This is a threshold of blocks pixels differences sum. Above this threshold, a block is considered as moving
*/

#define FMDregBank_FMD_THRESHOLD_RFD_T_REPEAT_OFFSET    (0x00000000)
#define FMDregBank_FMD_THRESHOLD_RFD_T_REPEAT_WIDTH     (18)
#define FMDregBank_FMD_THRESHOLD_RFD_T_REPEAT_MASK      (0x0003FFFF)

/**
* Register : FMD_THRESHOLD_MOVE
* Move field detection threshold
*/

#define FMDregBank_FMD_THRESHOLD_MOVE_SIZE              (32)
#define FMDregBank_FMD_THRESHOLD_MOVE_OFFSET            (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x1C)
#define FMDregBank_FMD_THRESHOLD_MOVE_RESET_VALUE       (0x00000000)
#define FMDregBank_FMD_THRESHOLD_MOVE_BITFIELD_MASK     (0x03FF00FF)
#define FMDregBank_FMD_THRESHOLD_MOVE_RWMASK            (0x03FF00FF)
#define FMDregBank_FMD_THRESHOLD_MOVE_ROMASK            (0x00000000)
#define FMDregBank_FMD_THRESHOLD_MOVE_WOMASK            (0x00000000)
#define FMDregBank_FMD_THRESHOLD_MOVE_UNUSED_MASK       (0xFC00FF00)

/**
* Bit-field : T_MOV
* Pixels are said moving pixels when their |A-D | difference is superior to t_mov.
*/

#define FMDregBank_FMD_THRESHOLD_MOVE_T_MOV_OFFSET      (0x00000000)
#define FMDregBank_FMD_THRESHOLD_MOVE_T_MOV_WIDTH       (8)
#define FMDregBank_FMD_THRESHOLD_MOVE_T_MOV_MASK        (0x000000FF)

/**
* Bit-field : T_NUM_MOV_PIX
* A block is said moving if it contains more than t_num_mov_pix moving pixels. Move status bit is set if at least one block is moving block.
*/

#define FMDregBank_FMD_THRESHOLD_MOVE_T_NUM_MOV_PIX_OFFSET (0x00000010)
#define FMDregBank_FMD_THRESHOLD_MOVE_T_NUM_MOV_PIX_WIDTH (10)
#define FMDregBank_FMD_THRESHOLD_MOVE_T_NUM_MOV_PIX_MASK (0x03FF0000)

/**
* Register : FMD_THRESHOLD_CFD
* Consecutive field difference threshold
*/

#define FMDregBank_FMD_THRESHOLD_CFD_SIZE               (32)
#define FMDregBank_FMD_THRESHOLD_CFD_OFFSET             (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x20)
#define FMDregBank_FMD_THRESHOLD_CFD_RESET_VALUE        (0x00000000)
#define FMDregBank_FMD_THRESHOLD_CFD_BITFIELD_MASK      (0x0000000F)
#define FMDregBank_FMD_THRESHOLD_CFD_RWMASK             (0x0000000F)
#define FMDregBank_FMD_THRESHOLD_CFD_ROMASK             (0x00000000)
#define FMDregBank_FMD_THRESHOLD_CFD_WOMASK             (0x00000000)
#define FMDregBank_FMD_THRESHOLD_CFD_UNUSED_MASK        (0xFFFFFFF0)

/**
* Bit-field : T_NOISE
* Min(|A-B|,|A-C|) pixels differences that are inferior to t_noise are considered as noise so not taken into account.
*/

#define FMDregBank_FMD_THRESHOLD_CFD_T_NOISE_OFFSET     (0x00000000)
#define FMDregBank_FMD_THRESHOLD_CFD_T_NOISE_WIDTH      (4)
#define FMDregBank_FMD_THRESHOLD_CFD_T_NOISE_MASK       (0x0000000F)

/**
* Register : FMD_CFDSUM_STATUS
* Sum of Consecutive field difference pixels
*/

#define FMDregBank_FMD_CFDSUM_STATUS_SIZE               (32)
#define FMDregBank_FMD_CFDSUM_STATUS_OFFSET             (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x24)
#define FMDregBank_FMD_CFDSUM_STATUS_RESET_VALUE        (0x00000000)
#define FMDregBank_FMD_CFDSUM_STATUS_BITFIELD_MASK      (0x0FFFFFFF)
#define FMDregBank_FMD_CFDSUM_STATUS_RWMASK             (0x0FFFFFFF)
#define FMDregBank_FMD_CFDSUM_STATUS_ROMASK             (0x00000000)
#define FMDregBank_FMD_CFDSUM_STATUS_WOMASK             (0x00000000)
#define FMDregBank_FMD_CFDSUM_STATUS_UNUSED_MASK        (0xF0000000)

/**
* Bit-field : CFDSUM_RWP
* Sum of CFD pixels difference min(|A-B|,|A-C|) that are superior to threshold t_noise computed during one field.
* Maximum Field size is 1920 pixels of 540 lines.
*/

#define FMDregBank_FMD_CFDSUM_STATUS_CFDSUM_RWP_OFFSET  (0x00000000)
#define FMDregBank_FMD_CFDSUM_STATUS_CFDSUM_RWP_WIDTH   (28)
#define FMDregBank_FMD_CFDSUM_STATUS_CFDSUM_RWP_MASK    (0x0FFFFFFF)

/**
* Register : FMD_FIELD_SUM_STATUS
* Sum of all pixels differences
*/

#define FMDregBank_FMD_FIELD_SUM_STATUS_SIZE            (32)
#define FMDregBank_FMD_FIELD_SUM_STATUS_OFFSET          (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x28)
#define FMDregBank_FMD_FIELD_SUM_STATUS_RESET_VALUE     (0x00000000)
#define FMDregBank_FMD_FIELD_SUM_STATUS_BITFIELD_MASK   (0x0FFFFFFF)
#define FMDregBank_FMD_FIELD_SUM_STATUS_RWMASK          (0x0FFFFFFF)
#define FMDregBank_FMD_FIELD_SUM_STATUS_ROMASK          (0x00000000)
#define FMDregBank_FMD_FIELD_SUM_STATUS_WOMASK          (0x00000000)
#define FMDregBank_FMD_FIELD_SUM_STATUS_UNUSED_MASK     (0xF0000000)

/**
* Bit-field : FIELD_SUM_RWP
* Sum of all pixels difference |A-D| computed during one field.
* Maximum Field size is 1920 pixels of 540 lines.
*/

#define FMDregBank_FMD_FIELD_SUM_STATUS_FIELD_SUM_RWP_OFFSET (0x00000000)
#define FMDregBank_FMD_FIELD_SUM_STATUS_FIELD_SUM_RWP_WIDTH (28)
#define FMDregBank_FMD_FIELD_SUM_STATUS_FIELD_SUM_RWP_MASK (0x0FFFFFFF)

/**
* Register : FMD_SCENE_COUNT_STATUS
* Scene count status register
*/

#define FMDregBank_FMD_SCENE_COUNT_STATUS_SIZE          (32)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_OFFSET        (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x2C)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_RESET_VALUE   (0x00000000)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_BITFIELD_MASK (0x000007FF)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_RWMASK        (0x000007FF)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_ROMASK        (0x00000000)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_WOMASK        (0x00000000)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_UNUSED_MASK   (0xFFFFF800)

/**
* Bit-field : SCENE_COUNT_RWP
* Number of blocks that have their pixels differences |A-D| sum superior to threshold t_scene.
*/

#define FMDregBank_FMD_SCENE_COUNT_STATUS_SCENE_COUNT_RWP_OFFSET (0x00000000)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_SCENE_COUNT_RWP_WIDTH (11)
#define FMDregBank_FMD_SCENE_COUNT_STATUS_SCENE_COUNT_RWP_MASK (0x000007FF)

/**
* Register : FMD_REPEAT_MOVE_STATUS
* Repeat and Move status register
*/

#define FMDregBank_FMD_REPEAT_MOVE_STATUS_SIZE          (32)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_OFFSET        (FMDregBank_fmd_pu_reg_BASE_ADDR + 0x30)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_RESET_VALUE   (0x00000000)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_BITFIELD_MASK (0x00000003)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_RWMASK        (0x00000000)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_ROMASK        (0x00000003)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_WOMASK        (0x00000000)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_UNUSED_MASK   (0xFFFFFFFC)

/**
* Bit-field : MOVE_BBD
* This bit is set if at least one block is a moving block regarding move register threshold.
*/

#define FMDregBank_FMD_REPEAT_MOVE_STATUS_MOVE_BBD_OFFSET (0x00000000)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_MOVE_BBD_WIDTH (1)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_MOVE_BBD_MASK (0x00000001)

/**
* Bit-field : REPEAT_BBD
* This bit is set if there is no moving block regarding t_repeat register threshold.
*/

#define FMDregBank_FMD_REPEAT_MOVE_STATUS_REPEAT_BBD_OFFSET (0x00000001)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_REPEAT_BBD_WIDTH (1)
#define FMDregBank_FMD_REPEAT_MOVE_STATUS_REPEAT_BBD_MASK (0x00000002)

#endif /** _FMDREGBANK_H_ */
