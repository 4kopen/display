/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _HQVDP_CHECK_H_
#define _HQVDP_CHECK_H_

 /**@file hqvdp_check.h
    @brief Function use to check validity of paramters, and patch it if requiered

    This file is only working file and should not be included in production

 */

#include "hqvdp_split.h"

/** @ingroup  check
    @brief Update value compute by split function
    @param[in,out] param : structure containing split result for half part of an image
*/
void hqvdp_check_split_result(struct cmd_split_param *param);

#endif /* _HQVDP_CHECK_H_ */
