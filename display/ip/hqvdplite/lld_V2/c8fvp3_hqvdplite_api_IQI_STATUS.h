/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_IQI_STATUS_H
#define _C8FVP3_HQVDPLITE_API_IQI_STATUS_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : IQI_STATUS
*/


/**
* Register : PXF_IT_STATUS
* Progressive to interlaced pixel fifo interrupt status
*/

#define IQI_STATUS_PXF_IT_STATUS_OFFSET                 (c8fvp3_hqvdplite_api_IQI_STATUS_BASE_ADDR + 0x00)
#define IQI_STATUS_PXF_IT_STATUS_MASK                   (0x00000003)

/**
* Bit-field : END_PROCESSING
* This register indicates that the last pixel on the last line has been read by the P2I core function...
* This bit is reset by the host when command is prepared
*/

#define IQI_STATUS_PXF_IT_STATUS_END_PROCESSING_SHIFT   (0x00000000)
#define IQI_STATUS_PXF_IT_STATUS_END_PROCESSING_WIDTH   (1)
#define IQI_STATUS_PXF_IT_STATUS_END_PROCESSING_MASK    (0x00000001)

/**
* Bit-field : IT_FIFO_EMPTY
* This register is set if the pixels fifo is empty when a gamma request occurs.
* This bit is set by the host when command is prepared
*/

#define IQI_STATUS_PXF_IT_STATUS_IT_FIFO_EMPTY_SHIFT    (0x00000001)
#define IQI_STATUS_PXF_IT_STATUS_IT_FIFO_EMPTY_WIDTH    (1)
#define IQI_STATUS_PXF_IT_STATUS_IT_FIFO_EMPTY_MASK     (0x00000002)

/**
* Register : Y_IQI_CRC
* CRC value luma image quality improvement field
*/

#define IQI_STATUS_Y_IQI_CRC_OFFSET                     (c8fvp3_hqvdplite_api_IQI_STATUS_BASE_ADDR + 0x04)
#define IQI_STATUS_Y_IQI_CRC_MASK                       (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
*/

#define IQI_STATUS_Y_IQI_CRC_CRC_RWP_SHIFT              (0x00000000)
#define IQI_STATUS_Y_IQI_CRC_CRC_RWP_WIDTH              (32)
#define IQI_STATUS_Y_IQI_CRC_CRC_RWP_MASK               (0xFFFFFFFF)

/**
* Register : U_IQI_CRC
* CRC value chroma U image quality improvement field
*/

#define IQI_STATUS_U_IQI_CRC_OFFSET                     (c8fvp3_hqvdplite_api_IQI_STATUS_BASE_ADDR + 0x08)
#define IQI_STATUS_U_IQI_CRC_MASK                       (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
*/

#define IQI_STATUS_U_IQI_CRC_CRC_RWP_SHIFT              (0x00000000)
#define IQI_STATUS_U_IQI_CRC_CRC_RWP_WIDTH              (32)
#define IQI_STATUS_U_IQI_CRC_CRC_RWP_MASK               (0xFFFFFFFF)

/**
* Register : V_IQI_CRC
* CRC value chroma V image quality improvement field
*/

#define IQI_STATUS_V_IQI_CRC_OFFSET                     (c8fvp3_hqvdplite_api_IQI_STATUS_BASE_ADDR + 0x0C)
#define IQI_STATUS_V_IQI_CRC_MASK                       (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
*/

#define IQI_STATUS_V_IQI_CRC_CRC_RWP_SHIFT              (0x00000000)
#define IQI_STATUS_V_IQI_CRC_CRC_RWP_WIDTH              (32)
#define IQI_STATUS_V_IQI_CRC_CRC_RWP_MASK               (0xFFFFFFFF)

/**
* C struct view
* Register block: TOP
*/




/**
* C struct view
* Register block: VC1RE
*/




/**
* C struct view
* Register block: FMD
*/




/**
* C struct view
* Register block: CSDI
*/




/**
* C struct view
* Register block: HVSRC
*/




/**
* C struct view
* Register block: IQI
*/




/**
* C struct view
* Register block: TOP_STATUS
*/




/**
* C struct view
* Register block: FMD_STATUS
*/




/**
* C struct view
* Register block: CSDI_STATUS
*/




/**
* C struct view
* Register block: HVSRC_STATUS
*/




/**
* C struct view
* Register block: IQI_STATUS
*/




#ifndef HQVDPLITE_API_FOR_STAPI
#ifdef FW_STXP70
typedef struct {
    gvh_u32_t PXF_IT_STATUS;  /* at 0 */
    gvh_u32_t Pad1;
    gvh_u32_t Pad2;
    gvh_u32_t Pad3;
    gvh_u32_t Y_IQI_CRC;  /* at 4 */
    gvh_u32_t Pad4;
    gvh_u32_t Pad5;
    gvh_u32_t Pad6;
    gvh_u32_t U_IQI_CRC;  /* at 8 */
    gvh_u32_t Pad7;
    gvh_u32_t Pad8;
    gvh_u32_t Pad9;
    gvh_u32_t V_IQI_CRC;  /* at 12 */
    gvh_u32_t Pad10;
    gvh_u32_t Pad11;
    gvh_u32_t Pad12;
} s_IQI_STATUS;
#else
typedef struct {
    gvh_u32_t PXF_IT_STATUS;  /* at 0 */
    gvh_u32_t Y_IQI_CRC;  /* at 4 */
    gvh_u32_t U_IQI_CRC;  /* at 8 */
    gvh_u32_t V_IQI_CRC;  /* at 12 */
} s_IQI_STATUS;
#endif
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t PxfItStatus;  /* at 0 */
uint32_t YIqiCrc;  /* at 4 */
uint32_t UIqiCrc;  /* at 8 */
uint32_t VIqiCrc;  /* at 12 */
} HQVDPLITE_IQIStatus_Params_t;
#endif /* FW_STXP70 */
#endif
