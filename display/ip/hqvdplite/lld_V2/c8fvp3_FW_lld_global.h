/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef C8FVP3_FW_LLD_GLOBAL_H_
#define C8FVP3_FW_LLD_GLOBAL_H_

#include "hqvdp_lld_platform.h"
#include "c8fvp3_stddefs.h"
#include "c8fvp3_hqvdplite_api_struct.h"

#ifdef HQVDPLITE_API_FOR_STAPI_SANITY_CHECK
#include "hqvdp_firm_rev.h"

#endif /* HQVDPLITE_API_FOR_STAPI_SANITY_CHECK */

#endif
