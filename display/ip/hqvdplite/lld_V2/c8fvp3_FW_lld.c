/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "c8fvp3_FW_lld.h"
#include "hqvdp_mailbox_macros.h"
#include "hqvdplite_ip_mapping.h"
#include "hqvdp_lld_api.h"
#include "hqvdp_handle.h"
#include "hqvdp_lld_platform.h"

#define CEIL(X) ((X-(uint32_t)(X)) > 0 ? (uint32_t)(X+1) : (uint32_t)(X))

/* TODO clarify content of this function
 *  - determined if we really need do a RESET_FULL
 *  - determined if we even need this function
 */
void hqvdp_soft_reset(void *base_addr)
{
         /* set SOFT_VSYNC mode */
         c8fvp3_SetVsyncMode(base_addr, 3);

         /* Fetch enable (Stop the XP70)  */
         REG32_WRITE(base_addr,
              HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_STARTUP_CTRL_2_OFFSET,
              0);

         REG32_WRITE(base_addr,
                     HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SW_RESET_CTRL_OFFSET,
                     mailbox_SW_RESET_CTRL_SW_RESET_CORE_MASK);
/** TODO temporary work-around to address BZ https://bugzilla.stlinux.com/show_bug.cgi?id=55993 */
#ifndef CONFIG_STM_VIRTUAL_PLATFORM
         while ( c8fvp3_IsBootIpDone(base_addr) == FALSE ) {
                 yield();
         }
#endif /* CONFIG_STM_VIRTUAL_PLATFORM */
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void c8fvp3_SetVsyncMode(void *base_addr, uint32_t vsyncmode)
{
        REG32_WRITE(base_addr,
                    HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SOFT_VSYNC_OFFSET,
                    vsyncmode & mailbox_SOFT_VSYNC_VSYNC_MODE_MASK);
        TRC(TRC_ID_HQVDPLITE_LLD, "(HQVDP %p) Set VSYNC mode to %d",
              base_addr, vsyncmode);
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void hqvdp_set_vsync_mode(hqvdp_lld_hdl_t hdl,
                              enum hqvdp_lld_vsync_mode_e vsync_mode)
{
        int i;

        for (i=0; i < hdl->reserved_resources; i++ ) {
                switch (vsync_mode) {
                case HQVDP_HW_VSYNC:
                        c8fvp3_SetVsyncMode(hdl->base_addr[i],
                                        mailbox_SOFT_VSYNC_VSYNC_MODE_B_0x0);
                        break;
                case HQVDP_CMD_PENDING_ONGOING:
                        c8fvp3_SetVsyncMode(hdl->base_addr[i],
                                        mailbox_SOFT_VSYNC_VSYNC_MODE_B_0x1);
                        break;
                default:
                        /* no define mailbox_SOFT_VSYNC_VSYNC_MODE_B_0x3 seems available */
                        c8fvp3_SetVsyncMode(hdl->base_addr[i], 3);
                        break;
                }
        }
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void c8fvp3_GenSoftVsync(void *base_addr)
{
#ifdef CONFIG_STM_VIRTUAL_PLATFORM /* VSOC HCE WA: model crash, to be studied */
        REG32_WRITE(base_addr,
                    HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SOFT_VSYNC_OFFSET,
                    0x0);
#else
        uint32_t vsync_mode;

        vsync_mode = REG32_READ(base_addr,
                                HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SOFT_VSYNC_OFFSET);
        REG32_WRITE(base_addr,
                    HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SOFT_VSYNC_OFFSET,
                    mailbox_SOFT_VSYNC_SW_VSYNC_CTRL_MASK | vsync_mode);
        REG32_WRITE(base_addr,
                    HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SOFT_VSYNC_OFFSET, vsync_mode);
#endif
        /* debug */
        TRC(TRC_ID_HQVDPLITE_LLD,
            "(HQVDP %p) Soft vsynch generated",
            base_addr);
#ifdef CONFIG_STM_VIRTUAL_PLATFORM
        /* Dual hqvdp workaround: this trace prevents (magically) left/right sides "mixed" issue */
        PRINTF("HQVDP - VSOC specific workaround trace to have display working correctly");
#endif
}

void c8fvp3_SetCmdAddress(void *base_addr, uint32_t cmd_address) {
        TRC(TRC_ID_HQVDPLITE_LLD, "(HQVDP %p) HQVDP command physical address = 0x%08X",
              base_addr, cmd_address);
        REG32_WRITE(base_addr, HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_NEXT_CMD_OFFSET,
                    cmd_address);
}

bool c8fvp3_IsReady(void *base_addr)
{
        yield();
        return (((REG32_READ(base_addr,
                             HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_INFO_XP70_OFFSET) &
                  mailbox_INFO_XP70_FW_READY_MASK) ==
                 mailbox_INFO_XP70_FW_READY_MASK) ? 1 : 0);
}

void c8fvp3_BootIp(void *base_addr)
{
          /* authorize idle mode */
          REG32_WRITE(base_addr,
                      HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_STARTUP_CTRL_1_OFFSET,
                      mailbox_STARTUP_CTRL_1_AUTHORIZE_IDLE_MODE_MASK);

          /* soft reset XP70 */
          REG32_WRITE(base_addr,
                      HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SW_RESET_CTRL_OFFSET,
                      mailbox_SW_RESET_CTRL_SW_RESET_CORE_MASK);

          /* fetch enable */
          REG32_WRITE(base_addr,
                      HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_STARTUP_CTRL_2_OFFSET,
                      mailbox_STARTUP_CTRL_2_FETCH_EN_MASK);

}

bool c8fvp3_IsBootIpDone (void *base_addr)
{
        /* wait for reset done */
        yield();
        return (((REG32_READ(base_addr,
                             HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_STARTUP_CTRL_1_OFFSET)
                  & mailbox_STARTUP_CTRL_1_XP70_RESET_DONE_MASK)
                 == mailbox_STARTUP_CTRL_1_XP70_RESET_DONE_MASK) ? 1: 0);
}

void c8fvp3_SetHvsrcLut(HQVDPLITE_CMD_t *FD_HQVDP_Cmd_p,
                        uint8_t filter_strength_yv,
                        uint8_t filter_strength_cv,
                        uint8_t filter_strength_yh,
                        uint8_t filter_strength_ch) {

  uint8_t input_chroma_sampling, output_chroma_sampling;
  uint8_t memtotv;
  uint32_t input_format, output_format;
  uint32_t output_3d_format;
  uint32_t input_3d_format;
  bool OuputIsSbs, OuputIsTab;
  bool InputIs444, OutputIs444;
  bool prog_not_int;
  bool dei_bypass;
  bool shift;
  uint8_t StrengthYV, StrengthCV, StrengthYH, StrengthCH;
  uint8_t shiftYH, shiftCH, shiftYV, shiftCV;
  uint32_t shifth,shiftv;
  uint32_t inputviewportori,inputviewportsize, outputsize;
  uint32_t border_width;
  uint32_t offset3d_left_width, offset3d_right_width;
  uint32_t memformat;

  memformat     = (FD_HQVDP_Cmd_p->Top.MemFormat);
  prog_not_int  = (FD_HQVDP_Cmd_p->Top.Config & TOP_CONFIG_PROGRESSIVE_MASK) >> TOP_CONFIG_PROGRESSIVE_SHIFT;
  dei_bypass    = (FD_HQVDP_Cmd_p->Csdi.Config & CSDI_CONFIG_MAIN_MODE_MASK) >> CSDI_CONFIG_MAIN_MODE_SHIFT;

  memtotv       = (memformat & TOP_MEM_FORMAT_MEM_TO_TV_MASK) >> TOP_MEM_FORMAT_MEM_TO_TV_SHIFT;
  input_format  = (memformat & TOP_MEM_FORMAT_INPUT_FORMAT_MASK) >> TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT;
  output_format = (memformat & TOP_MEM_FORMAT_OUTPUT_FORMAT_MASK) >> TOP_MEM_FORMAT_OUTPUT_FORMAT_SHIFT;

  output_3d_format = (memformat & TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK) >> TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT ;
  input_3d_format = (memformat & TOP_MEM_FORMAT_INPUT_3D_FORMAT_MASK) >> TOP_MEM_FORMAT_INPUT_3D_FORMAT_SHIFT ;

  OuputIsSbs = (output_3d_format == FORMAT_3DTV_SBS)?1:0;
  OuputIsTab = (output_3d_format == FORMAT_3DTV_TAB)?1:0;

  input_chroma_sampling  = (input_format & CHROMA_SAMPLING_MASK) >> CHROMA_SAMPLING_SHIFT;
  output_chroma_sampling = (output_format & CHROMA_SAMPLING_MASK) >> CHROMA_SAMPLING_SHIFT;

  InputIs444 = ((input_chroma_sampling == CHROMA_SAMPLING_444) || (input_chroma_sampling == CHROMA_SAMPLING_NO_ALPHA_444))?1:0;
  OutputIs444 = ((output_chroma_sampling == CHROMA_SAMPLING_444) || (memtotv))?1:0;

  StrengthYV = filter_strength_yv;
  StrengthYH = filter_strength_yh;
  StrengthCV = filter_strength_cv;
  StrengthCH = filter_strength_ch;

  if ((prog_not_int == 0) && (dei_bypass == CSDI_CONFIG_MAIN_MODE_BYPASS)) {
    shift = 1;
  } else {
    shift = 0;
  }


  inputviewportori     = (FD_HQVDP_Cmd_p->Top.InputViewportOri);
  inputviewportsize    = (FD_HQVDP_Cmd_p->Top.InputViewportSize);
  offset3d_left_width  = (FD_HQVDP_Cmd_p->Top.LeftView3doffsetWidth);
  offset3d_right_width = (FD_HQVDP_Cmd_p->Top.RightView3doffsetWidth);
  outputsize           = (FD_HQVDP_Cmd_p->Hvsrc.OutputPictureSize);

  if (output_3d_format == FORMAT_3DTV_SBS) {
    border_width = (((offset3d_left_width  & TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_MASK)   >> TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_SHIFT)  +
                   ((offset3d_left_width  & TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_MASK)  >> TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_SHIFT)  +
                   ((offset3d_right_width & TOP_RIGHT_VIEW_BORDER_WIDTH_LEFT_MASK)  >> TOP_RIGHT_VIEW_BORDER_WIDTH_LEFT_SHIFT)  +
                   ((offset3d_right_width & TOP_RIGHT_VIEW_BORDER_WIDTH_RIGHT_MASK) >> TOP_RIGHT_VIEW_BORDER_WIDTH_RIGHT_SHIFT)) >> 1;
  } else if (output_3d_format == FORMAT_3DTV_TAB) {
    border_width = ((offset3d_left_width  & TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_MASK)  >> TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_SHIFT) +
                   ((offset3d_left_width  & TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_MASK) >> TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_SHIFT);
  } else {
    border_width = ((offset3d_left_width  & TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_MASK)  >> TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_SHIFT) +
                   ((offset3d_left_width  & TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_MASK) >> TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_SHIFT);
  }


  HVSRC_getFilterCoefHV(((inputviewportori & TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_MASK) >> TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_SHIFT),
                        (((inputviewportsize & TOP_INPUT_VIEWPORT_SIZE_WIDTH_MASK) >> TOP_INPUT_VIEWPORT_SIZE_WIDTH_SHIFT) << ((input_3d_format == DOLBY_BASE_ENHANCED_LAYER)?1:0)) + border_width,
                        ((outputsize & HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_MASK) >> HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_SHIFT),
                        (((inputviewportsize & TOP_INPUT_VIEWPORT_SIZE_HEIGHT_MASK) >> TOP_INPUT_VIEWPORT_SIZE_HEIGHT_SHIFT) >> shift),
                        ((outputsize & HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_MASK) >> HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_SHIFT),
                        StrengthYV,
                        StrengthCV,
                        StrengthYH,
                        StrengthCH,
                        InputIs444,
                        OutputIs444,
                        OuputIsSbs,
                        OuputIsTab,
                        FD_HQVDP_Cmd_p->Hvsrc.YhCoef,
                        FD_HQVDP_Cmd_p->Hvsrc.ChCoef,
                        FD_HQVDP_Cmd_p->Hvsrc.YvCoef,
                        FD_HQVDP_Cmd_p->Hvsrc.CvCoef,
                        &shiftYH,
                        &shiftCH,
                        &shiftYV,
                        &shiftCV);

  shifth = (uint32_t) (shiftYH << HVSRC_HORI_SHIFT_Y_SHIFT) | (shiftCH << HVSRC_HORI_SHIFT_C_SHIFT);
  shiftv = (uint32_t) (shiftYV << HVSRC_VERT_SHIFT_Y_SHIFT) | (shiftCV << HVSRC_VERT_SHIFT_C_SHIFT);

  FD_HQVDP_Cmd_p->Hvsrc.HoriShift = shifth;
  FD_HQVDP_Cmd_p->Hvsrc.VertShift = shiftv;
}

enum hqvdp_lld_error c8fvp3_Set3DViewPort(HQVDPLITE_CMD_t *FD_HQVDP_Cmd_p,
                                          int16_t offset3D)
{

/*  bool right_not_left;
  uint32_t input_3d_format;*/
  uint32_t offset_left_Lx, offset_left_Rx;
  uint32_t offset_right_Lx, offset_right_Rx;
  uint32_t VpLx, VpRx, VpLy, VpRy;
  uint32_t VpW, VpH;
  uint16_t wL;
  int32_t tmp;

/*  input_3d_format = (FD_HQVDP_Cmd_p->Top.MemFormat & TOP_MEM_FORMAT_INPUT_3D_FORMAT_MASK) >> TOP_MEM_FORMAT_INPUT_3D_FORMAT_SHIFT ;
  right_not_left  = (FD_HQVDP_Cmd_p->Top.MemFormat & TOP_MEM_FORMAT_RIGHTNOTLEFT_MASK) >> TOP_MEM_FORMAT_RIGHTNOTLEFT_SHIFT; */
  VpLx    = (FD_HQVDP_Cmd_p->Top.InputViewportOri & TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_MASK) >> TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_SHIFT;
  VpRx    = (FD_HQVDP_Cmd_p->Top.InputViewportOriRight & TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_MASK) >> TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_SHIFT;
  VpLy    = (FD_HQVDP_Cmd_p->Top.InputViewportOri & TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_MASK) >> TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_SHIFT;
  VpRy    = (FD_HQVDP_Cmd_p->Top.InputViewportOriRight & TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_MASK) >> TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_SHIFT;
  VpW     = (FD_HQVDP_Cmd_p->Top.InputViewportSize & TOP_INPUT_VIEWPORT_SIZE_WIDTH_MASK) >> TOP_INPUT_VIEWPORT_SIZE_WIDTH_SHIFT;
  VpH     = (FD_HQVDP_Cmd_p->Top.InputViewportSize & TOP_INPUT_VIEWPORT_SIZE_HEIGHT_MASK) >> TOP_INPUT_VIEWPORT_SIZE_HEIGHT_SHIFT;
  wL      = (FD_HQVDP_Cmd_p->Top.InputFrameSize & TOP_INPUT_FRAME_SIZE_WIDTH_MASK) >> TOP_INPUT_FRAME_SIZE_WIDTH_SHIFT;

  offset_left_Lx  = 0;
  offset_left_Rx  = 0;
  offset_right_Lx = 0;
  offset_right_Rx = 0;

  if (offset3D < 0) {
    tmp = VpRx + VpW + (0 - offset3D) - wL;
    if (tmp > 0) {
      offset_right_Rx = (uint16_t) (tmp);
    }
    /* Define the right border width */
    tmp = VpLx - (0 - offset3D);
    if (tmp < 0) {
      offset_left_Lx = (uint16_t)(0 - tmp);
    }
    /* Choice the max border width */
    if (offset_right_Rx < offset_left_Lx) {
      offset_right_Rx = offset_left_Lx;
    } else {
      offset_left_Lx = offset_right_Rx;
    }
    VpW  -= offset_right_Rx;
    VpRx += (0 - offset3D);
    VpLx -= ((0 - offset3D) - offset_right_Rx);
  } else if (offset3D > 0) {
    /* Define the left border width */
    tmp = VpLx + VpW + offset3D - wL;
    if (tmp > 0) {
      offset_left_Rx = (uint16_t) (tmp);
    }
    /* Define the right border width */
    tmp = VpRx - offset3D;
    if (tmp < 0) {
      offset_right_Lx = (uint16_t)(0 - tmp);
    }
    /* Choice the max border width */
    if (offset_left_Rx < offset_right_Lx) {
      offset_left_Rx = offset_right_Lx;
    } else {
      offset_right_Lx = offset_left_Rx;
    }
    VpW  -= offset_left_Rx;
    VpLx += offset3D;
    VpRx -= (offset3D - offset_left_Rx);
    /* offset_right_Lx = 0; */
  }

  FD_HQVDP_Cmd_p->Top.InputViewportOri       = (VpLy << TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_SHIFT) | (VpLx << TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_SHIFT);
  FD_HQVDP_Cmd_p->Top.InputViewportOriRight  = (VpRy << TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_SHIFT) | (VpRx << TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_SHIFT);
  FD_HQVDP_Cmd_p->Top.InputViewportSize      = (VpH << TOP_INPUT_VIEWPORT_SIZE_HEIGHT_SHIFT) | (VpW << TOP_INPUT_VIEWPORT_SIZE_WIDTH_SHIFT);
  FD_HQVDP_Cmd_p->Top.LeftView3doffsetWidth  = (uint32_t)((offset_left_Lx << TOP_LEFT_VIEW_3DOFFSET_WIDTH_LEFT_SHIFT) | (offset_left_Rx << TOP_LEFT_VIEW_3DOFFSET_WIDTH_RIGHT_SHIFT));
  FD_HQVDP_Cmd_p->Top.RightView3doffsetWidth = (uint32_t)((offset_right_Lx << TOP_RIGHT_VIEW_3DOFFSET_WIDTH_LEFT_SHIFT) | (offset_right_Rx << TOP_RIGHT_VIEW_3DOFFSET_WIDTH_RIGHT_SHIFT));

  return HQVDP_LLD_NO_ERR;
}

bool c8fvp3_GetStatus(void *base_addr,
                      HQVDPLITE_STATUS_t *FD_HQVDP_Status_p)
{

        uint32_t status_address;
        uint32_t i;
        uint32_t *Cmd_p;

        status_address = REG32_READ(base_addr,
                                    HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_GP_STATUS_OFFSET);

        if (status_address) {
                Cmd_p = (uint32_t *)FD_HQVDP_Status_p;
                for (i = 0; i < sizeof(HQVDPLITE_STATUS_t) / 4; i++) {
                        Cmd_p[i] = REG32_READ(base_addr,
                                              status_address + (4*4*i));
                }
                TRC(TRC_ID_HQVDPLITE_LLD, "(HQVDP %p) Valid status",
                    base_addr);
                return true;
        } else {
                TRC(TRC_ID_HQVDPLITE_LLD, "(HQVDP %p) No status available",
                    base_addr);
                return false;
        }
}

