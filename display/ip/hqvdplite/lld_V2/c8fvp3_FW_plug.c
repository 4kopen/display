/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "hqvdp_lld_platform.h"
#include "hqvdp_lld_api.h"
/* temporary up we have a clean hqvdp mapping */
#include "hqvdplite_ip_mapping.h"

#include "c8fvp3_FW_plug.h"
#include "c8fvp3_FW_lld_global.h"
#include "hqvdp_ucode_256_rd.h"
#include "hqvdp_ucode_256_wr.h"
#include "InStreamer.h"

void c8fvp3_config_ucodeplug(void *base_addr, uint32_t plug_offset,
                             uint32_t *UcodePlug, uint32_t UcodeSize)
{
        uint32_t cnt = 0;
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_CONTROL_REG_OFFSET,
                    0);
        for (cnt = 0; cnt < UcodeSize; cnt ++) {
                REG32_WRITE(base_addr,plug_offset + cnt * sizeof(*UcodePlug),
                            *(UcodePlug + cnt));
        }
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_CONTROL_REG_OFFSET,
                    1);
}


/**
   @brief Do initialization of the registers of a plug
   @ingroup  lld_local_function
             see plug documentation for details on parameters

   @param[in] base_addr base address HQVDP
   @param[in] plug_offset plug_offset from HQVDP base address
   @param[in] hqvdp_plug_conf register config of plug
 */
void c8fvp3_config_regplug(void *base_addr,
                           uint32_t plug_offset,
                           const struct hqvdp_plug_s *hqvdp_plug_conf)
{
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_PAGE_SIZE_REG_OFFSET,
                    hqvdp_plug_conf->page_size);
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_MIN_OPC_REG_OFFSET,
                    hqvdp_plug_conf->min_opc);
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_MAX_OPC_REG_OFFSET,
                    hqvdp_plug_conf->max_opc);
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_MAX_CHK_REG_OFFSET,
                    hqvdp_plug_conf->max_chk);
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_MAX_MSG_REG_OFFSET,
                    hqvdp_plug_conf->max_msg);
        REG32_WRITE(base_addr, plug_offset + IN_STREAMER_MIN_SPACE_REG_OFFSET,
                    hqvdp_plug_conf->min_space);
}

void c8fvp3_config_rd_ucodeplug(void *base_addr)
{
        c8fvp3_config_ucodeplug(base_addr, HQVDP_LITE_PLUG_READ_UCODE_OFFSET,
                                HQVDP_UcodeRdPlug, HQVDP_UCODERDPLUG_SIZE);
}

void c8fvp3_config_wr_ucodeplug(void *base_addr)
{
        c8fvp3_config_ucodeplug(base_addr, HQVDP_LITE_PLUG_WRITE_UCODE_OFFSET,
                                HQVDP_UcodeWrPlug, HQVDP_UCODEWRPLUG_SIZE);
}


void c8fvp3_config_rd_regplug(void *base_addr,
                              const struct hqvdp_plug_s *hqvdp_plug_conf)
{
        c8fvp3_config_regplug(base_addr, HQVDP_LITE_PLUG_READ_UCODE_OFFSET,
                              hqvdp_plug_conf);
}

void c8fvp3_config_wr_regplug(void *base_addr,
                              const struct hqvdp_plug_s *hqvdp_plug_conf)

{
        c8fvp3_config_regplug(base_addr, HQVDP_LITE_PLUG_WRITE_UCODE_OFFSET,
                              hqvdp_plug_conf);
}
