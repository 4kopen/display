/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "c8fvp3_aligned_viewport.h"

void hor_viewport_alignement(bool OutputSbsFlag,
                             uint16_t InputViewportX,
                             uint16_t InputViewportRightX,
                             uint16_t InputViewportWidth,
                             uint16_t *AlignedViewportX,
                             uint16_t *AlignedViewportRightX,
                             uint16_t *AlignedViewportWidth)
{
#ifdef FW_STXP70
  printf_on_debug("\tCU:: %s \n", __FUNCTION__);
#endif

  *AlignedViewportX      = InputViewportX & 0xFFFFFFFE;
  if (AlignedViewportRightX != 0) {
    *AlignedViewportRightX = OutputSbsFlag?InputViewportRightX:(InputViewportRightX & 0xFFFFFFFE);
  }
  *AlignedViewportWidth  = InputViewportWidth + (InputViewportX & 0x1);
  *AlignedViewportWidth += OutputSbsFlag?0:(*AlignedViewportWidth & 0x1);
}

void ver_viewport_alignement(bool ChromaSampling420Not422,
                             bool ProgressiveFlag,
                             bool OutputTabFlag ,
                             uint16_t InputViewportY,
                             uint16_t InputViewportRightY,
                             uint16_t InputViewportHeight,
                             uint16_t *AlignedViewportY,
                             uint16_t *AlignedViewportRightY,
                             uint16_t *AlignedViewportHeight,
                             uint16_t *AlignedViewportRightHeight)
{
#ifdef FW_STXP70
  printf_on_debug("\tCU:: %s \n", __FUNCTION__);
#endif

  if(ChromaSampling420Not422 && (!ProgressiveFlag)) {
    *AlignedViewportY      = InputViewportY & 0xFFFFFFFC;
    if (AlignedViewportRightY != 0) {
      *AlignedViewportRightY = OutputTabFlag?InputViewportRightY:(InputViewportRightY & 0xFFFFFFFC);
    }
    *AlignedViewportHeight = InputViewportHeight  + (InputViewportY - (*AlignedViewportY));
    if (OutputTabFlag) {
      *AlignedViewportRightHeight = InputViewportHeight + (((*AlignedViewportHeight + 3) >> 2) << 2) - (*AlignedViewportHeight);
    } else {
      *AlignedViewportHeight = ((*AlignedViewportHeight + 3) >> 2) << 2;
      *AlignedViewportRightHeight = 0;
    }
  }
  else {
    *AlignedViewportY      = InputViewportY & 0xFFFFFFFE;
    if (AlignedViewportRightY != 0) {
      *AlignedViewportRightY = OutputTabFlag?InputViewportRightY:(InputViewportRightY & 0xFFFFFFFE);
    }
    *AlignedViewportHeight = InputViewportHeight + (InputViewportY & 0x1);
    *AlignedViewportHeight = OutputTabFlag?(*AlignedViewportHeight):(((*AlignedViewportHeight + 1) >> 1) << 1);
    *AlignedViewportRightHeight = OutputTabFlag?(*AlignedViewportHeight):0;
  }
}

