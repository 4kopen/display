/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_ALIGNED_VIEWPORT_H
#define _C8FVP3_ALIGNED_VIEWPORT_H

#ifdef FW_STXP70
#include "c8fvp3_stddefs.h"
#else
#include "hqvdp_lld_platform.h"
#endif /* (FW_STXP70) && !defined (TLM_NATIVE */

/*!
 * \brief Do initializations that shall be done one time per picture. Aligned
 *  horizontal viewport for a single picture
 *
 * Assumptions : This function is called after the command has been transfered.
 * \param[in] OutputSbsFlag          Define if output 3D format is Side by Side
 * \param[in] InputViewportX         Define the horizontal origin of the left side
 * \param[in] InputViewportRightX    Define the horizontal origin of the right side
 * \param[in] InputViewportWidth     Define the horizontal viewport size.
 * \param[out] AlignedViewportX      Define the horizontal aligned origin of the left side
 * \param[out] AlignedViewportRightX Define the horizontal aligned origin of the right side
 * \param[out] AlignedViewportWidth  Define the horizontal aligned viewport size
 */
extern void hor_viewport_alignement(bool OutputSbsFlag,
                                    uint16_t InputViewportX,
                                    uint16_t InputViewportRightX,
                                    uint16_t InputViewportWidth,
                                    uint16_t *AlignedViewportX,
                                    uint16_t *AlignedViewportRightX,
                                    uint16_t *AlignedViewportWidth);


/*!
 * \brief Do initializations that shall be done one time per picture. Aligned
 *  vertical viewport for a single picture
 *
 * Assumptions : This function is called after the command has been transfered.
 * \param[in] ChromaSampling420Not422      Define the chroma sampling
 * \param[in] ProgressiveFlag              Define if input material is progressive
 * \param[in] OutputTabFlag                Define if output 3D format is Top and Bottom
 * \param[in] InputViewportY               Define the vertical origin of the left side
 * \param[in] InputViewportRightY          Define the vertical origin of the right side
 * \param[in] InputViewportHeight          Define the vertical viewport size.
 * \param[out] AlignedViewportY            Define the vertical aligned origin of the left side
 * \param[out] AlignedViewportRightY       Define the vertical aligned origin of the right side
 * \param[out] AlignedViewportHeight       Define the vertical aligned viewport size of the left side
 * \param[out] AlignedViewportRightHeight  Define the vertical aligned viewport size of the right side
 */
extern void ver_viewport_alignement(bool ChromaSampling420Not422,
                                    bool ProgressiveFlag,
                                    bool OutputTabFlag,
                                    uint16_t InputViewportY,
                                    uint16_t InputViewportRightY,
                                    uint16_t InputViewportHeight,
                                    uint16_t *AlignedViewportY,
                                    uint16_t *AlignedViewportRightY,
                                    uint16_t *AlignedViewportHeight,
                                    uint16_t *AlignedViewportRightHeight);


#endif
