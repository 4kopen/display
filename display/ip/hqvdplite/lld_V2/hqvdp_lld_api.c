/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/** TODO determined if we are really able to directly include this file to
 *  manage version number return by LLD
#include "hqvdp_firm_rev.h"
*/
#include "hqvdp_handle.h"
#include "hqvdp_lld_platform.h"
#include "c8fvp3_FW_lld.h"
#include "c8fvp3_FW_plug.h"
#include "hqvdp_split.h"
#include "hqvdp_split_algo.h" /* needed only for HQVDPSplitProgrammation */
/* temporary up we have a clean hqvdp mapping */
#include "hqvdplite_ip_mapping.h"

#include "hqvdp_mailbox_macros.h"
#include "c8fvp3_hqvdplite_api_HVSRC.h"

#ifdef VERIFICATION_PLATFORM

#endif /* VERIFICATION_PLATFORM */

/*----------------------------------------------------------------------------*/
/** @defgroup lld_local_function LLD local functions */
/*----------------------------------------------------------------------------*/

/** @brief struct to store information specific to a given hqvdp hardware
    @ingroup lld_priv_struct
*/
struct hqvdp_hw_s {
        void *base_addr; /**< @brief table containing base address of each HQVDP */
        bool boot_done; /**< @brief table containing boot_done by HQVDP */
        hqvdp_lld_hdl_t ressource_reserved; /**< @brief handle by which hardware instance has been reserved */
        struct hqvdp_lld_fw_s fw; /**< @brief information required to load firmware */
        void *video_plug_handle; /**<@brief handle to know associated video plug */
        uint32_t mixer_bit; /**<@brief bit in CTL register allowing to enable video plug in compo */
};

/** @brief struct to save information relative to driver status
    @ingroup lld_priv_struct
*/
struct hqvdp_global_info_s {
        bool init_done;  /**< @brief initialize has bee done */
        int hqvdp_nb; /**< @brief number of hqvdp available (info provided by driver) */
        int available_ressources; /**< @brief Number of hardware resources not yet reserved */
        struct hqvdp_hw_s *hw; /**< @brief memorization of information provided during #hqvdp_lld_init */
};

/** @brief store size info of a picture
    @ingroup lld_priv_struct
*/
struct picture_size_s {
        uint16_t width;
        uint16_t height;
};

static struct hqvdp_global_info_s  hqvdp_global_info;

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
/** @ingroup  lld_local_function
    @brief Handle correspond to a valid handle

    @retval 1 handle is valid
    @retval 0 handle is not valid

*/
/* TODO check if we would like to inline function */
bool hqvdp_is_handle_valid(hqvdp_lld_hdl_t hdl)
{
        if ( hdl->magic == 0x12345678 )
                return 1;
        else
                return 0;
}


const char *hqvdp_lld_get_firmware_package_version(void)
{
        /** TODO find a better way to ensure alignment with firmware package
          * version
          */
        char *hqvdp_firware_package_version = (char *)"v5.7.3";
        /* char *hqvdp_firware_package_version = HQVDP_FW_VERSION; */

        TRC(TRC_ID_HQVDPLITE_LLD, "Firmware package version : %s",
            hqvdp_firware_package_version);

        /* return API revision */
        return hqvdp_firware_package_version;
}


/** @ingroup  lld_local_function
    @brief Initialize hqvdp_lld_mem_desc_s structure
    @param[out] mem_desc : memory address full description
*/
static void hqvdp_clear_mem_desc(struct hqvdp_lld_mem_desc_s *mem_desc)
{
        mem_desc->size = 0;
        mem_desc->alignment = 0;
        mem_desc->logical = NULL;
        mem_desc->physical = NULL;
}

/** @ingroup  lld_local_function
    @brief Reset driver status global variable
    @param [in]  hqvdp_nb number of HQVDP HW instances on the SOC
    @param [out] global_info Global status
*/
static void hqvdp_init_status(int hqvdp_nb,
                              struct hqvdp_global_info_s *global_info )
{
        int i;

        /* clean all previous status */
        global_info->init_done = false;

        global_info->hqvdp_nb = hqvdp_nb;
        for ( i = 0 ; i < hqvdp_nb; i++ ) {
                global_info->hw[i].base_addr = 0;
                global_info->hw[i].boot_done = 0;
                global_info->hw[i].ressource_reserved = NULL;
                /* initialize firmware info */
                global_info->hw[i].fw.mode = FW_LOAD_T1;
                hqvdp_clear_mem_desc(&(global_info->hw[i].fw.pmem));
                hqvdp_clear_mem_desc(&(global_info->hw[i].fw.dmem));
        }
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_init (int hqvdp_nb,
                                     const struct hqvdp_lld_init_s init_param[]
                                    )
{
        int i;

        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

        hqvdp_global_info.hw = NULL;

        hqvdp_global_info.hw = (struct hqvdp_hw_s *)ALLOCATE_LLD_MEM(sizeof(struct hqvdp_hw_s)
                                 * hqvdp_nb);
        if ( hqvdp_global_info.hw == NULL)
                goto out;
        MEMSET(hqvdp_global_info.hw,0,sizeof(struct hqvdp_hw_s));

        hqvdp_init_status(hqvdp_nb, &hqvdp_global_info);

        for ( i = 0 ; i < hqvdp_nb; i++ ) {
                hqvdp_global_info.hw[i].base_addr = (void *)(init_param[i].hqvdp_base_addr);
                hqvdp_global_info.hw[i].video_plug_handle = init_param[i].video_plug_handle;
                hqvdp_global_info.hw[i].mixer_bit = init_param[i].mixer_bit;
                hqvdp_global_info.hw[i].fw = init_param[i].fw;
        }

        hqvdp_global_info.available_ressources = hqvdp_nb;
        hqvdp_global_info.init_done = true;

        /* no way to detect error in this type of function */
        return HQVDP_LLD_NO_ERR;

out:    if (  hqvdp_global_info.hw )
                FREE_LLD_MEM(hqvdp_global_info.hw);

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        return HQVDP_LLD_ERR_INIT_NOT_DONE;
}

/* Motion buffer size is size of interleave LUMA buffer */
#define MOTION_BUFFER_WIDTH_HD (1920)
#define MOTION_BUFFER_HEIGHT_HD (544) /* TODO understand why it is 544 */
#define OVERLAP_WIDTH (128)

/** @brief add motion buffer size in handle
    @param[in,out] handle of session
*/
int hqvdp_get_motion_size(enum hqvdp_lld_profile_e profile)
{
        /* no interleaved format exist in 4K, so motion buffer allocated */
        switch (profile) {
        case HQVDP_PROFILE_4KP60:
                return (MOTION_BUFFER_WIDTH_HD + 2 * OVERLAP_WIDTH)
                       * MOTION_BUFFER_HEIGHT_HD;
                break;
        default:
                return MOTION_BUFFER_WIDTH_HD * MOTION_BUFFER_HEIGHT_HD;
        }
}

int hqvdp_get_cmd_size(void)
{
        return sizeof(HQVDPLITE_CMD_t) + sizeof(HQVDPLITE_STATUS_t);
}

void hqvdp_add_motion_size(hqvdp_lld_hdl_t hdl)
{
        hdl->motion_buffer_size = hqvdp_get_motion_size(hdl->profile);
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/

enum hqvdp_lld_error hqvdp_lld_get_memory_needs(enum hqvdp_lld_profile_e profile,
                                struct hqvdp_lld_mem_need_s *memory_needs )
{
        int hw_used=1;
        enum hqvdp_lld_error result = HQVDP_LLD_NO_ERR;

        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

        switch (profile) {
        case HQVDP_PROFILE_4KP60:
                hw_used = 2;
                break;
        case HQVDP_PROFILE_4KP30:
        case HQVDP_PROFILE_FULL_HD:
        case HQVDP_PROFILE_4KP60_PR:
                hw_used = 1;
                break;
        default:
                result = HQVDP_LLD_ERR_INVALID_PARAM;
                return result;
                break;
        }

        /* ping pong size is 2 commands */
        memory_needs->cmd_size = hqvdp_get_cmd_size() * hw_used * 2;

        memory_needs->motion_buffer_size = hqvdp_get_motion_size(profile);

        /* 128 bits aligned as SoC interconnect is 128 bits */
        memory_needs->cmd_alignment = 16;
        /* 128 bits aligned as SoC interconnect is 128 bits */
        memory_needs->motion_buffer_alignment = 16;

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        return result;

}

/** @ingroup  lld_local_function
    @brief initialize the command location in handler
    @param[in] mem_for_cmd: provide information about memory reserved for command
    @param[in] hw_inst: define which hardware instance is selected
    @param[in] side: define which HQVDP side have to be consider.
                     0 left
                     1 right
    @param[out] hdl: handle
*/
static void hqvdp_init_ctxt_cmd(const struct hqvdp_lld_mem_desc_s *mem_for_cmd,
                                int hw_inst, int side, hqvdp_lld_hdl_t hdl )
{
        int hqvdp_cmd_size;

        hqvdp_cmd_size = hqvdp_get_cmd_size();

        /* hw instance specific info */
        hdl->base_addr[side] = hqvdp_global_info.hw[hw_inst].base_addr;
        hdl->video_plug_handle[side] = hqvdp_global_info.hw[hw_inst].video_plug_handle;
        hdl->mixer_bit[side] = hqvdp_global_info.hw[hw_inst].mixer_bit;
        hdl->fw[side] = hqvdp_global_info.hw[hw_inst].fw;

        /* side specific */
        /* do not initialized ping pong buffer content */
        hdl->ctxt_cmd[side].cmd_used.logical = (void *)
                ((uintptr_t)mem_for_cmd->logical + (2 * side + 1) * hqvdp_cmd_size);
        hdl->ctxt_cmd[side].cmd_used.physical = (void *)
                ((uint32_t)mem_for_cmd->physical + (2 * side + 1) * hqvdp_cmd_size);
        hdl->ctxt_cmd[side].cmd_used.size = hqvdp_get_cmd_size();

        hdl->ctxt_cmd[side].cmd_next_vsync.logical = (void *)
                ((uintptr_t)mem_for_cmd->logical + (2 * side) * hqvdp_cmd_size);
        hdl->ctxt_cmd[side].cmd_next_vsync.physical = (void *)
                ((uint32_t)mem_for_cmd->physical + (2 * side) * hqvdp_cmd_size);
        hdl->ctxt_cmd[side].cmd_next_vsync.size = hqvdp_cmd_size;

        /* we don't care about alignment in this context */
        hdl->ctxt_cmd[side].cmd_used.alignment = 0;
        hdl->ctxt_cmd[side].cmd_next_vsync.alignment = 0;
}

/** @brief Initialize STBus plug resgisters
    @ingroup lld_internal_function
    @param[in] base_addr : base address of HQVDP
    @param[in] hqvdp_plug_rd_conf : configuration of read plug
    @param[in] hqvdp_plug_wr_conf : configuration of write plug
*/
static void hqvdp_init_regplugs(void *base_addr,
                                const struct hqvdp_plug_s *hqvdp_plug_rd_conf,
                                const struct hqvdp_plug_s *hqvdp_plug_wr_conf)
{
        c8fvp3_config_rd_regplug(base_addr,
                                 hqvdp_plug_rd_conf);
        c8fvp3_config_wr_regplug(base_addr,
                                 hqvdp_plug_wr_conf);
}

/** @brief Initialize STBus plug ucode
    @ingroup lld_internal_function
    @param[in] base_addr : base address of HQVDP
*/
static void hqvdp_init_ucodeplugs(void *base_addr)
{
        c8fvp3_config_rd_ucodeplug(base_addr);
        c8fvp3_config_wr_ucodeplug(base_addr);
}

/** @brief load xp70 firmware
    @ingroup lld_internal_function
    @param[in] base_addr: HQVDP base address
    @param[in] fw: Information to use to load fw
*/
static void hqvdp_load_firmware(void *base_addr, const struct hqvdp_lld_fw_s *fw)
{
        /* load DMEM */
        c8fvp3_download_code(base_addr, fw->mode, &(fw->dmem),
                             HQVDP_CTRL_CLUSTER_DMEM_OFFSET);
        /* load PMEM */
        c8fvp3_download_code(base_addr, fw->mode, &(fw->pmem),
                             HQVDP_CTRL_CLUSTER_IMEM_OFFSET);
}


/** @brief start boot sequence
    @param[in] base_addr: HQVDP base address

    @pre #hqvdp_init_regplugs has been called (in case of load with Streamer)
    @pre #hqvdp_init_ucodeplugs has been called (in case of load with Streamer)
    @pre #hqvdp_load_firmware has been called
*/
static void hqvdp_boot(void * base_addr)
{
        c8fvp3_BootIp(base_addr);

        /* workaround hang up VSOC */
        /* DEBUGF2 (1, ("****** BootIp ***** ")); */

        while( c8fvp3_IsBootIpDone(base_addr) == FALSE);

        /* Test if Bilbo is ready to receive first command */
        while( c8fvp3_IsReady(base_addr) == FALSE);

        /* Send a NULL command to the firmware */
        c8fvp3_SetCmdAddress(base_addr, (uint32_t)NULL);

}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_open (enum hqvdp_lld_profile_e profile,
                                     enum hqvdp_lld_vsync_mode_e vsync_mode,
                                     const struct hqvdp_lld_mem_desc_s *mem_for_cmd,
                                     const struct hqvdp_lld_stbp_setup_s *stbp_setup,
                                     uint32_t *hw_used_bit_mask,
                                     hqvdp_lld_hdl_t *hdl_p)
{
        hqvdp_lld_hdl_t hqvdp_ctxt = NULL;
        enum hqvdp_lld_error result = HQVDP_LLD_NO_ERR;
        int hqvdp_to_use;
        int i;
        int hw_inst;
        int side;

        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");


        /* init has to be done previously */
        if (hqvdp_global_info.init_done == false) {
                result = HQVDP_LLD_ERR_INIT_NOT_DONE;
                goto out;
        }
        /* determine how many HQVDP are required */
        switch (profile) {
        case HQVDP_PROFILE_4KP60:
                hqvdp_to_use = 2;
                break;
        case HQVDP_PROFILE_4KP30:
        case HQVDP_PROFILE_FULL_HD:
        case HQVDP_PROFILE_4KP60_PR:
                hqvdp_to_use = 1;
                break;
        default:
                hqvdp_to_use = -1;
                result = HQVDP_LLD_ERR_INVALID_PARAM;
                goto out;
        }

        /* exit if no enough resources are available */
        if ( hqvdp_to_use > hqvdp_global_info.available_ressources ) {
                result = HQVDP_LLD_ERR_NO_RESSOURCE_AVAILABLE;
                goto out;
        }

        hqvdp_ctxt = (hqvdp_lld_hdl_t)ALLOCATE_LLD_MEM(sizeof(struct hqvdp_lld_ctxt));
        if ( hqvdp_ctxt == NULL)
                goto out;
        MEMSET(hqvdp_ctxt,0,sizeof(struct hqvdp_lld_ctxt));

        hqvdp_ctxt->profile = profile;
        hqvdp_add_motion_size(hqvdp_ctxt);

        /* defined which HQVDP to use */
        hw_inst = 0;
        side = 0; /* 0 : left, 1: right */
        *hw_used_bit_mask = 0;

        /** TODO update this part to hard code
         *         - 4K2K_P60 profile -> HQVDP[1:0]
         *         - other            -> HQVDP[2]
         */
        for ( i = 0; i < hqvdp_global_info.hqvdp_nb; i ++ ) {
                if ( hqvdp_global_info.hw[i].ressource_reserved == 0 ) {
                        /* tag resources as reserved */
                        hqvdp_global_info.hw[i].ressource_reserved = hqvdp_ctxt;

                        /* initialize handle with global_info */
                        hqvdp_init_ctxt_cmd(mem_for_cmd, i, side,
                                            hqvdp_ctxt );
                        /* do a soft reset of XP70 and FULL macro */
                        /** TODO check it is really required */
                        hqvdp_soft_reset(hqvdp_ctxt->base_addr[side]);

                        if ( stbp_setup != NULL ) {
                          hqvdp_init_regplugs(hqvdp_ctxt->base_addr[side],
                                              &(stbp_setup->read_plug),
                                              &(stbp_setup->write_plug));
                        }

                        hqvdp_init_ucodeplugs(hqvdp_ctxt->base_addr[side]);

                        hqvdp_load_firmware(hqvdp_ctxt->base_addr[side],
                                            &hqvdp_ctxt->fw[side]);
                        hqvdp_boot(hqvdp_ctxt->base_addr[side]);

                        /* provide info to easily configure crossbar */
                        *hw_used_bit_mask |= hqvdp_ctxt->mixer_bit[side];

                        /* update global status */
                        hqvdp_global_info.hw[i].boot_done = true;
                        hqvdp_global_info.available_ressources--;

                        hw_inst++;
                        side++;
                }
                if ( hqvdp_to_use == hw_inst ) {
                        /* do post increment to know how many hardware resources
                        * has been reserved
                        */
                        i++;
                        /* Memorize how many HQVDP are used for this plan */
                        hqvdp_ctxt->reserved_resources = hw_inst;
                        break;
               }
        }
        /* not enough hardware resources are available */
        if ( hqvdp_to_use > i ) {
                result = HQVDP_LLD_ERR_UNDEFINED;
                goto out;
        }
        /* store where to store command and motion buffer in central memory */
        /* TODO */

        /* initialize control handler */
        hqvdp_ctxt->state = open_done;
        hqvdp_ctxt->magic = 0x12345678;

        hqvdp_set_vsync_mode(hqvdp_ctxt, vsync_mode);

        /* return handler */
        *hdl_p = hqvdp_ctxt;

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        return HQVDP_LLD_NO_ERR;

out:
        if (hqvdp_ctxt)
                FREE_LLD_MEM(hqvdp_ctxt);
        return result;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void hqvdp_lld_gen_soft_vsync(hqvdp_lld_hdl_t hdl)
{
        int i;

        for (i=0; i < hdl->reserved_resources; i++ ) {
                c8fvp3_GenSoftVsync(hdl->base_addr[i]);
        }
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
uint32_t hqvdp_lld_get_hw_version(hqvdp_lld_hdl_t hdl) {

        /* store in a table list */
        uint32_t version_out;
        uint32_t version;
        int hw_use_inst = 0;
        int i;
        bool is_version_aligned = true;

        /* invalid session */
        if (hqvdp_is_handle_valid(hdl) == 0 )
                return 0;

        /* get version of each hardware */
        hw_use_inst = hdl->reserved_resources;

        /* check that HQVDP has been registered */
        if (hw_use_inst == 0) {
                TRC(TRC_ID_HQVDPLITE_LLD,
                    "[ERROR] no HQVDP hardware registered");
                return 0;
        }

        /* read all available version */
        i = 0;
        do {
                /* TODO include address of version register in official mapping  */
                version = REG32_READ(hdl->base_addr[i], HQVDP_MBX_VERSION_OFFSET);
                TRC(TRC_ID_HQVDPLITE_LLD,
                    "(HQVDP %p) HW version = 0x%08X",
                    hdl->base_addr[i], version);
                if (i == 0)
                        version_out = version;
                else if ( version_out != version )
                        is_version_aligned = false;
                i++;
        } while ( i < hw_use_inst );

        /** TODO this implementation do not really make sense */
        if ( is_version_aligned )
                return version_out;
        else
                return 0xFFFFFFFF;
}

/** @ingroup  lld_local_function
    @brief determined if split is required or not
    @retval 1: command has to be split
            0: not split is required
*/
static bool hqvdp_split_required(enum hqvdp_lld_profile_e profile, /**< [in] : profile of session. */
                                 const uint32_t input_picture_height,
                                 const struct HQVDPLITE_OUT_Params_s *out_param /**< [in] : parameters of VTG output. */
                                 )
{
        switch(profile){
        case HQVDP_PROFILE_4KP60:
                /* not documented feature to force split, used in verification
                 * force split on very small image
                 */
                if ( out_param->output_vtg_fps == 0xFFFFFFFF )
                        return true;

                /* split only in case VTG output is
                 *      bigger than HD (2640 is HD P50)
                 *      FPS > P30
                 * Do not split in case
                 *      VTG is SD, HD
                 *      VTG is 4k with FPS <= 30
                 * It could be refined by adding criteria on size of output image
                 */
                if ( (input_picture_height > 1088)
                  || ((out_param->output_vtg_width > 2640) && (out_param->output_vtg_fps > 30000) ))
                        return true;
                else
                        return false;
        default:
                return false;
        }
}

/** @ingroup  lld_local_function
    @brief determined if pixel repeat is required or not
    @retval 1: enable pixel repeat
            0: disable pixel repeat
*/
static bool hqvdp_pixel_repeat_required(hqvdp_lld_hdl_t hdl,
                const struct hqvdp_lld_conf_s *conf)

{
        switch (hdl->profile) {
        case HQVDP_PROFILE_4KP60_PR:
                /*not documented feature to force split, used in verification
                 force split on very small image*/

                if (conf->Out.output_vtg_fps == 0xFFFFFFFF)
                        return true;

                /*split only in case VTG output is
                 bigger than HD (2640 is HD P50)
                 FPS > P30
                 Do not split in case
                 VTG is SD, HD
                 VTG is 4k with FPS <= 30
                 It could be refined by adding criteria on size of output image*/

                if (conf->Out.output_vtg_width > 2640
                                && conf->Out.output_vtg_fps > 30000
                                && ((conf->Hvsrc.OutputPictureSize
                                                & HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_MASK)
                                                >> HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_SHIFT)
                                                > 1920)
                        return true;
                else
                        return false;
        default:
                return false;
        }
}


/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_get_number_of_hardware(
                                    hqvdp_lld_hdl_t                       hdl,
                                    const uint32_t                        input_picture_height,
                                    const struct HQVDPLITE_OUT_Params_s * out_param,
                                    int                                 * hardware_nb)
{
    TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

    /* check handle is valid */
    if ( hqvdp_is_handle_valid(hdl) == 0 )
            return HQVDP_LLD_ERR_INVALID_PARAM;

    if ( hqvdp_split_required(hdl->profile, input_picture_height, out_param) )
            *hardware_nb = 2;
    else
            *hardware_nb = 1;

    TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

    return HQVDP_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_get_split_width(hqvdp_lld_hdl_t hdl,
                                               uint16_t        input_width,
                                               uint16_t        output_width,
                                               uint16_t        *input_split_width,
                                               uint16_t        *output_split_width)
{
    struct error_split_param error_split;
    struct cmd_origi_param input_param;
    struct cmd_split_param output_param[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE];

    TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

    if ( hqvdp_is_handle_valid(hdl) == 0 )
            return HQVDP_LLD_ERR_INVALID_PARAM;

    input_param.input_viewportx = 0;
    input_param.input_viewport_width = input_width;
    input_param.output_width = output_width;
    input_param.init_phase_luma = 0;
    input_param.xdo = 0;

    error_split = HQVDPSplitProgrammation(input_param, &output_param[0], &output_param[1]);

    *input_split_width  = output_param[0].input_viewport_width;
    *output_split_width = output_param[0].output_width;

    TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

    return HQVDP_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static void hqvdp_switch_cmd(hqvdp_lld_hdl_t hdl)
{
        int i;
        /* structure containing pointer to logical and physical address */
        struct hqvdp_lld_mem_desc_s tmp;

        for (i = 0; i < hdl->reserved_resources; i++ ) {
                tmp = hdl->ctxt_cmd[i].cmd_next_vsync;
                hdl->ctxt_cmd[i].cmd_next_vsync = hdl->ctxt_cmd[i].cmd_used;
                hdl->ctxt_cmd[i].cmd_used = tmp;
        }
}

enum hqvdp_lld_error hqvdp_lld_set_conf(hqvdp_lld_hdl_t hdl,
                                        const struct hqvdp_lld_conf_s *conf,
                                        int *setup_nb,
                                        struct hqvdp_lld_compo_setup_s compo_setup[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE]
                                       )
{
        uint32_t input_picture_height = 0;
        struct picture_size_s output_picture_size;
        bool split_required = false;
        uint8_t hw_to_use_cnt = 0;

        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

        /* check handle is valid */
        if ( hqvdp_is_handle_valid(hdl) == 0 )
                return HQVDP_LLD_ERR_INVALID_PARAM;

        /* get image size in human readable way */
        input_picture_height =
                (conf->Top.InputViewportSize & TOP_INPUT_VIEWPORT_SIZE_HEIGHT_MASK)
                >> TOP_INPUT_VIEWPORT_SIZE_HEIGHT_SHIFT;

        output_picture_size.height =
                (conf->Hvsrc.OutputPictureSize & HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_MASK)
                >> HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_SHIFT;
        output_picture_size.width =
                (conf->Hvsrc.OutputPictureSize & HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_MASK)
                >> HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_SHIFT;

        split_required = hqvdp_split_required(hdl->profile, input_picture_height, &(conf->Out));

        hw_to_use_cnt = split_required ? 2 : 1;

        hdl->used_resources = hw_to_use_cnt;

        hdl->pixel_repeat = hqvdp_pixel_repeat_required(hdl, conf);

        /* get the  right hvsrc lut*/
        if (conf->LutHvsrc.custom == false) {
                /* TODO integrate directly the wrapping in native */
                c8fvp3_SetHvsrcLut((HQVDPLITE_CMD_t *)conf,
                                   (uint8_t)conf->LutHvsrc.luma_v_strength, (uint8_t)conf->LutHvsrc.chroma_v_strength,
                                   (uint8_t)conf->LutHvsrc.luma_h_strength, (uint8_t)conf->LutHvsrc.chroma_h_strength
                                  );
        }

        /* write command in memory using 1 or 2 HQVDP */
        hqvdp_write_cmd(hdl, conf, hw_to_use_cnt, compo_setup);

        /* update pointer to be ready for next time */
        hqvdp_switch_cmd(hdl);

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        *setup_nb = hdl->reserved_resources;
        hdl->state = conf_done;

        return HQVDP_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_get_status (hqvdp_lld_hdl_t hdl,
                                           int *status_cnt,
                                           struct hqvdp_lld_status_s *status)
{
        int i;
        bool ret;
        HQVDPLITE_STATUS_t *status_p;

        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

        *status_cnt = 0;

        if ( hqvdp_is_handle_valid(hdl) == 0 )
                return HQVDP_LLD_ERR_INVALID_PARAM;

        /* always read status of all reserved HQVDP, if a NULL command has been
         * processed, status is not return
         */
        for ( i = 0; i < hdl->reserved_resources; i++ ) {

                /* trick used to managed name conversion */
                status_p = (HQVDPLITE_STATUS_t *)&status[*status_cnt];

                ret = c8fvp3_GetStatus((void *)hdl->base_addr[i], status_p);
                /* ret = 0 if null command */
                if ( ret == 1 ) {
                        (*status_cnt)++;
                }
        }

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        return HQVDP_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_stop (hqvdp_lld_hdl_t hdl)
{
        int i;

        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

        if ( hqvdp_is_handle_valid(hdl) == 0 )
                return HQVDP_LLD_ERR_INVALID_PARAM;

        /* stop processing of all reserved hqvdp */
        for (i = 0; i < hdl->reserved_resources; i++ ) {
                c8fvp3_SetCmdAddress(hdl->base_addr[i], (uint32_t)NULL);
        }
        hdl->state = stop_done;

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        return HQVDP_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_close(hqvdp_lld_hdl_t hdl)
{
        int i;

        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

        /* check previous condition */
        if (hdl->state == conf_done )
                return HQVDP_LLD_ERROR_NO_STOP_DONE;

        /* clear the magic flag to detect incorrect handle usage */
        hdl->magic = 0;

        /* update available resourced */
        hqvdp_global_info.available_ressources +=
                hdl->reserved_resources;

        /* free previously reserved resource */
        for (i = 0; i < hqvdp_global_info.hqvdp_nb; i++ ) {
                if ( hqvdp_global_info.hw[i].ressource_reserved == hdl )
                        hqvdp_global_info.hw[i].ressource_reserved = NULL;
        }

        /* free handle */
        FREE_LLD_MEM(hdl);

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        return HQVDP_LLD_NO_ERR;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
enum hqvdp_lld_error hqvdp_lld_terminate(void)
{
        TRCIN(TRC_ID_HQVDPLITE_LLD, " ");

        /* free structure used to store lld specific info */
        FREE_LLD_MEM(hqvdp_global_info.hw);

        /* reset hqvdp_global_info structure */
        hqvdp_global_info.hw = NULL;
        hqvdp_global_info.init_done = false;
        hqvdp_global_info.hqvdp_nb = 0;
        hqvdp_global_info.available_ressources = 0;

        TRCOUT(TRC_ID_HQVDPLITE_LLD, " ");

        return HQVDP_LLD_NO_ERR;
}

