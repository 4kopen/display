/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef HQVDP_SPLIT_ALGO_H_
#define HQVDP_SPLIT_ALGO_H_

#include "hqvdp_lld_platform.h"
#include "hqvdp_split.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/*!
 * /brief Configure parameter of dual HQVDP
 * @param[in] hqvdpFullParam
 * @param[out] hqvdpLeftParam
 * @param[out] hqvdpRightParam
 */
struct error_split_param HQVDPSplitProgrammation(struct cmd_origi_param hqvdpFullParam ,
                             struct cmd_split_param *hqvdpLeftParam,
                             struct cmd_split_param *hqvdpRightParam);

/* C++ support */
#ifdef __cplusplus
}
#endif

#endif /* HQVDP_SPLIT_ALGO_H_ */
