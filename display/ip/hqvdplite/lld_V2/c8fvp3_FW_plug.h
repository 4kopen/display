/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*!
 * \file c8fvp3_FW_plug.h
 *
 */

#ifndef C8FVP3_FW_PLUG_H_
#define C8FVP3_FW_PLUG_H_

#include "hqvdp_lld_api.h"
#include "hqvdp_lld_platform.h"
#include "hqvdp_ucode_256_rd.h"
#include "hqvdp_ucode_256_wr.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif


/*!
 * \brief Do initialization of the ucode of a plug
 *
 * \param[in] base_addr base address of the HQVDP
 * \param[in] plug_offset plug offset
 * \param[in] UcodePlug    microcode
 * \param[in] UcodeSize    microcode size in bytes
 */
void c8fvp3_config_ucodeplug(void *base_addr, uint32_t plug_offset,
                             uint32_t *UcodePlug, uint32_t UcodeSize);


/*!
 * @brief Do initialization of the ucode of read plug
 * see plug documentation for details on parameters
 *
 * @param[in] base_addr base address of hqvdp
 */
void c8fvp3_config_rd_ucodeplug(void *base_addr);
/*!
 * @brief Do initialization of the ucode of write plug
 * see plug documentation for details on parameters
 *
 * @param[in] base_addr base address of hqvdp
 */
void c8fvp3_config_wr_ucodeplug(void *base_addr);
/*!
 * @brief Do initialization of register of read plug
 * see plug documentation for details on parameters
 *
 * @param[in] base_addr base address of hqvdp
 * @param[in] hqvdp_plug_conf register configuration
 */
void c8fvp3_config_rd_regplug(void *base_addr,
                              const struct hqvdp_plug_s *hqvdp_plug_conf);
/*!
 * @brief Do initialization of register of write plug
 * see plug documentation for details on parameters
 *
 * @param[in] base_addr base address of hqvdp
 * @param[in] hqvdp_plug_conf register configuration
 */
void c8fvp3_config_wr_regplug(void *base_addr,
                              const struct hqvdp_plug_s *hqvdp_plug_conf);

/* C++ support */
#ifdef __cplusplus
}
#endif

#endif
