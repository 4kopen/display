/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_VC1RE_H
#define _C8FVP3_HQVDPLITE_API_VC1RE_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : VC1RE
*/


/**
* Register : CTRL_PRV_CSDI
* VC1RE Controls for Previous field to CSDi
*/

#define VC1RE_CTRL_PRV_CSDI_OFFSET                      (c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR + 0x00)
#define VC1RE_CTRL_PRV_CSDI_MASK                        (0x00770003)

/**
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_LUMA_SHIFT     (0x00000000)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_LUMA_WIDTH     (1)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_LUMA_MASK      (0x00000001)

/**
* Bit-field : VC1RE_ENABLE_CHROMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_CHROMA_SHIFT   (0x00000001)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_CHROMA_WIDTH   (1)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_CHROMA_MASK    (0x00000002)

/**
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_LUMA_SHIFT       (0x00000010)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_LUMA_WIDTH       (3)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_LUMA_MASK        (0x00070000)

/**
* Bit-field : VC1RE_COEF_CHROMA
* Coefficient for chroma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_CHROMA_SHIFT     (0x00000014)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_CHROMA_WIDTH     (3)
#define VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_CHROMA_MASK      (0x00700000)

/**
* Register : CTRL_CUR_CSDI
* VC1RE Controls for Current field to CSDi
*/

#define VC1RE_CTRL_CUR_CSDI_OFFSET                      (c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR + 0x04)
#define VC1RE_CTRL_CUR_CSDI_MASK                        (0x00770003)

/**
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_LUMA_SHIFT     (0x00000000)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_LUMA_WIDTH     (1)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_LUMA_MASK      (0x00000001)

/**
* Bit-field : VC1RE_ENABLE_CHROMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_CHROMA_SHIFT   (0x00000001)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_CHROMA_WIDTH   (1)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_CHROMA_MASK    (0x00000002)

/**
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_LUMA_SHIFT       (0x00000010)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_LUMA_WIDTH       (3)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_LUMA_MASK        (0x00070000)

/**
* Bit-field : VC1RE_COEF_CHROMA
* Coefficient for chroma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_CHROMA_SHIFT     (0x00000014)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_CHROMA_WIDTH     (3)
#define VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_CHROMA_MASK      (0x00700000)

/**
* Register : CTRL_NXT_CSDI
* VC1RE Controls for Next field to CSDi
*/

#define VC1RE_CTRL_NXT_CSDI_OFFSET                      (c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR + 0x08)
#define VC1RE_CTRL_NXT_CSDI_MASK                        (0x00770003)

/**
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_LUMA_SHIFT     (0x00000000)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_LUMA_WIDTH     (1)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_LUMA_MASK      (0x00000001)

/**
* Bit-field : VC1RE_ENABLE_CHROMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_CHROMA_SHIFT   (0x00000001)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_CHROMA_WIDTH   (1)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_CHROMA_MASK    (0x00000002)

/**
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_LUMA_SHIFT       (0x00000010)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_LUMA_WIDTH       (3)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_LUMA_MASK        (0x00070000)

/**
* Bit-field : VC1RE_COEF_CHROMA
* Coefficient for chroma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_CHROMA_SHIFT     (0x00000014)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_CHROMA_WIDTH     (3)
#define VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_CHROMA_MASK      (0x00700000)

/**
* Register : CTRL_CUR_FMD
* VC1RE Controls for Current field to FMD
*/

#define VC1RE_CTRL_CUR_FMD_OFFSET                       (c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR + 0x0C)
#define VC1RE_CTRL_CUR_FMD_MASK                         (0x00070001)

/**
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_CUR_FMD_VC1RE_ENABLE_LUMA_SHIFT      (0x00000000)
#define VC1RE_CTRL_CUR_FMD_VC1RE_ENABLE_LUMA_WIDTH      (1)
#define VC1RE_CTRL_CUR_FMD_VC1RE_ENABLE_LUMA_MASK       (0x00000001)

/**
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_CUR_FMD_VC1RE_COEF_LUMA_SHIFT        (0x00000010)
#define VC1RE_CTRL_CUR_FMD_VC1RE_COEF_LUMA_WIDTH        (3)
#define VC1RE_CTRL_CUR_FMD_VC1RE_COEF_LUMA_MASK         (0x00070000)

/**
* Register : CTRL_NXT_FMD
* VC1RE Controls for Next field to FMD
*/

#define VC1RE_CTRL_NXT_FMD_OFFSET                       (c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR + 0x10)
#define VC1RE_CTRL_NXT_FMD_MASK                         (0x00070001)

/**
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of VC1 Range Engine:
*/

#define VC1RE_CTRL_NXT_FMD_VC1RE_ENABLE_LUMA_SHIFT      (0x00000000)
#define VC1RE_CTRL_NXT_FMD_VC1RE_ENABLE_LUMA_WIDTH      (1)
#define VC1RE_CTRL_NXT_FMD_VC1RE_ENABLE_LUMA_MASK       (0x00000001)

/**
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define VC1RE_CTRL_NXT_FMD_VC1RE_COEF_LUMA_SHIFT        (0x00000010)
#define VC1RE_CTRL_NXT_FMD_VC1RE_COEF_LUMA_WIDTH        (3)
#define VC1RE_CTRL_NXT_FMD_VC1RE_COEF_LUMA_MASK         (0x00070000)


#ifndef HQVDPLITE_API_FOR_STAPI
typedef struct {
    gvh_u32_t CTRL_PRV_CSDI;  /* at 0 */
    gvh_u32_t CTRL_CUR_CSDI;  /* at 4 */
    gvh_u32_t CTRL_NXT_CSDI;  /* at 8 */
    gvh_u32_t CTRL_CUR_FMD;  /* at 12 */
    gvh_u32_t CTRL_NXT_FMD;  /* at 16 */
} s_VC1RE;
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t CtrlPrvCsdi;  /* at 0 */
uint32_t CtrlCurCsdi;  /* at 4 */
uint32_t CtrlNxtCsdi;  /* at 8 */
uint32_t CtrlCurFmd;  /* at 12 */
uint32_t CtrlNxtFmd;  /* at 16 */
} HQVDPLITE_VC1RE_Params_t;
#endif /* FW_STXP70 */
#endif
