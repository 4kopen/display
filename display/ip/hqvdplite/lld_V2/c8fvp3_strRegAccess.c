/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "strRegDefinition.h"
#include "c8fvp3_strRegAccess.h"
#include "c8fvp3_stddefs.h"
#ifndef FW_STXP70
#include "hqvdp_lld_platform.h"
#endif /* not FW_STXP70 */

#ifdef FW_STXP70

void STR_configure_IT(gvh_u32_t  IT_number, gvh_u32_t  RdChMask, gvh_u32_t  WrChMask,
		      gvh_u32_t  RdQMask, gvh_u32_t  WrQmask, unsigned int base_address)
{
   gvh_u32_t  data = 0;
   gvh_u32_t  addr = 0;

   if (IT_number <= 15) {
     /* STBUS interrupt mask Register of interrupt IT_number */
     addr = STR_ABS_BIM(base_address,IT_number);
     data = (RdChMask & STR_ALL_RIE_MASK) + ((WrChMask & STR_ALL_WIE_MASK) << STR_WIE_BIT(0));
     WriteRegister(addr, data);

     /* RD/WR Queue interrupt mask Register of interrupt IT_number */
     addr = STR_ABS_QIM(base_address,IT_number);
     data = (RdQMask & STR_ALL_RIE_MASK) + ((WrQmask & STR_ALL_WIE_MASK) << STR_WIE_BIT(0));
     WriteRegister(addr, data);
   }
}


void STR_enable_IT(gvh_u32_t IT_en_mask, unsigned int base_address)
{
     gvh_u32_t  data = 0;
     gvh_u32_t  addr = 0;

     data = IT_en_mask & STR_ALL_ISEB_MASK;
     addr = STR_ABS_ISE(base_address);
     WriteRegister(addr,data);
}
#endif /* FW_STXP70 */

#ifndef FW_STXP70
/*******************************************************************************
 * Name        : ???
 * Description : ???
 * Parameters  : ???
 * Assumptions : ???
 * Limitations : ???
 * Returns     : ???
 * *******************************************************************************/
void WriteBackSTBCh(void *base_addr, uint32_t offset, uint32_t chNb,
                    uint32_t rdNWr, uint32_t extsa, uint32_t plugam,
                    uint32_t attr, uint32_t extam, uint32_t ext2d,
                    uint32_t extsz, uint32_t ec, uint32_t df,
                    uint32_t intsa, uint32_t intra, uint32_t intai,
                    uint32_t intdc, uint32_t intrc, uint32_t into)
{
    uint32_t baseReg;
    uint32_t data;

    /*************************************************************************
    * 31  .. 0 |
    * ----------------------------------------------------
    *   extsa  |
    *************************************************************************/
    data = extsa;
    baseReg = (rdNWr == 0) ?
                    STR_ABS_BWEA(offset, chNb) : STR_ABS_BREA(offset, chNb);
    REG32_WRITE(base_addr, baseReg, data);

    /*************************************************************************
    * | 30 .. 28 | 27 .. 14 | 13 .. 0 |
    * ----------------------------------
    * |  attr   |   ext2d  |  extsz  |
    *************************************************************************/

    data =  ((attr << STR_ATTR_BIT) & STR_ATTR_MASK)
          | ((ext2d << STR_EXT2D_BIT) & STR_EXT2D_MASK)
	  | ((extsz << STR_EXTSZ_BIT) & STR_EXTSZ_MASK);
    baseReg = (rdNWr == 0) ?
                    STR_ABS_BWPM(offset, chNb): STR_ABS_BRPM(offset, chNb);
    REG32_WRITE(base_addr, baseReg, data);

    /*************************************************************************
    * 31 .. 28 | 27 .. 24 | 22 ..0 |
    * ---------------------------------
    *  INTRC   |  INTDC   | INTSA  |
    *************************************************************************/
    data =  ((intrc << STR_BINTRDC_BIT) & STR_BINTRDC_MASK)
          | ((intdc << STR_BINTDC_BIT) & STR_BINTDC_MASK)
          | ((intsa << STR_BINTSA_BIT) & STR_BINTSA_MASK);

    baseReg = (rdNWr == 0) ?
                    STR_ABS_BWIA(offset, chNb) : STR_ABS_BRIA(offset, chNb);
    REG32_WRITE(base_addr, baseReg, data);

    /*************************************************************************
    * 31 .. 23 | 22 .. 0 |
    * ------------------------------------------
    *  -      |  INTRA  |
    *************************************************************************/
    data = (intra << STR_BINTRA_BIT) & STR_BINTRA_MASK;
    baseReg = (rdNWr == 0) ?
                    STR_ABS_BWRA(offset, chNb) : STR_ABS_BRRA(offset, chNb);
    REG32_WRITE(base_addr, baseReg, data);

    /*************************************************************************
    * 31 .. 14 | 13 .. 0 |
    * ------------------------------------------
    *  -      |  INTAI  |
    *************************************************************************/
    data = (intai << STR_BINTAI_BIT) & STR_BINTAI_MASK;
    baseReg = (rdNWr == 0) ?
                    STR_ABS_BWII(offset, chNb) : STR_ABS_BRII(offset, chNb);
    REG32_WRITE(base_addr, baseReg, data);

    /*************************************************************************
    * 25  23 | 21 19 | 18 | 17 .. 14 | 13 .. 0 |
    * -----------------------------------
    *  EXTAM | DF    | EC |  PLUGAM  |   INTO  |
    *************************************************************************/
    data =  ((extam << STR_EXTAM_BIT) & STR_EXTAM_MASK)
          | ((df << STR_DF_BIT) & STR_DF_MASK)
          | ((ec << STR_EC_BIT) & STR_EC_MASK)
          | ((plugam << STR_PLUGAM_BIT) & STR_PLUGAM_MASK)
          | ((into << STR_INTO_BIT) & STR_INTO_MASK);
    baseReg = (rdNWr == 0) ?
                    STR_ABS_BWVP(offset, chNb) : STR_ABS_BRVP(offset, chNb);
    REG32_WRITE(base_addr, baseReg, data);
}
#endif

#ifndef FW_STXP70
/*******************************************************************************
 * Name        : ???
 * Description : ???
 * Parameters  : ???
 * Assumptions : ???
 * Limitations : ???
 * Returns     : ???
 * *******************************************************************************/
void WriteBackQCh(void *base_addr, uint32_t offset, uint32_t intsa,
                  uint32_t intra, uint32_t intai, uint32_t intdc,
                  uint32_t intrc, uint32_t intim, uint32_t qtw,
                  uint32_t dt, uint32_t rdNwr, uint32_t chNb, uint32_t phyQ,
                  uint32_t qsz, uint32_t fp, uint32_t lp, uint32_t puid)
{
    uint32_t baseReg;
    uint32_t data;
    /*************************************************************************
    * 31 .. 28 | 27 .. 24  |  23   | 22 ..0 |
    * --------------------------
    *  INTRC   |  INTDC   | INTIM | INTSA  |
    *************************************************************************/
    data =  ((intrc << STR_QINTRDC_BIT) & STR_QINTRDC_MASK)
          | ((intdc << STR_QINTDC_BIT) & STR_QINTDC_MASK)
          | ((intim << STR_QINTIM_BIT) & STR_QINTIM_MASK)
          | ((intsa << STR_QINTSA_BIT) & STR_QINTSA_MASK);
    baseReg = (rdNwr == 0) ? STR_ABS_QWIA(offset, phyQ, chNb): STR_ABS_QRIA(offset, phyQ, chNb);
    REG32_WRITE(base_addr, baseReg, data);

    /*************************************************************************
    *  31 | 30 .. 23 | 22 .. 0 |
    * ------------------------------------------
    *  DT |  FLAGS   |  INTRA  |
    *************************************************************************/
    data =  ((dt?1:0 /*& (STR_Q_DT_MASK)*/) << (STR_Q_DT_BIT))
      | ((puid << STR_QFLAG_PUID_BIT) & STR_QFLAG_PUID_MASK)
      | ((lp << STR_QFLAG_LP_BIT) & STR_QFLAG_LP_MASK)
      | ((fp << STR_QFLAG_FP_BIT) & STR_QFLAG_FP_MASK)
      | ((intra << STR_QINTRA_BIT) & STR_QINTRA_MASK);
    baseReg = (rdNwr == 0) ? STR_ABS_QWRA(offset, phyQ, chNb): STR_ABS_QRRA(offset, phyQ, chNb);
    REG32_WRITE(base_addr, baseReg, data);

    /*************************************************************************
    * 29. 28  |27 .. 14 | 13 ..0 |
    * ----------------------------
    *  QTW  |  INTAI  | QSZ    |
    ************************************************************************/
    data =  ((qtw << STR_QTW_BIT) & STR_QTW_MASK)
          | ((intai << STR_QINTAI_BIT) & STR_QINTAI_MASK)
          | ((qsz << STR_QSZ_BIT) & STR_QSZ_MASK);
    baseReg = (rdNwr == 0) ? STR_ABS_QWPM(offset, phyQ, chNb): STR_ABS_QRPM(offset, phyQ, chNb);
    REG32_WRITE(base_addr, baseReg, data);
}
#endif
