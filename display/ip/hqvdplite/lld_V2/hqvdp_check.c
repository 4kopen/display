/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "hqvdp_check.h"
#include "hqvdp_split.h"
#include "hqvdp_lld_platform.h"

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void hqvdp_check_split_result(struct cmd_split_param *param)
{
        /* output_width has to be even because of IQI HW constraints */
        if ((param->output_width % 2) != 0 ) {
                TRC(TRC_ID_HQVDPLITE_LLD,
                    "[0;47;35m [ERROR] Output %d is not even, Detected by function %s [0m"
                        , param->output_width, __FUNCTION__);
        }
}

