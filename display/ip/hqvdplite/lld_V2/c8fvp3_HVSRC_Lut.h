/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*!
 * \file c8fvp3_HVSRC_Lut.h
 * \brief Specifics functions that initialize the Lut of HVSRC
 *
 * These functions initialize the values of Luts for the HVSRC filters
 */

#ifndef _C8FVP3_HVSRC_LUT_H_
#define _C8FVP3_HVSRC_LUT_H_

#include "hqvdp_lld_platform.h"
#include "c8fvp3_aligned_viewport.h"

/* Luma strength */
#define HVSRC_LUT_Y_LEGACY (0)
#define HVSRC_LUT_Y_SMOOTH (1)
#define HVSRC_LUT_Y_MEDIUM (2)
#define HVSRC_LUT_Y_SHARP  (3)

/* Chroma strength */
#define HVSRC_LUT_C_LEGACY (0)
#define HVSRC_LUT_C_OPT    (1)

#define HORI_SHIFT_Y_SHIFT                        (0x00000000)
#define HORI_SHIFT_C_SHIFT                        (0x00000010)
#define VERT_SHIFT_Y_SHIFT                        (0x00000000)
#define VERT_SHIFT_C_SHIFT                        (0x00000010)

/*!
 * \brief Do initialization of the HVSRC LUT
 *
 * \param[in] ApiViewportX  Viewport X origin
 * \param[in] ApiWidthIn    Input viewport width
 * \param[in] ApiWidthOut   Output viewport width
 * \param[in] ApiHeightIn   Input viewport height
 * \param[in] ApiHeightOut  Output viewport height
 * \param[in] StrengthYV    luma strength of the vertical LUT legacy/smooth/medium/sharp
 * \param[in] StrengthCV    chroma strength of the vertical LUT legacy/smooth/medium/sharp
 * \param[in] StrengthYH    luma strength of the horizontal LUT legacy/smooth/medium/sharp
 * \param[in] StrengthCH    chroma strength of the horizontal LUT legacy/smooth/medium/sharp
 * \param[in] InputIs444    Input chroma sampling
 * \param[in] OutputIs444   Outputchroma sampling
 * \param[in] OuputIsSbs    Output 3D format is SbS
 * \param[in] OuputIsTab    Output 3D format is TaB
 * \param[out] LutYH        pointer to luma horizontal lut
 * \param[out] LutCH        pointer to chroma horizontal lut
 * \param[out] LutYV        pointer to luma vertical lut
 * \param[out] LutCV        pointer to chroma vertical lut
 * \param[out] shiftYH      sum of horizontal coef luma
 * \param[out] shiftCH      sum of horizontal coef chroma
 * \param[out] shiftYV      sum of vertical coef luma
 * \param[out] shiftCV      sum of vertical coef chroma
 */
void HVSRC_getFilterCoefHV(uint16_t ApiViewportX,
                           uint16_t ApiWidthIn,
                           uint16_t ApiWidthOut,
                           uint16_t ApiHeightIn,
                           uint16_t ApiHeightOut,
                           uint8_t StrengthYV,
                           uint8_t StrengthCV,
                           uint8_t StrengthYH,
                           uint8_t StrengthCH,
                           bool InputIs444,
                           bool OutputIs444,
                           bool OuputIsSbs,
                           bool OuputIsTab,
                           uint32_t *LutYH,
                           uint32_t *LutCH,
                           uint32_t *LutYV,
                           uint32_t *LutCV,
                           uint8_t *shiftYH,
                           uint8_t *shiftCH,
                           uint8_t *shiftYV,
                           uint8_t *shiftCV);

/*!
 * \brief Do initialization of the HVSRC LUT
 *
 * \param[in] StrengthY      luma strength of the LUT legacy/smooth/medium/sharp
 * \param[in] SizeInY        luma width or height of the input
 * \param[in] SizeOutY       luma width or height of the output
 * \param[in] StrengthC      chroma strength of the LUT legacy/smooth/medium/sharp
 * \param[in] SizeInC        chroma width or height of the input
 * \param[in] SizeOutC       chroma width or height of the output
 * \param[out] LutY          pointer to luma   lut
 * \param[out] LutC          pointer to chroma lut
 * \param[out] shiftY        sum of luma coef
 * \param[out] shiftC        sum of chroma coef
 */
void HVSRC_get_lut(uint8_t StrengthY,
                   uint16_t SizeInY,
                   uint16_t SizeOutY,
                   uint8_t StrengthC,
                   uint16_t SizeInC,
                   uint16_t SizeOutC,
                   uint32_t *LutY,
                   uint32_t *LutC,
                   uint8_t *shiftY,
                   uint8_t *shiftC);


#endif /* _C8FVP3_HVSRC_LUT_H_ */
