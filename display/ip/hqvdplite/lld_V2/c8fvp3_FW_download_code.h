/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef C8FVP3_DOWNLOAD_CODE_H_
#define C8FVP3_DOWNLOAD_CODE_H_
#include "c8fvp3_FW_lld_global.h"
#include "hqvdp_lld_api.h"
#include "hqvdp_lld_platform.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/*!
 * @brief Download code into the PMEM or DMEM by using classic writeRegister or using the streamer way.
 * @ingroup lld_local_functions
 *
 * @param[in] base_addr      Base address of the IP
 * @param[in] load_mode      Way to load code T1 or Streamer
 * @param[in] src_info       Information on code to load (address, size)
 * @param[in] dest_addr      Internal address where the code has to be stored.
 */
void c8fvp3_download_code(void *base_addr,
                          enum hqvdp_lld_fw_load_e load_mode,
                          const struct hqvdp_lld_mem_desc_s *src_info,
                          uint32_t dest_addr);

/* C++ support */
#ifdef __cplusplus
}
#endif

#endif
