/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _HQRCSDIREGBANK_H_
#define _HQRCSDIREGBANK_H_

/*!
* \brief
* Component section
* Purpose : defines macros for component
*           specific informations
*/

#define HQRCSDIRegBank_VENDOR                                     ("st.com")
#define HQRCSDIRegBank_LIBRARY                                    ("hqvdplite")
#define HQRCSDIRegBank_NAME                                       ("HQRCSDIRegBank")
#define HQRCSDIRegBank_VERSION                                    ("1.0")

/*!
* \brief
* Address Block : com_pu_regs
* communication cluster registers (IT combiner, VC1, CUP, CRC, etc.)
*/

#define HQRCSDIRegBank_com_pu_regs_BASE_ADDR                      (0x0)

/*!
* \brief
* Register : HQRIE_IT_CONFIG
* IT combiner configuration
*/

#define HQRCSDIRegBank_HQRIE_IT_CONFIG_SIZE                       (32)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_OFFSET                     (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x00)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_RESET_VALUE                (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_BITFIELD_MASK              (0x00000003)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_RWMASK                     (0x00000003)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_ROMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_WOMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_UNUSED_MASK                (0xFFFFFFFC)

/*!
* \brief
* Bit-field : OR_NOT_AND_COMB
* combinational scheme
* B_0x0: AND interrupt combination (default)
* B_0x1: OR interrupt combination
*/

#define HQRCSDIRegBank_HQRIE_IT_CONFIG_OR_NOT_AND_COMB_OFFSET     (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_OR_NOT_AND_COMB_WIDTH      (1)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_OR_NOT_AND_COMB_MASK       (0x00000001)

/*!
* \brief
* Bit-field : GLOB_NOT_LOC_RELAUNCH
* relaunch mode
* B_0x0: local relaunch (default)
* B_0x1: global relaunch
*/

#define HQRCSDIRegBank_HQRIE_IT_CONFIG_GLOB_NOT_LOC_RELAUNCH_OFFSET (0x00000001)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_GLOB_NOT_LOC_RELAUNCH_WIDTH (1)
#define HQRCSDIRegBank_HQRIE_IT_CONFIG_GLOB_NOT_LOC_RELAUNCH_MASK (0x00000002)

/*!
* \brief
* Register : HQRIE_IT_ENABLE
* interruption lines enable
*/

#define HQRCSDIRegBank_HQRIE_IT_ENABLE_SIZE                       (32)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_OFFSET                     (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x04)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_RESET_VALUE                (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_BITFIELD_MASK              (0x8000007F)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_RWMASK                     (0x8000007F)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_ROMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_WOMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_UNUSED_MASK                (0x7FFFFF80)

/*!
* \brief
* Bit-field : STR_IT_EN
* interruption line enable (one command per interruption line, maximum is 31 lines)
* B_0x0: disable (default). The IT line is stuck at its inactive state (depends on OR_NOT_AND_COMB bit)
* B_0x1: enable. The IT line can be activated
* [0] streamer read str_glb_irq
* [1] streamer write str_glb_irq
* [2] streamer inter str_glb_irq
* [3] data-path PU CSDI done irq
* [4] data-path PU HVSRC luma done irq
* [5] data-path PU HVSRC chroma done irq
* [6] data-path PU 444 done irq
*/

#define HQRCSDIRegBank_HQRIE_IT_ENABLE_STR_IT_EN_OFFSET           (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_STR_IT_EN_WIDTH            (7)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_STR_IT_EN_MASK             (0x0000007F)

/*!
* \brief
* Bit-field : FW_DONE_IT_EN
* firmware done interruption line enable
* B_0x0: disable (default). The firmware cannot activate the IT line through FW_DONE
* B_0x1: enable. The firmware can activate the IT line with FW_DONE
*/

#define HQRCSDIRegBank_HQRIE_IT_ENABLE_FW_DONE_IT_EN_OFFSET       (0x0000001f)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_FW_DONE_IT_EN_WIDTH        (1)
#define HQRCSDIRegBank_HQRIE_IT_ENABLE_FW_DONE_IT_EN_MASK         (0x80000000)

/*!
* \brief
* Register : HQRIE_IT_CTRL
* IT combiner control
*/

#define HQRCSDIRegBank_HQRIE_IT_CTRL_SIZE                         (32)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_OFFSET                       (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x08)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_RESET_VALUE                  (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_BITFIELD_MASK                (0x00000003)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_RWMASK                       (0x00000003)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_ROMASK                       (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_WOMASK                       (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_UNUSED_MASK                  (0xFFFFFFFC)

/*!
* \brief
* Bit-field : FW_DONE
* internal software interruption line accessible by firmware
* B_0x0: no interruption
* B_0x1: firmware activation of the internal interruption line
*/

#define HQRCSDIRegBank_HQRIE_IT_CTRL_FW_DONE_OFFSET               (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_FW_DONE_WIDTH                (1)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_FW_DONE_MASK                 (0x00000001)

/*!
* \brief
* Bit-field : FORCE_IT
* force the IT generation, whatever the input line states, STR_IT_EN and OR_NOT_AND_COMB values
* B_0x0: interruption not forced
* B_0x1: interruption forced
*/

#define HQRCSDIRegBank_HQRIE_IT_CTRL_FORCE_IT_OFFSET              (0x00000001)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_FORCE_IT_WIDTH               (1)
#define HQRCSDIRegBank_HQRIE_IT_CTRL_FORCE_IT_MASK                (0x00000002)

/*!
* \brief
* Register : HQRIE_IT_RELAUNCH_DECOUNT
* relaunch counter
*/

#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_SIZE             (32)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_OFFSET           (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x0C)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_RESET_VALUE      (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_BITFIELD_MASK    (0x00000FFF)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_RWMASK           (0x00000FFF)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_ROMASK           (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_WOMASK           (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_UNUSED_MASK      (0xFFFFF000)

/*!
* \brief
* Bit-field : IT_DEC_COUNT
* interruption counter start value (number of relaunch to perform before sending an interruption)
* unsigned value ranging from 0 to 4095 (default is 0)
*/

#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_IT_DEC_COUNT_OFFSET (0x00000000)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_IT_DEC_COUNT_WIDTH (12)
#define HQRCSDIRegBank_HQRIE_IT_RELAUNCH_DECOUNT_IT_DEC_COUNT_MASK (0x00000FFF)

/*!
* \brief
* Register : HQRIE_COM_CFG
* Communication cluster configuration
*/

#define HQRCSDIRegBank_HQRIE_COM_CFG_SIZE                         (32)
#define HQRCSDIRegBank_HQRIE_COM_CFG_OFFSET                       (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x10)
#define HQRCSDIRegBank_HQRIE_COM_CFG_RESET_VALUE                  (0x02008000)
#define HQRCSDIRegBank_HQRIE_COM_CFG_BITFIELD_MASK                (0x4F800003)
#define HQRCSDIRegBank_HQRIE_COM_CFG_RWMASK                       (0x4F800003)
#define HQRCSDIRegBank_HQRIE_COM_CFG_ROMASK                       (0x00000000)
#define HQRCSDIRegBank_HQRIE_COM_CFG_WOMASK                       (0x00000000)
#define HQRCSDIRegBank_HQRIE_COM_CFG_UNUSED_MASK                  (0xB07FFFFC)

/*!
* \brief
* Bit-field : NO_ALPHA
* When set no Alpha on 444 input format, used in demux444
*/

#define HQRCSDIRegBank_HQRIE_COM_CFG_NO_ALPHA_OFFSET              (0x00000000)
#define HQRCSDIRegBank_HQRIE_COM_CFG_NO_ALPHA_WIDTH               (1)
#define HQRCSDIRegBank_HQRIE_COM_CFG_NO_ALPHA_MASK                (0x00000001)

/*!
* \brief
* Bit-field : INPUT_IS_444
* input video format, used in mux2to1
* B_0x0: 4:2:2 or 4:2:0 (default)
* B_0x1: 4:4:4
*/

#define HQRCSDIRegBank_HQRIE_COM_CFG_INPUT_IS_444_OFFSET          (0x00000001)
#define HQRCSDIRegBank_HQRIE_COM_CFG_INPUT_IS_444_WIDTH           (1)
#define HQRCSDIRegBank_HQRIE_COM_CFG_INPUT_IS_444_MASK            (0x00000002)

/*!
* \brief
* Bit-field : INPUT10BITNOT8BIT
* pixels are 10 bits when set, used in every 16to10 and demux444
*/

#define HQRCSDIRegBank_HQRIE_COM_CFG_INPUT10BITNOT8BIT_OFFSET     (0x00000017)
#define HQRCSDIRegBank_HQRIE_COM_CFG_INPUT10BITNOT8BIT_WIDTH      (1)
#define HQRCSDIRegBank_HQRIE_COM_CFG_INPUT10BITNOT8BIT_MASK       (0x00800000)

/*!
* \brief
* Bit-field : OFFSET_PIX8TO10_Y
* conversion 8-bit to 10-bit offset for luma component, used in every 16to10
* unsigned value of the 2-LSB to be added, default is 2
*/

#define HQRCSDIRegBank_HQRIE_COM_CFG_OFFSET_PIX8TO10_Y_OFFSET     (0x00000018)
#define HQRCSDIRegBank_HQRIE_COM_CFG_OFFSET_PIX8TO10_Y_WIDTH      (2)
#define HQRCSDIRegBank_HQRIE_COM_CFG_OFFSET_PIX8TO10_Y_MASK       (0x03000000)

/*!
* \brief
* Bit-field : OFFSET_PIX8TO10_UV
* conversion 8-bit to 10-bit offset for chroma component, used in every 16to10.
* unsigned value of the 2-LSB to be added, default is 0
*/

#define HQRCSDIRegBank_HQRIE_COM_CFG_OFFSET_PIX8TO10_UV_OFFSET    (0x0000001a)
#define HQRCSDIRegBank_HQRIE_COM_CFG_OFFSET_PIX8TO10_UV_WIDTH     (2)
#define HQRCSDIRegBank_HQRIE_COM_CFG_OFFSET_PIX8TO10_UV_MASK      (0x0C000000)

/*!
* \brief
* Bit-field : REMUX_MODE
* when set, all the remux PU are activated, else bypassed
*/

#define HQRCSDIRegBank_HQRIE_COM_CFG_REMUX_MODE_OFFSET            (0x0000001e)
#define HQRCSDIRegBank_HQRIE_COM_CFG_REMUX_MODE_WIDTH             (1)
#define HQRCSDIRegBank_HQRIE_COM_CFG_REMUX_MODE_MASK              (0x40000000)

/*!
* \brief
* Register : HQRIE_REMUX_CTRL
* Remux Control
*/

#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_SIZE                      (32)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_OFFSET                    (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x14)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_RESET_VALUE               (0x00000000)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_BITFIELD_MASK             (0x0000003F)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_RWMASK                    (0x0000003F)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_ROMASK                    (0x00000000)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_WOMASK                    (0x00000000)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_UNUSED_MASK               (0xFFFFFFC0)

/*!
* \brief
* Bit-field : REMUX_Y_NXT_ENB_FIRST
*/

#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_NXT_ENB_FIRST_OFFSET (0x00000000)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_NXT_ENB_FIRST_WIDTH (1)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_NXT_ENB_FIRST_MASK (0x00000001)

/*!
* \brief
* Bit-field : REMUX_Y_CUR_ENB_FIRST
*/

#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_CUR_ENB_FIRST_OFFSET (0x00000001)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_CUR_ENB_FIRST_WIDTH (1)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_CUR_ENB_FIRST_MASK (0x00000002)

/*!
* \brief
* Bit-field : REMUX_Y_PRV_ENB_FIRST
*/

#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_PRV_ENB_FIRST_OFFSET (0x00000002)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_PRV_ENB_FIRST_WIDTH (1)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_Y_PRV_ENB_FIRST_MASK (0x00000004)

/*!
* \brief
* Bit-field : REMUX_UV_NXT_ENB_FIRST
*/

#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_NXT_ENB_FIRST_OFFSET (0x00000003)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_NXT_ENB_FIRST_WIDTH (1)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_NXT_ENB_FIRST_MASK (0x00000008)

/*!
* \brief
* Bit-field : REMUX_UV_CUR_ENB_FIRST
*/

#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_CUR_ENB_FIRST_OFFSET (0x00000004)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_CUR_ENB_FIRST_WIDTH (1)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_CUR_ENB_FIRST_MASK (0x00000010)

/*!
* \brief
* Bit-field : REMUX_UV_PRV_ENB_FIRST
*/

#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_PRV_ENB_FIRST_OFFSET (0x00000005)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_PRV_ENB_FIRST_WIDTH (1)
#define HQRCSDIRegBank_HQRIE_REMUX_CTRL_REMUX_UV_PRV_ENB_FIRST_MASK (0x00000020)

/*!
* \brief
* Register : FMD_PIX16TO10_WIDTH
* FMD Pix16to10 Line Widths
*/

#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_SIZE                   (32)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x18)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_RESET_VALUE            (0x03C003C0)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_BITFIELD_MASK          (0x0FFF0FFF)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_RWMASK                 (0x0FFF0FFF)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_UNUSED_MASK            (0xF000F000)

/*!
* \brief
* Bit-field : RIGHT
* number of pixels (10bpp resolution) required for the right eye at the output of the Pix16to10 PU.
* Unsigned value from 24 to 2048 (default is 960). Should be WIDTH_FMD/2.
* Used in every 16to10 related to FMD.
*/

#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_RIGHT_OFFSET           (0x00000000)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_RIGHT_WIDTH            (12)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_RIGHT_MASK             (0x00000FFF)

/*!
* \brief
* Bit-field : LEFT
* number of pixels (10bpp resolution) required for the left eye at the output of the Pix16to10 PU.
* Unsigned value from 24 to 2048 (default is 960). Should be WIDTH_FMD/2.
* Used in every 16to10 related to FMD.
*/

#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_LEFT_OFFSET            (0x00000010)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_LEFT_WIDTH             (12)
#define HQRCSDIRegBank_FMD_PIX16TO10_WIDTH_LEFT_MASK              (0x0FFF0000)

/*!
* \brief
* Register : FMD_PIX16TO10_CROP
* FMD Pix16to10 Line Crops
*/

#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_SIZE                    (32)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x1C)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_RESET_VALUE             (0x00000000)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_BITFIELD_MASK           (0x00000303)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_RWMASK                  (0x00000303)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_UNUSED_MASK             (0xFFFFFCFC)

/*!
* \brief
* Bit-field : RIGHT
* number of pixels (10bpp resolution) to be removed at the end of the transfer at the output of the Pix16to10 PU.
* Unsigned value from 0 to 3 (default is 0).
* Used in every 16to10 related to FMD.
*/

#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_RIGHT_OFFSET            (0x00000000)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_RIGHT_WIDTH             (2)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_RIGHT_MASK              (0x00000003)

/*!
* \brief
* Bit-field : LEFT
* number of pixels (10bpp resolution) to be removed at the beginning of the transfer at the output of the Pix16to10 PU.
* Unsigned value from 0 to 3 (default is 0).
* Used in every 16to10 related to FMD.
*/

#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_LEFT_OFFSET             (0x00000008)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_LEFT_WIDTH              (2)
#define HQRCSDIRegBank_FMD_PIX16TO10_CROP_LEFT_MASK               (0x00000300)

/*!
* \brief
* Register : CSDI_PIX16TO10_LUMA_WIDTH
* CSDI Pix16to10 Luma Widths
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_SIZE             (32)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_OFFSET           (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x20)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_RESET_VALUE      (0x03C003C0)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_BITFIELD_MASK    (0x0FFF0FFF)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_RWMASK           (0x0FFF0FFF)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_ROMASK           (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_WOMASK           (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_UNUSED_MASK      (0xF000F000)

/*!
* \brief
* Bit-field : RIGHT
* number of pixels (10bpp resolution) required for the right eye at the output of the Pix16to10 PU.
* Unsigned value from 24 to 2048 (default is 960). Should be WIDTH_VIDEO/2.
* Used in every 16to10 related to CSDI luma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_RIGHT_OFFSET     (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_RIGHT_WIDTH      (12)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_RIGHT_MASK       (0x00000FFF)

/*!
* \brief
* Bit-field : LEFT
* number of pixels (10bpp resolution) required for the left eye at the output of the Pix16to10 PU.
* Unsigned value from 24 to 2048 (default is 960). Should be WIDTH_VIDEO/2.
* Used in every 16to10 related to CSDI luma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_LEFT_OFFSET      (0x00000010)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_LEFT_WIDTH       (12)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_WIDTH_LEFT_MASK        (0x0FFF0000)

/*!
* \brief
* Register : CSDI_PIX16TO10_LUMA_CROP
* CSDI Pix16to10 Luma Crops
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_SIZE              (32)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_OFFSET            (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x24)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_RESET_VALUE       (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_BITFIELD_MASK     (0x00000373)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_RWMASK            (0x00000373)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_ROMASK            (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_WOMASK            (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_UNUSED_MASK       (0xFFFFFC8C)

/*!
* \brief
* Bit-field : RIGHT
* number of pixels (10bpp resolution) to be removed at the end of the transfer at the output of the Pix16to10 PU.
* Unsigned value from 0 to 3 (default is 0).
* Used in every 16to10 related to CSDI luma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_RIGHT_OFFSET      (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_RIGHT_WIDTH       (2)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_RIGHT_MASK        (0x00000003)

/*!
* \brief
* Bit-field : CENTRE
* number of pixels (10bpp resolution) to be removed at the middle of the transfer at the output of the Pix16to10 PU. Only used for 3D-TV side by side processing.
* Unsigned value from 0 to 6 (default is 0).
* Used in every 16to10 related to CSDI luma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_CENTRE_OFFSET     (0x00000004)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_CENTRE_WIDTH      (3)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_CENTRE_MASK       (0x00000070)

/*!
* \brief
* Bit-field : LEFT
* number of pixels (10bpp resolution) to be removed at the beginning of the transfer at the output of the Pix16to10 PU.
* Unsigned value from 0 to 3 (default is 0).
* Used in every 16to10 related to CSDI luma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_LEFT_OFFSET       (0x00000008)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_LEFT_WIDTH        (2)
#define HQRCSDIRegBank_CSDI_PIX16TO10_LUMA_CROP_LEFT_MASK         (0x00000300)

/*!
* \brief
* Register : CSDI_PIX16TO10_CHROMA_WIDTH
* CSDI Pix16to10 Chroma Widths
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_SIZE           (32)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_OFFSET         (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x28)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_RESET_VALUE    (0x03C003C0)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_BITFIELD_MASK  (0x0FFF0FFF)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_RWMASK         (0x0FFF0FFF)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_ROMASK         (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_WOMASK         (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_UNUSED_MASK    (0xF000F000)

/*!
* \brief
* Bit-field : RIGHT
* number of pixels (10bpp resolution) required for the right eye at the output of the Pix16to10 PU.
* Unsigned value from 24 to 2048 (default is 960).
* Used in every 16to10 related to CSDI chroma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_RIGHT_OFFSET   (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_RIGHT_WIDTH    (12)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_RIGHT_MASK     (0x00000FFF)

/*!
* \brief
* Bit-field : LEFT
* number of pixels (10bpp resolution) required for the left eye at the output of the Pix16to10 PU.
* Unsigned value from 24 to 2048 (default is 960).
* Used in every 16to10 related to CSDI chroma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_LEFT_OFFSET    (0x00000010)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_LEFT_WIDTH     (12)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_WIDTH_LEFT_MASK      (0x0FFF0000)

/*!
* \brief
* Register : CSDI_PIX16TO10_CHROMA_CROP
* CSDI Pix16to10 Chroma Crops
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_SIZE            (32)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_OFFSET          (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x2C)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_RESET_VALUE     (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_BITFIELD_MASK   (0x00000373)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_RWMASK          (0x00000373)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_ROMASK          (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_WOMASK          (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_UNUSED_MASK     (0xFFFFFC8C)

/*!
* \brief
* Bit-field : RIGHT
* number of pixels (10bpp resolution) to be removed at the end of the transfer at the output of the Pix16to10 PU.
* Unsigned value from 0 to 3 (default is 0).
* Used in every 16to10 related to CSDI chroma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_RIGHT_OFFSET    (0x00000000)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_RIGHT_WIDTH     (2)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_RIGHT_MASK      (0x00000003)

/*!
* \brief
* Bit-field : CENTRE
* number of pixels (10bpp resolution) to be removed at the middle of the transfer at the output of the Pix16to10 PU. Only used for 3D-TV side by side processing.
* Unsigned value from 0 to 6 (default is 0).
* Used in every 16to10 related to CSDI chroma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_CENTRE_OFFSET   (0x00000004)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_CENTRE_WIDTH    (3)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_CENTRE_MASK     (0x00000070)

/*!
* \brief
* Bit-field : LEFT
* number of pixels (10bpp resolution) to be removed at the beginning of the transfer at the output of the Pix16to10 PU.
* Unsigned value from 0 to 3 (default is 0).
* Used in every 16to10 related to CSDI chroma.
*/

#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_LEFT_OFFSET     (0x00000008)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_LEFT_WIDTH      (2)
#define HQRCSDIRegBank_CSDI_PIX16TO10_CHROMA_CROP_LEFT_MASK       (0x00000300)

/*!
* \brief
* Register : FMD_WIDTH
* FMD viewport Line width
*/

#define HQRCSDIRegBank_FMD_WIDTH_SIZE                             (32)
#define HQRCSDIRegBank_FMD_WIDTH_OFFSET                           (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x30)
#define HQRCSDIRegBank_FMD_WIDTH_RESET_VALUE                      (0x00000780)
#define HQRCSDIRegBank_FMD_WIDTH_BITFIELD_MASK                    (0x00001FFF)
#define HQRCSDIRegBank_FMD_WIDTH_RWMASK                           (0x00001FFF)
#define HQRCSDIRegBank_FMD_WIDTH_ROMASK                           (0x00000000)
#define HQRCSDIRegBank_FMD_WIDTH_WOMASK                           (0x00000000)
#define HQRCSDIRegBank_FMD_WIDTH_UNUSED_MASK                      (0xFFFFE000)

/*!
* \brief
* Bit-field : WIDTH_FMD
* input picture line width in Luma pixels.
* Unsigned value from 48 to 4096, must always be a multiple of 2 (default is 1920).
* Used in every VC1 and CRC8 related to FMD.
*/

#define HQRCSDIRegBank_FMD_WIDTH_WIDTH_FMD_OFFSET                 (0x00000000)
#define HQRCSDIRegBank_FMD_WIDTH_WIDTH_FMD_WIDTH                  (13)
#define HQRCSDIRegBank_FMD_WIDTH_WIDTH_FMD_MASK                   (0x00001FFF)

/*!
* \brief
* Register : INPUT_WIDTH
* CRC Input width
*/

#define HQRCSDIRegBank_INPUT_WIDTH_SIZE                           (32)
#define HQRCSDIRegBank_INPUT_WIDTH_OFFSET                         (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x34)
#define HQRCSDIRegBank_INPUT_WIDTH_RESET_VALUE                    (0x00000780)
#define HQRCSDIRegBank_INPUT_WIDTH_BITFIELD_MASK                  (0x00001FFF)
#define HQRCSDIRegBank_INPUT_WIDTH_RWMASK                         (0x00001FFF)
#define HQRCSDIRegBank_INPUT_WIDTH_ROMASK                         (0x00000000)
#define HQRCSDIRegBank_INPUT_WIDTH_WOMASK                         (0x00000000)
#define HQRCSDIRegBank_INPUT_WIDTH_UNUSED_MASK                    (0xFFFFE000)

/*!
* \brief
* Bit-field : WIDTH_IN
* input picture line width in Luma pixels.
* Unsigned value from 48 to 4096, must always be a multiple of 2 (default is 1920).
* Used in every input CRC8/10 related to input picture, and also demux444.
*/

#define HQRCSDIRegBank_INPUT_WIDTH_WIDTH_IN_OFFSET                (0x00000000)
#define HQRCSDIRegBank_INPUT_WIDTH_WIDTH_IN_WIDTH                 (13)
#define HQRCSDIRegBank_INPUT_WIDTH_WIDTH_IN_MASK                  (0x00001FFF)

/*!
* \brief
* Register : CSDI_WIDTH
* CSDi Line width
*/

#define HQRCSDIRegBank_CSDI_WIDTH_SIZE                            (32)
#define HQRCSDIRegBank_CSDI_WIDTH_OFFSET                          (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x38)
#define HQRCSDIRegBank_CSDI_WIDTH_RESET_VALUE                     (0x07800780)
#define HQRCSDIRegBank_CSDI_WIDTH_BITFIELD_MASK                   (0x1FFF1FFF)
#define HQRCSDIRegBank_CSDI_WIDTH_RWMASK                          (0x1FFF1FFF)
#define HQRCSDIRegBank_CSDI_WIDTH_ROMASK                          (0x00000000)
#define HQRCSDIRegBank_CSDI_WIDTH_WOMASK                          (0x00000000)
#define HQRCSDIRegBank_CSDI_WIDTH_UNUSED_MASK                     (0xE000E000)

/*!
* \brief
* Bit-field : WIDTH_VIDEO
* CSDI line width in Luma pixels.
* Unsigned value from 48 to 4096, must always be a multiple of 2 (default is 1920).
* Used in every REMY, REMUV, VC1, CUP and CRC10 related to CSDI.
*/

#define HQRCSDIRegBank_CSDI_WIDTH_WIDTH_VIDEO_OFFSET              (0x00000000)
#define HQRCSDIRegBank_CSDI_WIDTH_WIDTH_VIDEO_WIDTH               (13)
#define HQRCSDIRegBank_CSDI_WIDTH_WIDTH_VIDEO_MASK                (0x00001FFF)

/*!
* \brief
* Bit-field : WIDTH_MOTION
* CSDI line width of motion.
* Unsigned value from 48 to 4096, must always be a multiple of 2 (default is 1920).
* Used in every CRC8 motion.
*/

#define HQRCSDIRegBank_CSDI_WIDTH_WIDTH_MOTION_OFFSET             (0x00000010)
#define HQRCSDIRegBank_CSDI_WIDTH_WIDTH_MOTION_WIDTH              (13)
#define HQRCSDIRegBank_CSDI_WIDTH_WIDTH_MOTION_MASK               (0x1FFF0000)

/*!
* \brief
* Register : HVSRC_OUTPUT_WIDTH
* HVSRC Output picture line width
*/

#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_SIZE                    (32)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x3C)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_RESET_VALUE             (0x07800780)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_BITFIELD_MASK           (0x1FFF1FFF)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_RWMASK                  (0x1FFF1FFF)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_UNUSED_MASK             (0xE000E000)

/*!
* \brief
* Bit-field : WIDTH_LUMA
* output picture line width in pixels.
* Unsigned value from 48 to 4096, must always be a multiple of 2 (default is 1920).
* Used in CRC10 related to HVSRC luma and every padder related to IQI input data-path..
*/

#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_WIDTH_LUMA_OFFSET       (0x00000000)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_WIDTH_LUMA_WIDTH        (13)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_WIDTH_LUMA_MASK         (0x00001FFF)

/*!
* \brief
* Bit-field : WIDTH_CHROMA
* output picture line width in pixels.
* Unsigned value from 48 to 4096, must always be a multiple of 2 (default is 1920).
* Used in every CRC10 related to HVSRC chroma.
*/

#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_WIDTH_CHROMA_OFFSET     (0x00000010)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_WIDTH_CHROMA_WIDTH      (13)
#define HQRCSDIRegBank_HVSRC_OUTPUT_WIDTH_WIDTH_CHROMA_MASK       (0x1FFF0000)

/*!
* \brief
* Register : OFFSET_LUMA_WIDTH
* 3D offset luma widths
*/

#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_SIZE                     (32)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_OFFSET                   (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x40)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_RESET_VALUE              (0x07800780)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_BITFIELD_MASK            (0x1FFF1FFF)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_RWMASK                   (0x1FFF1FFF)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_ROMASK                   (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_WOMASK                   (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_UNUSED_MASK              (0xE000E000)

/*!
* \brief
* Bit-field : RIGHT
* number of luma samples for the right eye.
* Unsigned value from 48 to 4096 (default is 1920).
* Used in pady.
*/

#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_RIGHT_OFFSET             (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_RIGHT_WIDTH              (13)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_RIGHT_MASK               (0x00001FFF)

/*!
* \brief
* Bit-field : LEFT
* number of luma samples for the left eye.
* Unsigned value from 48 to 4096 (default is 1920).
* Used in pady.
*/

#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_LEFT_OFFSET              (0x00000010)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_LEFT_WIDTH               (13)
#define HQRCSDIRegBank_OFFSET_LUMA_WIDTH_LEFT_MASK                (0x1FFF0000)

/*!
* \brief
* Register : OFFSET_LUMA_LEFT_VIEW
* 3D offset luma left view border widths
*/

#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_SIZE                 (32)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_OFFSET               (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x44)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_RESET_VALUE          (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_BITFIELD_MASK        (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_RWMASK               (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_ROMASK               (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_WOMASK               (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_UNUSED_MASK          (0xF800F800)

/*!
* \brief
* Bit-field : LR
* number of luma samples to be added at the rigth border by the padder PU.
* Used in pady.
*/

#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_LR_OFFSET            (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_LR_WIDTH             (11)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_LR_MASK              (0x000007FF)

/*!
* \brief
* Bit-field : LL
* number of luma samples to be added at the left border by the padder PU.
* Used in pady.
*/

#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_LL_OFFSET            (0x00000010)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_LL_WIDTH             (11)
#define HQRCSDIRegBank_OFFSET_LUMA_LEFT_VIEW_LL_MASK              (0x07FF0000)

/*!
* \brief
* Register : OFFSET_LUMA_RIGHT_VIEW
* 3D offset luma right view border widths
*/

#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_SIZE                (32)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_OFFSET              (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x48)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RESET_VALUE         (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_BITFIELD_MASK       (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RWMASK              (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_ROMASK              (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_WOMASK              (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_UNUSED_MASK         (0xF800F800)

/*!
* \brief
* Bit-field : RR
* number of luma samples to be added at the rigth border by the padder PU.
* Used in pady.
*/

#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RR_OFFSET           (0x00000000)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RR_WIDTH            (11)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RR_MASK             (0x000007FF)

/*!
* \brief
* Bit-field : RL
* number of luma samples to be added at the left border by the padder PU.
* Used in pady.
*/

#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RL_OFFSET           (0x00000010)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RL_WIDTH            (11)
#define HQRCSDIRegBank_OFFSET_LUMA_RIGHT_VIEW_RL_MASK             (0x07FF0000)

/*!
* \brief
* Register : OFFSET_CHROMA_WIDTH
* 3D offset chroma widths
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_SIZE                   (32)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x4C)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_RESET_VALUE            (0x07800780)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_BITFIELD_MASK          (0x1FFF1FFF)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_RWMASK                 (0x1FFF1FFF)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_UNUSED_MASK            (0xE000E000)

/*!
* \brief
* Bit-field : RIGHT
* number of luma samples for the right eye.
* Unsigned value from 48 to 4096 (default is 1920).
* Used in paduv related to HVSRC input data-path.
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_RIGHT_OFFSET           (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_RIGHT_WIDTH            (13)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_RIGHT_MASK             (0x00001FFF)

/*!
* \brief
* Bit-field : LEFT
* number of chroma samples for the left eye.
* Unsigned value from 48 to 4096 (default is 1920).
* Used in paduv related to HVSRC input data-path.
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_LEFT_OFFSET            (0x00000010)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_LEFT_WIDTH             (13)
#define HQRCSDIRegBank_OFFSET_CHROMA_WIDTH_LEFT_MASK              (0x1FFF0000)

/*!
* \brief
* Register : OFFSET_CHROMA_LEFT_VIEW
* 3D offset chroma left view border widths
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_SIZE               (32)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_OFFSET             (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x50)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_RESET_VALUE        (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_BITFIELD_MASK      (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_RWMASK             (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_ROMASK             (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_WOMASK             (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_UNUSED_MASK        (0xF800F800)

/*!
* \brief
* Bit-field : LR
* number of chroma samples to be added at the rigth border by the padder PU.
* Used in paduv related to HVSRC input data-path.
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_LR_OFFSET          (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_LR_WIDTH           (11)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_LR_MASK            (0x000007FF)

/*!
* \brief
* Bit-field : LL
* number of chroma samples to be added at the left border by the padder PU.
* Used in paduv related to HVSRC input data-path.
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_LL_OFFSET          (0x00000010)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_LL_WIDTH           (11)
#define HQRCSDIRegBank_OFFSET_CHROMA_LEFT_VIEW_LL_MASK            (0x07FF0000)

/*!
* \brief
* Register : OFFSET_CHROMA_RIGHT_VIEW
* 3D offset chroma right view border widths
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_SIZE              (32)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_OFFSET            (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x54)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RESET_VALUE       (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_BITFIELD_MASK     (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RWMASK            (0x07FF07FF)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_ROMASK            (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_WOMASK            (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_UNUSED_MASK       (0xF800F800)

/*!
* \brief
* Bit-field : RR
* number of chroma samples to be added at the rigth border by the padder PU.
* Used in paduv related to HVSRC input data-path.
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RR_OFFSET         (0x00000000)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RR_WIDTH          (11)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RR_MASK           (0x000007FF)

/*!
* \brief
* Bit-field : RL
* number of chroma samples to be added at the left border by the padder PU.
* Used in paduv related to HVSRC input data-path.
*/

#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RL_OFFSET         (0x00000010)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RL_WIDTH          (11)
#define HQRCSDIRegBank_OFFSET_CHROMA_RIGHT_VIEW_RL_MASK           (0x07FF0000)

/*!
* \brief
* Register : IQI_WIDTH
* IQI widths
*/

#define HQRCSDIRegBank_IQI_WIDTH_SIZE                             (32)
#define HQRCSDIRegBank_IQI_WIDTH_OFFSET                           (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x58)
#define HQRCSDIRegBank_IQI_WIDTH_RESET_VALUE                      (0x07800780)
#define HQRCSDIRegBank_IQI_WIDTH_BITFIELD_MASK                    (0x1FFF1FFF)
#define HQRCSDIRegBank_IQI_WIDTH_RWMASK                           (0x1FFF1FFF)
#define HQRCSDIRegBank_IQI_WIDTH_ROMASK                           (0x00000000)
#define HQRCSDIRegBank_IQI_WIDTH_WOMASK                           (0x00000000)
#define HQRCSDIRegBank_IQI_WIDTH_UNUSED_MASK                      (0xE000E000)

/*!
* \brief
* Bit-field : RIGHT
* number of luma samples for the right eye.
* Unsigned value from 48 to 4096 (default is 1920).
* Used in every padder related to IQI input data-path.
*/

#define HQRCSDIRegBank_IQI_WIDTH_RIGHT_OFFSET                     (0x00000000)
#define HQRCSDIRegBank_IQI_WIDTH_RIGHT_WIDTH                      (13)
#define HQRCSDIRegBank_IQI_WIDTH_RIGHT_MASK                       (0x00001FFF)

/*!
* \brief
* Bit-field : LEFT
* number of luma samples for the left eye.
* Unsigned value from 48 to 4096 (default is 1920).
* Used in every padder related to IQI input data-path.
*/

#define HQRCSDIRegBank_IQI_WIDTH_LEFT_OFFSET                      (0x00000010)
#define HQRCSDIRegBank_IQI_WIDTH_LEFT_WIDTH                       (13)
#define HQRCSDIRegBank_IQI_WIDTH_LEFT_MASK                        (0x1FFF0000)

/*!
* \brief
* Register : IQI_LEFT_VIEW
* IQI left view border widths
*/

#define HQRCSDIRegBank_IQI_LEFT_VIEW_SIZE                         (32)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_OFFSET                       (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x5C)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_RESET_VALUE                  (0x00000000)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_BITFIELD_MASK                (0x07FF07FF)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_RWMASK                       (0x07FF07FF)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_ROMASK                       (0x00000000)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_WOMASK                       (0x00000000)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_UNUSED_MASK                  (0xF800F800)

/*!
* \brief
* Bit-field : LR
* number of luma samples to be added at the rigth border by the padder PU.
* Used in every padder related to IQI input data-path.
*/

#define HQRCSDIRegBank_IQI_LEFT_VIEW_LR_OFFSET                    (0x00000000)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_LR_WIDTH                     (11)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_LR_MASK                      (0x000007FF)

/*!
* \brief
* Bit-field : LL
* number of luma samples to be added at the left border by the padder PU.
* Used in every padder related to IQI input data-path.
*/

#define HQRCSDIRegBank_IQI_LEFT_VIEW_LL_OFFSET                    (0x00000010)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_LL_WIDTH                     (11)
#define HQRCSDIRegBank_IQI_LEFT_VIEW_LL_MASK                      (0x07FF0000)

/*!
* \brief
* Register : IQI_RIGHT_VIEW
* IQI right view border widths
*/

#define HQRCSDIRegBank_IQI_RIGHT_VIEW_SIZE                        (32)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_OFFSET                      (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x60)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RESET_VALUE                 (0x00000000)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_BITFIELD_MASK               (0x07FF07FF)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RWMASK                      (0x07FF07FF)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_ROMASK                      (0x00000000)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_WOMASK                      (0x00000000)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_UNUSED_MASK                 (0xF800F800)

/*!
* \brief
* Bit-field : RR
* number of luma samples to be added at the rigth border by the padder PU.
* Used in every padder related to IQI input data-path.
*/

#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RR_OFFSET                   (0x00000000)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RR_WIDTH                    (11)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RR_MASK                     (0x000007FF)

/*!
* \brief
* Bit-field : RL
* number of luma samples to be added at the left border by the padder PU.
* Used in every padder related to IQI input data-path.
*/

#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RL_OFFSET                   (0x00000010)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RL_WIDTH                    (11)
#define HQRCSDIRegBank_IQI_RIGHT_VIEW_RL_MASK                     (0x07FF0000)

/*!
* \brief
* Register : SIDE_STRIPE_COLOR
* Side stripe color
*/

#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_SIZE                     (32)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_OFFSET                   (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x64)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_RESET_VALUE              (0x00000000)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_BITFIELD_MASK            (0x3FFFFFFF)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_RWMASK                   (0x3FFFFFFF)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_ROMASK                   (0x00000000)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_WOMASK                   (0x00000000)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_UNUSED_MASK              (0xC0000000)

/*!
* \brief
* Bit-field : DEFAULT_V
* default chroma red value to be added by the padder PU.
* Used in every chroma padder.
*/

#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_V_OFFSET         (0x00000000)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_V_WIDTH          (10)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_V_MASK           (0x000003FF)

/*!
* \brief
* Bit-field : DEFAULT_U
* default chroma blue value to be added by the padder PU.
* Used in every chroma padder.
*/

#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_U_OFFSET         (0x0000000a)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_U_WIDTH          (10)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_U_MASK           (0x000FFC00)

/*!
* \brief
* Bit-field : DEFAULT_Y
* default luma value to be added by the padder PU.
* Used in every luma padder.
*/

#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_Y_OFFSET         (0x00000014)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_Y_WIDTH          (10)
#define HQRCSDIRegBank_SIDE_STRIPE_COLOR_DEFAULT_Y_MASK           (0x3FF00000)

/*!
* \brief
* Register : VC1RE_CTRL_PRV_CSDI
* VC1RE Controls for Previous field to CSDi
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_SIZE                   (32)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x68)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_RESET_VALUE            (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_BITFIELD_MASK          (0x00770003)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_RWMASK                 (0x00770003)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_UNUSED_MASK            (0xFF88FFFC)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of luma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_LUMA_OFFSET (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_LUMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_LUMA_MASK (0x00000001)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_CHROMA
* Enable of chroma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_CHROMA_OFFSET (0x00000001)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_CHROMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_ENABLE_CHROMA_MASK (0x00000002)

/*!
* \brief
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_LUMA_OFFSET (0x00000010)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_LUMA_WIDTH  (3)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_LUMA_MASK   (0x00070000)

/*!
* \brief
* Bit-field : VC1RE_COEF_CHROMA
* Coefficient for chroma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_CHROMA_OFFSET (0x00000014)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_CHROMA_WIDTH (3)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_CSDI_VC1RE_COEF_CHROMA_MASK (0x00700000)

/*!
* \brief
* Register : VC1RE_CTRL_CUR_CSDI
* VC1RE Controls for Current field to CSDi
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_SIZE                   (32)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x6C)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_RESET_VALUE            (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_BITFIELD_MASK          (0x00770003)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_RWMASK                 (0x00770003)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_UNUSED_MASK            (0xFF88FFFC)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of luma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_LUMA_OFFSET (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_LUMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_LUMA_MASK (0x00000001)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_CHROMA
* Enable of chroma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_CHROMA_OFFSET (0x00000001)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_CHROMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_ENABLE_CHROMA_MASK (0x00000002)

/*!
* \brief
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_LUMA_OFFSET (0x00000010)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_LUMA_WIDTH  (3)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_LUMA_MASK   (0x00070000)

/*!
* \brief
* Bit-field : VC1RE_COEF_CHROMA
* Coefficient for chroma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_CHROMA_OFFSET (0x00000014)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_CHROMA_WIDTH (3)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_CSDI_VC1RE_COEF_CHROMA_MASK (0x00700000)

/*!
* \brief
* Register : VC1RE_CTRL_NXT_CSDI
* VC1RE Controls for Next field to CSDi
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_SIZE                   (32)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x70)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_RESET_VALUE            (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_BITFIELD_MASK          (0x00770003)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_RWMASK                 (0x00770003)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_UNUSED_MASK            (0xFF88FFFC)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of luma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_LUMA_OFFSET (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_LUMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_LUMA_MASK (0x00000001)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_CHROMA
* Enable of chroma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_CHROMA_OFFSET (0x00000001)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_CHROMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_ENABLE_CHROMA_MASK (0x00000002)

/*!
* \brief
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_LUMA_OFFSET (0x00000010)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_LUMA_WIDTH  (3)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_LUMA_MASK   (0x00070000)

/*!
* \brief
* Bit-field : VC1RE_COEF_CHROMA
* Coefficient for chroma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_CHROMA_OFFSET (0x00000014)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_CHROMA_WIDTH (3)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_CSDI_VC1RE_COEF_CHROMA_MASK (0x00700000)

/*!
* \brief
* Register : VC1RE_CTRL_PRV_FMD
* VC1RE Controls for Previous field to FMD
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_SIZE                    (32)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x74)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_RESET_VALUE             (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_BITFIELD_MASK           (0x00070001)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_RWMASK                  (0x00070001)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_UNUSED_MASK             (0xFFF8FFFE)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of luma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_VC1RE_ENABLE_LUMA_OFFSET (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_VC1RE_ENABLE_LUMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_VC1RE_ENABLE_LUMA_MASK  (0x00000001)

/*!
* \brief
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_VC1RE_COEF_LUMA_OFFSET  (0x00000010)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_VC1RE_COEF_LUMA_WIDTH   (3)
#define HQRCSDIRegBank_VC1RE_CTRL_PRV_FMD_VC1RE_COEF_LUMA_MASK    (0x00070000)

/*!
* \brief
* Register : VC1RE_CTRL_CUR_FMD
* VC1RE Controls for Current field to FMD
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_SIZE                    (32)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x78)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_RESET_VALUE             (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_BITFIELD_MASK           (0x00070001)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_RWMASK                  (0x00070001)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_UNUSED_MASK             (0xFFF8FFFE)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of luma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_VC1RE_ENABLE_LUMA_OFFSET (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_VC1RE_ENABLE_LUMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_VC1RE_ENABLE_LUMA_MASK  (0x00000001)

/*!
* \brief
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_VC1RE_COEF_LUMA_OFFSET  (0x00000010)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_VC1RE_COEF_LUMA_WIDTH   (3)
#define HQRCSDIRegBank_VC1RE_CTRL_CUR_FMD_VC1RE_COEF_LUMA_MASK    (0x00070000)

/*!
* \brief
* Register : VC1RE_CTRL_NXT_FMD
* VC1RE Controls for Next field to FMD
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_SIZE                    (32)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x7C)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_RESET_VALUE             (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_BITFIELD_MASK           (0x00070001)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_RWMASK                  (0x00070001)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_UNUSED_MASK             (0xFFF8FFFE)

/*!
* \brief
* Bit-field : VC1RE_ENABLE_LUMA
* Enable of luma VC1 Range Engine:
* B_0x0: Bypass mode (default) B_0x1: Functional mode
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_VC1RE_ENABLE_LUMA_OFFSET (0x00000000)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_VC1RE_ENABLE_LUMA_WIDTH (1)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_VC1RE_ENABLE_LUMA_MASK  (0x00000001)

/*!
* \brief
* Bit-field : VC1RE_COEF_LUMA
* Coefficient for luma VC1 Range Engine.
* Coeff in [0:7] for Range Mapping
* Coeff = 7 for Range Reduction
*/

#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_VC1RE_COEF_LUMA_OFFSET  (0x00000010)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_VC1RE_COEF_LUMA_WIDTH   (3)
#define HQRCSDIRegBank_VC1RE_CTRL_NXT_FMD_VC1RE_COEF_LUMA_MASK    (0x00070000)

/*!
* \brief
* Register : CUP_CTRL
* CUP Controls
*/

#define HQRCSDIRegBank_CUP_CTRL_SIZE                              (32)
#define HQRCSDIRegBank_CUP_CTRL_OFFSET                            (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x80)
#define HQRCSDIRegBank_CUP_CTRL_RESET_VALUE                       (0x00000001)
#define HQRCSDIRegBank_CUP_CTRL_BITFIELD_MASK                     (0x00000001)
#define HQRCSDIRegBank_CUP_CTRL_RWMASK                            (0x00000001)
#define HQRCSDIRegBank_CUP_CTRL_ROMASK                            (0x00000000)
#define HQRCSDIRegBank_CUP_CTRL_WOMASK                            (0x00000000)
#define HQRCSDIRegBank_CUP_CTRL_UNUSED_MASK                       (0xFFFFFFFE)

/*!
* \brief
* Bit-field : VERTICALFILTERBYPASS
* Bypass of Vertical Filter PU:
* B_0x0: PU Functional mode B_0x1:PU Bypass mode (default)
* Caution: VERTICALFILTERBYPASS is controlled by FW. In Normal mode, the FW programs the vertical filter PU in order to alternatively bypass a significant line or make the average of two consecutive significant lines; in Bypass mode, the vertical filter PU is always bypassed.
*/

#define HQRCSDIRegBank_CUP_CTRL_VERTICALFILTERBYPASS_OFFSET       (0x00000000)
#define HQRCSDIRegBank_CUP_CTRL_VERTICALFILTERBYPASS_WIDTH        (1)
#define HQRCSDIRegBank_CUP_CTRL_VERTICALFILTERBYPASS_MASK         (0x00000001)

/**
* Value : B_0x0
* PU Functional mode
*/

#define HQRCSDIRegBank_CUP_CTRL_VERTICALFILTERBYPASS_B_0x0 (0x00000000)

/**
* Value : B_0x1
* PU Bypass mode (default)
*/

#define HQRCSDIRegBank_CUP_CTRL_VERTICALFILTERBYPASS_B_0x1 (0x00000001)

/*!
* \brief
* Register : CUP_DL_INIT
* CUP Delay Lines INIT bits
*/

#define HQRCSDIRegBank_CUP_DL_INIT_SIZE                           (32)
#define HQRCSDIRegBank_CUP_DL_INIT_OFFSET                         (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x84)
#define HQRCSDIRegBank_CUP_DL_INIT_RESET_VALUE                    (0x00000000)
#define HQRCSDIRegBank_CUP_DL_INIT_BITFIELD_MASK                  (0x00000001)
#define HQRCSDIRegBank_CUP_DL_INIT_RWMASK                         (0x00000001)
#define HQRCSDIRegBank_CUP_DL_INIT_ROMASK                         (0x00000000)
#define HQRCSDIRegBank_CUP_DL_INIT_WOMASK                         (0x00000000)
#define HQRCSDIRegBank_CUP_DL_INIT_UNUSED_MASK                    (0xFFFFFFFE)

/*!
* \brief
* Bit-field : VERTICALFILTERINIT
* Initialization of Vertical Filter DL:
* B_0x0: DL Functional mode (default) B_0x1:DL Initialization
* Caution: VERTICALFILTERINIT is controlled by FW.
*/

#define HQRCSDIRegBank_CUP_DL_INIT_VERTICALFILTERINIT_OFFSET      (0x00000000)
#define HQRCSDIRegBank_CUP_DL_INIT_VERTICALFILTERINIT_WIDTH       (1)
#define HQRCSDIRegBank_CUP_DL_INIT_VERTICALFILTERINIT_MASK        (0x00000001)

/**
* Value : B_0x0
* DL Functional mode (default)
*/

#define HQRCSDIRegBank_CUP_DL_INIT_VERTICALFILTERINIT_B_0x0 (0x00000000)

/**
* Value : B_0x1
* DL Initialization
*/

#define HQRCSDIRegBank_CUP_DL_INIT_VERTICALFILTERINIT_B_0x1 (0x00000001)

/*!
* \brief
* Register : CUP_DL_FLUSH
* CUP Delay Lines FLUSH bits
*/

#define HQRCSDIRegBank_CUP_DL_FLUSH_SIZE                          (32)
#define HQRCSDIRegBank_CUP_DL_FLUSH_OFFSET                        (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x88)
#define HQRCSDIRegBank_CUP_DL_FLUSH_RESET_VALUE                   (0x00000000)
#define HQRCSDIRegBank_CUP_DL_FLUSH_BITFIELD_MASK                 (0x00000001)
#define HQRCSDIRegBank_CUP_DL_FLUSH_RWMASK                        (0x00000001)
#define HQRCSDIRegBank_CUP_DL_FLUSH_ROMASK                        (0x00000000)
#define HQRCSDIRegBank_CUP_DL_FLUSH_WOMASK                        (0x00000000)
#define HQRCSDIRegBank_CUP_DL_FLUSH_UNUSED_MASK                   (0xFFFFFFFE)

/*!
* \brief
* Bit-field : VERTICALFILTERFLUSH
* Flush of Vertical Filter DL:
* B_0x0: DL Functional mode (default) B_0x1:DL Flush
* Caution: VERTICALFILTERFLUSH is controlled by FW.
*/

#define HQRCSDIRegBank_CUP_DL_FLUSH_VERTICALFILTERFLUSH_OFFSET    (0x00000000)
#define HQRCSDIRegBank_CUP_DL_FLUSH_VERTICALFILTERFLUSH_WIDTH     (1)
#define HQRCSDIRegBank_CUP_DL_FLUSH_VERTICALFILTERFLUSH_MASK      (0x00000001)

/**
* Value : B_0x0
* DL Functional mode (default)
*/

#define HQRCSDIRegBank_CUP_DL_FLUSH_VERTICALFILTERFLUSH_B_0x0 (0x00000000)

/**
* Value : B_0x1
* DL Flush
*/

#define HQRCSDIRegBank_CUP_DL_FLUSH_VERTICALFILTERFLUSH_B_0x1 (0x00000001)

/*!
* \brief
* Register : CUP_DL_SKIP
* CUP Delay Lines SKIP bits
*/

#define HQRCSDIRegBank_CUP_DL_SKIP_SIZE                           (32)
#define HQRCSDIRegBank_CUP_DL_SKIP_OFFSET                         (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x8C)
#define HQRCSDIRegBank_CUP_DL_SKIP_RESET_VALUE                    (0x00000000)
#define HQRCSDIRegBank_CUP_DL_SKIP_BITFIELD_MASK                  (0x00000001)
#define HQRCSDIRegBank_CUP_DL_SKIP_RWMASK                         (0x00000001)
#define HQRCSDIRegBank_CUP_DL_SKIP_ROMASK                         (0x00000000)
#define HQRCSDIRegBank_CUP_DL_SKIP_WOMASK                         (0x00000000)
#define HQRCSDIRegBank_CUP_DL_SKIP_UNUSED_MASK                    (0xFFFFFFFE)

/*!
* \brief
* Bit-field : VERTICALFILTERSKIP
* Skip of Vertical Filter DL:
* B_0x0: DL Functional mode (default) B_0x1:DL Skip
* Caution: VERTICALFILTERSKIP is controlled by FW.
*/

#define HQRCSDIRegBank_CUP_DL_SKIP_VERTICALFILTERSKIP_OFFSET      (0x00000000)
#define HQRCSDIRegBank_CUP_DL_SKIP_VERTICALFILTERSKIP_WIDTH       (1)
#define HQRCSDIRegBank_CUP_DL_SKIP_VERTICALFILTERSKIP_MASK        (0x00000001)

/**
* Value : B_0x0
* DL Functional mode (default)
*/

#define HQRCSDIRegBank_CUP_DL_SKIP_VERTICALFILTERSKIP_B_0x0 (0x00000000)

/**
* Value : B_0x1
* DL Skip
*/

#define HQRCSDIRegBank_CUP_DL_SKIP_VERTICALFILTERSKIP_B_0x1 (0x00000001)

/*!
* \brief
* Register : CUP_DL_REPEAT
* CUP Delay Lines REPEAT bits
*/

#define HQRCSDIRegBank_CUP_DL_REPEAT_SIZE                         (32)
#define HQRCSDIRegBank_CUP_DL_REPEAT_OFFSET                       (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x90)
#define HQRCSDIRegBank_CUP_DL_REPEAT_RESET_VALUE                  (0x00000000)
#define HQRCSDIRegBank_CUP_DL_REPEAT_BITFIELD_MASK                (0x00000001)
#define HQRCSDIRegBank_CUP_DL_REPEAT_RWMASK                       (0x00000001)
#define HQRCSDIRegBank_CUP_DL_REPEAT_ROMASK                       (0x00000000)
#define HQRCSDIRegBank_CUP_DL_REPEAT_WOMASK                       (0x00000000)
#define HQRCSDIRegBank_CUP_DL_REPEAT_UNUSED_MASK                  (0xFFFFFFFE)

/*!
* \brief
* Bit-field : VERTICALFILTERREPEAT
* Repeat of Vertical Filter DL:
* B_0x0: DL Functional mode (default) B_0x1:DL Repeat
* Caution: VERTICALFILTERREPEAT is controlled by FW.
*/

#define HQRCSDIRegBank_CUP_DL_REPEAT_VERTICALFILTERREPEAT_OFFSET  (0x00000000)
#define HQRCSDIRegBank_CUP_DL_REPEAT_VERTICALFILTERREPEAT_WIDTH   (1)
#define HQRCSDIRegBank_CUP_DL_REPEAT_VERTICALFILTERREPEAT_MASK    (0x00000001)

/**
* Value : B_0x0
* DL Functional mode (default)
*/

#define HQRCSDIRegBank_CUP_DL_REPEAT_VERTICALFILTERREPEAT_B_0x0 (0x00000000)

/**
* Value : B_0x1
* DL Repeat
*/

#define HQRCSDIRegBank_CUP_DL_REPEAT_VERTICALFILTERREPEAT_B_0x1 (0x00000001)

/*!
* \brief
* Register : IQI_WRQ_FLAGS
* IQI write queues control transfer
*/

#define HQRCSDIRegBank_IQI_WRQ_FLAGS_SIZE                         (32)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_OFFSET                       (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x94)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_RESET_VALUE                  (0x00000000)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_BITFIELD_MASK                (0x00000003)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_RWMASK                       (0x00000003)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_ROMASK                       (0x00000000)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_WOMASK                       (0x00000000)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_UNUSED_MASK                  (0xFFFFFFFC)

/*!
* \brief
* Bit-field : FIRST_LINE
* This flags is set high during the transfer of the first line to the IQI cluster then to zero:
* B_0x0: other line.
* B_0x1: first line
*/

#define HQRCSDIRegBank_IQI_WRQ_FLAGS_FIRST_LINE_OFFSET            (0x00000000)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_FIRST_LINE_WIDTH             (1)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_FIRST_LINE_MASK              (0x00000001)

/*!
* \brief
* Bit-field : LAST_LINE
* This flags is set high during the transfer of the last line to the IQI cluster then to zero:
* B_0x0: other line.
* B_0x1: last line
*/

#define HQRCSDIRegBank_IQI_WRQ_FLAGS_LAST_LINE_OFFSET             (0x00000001)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_LAST_LINE_WIDTH              (1)
#define HQRCSDIRegBank_IQI_WRQ_FLAGS_LAST_LINE_MASK               (0x00000002)

/*!
* \brief
* Register : HQRIE_CRC_CTRL
* CRC Controls
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_SIZE                        (32)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_OFFSET                      (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x98)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_RESET_VALUE                 (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_BITFIELD_MASK               (0x000F1FF8)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_RWMASK                      (0x000F1FF8)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_ROMASK                      (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_WOMASK                      (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_UNUSED_MASK                 (0xFFF0E007)

/*!
* \brief
* Bit-field : CRC_MOT_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_CSDI_OFFSET         (0x00000003)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_CSDI_WIDTH          (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_CSDI_MASK           (0x00000008)

/*!
* \brief
* Bit-field : CRC_UV_CUP
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CUP_OFFSET           (0x00000004)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CUP_WIDTH            (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CUP_MASK             (0x00000010)

/*!
* \brief
* Bit-field : CRC_UV_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CSDI_OFFSET          (0x00000005)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CSDI_WIDTH           (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CSDI_MASK            (0x00000020)

/*!
* \brief
* Bit-field : CRC_Y_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_CSDI_OFFSET           (0x00000006)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_CSDI_WIDTH            (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_CSDI_MASK             (0x00000040)

/*!
* \brief
* Bit-field : CRC_UV_NXT_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_NXT_CSDI_OFFSET      (0x00000007)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_NXT_CSDI_WIDTH       (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_NXT_CSDI_MASK        (0x00000080)

/*!
* \brief
* Bit-field : CRC_UV_CUR_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CUR_CSDI_OFFSET      (0x00000008)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CUR_CSDI_WIDTH       (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_CUR_CSDI_MASK        (0x00000100)

/*!
* \brief
* Bit-field : CRC_UV_PRV_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_PRV_CSDI_OFFSET      (0x00000009)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_PRV_CSDI_WIDTH       (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_PRV_CSDI_MASK        (0x00000200)

/*!
* \brief
* Bit-field : CRC_Y_NXT_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_NXT_CSDI_OFFSET       (0x0000000a)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_NXT_CSDI_WIDTH        (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_NXT_CSDI_MASK         (0x00000400)

/*!
* \brief
* Bit-field : CRC_Y_CUR_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_CUR_CSDI_OFFSET       (0x0000000b)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_CUR_CSDI_WIDTH        (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_CUR_CSDI_MASK         (0x00000800)

/*!
* \brief
* Bit-field : CRC_Y_PRV_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_PRV_CSDI_OFFSET       (0x0000000c)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_PRV_CSDI_WIDTH        (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_PRV_CSDI_MASK         (0x00001000)

/*!
* \brief
* Bit-field : CRC_UV_IN
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_IN_OFFSET            (0x00000010)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_IN_WIDTH             (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_UV_IN_MASK              (0x00010000)

/*!
* \brief
* Bit-field : CRC_Y_IN
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_IN_OFFSET             (0x00000011)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_IN_WIDTH              (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_Y_IN_MASK               (0x00020000)

/*!
* \brief
* Bit-field : CRC_MOT_CUR_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_CUR_CSDI_OFFSET     (0x00000012)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_CUR_CSDI_WIDTH      (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_CUR_CSDI_MASK       (0x00040000)

/*!
* \brief
* Bit-field : CRC_MOT_PRV_CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_PRV_CSDI_OFFSET     (0x00000013)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_PRV_CSDI_WIDTH      (1)
#define HQRCSDIRegBank_HQRIE_CRC_CTRL_CRC_MOT_PRV_CSDI_MASK       (0x00080000)

/*!
* \brief
* Register : HQRIE_CRC_HQR_CTRL
* CRC HQR Control
*/

#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_SIZE                    (32)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0x9C)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_RESET_VALUE             (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_BITFIELD_MASK           (0x00000007)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_RWMASK                  (0x00000007)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_UNUSED_MASK             (0xFFFFFFF8)

/*!
* \brief
* Bit-field : CRC_V_HQR
*/

#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_V_HQR_OFFSET        (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_V_HQR_WIDTH         (1)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_V_HQR_MASK          (0x00000001)

/*!
* \brief
* Bit-field : CRC_U_HQR
*/

#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_U_HQR_OFFSET        (0x00000001)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_U_HQR_WIDTH         (1)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_U_HQR_MASK          (0x00000002)

/*!
* \brief
* Bit-field : CRC_Y_HQR
*/

#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_Y_HQR_OFFSET        (0x00000002)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_Y_HQR_WIDTH         (1)
#define HQRCSDIRegBank_HQRIE_CRC_HQR_CTRL_CRC_Y_HQR_MASK          (0x00000004)

/*!
* \brief
* Register : HQRIE_CRC_FMD_CTRL
* CRC FMD Control
*/

#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_SIZE                    (32)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xA0)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_RESET_VALUE             (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_BITFIELD_MASK           (0x00000007)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_RWMASK                  (0x00000007)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_UNUSED_MASK             (0xFFFFFFF8)

/*!
* \brief
* Bit-field : CRC_Y_NXT_FMD
*/

#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_NXT_FMD_OFFSET    (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_NXT_FMD_WIDTH     (1)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_NXT_FMD_MASK      (0x00000001)

/*!
* \brief
* Bit-field : CRC_Y_CUR_FMD
*/

#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_CUR_FMD_OFFSET    (0x00000001)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_CUR_FMD_WIDTH     (1)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_CUR_FMD_MASK      (0x00000002)

/*!
* \brief
* Bit-field : CRC_Y_PRV_FMD
*/

#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_PRV_FMD_OFFSET    (0x00000002)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_PRV_FMD_WIDTH     (1)
#define HQRCSDIRegBank_HQRIE_CRC_FMD_CTRL_CRC_Y_PRV_FMD_MASK      (0x00000004)

/*!
* \brief
* Register : HQRIE_CRC_Y_IN
* CRC Value Luma input field
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_SIZE                        (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_OFFSET                      (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xA4)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_RESET_VALUE                 (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_BITFIELD_MASK               (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_RWMASK                      (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_ROMASK                      (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_WOMASK                      (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_UNUSED_MASK                 (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_CRC_RWP_OFFSET              (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_CRC_RWP_WIDTH               (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_IN_CRC_RWP_MASK                (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_UV_IN
* CRC Value Chroma input field
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_SIZE                       (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_OFFSET                     (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xA8)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_RESET_VALUE                (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_BITFIELD_MASK              (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_RWMASK                     (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_ROMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_WOMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_UNUSED_MASK                (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_CRC_RWP_OFFSET             (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_CRC_RWP_WIDTH              (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_IN_CRC_RWP_MASK               (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_PRV_FMD
* CRC Value Luma Previous field to FMD
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_SIZE                   (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xAC)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_RESET_VALUE            (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_BITFIELD_MASK          (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_RWMASK                 (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_UNUSED_MASK            (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_CRC_RWP_OFFSET         (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_CRC_RWP_WIDTH          (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_FMD_CRC_RWP_MASK           (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_CUR_FMD
* CRC Value Luma Current field to FMD
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_SIZE                   (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xB0)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_RESET_VALUE            (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_BITFIELD_MASK          (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_RWMASK                 (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_UNUSED_MASK            (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_CRC_RWP_OFFSET         (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_CRC_RWP_WIDTH          (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_FMD_CRC_RWP_MASK           (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_NXT_FMD
* CRC Value Luma Next field to FMD
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_SIZE                   (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_OFFSET                 (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xB4)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_RESET_VALUE            (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_BITFIELD_MASK          (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_RWMASK                 (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_ROMASK                 (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_WOMASK                 (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_UNUSED_MASK            (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_CRC_RWP_OFFSET         (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_CRC_RWP_WIDTH          (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_FMD_CRC_RWP_MASK           (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_PRV_CSDI
* CRC Value Luma Previous field to CSDi
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_SIZE                  (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_OFFSET                (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xB8)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_RESET_VALUE           (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_BITFIELD_MASK         (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_RWMASK                (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_ROMASK                (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_WOMASK                (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_UNUSED_MASK           (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_CRC_RWP_OFFSET        (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_CRC_RWP_WIDTH         (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_PRV_CSDI_CRC_RWP_MASK          (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_CUR_CSDI
* CRC Value Luma Current field to CSDi
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_SIZE                  (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_OFFSET                (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xBC)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_RESET_VALUE           (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_BITFIELD_MASK         (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_RWMASK                (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_ROMASK                (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_WOMASK                (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_UNUSED_MASK           (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_CRC_RWP_OFFSET        (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_CRC_RWP_WIDTH         (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CUR_CSDI_CRC_RWP_MASK          (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_NXT_CSDI
* CRC Value Luma Next field to CSDi
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_SIZE                  (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_OFFSET                (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xC0)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_RESET_VALUE           (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_BITFIELD_MASK         (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_RWMASK                (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_ROMASK                (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_WOMASK                (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_UNUSED_MASK           (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_CRC_RWP_OFFSET        (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_CRC_RWP_WIDTH         (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_NXT_CSDI_CRC_RWP_MASK          (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_UV_PRV_CSDI
* CRC Value Chroma Previous field to CSDi
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_SIZE                 (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_OFFSET               (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xC4)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_RESET_VALUE          (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_BITFIELD_MASK        (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_RWMASK               (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_ROMASK               (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_WOMASK               (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_UNUSED_MASK          (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_CRC_RWP_OFFSET       (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_CRC_RWP_WIDTH        (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_PRV_CSDI_CRC_RWP_MASK         (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_UV_CUR_CSDI
* CRC Value Chroma Current field to CSDi
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_SIZE                 (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_OFFSET               (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xC8)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_RESET_VALUE          (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_BITFIELD_MASK        (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_RWMASK               (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_ROMASK               (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_WOMASK               (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_UNUSED_MASK          (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_CRC_RWP_OFFSET       (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_CRC_RWP_WIDTH        (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUR_CSDI_CRC_RWP_MASK         (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_UV_NXT_CSDI
* CRC Value Chroma Next field to CSDi
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_SIZE                 (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_OFFSET               (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xCC)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_RESET_VALUE          (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_BITFIELD_MASK        (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_RWMASK               (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_ROMASK               (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_WOMASK               (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_UNUSED_MASK          (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_CRC_RWP_OFFSET       (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_CRC_RWP_WIDTH        (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_NXT_CSDI_CRC_RWP_MASK         (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_CSDI
* CRC Value Luma deinterlaced field
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_SIZE                      (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_OFFSET                    (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xD0)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_RESET_VALUE               (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_BITFIELD_MASK             (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_RWMASK                    (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_ROMASK                    (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_WOMASK                    (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_UNUSED_MASK               (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_CRC_RWP_OFFSET            (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_CRC_RWP_WIDTH             (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_CSDI_CRC_RWP_MASK              (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_UV_CSDI
* CRC Value Chroma deinterlaced field
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_SIZE                     (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_OFFSET                   (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xD4)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_RESET_VALUE              (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_BITFIELD_MASK            (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_RWMASK                   (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_ROMASK                   (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_WOMASK                   (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_UNUSED_MASK              (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_CRC_RWP_OFFSET           (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_CRC_RWP_WIDTH            (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CSDI_CRC_RWP_MASK             (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_UV_CUP
* CRC Value Chroma deinterlaced field after upsampling (CUP)
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_SIZE                      (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_OFFSET                    (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xD8)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_RESET_VALUE               (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_BITFIELD_MASK             (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_RWMASK                    (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_ROMASK                    (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_WOMASK                    (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_UNUSED_MASK               (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_CRC_RWP_OFFSET            (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_CRC_RWP_WIDTH             (32)
#define HQRCSDIRegBank_HQRIE_CRC_UV_CUP_CRC_RWP_MASK              (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_MOT_CSDI
* CRC Value Motion CSDi output field
*/

#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_SIZE                    (32)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_OFFSET                  (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xDC)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_RESET_VALUE             (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_BITFIELD_MASK           (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_RWMASK                  (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_ROMASK                  (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_WOMASK                  (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_UNUSED_MASK             (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_CRC_RWP_OFFSET          (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_CRC_RWP_WIDTH           (32)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CSDI_CRC_RWP_MASK            (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_Y_HQR
* CRC Value Luma rescaled field
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_SIZE                       (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_OFFSET                     (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xE0)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_RESET_VALUE                (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_BITFIELD_MASK              (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_RWMASK                     (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_ROMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_WOMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_UNUSED_MASK                (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_CRC_RWP_OFFSET             (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_CRC_RWP_WIDTH              (32)
#define HQRCSDIRegBank_HQRIE_CRC_Y_HQR_CRC_RWP_MASK               (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_U_HQR
* CRC Value Chroma U rescaled field
*/

#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_SIZE                       (32)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_OFFSET                     (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xE4)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_RESET_VALUE                (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_BITFIELD_MASK              (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_RWMASK                     (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_ROMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_WOMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_UNUSED_MASK                (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_CRC_RWP_OFFSET             (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_CRC_RWP_WIDTH              (32)
#define HQRCSDIRegBank_HQRIE_CRC_U_HQR_CRC_RWP_MASK               (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_V_HQR
* CRC Value Chroma V rescaled field
*/

#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_SIZE                       (32)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_OFFSET                     (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xE8)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_RESET_VALUE                (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_BITFIELD_MASK              (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_RWMASK                     (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_ROMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_WOMASK                     (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_UNUSED_MASK                (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_CRC_RWP_OFFSET             (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_CRC_RWP_WIDTH              (32)
#define HQRCSDIRegBank_HQRIE_CRC_V_HQR_CRC_RWP_MASK               (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_MOT_PRV_CSDI
* CRC Value Previous Input Motion CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_SIZE                (32)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_OFFSET              (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xEC)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_RESET_VALUE         (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_BITFIELD_MASK       (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_RWMASK              (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_ROMASK              (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_WOMASK              (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_UNUSED_MASK         (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_CRC_RWP_OFFSET      (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_CRC_RWP_WIDTH       (32)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_PRV_CSDI_CRC_RWP_MASK        (0xFFFFFFFF)

/*!
* \brief
* Register : HQRIE_CRC_MOT_CUR_CSDI
* CRC Value Current Input Motion CSDI
*/

#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_SIZE                (32)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_OFFSET              (HQRCSDIRegBank_com_pu_regs_BASE_ADDR + 0xF0)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_RESET_VALUE         (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_BITFIELD_MASK       (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_RWMASK              (0xFFFFFFFF)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_ROMASK              (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_WOMASK              (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_UNUSED_MASK         (0x00000000)

/*!
* \brief
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_CRC_RWP_OFFSET      (0x00000000)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_CRC_RWP_WIDTH       (32)
#define HQRCSDIRegBank_HQRIE_CRC_MOT_CUR_CSDI_CRC_RWP_MASK        (0xFFFFFFFF)

#endif /** _HQRCSDIREGBANK_H_ */
