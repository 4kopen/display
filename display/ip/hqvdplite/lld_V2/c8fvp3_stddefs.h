/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/* Define to prevent recursive inclusion */
#ifndef _HQVDPLITE_STDDEFS_H
#define _HQVDPLITE_STDDEFS_H


/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/* Includes ---------------------------------------------------------------- */

#ifndef HQVDPLITE_API_FOR_STAPI
  #define MIN(a, b)   ((a) <= (b) ? (a) : (b))
  #define MAX(a, b)   ((a) <= (b) ? (b) : (a))
  #if defined (FW_STXP70) && (!defined (TLM_NATIVE))
    #include "global_verif_hal_stddefs.h"
    /* temporary to start migration to standard type */
    #define bool gvh_bool_t
    #define uint8_t gvh_u8_t
    #define uint16_t gvh_u16_t
    #define uint32_t gvh_u32_t

    #define yield()
  #else                /* #if defined (TLM_NATIVE) || defined (NATIVE_CORE) || defined (NCSIM_RTL)  Add NATIVE_CORE and NCSIM_RTL flags for SOC verif*/
    #include "hqvdp_lld_platform.h"
    #include "global_verif_services.h"
    #ifdef FW_STXP70
      #include "eswnative_base_esw.h"
      #define yield() esw_sync((unsigned int)1)
    #else
      #define yield() global_verif_hal.delay((unsigned int)1)
    #endif /* #ifdef FW_STXP70 */
  #endif /* #if defined (FW_STXP70) && !defined (TLM_NATIVE) */
#else

  #ifdef VIRTUAL_PLATFORM_TLM
    #include "eswnative_base_esw.h"
    #define yield() esw_sync((unsigned int)1)
  #else
    #define yield()
  #endif /* #ifdef VIRTUAL_PLATFORM_TLM */
#endif /*#ifndef HQVDPLITE_API_FOR_STAPI*/

/*------------------------------------------------------------------------------
 * Debug mode temporary done here. Therefore, must be move in stxp70_main.h file
 *----------------------------------------------------------------------------*/

#ifdef DEBUG_MODE
  #include <stdio.h>
  #define printf_on_debug printf
#else
  #define printf_on_debug(...) do {} while (0)
#endif

/* Exported Types ---------------------------------------------------------- */


/* Exported Constants ------------------------------------------------------ */

/* To define addresses aligned on 64-bits */
#define ALIGNED_64BITS(addr)    ((((addr) + 7) >> 3) << 3)

/* To define addresses aligned on 512 bytes (for MB alignment) */
#define ALIGNED_512BYTES(addr)    ((((addr) + 511) >> 9) << 9)
/* To define addresses aligned on 16 bytes (for MB alignment) */
#define ALIGNED_16BYTES(addr)    ((((addr) + 15) >> 4) << 4)
/* To define addresses aligned on 32 bytes (for MB alignment) */
#define ALIGNED_32BYTES(addr)    ((((addr) + 31) >> 5) << 5)
/* To define addresses aligned on 64 bytes (for MB alignment) */
#define ALIGNED_64BYTES(addr)    ((((addr) + 63) >> 6) << 6)

/* Exported Types ---------------------------------------------------------- */

/* Exported Variables ------------------------------------------------------ */

/* Exported Macros --------------------------------------------------------- */

/* Exported Functions ------------------------------------------------------ */

#if (!defined (HQVDPLITE_API_FOR_STAPI))
#pragma inline
static void WriteRegister(
    gvh_u32_t Addr,
    gvh_u32_t Value);
#pragma inline
static gvh_u32_t ReadRegister(
    gvh_u32_t Addr);

/*******************************************************************************
 * Name        : WriteRegister
 * Description : Assign a value (Value) to a register (Addr)
 * Parameters  : ???
 * Assumptions : ???
 * Limitations : ???
 * Returns     : ???
 * *******************************************************************************/
static void WriteRegister(
    gvh_u32_t Addr,
    gvh_u32_t Value)
{

#if ((defined (FW_STXP70) && !defined (TLM_NATIVE)) )
  /*#if defined (FW_STXP70) && !defined (TLM_NATIVE)  */
  volatile gvh_u32_t* Pt = (gvh_u32_t*) Addr;
  *Pt = Value;
#else
  /* volatile gvh_u32_t* Pt = (gvh_u32_t*) esw_tlm_to_host(Addr);
   * *Pt = Value;
   * yield();
   */
  /* If we want to stop using pointer dereferencing...
     U64 baseReg = (U64)Addr;
     gvh_u32_t data = Value; */
  global_verif_hal.write(Addr,0 , Value);
  /*esw_sync(1);*/
#endif


  /*printf("WriteRegister: write 0x%x at address 0x%x \n", Value, Addr);
    On systemC platform, force the current thread to be descheduled. This is
    necessary to do that if we want the streamer transfers to be done.
    esw_sync(0); */


}

/*******************************************************************************
 * Name        : ReadRegister
 * Description : Read a register (Addr)
 * Parameters  : ???
 * Assumptions : ???
 * Limitations : ???
 * Returns     : ???
 * *******************************************************************************/
static gvh_u32_t ReadRegister(
    gvh_u32_t Addr)
{
    gvh_u32_t Value;

#if ((defined (FW_STXP70) && !defined (TLM_NATIVE)) )
    /*#if defined (FW_STXP70) && !defined (TLM_NATIVE) */
    volatile gvh_u32_t* Pt = (gvh_u32_t*) Addr;
    Value = *Pt;
#else
    /*  volatile gvh_u32_t* Pt = (gvh_u32_t*) esw_tlm_to_host(Addr);
     *  Value = *Pt;
     *  yield();
     */
    /* If we want to stop using pointer dereferencing...

    Value = esw_read32((gvh_u64_t)Addr);
    U64 baseReg = (U64)Addr; */
    global_verif_hal.read(Addr,0 , &Value);
    /*esw_sync(1);*/
#endif


    /*printf("ReadRegister: read 0x%x at address 0x%x \n", *Pt, Addr);
      If we want to stop using pointer dereferencing...
      On systemC platform, force the current thread to be descheduled. This is
      necessary to do that if we want the streamer transfers to be done.
      esw_sync(0); */

    return Value;
}

#endif  /* #ifndef (HQVDPLITE_API_FOR_STAPI)*/

/* C++ support */
#ifdef __cplusplus
}
#endif

#endif  /* #ifndef _HQVDPLITE_STDDEFS_H */



