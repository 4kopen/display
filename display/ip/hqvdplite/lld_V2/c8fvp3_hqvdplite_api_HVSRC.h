/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_HVSRC_H
#define _C8FVP3_HQVDPLITE_API_HVSRC_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : HVSRC
*/


/**
* Register : HOR_PANORAMIC_CTRL
* Horizontal panoramic mode settings
*/

#define HVSRC_HOR_PANORAMIC_CTRL_OFFSET                 (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x00)
#define HVSRC_HOR_PANORAMIC_CTRL_MASK                   (0x0FFF3FFF)

/**
* Bit-field : CWIN
* it contains the width of linear scale zone in the input image, defined in 1/32th of luma pixel unit. It shall be ranged from 0 to (ImageWidthIn*32).
*/

#define HVSRC_HOR_PANORAMIC_CTRL_CWIN_SHIFT             (0x00000000)
#define HVSRC_HOR_PANORAMIC_CTRL_CWIN_WIDTH             (14)
#define HVSRC_HOR_PANORAMIC_CTRL_CWIN_MASK              (0x00003FFF)

/**
* Bit-field : CWOUT
* it contains the width of linear scale zone in the output image. It shall be ranged from 0 to ImageWidthOut excluded.
* If it is equal to 0, means that panoramic mode is off. Otherwise, it's on.
*/

#define HVSRC_HOR_PANORAMIC_CTRL_CWOUT_SHIFT            (0x00000010)
#define HVSRC_HOR_PANORAMIC_CTRL_CWOUT_WIDTH            (12)
#define HVSRC_HOR_PANORAMIC_CTRL_CWOUT_MASK             (0x0FFF0000)

/**
* Register : OUTPUT_PICTURE_SIZE
* Output picture size
*/

#define HVSRC_OUTPUT_PICTURE_SIZE_OFFSET                (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x04)
#define HVSRC_OUTPUT_PICTURE_SIZE_MASK                  (0x0FFF1FFF)

/**
* Bit-field : WIDTH
* it contains the width of the output picture, expressed in pixels. It shall be multiple of 2.
* The min values for this parameters must be equal to:
* Min output width: 48 pixels or 1/8 of input width, largest of the two values.
*/

#define HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_SHIFT           (0x00000000)
#define HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_WIDTH           (13)
#define HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_MASK            (0x00001FFF)

/**
* Bit-field : HEIGHT
* it contains the number of line that the HQR algorithm may outputs. It shall be multiple of 2.
* The min values for this parameters must be equal to:
* Min input height: 16 lines or 1/8 of HQR input height, largest of the two values.
*/

#define HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_SHIFT          (0x00000010)
#define HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_WIDTH          (12)
#define HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_MASK           (0x0FFF0000)

/**
* Register : INIT_HORIZONTAL
* Horizontal Initial phase control
*/

#define HVSRC_INIT_HORIZONTAL_OFFSET                    (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x08)
#define HVSRC_INIT_HORIZONTAL_MASK                      (0x003F001F)

/**
* Bit-field : LUMA_INIT_PHASE
* 1/32 signed luma initial phase in 0/31 range
*/

#define HVSRC_INIT_HORIZONTAL_LUMA_INIT_PHASE_SHIFT     (0x00000000)
#define HVSRC_INIT_HORIZONTAL_LUMA_INIT_PHASE_WIDTH     (5)
#define HVSRC_INIT_HORIZONTAL_LUMA_INIT_PHASE_MASK      (0x0000001F)

/**
* Bit-field : CHROMA_PHASE_SHIFT
* 1/32 signed phase shift from luma in -32/31 range
*/

#define HVSRC_INIT_HORIZONTAL_CHROMA_PHASE_SHIFT_SHIFT  (0x00000010)
#define HVSRC_INIT_HORIZONTAL_CHROMA_PHASE_SHIFT_WIDTH  (6)
#define HVSRC_INIT_HORIZONTAL_CHROMA_PHASE_SHIFT_MASK   (0x003F0000)

/**
* Register : INIT_VERTICAL
* Vertical Initial phase control
*/

#define HVSRC_INIT_VERTICAL_OFFSET                      (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x0C)
#define HVSRC_INIT_VERTICAL_MASK                        (0x003F001F)

/**
* Bit-field : LUMA_INIT_PHASE
* 1/32 signed luma initial phase in 0/31 range
*/

#define HVSRC_INIT_VERTICAL_LUMA_INIT_PHASE_SHIFT       (0x00000000)
#define HVSRC_INIT_VERTICAL_LUMA_INIT_PHASE_WIDTH       (5)
#define HVSRC_INIT_VERTICAL_LUMA_INIT_PHASE_MASK        (0x0000001F)

/**
* Bit-field : CHROMA_PHASE_SHIFT
* 1/32 signed phase shift from luma in -32/31 range. Must be 0 if input format is 444
*/

#define HVSRC_INIT_VERTICAL_CHROMA_PHASE_SHIFT_SHIFT    (0x00000010)
#define HVSRC_INIT_VERTICAL_CHROMA_PHASE_SHIFT_WIDTH    (6)
#define HVSRC_INIT_VERTICAL_CHROMA_PHASE_SHIFT_MASK     (0x003F0000)

/**
* Register : PARAM_CTRL
* Parameters control
*/

#define HVSRC_PARAM_CTRL_OFFSET                         (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x10)
#define HVSRC_PARAM_CTRL_MASK                           (0xF0F0F0FF)

/**
* Bit-field : H_ACON
* Adaptive control for both horizontal filters
*/

#define HVSRC_PARAM_CTRL_H_ACON_SHIFT                   (0x00000000)
#define HVSRC_PARAM_CTRL_H_ACON_WIDTH                   (1)
#define HVSRC_PARAM_CTRL_H_ACON_MASK                    (0x00000001)

/**
* Bit-field : H_AUTOKOVS
* When Set YH_KOVS and CH_KOVS are ignored
*/

#define HVSRC_PARAM_CTRL_H_AUTOKOVS_SHIFT               (0x00000001)
#define HVSRC_PARAM_CTRL_H_AUTOKOVS_WIDTH               (1)
#define HVSRC_PARAM_CTRL_H_AUTOKOVS_MASK                (0x00000002)

/**
* Bit-field : V_ACON
* Adaptive control for both vertical filters
*/

#define HVSRC_PARAM_CTRL_V_ACON_SHIFT                   (0x00000002)
#define HVSRC_PARAM_CTRL_V_ACON_WIDTH                   (1)
#define HVSRC_PARAM_CTRL_V_ACON_MASK                    (0x00000004)

/**
* Bit-field : V_AUTOKOVS
* When Set YV_KOVS and CV_KOVS are ignored
*/

#define HVSRC_PARAM_CTRL_V_AUTOKOVS_SHIFT               (0x00000003)
#define HVSRC_PARAM_CTRL_V_AUTOKOVS_WIDTH               (1)
#define HVSRC_PARAM_CTRL_V_AUTOKOVS_MASK                (0x00000008)

/**
* Bit-field : YH_KOVS
* overshoot control parameter for luma horizontal
*/

#define HVSRC_PARAM_CTRL_YH_KOVS_SHIFT                  (0x00000004)
#define HVSRC_PARAM_CTRL_YH_KOVS_WIDTH                  (4)
#define HVSRC_PARAM_CTRL_YH_KOVS_MASK                   (0x000000F0)

/**
* Bit-field : YV_KOVS
* overshoot control parameter for luma vertical
*/

#define HVSRC_PARAM_CTRL_YV_KOVS_SHIFT                  (0x0000000c)
#define HVSRC_PARAM_CTRL_YV_KOVS_WIDTH                  (4)
#define HVSRC_PARAM_CTRL_YV_KOVS_MASK                   (0x0000F000)

/**
* Bit-field : CH_KOVS
* overshoot control parameter for chroma horizontal
*/

#define HVSRC_PARAM_CTRL_CH_KOVS_SHIFT                  (0x00000014)
#define HVSRC_PARAM_CTRL_CH_KOVS_WIDTH                  (4)
#define HVSRC_PARAM_CTRL_CH_KOVS_MASK                   (0x00F00000)

/**
* Bit-field : CV_KOVS
* overshoot control parameter for chroma vertical
*/

#define HVSRC_PARAM_CTRL_CV_KOVS_SHIFT                  (0x0000001c)
#define HVSRC_PARAM_CTRL_CV_KOVS_WIDTH                  (4)
#define HVSRC_PARAM_CTRL_CV_KOVS_MASK                   (0xF0000000)

/**
* Register : YH_COEF
* Luma horizontal COEF x (x=0:127)
*/

#define HVSRC_YH_COEF_OFFSET                            (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x14)
#define HVSRC_YH_COEF_MASK                              (0x03FF03FF)

/**
* Bit-field : PHASE_EVEN
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_YH_COEF_PHASE_EVEN_SHIFT                  (0x00000000)
#define HVSRC_YH_COEF_PHASE_EVEN_WIDTH                  (10)
#define HVSRC_YH_COEF_PHASE_EVEN_MASK                   (0x000003FF)

/**
* Bit-field : PHASE_ODD
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_YH_COEF_PHASE_ODD_SHIFT                   (0x00000010)
#define HVSRC_YH_COEF_PHASE_ODD_WIDTH                   (10)
#define HVSRC_YH_COEF_PHASE_ODD_MASK                    (0x03FF0000)

/**
* Register : CH_COEF
* Chroma horizontal COEF x (x=0:127)
*/

#define HVSRC_CH_COEF_OFFSET                            (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x214)
#define HVSRC_CH_COEF_MASK                              (0x03FF03FF)

/**
* Bit-field : PHASE_EVEN
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_CH_COEF_PHASE_EVEN_SHIFT                  (0x00000000)
#define HVSRC_CH_COEF_PHASE_EVEN_WIDTH                  (10)
#define HVSRC_CH_COEF_PHASE_EVEN_MASK                   (0x000003FF)

/**
* Bit-field : PHASE_ODD
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_CH_COEF_PHASE_ODD_SHIFT                   (0x00000010)
#define HVSRC_CH_COEF_PHASE_ODD_WIDTH                   (10)
#define HVSRC_CH_COEF_PHASE_ODD_MASK                    (0x03FF0000)

/**
* Register : YV_COEF
* Luma vertical COEF x (x=0:127)
*/

#define HVSRC_YV_COEF_OFFSET                            (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x414)
#define HVSRC_YV_COEF_MASK                              (0x03FF03FF)

/**
* Bit-field : PHASE_EVEN
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_YV_COEF_PHASE_EVEN_SHIFT                  (0x00000000)
#define HVSRC_YV_COEF_PHASE_EVEN_WIDTH                  (10)
#define HVSRC_YV_COEF_PHASE_EVEN_MASK                   (0x000003FF)

/**
* Bit-field : PHASE_ODD
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_YV_COEF_PHASE_ODD_SHIFT                   (0x00000010)
#define HVSRC_YV_COEF_PHASE_ODD_WIDTH                   (10)
#define HVSRC_YV_COEF_PHASE_ODD_MASK                    (0x03FF0000)

/**
* Register : CV_COEF
* Chroma vertical COEF x (x=0:127)
*/

#define HVSRC_CV_COEF_OFFSET                            (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x614)
#define HVSRC_CV_COEF_MASK                              (0x03FF03FF)

/**
* Bit-field : PHASE_EVEN
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_CV_COEF_PHASE_EVEN_SHIFT                  (0x00000000)
#define HVSRC_CV_COEF_PHASE_EVEN_WIDTH                  (10)
#define HVSRC_CV_COEF_PHASE_EVEN_MASK                   (0x000003FF)

/**
* Bit-field : PHASE_ODD
* This look up table contain the coefficients of the filter.
*/

#define HVSRC_CV_COEF_PHASE_ODD_SHIFT                   (0x00000010)
#define HVSRC_CV_COEF_PHASE_ODD_WIDTH                   (10)
#define HVSRC_CV_COEF_PHASE_ODD_MASK                    (0x03FF0000)

/**
* Register : HORI_SHIFT
* Shift of horizontal coefs
*/

#define HVSRC_HORI_SHIFT_OFFSET                         (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x814)
#define HVSRC_HORI_SHIFT_MASK                           (0x000F000F)

/**
* Bit-field : Y
* shift of horizontal luma coef
*/

#define HVSRC_HORI_SHIFT_Y_SHIFT                        (0x00000000)
#define HVSRC_HORI_SHIFT_Y_WIDTH                        (4)
#define HVSRC_HORI_SHIFT_Y_MASK                         (0x0000000F)

/**
* Bit-field : C
* shift of horizontal chroma coef
*/

#define HVSRC_HORI_SHIFT_C_SHIFT                        (0x00000010)
#define HVSRC_HORI_SHIFT_C_WIDTH                        (4)
#define HVSRC_HORI_SHIFT_C_MASK                         (0x000F0000)

/**
* Register : VERT_SHIFT
* Shift of vertical coefs
*/

#define HVSRC_VERT_SHIFT_OFFSET                         (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR + 0x818)
#define HVSRC_VERT_SHIFT_MASK                           (0x000F000F)

/**
* Bit-field : Y
* shift of horizontal luma coef
*/

#define HVSRC_VERT_SHIFT_Y_SHIFT                        (0x00000000)
#define HVSRC_VERT_SHIFT_Y_WIDTH                        (4)
#define HVSRC_VERT_SHIFT_Y_MASK                         (0x0000000F)

/**
* Bit-field : C
* shift of horizontal chroma coef
*/

#define HVSRC_VERT_SHIFT_C_SHIFT                        (0x00000010)
#define HVSRC_VERT_SHIFT_C_WIDTH                        (4)
#define HVSRC_VERT_SHIFT_C_MASK                         (0x000F0000)


#ifndef HQVDPLITE_API_FOR_STAPI
typedef struct {
    gvh_u32_t HOR_PANORAMIC_CTRL;  /* at 0 */
    gvh_u32_t OUTPUT_PICTURE_SIZE;  /* at 4 */
    gvh_u32_t INIT_HORIZONTAL;  /* at 8 */
    gvh_u32_t INIT_VERTICAL;  /* at 12 */
    gvh_u32_t PARAM_CTRL;  /* at 16 */
    gvh_u32_t YH_COEF[128];  /* at 20 */
    gvh_u32_t CH_COEF[128];  /* at 532 */
    gvh_u32_t YV_COEF[128];  /* at 1044 */
    gvh_u32_t CV_COEF[128];  /* at 1556 */
    gvh_u32_t HORI_SHIFT;  /* at 2068 */
    gvh_u32_t VERT_SHIFT;  /* at 2072 */
} s_HVSRC;
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t HorPanoramicCtrl;  /* at 0 */
uint32_t OutputPictureSize;  /* at 4 */
uint32_t InitHorizontal;  /* at 8 */
uint32_t InitVertical;  /* at 12 */
uint32_t ParamCtrl;  /* at 16 */
uint32_t YhCoef[128];  /* at 20 */
uint32_t ChCoef[128];  /* at 532 */
uint32_t YvCoef[128];  /* at 1044 */
uint32_t CvCoef[128];  /* at 1556 */
uint32_t HoriShift;  /* at 2068 */
uint32_t VertShift;  /* at 2072 */
} HQVDPLITE_HVSRC_Params_t;
#endif /* FW_STXP70 */
#endif
