/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "hqvdp_ucode_256_rd.h"
uint32_t HQVDP_UcodeRdPlug[HQVDP_UCODERDPLUG_SIZE] = {
0x600060,
0x600140,
0x600380,
0x005a10,
0x000120,
0x000101,
0x000002,
0x000003,
0xe00001,
0x400010,
0x0b0b11,
0x000260,
0x000201,
0x000c62,
0x000003,
0x000230,
0x200009,
0x800700,
0x803710,
0x203605,
0x400011,
0x201606,
0x400b00,
0x201007,
0x5ff300,
0x201008,
0x5fef00,
0x401000,
0x058711,
0x056712,
0x0005c0,
0x000461,
0x000ae2,
0x0000e3,
0x000550,
0x200008,
0x600500,
0x20040d,
0x6005a0,
0x600580,
0x20040d,
0x600560,
0x800300,
0x800b00,
0x800300,
0x802b10,
0x202a0a,
0x6006e0,
0x200a0b,
0x401700,
0x200009,
0x400800,
0x20010c,
0x5ff700,
0x5ffb00,
0x201008,
0x400011,
0x400012,
0x000044,
0x003705,
0x001706,
0x001107,
0x001008,
0x000809,
0x002b0a,
0x000b0b,
0x00010c,
0x00040d,
0x4fffe0,
0xffffff,
0xffffff,
0xffffff,
0xffffff,
0xffffff,
0xffffff,
0xffffff,
0xffffff,
0xffffff,
0xffffff,
0x600740};

