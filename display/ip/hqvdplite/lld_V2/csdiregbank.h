/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */


#ifndef _CSDIREGBANK_H_
#define _CSDIREGBANK_H_

/**
* Component section
* Purpose : defines macros for component
*           specific informations
*/

#define CSDIregBank_VENDOR                              ("st.com")
#define CSDIregBank_LIBRARY                             ("hqvdp")
#define CSDIregBank_NAME                                ("CSDIregBank")
#define CSDIregBank_VERSION                             ("1.0")

/**
* Address Block : csdi_pu_reg
*/

#define CSDIregBank_csdi_pu_reg_BASE_ADDR               (0x0)

/**
* Register : CSDI_CONFIG
* CSDi Config
*/

#define CSDIregBank_CSDI_CONFIG_SIZE                    (32)
#define CSDIregBank_CSDI_CONFIG_OFFSET                  (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x00)
#define CSDIregBank_CSDI_CONFIG_RESET_VALUE             (0x00EA48C2)
#define CSDIregBank_CSDI_CONFIG_BITFIELD_MASK           (0x03FFF9C3)
#define CSDIregBank_CSDI_CONFIG_RWMASK                  (0x03FFF9C3)
#define CSDIregBank_CSDI_CONFIG_ROMASK                  (0x00000000)
#define CSDIregBank_CSDI_CONFIG_WOMASK                  (0x00000000)
#define CSDIregBank_CSDI_CONFIG_UNUSED_MASK             (0xFC00063C)

/**
* Bit-field : MAIN_MODE
* Selects a group of configurations for the deinterlacer
*/

#define CSDIregBank_CSDI_CONFIG_MAIN_MODE_OFFSET        (0x00000000)
#define CSDIregBank_CSDI_CONFIG_MAIN_MODE_WIDTH         (2)
#define CSDIregBank_CSDI_CONFIG_MAIN_MODE_MASK          (0x00000003)

/**
* Value : B_0x0
* bypass
*/

#define CSDIregBank_CSDI_CONFIG_MAIN_MODE_B_0x0         (0x00000000)

/**
* Value : B_0x1
* field merging
*/

#define CSDIregBank_CSDI_CONFIG_MAIN_MODE_B_0x1         (0x00000001)

/**
* Value : B_0x2
* deinterlacement (default)
*/

#define CSDIregBank_CSDI_CONFIG_MAIN_MODE_B_0x2         (0x00000002)

/**
* Value : B_0x3
* LFM (not implemented yet)
*/

#define CSDIregBank_CSDI_CONFIG_MAIN_MODE_B_0x3         (0x00000003)

/**
* Bit-field : MD_MODE
* Defines the processing of motion.
* Init mode and low conf mode allow easy initialisation of the motion feedback loop at the beginning of a video sequence.
* Low conf mode is also intended to be a "standard" mode of motion detection.
* Used only when MAIN_MODE is "1x" (deinterlacement or LFM).
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_OFFSET          (0x00000006)
#define CSDIregBank_CSDI_CONFIG_MD_MODE_WIDTH           (3)
#define CSDIregBank_CSDI_CONFIG_MD_MODE_MASK            (0x000001C0)

/**
* Value : B_0x0
* off, no motion produced
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x0           (0x00000000)

/**
* Value : B_0x1
* init mode, based on previous and next luma fields
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x1           (0x00000001)

/**
* Value : B_0x2
* low conf mode, based on previous and next luma and current motion fields
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x2           (0x00000002)

/**
* Value : B_0x3
* full conf mode, based on previous and next luma and current and previous motion fields (default)
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x3           (0x00000003)

/**
* Value : B_0x4
* short conf no recursive mode, the same as the full conf mode, but without recursivity.
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x4           (0x00000004)

/**
* Value : B_0x5
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x5           (0x00000005)

/**
* Value : B_0x6
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x6           (0x00000006)

/**
* Value : B_0x7
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_MD_MODE_B_0x7           (0x00000007)

/**
* Bit-field : ENABLE_RECURSIVE_DIR
* Defines the use of the direction change limitation
* Used only when MAIN_MODE is "1x" (deinterlacement or LFM).
*/

#define CSDIregBank_CSDI_CONFIG_ENABLE_RECURSIVE_DIR_OFFSET (0x0000000b)
#define CSDIregBank_CSDI_CONFIG_ENABLE_RECURSIVE_DIR_WIDTH (1)
#define CSDIregBank_CSDI_CONFIG_ENABLE_RECURSIVE_DIR_MASK (0x00000800)

/**
* Value : B_0x0
* recursivity disable, no impact of the previous direction to the current one
*/

#define CSDIregBank_CSDI_CONFIG_ENABLE_RECURSIVE_DIR_B_0x0 (0x00000000)

/**
* Value : B_0x1
* recursivity enable, current direction depends recursively on the previous one (default)
*/

#define CSDIregBank_CSDI_CONFIG_ENABLE_RECURSIVE_DIR_B_0x1 (0x00000001)

/**
* Bit-field : KCOR
* Correction factor used in the fader.
* Unsigned fractional value, ranging from 0 (0.5) to 15 (2.375) with a step of 0.125, default is 4.
* Used only when MAIN_MODE is "1x" (deinterlacement or LFM ).
*/

#define CSDIregBank_CSDI_CONFIG_KCOR_OFFSET             (0x0000000c)
#define CSDIregBank_CSDI_CONFIG_KCOR_WIDTH              (4)
#define CSDIregBank_CSDI_CONFIG_KCOR_MASK               (0x0000F000)

/**
* Bit-field : T_DETAIL
* Threshold for detail used in the fader.
* Unsigned value ranging from 0 to 15, default is 10.
* Used only when MAIN_MODE is "1x" (deinterlacement or LFM)
*/

#define CSDIregBank_CSDI_CONFIG_T_DETAIL_OFFSET         (0x00000010)
#define CSDIregBank_CSDI_CONFIG_T_DETAIL_WIDTH          (4)
#define CSDIregBank_CSDI_CONFIG_T_DETAIL_MASK           (0x000F0000)

/**
* Bit-field : KMOV
* Defines the motion factor.
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_OFFSET             (0x00000014)
#define CSDIregBank_CSDI_CONFIG_KMOV_WIDTH              (4)
#define CSDIregBank_CSDI_CONFIG_KMOV_MASK               (0x00F00000)

/**
* Value : B_0x0
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x0              (0x00000000)

/**
* Value : B_0x1
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x1              (0x00000001)

/**
* Value : B_0x2
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x2              (0x00000002)

/**
* Value : B_0x3
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x3              (0x00000003)

/**
* Value : B_0x4
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x4              (0x00000004)

/**
* Value : B_0x5
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x5              (0x00000005)

/**
* Value : B_0x6
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x6              (0x00000006)

/**
* Value : B_0x7
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x7              (0x00000007)

/**
* Value : B_0x8
* 8/16
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x8              (0x00000008)

/**
* Value : B_0x9
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0x9              (0x00000009)

/**
* Value : B_0xA
* 10/16
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0xA              (0x0000000A)

/**
* Value : B_0xB
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0xB              (0x0000000B)

/**
* Value : B_0xC
* 12/16
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0xC              (0x0000000C)

/**
* Value : B_0xD
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0xD              (0x0000000D)

/**
* Value : B_0xE
* 14/16 (default)
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0xE              (0x0000000E)

/**
* Value : B_0xF
* invalid
*/

#define CSDIregBank_CSDI_CONFIG_KMOV_B_0xF              (0x0000000F)

/**
* Bit-field : MOTION_REDUCED
* Motion on 4 bits enable
*/

#define CSDIregBank_CSDI_CONFIG_MOTION_REDUCED_OFFSET   (0x00000018)
#define CSDIregBank_CSDI_CONFIG_MOTION_REDUCED_WIDTH    (1)
#define CSDIregBank_CSDI_CONFIG_MOTION_REDUCED_MASK     (0x01000000)

/**
* Value : B_0x0
* The DEI is working with a 8 bits width motion (default)
*/

#define CSDIregBank_CSDI_CONFIG_MOTION_REDUCED_B_0x0    (0x00000000)

/**
* Value : B_0x1
* The DEI is working with a 4 bits width motion
*/

#define CSDIregBank_CSDI_CONFIG_MOTION_REDUCED_B_0x1    (0x00000001)

/**
* Bit-field : TOPNOTBOTTOM
* Defines the parity of the field.
*/

#define CSDIregBank_CSDI_CONFIG_TOPNOTBOTTOM_OFFSET     (0x00000019)
#define CSDIregBank_CSDI_CONFIG_TOPNOTBOTTOM_WIDTH      (1)
#define CSDIregBank_CSDI_CONFIG_TOPNOTBOTTOM_MASK       (0x02000000)

/**
* Value : B_0x0
* Bottom
*/

#define CSDIregBank_CSDI_CONFIG_TOPNOTBOTTOM_B_0x0      (0x00000000)

/**
* Value : B_0x1
* Top
*/

#define CSDIregBank_CSDI_CONFIG_TOPNOTBOTTOM_B_0x1      (0x00000001)

/**
* Register : CSDI_CONFIG2
* CSDi Config2 (new features)
*/

#define CSDIregBank_CSDI_CONFIG2_SIZE                   (32)
#define CSDIregBank_CSDI_CONFIG2_OFFSET                 (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x04)
#define CSDIregBank_CSDI_CONFIG2_RESET_VALUE            (0x00000000)
#define CSDIregBank_CSDI_CONFIG2_BITFIELD_MASK          (0x000001F7)
#define CSDIregBank_CSDI_CONFIG2_RWMASK                 (0x000001F7)
#define CSDIregBank_CSDI_CONFIG2_ROMASK                 (0x00000000)
#define CSDIregBank_CSDI_CONFIG2_WOMASK                 (0x00000000)
#define CSDIregBank_CSDI_CONFIG2_UNUSED_MASK            (0xFFFFFE08)

/**
* Bit-field : ADAPTIVE_FADING
* enable adaptive fading for luma in fading
*/

#define CSDIregBank_CSDI_CONFIG2_ADAPTIVE_FADING_OFFSET (0x00000000)
#define CSDIregBank_CSDI_CONFIG2_ADAPTIVE_FADING_WIDTH  (1)
#define CSDIregBank_CSDI_CONFIG2_ADAPTIVE_FADING_MASK   (0x00000001)

/**
* Value : B_0x0
* disable (default)
*/

#define CSDIregBank_CSDI_CONFIG2_ADAPTIVE_FADING_B_0x0  (0x00000000)

/**
* Value : B_0x1
* enable
*/

#define CSDIregBank_CSDI_CONFIG2_ADAPTIVE_FADING_B_0x1  (0x00000001)

/**
* Bit-field : CHROMA_MOTION
* enable specific motion for chroma in fading
*/

#define CSDIregBank_CSDI_CONFIG2_CHROMA_MOTION_OFFSET   (0x00000001)
#define CSDIregBank_CSDI_CONFIG2_CHROMA_MOTION_WIDTH    (1)
#define CSDIregBank_CSDI_CONFIG2_CHROMA_MOTION_MASK     (0x00000002)

/**
* Value : B_0x0
* disable (default)
*/

#define CSDIregBank_CSDI_CONFIG2_CHROMA_MOTION_B_0x0    (0x00000000)

/**
* Value : B_0x1
* enable
*/

#define CSDIregBank_CSDI_CONFIG2_CHROMA_MOTION_B_0x1    (0x00000001)

/**
* Bit-field : DETAILS
* choose the detail to be used in fading
*/

#define CSDIregBank_CSDI_CONFIG2_DETAILS_OFFSET         (0x00000002)
#define CSDIregBank_CSDI_CONFIG2_DETAILS_WIDTH          (1)
#define CSDIregBank_CSDI_CONFIG2_DETAILS_MASK           (0x00000004)

/**
* Value : B_0x0
* detail from DCDi (default)
*/

#define CSDIregBank_CSDI_CONFIG2_DETAILS_B_0x0          (0x00000000)

/**
* Value : B_0x1
* detail from CSDi diag7
*/

#define CSDIregBank_CSDI_CONFIG2_DETAILS_B_0x1          (0x00000001)

/**
* Bit-field : MOTION_CLOSING
* enable to filter the motion (max and min)
*/

#define CSDIregBank_CSDI_CONFIG2_MOTION_CLOSING_OFFSET  (0x00000004)
#define CSDIregBank_CSDI_CONFIG2_MOTION_CLOSING_WIDTH   (1)
#define CSDIregBank_CSDI_CONFIG2_MOTION_CLOSING_MASK    (0x00000010)

/**
* Value : B_0x0
* disable (default)
*/

#define CSDIregBank_CSDI_CONFIG2_MOTION_CLOSING_B_0x0   (0x00000000)

/**
* Value : B_0x1
* enable
*/

#define CSDIregBank_CSDI_CONFIG2_MOTION_CLOSING_B_0x1   (0x00000001)

/**
* Bit-field : MOTION_FIR
* enable the FIR on motion
*/

#define CSDIregBank_CSDI_CONFIG2_MOTION_FIR_OFFSET      (0x00000005)
#define CSDIregBank_CSDI_CONFIG2_MOTION_FIR_WIDTH       (1)
#define CSDIregBank_CSDI_CONFIG2_MOTION_FIR_MASK        (0x00000020)

/**
* Value : B_0x0
* disable (default)
*/

#define CSDIregBank_CSDI_CONFIG2_MOTION_FIR_B_0x0       (0x00000000)

/**
* Value : B_0x1
* enable
*/

#define CSDIregBank_CSDI_CONFIG2_MOTION_FIR_B_0x1       (0x00000001)

/**
* Bit-field : FIR_LENGTH
* size of the FIR on motion
*/

#define CSDIregBank_CSDI_CONFIG2_FIR_LENGTH_OFFSET      (0x00000006)
#define CSDIregBank_CSDI_CONFIG2_FIR_LENGTH_WIDTH       (2)
#define CSDIregBank_CSDI_CONFIG2_FIR_LENGTH_MASK        (0x000000C0)

/**
* Value : B_0x0
* 5-tap (default)
*/

#define CSDIregBank_CSDI_CONFIG2_FIR_LENGTH_B_0x0       (0x00000000)

/**
* Value : B_0x1
* 7-tap
*/

#define CSDIregBank_CSDI_CONFIG2_FIR_LENGTH_B_0x1       (0x00000001)

/**
* Value : B_0x2
* 9-tap
*/

#define CSDIregBank_CSDI_CONFIG2_FIR_LENGTH_B_0x2       (0x00000002)

/**
* Value : B_0x3
* invalid
*/

#define CSDIregBank_CSDI_CONFIG2_FIR_LENGTH_B_0x3       (0x00000003)

/**
* Bit-field : INFLEXION_MOTION
* enable the inflexion recursivity on motion
*/

#define CSDIregBank_CSDI_CONFIG2_INFLEXION_MOTION_OFFSET (0x00000008)
#define CSDIregBank_CSDI_CONFIG2_INFLEXION_MOTION_WIDTH (1)
#define CSDIregBank_CSDI_CONFIG2_INFLEXION_MOTION_MASK  (0x00000100)

/**
* Value : B_0x0
* disable (default)
*/

#define CSDIregBank_CSDI_CONFIG2_INFLEXION_MOTION_B_0x0 (0x00000000)

/**
* Value : B_0x1
* enable
*/

#define CSDIregBank_CSDI_CONFIG2_INFLEXION_MOTION_B_0x1 (0x00000001)

/**
* Register : DCDI_CONFIG
* DCDi Config
*/

#define CSDIregBank_DCDI_CONFIG_SIZE                    (32)
#define CSDIregBank_DCDI_CONFIG_OFFSET                  (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x08)
#define CSDIregBank_DCDI_CONFIG_RESET_VALUE             (0x00203803)
#define CSDIregBank_DCDI_CONFIG_BITFIELD_MASK           (0x007FFF03)
#define CSDIregBank_DCDI_CONFIG_RWMASK                  (0x007FFF03)
#define CSDIregBank_DCDI_CONFIG_ROMASK                  (0x00000000)
#define CSDIregBank_DCDI_CONFIG_WOMASK                  (0x00000000)
#define CSDIregBank_DCDI_CONFIG_UNUSED_MASK             (0xFF8000FC)

/**
* Bit-field : ALPHA_MEDIAN_EN
* enable the alpha median filter
*/

#define CSDIregBank_DCDI_CONFIG_ALPHA_MEDIAN_EN_OFFSET  (0x00000000)
#define CSDIregBank_DCDI_CONFIG_ALPHA_MEDIAN_EN_WIDTH   (1)
#define CSDIregBank_DCDI_CONFIG_ALPHA_MEDIAN_EN_MASK    (0x00000001)

/**
* Value : B_0x0
* disable
*/

#define CSDIregBank_DCDI_CONFIG_ALPHA_MEDIAN_EN_B_0x0   (0x00000000)

/**
* Value : B_0x1
* enable (default)
*/

#define CSDIregBank_DCDI_CONFIG_ALPHA_MEDIAN_EN_B_0x1   (0x00000001)

/**
* Bit-field : MEDIAN_BLEND_ANGLE_DEP
* enable median blender dependency on angle
*/

#define CSDIregBank_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_OFFSET (0x00000001)
#define CSDIregBank_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_WIDTH (1)
#define CSDIregBank_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_MASK (0x00000002)

/**
* Value : B_0x0
* disable
*/

#define CSDIregBank_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_B_0x0 (0x00000000)

/**
* Value : B_0x1
* enable (default)
*/

#define CSDIregBank_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_B_0x1 (0x00000001)

/**
* Bit-field : WIN_CLAMP_SIZE
* interpolation window clamp size, ranging from 0 to 15, default is 8
*/

#define CSDIregBank_DCDI_CONFIG_WIN_CLAMP_SIZE_OFFSET   (0x00000008)
#define CSDIregBank_DCDI_CONFIG_WIN_CLAMP_SIZE_WIDTH    (4)
#define CSDIregBank_DCDI_CONFIG_WIN_CLAMP_SIZE_MASK     (0x00000F00)

/**
* Bit-field : MEDIAN_MODE
* median filter mode ranging from -1 to 4, default is 3
*/

#define CSDIregBank_DCDI_CONFIG_MEDIAN_MODE_OFFSET      (0x0000000c)
#define CSDIregBank_DCDI_CONFIG_MEDIAN_MODE_WIDTH       (4)
#define CSDIregBank_DCDI_CONFIG_MEDIAN_MODE_MASK        (0x0000F000)

/**
* Bit-field : ALPHA_LIMIT
* Alpha limit ranging from 0 to 127, default is 32
*/

#define CSDIregBank_DCDI_CONFIG_ALPHA_LIMIT_OFFSET      (0x00000010)
#define CSDIregBank_DCDI_CONFIG_ALPHA_LIMIT_WIDTH       (7)
#define CSDIregBank_DCDI_CONFIG_ALPHA_LIMIT_MASK        (0x007F0000)

/**
* Register : CSDI_WIDTH
* DEI Width
*/

#define CSDIregBank_CSDI_WIDTH_SIZE                     (32)
#define CSDIregBank_CSDI_WIDTH_OFFSET                   (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x0C)
#define CSDIregBank_CSDI_WIDTH_RESET_VALUE              (0x00000000)
#define CSDIregBank_CSDI_WIDTH_BITFIELD_MASK            (0x000007FF)
#define CSDIregBank_CSDI_WIDTH_RWMASK                   (0x000007FF)
#define CSDIregBank_CSDI_WIDTH_ROMASK                   (0x00000000)
#define CSDIregBank_CSDI_WIDTH_WOMASK                   (0x00000000)
#define CSDIregBank_CSDI_WIDTH_UNUSED_MASK              (0xFFFFF800)

/**
* Bit-field : WIDTH
* define the line width.
*/

#define CSDIregBank_CSDI_WIDTH_WIDTH_OFFSET             (0x00000000)
#define CSDIregBank_CSDI_WIDTH_WIDTH_WIDTH              (11)
#define CSDIregBank_CSDI_WIDTH_WIDTH_MASK               (0x000007FF)

/**
* Register : CSDI_WIDTH_MOTION
* DEI Width Motion
*/

#define CSDIregBank_CSDI_WIDTH_MOTION_SIZE              (32)
#define CSDIregBank_CSDI_WIDTH_MOTION_OFFSET            (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x10)
#define CSDIregBank_CSDI_WIDTH_MOTION_RESET_VALUE       (0x00000000)
#define CSDIregBank_CSDI_WIDTH_MOTION_BITFIELD_MASK     (0x000007FF)
#define CSDIregBank_CSDI_WIDTH_MOTION_RWMASK            (0x000007FF)
#define CSDIregBank_CSDI_WIDTH_MOTION_ROMASK            (0x00000000)
#define CSDIregBank_CSDI_WIDTH_MOTION_WOMASK            (0x00000000)
#define CSDIregBank_CSDI_WIDTH_MOTION_UNUSED_MASK       (0xFFFFF800)

/**
* Bit-field : WIDTH_MOTION
* define the line width of motion.
*/

#define CSDIregBank_CSDI_WIDTH_MOTION_WIDTH_MOTION_OFFSET (0x00000000)
#define CSDIregBank_CSDI_WIDTH_MOTION_WIDTH_MOTION_WIDTH (11)
#define CSDIregBank_CSDI_WIDTH_MOTION_WIDTH_MOTION_MASK (0x000007FF)

/**
* Register : CSDI_MUX
* DEI Mux
*/

#define CSDIregBank_CSDI_MUX_SIZE                       (32)
#define CSDIregBank_CSDI_MUX_OFFSET                     (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x14)
#define CSDIregBank_CSDI_MUX_RESET_VALUE                (0x00000000)
#define CSDIregBank_CSDI_MUX_BITFIELD_MASK              (0x0000003F)
#define CSDIregBank_CSDI_MUX_RWMASK                     (0x0000003F)
#define CSDIregBank_CSDI_MUX_ROMASK                     (0x00000000)
#define CSDIregBank_CSDI_MUX_WOMASK                     (0x00000000)
#define CSDIregBank_CSDI_MUX_UNUSED_MASK                (0xFFFFFFC0)

/**
* Bit-field : MUX_MODE_LUMA
* Selects luma output mux
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_OFFSET       (0x00000000)
#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_WIDTH        (3)
#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_MASK         (0x00000007)

/**
* Value : B_0x0
* Bypass
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_B_0x0        (0x00000000)

/**
* Value : B_0x1
* Field merging
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_B_0x1        (0x00000001)

/**
* Value : B_0x2
* Spatial
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_B_0x2        (0x00000002)

/**
* Value : B_0x3
* Median
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_B_0x3        (0x00000003)

/**
* Value : B_0x4
* 3D
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_LUMA_B_0x4        (0x00000004)

/**
* Bit-field : MUX_MODE_CHROMA
* Selects chroma output mux
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_OFFSET     (0x00000003)
#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_WIDTH      (3)
#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_MASK       (0x00000038)

/**
* Value : B_0x0
* Bypass
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_B_0x0      (0x00000000)

/**
* Value : B_0x1
* Field merging
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_B_0x1      (0x00000001)

/**
* Value : B_0x2
* Spatial
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_B_0x2      (0x00000002)

/**
* Value : B_0x3
* Median
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_B_0x3      (0x00000003)

/**
* Value : B_0x4
* 3D
*/

#define CSDIregBank_CSDI_MUX_MUX_MODE_CHROMA_B_0x4      (0x00000004)

/**
* Register : CSDI_DL_INIT
* Delay line Init bits
*/

#define CSDIregBank_CSDI_DL_INIT_SIZE                   (32)
#define CSDIregBank_CSDI_DL_INIT_OFFSET                 (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x18)
#define CSDIregBank_CSDI_DL_INIT_RESET_VALUE            (0x00000000)
#define CSDIregBank_CSDI_DL_INIT_BITFIELD_MASK          (0x00000007)
#define CSDIregBank_CSDI_DL_INIT_RWMASK                 (0x00000007)
#define CSDIregBank_CSDI_DL_INIT_ROMASK                 (0x00000000)
#define CSDIregBank_CSDI_DL_INIT_WOMASK                 (0x00000000)
#define CSDIregBank_CSDI_DL_INIT_UNUSED_MASK            (0xFFFFFFF8)

/**
* Bit-field : CYFIN
*/

#define CSDIregBank_CSDI_DL_INIT_CYFIN_OFFSET           (0x00000000)
#define CSDIregBank_CSDI_DL_INIT_CYFIN_WIDTH            (1)
#define CSDIregBank_CSDI_DL_INIT_CYFIN_MASK             (0x00000001)

/**
* Bit-field : CCFIN
*/

#define CSDIregBank_CSDI_DL_INIT_CCFIN_OFFSET           (0x00000001)
#define CSDIregBank_CSDI_DL_INIT_CCFIN_WIDTH            (1)
#define CSDIregBank_CSDI_DL_INIT_CCFIN_MASK             (0x00000002)

/**
* Bit-field : CMFIN
*/

#define CSDIregBank_CSDI_DL_INIT_CMFIN_OFFSET           (0x00000002)
#define CSDIregBank_CSDI_DL_INIT_CMFIN_WIDTH            (1)
#define CSDIregBank_CSDI_DL_INIT_CMFIN_MASK             (0x00000004)

/**
* Register : CSDI_DL_FLUSH
* Delay line Flush bits
*/

#define CSDIregBank_CSDI_DL_FLUSH_SIZE                  (32)
#define CSDIregBank_CSDI_DL_FLUSH_OFFSET                (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x1C)
#define CSDIregBank_CSDI_DL_FLUSH_RESET_VALUE           (0x00000000)
#define CSDIregBank_CSDI_DL_FLUSH_BITFIELD_MASK         (0x00000007)
#define CSDIregBank_CSDI_DL_FLUSH_RWMASK                (0x00000007)
#define CSDIregBank_CSDI_DL_FLUSH_ROMASK                (0x00000000)
#define CSDIregBank_CSDI_DL_FLUSH_WOMASK                (0x00000000)
#define CSDIregBank_CSDI_DL_FLUSH_UNUSED_MASK           (0xFFFFFFF8)

/**
* Bit-field : CYFIN
*/

#define CSDIregBank_CSDI_DL_FLUSH_CYFIN_OFFSET          (0x00000000)
#define CSDIregBank_CSDI_DL_FLUSH_CYFIN_WIDTH           (1)
#define CSDIregBank_CSDI_DL_FLUSH_CYFIN_MASK            (0x00000001)

/**
* Bit-field : CCFIN
*/

#define CSDIregBank_CSDI_DL_FLUSH_CCFIN_OFFSET          (0x00000001)
#define CSDIregBank_CSDI_DL_FLUSH_CCFIN_WIDTH           (1)
#define CSDIregBank_CSDI_DL_FLUSH_CCFIN_MASK            (0x00000002)

/**
* Bit-field : CMFIN
*/

#define CSDIregBank_CSDI_DL_FLUSH_CMFIN_OFFSET          (0x00000002)
#define CSDIregBank_CSDI_DL_FLUSH_CMFIN_WIDTH           (1)
#define CSDIregBank_CSDI_DL_FLUSH_CMFIN_MASK            (0x00000004)

/**
* Register : CSDI_DL_SKIP
* Delay line Skip bits
*/

#define CSDIregBank_CSDI_DL_SKIP_SIZE                   (32)
#define CSDIregBank_CSDI_DL_SKIP_OFFSET                 (CSDIregBank_csdi_pu_reg_BASE_ADDR + 0x20)
#define CSDIregBank_CSDI_DL_SKIP_RESET_VALUE            (0x00000000)
#define CSDIregBank_CSDI_DL_SKIP_BITFIELD_MASK          (0x00000007)
#define CSDIregBank_CSDI_DL_SKIP_RWMASK                 (0x00000007)
#define CSDIregBank_CSDI_DL_SKIP_ROMASK                 (0x00000000)
#define CSDIregBank_CSDI_DL_SKIP_WOMASK                 (0x00000000)
#define CSDIregBank_CSDI_DL_SKIP_UNUSED_MASK            (0xFFFFFFF8)

/**
* Bit-field : CYFIN
*/

#define CSDIregBank_CSDI_DL_SKIP_CYFIN_OFFSET           (0x00000000)
#define CSDIregBank_CSDI_DL_SKIP_CYFIN_WIDTH            (1)
#define CSDIregBank_CSDI_DL_SKIP_CYFIN_MASK             (0x00000001)

/**
* Bit-field : CCFIN
*/

#define CSDIregBank_CSDI_DL_SKIP_CCFIN_OFFSET           (0x00000001)
#define CSDIregBank_CSDI_DL_SKIP_CCFIN_WIDTH            (1)
#define CSDIregBank_CSDI_DL_SKIP_CCFIN_MASK             (0x00000002)

/**
* Bit-field : CMFIN
*/

#define CSDIregBank_CSDI_DL_SKIP_CMFIN_OFFSET           (0x00000002)
#define CSDIregBank_CSDI_DL_SKIP_CMFIN_WIDTH            (1)
#define CSDIregBank_CSDI_DL_SKIP_CMFIN_MASK             (0x00000004)

#endif /** _CSDIREGBANK_H_ */
