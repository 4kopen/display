/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _HQVDPILTE_IT_MAPPING_H_
#define _HQVDPILTE_IT_MAPPING_H_

/***************************************
 * STXP70 INTERRUPTS MAPPING
***************************************/
#define HQVDP_LITE_IT_FROM_COMBINER_NUMBER        (0)
#define HQVDP_LITE_IT_FROM_HQR_CLUSTER_NUMBER     (1)
#define HQVDP_LITE_IT_IQI_END_PROC_NUMBER         (2)
#define HQVDP_LITE_IT_IQI_FIFO_EMPTY_NUMBER       (3)
#define HQVDP_LITE_XP70_IT_FROM_HOST_NUMBER       (4)
#define HQVDP_LITE_XP70_VSYNC_IT_NUMBER           (5)
#define HQVDP_LITE_IT_FROM_STR0_ERROR_NUMBER      (15) // connected to NMI

#endif /*_HQVDPILTE_IT_MAPPING_H_  */
