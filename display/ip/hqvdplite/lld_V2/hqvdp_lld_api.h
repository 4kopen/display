/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

 /**@file hqvdp_lld_api.h
    @brief api file for hqvdp low level driver

    This files exposes functions (@ref lld_function), structures
    (@ref lld_struct) and enums (@ref lld_enum) of the lld API.


 */

#ifndef __HQVDP_LLD_API__
#define __HQVDP_LLD_API__
#include "hqvdp_lld_platform.h" /* basic types definition per target platform */

/* firmware binary symbols definition */
#include "hqvdplite_dmem.h"
#include "hqvdplite_pmem.h"

/* types for the XP70 command  */
#include "c8fvp3_hqvdplite_api_struct.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/** @brief maximum number of hardware resources which can be used by one plane
 *  @ingroup lld_defines
 */
#define HQVDP_LLD_MAX_HW_FOR_ONE_PLANE 2

/*----------------------------------------------------------------------------*/
/** @defgroup lld_enum LLD API exported enums */
/*----------------------------------------------------------------------------*/


/** @brief error values for LLD function calls return
    @ingroup lld_enum

*/
enum hqvdp_lld_error {
    HQVDP_LLD_NO_ERR = 0, /**< @brief No error */
    HQVDP_LLD_ERR_INVALID_PARAM,           /**< @brief invalid parameters passed during the function call. */
    HQVDP_LLD_ERR_BOOT_FAILED,             /**< @brief XP70 firmware booting failed */
    HQVDP_LLD_ERR_NO_PROFILE,              /**< @brief profile requested is not supported or already open*/
    HQVDP_LLD_ERROR_NO_STOP_DONE,          /**< @brief precondition hqvdp_lld_stop has not be done before */
    HQVDP_LLD_ERR_INIT_NOT_DONE,           /**< @brief Init has not been done when required */
    HQVDP_LLD_ERR_NO_RESSOURCE_AVAILABLE,  /**< @brief no hardware resource available */
    HQVDP_LLD_ERR_UNDEFINED,               /**< @brief Other type of error */

};

/** @brief profile used for initializing the HQVDP lld
    @ingroup lld_enum

    - The profile information is used at open time (#hqvdp_lld_open) so that the lld
    can report its memory needs in terms of storage for motion buffer and
    xp70 commands as well as reserve the required HW resources.
*/
enum hqvdp_lld_profile_e {
    HQVDP_PROFILE_FULL_HD,   /**< 1920x1080*/
    HQVDP_PROFILE_4KP30,     /**< 3840x2160 30 fps (requires 1 HQVDP HW)*/
    HQVDP_PROFILE_4KP60,     /**< 3840x2160 60 fps  (requires 2 HQVDP HW)*/
    HQVDP_PROFILE_4KP60_PR,  /**< 3840x2160 60 fps  (requires 1 HQVDP HW with pixel repeat)*/
};


/** @brief Define HQVDP start processing trigger event
    @ingroup lld_enum
*/
enum hqvdp_lld_vsync_mode_e {
    HQVDP_HW_VSYNC,            /**< External hard VSYNC */
    HQVDP_CMD_PENDING_ONGOING, /**< Internal soft VSYNC based on CMD_PENDING/CMD_ONGOING */
    HQVDP_SOFT_VSYNC           /**< Internal soft VSYNC based on SW_VSYNC_CTRL/IRQ_TO_HOST */
};

/** @brief luma filter set strength
    @ingroup lld_enum

 */
enum hvsrc_luma_strength_e {
    HQVDP_HVSRC_LUT_Y_LEGACY = 0,
    HQVDP_HVSRC_LUT_Y_SMOOTH = 1,
    HQVDP_HVSRC_LUT_Y_MEDIUM = 2,
    HQVDP_HVSRC_LUT_Y_SHARP  = 3,
};


/** @brief chroma filter set strength
    @ingroup lld_enum                  */
enum hvsrc_chroma_strength_e {
    HQVDP_HVSRC_LUT_C_LEGACY = 0,
    HQVDP_HVSRC_LUT_C_OPT    = 1,
};


/** @brief Firmware loading mode
    @ingroup lld_enum               */
enum hqvdp_lld_fw_load_e {
    FW_LOAD_STREAMER = 0,   /**< @brief Load firmware with T3 interface
                                 @warning #FW_LOAD_STREAMER requires the FW binaries to be placed in physical
                                 memory. */
    FW_LOAD_T1       = 1,   /**< @brief Load firmware using T1 access ie writing to XP70 TCM from ARM core. */
};



/*----------------------------------------------------------------------------*/
/** @defgroup lld_struct LLD API exported structures
*/
/*----------------------------------------------------------------------------*/

/** @brief forward declaration for hqvdp session handle */
typedef struct hqvdp_lld_ctxt *hqvdp_lld_hdl_t;


/** @brief Memory range descriptor
    @ingroup lld_struct
 */
struct hqvdp_lld_mem_desc_s
{
    int size;            /**< @brief byte size */
    int alignment;       /**< @brief byte alignment */
    void *logical;       /**< @brief logical address */
    void *physical;      /**< @brief physical address */
};


/** @brief Memory needs descriptor
    @ingroup lld_struct
    @todo HW/SW: review why we would need a buffer specific for sw context storing. (what is the rational?)
 */
struct hqvdp_lld_mem_need_s
{
    int cmd_size;           /**< @brief byte size for storing XP70(s) commands*/
    int cmd_alignment;         /**< @brief byte alignment for the command so that XP70 streamer can read it. */
    /** @brief byte size for storing 1 motion buffer
        @note 3 of those must be allocated. Rotation of motion buffer is performed by user of the lld.
        - In case of 4K60, each motion buffer store motion relative to 2 HQVDP HW
        - It is ldd implementation that handles the offset of second HQVDP motion storage
    */
    int motion_buffer_size; /**< @note must include margin for alignment of second HQVDP HW motion storing in case of 4K60 */
    int motion_buffer_alignment; /**< @brief byte alignment for motion buffer so that HQVDP HW can R/W it. */
};


/** @brief mixer descriptor
    @ingroup lld_struct

*/

typedef struct HQVDPLITE_OUT_Params_s {
    uint32_t output_window_top_left_x; /**< @brief top left x coordinate of the output window in the output vtg */
    uint32_t output_window_top_left_y; /**< @brief top left y coordinate of the output window in the output vtg */
    uint32_t output_vtg_width; /**< @brief horizontal active size of the MIXER in pixels*/
    uint32_t output_vtg_height; /**< @brief vertical active size of the MIXER in pixels wrt progressive frame*/
    uint32_t output_vtg_fps;   /**< @brief output vtg frame rate*/
} HQVDPLITE_OUT_Params_t;

/** @brief color fill info
    @ingroup lld_struct

*/
typedef struct HQVDPLITE_VIDEOPLUG_Params_s {
    bool  color_fill_enabled; /**< @brief true if color fill is enabled at video plug level */
} HQVDPLITE_VIDEOPLUG_Params_t;

/** @brief hvsrc lut meta info
    @ingroup lld_struct

*/
typedef struct HQVDPLITE_LUT_HVSRC_Params_s {
    bool  custom; /**< @brief If true, no need to calculate the hvsrc lut, use directly the lut set in conf param */
    enum hvsrc_luma_strength_e luma_h_strength; /**< @brief define the horizontal luma filter strength */
    enum hvsrc_chroma_strength_e chroma_h_strength; /**< @brief define the horizontal chroma filter strength */
    enum hvsrc_luma_strength_e luma_v_strength; /**< @brief define the vertical luma filter strength */
    enum hvsrc_chroma_strength_e chroma_v_strength; /**< @brief define the vertical chroma filter strength */
} HQVDPLITE_LUT_HVSRC_Params_t;

/** @brief lld api configuration structure
    @ingroup lld_struct
    @note refer to [c8fvp3_hqvdplite_api.pdf](#AR1)http://gnx5572.gnb.st.com:8080/hdmsdb/C8FVP3_FW/c8fvp3_FW_hqvdplite_MGC_api_lib/doc/v2.13/c8fvp3_hqvdplite_api.pdf) for fields description
    and usage.

    @todo HW/SW Agree on the strict need or not for having HQVDPLITE_HVSRC_Params_t Hvsrc in the conf instead of only strength parameters.
    between 2 Vsync (limit number of memcopy on lld side).
    @note this is the Cannes HQVDPLITE_CMD_t where only the command part is kept and a new structure for describing the
    HQVDP output window positioning in the compositor frame.
*/

struct hqvdp_lld_conf_s {
  HQVDPLITE_TOP_Params_t Top;
  HQVDPLITE_VC1RE_Params_t Vc1re;
  HQVDPLITE_FMD_Params_t Fmd;
  HQVDPLITE_CSDI_Params_t Csdi;
  HQVDPLITE_HVSRC_Params_t Hvsrc;
  HQVDPLITE_IQI_Params_t Iqi;
  HQVDPLITE_OUT_Params_t Out; /**< @brief mixer relative information */
  HQVDPLITE_VIDEOPLUG_Params_t VideoPlug; /**< @brief Video Plug related information */
  HQVDPLITE_LUT_HVSRC_Params_t LutHvsrc; /**< @brief meta info to get the right hvsrc lut */
} ;


/** @brief structure describing HQVDP XP70 binaries location and loading mode
    @ingroup lld_struct
    @note
    - in #FW_LOAD_STREAMER mode, LLD user must copy fw binary in physical memory,
        ie hqvdp_lld_fw_s.pmem.physical & hqvdp_lld_fw_s.dmem.physical must be set by user.
    - in #FW_LOAD_T1, fw does not need to be loaded in physical memory, setting
        hqvdp_lld_fw_s.pmem.logical and hqvdp_lld_fw_s.dmem.logical to the address of the symbol storing the FW is
        sufficient.
        A typical call for setting this structure is:
        @code
        hqvdp_lld_fw_s fw;
        fw.mode = FW_LOAD_T1;
        fw.pmem.logical = HQVDPLITE_pmem;
        fw.pmem.size = sizeof(HQVDPLITE_pmem);
        fw.dmem.logical = HQVDPLITE_dmem;
        fw.dmem.size = sizeof(HQVDPLITE_dmem);
        fw.pmem.physical = fw.dmem.physical = NULL; // not used

        @endcode
 */
struct hqvdp_lld_fw_s {
        enum hqvdp_lld_fw_load_e mode; /**< @brief DMA (streamer) or ARM FW loading. */
        struct hqvdp_lld_mem_desc_s pmem; /**< @brief pmem binary */
        struct hqvdp_lld_mem_desc_s dmem; /**< @brief dmem binary */
};


/** @brief lld api init configuration structure
    @ingroup lld_struct                         */
struct hqvdp_lld_init_s
{
    struct hqvdp_lld_fw_s   fw;                /**< @brief FW loading informations. */

    const void            * hqvdp_base_addr;   /**< @brief base address of an HQVDP HW instance*/

    void                  * video_plug_handle; /**< @brief handle on the video plug associated to the HQVDP HW instance
                                                    @note this handle may be a base address or any reference to a sw abstraction
                                                    of the plug. */

    uint32_t                mixer_bit;         /**< @brief bit of the GAM_MIXER_MAIN_CTL/GAM_MIXER_AUX_CTL,... associated to the HQVDP

                                                    @todo check that all MIXER will always use the same bit position to
                                                    enable the same plane. (ie HQVDP1 bit position is the same in all MIXERS) */

    void                  * debug_settings;    /**< @brief init opaque parameter used for lld validation/testing.*/
};


/** @brief lld api status structure
    @ingroup lld_struct

*/

struct hqvdp_lld_status_s {
  HQVDPLITE_TOPStatus_Params_t TopStatus;
  HQVDPLITE_FMDStatus_Params_t FmdStatus;
  HQVDPLITE_CSDIStatus_Params_t CsdiStatus;
  HQVDPLITE_HVSRCStatus_Params_t HvsrcStatus;
  HQVDPLITE_IQIStatus_Params_t IqiStatus;
};

/** @brief structure describing R or W HQVDP STBusPlug Setup
    @ingroup lld_struct
 */

struct hqvdp_plug_s {
        uint32_t page_size;
        uint32_t min_opc;
        uint32_t max_opc;
        uint32_t max_chk;
        uint32_t max_msg;
        uint32_t min_space;
        uint32_t control;
};


/** @brief structure describing  R & W HQVDP STBusPlug Setup
    @ingroup lld_struct
*/
struct hqvdp_lld_stbp_setup_s {
    struct hqvdp_plug_s read_plug;
    struct hqvdp_plug_s write_plug;
};

/** @brief video plug setup
    @ingroup lld_struct
*/
struct hqvdp_lld_video_plug_setup_s {
    uint16_t top_left_x; /**< @brief picture position for VideoPlug */
    uint16_t top_left_y; /**< @brief picture position for VideoPlug */
    uint16_t bottom_right_x; /**< @brief picture position for VideoPlug */
    uint16_t bottom_right_y; /**< @brief picture position for VideoPlug */
    uint16_t crop_left; /**< @brief crop value to use to program VideoPlug */
    uint16_t crop_right; /**< @brief crop value to use to program VideoPlug */
    bool pixel_repeat; /**< @brief pixel repeat flag to use to program VideoPlug */
    void * video_plug_handle;  /**< @brief handle of video plug of the HQVDP HW as described in #hqvdp_lld_init_s */
};


/** @brief hqvdp_lld_set_conf output parameters
    @ingroup lld_struct                         */
struct hqvdp_lld_compo_setup_s
{
    bool     is_active;     /**< @brief state if the HW HQVDP instance is used (MIXER configuration) */

    uint32_t mixer_bit;     /**< @brief #hqvdp_lld_init_s.mixer_bit of the active/ inactive hqvdp hw */

    struct hqvdp_lld_video_plug_setup_s plug_setup;  /**< @brief video plug setup
                                                          plug setup is relevant (needs to be applied by drive) only if hqvdp hw instance
                                                          is active. */
};


/*----------------------------------------------------------------------------*/
/** @defgroup lld_function LLD API exported functions
*/
/*----------------------------------------------------------------------------*/



/** @brief report firmware package version version.
    @ingroup  lld_function

    @retval Firmware package version
    @pre no pre condition
*/
const char *hqvdp_lld_get_firmware_package_version(void);

/** @brief initialyze the hqvdp lld ...
    @ingroup  lld_function

    @retval 0 on success,
    @retval #HQVDP_LLD_ERR_BOOT_FAILED if boot XP70(s) failed.

    @warning This function must be called only once. Depending on the SOC HW and
    parameters passed, 2 HQVDP logical instances supporting up to 4Kp30 or one supporting up to
    4Kp60 will be available.
    @post #hqvdp_lld_get_memory_needs call can be made.

    @note It is assume that no use case requires to change the STB plug setup once lld init has been done.
    @todo HW/SW How can we implement a timeout for the boot. Does the LLD actual implementation "wait" or do active polling?
    @todo HW/SW clarify how XP70 firmware shall be loaded xstreamer/direct write.
*/
enum hqvdp_lld_error hqvdp_lld_init (
    int                           hqvdp_nb,       /**< [in] : number of HQVDP HW instances on the SOC. */
    const struct hqvdp_lld_init_s init_param[]    /**< [in] : initialization struct. */
);



/** @brief get memory needs for a HQVDP plane (whether it uses 1 or 2 HQVDP HW Ips)
    @ingroup  lld_function
    @pre none
    @post #hqvdp_lld_open
*/
enum hqvdp_lld_error hqvdp_lld_get_memory_needs (
    enum hqvdp_lld_profile_e      profile,        /**< [in] : worst case (fps/resolution) intented to be used with the session. */
    struct hqvdp_lld_mem_need_s * memory_needs    /**< [in] : memory required for Motion buffers and XP70(s) cmds */
);


/** @brief Load and boot the HQVDP(s) XP70 firmware(s), configure HQVDP STBus R&W Plugs
    @ingroup  lld_function

    @retval 0 on success,
    @retval HQVDP_LLD_ERR_INVALID_PARAM in mem_for_cmd is null.
    @retval HQVDP_LLD_ERR_NO_PROFILE in case profile requested is not supported or
    not enough free HW resources to enable it.

    @pre #hqvdp_lld_init, #hqvdp_lld_get_memory_needs  has been called
    @warning mem_for_cmd must be contiguous non cachable physical memory.
    @note
        - *hw_used_bit_mask is used to statically configure the dataflow from HQVDP HW
    to mixers.
        - *mixer_bit_mask (from #hqvdp_lld_set_conf) is used to know which plane to
        enable at MIXER input for each LLD frame operation as LLD implementation may
        use only one HQVDP HW instance when VTG is lower than 4k60 even if 2 HW instances
        have been connected at open time.
*/
enum hqvdp_lld_error hqvdp_lld_open (
    enum hqvdp_lld_profile_e              profile,          /**< [in]  : worst case (FPS/resolution) intent to be used with this session.     */
    enum hqvdp_lld_vsync_mode_e           vsync_mode,        /**< [in]  : define which event will start HQVDP command processing */
    const struct hqvdp_lld_mem_desc_s    *mem_for_cmd,      /**< [in]  : pointer to descriptor of memory allocated by caller to store XP70 command. */
    const struct hqvdp_lld_stbp_setup_s  *stbp_setup,       /**< [in]  : pointer to setup configuration for the STBus plug of a given HQVDP HW IP.
                                                               - Only one configuration is passed by the user whether the LLD use one or 2 HW instances as same stbus plug
                                                               setup is used in case 2 HW HQVDP are reserved for the profile. */
    uint32_t                             *hw_used_bit_mask, /**< [out] : pointer to ORed bit mask for the maximum number of HQVDP HW IPs used for the requested profile.
                                                               - This is the ORed result of the #hqvdp_lld_init_s.mixer_bit of the HQVDP HW instance selected
                                                               by the LLD. This information is used statically by the driver to program the plane routing
                                                               to the MIXER (aka cross bar). */
    hqvdp_lld_hdl_t                      *hdl_p             /**< [out] : pointer to handle identifying the HQVDP. NULL in case of failure   */
);


/** @brief Generate a soft VSYNC on HQVDP(s) attached to handle
    @pre : HQVDP has to be set in SOFT_VSYNC mode before
*/
void hqvdp_lld_gen_soft_vsync(
    hqvdp_lld_hdl_t hdl /**< [in] : handle of the session. */
);


/** @brief get the HW revision of the IP used for a given session.
    @ingroup  lld_function

    @retval HW revision, 0 if hdl is not valid.
    @note this function is expected to be used for debug/tracing purpose only.
*/
uint32_t hqvdp_lld_get_hw_version ( hqvdp_lld_hdl_t hdl     /**< [in] : handle of the session. */);


/** @brief get the capabilities of a given plane (bitmask)
    @ingroup  lld_function

    @retval capabilities bitfield
    @pre hdl is valid
    @note this function is a provision.
    @todo HW/SW define capabilities bitfield
*/
uint32_t hqvdp_lld_get_capability ( hqvdp_lld_hdl_t hdl     /**< [in] : handle of the session. */);


/** @brief get the number of hardware that will be used for a specific use case
    @details This function allows to implement 'IsHwProcessingTimeOk' function in the driver
    @ingroup  lld_function

    @retval 0 on success,
    @retval HQVDP_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @todo [version 7.x] 'IsHwProcessingTimeOk' should be implemented in the LLD and replace this function.
*/
enum hqvdp_lld_error hqvdp_lld_get_number_of_hardware (
    hqvdp_lld_hdl_t                       hdl,               /**< [in] : handle of the session. */
    const uint32_t                        input_picture_height,
    const struct HQVDPLITE_OUT_Params_s * out_param,         /**< [in] : pointer to output settings */
    int                                 * hardware_nb        /**< [out]: pointer to number of hardwares needed (1 or 2) */
);

/** @brief get the split width value of one HW.
    @details This function allows to get the split width needed for check size or rescale perfomances.
    @ingroup  lld_function

    @retval 0 on success,
    @retval HQVDP_LLD_ERR_INVALID_PARAM in case od invalid input and ouput width.
*/
enum hqvdp_lld_error hqvdp_lld_get_split_width(
    hqvdp_lld_hdl_t hdl,                 /**< [in] : handle of the session. */
    uint16_t        input_width,         /**< [in] : api command input viewport width. */
    uint16_t        output_width,        /**< [in] : api command ouput hvsrc width. */
    uint16_t        *input_split_width,   /**< [out]: define the input split width of one HW */
    uint16_t        *output_split_width  /**< [out]: define the output split width of one HW */
);

/** @brief configure the HQVDP for the next Vsync operation
    @ingroup  lld_function

    @retval 0 on success,
    @retval HQVDP_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @note depending on the init performed (#hqvdp_lld_init) the command may be
    split on 2 or 1 HQVDP HW IPs.
    @note lld handles on it owns the bufferization of the cmd into pending and
    active.
    @note upon call completion caller is free to modify *cmd
    @note user of lld must allocate struct hw_setup[2]
*/
enum hqvdp_lld_error hqvdp_lld_set_conf (
    hqvdp_lld_hdl_t                 hdl,               /**< [in] : handle of the session. */
    const struct hqvdp_lld_conf_s * conf,              /**< [in] : pointer to configuration (processing command). */
    int                           * setup_nb,          /**< [out]: pointer to number of mixer video plug to configure (1 or 2)
                                                          - For 4K60 profile *setup_nb is always 2 as lld needs to state for each hqvdp_lld_set_conf call
                                                          which HQVDP HW id is used so that mixer is reprogrammed accordingly. */
    struct hqvdp_lld_compo_setup_s  compo_setup [HQVDP_LLD_MAX_HW_FOR_ONE_PLANE]     /**< [out]: hw setup (plug, mixer) to be applied outside of the lld scope. */
);


/** @brief configure the HQVDP for the next Vsync operation
    @ingroup  lld_function

    @retval 0 on success,
    @retval HQVDP_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @pre #hqvdp_lld_set_conf has been called

    @note depending on the init performed (#hqvdp_lld_init) the command will be
    split on 2 HQVDP HW IPs. depending on the split 1 or 2 status are reported
    @note user of lld must allocate struct hqvdp_lld_status_s[2]
*/
enum hqvdp_lld_error hqvdp_lld_get_status (
    hqvdp_lld_hdl_t             hdl,                                        /**< [in] : handle of the session. */
    int                       * status_cnt,                                 /**< [out]: number of statuses (1 or 2) */
    struct hqvdp_lld_status_s   status [HQVDP_LLD_MAX_HW_FOR_ONE_PLANE]     /**< [out]: status(es) of the last command. caller allocates memory for 2 setup */
);



/** @brief Stop the HQVDP processing
    @ingroup  lld_function
    @retval HQVDP_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @note this call is equivalent to the old "NullCmd"
    @todo HW/SW check In which cases we actually need to stop.
*/
enum hqvdp_lld_error hqvdp_lld_stop ( hqvdp_lld_hdl_t hdl     /**< [in] : handle of the session. */);


/** @brief close an hqvdp session.
    @ingroup  lld_function

    @retval 0 on success,
    @retval HQVDP_LLD_ERR_INVALID_PARAM in case hdl is not valid

    @pre #hqvdp_lld_stop has been stopped.
    @post a new #hqvdp_lld_open call can be done
*/
enum hqvdp_lld_error hqvdp_lld_close ( hqvdp_lld_hdl_t hdl     /**< [in] : handle of the session. */);


/** @brief delete hqvdp_lld
    @ingroup  lld_function

    @retval 0 on success
    @note this function cannot fail. It deallocates any logical memory
    allocated by the lld at #hqvdp_lld_init time. All IP clocks that are
    under lld control are stopped.
    @note this function is typically called when hqvdp driver is unloaded.
*/
enum hqvdp_lld_error hqvdp_lld_terminate ( void );





/* C++ support */
#ifdef __cplusplus
}
#endif


#endif /*__HQVDP_LLD_API__*/

/* End of C stuff, following stuff is purely for documentation of the API. */

/**

@mainpage Orly3/Cannes 2.5 HQVDP Lite LLD API

@tableofcontents



References {#S1}
================

| ref                       | author            | title                                             | Link                          |
|:--------------------------|:------------------|:--------------------------------------------------|-------------------------------|
|<a id="SMA1">SMA1</a>  |Serge Mazer                | c8fvp3_hqvdplite_func_spec.pdf                  |[c8fvp3_hqvdplite_func_spec.pdf](http://gnx5572.gnb.st.com:8080/hdmsdb/C8FVP3_HW/c8fvp3_hqvdplite_top_lib/doc/v2.4/c8fvp3_hqvdplite_func_spec.pdf)|
|<a id="HAR1">HAR1</a>  |Heni Arab                | c8fvp3_hqvdplite_api.pdf                 |[c8fvp3_hqvdplite_api.pdf](http://gnx5572.gnb.st.com:8080/hdmsdb/C8FVP3_FW/c8fvp3_FW_hqvdplite_MGC_api_lib/doc/v2.13/c8fvp3_hqvdplite_api.pdf)|


Open points & Todos {#S2}
===================
[todo](./todo.html)


Functions {#S3}
=========
@ref lld_function

Types {#S4}
=====
@ref lld_struct

@ref lld_enum

Defines {#S5}
======
@ref lld_defines

Macros {#S6}
======

Macro are used to remap HW access/ libC calls to the actual target implementation.

@ref macro_hw

@ref macro_mem

Usage {#S7}
=====
The bellow sequence diagram show 2 intended usage of the LLD API.
- One "4KP60 STB" intended usage where both HQVDP HW resources are seen as one 4KP60 capable plane.
- One "dual 4KP30 IVI " intended usage where both HQVDP HW resources are used as 2 independant planes

4KP60 usage {#S71}
-----------

![HQVDP usage for 4K STB ](hqvdp_lld_4KP60.png)

2x 4KP30 usage {#S72}
--------------
![HQVDP usage for IVI ](hqvdp_lld_2X4KP30.png)

Example of motion buffer sizing for dual HQVDP mode {#S63}
--------------
@code

#define OVERLAP_WIDTH 100
#define MOTION_BUFFER_WIDTH (1920+2*OVERLAP_WIDTH)
#define MOTION_BUFFER_HEIGHT (544)

@endcode

Revisions {#S8}
=========
@version 6.2.5 (September, 9th 2014)
    - merge of .h available on software architecture SVN and LLD official delivery

@version 6.2.5 (August 2014)
    - __API BREAK__: replace #hqvdp_lld_get_api_version by #hqvdp_lld_get_firmware_package_version
      new function is returning a string containing firmware package version
    - remove #hqvdp_lld_get_capability as it is not used

@version 6.2.4 (July 2014)
    - fix doxygen documentation on define
    - fix spelling

@version 6.2.3 (July 2014)
    - add void in function with no arguments
    - change alignment to alignment in structure member
    - rename hvsrc_chroma_strength to hvsrc_chroma_strength_e
    - add const on mem_for_cmd argument of function hqvdp_lld_open

@version 6.2.2 (3 June 2014)
    - keep only one include for all command structures
    - add HQVDP_LLD_ERR_NO_RESSOURCE_AVAILABLE error
    - remove const on "struct hqvdp_lld_fw_s     fw"
    - fix doxygen documentation of  hqvdp_lld_terminate

@version 6.2.1 (21 May 2014)
    - minor changes to allow complation
    - remove // comments
    - added HQVDPLITE_HVSRCStatus_Params_t in hqvdp_lld_status_s structure
    - integrate in hqvdp_lld_platform.h what is requiered to compile in VERIFICATION_PLATFORM
      and allowing to make check for fwpackage delivery.

@version 6.2.0 draft (20 May 2014)
    - __API BREAK__: added fw descriptor in #hqvdp_lld_init_s (#hqvdp_lld_fw_s)
    - __API BREAK__: renamed hqvd_lld_compo_setup_s as #hqvdp_lld_compo_setup_s (typo correction)
    - renamed hvsrc_luma_strength_e & hvsrc_chroma_strength enums to avoid lld compilation issues.

@version 6.1.0 (5 May 2014)
    - __API BREAK__:renamed #hqvd_lld_init_s to #hqvdp_lld_init_s (typo)
    - __API BREAK__:stbp_setup field removed from #hqvdp_lld_init_s
    - __API BREAK__: stbp_setup passed as a parameter in hqvdp_lld_open()
    @todo VsyncMode support.
    @todo update sequence diagrams.

@version 6.0.0 (16 April 2014)
    -  added includes for all c8fvp3_hqvdplite_api_XXX.h files
    - renamed  mem_desc_s to #hqvdp_lld_mem_desc_s
    - renamed mem_need_s to #hqvdp_lld_mem_need_s
    - renamed hqvdp_lld_output_window_s to #HQVDPLITE_OUT_Params_s (to avoid confusing coding styles).
    - modified #hqvdp_lld_conf_s
    - added #hqvd_lld_init_s to club all init parameters together
    - #hqvdp_lld_video_plug_setup_s modified to better suite HW programming model
    - added #hqvdp_lld_compo_setup_s to club together video plugs and mixer configuration setup.
    - added *hw_used_bit_mask output param to #hqvdp_lld_open to enable cross bar configuration.
    - renamed hqvdp_lld_exit as #hqvdp_lld_terminate




@version 5.1.0 (8 April 2014)
    - Changed #hqvdp_lld_set_conf prototype and definition of #hqvdp_lld_video_plug_setup_s
    - Changes in #hqvdp_lld_conf_s

@version 5.0.0 (4 April 2014)
    - Update following review done with Yvan's team W14.2
    - change all names to hqvdp_lld_XXX to be compliant with [Low Level Driver Developer Guide](https://codex.cro.st.com/plugins/docman/?group_id=3176&action=show&id=305584)
    - add of #hqvdp_lld_exit
    - add of #hqvdp_lld_set_hvsrc_lut (equivalent to former c8fvp3_SetHvsrcLut)
    - add of #hqvdp_lld_get_capability
    - add of #hqvdp_lld_get_api_version
    - HQVDPLITE_HVSRC_Params_t Hvsrc put back in #hqvdp_lld_conf_s
    - add of #WRITE_MB write memory barrier macro.
    - add of #ALLOCATE_LLD_MEM
    - add of #FREE_LLD_MEM
    - updated list of [todo](./todo.html) to have a clean slate for next API review.

@version 4.0.1 (31 March 2014)
    - Update of sequence diagram following latest API review.
@version 4.0.0
    - __API BREAK__: update of #hqvdp_conf_s, creation of #hqvdp_scaler_param_s
@version 3.0.1
    - changes in hqvdp_target.h to ease later integration of lld SW in linux kernel build.
@version 3.0.0
    - Many modification following review with O Lorente.
@version 2.0.0
    - cleaning of hqvdp_conf_s (removal of HVSRC part) for review with HW/SW team.
    - pending: review of all macros for running the LLD on verification platform /linux kernel

@version 1.0.0 initial revision provided to dev for review.


*/
