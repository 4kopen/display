/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

 /**@file hqvdp_lld_platform.h
    @brief macro definition files to enable execution on different targets

    @warning any call to the libc function shall be done through a macro so that
    call can be redirected when driver is from inside a linux kernel context.
 */

#ifndef __HQVDP_LLD_PLATFORM_H__
#define __HQVDP_LLD_PLATFORM_H__

/*----------------------------------------------------------------------------*/
#ifdef VERIFICATION_PLATFORM /**< @todo: change for actual define from Yvan's team*/
/*----------------------------------------------------------------------------*/

#include <stdint.h>     /**< @brief definition of  uint32_t and so on */
#include <stdbool.h>    /**< @brief definition of  bool */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#define ANSI_COLOR_RED     "\x1b[31m" /**< @brief red */
#define ANSI_COLOR_GREEN   "\x1b[32m" /**< @brief green */
#define ANSI_COLOR_YELLOW  "\x1b[33m" /**< @brief yellow */
#define ANSI_COLOR_BLUE    "\x1b[34m" /**< @brief blue */
#define ANSI_COLOR_MAGENTA "\x1b[35m" /**< @brief magenta */
#define ANSI_COLOR_CYAN    "\x1b[36m" /**< @brief cyan */
#define ANSI_COLOR_RESET   "\x1b[0m" /**< @brief reset */

/* get definition */
#include "global_verif_hal_stddefs.h"
#include "vibe_os.h"

/** @todo Add all verification platform build specific includes here */

/** @defgroup macro_hw verification platform HW registers write macros
    @warning all base address parameters must be of type uint32_t*
    which means that there is a byte address increase of 4 bytes for each
    offset increment.
    @{
*/

/** @brief write a 32 bits register
    @param[in] base : base address of the IP
    @param[in] offset: offset of the register from the base address
    @param[in] val: 32 bits value to write.
*/

#define REG32_WRITE(base, offset, val) vibe_os_write_register((volatile uint32_t*)(base), (offset), (val))

/** @brief read a 32 bits register */
#define REG32_READ(base, offset) (uint32_t)(vibe_os_read_register((volatile uint32_t*)(base), (offset)))

/** @} register access macros */

/** @defgroup macro_mem macro related to memory management
    @{  */

/** @brief write memory barrier
    @note most probably not needed on verification platform as long as code does
    not use an ARM cycle accurate emulator.
 */
#define WRITE_MB()

/** @brief memory allocator for sw related content only memory cannot be shared with HW
    @param[in] size in sizeof units
    @retval void* pointer on allocated memory.
    @note most probably a simple malloc on verification platform
 */

#define ALLOCATE_LLD_MEM(size) malloc(size)


/** @brief memory deallocator for sw related content only
    @param[in] mem: pointer returned by ALLOCATE_LLD_MEM
    @note most probably a simple free on verification platform
 */
#define FREE_LLD_MEM(mem) free(mem)


/** @brief memcpy macro
    @param[in] d: (void*) pointer on destination address
    @param[in] s: (void*)pointer on source address
    @param[in] n: memory transfer size is size_t units

    @note This is a macro. It might mapped on standard memcpy or not.

 */

#define MEMCPY(d,s,n) memcpy(d,s,n)

/** @brief memset macro
    @param[in] p: (void*) pointer to the first bytes of the block to be fill.
    @param[in] v: (int) value to be set
    @param[in] s: (size_t) number of bytes to be set to v

    @note This is a macro. It might mapped on standard memset or not.

*/
#define MEMSET(p,v,s) memset(p,v,s)



/** @} macro_mem */

/** @warning any libc call must be discussed and accessed through a macro as libc is not
accessible from code executing in linux kernel */


/** @defgroup trace_func trace generation macros.
    @note to be compliant with the embedded code tracing framework the following
    function must be used for tracing (if needed). A dedicated trace ID is allocated
    to hqvdp lld implementation: id = TRC_ID_HQVDPLITE_LLD

    @{
*/


enum trc_id {                     /* [family -] trace description */
    TRC_ID_ERROR,                 /**< @brief always print messages */
    TRC_ID_WARNING,                /**< @brief print a warning message */
    TRC_ID_HQVDPLITE_LLD,         /**< @brief print HQVDP LLD relative normal messages */
    TRC_ID_CAPTURE_LLD,           /**< @brief print HQVDP LLD relative normal messages */
    TRC_ID_HQVDPLITE_VERIF,       /**< @brief print HQVDP VERIF relative normal messages */
    TRC_ID_PRINTF,                /**< @brief do a normal printf */
};


/** @brief generic trace macro
    @note do not use directly, use #TRCIN, #TRCOUT or #TRC instead
*/
#define TRCGEN(id,c,...)  \
    do { \
        switch ( id ) { \
        case TRC_ID_HQVDPLITE_LLD: \
                printf(ANSI_COLOR_GREEN "[TRC_HQVDP_LLD] %5c %-35s " ANSI_COLOR_RESET, c, __PRETTY_FUNCTION__); \
                break; \
        case TRC_ID_CAPTURE_LLD: \
                printf(ANSI_COLOR_YELLOW "[TRC_CAPTURE_LLD] %5c %-35s " ANSI_COLOR_RESET, c, __PRETTY_FUNCTION__); \
                break; \
        case TRC_ID_HQVDPLITE_VERIF: \
                printf(ANSI_COLOR_MAGENTA "[TRC_HQVDP_VERIF] %5c %-35s " ANSI_COLOR_RESET, c, __PRETTY_FUNCTION__); \
                break; \
        case TRC_ID_ERROR: \
                printf(ANSI_COLOR_RED "[TRC_HQVDP_ERROR] %5c %-35s " ANSI_COLOR_RESET, c, __PRETTY_FUNCTION__); \
                break; \
        case TRC_ID_WARNING: \
                printf(ANSI_COLOR_MAGENTA "[TRC_HQVDP_WARNING] %5c %-35s " ANSI_COLOR_RESET, c, __PRETTY_FUNCTION__); \
                break; \
        case TRC_ID_PRINTF: \
                printf("%c", c); \
                break; \
        default: \
                printf(ANSI_COLOR_RED "[TRC_HQVDP_UNKNOWN] %5c %-35s " ANSI_COLOR_RESET, c, __PRETTY_FUNCTION__); \
        } \
        printf("" __VA_ARGS__); \
        printf("\n"); \
    } while(0)


/** @brief trace macro to log entry in a function */
#define TRCIN(id,args...)   TRCGEN(id, '>', ##args)

/** @brief trace macro to log exit of a function */
#define TRCOUT(id,args...)  TRCGEN(id, '<', ##args)

/** @brief trace macro to log anything but exit/entry. */
#define TRC(id,args...)     TRCGEN(id, '.', ##args)

/** @brief workaround for VSOC (ticket : 60723) */
#define PRINTF(args) vibe_printf(args)

/** @} trace_func */


/*----------------------------------------------------------------------------*/
#else /*  Linux Kernel build with VibeOs*/
/*----------------------------------------------------------------------------*/


/* Type definition */
#ifndef __cplusplus
/* do not redefine bool if we are compiling in c++ */
typedef unsigned char  bool;
#endif /* n __cplusplus */
typedef int            int32_t;
typedef short          int16_t;

/* define int8_t and prevent redefinition by types.h include by stdlib.h */
typedef char           int8_t;
#define __int8_t_defined

typedef unsigned int   uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char  uint8_t;
__extension__ typedef unsigned long long uint64_t;

typedef uint32_t uintptr_t;

/*
 * Some good old style C definitions for where we have imported code
 * from elsewhere.
*/
#define FALSE (1==2)
#define TRUE  (!FALSE)
#undef NULL
#if defined(__cplusplus)
  #define NULL 0
#else
  #define NULL ((void *)0)
#endif

#define false FALSE
#define true TRUE

#include "vibe_os.h"  /* header file for vibe osal */
#include "vibe_debug.h" /* definition of TRCIN, TRCOUT, TRC */

 /** @defgroup macro_hw linux kernel HW registers write macros
    @{
*/

#define REG32_WRITE(base, offset, val) vibe_os_write_register((volatile uint32_t*)(base), (offset), (val))
#define REG32_READ(base, offset) (uint32_t)(vibe_os_read_register((volatile uint32_t*)(base), (offset)))
/* to be replaced by macro above */

/** @} */


/** @defgroup macro_mem macro related to memory management
    @{  */

/** @brief write memory barrier
 */
#define WRITE_MB() vibe_os_write_memory_barrier()


/** @brief memory allocator for sw related content only memory cannot be shared with HW
    @param[in] size in sizeof units
    @retval void* pointer on allocated memory.
 */
#define ALLOCATE_LLD_MEM(size)  vibe_os_allocate_memory(size)


/** @brief memory deallocator for sw related content only
    @param[in] mem: pointer returned by ALLOCATE_LLD_MEM
 */
#define FREE_LLD_MEM(mem)       vibe_os_free_memory(mem)


/** @brief memcpy macro
    @param[in] d: (void*) pointer on destination address
    @param[in] s: (void*) pointer on source address
    @param[in] n: memory transfer size is size_t units

    @note This is a macro. It might mapped on standard memcpy or not.
*/
#define MEMCPY(d,s,n) vibe_os_memcpy(d,s,n)

/** @brief memset macro
    @param[in] p: (void*) pointer to the first bytes of the block to be fill.
    @param[in] v: (int) value to be set
    @param[in] s: (size_t) number of bytes to be set to v

    @note This is a macro. It might mapped on standard memset or not.

*/
#define MEMSET(p,v,s) vibe_os_memset(p,v,s)


/** @brief workaround for VSOC (ticket : 60723) */
#define PRINTF(args) vibe_printf(args)

/** @} macro_mem */

#endif /*VERIFICATION_PLATFORM */

#endif /* __HQVDP_LLD_PLATFORM_H__*/
