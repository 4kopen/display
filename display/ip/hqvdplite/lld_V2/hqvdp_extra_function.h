/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _HQVDP_EXTRA_FUNCTION_
#define _HQVDP_EXTRA_FUNCTION_

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

#include "hqvdp_lld_platform.h"
#include "hqvdp_handle.h"

/*!
 * \brief return true when a command is pending in next command mode
 *
 * \param[in] base_addr base address of the IP
 */
bool c8fvp3_IsCmdPending(void *base_addr);

/*!
 * \brief return true when a command is pending in next command mode for hqvdp(s) attached to session
 *
 * \param[in] hdl Handle of a session
 */
bool hqvdp_extra_is_cmd_pending(hqvdp_lld_hdl_t hdl);

/*!
 * \brief return true when FW ready to process a command  for hqvdp(s) attached to session
 *
 * \param[in] hdl : handle to session
 */
bool hqvdp_extra_is_ready(hqvdp_lld_hdl_t hdl);

/*!
 * \brief return true when command finished
 *
 * \param[in] base_addr base address of the IP
 */
bool c8fvp3_GetEopStatus(void *base_addr);

/*!
 * \brief return true when command finished for all hqvdp attached to session
 *
 * \param[in] hdl : handle to session
 */
bool hqvdp_extra_get_eop_status(hqvdp_lld_hdl_t hdl);
/*!
 * \brief Clear End of Picture status bit
 *
 * \param[in] base_addr : base address of the IP
 */
void c8fvp3_ClearInfoToHost(void *base_addr);

/*!
 * \brief Clear Irq
 *
 * \param[in] base_addr base address of the IP
 */
void c8fvp3_ClearIrqToHost(void *base_addr);

/* C++ support */
#ifdef __cplusplus
}
#endif

#endif /* _HQVDP_EXTRA_FUNCTION_ */

