/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_TOP_H
#define _C8FVP3_HQVDPLITE_API_TOP_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : TOP
*/


/**
* Register : CONFIG
* Global configuration
*/

#define TOP_CONFIG_OFFSET                               (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x00)
#define TOP_CONFIG_MASK                                 (0x0000003F)

/**
* Bit-field : PROGRESSIVE
* If set, this bit indicates that the input material is progressive and so that the algorithm should do a frame based processing. The input and output buffers are frame buffers. When clear, the algorithms process interlaced material and work on Field buffers.
*/

#define TOP_CONFIG_PROGRESSIVE_SHIFT                    (0x00000000)
#define TOP_CONFIG_PROGRESSIVE_WIDTH                    (1)
#define TOP_CONFIG_PROGRESSIVE_MASK                     (0x00000001)

/**
* Bit-field : TOP_NOT_BOT
* If set, this bit indicates that the current field is a Top (Odd) Field. Otherwise, it is a bottom field.
* Note: This bit is irrelevant and ignored if the PROGRESSIVE is set.
*/

#define TOP_CONFIG_TOP_NOT_BOT_SHIFT                    (0x00000001)
#define TOP_CONFIG_TOP_NOT_BOT_WIDTH                    (1)
#define TOP_CONFIG_TOP_NOT_BOT_MASK                     (0x00000002)

/**
* Bit-field : IT_ENABLE_EOP
* ThIS BIT indicates that an interrupt shall be raised to the driver when the processing of the current command is finished. The driver interrupt handler can then issue a new command using a software Vsync event.
*/

#define TOP_CONFIG_IT_ENABLE_EOP_SHIFT                  (0x00000002)
#define TOP_CONFIG_IT_ENABLE_EOP_WIDTH                  (1)
#define TOP_CONFIG_IT_ENABLE_EOP_MASK                   (0x00000004)

/**
* Bit-field : PASS_THRU
* This bit indicates that none processing by the algorithms is done: the input picture is only transferred from the system memory to the megacell and then written back to the external memory. This configuration shall be supported for an integration test case to check IP connections to the whole system.
*/

#define TOP_CONFIG_PASS_THRU_SHIFT                      (0x00000003)
#define TOP_CONFIG_PASS_THRU_WIDTH                      (1)
#define TOP_CONFIG_PASS_THRU_MASK                       (0x00000008)

/**
* Bit-field : NB_OF_FIELD
* Defines the number of field to be used at input of the megacell:
*/

#define TOP_CONFIG_NB_OF_FIELD_SHIFT                    (0x00000004)
#define TOP_CONFIG_NB_OF_FIELD_WIDTH                    (2)
#define TOP_CONFIG_NB_OF_FIELD_MASK                     (0x00000030)

/**
* Register : MEM_FORMAT
* Memory format
*/

#define TOP_MEM_FORMAT_OFFSET                           (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x04)
#define TOP_MEM_FORMAT_MASK                             (0x7FBFFFFF)

/**
* Bit-field : INPUT_FORMAT
* This 4 bit value defines the format of the unprocessed video buffers, i.e. video buffers that are read from external memory and will be processed by the megacell, as follows:
* others: reserved
*/

#define TOP_MEM_FORMAT_INPUT_FORMAT_SHIFT               (0x00000000)
#define TOP_MEM_FORMAT_INPUT_FORMAT_WIDTH               (4)
#define TOP_MEM_FORMAT_INPUT_FORMAT_MASK                (0x0000000F)

/**
* Bit-field : OUTPUT_FORMAT
* This 4 bit value defines the format of the processed video buffers, i.e. video buffers that are write to external memory, as follows:
* others: reserved
*/

#define TOP_MEM_FORMAT_OUTPUT_FORMAT_SHIFT              (0x00000004)
#define TOP_MEM_FORMAT_OUTPUT_FORMAT_WIDTH              (4)
#define TOP_MEM_FORMAT_OUTPUT_FORMAT_MASK               (0x000000F0)

/**
* Bit-field : OUTPUT_ALPHA
* Output alpha value. This value is taken in account only if MEM_TO_MEM is set, otherwise is ignored.
*/

#define TOP_MEM_FORMAT_OUTPUT_ALPHA_SHIFT               (0x00000008)
#define TOP_MEM_FORMAT_OUTPUT_ALPHA_WIDTH               (8)
#define TOP_MEM_FORMAT_OUTPUT_ALPHA_MASK                (0x0000FF00)

/**
* Bit-field : MEM_TO_TV
* if set, it indicates that data pixels (10 bits per pixels) should be sent to the compositor. Otherwise, data pixels will not be sent to the compositor and MEM_TO_MEM flag should be set. MEM_TO_TV and MEM_TO_MEM can't be set at the same time.
*/

#define TOP_MEM_FORMAT_MEM_TO_TV_SHIFT                  (0x00000010)
#define TOP_MEM_FORMAT_MEM_TO_TV_WIDTH                  (1)
#define TOP_MEM_FORMAT_MEM_TO_TV_MASK                   (0x00010000)

/**
* Bit-field : MEM_TO_MEM
* if set, it indicates that data pixels (8 bits per pixels) should be stored in external memory. Otherwise, data pixels will not be stored in external memory and MEM_TO_TV flag should be set. MEM_TO_TV and MEM_TO_MEM can't be set at the same time.
*/

#define TOP_MEM_FORMAT_MEM_TO_MEM_SHIFT                 (0x00000011)
#define TOP_MEM_FORMAT_MEM_TO_MEM_WIDTH                 (1)
#define TOP_MEM_FORMAT_MEM_TO_MEM_MASK                  (0x00020000)

/**
* Bit-field : FLAT3D
* Output is flat3d, the side duplicated is selected by RIGHTNOTLEFT
*/

#define TOP_MEM_FORMAT_FLAT3D_SHIFT                     (0x00000012)
#define TOP_MEM_FORMAT_FLAT3D_WIDTH                     (1)
#define TOP_MEM_FORMAT_FLAT3D_MASK                      (0x00040000)

/**
* Bit-field : RIGHTNOTLEFT
* selected side for Flat3d and FMD
*/

#define TOP_MEM_FORMAT_RIGHTNOTLEFT_SHIFT               (0x00000013)
#define TOP_MEM_FORMAT_RIGHTNOTLEFT_WIDTH               (1)
#define TOP_MEM_FORMAT_RIGHTNOTLEFT_MASK                (0x00080000)

/**
* Bit-field : OUTPUT_3D_FORMAT
*/

#define TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT           (0x00000014)
#define TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_WIDTH           (2)
#define TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK            (0x00300000)

/**
* Bit-field : PIX10BIT_NOT_8BIT
* if set, the data pixels are coded on 10 bits.
*/

#define TOP_MEM_FORMAT_PIX10BIT_NOT_8BIT_SHIFT          (0x00000017)
#define TOP_MEM_FORMAT_PIX10BIT_NOT_8BIT_WIDTH          (1)
#define TOP_MEM_FORMAT_PIX10BIT_NOT_8BIT_MASK           (0x00800000)

/**
* Bit-field : PIX8TO10_LSB_Y
* Defines the value of the two lsb luma bits to use in the pix8to10 block.(default: "10").
* Programmed only during verification phase. For application and functional used cases, the default value will be always programmed.
*/

#define TOP_MEM_FORMAT_PIX8TO10_LSB_Y_SHIFT             (0x00000018)
#define TOP_MEM_FORMAT_PIX8TO10_LSB_Y_WIDTH             (2)
#define TOP_MEM_FORMAT_PIX8TO10_LSB_Y_MASK              (0x03000000)

/**
* Bit-field : PIX8TO10_LSB_C
* Defines the value of the two lsb chroma bits to use in the pix8to10 block.(default :"00").
* Programmed only during verification phase. For application and functional used cases, the default value will be always programmed.
*/

#define TOP_MEM_FORMAT_PIX8TO10_LSB_C_SHIFT             (0x0000001a)
#define TOP_MEM_FORMAT_PIX8TO10_LSB_C_WIDTH             (2)
#define TOP_MEM_FORMAT_PIX8TO10_LSB_C_MASK              (0x0C000000)

/**
* Bit-field : INPUT_3D_FORMAT
*/

#define TOP_MEM_FORMAT_INPUT_3D_FORMAT_SHIFT            (0x0000001c)
#define TOP_MEM_FORMAT_INPUT_3D_FORMAT_WIDTH            (3)
#define TOP_MEM_FORMAT_INPUT_3D_FORMAT_MASK             (0x70000000)

/**
* Register : CURRENT_LUMA
* Input luma buffer base address
*/

#define TOP_CURRENT_LUMA_OFFSET                         (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x08)
#define TOP_CURRENT_LUMA_MASK                           (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer used as follows:
* For sources stored in raster format, it provides the translated base address in system memory of the input luma frame or field as indicated by PROGRESSIVE. When processing an interlaced material stored in an interleaved frame buffer, this address should therefore point alternatively to the first line of the frame (first line of top field) and the second line of the frame (first line of bottom field) as indicated by TOP_NOT_BOT.
* For sources stored in macroblock format, it provides the translated base address in system memory of the input luma buffer. In this case, even for interlaced material, this address corresponds to the position of the first pixel of the first line of the frame. This kind of buffers results from a decoding by the Delta IP and thus is supposed to respect its alignment constraints: height shall be a multiple of 32 bytes and width shall be a multiple of 16 bytes. The full frame buffer shall be aligned on 512 bytes.
*/

#define TOP_CURRENT_LUMA_BASE_ADDR_SHIFT                (0x00000000)
#define TOP_CURRENT_LUMA_BASE_ADDR_WIDTH                (32)
#define TOP_CURRENT_LUMA_BASE_ADDR_MASK                 (0xFFFFFFFF)

/**
* Register : CURRENT_ENH_LUMA
* Input luma enhanced buffer base address
*/

#define TOP_CURRENT_ENH_LUMA_OFFSET                     (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x0C)
#define TOP_CURRENT_ENH_LUMA_MASK                       (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define TOP_CURRENT_ENH_LUMA_BASE_ADDR_SHIFT            (0x00000000)
#define TOP_CURRENT_ENH_LUMA_BASE_ADDR_WIDTH            (32)
#define TOP_CURRENT_ENH_LUMA_BASE_ADDR_MASK             (0xFFFFFFFF)

/**
* Register : CURRENT_RIGHT_LUMA
* Input luma right buffer base address
*/

#define TOP_CURRENT_RIGHT_LUMA_OFFSET                   (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x10)
#define TOP_CURRENT_RIGHT_LUMA_MASK                     (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define TOP_CURRENT_RIGHT_LUMA_BASE_ADDR_SHIFT          (0x00000000)
#define TOP_CURRENT_RIGHT_LUMA_BASE_ADDR_WIDTH          (32)
#define TOP_CURRENT_RIGHT_LUMA_BASE_ADDR_MASK           (0xFFFFFFFF)

/**
* Register : CURRENT_ENH_RIGHT_LUMA
* Input luma enhanced right buffer base address
*/

#define TOP_CURRENT_ENH_RIGHT_LUMA_OFFSET               (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x14)
#define TOP_CURRENT_ENH_RIGHT_LUMA_MASK                 (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input enhanced right luma buffer
*/

#define TOP_CURRENT_ENH_RIGHT_LUMA_BASE_ADDR_SHIFT      (0x00000000)
#define TOP_CURRENT_ENH_RIGHT_LUMA_BASE_ADDR_WIDTH      (32)
#define TOP_CURRENT_ENH_RIGHT_LUMA_BASE_ADDR_MASK       (0xFFFFFFFF)

/**
* Register : CURRENT_CHROMA
* Input chroma buffer base address
*/

#define TOP_CURRENT_CHROMA_OFFSET                       (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x18)
#define TOP_CURRENT_CHROMA_MASK                         (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer used as follows:
* For sources stored in raster format, it provides the translated base address in system memory of the input chroma frame or field as indicated by PROGRESSIVE. When processing an interlaced material stored in an interleaved frame buffer, this address should therefore point alternatively to the first line of the frame (first line of top field) and the second line of the frame (first line of bottom field) as indicated by TOP_NOTBOT.
* For sources stored in macroblock format, it provides the translated base address in system memory of the input chroma buffer. In this case, even for interlaced material, this address corresponds to the position of the first pixel of the first line of the frame. This kind of buffers results from a decoding by the Delta IP and thus is supposed to respect its alignment constraints: width shall be a multiple of 16 bytes. If the number of macroblocks for the width is even the height shall be a multiple of 16 bytes. Otherwise it shall be a multiple of 32 bytes. The full frame buffer shall be aligned on 512 bytes.
*/

#define TOP_CURRENT_CHROMA_BASE_ADDR_SHIFT              (0x00000000)
#define TOP_CURRENT_CHROMA_BASE_ADDR_WIDTH              (32)
#define TOP_CURRENT_CHROMA_BASE_ADDR_MASK               (0xFFFFFFFF)

/**
* Register : CURRENT_ENH_CHROMA
* Input chroma enhanced buffer base address
*/

#define TOP_CURRENT_ENH_CHROMA_OFFSET                   (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x1C)
#define TOP_CURRENT_ENH_CHROMA_MASK                     (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input enhanced chroma buffer
*/

#define TOP_CURRENT_ENH_CHROMA_BASE_ADDR_SHIFT          (0x00000000)
#define TOP_CURRENT_ENH_CHROMA_BASE_ADDR_WIDTH          (32)
#define TOP_CURRENT_ENH_CHROMA_BASE_ADDR_MASK           (0xFFFFFFFF)

/**
* Register : CURRENT_RIGHT_CHROMA
* Input chroma right buffer base address
*/

#define TOP_CURRENT_RIGHT_CHROMA_OFFSET                 (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x20)
#define TOP_CURRENT_RIGHT_CHROMA_MASK                   (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input right chroma buffer
*/

#define TOP_CURRENT_RIGHT_CHROMA_BASE_ADDR_SHIFT        (0x00000000)
#define TOP_CURRENT_RIGHT_CHROMA_BASE_ADDR_WIDTH        (32)
#define TOP_CURRENT_RIGHT_CHROMA_BASE_ADDR_MASK         (0xFFFFFFFF)

/**
* Register : CURRENT_ENH_RIGHT_CHROMA
* Input chroma enhanced right buffer base address
*/

#define TOP_CURRENT_ENH_RIGHT_CHROMA_OFFSET             (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x24)
#define TOP_CURRENT_ENH_RIGHT_CHROMA_MASK               (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input enhanced right chroma buffer
*/

#define TOP_CURRENT_ENH_RIGHT_CHROMA_BASE_ADDR_SHIFT    (0x00000000)
#define TOP_CURRENT_ENH_RIGHT_CHROMA_BASE_ADDR_WIDTH    (32)
#define TOP_CURRENT_ENH_RIGHT_CHROMA_BASE_ADDR_MASK     (0xFFFFFFFF)

/**
* Register : OUTPUT_LUMA
* Output luma buffer base address
*/

#define TOP_OUTPUT_LUMA_OFFSET                          (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x28)
#define TOP_OUTPUT_LUMA_MASK                            (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the begin address in external memory of the output luma video frame in which the firmware will stored the processing data pixels (ACrYCb or Y only).
*/

#define TOP_OUTPUT_LUMA_BASE_ADDR_SHIFT                 (0x00000000)
#define TOP_OUTPUT_LUMA_BASE_ADDR_WIDTH                 (32)
#define TOP_OUTPUT_LUMA_BASE_ADDR_MASK                  (0xFFFFFFFF)

/**
* Register : OUTPUT_CHROMA
* Output chroma buffer base address
*/

#define TOP_OUTPUT_CHROMA_OFFSET                        (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x2C)
#define TOP_OUTPUT_CHROMA_MASK                          (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the begin address in external memory of the output chroma video frame in which the firmware will stored the processing data pixels (CbCr only).
*/

#define TOP_OUTPUT_CHROMA_BASE_ADDR_SHIFT               (0x00000000)
#define TOP_OUTPUT_CHROMA_BASE_ADDR_WIDTH               (32)
#define TOP_OUTPUT_CHROMA_BASE_ADDR_MASK                (0xFFFFFFFF)

/**
* Register : LUMA_SRC_PITCH
* Luma source pitch
*/

#define TOP_LUMA_SRC_PITCH_OFFSET                       (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x30)
#define TOP_LUMA_SRC_PITCH_MASK                         (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* For sources stored in raster format, it directly represents the distance inside the memory, in byte unit, between two vertically adjacent pixels of the same field (if interlaced source) or frame (if progressive source). It is therefore typically equal to the line length for a progressive source, to twice the line length for an interlaced source stored in interleaved frame buffer, or again to the line length in case of interlaced source stored in distinct field buffers; but may include a multiplying factor to enable skipping lines (crude vertical decimation). For sources stored in macroblock format, it is irrelevant.
*/

#define TOP_LUMA_SRC_PITCH_RASTERSRCPITCH_SHIFT         (0x00000000)
#define TOP_LUMA_SRC_PITCH_RASTERSRCPITCH_WIDTH         (16)
#define TOP_LUMA_SRC_PITCH_RASTERSRCPITCH_MASK          (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* For sources stored in macroblock format, it is the number of lines in the frame buffer between vertically adjacent pixels of the picture. It is therefore typically equal to 1 for a progressive source, to 2 for an interlaced source (always stored in multiplexed macroblock frame buffer); but may include a multiplying factor to enable skipping lines. The megacell firmware will use this information together with PictureSize to compute the required address jumps in the frame buffer. For sources stored in raster format or compressed, it is irrelevant.
*/

#define TOP_LUMA_SRC_PITCH_MBSRCPITCH_SHIFT             (0x00000010)
#define TOP_LUMA_SRC_PITCH_MBSRCPITCH_WIDTH             (4)
#define TOP_LUMA_SRC_PITCH_MBSRCPITCH_MASK              (0x000F0000)

/**
* Register : LUMA_ENH_SRC_PITCH
* Luma enhanced source pitch
*/

#define TOP_LUMA_ENH_SRC_PITCH_OFFSET                   (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x34)
#define TOP_LUMA_ENH_SRC_PITCH_MASK                     (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* see LUMA_SRC_PITCH
*/

#define TOP_LUMA_ENH_SRC_PITCH_RASTERSRCPITCH_SHIFT     (0x00000000)
#define TOP_LUMA_ENH_SRC_PITCH_RASTERSRCPITCH_WIDTH     (16)
#define TOP_LUMA_ENH_SRC_PITCH_RASTERSRCPITCH_MASK      (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* see LUMA_SRC_PITCH
*/

#define TOP_LUMA_ENH_SRC_PITCH_MBSRCPITCH_SHIFT         (0x00000010)
#define TOP_LUMA_ENH_SRC_PITCH_MBSRCPITCH_WIDTH         (4)
#define TOP_LUMA_ENH_SRC_PITCH_MBSRCPITCH_MASK          (0x000F0000)

/**
* Register : LUMA_RIGHT_SRC_PITCH
* Luma right source pitch
*/

#define TOP_LUMA_RIGHT_SRC_PITCH_OFFSET                 (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x38)
#define TOP_LUMA_RIGHT_SRC_PITCH_MASK                   (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* see LUMA_SRC_PITCH
*/

#define TOP_LUMA_RIGHT_SRC_PITCH_RASTERSRCPITCH_SHIFT   (0x00000000)
#define TOP_LUMA_RIGHT_SRC_PITCH_RASTERSRCPITCH_WIDTH   (16)
#define TOP_LUMA_RIGHT_SRC_PITCH_RASTERSRCPITCH_MASK    (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* see LUMA_SRC_PITCH
*/

#define TOP_LUMA_RIGHT_SRC_PITCH_MBSRCPITCH_SHIFT       (0x00000010)
#define TOP_LUMA_RIGHT_SRC_PITCH_MBSRCPITCH_WIDTH       (4)
#define TOP_LUMA_RIGHT_SRC_PITCH_MBSRCPITCH_MASK        (0x000F0000)

/**
* Register : LUMA_ENH_RIGHT_SRC_PITCH
* Luma enhanced right source pitch
*/

#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_OFFSET             (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x3C)
#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_MASK               (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* see LUMA_SRC_PITCH
*/

#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_RASTERSRCPITCH_SHIFT (0x00000000)
#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_RASTERSRCPITCH_WIDTH (16)
#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_RASTERSRCPITCH_MASK (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* see LUMA_SRC_PITCH
*/

#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_MBSRCPITCH_SHIFT   (0x00000010)
#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_MBSRCPITCH_WIDTH   (4)
#define TOP_LUMA_ENH_RIGHT_SRC_PITCH_MBSRCPITCH_MASK    (0x000F0000)

/**
* Register : CHROMA_SRC_PITCH
* Chroma source pitch
*/

#define TOP_CHROMA_SRC_PITCH_OFFSET                     (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x40)
#define TOP_CHROMA_SRC_PITCH_MASK                       (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* For sources stored in raster format, it directly represents the distance inside the memory, in byte unit, between two vertically adjacent pixels of the same field (if interlaced source) or frame (if progressive source). It is therefore typically equal to the line length for a progressive source, to twice the line length for an interlaced source stored in interleaved frame buffer, or again to the line length in case of interlaced source stored in distinct field buffers; but may include a multiplying factor to enable skipping lines (crude vertical decimation). For sources stored in macroblock format, it is irrelevant.
*/

#define TOP_CHROMA_SRC_PITCH_RASTERSRCPITCH_SHIFT       (0x00000000)
#define TOP_CHROMA_SRC_PITCH_RASTERSRCPITCH_WIDTH       (16)
#define TOP_CHROMA_SRC_PITCH_RASTERSRCPITCH_MASK        (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* For sources stored in macroblock format, it is the number of lines in the frame buffer between vertically adjacent pixels of the picture. It is therefore typically equal to 1 for a progressive source, to 2 for an interlaced source (always stored in multiplexed macroblock frame buffer); but may include a multiplying factor to enable skipping lines. The megacell firmware will use this information together with PictureSize to compute the required address jumps in the frame buffer. For sources stored in raster format or compressed, it is irrelevant.
*/

#define TOP_CHROMA_SRC_PITCH_MBSRCPITCH_SHIFT           (0x00000010)
#define TOP_CHROMA_SRC_PITCH_MBSRCPITCH_WIDTH           (4)
#define TOP_CHROMA_SRC_PITCH_MBSRCPITCH_MASK            (0x000F0000)

/**
* Register : CHROMA_ENH_SRC_PITCH
* Chroma enhanced source pitch
*/

#define TOP_CHROMA_ENH_SRC_PITCH_OFFSET                 (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x44)
#define TOP_CHROMA_ENH_SRC_PITCH_MASK                   (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* see CHROMA_SRC_PITCH
*/

#define TOP_CHROMA_ENH_SRC_PITCH_RASTERSRCPITCH_SHIFT   (0x00000000)
#define TOP_CHROMA_ENH_SRC_PITCH_RASTERSRCPITCH_WIDTH   (16)
#define TOP_CHROMA_ENH_SRC_PITCH_RASTERSRCPITCH_MASK    (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* see CHROMA_SRC_PITCH
*/

#define TOP_CHROMA_ENH_SRC_PITCH_MBSRCPITCH_SHIFT       (0x00000010)
#define TOP_CHROMA_ENH_SRC_PITCH_MBSRCPITCH_WIDTH       (4)
#define TOP_CHROMA_ENH_SRC_PITCH_MBSRCPITCH_MASK        (0x000F0000)

/**
* Register : CHROMA_RIGHT_SRC_PITCH
* Chroma right source pitch
*/

#define TOP_CHROMA_RIGHT_SRC_PITCH_OFFSET               (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x48)
#define TOP_CHROMA_RIGHT_SRC_PITCH_MASK                 (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* see CHROMA_SRC_PITCH
*/

#define TOP_CHROMA_RIGHT_SRC_PITCH_RASTERSRCPITCH_SHIFT (0x00000000)
#define TOP_CHROMA_RIGHT_SRC_PITCH_RASTERSRCPITCH_WIDTH (16)
#define TOP_CHROMA_RIGHT_SRC_PITCH_RASTERSRCPITCH_MASK  (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* see CHROMA_SRC_PITCH
*/

#define TOP_CHROMA_RIGHT_SRC_PITCH_MBSRCPITCH_SHIFT     (0x00000010)
#define TOP_CHROMA_RIGHT_SRC_PITCH_MBSRCPITCH_WIDTH     (4)
#define TOP_CHROMA_RIGHT_SRC_PITCH_MBSRCPITCH_MASK      (0x000F0000)

/**
* Register : CHROMA_ENH_RIGHT_SRC_PITCH
* Chroma enhanced right source pitch
*/

#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_OFFSET           (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x4C)
#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_MASK             (0x000FFFFF)

/**
* Bit-field : RASTERSRCPITCH
* see CHROMA_SRC_PITCH
*/

#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_RASTERSRCPITCH_SHIFT (0x00000000)
#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_RASTERSRCPITCH_WIDTH (16)
#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_RASTERSRCPITCH_MASK (0x0000FFFF)

/**
* Bit-field : MBSRCPITCH
* see CHROMA_SRC_PITCH
*/

#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_MBSRCPITCH_SHIFT (0x00000010)
#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_MBSRCPITCH_WIDTH (4)
#define TOP_CHROMA_ENH_RIGHT_SRC_PITCH_MBSRCPITCH_MASK  (0x000F0000)

/**
* Register : LUMA_PROCESSED_PITCH
* Luma processed pitch
*/

#define TOP_LUMA_PROCESSED_PITCH_OFFSET                 (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x50)
#define TOP_LUMA_PROCESSED_PITCH_MASK                   (0x0000FFFF)

/**
* Bit-field : PROCESSEDPITCH
* This parameter specifies the pitch for the processed luma pictures, as stored in system memory by the megacell. A processed luma picture can only be stored in raster format, and ProcessedPitch[13:0] specifies the distance inside the memory, in byte unit, between two vertically adjacent pixels of the same frame (as the outputs is always in progressive).
*/

#define TOP_LUMA_PROCESSED_PITCH_PROCESSEDPITCH_SHIFT   (0x00000000)
#define TOP_LUMA_PROCESSED_PITCH_PROCESSEDPITCH_WIDTH   (16)
#define TOP_LUMA_PROCESSED_PITCH_PROCESSEDPITCH_MASK    (0x0000FFFF)

/**
* Register : CHROMA_PROCESSED_PITCH
* Chroma processed pitch
*/

#define TOP_CHROMA_PROCESSED_PITCH_OFFSET               (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x54)
#define TOP_CHROMA_PROCESSED_PITCH_MASK                 (0x0000FFFF)

/**
* Bit-field : PROCESSEDPITCH
* This parameter specifies the pitch for the processed pictures, as stored in system memory by the megacell. A processed picture can only be stored in raster format, and ProcessedPitch[13:0] specifies the distance inside the memory, in byte unit, between two vertically adjacent pixels of the same frame (as the outputs is always in progressive).
*/

#define TOP_CHROMA_PROCESSED_PITCH_PROCESSEDPITCH_SHIFT (0x00000000)
#define TOP_CHROMA_PROCESSED_PITCH_PROCESSEDPITCH_WIDTH (16)
#define TOP_CHROMA_PROCESSED_PITCH_PROCESSEDPITCH_MASK  (0x0000FFFF)

/**
* Register : INPUT_FRAME_SIZE
* Input picture size
*/

#define TOP_INPUT_FRAME_SIZE_OFFSET                     (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x58)
#define TOP_INPUT_FRAME_SIZE_MASK                       (0x0FFF1FFF)

/**
* Bit-field : WIDTH
* it contains the width of the input viewport, expressed in pixels.
* The min values for this parameters must be equal to:
* Min input width: 48 pixels or 1/8 of output width, largest of the two values.
*/

#define TOP_INPUT_FRAME_SIZE_WIDTH_SHIFT                (0x00000000)
#define TOP_INPUT_FRAME_SIZE_WIDTH_WIDTH                (13)
#define TOP_INPUT_FRAME_SIZE_WIDTH_MASK                 (0x00001FFF)

/**
* Bit-field : HEIGHT
* it contains the height of the input picture, expressed in pixels, whatever the value of PROGRESSIVE.
* The min values for this parameters must be equal to:
* Min input height: 16 pixels or 1/8 of output height, largest of the two values.
*/

#define TOP_INPUT_FRAME_SIZE_HEIGHT_SHIFT               (0x00000010)
#define TOP_INPUT_FRAME_SIZE_HEIGHT_WIDTH               (12)
#define TOP_INPUT_FRAME_SIZE_HEIGHT_MASK                (0x0FFF0000)

/**
* Register : INPUT_VIEWPORT_ORI
* Viewport origin
*/

#define TOP_INPUT_VIEWPORT_ORI_OFFSET                   (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x5C)
#define TOP_INPUT_VIEWPORT_ORI_MASK                     (0x0FFF1FFF)

/**
* Bit-field : VIEWPORTX
* it contains the coordinate 'X' of the input viewport left corner, expressed in pixels.
*/

#define TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_SHIFT          (0x00000000)
#define TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_WIDTH          (13)
#define TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_MASK           (0x00001FFF)

/**
* Bit-field : VIEWPORTY
* it contains the coordinate of the input viewport top corner, expressed in pixels..
*/

#define TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_SHIFT          (0x00000010)
#define TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_WIDTH          (12)
#define TOP_INPUT_VIEWPORT_ORI_VIEWPORTY_MASK           (0x0FFF0000)

/**
* Register : INPUT_VIEWPORT_ORI_RIGHT
* Viewport origin Right
*/

#define TOP_INPUT_VIEWPORT_ORI_RIGHT_OFFSET             (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x60)
#define TOP_INPUT_VIEWPORT_ORI_RIGHT_MASK               (0x0FFF1FFF)

/**
* Bit-field : VIEWPORTX
* it contains the coordinate 'X' of the input viewport left corner, expressed in pixels from the upper left corner of the right side
*/

#define TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_SHIFT    (0x00000000)
#define TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_WIDTH    (13)
#define TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTX_MASK     (0x00001FFF)

/**
* Bit-field : VIEWPORTY
* it contains the coordinate of the input viewport top corner, expressed in pixels from the upper left corner of the right side..
*/

#define TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_SHIFT    (0x00000010)
#define TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_WIDTH    (12)
#define TOP_INPUT_VIEWPORT_ORI_RIGHT_VIEWPORTY_MASK     (0x0FFF0000)

/**
* Register : INPUT_VIEWPORT_SIZE
* Viewport size
*/

#define TOP_INPUT_VIEWPORT_SIZE_OFFSET                  (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x64)
#define TOP_INPUT_VIEWPORT_SIZE_MASK                    (0x0FFF1FFF)

/**
* Bit-field : WIDTH
* it contains the width of the input viewport, expressed in pixels.
* The min values for this parameters must be equal to:
* Min input width: 48 pixels or 1/8 of output width, largest of the two values.
*/

#define TOP_INPUT_VIEWPORT_SIZE_WIDTH_SHIFT             (0x00000000)
#define TOP_INPUT_VIEWPORT_SIZE_WIDTH_WIDTH             (13)
#define TOP_INPUT_VIEWPORT_SIZE_WIDTH_MASK              (0x00001FFF)

/**
* Bit-field : HEIGHT
* it contains the height of the input viewport, expressed in pixels.
* The min values for this parameters must be equal to:
* Min input height: 16 pixels or 1/8 of output height, largest of the two values.
*/

#define TOP_INPUT_VIEWPORT_SIZE_HEIGHT_SHIFT            (0x00000010)
#define TOP_INPUT_VIEWPORT_SIZE_HEIGHT_WIDTH            (12)
#define TOP_INPUT_VIEWPORT_SIZE_HEIGHT_MASK             (0x0FFF0000)

/**
* Register : LEFT_VIEW_BORDER_WIDTH
* Left view border width
*/

#define TOP_LEFT_VIEW_BORDER_WIDTH_OFFSET               (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x68)
#define TOP_LEFT_VIEW_BORDER_WIDTH_MASK                 (0x07FF07FF)

/**
* Bit-field : RIGHT
* This is the width of the right handside border affter rescale for 4/3 to 16/9 conversion.
*/

#define TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_SHIFT          (0x00000000)
#define TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_WIDTH          (11)
#define TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_MASK           (0x000007FF)

/**
* Bit-field : LEFT
* This is the width of the left handside border affter rescale for 4/3 to 16/9 conversion.
*/

#define TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_SHIFT           (0x00000010)
#define TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_WIDTH           (11)
#define TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_MASK            (0x07FF0000)

/**
* Register : RIGHT_VIEW_BORDER_WIDTH
* Right view border width
*/

#define TOP_RIGHT_VIEW_BORDER_WIDTH_OFFSET              (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x6C)
#define TOP_RIGHT_VIEW_BORDER_WIDTH_MASK                (0x07FF07FF)

/**
* Bit-field : RIGHT
* This is the width of the right handside border affter rescale for 4/3 to 16/9 conversion.
*/

#define TOP_RIGHT_VIEW_BORDER_WIDTH_RIGHT_SHIFT         (0x00000000)
#define TOP_RIGHT_VIEW_BORDER_WIDTH_RIGHT_WIDTH         (11)
#define TOP_RIGHT_VIEW_BORDER_WIDTH_RIGHT_MASK          (0x000007FF)

/**
* Bit-field : LEFT
* This is the width of the left handside border affter rescale for 4/3 to 16/9 conversion.
*/

#define TOP_RIGHT_VIEW_BORDER_WIDTH_LEFT_SHIFT          (0x00000010)
#define TOP_RIGHT_VIEW_BORDER_WIDTH_LEFT_WIDTH          (11)
#define TOP_RIGHT_VIEW_BORDER_WIDTH_LEFT_MASK           (0x07FF0000)

/**
* Register : LEFT_VIEW_3DOFFSET_WIDTH
* Left view 3D offset width
*/

#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_OFFSET             (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x70)
#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_MASK               (0x07FF07FF)

/**
* Bit-field : RIGHT
* This is the width of the right handside border introduce by 3D offset settings.
*/

#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_RIGHT_SHIFT        (0x00000000)
#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_RIGHT_WIDTH        (11)
#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_RIGHT_MASK         (0x000007FF)

/**
* Bit-field : LEFT
* This is the width of the left handside border introduce by 3D offset settings.
*/

#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_LEFT_SHIFT         (0x00000010)
#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_LEFT_WIDTH         (11)
#define TOP_LEFT_VIEW_3DOFFSET_WIDTH_LEFT_MASK          (0x07FF0000)

/**
* Register : RIGHT_VIEW_3DOFFSET_WIDTH
* Right view 3D offset width
*/

#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_OFFSET            (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x74)
#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_MASK              (0x07FF07FF)

/**
* Bit-field : RIGHT
* This is the width of the right handside border introduce by 3D offset settings before rescale.
*/

#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_RIGHT_SHIFT       (0x00000000)
#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_RIGHT_WIDTH       (11)
#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_RIGHT_MASK        (0x000007FF)

/**
* Bit-field : LEFT
* This is the width of the left handside border introduce by 3D offset settings before rescale.
*/

#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_LEFT_SHIFT        (0x00000010)
#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_LEFT_WIDTH        (11)
#define TOP_RIGHT_VIEW_3DOFFSET_WIDTH_LEFT_MASK         (0x07FF0000)

/**
* Register : SIDE_STRIPE_COLOR
* Side stripe color
*/

#define TOP_SIDE_STRIPE_COLOR_OFFSET                    (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x78)
#define TOP_SIDE_STRIPE_COLOR_MASK                      (0x3FFFFFFF)

/**
* Bit-field : CHROMA_RED
* Cr[9:0]: Default Chroma red data sent to the compositor
*/

#define TOP_SIDE_STRIPE_COLOR_CHROMA_RED_SHIFT          (0x00000000)
#define TOP_SIDE_STRIPE_COLOR_CHROMA_RED_WIDTH          (10)
#define TOP_SIDE_STRIPE_COLOR_CHROMA_RED_MASK           (0x000003FF)

/**
* Bit-field : CHROMA_BLUE
* Cb[9:0]: Default Chroma blue data sent to the compositor
*/

#define TOP_SIDE_STRIPE_COLOR_CHROMA_BLUE_SHIFT         (0x0000000a)
#define TOP_SIDE_STRIPE_COLOR_CHROMA_BLUE_WIDTH         (10)
#define TOP_SIDE_STRIPE_COLOR_CHROMA_BLUE_MASK          (0x000FFC00)

/**
* Bit-field : LUMA_DATA
* Y[9:0]: Default Luma data sent to the compositor
*/

#define TOP_SIDE_STRIPE_COLOR_LUMA_DATA_SHIFT           (0x00000014)
#define TOP_SIDE_STRIPE_COLOR_LUMA_DATA_WIDTH           (10)
#define TOP_SIDE_STRIPE_COLOR_LUMA_DATA_MASK            (0x3FF00000)

/**
* Register : CRC_RESET_CTRL
* Control the initial value of the CRC
*/

#define TOP_CRC_RESET_CTRL_OFFSET                       (c8fvp3_hqvdplite_api_TOP_BASE_ADDR + 0x7C)
#define TOP_CRC_RESET_CTRL_MASK                         (0x007FFFFF)

/**
* Bit-field : INPUT_Y
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
*/

#define TOP_CRC_RESET_CTRL_INPUT_Y_SHIFT                (0x00000000)
#define TOP_CRC_RESET_CTRL_INPUT_Y_WIDTH                (1)
#define TOP_CRC_RESET_CTRL_INPUT_Y_MASK                 (0x00000001)

/**
* Bit-field : INPUT_UV
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
*/

#define TOP_CRC_RESET_CTRL_INPUT_UV_SHIFT               (0x00000001)
#define TOP_CRC_RESET_CTRL_INPUT_UV_WIDTH               (1)
#define TOP_CRC_RESET_CTRL_INPUT_UV_MASK                (0x00000002)

/**
* Bit-field : NEXT_Y_FMD
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and functional uses cases.
*/

#define TOP_CRC_RESET_CTRL_NEXT_Y_FMD_SHIFT             (0x00000002)
#define TOP_CRC_RESET_CTRL_NEXT_Y_FMD_WIDTH             (1)
#define TOP_CRC_RESET_CTRL_NEXT_Y_FMD_MASK              (0x00000004)

/**
* Bit-field : NEXT_NEXT_Y_FMD
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_NEXT_NEXT_Y_FMD_SHIFT        (0x00000003)
#define TOP_CRC_RESET_CTRL_NEXT_NEXT_Y_FMD_WIDTH        (1)
#define TOP_CRC_RESET_CTRL_NEXT_NEXT_Y_FMD_MASK         (0x00000008)

/**
* Bit-field : NEXT_NEXT_NEXT_Y_FMD
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_NEXT_NEXT_NEXT_Y_FMD_SHIFT   (0x00000004)
#define TOP_CRC_RESET_CTRL_NEXT_NEXT_NEXT_Y_FMD_WIDTH   (1)
#define TOP_CRC_RESET_CTRL_NEXT_NEXT_NEXT_Y_FMD_MASK    (0x00000010)

/**
* Bit-field : PREV_Y_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_PREV_Y_CSDI_SHIFT            (0x00000005)
#define TOP_CRC_RESET_CTRL_PREV_Y_CSDI_WIDTH            (1)
#define TOP_CRC_RESET_CTRL_PREV_Y_CSDI_MASK             (0x00000020)

/**
* Bit-field : CUR_Y_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_CUR_Y_CSDI_SHIFT             (0x00000006)
#define TOP_CRC_RESET_CTRL_CUR_Y_CSDI_WIDTH             (1)
#define TOP_CRC_RESET_CTRL_CUR_Y_CSDI_MASK              (0x00000040)

/**
* Bit-field : NEXT_Y_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_NEXT_Y_CSDI_SHIFT            (0x00000007)
#define TOP_CRC_RESET_CTRL_NEXT_Y_CSDI_WIDTH            (1)
#define TOP_CRC_RESET_CTRL_NEXT_Y_CSDI_MASK             (0x00000080)

/**
* Bit-field : PREV_UV_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_PREV_UV_CSDI_SHIFT           (0x00000008)
#define TOP_CRC_RESET_CTRL_PREV_UV_CSDI_WIDTH           (1)
#define TOP_CRC_RESET_CTRL_PREV_UV_CSDI_MASK            (0x00000100)

/**
* Bit-field : CUR_UV_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_CUR_UV_CSDI_SHIFT            (0x00000009)
#define TOP_CRC_RESET_CTRL_CUR_UV_CSDI_WIDTH            (1)
#define TOP_CRC_RESET_CTRL_CUR_UV_CSDI_MASK             (0x00000200)

/**
* Bit-field : NEXT_UV_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_NEXT_UV_CSDI_SHIFT           (0x0000000a)
#define TOP_CRC_RESET_CTRL_NEXT_UV_CSDI_WIDTH           (1)
#define TOP_CRC_RESET_CTRL_NEXT_UV_CSDI_MASK            (0x00000400)

/**
* Bit-field : Y_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_Y_CSDI_SHIFT                 (0x0000000b)
#define TOP_CRC_RESET_CTRL_Y_CSDI_WIDTH                 (1)
#define TOP_CRC_RESET_CTRL_Y_CSDI_MASK                  (0x00000800)

/**
* Bit-field : UV_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_UV_CSDI_SHIFT                (0x0000000c)
#define TOP_CRC_RESET_CTRL_UV_CSDI_WIDTH                (1)
#define TOP_CRC_RESET_CTRL_UV_CSDI_MASK                 (0x00001000)

/**
* Bit-field : UV_CUP
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_UV_CUP_SHIFT                 (0x0000000d)
#define TOP_CRC_RESET_CTRL_UV_CUP_WIDTH                 (1)
#define TOP_CRC_RESET_CTRL_UV_CUP_MASK                  (0x00002000)

/**
* Bit-field : MOT_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_MOT_CSDI_SHIFT               (0x0000000e)
#define TOP_CRC_RESET_CTRL_MOT_CSDI_WIDTH               (1)
#define TOP_CRC_RESET_CTRL_MOT_CSDI_MASK                (0x00004000)

/**
* Bit-field : Y_HVSRC
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_Y_HVSRC_SHIFT                (0x0000000f)
#define TOP_CRC_RESET_CTRL_Y_HVSRC_WIDTH                (1)
#define TOP_CRC_RESET_CTRL_Y_HVSRC_MASK                 (0x00008000)

/**
* Bit-field : U_HVSRC
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_U_HVSRC_SHIFT                (0x00000010)
#define TOP_CRC_RESET_CTRL_U_HVSRC_WIDTH                (1)
#define TOP_CRC_RESET_CTRL_U_HVSRC_MASK                 (0x00010000)

/**
* Bit-field : V_HVSRC
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_V_HVSRC_SHIFT                (0x00000011)
#define TOP_CRC_RESET_CTRL_V_HVSRC_WIDTH                (1)
#define TOP_CRC_RESET_CTRL_V_HVSRC_MASK                 (0x00020000)

/**
* Bit-field : Y_IQI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
*/

#define TOP_CRC_RESET_CTRL_Y_IQI_SHIFT                  (0x00000012)
#define TOP_CRC_RESET_CTRL_Y_IQI_WIDTH                  (1)
#define TOP_CRC_RESET_CTRL_Y_IQI_MASK                   (0x00040000)

/**
* Bit-field : U_IQI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
*/

#define TOP_CRC_RESET_CTRL_U_IQI_SHIFT                  (0x00000013)
#define TOP_CRC_RESET_CTRL_U_IQI_WIDTH                  (1)
#define TOP_CRC_RESET_CTRL_U_IQI_MASK                   (0x00080000)

/**
* Bit-field : V_IQI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
*/

#define TOP_CRC_RESET_CTRL_V_IQI_SHIFT                  (0x00000014)
#define TOP_CRC_RESET_CTRL_V_IQI_WIDTH                  (1)
#define TOP_CRC_RESET_CTRL_V_IQI_MASK                   (0x00100000)

/**
* Bit-field : MOT_CUR_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_MOT_CUR_CSDI_SHIFT           (0x00000015)
#define TOP_CRC_RESET_CTRL_MOT_CUR_CSDI_WIDTH           (1)
#define TOP_CRC_RESET_CTRL_MOT_CUR_CSDI_MASK            (0x00200000)

/**
* Bit-field : MOT_PRV_CSDI
* If set to 1, reset the CRC_VALUE. Otherwise, keep the old value.
* Dedicated for verification step. This bit is irrelevant for application and validation step.
*/

#define TOP_CRC_RESET_CTRL_MOT_PRV_CSDI_SHIFT           (0x00000016)
#define TOP_CRC_RESET_CTRL_MOT_PRV_CSDI_WIDTH           (1)
#define TOP_CRC_RESET_CTRL_MOT_PRV_CSDI_MASK            (0x00400000)


#ifndef HQVDPLITE_API_FOR_STAPI
typedef struct {
    gvh_u32_t CONFIG;  /* at 0 */
    gvh_u32_t MEM_FORMAT;  /* at 4 */
    gvh_u32_t CURRENT_LUMA;  /* at 8 */
    gvh_u32_t CURRENT_ENH_LUMA;  /* at 12 */
    gvh_u32_t CURRENT_RIGHT_LUMA;  /* at 16 */
    gvh_u32_t CURRENT_ENH_RIGHT_LUMA;  /* at 20 */
    gvh_u32_t CURRENT_CHROMA;  /* at 24 */
    gvh_u32_t CURRENT_ENH_CHROMA;  /* at 28 */
    gvh_u32_t CURRENT_RIGHT_CHROMA;  /* at 32 */
    gvh_u32_t CURRENT_ENH_RIGHT_CHROMA;  /* at 36 */
    gvh_u32_t OUTPUT_LUMA;  /* at 40 */
    gvh_u32_t OUTPUT_CHROMA;  /* at 44 */
    gvh_u32_t LUMA_SRC_PITCH;  /* at 48 */
    gvh_u32_t LUMA_ENH_SRC_PITCH;  /* at 52 */
    gvh_u32_t LUMA_RIGHT_SRC_PITCH;  /* at 56 */
    gvh_u32_t LUMA_ENH_RIGHT_SRC_PITCH;  /* at 60 */
    gvh_u32_t CHROMA_SRC_PITCH;  /* at 64 */
    gvh_u32_t CHROMA_ENH_SRC_PITCH;  /* at 68 */
    gvh_u32_t CHROMA_RIGHT_SRC_PITCH;  /* at 72 */
    gvh_u32_t CHROMA_ENH_RIGHT_SRC_PITCH;  /* at 76 */
    gvh_u32_t LUMA_PROCESSED_PITCH;  /* at 80 */
    gvh_u32_t CHROMA_PROCESSED_PITCH;  /* at 84 */
    gvh_u32_t INPUT_FRAME_SIZE;  /* at 88 */
    gvh_u32_t INPUT_VIEWPORT_ORI;  /* at 92 */
    gvh_u32_t INPUT_VIEWPORT_ORI_RIGHT;  /* at 96 */
    gvh_u32_t INPUT_VIEWPORT_SIZE;  /* at 100 */
    gvh_u32_t LEFT_VIEW_BORDER_WIDTH;  /* at 104 */
    gvh_u32_t RIGHT_VIEW_BORDER_WIDTH;  /* at 108 */
    gvh_u32_t LEFT_VIEW_3DOFFSET_WIDTH;  /* at 112 */
    gvh_u32_t RIGHT_VIEW_3DOFFSET_WIDTH;  /* at 116 */
    gvh_u32_t SIDE_STRIPE_COLOR;  /* at 120 */
    gvh_u32_t CRC_RESET_CTRL;  /* at 124 */
} s_TOP;
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70

typedef struct {
uint32_t Config;  /* at 0 */
uint32_t MemFormat;  /* at 4 */
uint32_t CurrentLuma;  /* at 8 */
uint32_t CurrentEnhLuma;  /* at 12 */
uint32_t CurrentRightLuma;  /* at 16 */
uint32_t CurrentEnhRightLuma;  /* at 20 */
uint32_t CurrentChroma;  /* at 24 */
uint32_t CurrentEnhChroma;  /* at 28 */
uint32_t CurrentRightChroma;  /* at 32 */
uint32_t CurrentEnhRightChroma;  /* at 36 */
uint32_t OutputLuma;  /* at 40 */
uint32_t OutputChroma;  /* at 44 */
uint32_t LumaSrcPitch;  /* at 48 */
uint32_t LumaEnhSrcPitch;  /* at 52 */
uint32_t LumaRightSrcPitch;  /* at 56 */
uint32_t LumaEnhRightSrcPitch;  /* at 60 */
uint32_t ChromaSrcPitch;  /* at 64 */
uint32_t ChromaEnhSrcPitch;  /* at 68 */
uint32_t ChromaRightSrcPitch;  /* at 72 */
uint32_t ChromaEnhRightSrcPitch;  /* at 76 */
uint32_t LumaProcessedPitch;  /* at 80 */
uint32_t ChromaProcessedPitch;  /* at 84 */
uint32_t InputFrameSize;  /* at 88 */
uint32_t InputViewportOri;  /* at 92 */
uint32_t InputViewportOriRight;  /* at 96 */
uint32_t InputViewportSize;  /* at 100 */
uint32_t LeftViewBorderWidth;  /* at 104 */
uint32_t RightViewBorderWidth;  /* at 108 */
uint32_t LeftView3doffsetWidth;  /* at 112 */
uint32_t RightView3doffsetWidth;  /* at 116 */
uint32_t SideStripeColor;  /* at 120 */
uint32_t CrcResetCtrl;  /* at 124 */
} HQVDPLITE_TOP_Params_t;
#endif /* FW_STXP70 */
#endif
