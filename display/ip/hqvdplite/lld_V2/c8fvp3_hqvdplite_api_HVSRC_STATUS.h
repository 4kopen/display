/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_HVSRC_STATUS_H
#define _C8FVP3_HQVDPLITE_API_HVSRC_STATUS_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : HVSRC_STATUS
*/


/**
* Register : Y_HVSRC_CRC
* CRC value luma rescaled field
*/

#define HVSRC_STATUS_Y_HVSRC_CRC_OFFSET                 (c8fvp3_hqvdplite_api_HVSRC_STATUS_BASE_ADDR + 0x00)
#define HVSRC_STATUS_Y_HVSRC_CRC_MASK                   (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define HVSRC_STATUS_Y_HVSRC_CRC_CRC_RWP_SHIFT          (0x00000000)
#define HVSRC_STATUS_Y_HVSRC_CRC_CRC_RWP_WIDTH          (32)
#define HVSRC_STATUS_Y_HVSRC_CRC_CRC_RWP_MASK           (0xFFFFFFFF)

/**
* Register : U_HVSRC_CRC
* CRC value chroma U rescaled field
*/

#define HVSRC_STATUS_U_HVSRC_CRC_OFFSET                 (c8fvp3_hqvdplite_api_HVSRC_STATUS_BASE_ADDR + 0x04)
#define HVSRC_STATUS_U_HVSRC_CRC_MASK                   (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define HVSRC_STATUS_U_HVSRC_CRC_CRC_RWP_SHIFT          (0x00000000)
#define HVSRC_STATUS_U_HVSRC_CRC_CRC_RWP_WIDTH          (32)
#define HVSRC_STATUS_U_HVSRC_CRC_CRC_RWP_MASK           (0xFFFFFFFF)

/**
* Register : V_HVSRC_CRC
* CRC value chroma V rescaled field
*/

#define HVSRC_STATUS_V_HVSRC_CRC_OFFSET                 (c8fvp3_hqvdplite_api_HVSRC_STATUS_BASE_ADDR + 0x08)
#define HVSRC_STATUS_V_HVSRC_CRC_MASK                   (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define HVSRC_STATUS_V_HVSRC_CRC_CRC_RWP_SHIFT          (0x00000000)
#define HVSRC_STATUS_V_HVSRC_CRC_CRC_RWP_WIDTH          (32)
#define HVSRC_STATUS_V_HVSRC_CRC_CRC_RWP_MASK           (0xFFFFFFFF)


#ifndef HQVDPLITE_API_FOR_STAPI
#ifdef FW_STXP70
typedef struct {
    gvh_u32_t Y_HVSRC_CRC;  /* at 0 */
    gvh_u32_t Pad1;
    gvh_u32_t Pad2;
    gvh_u32_t Pad3;
    gvh_u32_t U_HVSRC_CRC;  /* at 4 */
    gvh_u32_t Pad4;
    gvh_u32_t Pad5;
    gvh_u32_t Pad6;
    gvh_u32_t V_HVSRC_CRC;  /* at 8 */
    gvh_u32_t Pad7;
    gvh_u32_t Pad8;
    gvh_u32_t Pad9;
} s_HVSRC_STATUS;
#else
typedef struct {
    gvh_u32_t Y_HVSRC_CRC;  /* at 0 */
    gvh_u32_t U_HVSRC_CRC;  /* at 4 */
    gvh_u32_t V_HVSRC_CRC;  /* at 8 */
} s_HVSRC_STATUS;
#endif
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t YHvsrcCrc;  /* at 0 */
uint32_t UHvsrcCrc;  /* at 4 */
uint32_t VHvsrcCrc;  /* at 8 */
} HQVDPLITE_HVSRCStatus_Params_t;
#endif /* FW_STXP70 */
#endif
