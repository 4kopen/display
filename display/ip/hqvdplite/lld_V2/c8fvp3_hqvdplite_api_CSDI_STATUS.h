/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_CSDI_STATUS_H
#define _C8FVP3_HQVDPLITE_API_CSDI_STATUS_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : CSDI_STATUS
*/


/**
* Register : PREV_Y_CSDI_CRC
* CRC value luma previous field to CSDI
*/

#define CSDI_STATUS_PREV_Y_CSDI_CRC_OFFSET              (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x00)
#define CSDI_STATUS_PREV_Y_CSDI_CRC_MASK                (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_PREV_Y_CSDI_CRC_CRC_RWP_SHIFT       (0x00000000)
#define CSDI_STATUS_PREV_Y_CSDI_CRC_CRC_RWP_WIDTH       (32)
#define CSDI_STATUS_PREV_Y_CSDI_CRC_CRC_RWP_MASK        (0xFFFFFFFF)

/**
* Register : CUR_Y_CSDI_CRC
* CRC value luma current field to CSDI
*/

#define CSDI_STATUS_CUR_Y_CSDI_CRC_OFFSET               (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x04)
#define CSDI_STATUS_CUR_Y_CSDI_CRC_MASK                 (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_CUR_Y_CSDI_CRC_CRC_RWP_SHIFT        (0x00000000)
#define CSDI_STATUS_CUR_Y_CSDI_CRC_CRC_RWP_WIDTH        (32)
#define CSDI_STATUS_CUR_Y_CSDI_CRC_CRC_RWP_MASK         (0xFFFFFFFF)

/**
* Register : NEXT_Y_CSDI_CRC
* CRC value luma next field to CSDI
*/

#define CSDI_STATUS_NEXT_Y_CSDI_CRC_OFFSET              (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x08)
#define CSDI_STATUS_NEXT_Y_CSDI_CRC_MASK                (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_NEXT_Y_CSDI_CRC_CRC_RWP_SHIFT       (0x00000000)
#define CSDI_STATUS_NEXT_Y_CSDI_CRC_CRC_RWP_WIDTH       (32)
#define CSDI_STATUS_NEXT_Y_CSDI_CRC_CRC_RWP_MASK        (0xFFFFFFFF)

/**
* Register : PREV_UV_CSDI_CRC
* CRC value chroma previous field to CSDI
*/

#define CSDI_STATUS_PREV_UV_CSDI_CRC_OFFSET             (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x0C)
#define CSDI_STATUS_PREV_UV_CSDI_CRC_MASK               (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_PREV_UV_CSDI_CRC_CRC_RWP_SHIFT      (0x00000000)
#define CSDI_STATUS_PREV_UV_CSDI_CRC_CRC_RWP_WIDTH      (32)
#define CSDI_STATUS_PREV_UV_CSDI_CRC_CRC_RWP_MASK       (0xFFFFFFFF)

/**
* Register : CUR_UV_CSDI_CRC
* CRC value chroma current field to CSDI
*/

#define CSDI_STATUS_CUR_UV_CSDI_CRC_OFFSET              (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x10)
#define CSDI_STATUS_CUR_UV_CSDI_CRC_MASK                (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_CUR_UV_CSDI_CRC_CRC_RWP_SHIFT       (0x00000000)
#define CSDI_STATUS_CUR_UV_CSDI_CRC_CRC_RWP_WIDTH       (32)
#define CSDI_STATUS_CUR_UV_CSDI_CRC_CRC_RWP_MASK        (0xFFFFFFFF)

/**
* Register : NEXT_UV_CSDI_CRC
* CRC value chroma next field to CSDI
*/

#define CSDI_STATUS_NEXT_UV_CSDI_CRC_OFFSET             (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x14)
#define CSDI_STATUS_NEXT_UV_CSDI_CRC_MASK               (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_NEXT_UV_CSDI_CRC_CRC_RWP_SHIFT      (0x00000000)
#define CSDI_STATUS_NEXT_UV_CSDI_CRC_CRC_RWP_WIDTH      (32)
#define CSDI_STATUS_NEXT_UV_CSDI_CRC_CRC_RWP_MASK       (0xFFFFFFFF)

/**
* Register : Y_CSDI_CRC
* CRC value luma deinterlaced field
*/

#define CSDI_STATUS_Y_CSDI_CRC_OFFSET                   (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x18)
#define CSDI_STATUS_Y_CSDI_CRC_MASK                     (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_Y_CSDI_CRC_CRC_RWP_SHIFT            (0x00000000)
#define CSDI_STATUS_Y_CSDI_CRC_CRC_RWP_WIDTH            (32)
#define CSDI_STATUS_Y_CSDI_CRC_CRC_RWP_MASK             (0xFFFFFFFF)

/**
* Register : UV_CSDI_CRC
* CRC value chroma deinterlaced field
*/

#define CSDI_STATUS_UV_CSDI_CRC_OFFSET                  (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x1C)
#define CSDI_STATUS_UV_CSDI_CRC_MASK                    (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_UV_CSDI_CRC_CRC_RWP_SHIFT           (0x00000000)
#define CSDI_STATUS_UV_CSDI_CRC_CRC_RWP_WIDTH           (32)
#define CSDI_STATUS_UV_CSDI_CRC_CRC_RWP_MASK            (0xFFFFFFFF)

/**
* Register : UV_CUP_CRC
* CRC value chroma deinterlaced field after upsampling
*/

#define CSDI_STATUS_UV_CUP_CRC_OFFSET                   (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x20)
#define CSDI_STATUS_UV_CUP_CRC_MASK                     (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_UV_CUP_CRC_CRC_RWP_SHIFT            (0x00000000)
#define CSDI_STATUS_UV_CUP_CRC_CRC_RWP_WIDTH            (32)
#define CSDI_STATUS_UV_CUP_CRC_CRC_RWP_MASK             (0xFFFFFFFF)

/**
* Register : MOT_CSDI_CRC
* CRC value motion CSDI output field
*/

#define CSDI_STATUS_MOT_CSDI_CRC_OFFSET                 (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x24)
#define CSDI_STATUS_MOT_CSDI_CRC_MASK                   (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_MOT_CSDI_CRC_CRC_RWP_SHIFT          (0x00000000)
#define CSDI_STATUS_MOT_CSDI_CRC_CRC_RWP_WIDTH          (32)
#define CSDI_STATUS_MOT_CSDI_CRC_CRC_RWP_MASK           (0xFFFFFFFF)

/**
* Register : MOT_CUR_CSDI_CRC
* CRC value current motion CSDI input field
*/

#define CSDI_STATUS_MOT_CUR_CSDI_CRC_OFFSET             (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x28)
#define CSDI_STATUS_MOT_CUR_CSDI_CRC_MASK               (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_MOT_CUR_CSDI_CRC_CRC_RWP_SHIFT      (0x00000000)
#define CSDI_STATUS_MOT_CUR_CSDI_CRC_CRC_RWP_WIDTH      (32)
#define CSDI_STATUS_MOT_CUR_CSDI_CRC_CRC_RWP_MASK       (0xFFFFFFFF)

/**
* Register : MOT_PREV_CSDI_CRC
* CRC value previous motion CSDI input field
*/

#define CSDI_STATUS_MOT_PREV_CSDI_CRC_OFFSET            (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR + 0x2C)
#define CSDI_STATUS_MOT_PREV_CSDI_CRC_MASK              (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer.
* Not used during application and validation step.
*/

#define CSDI_STATUS_MOT_PREV_CSDI_CRC_CRC_RWP_SHIFT     (0x00000000)
#define CSDI_STATUS_MOT_PREV_CSDI_CRC_CRC_RWP_WIDTH     (32)
#define CSDI_STATUS_MOT_PREV_CSDI_CRC_CRC_RWP_MASK      (0xFFFFFFFF)


#ifndef HQVDPLITE_API_FOR_STAPI
#ifdef FW_STXP70
typedef struct {
  gvh_u32_t PREV_Y_CSDI_CRC;  /* at 0 */
  gvh_u32_t Pad1;
  gvh_u32_t Pad2;
  gvh_u32_t Pad3;
  gvh_u32_t CUR_Y_CSDI_CRC;  /* at 4 */
  gvh_u32_t Pad4;
  gvh_u32_t Pad5;
  gvh_u32_t Pad6;
  gvh_u32_t NEXT_Y_CSDI_CRC;  /* at 8 */
  gvh_u32_t Pad7;
  gvh_u32_t Pad8;
  gvh_u32_t Pad9;
  gvh_u32_t PREV_UV_CSDI_CRC;  /* at 12 */
  gvh_u32_t Pad10;
  gvh_u32_t Pad11;
  gvh_u32_t Pad12;
  gvh_u32_t CUR_UV_CSDI_CRC;  /* at 16 */
  gvh_u32_t Pad13;
  gvh_u32_t Pad14;
  gvh_u32_t Pad15;
  gvh_u32_t NEXT_UV_CSDI_CRC;  /* at 20 */
  gvh_u32_t Pad16;
  gvh_u32_t Pad17;
  gvh_u32_t Pad18;
  gvh_u32_t Y_CSDI_CRC;  /* at 24 */
  gvh_u32_t Pad19;
  gvh_u32_t Pad20;
  gvh_u32_t Pad21;
  gvh_u32_t UV_CSDI_CRC;  /* at 28 */
  gvh_u32_t Pad22;
  gvh_u32_t Pad23;
  gvh_u32_t Pad24;
  gvh_u32_t UV_CUP_CRC;  /* at 32 */
  gvh_u32_t Pad25;
  gvh_u32_t Pad26;
  gvh_u32_t Pad27;
  gvh_u32_t MOT_CSDI_CRC;  /* at 36 */
  gvh_u32_t Pad28;
  gvh_u32_t Pad29;
  gvh_u32_t Pad30;
  gvh_u32_t MOT_CUR_CSDI_CRC;  /* at 40 */
  gvh_u32_t Pad31;
  gvh_u32_t Pad32;
  gvh_u32_t Pad33;
  gvh_u32_t MOT_PREV_CSDI_CRC;  /* at 44 */
  gvh_u32_t Pad34;
  gvh_u32_t Pad35;
  gvh_u32_t Pad36;
} s_CSDI_STATUS;
#else
typedef struct {
    gvh_u32_t PREV_Y_CSDI_CRC;  /* at 0 */
    gvh_u32_t CUR_Y_CSDI_CRC;  /* at 4 */
    gvh_u32_t NEXT_Y_CSDI_CRC;  /* at 8 */
    gvh_u32_t PREV_UV_CSDI_CRC;  /* at 12 */
    gvh_u32_t CUR_UV_CSDI_CRC;  /* at 16 */
    gvh_u32_t NEXT_UV_CSDI_CRC;  /* at 20 */
    gvh_u32_t Y_CSDI_CRC;  /* at 24 */
    gvh_u32_t UV_CSDI_CRC;  /* at 28 */
    gvh_u32_t UV_CUP_CRC;  /* at 32 */
    gvh_u32_t MOT_CSDI_CRC;  /* at 36 */
    gvh_u32_t MOT_CUR_CSDI_CRC;  /* at 40 */
    gvh_u32_t MOT_PREV_CSDI_CRC;  /* at 44 */
} s_CSDI_STATUS;
#endif
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t PrevYCsdiCrc;  /* at 0 */
uint32_t CurYCsdiCrc;  /* at 4 */
uint32_t NextYCsdiCrc;  /* at 8 */
uint32_t PrevUvCsdiCrc;  /* at 12 */
uint32_t CurUvCsdiCrc;  /* at 16 */
uint32_t NextUvCsdiCrc;  /* at 20 */
uint32_t YCsdiCrc;  /* at 24 */
uint32_t UvCsdiCrc;  /* at 28 */
uint32_t UvCupCrc;  /* at 32 */
uint32_t MotCsdiCrc;  /* at 36 */
uint32_t MotCurCsdiCrc;  /* at 40 */
uint32_t MotPrevCsdiCrc;  /* at 44 */
} HQVDPLITE_CSDIStatus_Params_t;
#endif /* FW_STXP70 */
#endif
