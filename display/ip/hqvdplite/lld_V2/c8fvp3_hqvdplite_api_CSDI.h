/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_CSDI_H
#define _C8FVP3_HQVDPLITE_API_CSDI_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : CSDI
*/


/**
* Register : CONFIG
* CSDI parameters
*/

#define CSDI_CONFIG_OFFSET                              (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x00)
#define CSDI_CONFIG_MASK                                (0x11FFF9FF)

/**
* Bit-field : MAIN_MODE
* Selects a group of configurations for the deinterlacer
*/

#define CSDI_CONFIG_MAIN_MODE_SHIFT                     (0x00000000)
#define CSDI_CONFIG_MAIN_MODE_WIDTH                     (2)
#define CSDI_CONFIG_MAIN_MODE_MASK                      (0x00000003)

/**
* Bit-field : LUMA_INTERP
* Defines the interpolation algorithm used for luma
* 3D mixes directional and temporal interpolations, using the MLD or the LFM fading algorithm.
* Used only when MAIN_MODE is "1x" (de-interlacement or LFM).
*/

#define CSDI_CONFIG_LUMA_INTERP_SHIFT                   (0x00000002)
#define CSDI_CONFIG_LUMA_INTERP_WIDTH                   (2)
#define CSDI_CONFIG_LUMA_INTERP_MASK                    (0x0000000C)

/**
* Bit-field : CHROMA_INTERP
* Defines the interpolation algorithm used for chroma
* 3D mixes directional and temporal interpolations, using the MLD or the LFM fading algorithm. The algorithm used for chroma shall not be a higher performance algorithm than the one used for luma.
* Used only when MAIN_MODE is "1x" (de-interlacement or LFM).
*/

#define CSDI_CONFIG_CHROMA_INTERP_SHIFT                 (0x00000004)
#define CSDI_CONFIG_CHROMA_INTERP_WIDTH                 (2)
#define CSDI_CONFIG_CHROMA_INTERP_MASK                  (0x00000030)

/**
* Bit-field : MD_MODE
* Defines the processing of motion.
* others: reserved
* Init mode and low conf mode allow easy initialisation of the MD feedback loop at the beginning of a video sequence.
* Low conf mode is also intended to be a "standard" mode of MD.
* Used only when MAIN_MODE is "1x" (de-interlacement or LFM).
*/

#define CSDI_CONFIG_MD_MODE_SHIFT                       (0x00000006)
#define CSDI_CONFIG_MD_MODE_WIDTH                       (3)
#define CSDI_CONFIG_MD_MODE_MASK                        (0x000001C0)

/**
* Bit-field : ENABLE_RECURSIVE_DIR
* Defines the use of the direction change limitation
* Used only when MAIN_MODE is "1x" (de-interlacement or LFM) and LUMA_INTERP is "x1" (3D or directional).
*/

#define CSDI_CONFIG_ENABLE_RECURSIVE_DIR_SHIFT          (0x0000000b)
#define CSDI_CONFIG_ENABLE_RECURSIVE_DIR_WIDTH          (1)
#define CSDI_CONFIG_ENABLE_RECURSIVE_DIR_MASK           (0x00000800)

/**
* Bit-field : KCOR
* Correction factor used in the fader.
* Unsigned fractional value, ranging from 0.5 ("0000") to 2.375 ("1111") with a step of 0.125.
* Used only when MAIN_MODE is "1x" (de-interlacement or LFM) and LUMA_INTERP is "11" (3D).
*/

#define CSDI_CONFIG_KCOR_SHIFT                          (0x0000000c)
#define CSDI_CONFIG_KCOR_WIDTH                          (4)
#define CSDI_CONFIG_KCOR_MASK                           (0x0000F000)

/**
* Bit-field : T_DETAIL
* Threshold for detail used in the fader.
* Unsigned value ranging from 0 to 15.
* Used only when MAIN_MODE is "1x" (de-interlacement or LFM) and LUMA_INTERP is "11" (3D)
*/

#define CSDI_CONFIG_T_DETAIL_SHIFT                      (0x00000010)
#define CSDI_CONFIG_T_DETAIL_WIDTH                      (4)
#define CSDI_CONFIG_T_DETAIL_MASK                       (0x000F0000)

/**
* Bit-field : KMOV
* Defines the motion factor.
* Unsigned value that could be 14, 12, 10 or 8. All others value are not allowed.
*/

#define CSDI_CONFIG_KMOV_SHIFT                          (0x00000014)
#define CSDI_CONFIG_KMOV_WIDTH                          (4)
#define CSDI_CONFIG_KMOV_MASK                           (0x00F00000)

/**
* Bit-field : MOTION_REDUCED
* Motion on 4 bits mode enable:
*/

#define CSDI_CONFIG_MOTION_REDUCED_SHIFT                (0x00000018)
#define CSDI_CONFIG_MOTION_REDUCED_WIDTH                (1)
#define CSDI_CONFIG_MOTION_REDUCED_MASK                 (0x01000000)

/**
* Bit-field : NEXT_NOT_PREV
* Defines the field index to use in case of field merging:
* Used only when MAIN_MODE is "01" (fieldmerging).
*/

#define CSDI_CONFIG_NEXT_NOT_PREV_SHIFT                 (0x0000001c)
#define CSDI_CONFIG_NEXT_NOT_PREV_WIDTH                 (1)
#define CSDI_CONFIG_NEXT_NOT_PREV_MASK                  (0x10000000)

/**
* Register : CONFIG2
* CSDi Config2
*/

#define CSDI_CONFIG2_OFFSET                             (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x04)
#define CSDI_CONFIG2_MASK                               (0x000001F7)

/**
* Bit-field : ADAPTIVE_FADING
* enable adaptive fading for luma in fading
*/

#define CSDI_CONFIG2_ADAPTIVE_FADING_SHIFT              (0x00000000)
#define CSDI_CONFIG2_ADAPTIVE_FADING_WIDTH              (1)
#define CSDI_CONFIG2_ADAPTIVE_FADING_MASK               (0x00000001)

/**
* Bit-field : CHROMA_MOTION
* enable specific motion for chroma in fading
*/

#define CSDI_CONFIG2_CHROMA_MOTION_SHIFT                (0x00000001)
#define CSDI_CONFIG2_CHROMA_MOTION_WIDTH                (1)
#define CSDI_CONFIG2_CHROMA_MOTION_MASK                 (0x00000002)

/**
* Bit-field : DETAILS
* choose the detail to be used in fading
*/

#define CSDI_CONFIG2_DETAILS_SHIFT                      (0x00000002)
#define CSDI_CONFIG2_DETAILS_WIDTH                      (1)
#define CSDI_CONFIG2_DETAILS_MASK                       (0x00000004)

/**
* Bit-field : MOTION_CLOSING
* enable to filter the motion (max and min)
*/

#define CSDI_CONFIG2_MOTION_CLOSING_SHIFT               (0x00000004)
#define CSDI_CONFIG2_MOTION_CLOSING_WIDTH               (1)
#define CSDI_CONFIG2_MOTION_CLOSING_MASK                (0x00000010)

/**
* Bit-field : MOTION_FIR
* enable the FIR on motion
*/

#define CSDI_CONFIG2_MOTION_FIR_SHIFT                   (0x00000005)
#define CSDI_CONFIG2_MOTION_FIR_WIDTH                   (1)
#define CSDI_CONFIG2_MOTION_FIR_MASK                    (0x00000020)

/**
* Bit-field : FIR_LENGTH
* size of the FIR on motion
*/

#define CSDI_CONFIG2_FIR_LENGTH_SHIFT                   (0x00000006)
#define CSDI_CONFIG2_FIR_LENGTH_WIDTH                   (2)
#define CSDI_CONFIG2_FIR_LENGTH_MASK                    (0x000000C0)

/**
* Bit-field : INFLEXION_MOTION
* enable the inflexion recursivity on motion
*/

#define CSDI_CONFIG2_INFLEXION_MOTION_SHIFT             (0x00000008)
#define CSDI_CONFIG2_INFLEXION_MOTION_WIDTH             (1)
#define CSDI_CONFIG2_INFLEXION_MOTION_MASK              (0x00000100)

/**
* Register : DCDI_CONFIG
* DCDi config
*/

#define CSDI_DCDI_CONFIG_OFFSET                         (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x08)
#define CSDI_DCDI_CONFIG_MASK                           (0x007FFF03)

/**
* Bit-field : ALPHA_MEDIAN_EN
* enable the alpha median filter
*/

#define CSDI_DCDI_CONFIG_ALPHA_MEDIAN_EN_SHIFT          (0x00000000)
#define CSDI_DCDI_CONFIG_ALPHA_MEDIAN_EN_WIDTH          (1)
#define CSDI_DCDI_CONFIG_ALPHA_MEDIAN_EN_MASK           (0x00000001)

/**
* Bit-field : MEDIAN_BLEND_ANGLE_DEP
* enable median blender dependency on angle
*/

#define CSDI_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_SHIFT   (0x00000001)
#define CSDI_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_WIDTH   (1)
#define CSDI_DCDI_CONFIG_MEDIAN_BLEND_ANGLE_DEP_MASK    (0x00000002)

/**
* Bit-field : WIN_CLAMP_SIZE
* interpolation window clamp size, ranging from 0 to 15, default is 8
*/

#define CSDI_DCDI_CONFIG_WIN_CLAMP_SIZE_SHIFT           (0x00000008)
#define CSDI_DCDI_CONFIG_WIN_CLAMP_SIZE_WIDTH           (4)
#define CSDI_DCDI_CONFIG_WIN_CLAMP_SIZE_MASK            (0x00000F00)

/**
* Bit-field : MEDIAN_MODE
* median filter mode ranging from -1 to 4, default is 3
*/

#define CSDI_DCDI_CONFIG_MEDIAN_MODE_SHIFT              (0x0000000c)
#define CSDI_DCDI_CONFIG_MEDIAN_MODE_WIDTH              (4)
#define CSDI_DCDI_CONFIG_MEDIAN_MODE_MASK               (0x0000F000)

/**
* Bit-field : ALPHA_LIMIT
* Alpha limit ranging from 0 to 127, default is 32
*/

#define CSDI_DCDI_CONFIG_ALPHA_LIMIT_SHIFT              (0x00000010)
#define CSDI_DCDI_CONFIG_ALPHA_LIMIT_WIDTH              (7)
#define CSDI_DCDI_CONFIG_ALPHA_LIMIT_MASK               (0x007F0000)

/**
* Register : PREV_LUMA
* Previous luma buffer base address
*/

#define CSDI_PREV_LUMA_OFFSET                           (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x0C)
#define CSDI_PREV_LUMA_MASK                             (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_PREV_LUMA_BASE_ADDR_SHIFT                  (0x00000000)
#define CSDI_PREV_LUMA_BASE_ADDR_WIDTH                  (32)
#define CSDI_PREV_LUMA_BASE_ADDR_MASK                   (0xFFFFFFFF)

/**
* Register : PREV_ENH_LUMA
* Previous Enhanced luma buffer base address
*/

#define CSDI_PREV_ENH_LUMA_OFFSET                       (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x10)
#define CSDI_PREV_ENH_LUMA_MASK                         (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_PREV_ENH_LUMA_BASE_ADDR_SHIFT              (0x00000000)
#define CSDI_PREV_ENH_LUMA_BASE_ADDR_WIDTH              (32)
#define CSDI_PREV_ENH_LUMA_BASE_ADDR_MASK               (0xFFFFFFFF)

/**
* Register : PREV_RIGHT_LUMA
* Previous Right luma buffer base address
*/

#define CSDI_PREV_RIGHT_LUMA_OFFSET                     (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x14)
#define CSDI_PREV_RIGHT_LUMA_MASK                       (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_PREV_RIGHT_LUMA_BASE_ADDR_SHIFT            (0x00000000)
#define CSDI_PREV_RIGHT_LUMA_BASE_ADDR_WIDTH            (32)
#define CSDI_PREV_RIGHT_LUMA_BASE_ADDR_MASK             (0xFFFFFFFF)

/**
* Register : PREV_ENH_RIGHT_LUMA
* Previous Enhanced Right luma buffer base address
*/

#define CSDI_PREV_ENH_RIGHT_LUMA_OFFSET                 (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x18)
#define CSDI_PREV_ENH_RIGHT_LUMA_MASK                   (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_PREV_ENH_RIGHT_LUMA_BASE_ADDR_SHIFT        (0x00000000)
#define CSDI_PREV_ENH_RIGHT_LUMA_BASE_ADDR_WIDTH        (32)
#define CSDI_PREV_ENH_RIGHT_LUMA_BASE_ADDR_MASK         (0xFFFFFFFF)

/**
* Register : NEXT_LUMA
* Next luma buffer base address
*/

#define CSDI_NEXT_LUMA_OFFSET                           (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x1C)
#define CSDI_NEXT_LUMA_MASK                             (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_NEXT_LUMA_BASE_ADDR_SHIFT                  (0x00000000)
#define CSDI_NEXT_LUMA_BASE_ADDR_WIDTH                  (32)
#define CSDI_NEXT_LUMA_BASE_ADDR_MASK                   (0xFFFFFFFF)

/**
* Register : NEXT_ENH_LUMA
* Next Enhanced luma buffer base address
*/

#define CSDI_NEXT_ENH_LUMA_OFFSET                       (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x20)
#define CSDI_NEXT_ENH_LUMA_MASK                         (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_NEXT_ENH_LUMA_BASE_ADDR_SHIFT              (0x00000000)
#define CSDI_NEXT_ENH_LUMA_BASE_ADDR_WIDTH              (32)
#define CSDI_NEXT_ENH_LUMA_BASE_ADDR_MASK               (0xFFFFFFFF)

/**
* Register : NEXT_RIGHT_LUMA
* Next Right luma buffer base address
*/

#define CSDI_NEXT_RIGHT_LUMA_OFFSET                     (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x24)
#define CSDI_NEXT_RIGHT_LUMA_MASK                       (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_NEXT_RIGHT_LUMA_BASE_ADDR_SHIFT            (0x00000000)
#define CSDI_NEXT_RIGHT_LUMA_BASE_ADDR_WIDTH            (32)
#define CSDI_NEXT_RIGHT_LUMA_BASE_ADDR_MASK             (0xFFFFFFFF)

/**
* Register : NEXT_ENH_RIGHT_LUMA
* Next Enhanced Right luma buffer base address
*/

#define CSDI_NEXT_ENH_RIGHT_LUMA_OFFSET                 (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x28)
#define CSDI_NEXT_ENH_RIGHT_LUMA_MASK                   (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define CSDI_NEXT_ENH_RIGHT_LUMA_BASE_ADDR_SHIFT        (0x00000000)
#define CSDI_NEXT_ENH_RIGHT_LUMA_BASE_ADDR_WIDTH        (32)
#define CSDI_NEXT_ENH_RIGHT_LUMA_BASE_ADDR_MASK         (0xFFFFFFFF)

/**
* Register : PREV_CHROMA
* Previous chroma UV buffer base address
*/

#define CSDI_PREV_CHROMA_OFFSET                         (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x2C)
#define CSDI_PREV_CHROMA_MASK                           (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer
*/

#define CSDI_PREV_CHROMA_BASE_ADDR_SHIFT                (0x00000000)
#define CSDI_PREV_CHROMA_BASE_ADDR_WIDTH                (32)
#define CSDI_PREV_CHROMA_BASE_ADDR_MASK                 (0xFFFFFFFF)

/**
* Register : PREV_ENH_CHROMA
* Previous Enhanced chroma UV buffer base address
*/

#define CSDI_PREV_ENH_CHROMA_OFFSET                     (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x30)
#define CSDI_PREV_ENH_CHROMA_MASK                       (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer
*/

#define CSDI_PREV_ENH_CHROMA_BASE_ADDR_SHIFT            (0x00000000)
#define CSDI_PREV_ENH_CHROMA_BASE_ADDR_WIDTH            (32)
#define CSDI_PREV_ENH_CHROMA_BASE_ADDR_MASK             (0xFFFFFFFF)

/**
* Register : PREV_RIGHT_CHROMA
* Previous right chroma UV buffer base address
*/

#define CSDI_PREV_RIGHT_CHROMA_OFFSET                   (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x34)
#define CSDI_PREV_RIGHT_CHROMA_MASK                     (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer.
*/

#define CSDI_PREV_RIGHT_CHROMA_BASE_ADDR_SHIFT          (0x00000000)
#define CSDI_PREV_RIGHT_CHROMA_BASE_ADDR_WIDTH          (32)
#define CSDI_PREV_RIGHT_CHROMA_BASE_ADDR_MASK           (0xFFFFFFFF)

/**
* Register : PREV_ENH_RIGHT_CHROMA
* Previous Enhanced right chroma UV buffer base address
*/

#define CSDI_PREV_ENH_RIGHT_CHROMA_OFFSET               (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x38)
#define CSDI_PREV_ENH_RIGHT_CHROMA_MASK                 (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer
*/

#define CSDI_PREV_ENH_RIGHT_CHROMA_BASE_ADDR_SHIFT      (0x00000000)
#define CSDI_PREV_ENH_RIGHT_CHROMA_BASE_ADDR_WIDTH      (32)
#define CSDI_PREV_ENH_RIGHT_CHROMA_BASE_ADDR_MASK       (0xFFFFFFFF)

/**
* Register : NEXT_CHROMA
* Next chroma UV buffer base address
*/

#define CSDI_NEXT_CHROMA_OFFSET                         (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x3C)
#define CSDI_NEXT_CHROMA_MASK                           (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer
*/

#define CSDI_NEXT_CHROMA_BASE_ADDR_SHIFT                (0x00000000)
#define CSDI_NEXT_CHROMA_BASE_ADDR_WIDTH                (32)
#define CSDI_NEXT_CHROMA_BASE_ADDR_MASK                 (0xFFFFFFFF)

/**
* Register : NEXT_ENH_CHROMA
* Next Enhanced chroma UV buffer base address
*/

#define CSDI_NEXT_ENH_CHROMA_OFFSET                     (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x40)
#define CSDI_NEXT_ENH_CHROMA_MASK                       (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer.
*/

#define CSDI_NEXT_ENH_CHROMA_BASE_ADDR_SHIFT            (0x00000000)
#define CSDI_NEXT_ENH_CHROMA_BASE_ADDR_WIDTH            (32)
#define CSDI_NEXT_ENH_CHROMA_BASE_ADDR_MASK             (0xFFFFFFFF)

/**
* Register : NEXT_RIGHT_CHROMA
* Next Right chroma UV buffer base address
*/

#define CSDI_NEXT_RIGHT_CHROMA_OFFSET                   (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x44)
#define CSDI_NEXT_RIGHT_CHROMA_MASK                     (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer
*/

#define CSDI_NEXT_RIGHT_CHROMA_BASE_ADDR_SHIFT          (0x00000000)
#define CSDI_NEXT_RIGHT_CHROMA_BASE_ADDR_WIDTH          (32)
#define CSDI_NEXT_RIGHT_CHROMA_BASE_ADDR_MASK           (0xFFFFFFFF)

/**
* Register : NEXT_ENH_RIGHT_CHROMA
* Next Enhanced Right chroma UV buffer base address
*/

#define CSDI_NEXT_ENH_RIGHT_CHROMA_OFFSET               (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x48)
#define CSDI_NEXT_ENH_RIGHT_CHROMA_MASK                 (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input chroma buffer.
*/

#define CSDI_NEXT_ENH_RIGHT_CHROMA_BASE_ADDR_SHIFT      (0x00000000)
#define CSDI_NEXT_ENH_RIGHT_CHROMA_BASE_ADDR_WIDTH      (32)
#define CSDI_NEXT_ENH_RIGHT_CHROMA_BASE_ADDR_MASK       (0xFFFFFFFF)

/**
* Register : PREV_MOTION
* Previous motion buffer base address
*/

#define CSDI_PREV_MOTION_OFFSET                         (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x4C)
#define CSDI_PREV_MOTION_MASK                           (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the motion buffer of the previous motion field. The memory cell pointed by BASE_ADDR contains the top-left motion detection pixel of the previous motion field.
*/

#define CSDI_PREV_MOTION_BASE_ADDR_SHIFT                (0x00000000)
#define CSDI_PREV_MOTION_BASE_ADDR_WIDTH                (32)
#define CSDI_PREV_MOTION_BASE_ADDR_MASK                 (0xFFFFFFFF)

/**
* Register : PREV_RIGHT_MOTION
* Previous right motion buffer base address
*/

#define CSDI_PREV_RIGHT_MOTION_OFFSET                   (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x50)
#define CSDI_PREV_RIGHT_MOTION_MASK                     (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the motion buffer of the previous motion field. The memory cell pointed by BASE_ADDR contains the top-left motion detection pixel of the previous motion field.
*/

#define CSDI_PREV_RIGHT_MOTION_BASE_ADDR_SHIFT          (0x00000000)
#define CSDI_PREV_RIGHT_MOTION_BASE_ADDR_WIDTH          (32)
#define CSDI_PREV_RIGHT_MOTION_BASE_ADDR_MASK           (0xFFFFFFFF)

/**
* Register : CUR_MOTION
* Current motion buffer base address
*/

#define CSDI_CUR_MOTION_OFFSET                          (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x54)
#define CSDI_CUR_MOTION_MASK                            (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the motion buffer of the current motion field. The memory cell pointed by BASE_ADDR contains the top-left motion detection pixel of the current motion field.
*/

#define CSDI_CUR_MOTION_BASE_ADDR_SHIFT                 (0x00000000)
#define CSDI_CUR_MOTION_BASE_ADDR_WIDTH                 (32)
#define CSDI_CUR_MOTION_BASE_ADDR_MASK                  (0xFFFFFFFF)

/**
* Register : CUR_RIGHT_MOTION
* Current right motion buffer base address
*/

#define CSDI_CUR_RIGHT_MOTION_OFFSET                    (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x58)
#define CSDI_CUR_RIGHT_MOTION_MASK                      (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the motion buffer of the current motion field. The memory cell pointed by BASE_ADDR contains the top-left motion detection pixel of the current motion field.
*/

#define CSDI_CUR_RIGHT_MOTION_BASE_ADDR_SHIFT           (0x00000000)
#define CSDI_CUR_RIGHT_MOTION_BASE_ADDR_WIDTH           (32)
#define CSDI_CUR_RIGHT_MOTION_BASE_ADDR_MASK            (0xFFFFFFFF)

/**
* Register : NEXT_MOTION
* Next motion buffer base address
*/

#define CSDI_NEXT_MOTION_OFFSET                         (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x5C)
#define CSDI_NEXT_MOTION_MASK                           (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the motion buffer in which the firmware will stored the next motion.The memory cell pointed by BASE_ADDR contains the top-left motion detection pixel of the next motion field.
*/

#define CSDI_NEXT_MOTION_BASE_ADDR_SHIFT                (0x00000000)
#define CSDI_NEXT_MOTION_BASE_ADDR_WIDTH                (32)
#define CSDI_NEXT_MOTION_BASE_ADDR_MASK                 (0xFFFFFFFF)

/**
* Register : NEXT_RIGHT_MOTION
* Next right motion buffer base address
*/

#define CSDI_NEXT_RIGHT_MOTION_OFFSET                   (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR + 0x60)
#define CSDI_NEXT_RIGHT_MOTION_MASK                     (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the motion buffer in which the firmware will stored the next motion.The memory cell pointed by BASE_ADDR contains the top-left motion detection pixel of the next motion field.
*/

#define CSDI_NEXT_RIGHT_MOTION_BASE_ADDR_SHIFT          (0x00000000)
#define CSDI_NEXT_RIGHT_MOTION_BASE_ADDR_WIDTH          (32)
#define CSDI_NEXT_RIGHT_MOTION_BASE_ADDR_MASK           (0xFFFFFFFF)


#ifndef HQVDPLITE_API_FOR_STAPI
typedef struct {
    gvh_u32_t CONFIG;  /* at 0 */
    gvh_u32_t CONFIG2;  /* at 4 */
    gvh_u32_t DCDI_CONFIG;  /* at 8 */
    gvh_u32_t PREV_LUMA;  /* at 12 */
    gvh_u32_t PREV_ENH_LUMA;  /* at 16 */
    gvh_u32_t PREV_RIGHT_LUMA;  /* at 20 */
    gvh_u32_t PREV_ENH_RIGHT_LUMA;  /* at 24 */
    gvh_u32_t NEXT_LUMA;  /* at 28 */
    gvh_u32_t NEXT_ENH_LUMA;  /* at 32 */
    gvh_u32_t NEXT_RIGHT_LUMA;  /* at 36 */
    gvh_u32_t NEXT_ENH_RIGHT_LUMA;  /* at 40 */
    gvh_u32_t PREV_CHROMA;  /* at 44 */
    gvh_u32_t PREV_ENH_CHROMA;  /* at 48 */
    gvh_u32_t PREV_RIGHT_CHROMA;  /* at 52 */
    gvh_u32_t PREV_ENH_RIGHT_CHROMA;  /* at 56 */
    gvh_u32_t NEXT_CHROMA;  /* at 60 */
    gvh_u32_t NEXT_ENH_CHROMA;  /* at 64 */
    gvh_u32_t NEXT_RIGHT_CHROMA;  /* at 68 */
    gvh_u32_t NEXT_ENH_RIGHT_CHROMA;  /* at 72 */
    gvh_u32_t PREV_MOTION;  /* at 76 */
    gvh_u32_t PREV_RIGHT_MOTION;  /* at 80 */
    gvh_u32_t CUR_MOTION;  /* at 84 */
    gvh_u32_t CUR_RIGHT_MOTION;  /* at 88 */
    gvh_u32_t NEXT_MOTION;  /* at 92 */
    gvh_u32_t NEXT_RIGHT_MOTION;  /* at 96 */
} s_CSDI;
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t Config;  /* at 0 */
uint32_t Config2;  /* at 4 */
uint32_t DcdiConfig;  /* at 8 */
uint32_t PrevLuma;  /* at 12 */
uint32_t PrevEnhLuma;  /* at 16 */
uint32_t PrevRightLuma;  /* at 20 */
uint32_t PrevEnhRightLuma;  /* at 24 */
uint32_t NextLuma;  /* at 28 */
uint32_t NextEnhLuma;  /* at 32 */
uint32_t NextRightLuma;  /* at 36 */
uint32_t NextEnhRightLuma;  /* at 40 */
uint32_t PrevChroma;  /* at 44 */
uint32_t PrevEnhChroma;  /* at 48 */
uint32_t PrevRightChroma;  /* at 52 */
uint32_t PrevEnhRightChroma;  /* at 56 */
uint32_t NextChroma;  /* at 60 */
uint32_t NextEnhChroma;  /* at 64 */
uint32_t NextRightChroma;  /* at 68 */
uint32_t NextEnhRightChroma;  /* at 72 */
uint32_t PrevMotion;  /* at 76 */
uint32_t PrevRightMotion;  /* at 80 */
uint32_t CurMotion;  /* at 84 */
uint32_t CurRightMotion;  /* at 88 */
uint32_t NextMotion;  /* at 92 */
uint32_t NextRightMotion;  /* at 96 */
} HQVDPLITE_CSDI_Params_t;
#endif /* FW_STXP70 */
#endif
