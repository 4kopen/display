/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_FMD_STATUS_H
#define _C8FVP3_HQVDPLITE_API_FMD_STATUS_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : FMD_STATUS
*/


/**
* Register : FMD_REPEAT_MOVE_STATUS
* Repeat and move status
*/

#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_OFFSET        (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR + 0x00)
#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_MASK          (0x00000003)

/**
* Bit-field : MOVE_STATUS
* This bit is set if at least one block is a moving block regarding move register threshold.
*/

#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_MOVE_STATUS_SHIFT (0x00000000)
#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_MOVE_STATUS_WIDTH (1)
#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_MOVE_STATUS_MASK (0x00000001)

/**
* Bit-field : REPEAT_STATUS
* This bit is set if there is no moving block regarding t_repeat register threshold.
*/

#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_REPEAT_STATUS_SHIFT (0x00000001)
#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_REPEAT_STATUS_WIDTH (1)
#define FMD_STATUS_FMD_REPEAT_MOVE_STATUS_REPEAT_STATUS_MASK (0x00000002)

/**
* Register : FMD_SCENE_COUNT_STATUS
* Scene count status
*/

#define FMD_STATUS_FMD_SCENE_COUNT_STATUS_OFFSET        (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR + 0x04)
#define FMD_STATUS_FMD_SCENE_COUNT_STATUS_MASK          (0x000007FF)

/**
* Bit-field : SCENE_COUNT
* Number of blocks that have their pixels differences |A-D| sum superior to threshold t_scene.
*/

#define FMD_STATUS_FMD_SCENE_COUNT_STATUS_SCENE_COUNT_SHIFT (0x00000000)
#define FMD_STATUS_FMD_SCENE_COUNT_STATUS_SCENE_COUNT_WIDTH (11)
#define FMD_STATUS_FMD_SCENE_COUNT_STATUS_SCENE_COUNT_MASK (0x000007FF)

/**
* Register : CFD_SUM
* Sum of consecutive field difference pixels
*/

#define FMD_STATUS_CFD_SUM_OFFSET                       (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR + 0x08)
#define FMD_STATUS_CFD_SUM_MASK                         (0x0FFFFFFF)

/**
* Bit-field : CFD_SUM
* Sum of CFD pixels difference min(|A-B|,|A-C|) that are superior to threshold t_noise computed during one field.
* Maximum Field size is 1920 pixels of 540 lines.
*/

#define FMD_STATUS_CFD_SUM_CFD_SUM_SHIFT                (0x00000000)
#define FMD_STATUS_CFD_SUM_CFD_SUM_WIDTH                (28)
#define FMD_STATUS_CFD_SUM_CFD_SUM_MASK                 (0x0FFFFFFF)

/**
* Register : FIELD_SUM
* Sum of all pixels differences
*/

#define FMD_STATUS_FIELD_SUM_OFFSET                     (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR + 0x0C)
#define FMD_STATUS_FIELD_SUM_MASK                       (0x0FFFFFFF)

/**
* Bit-field : FIELD_SUM
* Sum of all pixels difference |A-D| computed during one field.
* Maximum Field size is 1920 pixels of 540 lines.
*/

#define FMD_STATUS_FIELD_SUM_FIELD_SUM_SHIFT            (0x00000000)
#define FMD_STATUS_FIELD_SUM_FIELD_SUM_WIDTH            (28)
#define FMD_STATUS_FIELD_SUM_FIELD_SUM_MASK             (0x0FFFFFFF)

/**
* Register : NEXT_Y_FMD_CRC
* CRC value luma next field to FMD
*/

#define FMD_STATUS_NEXT_Y_FMD_CRC_OFFSET                (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR + 0x10)
#define FMD_STATUS_NEXT_Y_FMD_CRC_MASK                  (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define FMD_STATUS_NEXT_Y_FMD_CRC_CRC_RWP_SHIFT         (0x00000000)
#define FMD_STATUS_NEXT_Y_FMD_CRC_CRC_RWP_WIDTH         (32)
#define FMD_STATUS_NEXT_Y_FMD_CRC_CRC_RWP_MASK          (0xFFFFFFFF)

/**
* Register : NEXT_NEXT_Y_FMD_CRC
* CRC value luma next next field to FMD
*/

#define FMD_STATUS_NEXT_NEXT_Y_FMD_CRC_OFFSET           (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR + 0x14)
#define FMD_STATUS_NEXT_NEXT_Y_FMD_CRC_MASK             (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define FMD_STATUS_NEXT_NEXT_Y_FMD_CRC_CRC_RWP_SHIFT    (0x00000000)
#define FMD_STATUS_NEXT_NEXT_Y_FMD_CRC_CRC_RWP_WIDTH    (32)
#define FMD_STATUS_NEXT_NEXT_Y_FMD_CRC_CRC_RWP_MASK     (0xFFFFFFFF)

/**
* Register : NEXT_NEXT_NEXT_Y_FMD_CRC
* CRC value luma next next next field to FMD
*/

#define FMD_STATUS_NEXT_NEXT_NEXT_Y_FMD_CRC_OFFSET      (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR + 0x18)
#define FMD_STATUS_NEXT_NEXT_NEXT_Y_FMD_CRC_MASK        (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer
*/

#define FMD_STATUS_NEXT_NEXT_NEXT_Y_FMD_CRC_CRC_RWP_SHIFT (0x00000000)
#define FMD_STATUS_NEXT_NEXT_NEXT_Y_FMD_CRC_CRC_RWP_WIDTH (32)
#define FMD_STATUS_NEXT_NEXT_NEXT_Y_FMD_CRC_CRC_RWP_MASK (0xFFFFFFFF)


#ifndef HQVDPLITE_API_FOR_STAPI
#ifdef FW_STXP70

typedef struct {
    gvh_u32_t FMD_REPEAT_MOVE_STATUS;  /* at 0 */
    gvh_u32_t Pad1;
    gvh_u32_t Pad2;
    gvh_u32_t Pad3;
    gvh_u32_t FMD_SCENE_COUNT_STATUS;  /* at 4 */
    gvh_u32_t Pad4;
    gvh_u32_t Pad5;
    gvh_u32_t Pad6;
    gvh_u32_t CFD_SUM;  /* at 8 */
    gvh_u32_t Pad7;
    gvh_u32_t Pad8;
    gvh_u32_t Pad9;
    gvh_u32_t FIELD_SUM;  /* at 12 */
    gvh_u32_t Pad10;
    gvh_u32_t Pad11;
    gvh_u32_t Pad12;
    gvh_u32_t NEXT_Y_FMD_CRC;  /* at 16 */
    gvh_u32_t Pad13;
    gvh_u32_t Pad14;
    gvh_u32_t Pad15;
    gvh_u32_t NEXT_NEXT_Y_FMD_CRC;  /* at 20 */
    gvh_u32_t Pad16;
    gvh_u32_t Pad17;
    gvh_u32_t Pad18;
    gvh_u32_t NEXT_NEXT_NEXT_Y_FMD_CRC;  /* at 24 */
    gvh_u32_t Pad19;
    gvh_u32_t Pad20;
    gvh_u32_t Pad21;
} s_FMD_STATUS;
#else
typedef struct {
    gvh_u32_t FMD_REPEAT_MOVE_STATUS;  /* at 0 */
    gvh_u32_t FMD_SCENE_COUNT_STATUS;  /* at 4 */
    gvh_u32_t CFD_SUM;  /* at 8 */
    gvh_u32_t FIELD_SUM;  /* at 12 */
    gvh_u32_t NEXT_Y_FMD_CRC;  /* at 16 */
    gvh_u32_t NEXT_NEXT_Y_FMD_CRC;  /* at 20 */
    gvh_u32_t NEXT_NEXT_NEXT_Y_FMD_CRC;  /* at 24 */
} s_FMD_STATUS;
#endif
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t FmdRepeatMoveStatus;  /* at 0 */
uint32_t FmdSceneCountStatus;  /* at 4 */
uint32_t CfdSum;  /* at 8 */
uint32_t FieldSum;  /* at 12 */
uint32_t NextYFmdCrc;  /* at 16 */
uint32_t NextNextYFmdCrc;  /* at 20 */
uint32_t NextNextNextYFmdCrc;  /* at 24 */
} HQVDPLITE_FMDStatus_Params_t;
#endif /* FW_STXP70 */
#endif
