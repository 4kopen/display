/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "c8fvp3_FW_download_code.h"
#include "c8fvp3_strRegAccess.h"
#include "hqvdplite_ip_mapping.h"

/****************************************
  fct to download FW Code in PMEM and DMEM
******************************************/

void c8fvp3_download_code(void *base_addr,
                          enum hqvdp_lld_fw_load_e load_mode,
                          const struct hqvdp_lld_mem_desc_s *src_info,
                          uint32_t dest_addr)
{
        uint32_t src_addr_32;
        uint32_t offset;
        uint32_t data;

        uint32_t page;
        uint32_t currsize;

        const uint32_t pagesize = 0x1C00; /* 8192 max transfer */
        const uint32_t DownloadCh = 0;

        switch(load_mode) {
        case FW_LOAD_STREAMER:

                /* use physical address when using streamer */
                src_addr_32 = (uint32_t)src_info->physical;

                TRC(TRC_ID_HQVDPLITE_LLD,
                    "(HQVDP %p) Loading firmware with streamer from physical memory @ 0x%08X in HQVDP @ 0x%05X",
                    base_addr, src_addr_32, dest_addr);

                /* write code in memory with streamer plug by page size byte page */
                for (page = 0; page < (uint32_t)src_info->size; page += pagesize) {

                        currsize = ((src_info->size - page) > pagesize) ?
                                        pagesize : ( src_info->size - page);

                        /* TODO WriteBackSTBCh is not ready to 64 bits */
                        WriteBackSTBCh(
                                 /* Streamer base address */
                                 /* HQVDP base address */ base_addr,
                                 /* offset */ HQVDP_LITE_STR_READ_REGS_OFFSET,
                                 /* Channel number */
                                 /* chNb         */ DownloadCh,
                                 /* Read channel (1) or write channel (0) */
                                 /* rdNWr        */ 1,
                                 /* External start address */
                                 /* extsa        */ src_addr_32 + page,
                                 /* PLUG addressing mode */
                                 /* plugam       */ STR_PLUGAM_RASTER,
                                 /* STBUS Attribute Fields */
                                 /* attr         */ 0,
                                 /* External addressing mode */
                                 /* extam        */ STR_EXTAM_LINEAR_BUFFER,
                                 /* External two dimensional increment */
                                 /* ext2d        */ 0,
                                 /* External transfer size = size to transfer (in bytes) - 1 */
                                 /* extsz        */ currsize - 1,
                                 /* Endianness correction */
                                 /* ec           */ STR_EC_BYTES_NOT_SWAPPED,
                                 /* Data formatting */
                                 /* df           */ STR_DF_NO_DEMUX,
                                 /* Internal start address */
                                 /* intsa        */ dest_addr + page, /*HQVDP_LITE_MEGACELL_OFFSET,*/
                                 /* Internal reload address */
                                 /* intra        */ 0,
                                 /* Internal address increment = address increment to switch on the next circular buffer. */
                                 /* intai        */ 0,
                                 /* Internal decrement counter = remaining number of circular buffers to use */
                                 /* intdc        */ 0,
                                 /* Internal reload decrement counter = number of circular buffer to use - 1 */
                                 /* intrc        */ 0,
                                 /* Internal offset = offset between two consecutive sub-channels */
                                 /* into         */ 0); /* Dummy value because we don't use channel 1*/

                        REG32_WRITE(base_addr, STR_ABS_BSF(HQVDP_LITE_STR_READ_REGS_OFFSET),
                                    STR_BRSF_MASK(DownloadCh));

                        while (REG32_READ(base_addr, STR_ABS_BSF(HQVDP_LITE_STR_READ_REGS_OFFSET))
                               & STR_BRSF_MASK(DownloadCh)) {
                                yield();
                        }
                }
                break;
        case FW_LOAD_T1:
                /* use logical address ic case of copy from ARM */
                /* write code in memory word by word */
                TRC(TRC_ID_HQVDPLITE_LLD,
                    "(HQVDP %p) Loading firmware with T1 from logical address %p in HQVDP @ 0x%05X",
                    base_addr, src_info->logical, dest_addr);

                for (offset = 0; offset < (uint32_t)src_info->size; offset +=4 ) {
                        data = *(uint32_t *)((uintptr_t)src_info->logical
                                             + offset);
                        REG32_WRITE(base_addr, dest_addr + offset, data);
                }
                break;
       }

}
