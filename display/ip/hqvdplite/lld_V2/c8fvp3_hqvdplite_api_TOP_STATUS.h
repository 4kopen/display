/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_TOP_STATUS_H
#define _C8FVP3_HQVDPLITE_API_TOP_STATUS_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : TOP_STATUS
*/


/**
* Register : PROCESSING_TIME
* XP70 processing time in cycles
*/

#define TOP_STATUS_PROCESSING_TIME_OFFSET               (c8fvp3_hqvdplite_api_TOP_STATUS_BASE_ADDR + 0x00)
#define TOP_STATUS_PROCESSING_TIME_MASK                 (0xFFFFFFFF)

/**
* Bit-field : NB_CYCLES
* Unsigned value representing the number of cycle spent by the xp70 between cmd start and end of processing.
*/

#define TOP_STATUS_PROCESSING_TIME_NB_CYCLES_SHIFT      (0x00000000)
#define TOP_STATUS_PROCESSING_TIME_NB_CYCLES_WIDTH      (32)
#define TOP_STATUS_PROCESSING_TIME_NB_CYCLES_MASK       (0xFFFFFFFF)

/**
* Register : INPUT_Y_CRC
* CRC value luma input field
*/

#define TOP_STATUS_INPUT_Y_CRC_OFFSET                   (c8fvp3_hqvdplite_api_TOP_STATUS_BASE_ADDR + 0x04)
#define TOP_STATUS_INPUT_Y_CRC_MASK                     (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer. This status is dedicated to check the crc value of output of the delta IP.
*/

#define TOP_STATUS_INPUT_Y_CRC_CRC_RWP_SHIFT            (0x00000000)
#define TOP_STATUS_INPUT_Y_CRC_CRC_RWP_WIDTH            (32)
#define TOP_STATUS_INPUT_Y_CRC_CRC_RWP_MASK             (0xFFFFFFFF)

/**
* Register : INPUT_UV_CRC
* CRC value chroma input field
*/

#define TOP_STATUS_INPUT_UV_CRC_OFFSET                  (c8fvp3_hqvdplite_api_TOP_STATUS_BASE_ADDR + 0x08)
#define TOP_STATUS_INPUT_UV_CRC_MASK                    (0xFFFFFFFF)

/**
* Bit-field : CRC_RWP
* Unsigned value representing the calculated checksum on the data of the current transfer. This status is dedicated to check the crc value of output of the delta IP.
*/

#define TOP_STATUS_INPUT_UV_CRC_CRC_RWP_SHIFT           (0x00000000)
#define TOP_STATUS_INPUT_UV_CRC_CRC_RWP_WIDTH           (32)
#define TOP_STATUS_INPUT_UV_CRC_CRC_RWP_MASK            (0xFFFFFFFF)


#ifndef HQVDPLITE_API_FOR_STAPI
#ifdef FW_STXP70
typedef struct {
    gvh_u32_t PROCESSING_TIME;  /* at 0 */
    gvh_u32_t Pad1;
    gvh_u32_t Pad2;
    gvh_u32_t Pad3;
    gvh_u32_t INPUT_Y_CRC;  /* at 4 */
    gvh_u32_t Pad4;
    gvh_u32_t Pad5;
    gvh_u32_t Pad6;
    gvh_u32_t INPUT_UV_CRC;  /* at 8 */
    gvh_u32_t Pad7;
    gvh_u32_t Pad8;
    gvh_u32_t Pad9;
} s_TOP_STATUS;
#else
typedef struct {
    gvh_u32_t PROCESSING_TIME;  /* at 0 */
    gvh_u32_t INPUT_Y_CRC;  /* at 4 */
    gvh_u32_t INPUT_UV_CRC;  /* at 8 */
} s_TOP_STATUS;
#endif
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t ProcessingTime;  /* at 0 */
uint32_t InputYCrc;  /* at 4 */
uint32_t InputUvCrc;  /* at 8 */
} HQVDPLITE_TOPStatus_Params_t;
#endif /* FW_STXP70 */
#endif
