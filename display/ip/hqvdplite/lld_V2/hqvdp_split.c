/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

 /**@file hqvdp_split.c
    @brief low level driver internal function

    This file content function used internally by lld.

 */

#include "hqvdp_lld_api.h"
#include "hqvdp_lld_platform.h"
#include "hqvdp_handle.h"

#include "hqvdp_split.h"
#include "hqvdp_split_algo.h"

#include "c8fvp3_FW_lld.h"
#include "c8fvp3_hqvdplite_api_TOP.h"
#include "c8fvp3_hqvdplite_api_HVSRC.h"

/* debug only */
#include "hqvdp_check.h"

/** @brief update command according to new_param
    @ingroup  lld_local_function

    @param[in] new_param : parameter to use to update command
    @param[in] hqvdp_cmd : command to update
    @param[in] side : hqvdp to update
    @param[in] param_3d: 3d specific info
    @param[in] hdl : handle to session
*/
void hqvdp_update_cmd(const struct cmd_split_param *new_param,
                      HQVDPLITE_CMD_t *hqvdp_cmd,
                      enum side_e side,
                      const struct cmd_origi_param_3d *param_3d,
                      hqvdp_lld_hdl_t hdl)
{

        /* clear field and put new value */
        hqvdp_cmd->Top.InputViewportOri &= ~TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_MASK;
        hqvdp_cmd->Top.InputViewportOri |= new_param->input_viewportx;

        /* clear field and put new value */
        hqvdp_cmd->Top.InputViewportSize &= ~TOP_INPUT_VIEWPORT_SIZE_WIDTH_MASK;
        hqvdp_cmd->Top.InputViewportSize |= new_param->input_viewport_width;

        /* clear field and put new value */
        hqvdp_cmd->Hvsrc.OutputPictureSize &= ~HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_MASK;
        hqvdp_cmd->Hvsrc.OutputPictureSize |= new_param->output_width;

        /* clear field and put new value */
        hqvdp_cmd->Hvsrc.InitHorizontal &= ~HVSRC_INIT_HORIZONTAL_LUMA_INIT_PHASE_MASK;
        hqvdp_cmd->Hvsrc.InitHorizontal |= (new_param->init_phase_luma &
                                            HVSRC_INIT_HORIZONTAL_LUMA_INIT_PHASE_MASK);

        if ( param_3d->output_3d_format == FORMAT_3DTV_SBS ) {
                hqvdp_cmd->Top.MemFormat &= ~TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK;
                hqvdp_cmd->Top.MemFormat |= ((FORMAT_2DTV << TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT) & TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK);
                hqvdp_cmd->Top.MemFormat &= ~TOP_MEM_FORMAT_RIGHTNOTLEFT_MASK;
                hqvdp_cmd->Top.MemFormat |= ((side << TOP_MEM_FORMAT_RIGHTNOTLEFT_SHIFT) & TOP_MEM_FORMAT_RIGHTNOTLEFT_MASK);
        } else if ( param_3d->output_3d_format == FORMAT_3DTV_TAB ) {
                hqvdp_cmd->Top.InputViewportOriRight = hqvdp_cmd->Top.InputViewportOri;
        } else if ( param_3d->output_3d_format == FORMAT_3DTV_FP ) {
                hqvdp_cmd->Top.InputViewportOriRight = hqvdp_cmd->Top.InputViewportOri;
        }

        if ( side == RIGHT ) {
                /* update motion buffer address */
                hqvdp_cmd->Csdi.PrevMotion += hdl->motion_buffer_size/ 2;
                hqvdp_cmd->Csdi.CurMotion  += hdl->motion_buffer_size/ 2;
                hqvdp_cmd->Csdi.NextMotion += hdl->motion_buffer_size/ 2;
        }
}

/** @brief update command according to pixel repeat
    @ingroup  lld_local_function

    @param[in] output_width  : original output width of HVSRC
    @param[in,out] hqvdp_cmd : command to update

*/
void hqvdp_update_cmd_for_pixel_repeat(uint16_t output_width,
                      HQVDPLITE_CMD_t *hqvdp_cmd)
{

        /* clear field and put new value */
        hqvdp_cmd->Hvsrc.OutputPictureSize &= ~HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_MASK;
        hqvdp_cmd->Hvsrc.OutputPictureSize |= output_width/  2;

}

/*!
 * /brief Configure parameter of dual HQVDP in case of Sbs 3Dtv mode
 * @param[in] hqvdpFullParam
 * @param[out] hqvdpLeftParam
 * @param[out] hqvdpRightParam
 */
void HQVDPSplitSbsProgrammation(struct cmd_origi_param hqvdpFullParam ,
                         struct cmd_split_param *hqvdpLeftParam,
                         struct cmd_split_param *hqvdpRightParam){

      hqvdpLeftParam->input_viewport_width = hqvdpFullParam.input_viewport_width;
      hqvdpLeftParam->output_width         = hqvdpFullParam.output_width;
      hqvdpLeftParam->input_viewportx      = hqvdpFullParam.input_viewportx;
      hqvdpLeftParam->init_phase_luma      = hqvdpFullParam.init_phase_luma;
      hqvdpLeftParam->crop_xdo             = 0;
      hqvdpLeftParam->crop_xds             = 0;

      hqvdpRightParam->input_viewport_width = hqvdpFullParam.input_viewport_width;
      hqvdpRightParam->output_width         = hqvdpFullParam.output_width;
      hqvdpRightParam->input_viewportx      = hqvdpFullParam.input_viewportx;
      hqvdpRightParam->init_phase_luma      = hqvdpFullParam.init_phase_luma;
      hqvdpRightParam->crop_xdo             = 0;
      hqvdpRightParam->crop_xds             = 0;

}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
/** @brief get parameters impacted by split from original parameters
    @ingroup  lld_local_function

    @note this function has to be called only when split is required
*/
void hqvdp_prepare_split(const struct cmd_origi_param *full_param, /**< [in]: parameters of original command */
                         const struct cmd_origi_param_3d *param_3d, /**< [in]: 3d specific parameters of original command */
                         struct cmd_split_param side_param[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE]) /**< [out]: table containing parameter to apply on each HQVDP */
{
      struct error_split_param error_split;

        if ( param_3d->output_3d_format == FORMAT_3DTV_SBS ) {
                HQVDPSplitSbsProgrammation(*full_param, &side_param[0], &side_param[1]);

                /* update xdo for VideoPlug */
                side_param[0].xdo = full_param->xdo;
                side_param[1].xdo = full_param->xdo + full_param->output_width;
        } else {
                error_split = HQVDPSplitProgrammation(*full_param, &side_param[0], &side_param[1]);

                /* update xdo for VideoPlug */
                side_param[0].xdo = full_param->xdo;
                side_param[1].xdo = full_param->xdo + full_param->output_width/2 -
                                    side_param[1].crop_xdo;
        }

        /* patch result of command */
        hqvdp_check_split_result(&side_param[0]);
        hqvdp_check_split_result(&side_param[1]);

}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
/** @brief Build video plug setup from result of split function
    @ingroup lld_internal_function
*/
void hqvdp_update_video_plug_setup(const struct cmd_split_param *new_param, /**< [in]: parameter calculated by split function */
                                   const struct cmd_origi_param_3d *param_3d, /**<[in]: 3d specific info */
                                   const uint16_t *top_left_y, /**< [in]: Unmodified top left y of HQVDP output */
                                   const uint16_t *output_height, /**< [in]: height of the output */
                                   uint8_t hw_to_use_cnt, /**< [in]: number of hw to use */
                                   struct hqvdp_lld_video_plug_setup_s *setup /**< [out]: one video plug setup */
                                   )
{
        uint16_t video_plug_width;
        uint16_t video_plug_height;

        /* adjust width according to 3D mode */
        switch ( param_3d->output_3d_format) {
        case  FORMAT_3DTV_SBS:
                if ( hw_to_use_cnt > 1 )
                        video_plug_width = new_param->output_width;
                else
                        video_plug_width = new_param->output_width * 2;
                break;
        default:
                video_plug_width = new_param->output_width;
                break;
        }

        /* manage additional band in case of flat3d */
        if (param_3d->flat3d == 1 && param_3d->output_3d_format != FORMAT_2DTV) {
          if (param_3d->output_3d_format == FORMAT_3DTV_SBS)
            video_plug_width +=
                param_3d->left_view_border_width_left +
                param_3d->left_view_border_width_right +
                param_3d->right_view_border_width_left +
                param_3d->right_view_border_width_right;

          else
            video_plug_width += param_3d->left_view_border_width_left +
            param_3d->left_view_border_width_right;

        }

        video_plug_height = *output_height;

        /* adjust height according to 3D mode */
        if ( param_3d->output_3d_format == FORMAT_3DTV_TAB )
                video_plug_height = 2 * video_plug_height;

        setup->top_left_x = new_param->xdo;

        setup->bottom_right_x = new_param->xdo + video_plug_width - 1;
        /* y is not impacted by split */
        setup->top_left_y = *top_left_y;
        setup->bottom_right_y = *top_left_y + video_plug_height - 1;

        setup->crop_left = new_param->crop_xdo;
        setup->crop_right = new_param->crop_xds;


}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void hqvdp_write_cmd(hqvdp_lld_hdl_t hdl,
                    const struct hqvdp_lld_conf_s *expected_conf,
                    uint8_t hw_to_use_cnt,
                    struct hqvdp_lld_compo_setup_s compo_setup[]
                   )
{
        struct cmd_origi_param full_param;
        struct cmd_origi_param_3d param_3d;
        struct cmd_split_param side_param[2];
        uint16_t output_height;
        uint16_t output_width;
        int i;

        /* height */
        output_height =
                (expected_conf->Hvsrc.OutputPictureSize
                & HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_MASK)
                >> HVSRC_OUTPUT_PICTURE_SIZE_HEIGHT_SHIFT;
        output_width =
                expected_conf->Hvsrc.OutputPictureSize
                & HVSRC_OUTPUT_PICTURE_SIZE_WIDTH_MASK;
        param_3d.output_3d_format =
                 (expected_conf->Top.MemFormat
                 & TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_MASK)
                 >> TOP_MEM_FORMAT_OUTPUT_3D_FORMAT_SHIFT;
        param_3d.rightnotleft =
                (expected_conf->Top.MemFormat
                & TOP_MEM_FORMAT_RIGHTNOTLEFT_MASK)
                >> TOP_MEM_FORMAT_RIGHTNOTLEFT_SHIFT;
        param_3d.flat3d =
                (expected_conf->Top.MemFormat
                & TOP_MEM_FORMAT_FLAT3D_MASK)
                >> TOP_MEM_FORMAT_FLAT3D_SHIFT;
        /* following parameters are meaningful only in specific case */
        if ( param_3d.flat3d == 1 && param_3d.output_3d_format != FORMAT_2DTV) {
                param_3d.left_view_border_width_left =
                                (expected_conf->Top.LeftViewBorderWidth
                                & TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_MASK)
                                >> TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_SHIFT;
                param_3d.left_view_border_width_right =
                                (expected_conf->Top.LeftViewBorderWidth
                                & TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_MASK)
                                >> TOP_LEFT_VIEW_BORDER_WIDTH_RIGHT_SHIFT;
                param_3d.right_view_border_width_left =
                                (expected_conf->Top.RightViewBorderWidth
                                & TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_MASK)
                                >> TOP_LEFT_VIEW_BORDER_WIDTH_LEFT_SHIFT;
                param_3d.right_view_border_width_right =
                                (expected_conf->Top.RightViewBorderWidth
                                & TOP_RIGHT_VIEW_BORDER_WIDTH_RIGHT_MASK)
                                >> TOP_RIGHT_VIEW_BORDER_WIDTH_RIGHT_SHIFT;
        } else {
                param_3d.left_view_border_width_left = 0;
                param_3d.left_view_border_width_right = 0;
                param_3d.right_view_border_width_left = 0;
                param_3d.right_view_border_width_right = 0;
        }

        if ( hw_to_use_cnt > 1 ) {
                TRC(TRC_ID_HQVDPLITE_LLD, "SPLIT ACTIVATED");

                /* pick input from original command */
                /* HQVDP part */
                full_param.input_viewport_width =
                        expected_conf->Top.InputViewportSize
                        & TOP_INPUT_VIEWPORT_SIZE_WIDTH_MASK;
                full_param.input_viewportx =
                        expected_conf->Top.InputViewportOri
                        & TOP_INPUT_VIEWPORT_ORI_VIEWPORTX_MASK;
                full_param.output_width = output_width;
                full_param.init_phase_luma =
                        expected_conf->Hvsrc.InitHorizontal
                        & HVSRC_INIT_HORIZONTAL_LUMA_INIT_PHASE_MASK;
                /* For VideoPlug part */
                full_param.xdo = expected_conf->Out.output_window_top_left_x;



                /* compute parameters to be able to update command */
                hqvdp_prepare_split(&full_param, &param_3d, side_param);
        } else {
                TRC(TRC_ID_HQVDPLITE_LLD, "SPLIT off");
                /* dummy initialization should not be used by hqvdp_update_video_plug_setup*/
                side_param[0].init_phase_luma = 0;
                side_param[0].input_viewport_width = 0;
                side_param[0].input_viewportx = 0;

                /* initialize to correct value */
                side_param[0].output_width = output_width;
                /* For VideoPlug part */
                side_param[0].crop_xdo = 0;
                side_param[0].crop_xds = 0;
                side_param[0].xdo = expected_conf->Out.output_window_top_left_x;
        }


        /* TODO first implementation where parameters to updated are updated
           in a second time */
        for (i = 0; i < hw_to_use_cnt; i++) {
                /*
                 * i = 0 => hqvdp left
                 * i = 1 => hqvdp right
                 * */
                TRC(TRC_ID_HQVDPLITE_LLD, "(HQVDP %p) Write HQVDP command at logical address %p",
                      hdl->base_addr[i], hdl->ctxt_cmd[i].cmd_next_vsync.logical);
                MEMCPY(hdl->ctxt_cmd[i].cmd_next_vsync.logical, expected_conf,
                       sizeof(HQVDPLITE_CMD_t) );

                /* by default, pixel repeat is disable */
                compo_setup[i].plug_setup.pixel_repeat = false;

                /* patch command only in case of split */
                if (hw_to_use_cnt > 1) {
                        hqvdp_update_cmd(&side_param[i],
                                        (HQVDPLITE_CMD_t*) hdl->ctxt_cmd[i].cmd_next_vsync.logical,
                                        (enum side_e) i,
                                        &param_3d,
                                        hdl);
                } else if (hdl->pixel_repeat) {
                        TRC(TRC_ID_HQVDPLITE_LLD, "PIXEL_REPEAT on");
                        hqvdp_update_cmd_for_pixel_repeat(side_param[i].output_width,
                                        (HQVDPLITE_CMD_t*) hdl->ctxt_cmd[i].cmd_next_vsync.logical);
                        compo_setup[i].plug_setup.pixel_repeat =
                                        hdl->pixel_repeat;
                } else {
                        TRC(TRC_ID_HQVDPLITE_LLD, "PIXEL_REPEAT off");
                }

                /* build video parameters to export */
                compo_setup[i].is_active = true;
                compo_setup[i].mixer_bit = hdl->mixer_bit[i];
                compo_setup[i].plug_setup.video_plug_handle = hdl->video_plug_handle[i];
                hqvdp_update_video_plug_setup(&side_param[i],
                                              &param_3d,
                                              (uint16_t *)&(expected_conf->Out.output_window_top_left_y),
                                              &(output_height),
                                              hw_to_use_cnt,
                                              &(compo_setup[i].plug_setup)
                                              );
        }

        /* Disable the unused HQVDP */
        for(i = hw_to_use_cnt; (i < hdl->reserved_resources) && (i<(sizeof(hdl->mixer_bit)/(sizeof(hdl->mixer_bit[0])))); i++) {
            /* Both is_active and mixer_bit are required so that upper layer */
            /* can disable the mixer input accordingly                       */
            compo_setup[i].is_active = false;
            compo_setup[i].mixer_bit = hdl->mixer_bit[i];
        }

        /* ensure that command register access is not done before end of
         * write in memory
         */
        WRITE_MB();

        /*
          * Program _all_ hardwares associated to the session:
          * - If color fill is enabled, send null command to all HW instances to ensure that
          *   no pixels are produced since pixels will be generated by the video plug(s).
          * - If HW instance is inactive then send a null command to ensure that HW will not
          *   process an old command.
          * - Else send the HW instance command computed above.
          */
        for (i  = 0; i < hdl->reserved_resources; i++) {
                if ( expected_conf->VideoPlug.color_fill_enabled
                  || !compo_setup[i].is_active){
                        c8fvp3_SetCmdAddress(hdl->base_addr[i],
                                             (uint32_t)NULL);
                } else {
                        /* write command address on side  */
                        c8fvp3_SetCmdAddress(hdl->base_addr[i],
                                             (uint32_t)hdl->ctxt_cmd[i].cmd_next_vsync.physical);
                }
        }
}

/******************************************************************************/
/******************************************************************************/


/* TODO VSYNC Mode has to be set externally */

/* Enable the xp70 to start on hard Vsync */
/* c8fvp3_SetVsyncMode((uint32_t)base_addr, mailbox_SOFT_VSYNC_VSYNC_MODE_HW); */
/*****************************************************************************/
