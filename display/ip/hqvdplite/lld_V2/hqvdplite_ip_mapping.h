/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _HQVDPLITE_IP_MAPPING_H_
#define _HQVDPLITE_IP_MAPPING_H_

/*  CONTROL CLUSTER */
#ifdef FW_STXP70
  #define   HQVDP_CTRL_CLUSTER_DMEM_OFFSET        (0x00000000)
  #define   HQVDP_CTRL_CLUSTER_DMEM_SIZE          (0x00040000)
  #define   HQVDP_CTRL_CLUSTER_IMEM_OFFSET        (0x00400000)
  #define   HQVDP_CTRL_CLUSTER_IMEM_SIZE          (0x00040000)
  #define   HQVDP_LITE_MEGACELL_OFFSET            (0x00800000)
  #define   HQVDP_LITE_BASE_ADDR                  (0x00000000)
#else
  #define   HQVDP_CTRL_CLUSTER_DMEM_OFFSET        (0x00000000)
  #define   HQVDP_CTRL_CLUSTER_DMEM_SIZE          (0x00040000)
  #define   HQVDP_CTRL_CLUSTER_IMEM_OFFSET        (0x00040000)
  #define   HQVDP_CTRL_CLUSTER_IMEM_SIZE          (0x00040000)
  #define   HQVDP_LITE_MEGACELL_OFFSET            (0x00000000)
#endif

/*  COMMUNICATION CLUSTER */
#define HQVDP_LITE_STR_WRITE_LRAM_OFFSET          (HQVDP_LITE_MEGACELL_OFFSET + 0x00080000)
#define HQVDP_LITE_STR_WRITE_LRAM_SIZE            (0x00018000)
#define HQVDP_LITE_STR_WRITE_REGS_OFFSET          (HQVDP_LITE_MEGACELL_OFFSET + 0x00098000)
#define HQVDP_LITE_STR_WRITE_REGS_SIZE            (0x00008000)

#define HQVDP_LITE_STR_READ_LRAM_OFFSET           (HQVDP_LITE_MEGACELL_OFFSET + 0x000A0000)
#define HQVDP_LITE_STR_READ_LRAM_SIZE             (0x00018000)
#define HQVDP_LITE_STR_READ_REGS_OFFSET           (HQVDP_LITE_MEGACELL_OFFSET + 0x000B8000)
#define HQVDP_LITE_STR_READ_REGS_SIZE             (0x00008000)

#define HQVDP_LITE_STR_INTER_LRAM_OFFSET          (HQVDP_LITE_MEGACELL_OFFSET + 0x000C0000)
#define HQVDP_LITE_STR_INTER_LRAM_SIZE            (0x00018000)
#define HQVDP_LITE_STR_INTER_REGS_OFFSET          (HQVDP_LITE_MEGACELL_OFFSET + 0x000D8000)
#define HQVDP_LITE_STR_INTER_REGS_SIZE            (0x00008000)

/*  T1 PERIPHERALS */
#define HQVDP_LITE_PLUG_READ_UCODE_OFFSET         (HQVDP_LITE_MEGACELL_OFFSET + 0x000E0000)
#define HQVDP_LITE_PLUG_READ_UCODE_SIZE           (0x00001000)
#define HQVDP_LITE_PLUG_READ_REGS_OFFSET          (HQVDP_LITE_MEGACELL_OFFSET + 0x000E1000)
#define HQVDP_LITE_PLUG_READ_REGS_SIZE            (0x00001000)

#define HQVDP_LITE_PLUG_WRITE_UCODE_OFFSET        (HQVDP_LITE_MEGACELL_OFFSET + 0x000E2000)
#define HQVDP_LITE_PLUG_WRITE_UCODE_SIZE          (0x00001000)
#define HQVDP_LITE_PLUG_WRITE_REGS_OFFSET         (HQVDP_LITE_MEGACELL_OFFSET + 0x000E3000)
#define HQVDP_LITE_PLUG_WRITE_REGS_SIZE           (0x00001000)

#define HQVDP_LITE_MAILBOX_REGS_OFFSET            (HQVDP_LITE_MEGACELL_OFFSET + 0x000E4000)
#define HQVDP_LITE_MAILBOX_REGS_SIZE              (0x00002000)

#define HQVDP_LITE_COM_PU_REGS_OFFSET             (HQVDP_LITE_MEGACELL_OFFSET + 0x000E6000)
#define HQVDP_LITE_COM_PU_REGS_SIZE               (0x00002000)

#define HQVDP_LITE_FMD_PU_REGS_OFFSET             (HQVDP_LITE_MEGACELL_OFFSET + 0x000E8000)
#define HQVDP_LITE_FMD_PU_REGS_SIZE               (0x00002000)

#define HQVDP_LITE_CSDI_PU_REGS_OFFSET            (HQVDP_LITE_MEGACELL_OFFSET + 0x000EA000)
#define HQVDP_LITE_CSDI_PU_REGS_SIZE              (0x00002000)

#define HQVDP_LITE_HVSRC_PU_REGS_OFFSET           (HQVDP_LITE_MEGACELL_OFFSET + 0x000EC000)
#define HQVDP_LITE_HVSRC_PU_REGS_SIZE             (0x00002000)

#define HQVDP_LITE_IQI_PU_REGS_OFFSET             (HQVDP_LITE_MEGACELL_OFFSET + 0x000EE000)
#define HQVDP_LITE_IQI_PU_REGS_SIZE               (0x00002000)

/* Manual definition of version register added from Orly3 */
#define HQVDP_MBX_VERSION_OFFSET          (HQVDP_LITE_MAILBOX_REGS_OFFSET + 0x030)

/* Manual defninition of the hardware size of the PMEM and DMEM */
#define HQVDP_DMEM_SIZE (0x4000)
#define HQVDP_PMEM_SIZE (0x10000)

#if 0
#define HQVDP_MAPPING_RDPLUG_BASEADDR 0xE0000
#define HQVDP_MAPPING_WRPLUG_BASEADDR 0xE2000

#define HQVDP_MAPPING_DMEM_BASEADDR 0x00000
#define HQVDP_MAPPING_IMEM_BASEADDR 0x40000

/* exported defines to drivers */
#define HQVDP_STR64_RDPLUG_OFFSET         (0x0e0000)
#define HQVDP_STR64_WRPLUG_OFFSET         (0x0e2000)
/* manual definition of HQVDP_LITE_MBX_BASEADDR */
#define HQVDP_MBX_OFFSET                  (0x0e4000)
#define HQVDP_PMEM_OFFSET                 (0x040000)
#define HQVDP_DMEM_OFFSET                 (0x000000)
#define HQVDP_LITE_STR_READ_REGS_BASEADDR (0x0b8000)
#endif

#endif /* _HQVDPLITE_IP_MAPPING_H_ */
