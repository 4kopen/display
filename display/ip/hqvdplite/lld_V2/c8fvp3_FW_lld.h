/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*!
 * \file c8fvp3_FW_lld.h
 *
 */

#ifndef C8FVP3_FW_LLD_H_
#define C8FVP3_FW_LLD_H_

#include "c8fvp3_FW_lld_global.h"
#include "c8fvp3_FW_download_code.h"
#include "c8fvp3_FW_plug.h"
#include "c8fvp3_HVSRC_Lut.h"

/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/**
    @brief SOft reset IP

    @param[in] base_addr base address of the IP
 */
void hqvdp_soft_reset(void *base_addr);

/*!
 * \brief Do initialization of the IP
 *
 * \param[in] base_addr base address of the IP
 */
void c8fvp3_BootIp(void *base_addr);

/*!
 * \brief to wait until reset is done
 *
 * \param[in] base_address base address of the IP
 */
bool c8fvp3_IsBootIpDone (void *base_address);

/*!
 * \brief Do initialization of the vsync mode
 *
 * \param[in] base_addr base address of the IP
 * \param[in] vsyncmode as define in the mailbox register definition and c8fvp3_hqvdplite_api_define.h
 */
void c8fvp3_SetVsyncMode(void *base_addr, uint32_t vsyncmode);

/** @brief Change VSYNC mode of HQVDP(s) attached to a given handle

    @ingroup  lld_function
    @param[in] hdl : handle to LLD session
    @param[in] vsync_mode : synchronization mode

    @pre #hqvdp_lld_open has been called
    @note It is recommended to directly select expected mode during
          #hqvdp_lld_open
*/
void hqvdp_set_vsync_mode(hqvdp_lld_hdl_t hdl,
                              enum hqvdp_lld_vsync_mode_e vsync_mode);

/*!
 * \brief Generate a soft vsync
 *
 * \param[in] base_addr base address of the IP
 */
void c8fvp3_GenSoftVsync(void *base_addr);

/*!
 * \brief Do initialization of the next command address
 *
 * \param[in] base_addr base address of the IP
 * \param[in] cmd_address address of the next command to process in LMI
 */
extern void c8fvp3_SetCmdAddress(void *base_addr, uint32_t cmd_address);

/*!
 * \brief return true when FW ready to process a command
 *
 * \param[in] base_addr base address of the IP
 */
bool c8fvp3_IsReady(void *base_addr);

/*!
 * \brief Set HVSRC Lut according to command settings and filter strength
 *
 * \param[out] FD_HQVDP_Cmd_p command to be process, LUT are set by this function
 * \param[in] filter_strength_yv vertical luma filter strength
 * \param[in] filter_strength_cv vertical chroma filter strength
 * \param[in] filter_strength_yh horizontal luma filter strength
 * \param[in] filter_strength_ch horizontal chroma filter strength
 */
void c8fvp3_SetHvsrcLut(HQVDPLITE_CMD_t *FD_HQVDP_Cmd_p,
                        uint8_t filter_strength_yv,
                        uint8_t filter_strength_cv,
                        uint8_t filter_strength_yh,
                        uint8_t filter_strength_ch);

/*!
 * \brief Set 3D Viewport according to left viewport and Offset3D.
 *  the command provide input viewport size/origin and initphases
 *  all the information related to input and output format must be set before calling this.
 *
 * \param[out] FD_HQVDP_Cmd_p command to be process, 3D viewport are set
 * \param[in]  offset3D is a lateral offset between 2 views, this function is neutral if Offset3D=0
 */
enum hqvdp_lld_error c8fvp3_Set3DViewPort(HQVDPLITE_CMD_t *FD_HQVDP_Cmd_p,
                                          int16_t offset3D);

/*!
 * \brief Get status from dmem.
 *
 * \param[in]  base_addr : base address of the IP
 * \param[out] FD_HQVDP_Status_p : pointer to the status structure to be fill
 *
 * return true when get status succeed
 */
bool c8fvp3_GetStatus(void * base_addr,
                      HQVDPLITE_STATUS_t *FD_HQVDP_Status_p);

/* C++ support */
#ifdef __cplusplus
}
#endif

#endif

