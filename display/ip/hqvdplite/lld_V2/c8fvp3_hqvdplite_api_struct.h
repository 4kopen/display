/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE
#define _C8FVP3_HQVDPLITE

#include "c8fvp3_hqvdplite_api_defines.h"
#include "c8fvp3_hqvdplite_api_TOP.h"
#include "c8fvp3_hqvdplite_api_VC1RE.h"
#include "c8fvp3_hqvdplite_api_FMD.h"
#include "c8fvp3_hqvdplite_api_CSDI.h"
#include "c8fvp3_hqvdplite_api_HVSRC.h"
#include "c8fvp3_hqvdplite_api_IQI.h"
#include "c8fvp3_hqvdplite_api_TOP_STATUS.h"
#include "c8fvp3_hqvdplite_api_FMD_STATUS.h"
#include "c8fvp3_hqvdplite_api_CSDI_STATUS.h"
#include "c8fvp3_hqvdplite_api_HVSRC_STATUS.h"
#include "c8fvp3_hqvdplite_api_IQI_STATUS.h"

#ifndef HQVDPLITE_API_FOR_STAPI
typedef struct {
  s_TOP TOP;
  s_VC1RE VC1RE;
  s_FMD FMD;
  s_CSDI CSDI;
  s_HVSRC HVSRC;
  s_IQI IQI;
} c8fvp3_hqvdplite_api_struct_t;

typedef struct {
  s_TOP_STATUS TOP_STATUS;
  s_FMD_STATUS FMD_STATUS;
  s_CSDI_STATUS CSDI_STATUS;
  s_HVSRC_STATUS HVSRC_STATUS;
  s_IQI_STATUS IQI_STATUS;
} c8fvp3_hqvdplite_api_struct_status_t;

#endif /* HQVDPLITE_API_FOR_STAPI */

#ifdef FW_STXP70
/* When only firmware is compiled used old names */
#define c8fvp3_hqvdplite_api_TOP_BASE_ADDR (0)
#define c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR (c8fvp3_hqvdplite_api_TOP_BASE_ADDR +sizeof(s_TOP))
#define c8fvp3_hqvdplite_api_FMD_BASE_ADDR (c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR +sizeof(s_VC1RE))
#define c8fvp3_hqvdplite_api_CSDI_BASE_ADDR (c8fvp3_hqvdplite_api_FMD_BASE_ADDR +sizeof(s_FMD))
#define c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR +sizeof(s_CSDI))
#define c8fvp3_hqvdplite_api_IQI_BASE_ADDR (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR +sizeof(s_HVSRC))
#define c8fvp3_hqvdplite_api_TOP_STATUS_BASE_ADDR (0)
#define c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_TOP_STATUS_BASE_ADDR +sizeof(s_TOP_STATUS))
#define c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR +sizeof(s_FMD_STATUS))
#define c8fvp3_hqvdplite_api_HVSRC_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR +sizeof(s_CSDI_STATUS))
#define c8fvp3_hqvdplite_api_IQI_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_HVSRC_STATUS_BASE_ADDR +sizeof(s_HVSRC_STATUS))

#define c8fvp3_hqvdplite_api_COMMAND_OFFSET (0)
#define c8fvp3_hqvdplite_api_COMMAND_SIZE (sizeof(s_TOP) + sizeof(s_VC1RE) + sizeof(s_FMD) + sizeof(s_CSDI) + sizeof(s_HVSRC) + sizeof(s_IQI))
#define c8fvp3_hqvdplite_api_STATUS_OFFSET (0)
#define c8fvp3_hqvdplite_api_STATUS_SIZE (sizeof(c8fvp3_hqvdplite_api_struct_status_t))
#else
/* When compiling test + LLD use structure exported to drivers */
#define c8fvp3_hqvdplite_api_TOP_BASE_ADDR (0)
#define c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR (c8fvp3_hqvdplite_api_TOP_BASE_ADDR +sizeof(HQVDPLITE_TOP_Params_t))
#define c8fvp3_hqvdplite_api_FMD_BASE_ADDR (c8fvp3_hqvdplite_api_VC1RE_BASE_ADDR +sizeof(HQVDPLITE_VC1RE_Params_t))
#define c8fvp3_hqvdplite_api_CSDI_BASE_ADDR (c8fvp3_hqvdplite_api_FMD_BASE_ADDR +sizeof(HQVDPLITE_FMD_Params_t))
#define c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR (c8fvp3_hqvdplite_api_CSDI_BASE_ADDR +sizeof(HQVDPLITE_CSDI_Params_t))
#define c8fvp3_hqvdplite_api_IQI_BASE_ADDR (c8fvp3_hqvdplite_api_HVSRC_BASE_ADDR +sizeof(HQVDPLITE_HVSRC_Params_t))
#define c8fvp3_hqvdplite_api_TOP_STATUS_BASE_ADDR (0)
#define c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_TOP_STATUS_BASE_ADDR +sizeof(HQVDPLITE_TOPStatus_Params_t))
#define c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_FMD_STATUS_BASE_ADDR +sizeof(HQVDPLITE_FMDStatus_Params_t))
#define c8fvp3_hqvdplite_api_HVSRC_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_CSDI_STATUS_BASE_ADDR +sizeof(HQVDPLITE_CSDIStatus_Params_t))
#define c8fvp3_hqvdplite_api_IQI_STATUS_BASE_ADDR (c8fvp3_hqvdplite_api_HVSRC_STATUS_BASE_ADDR +sizeof(HQVDPLITE_HVSRCStatus_Params_t))

#define HQVDPLITE_COMMAND_OFFSET (0)
#define HQVDPLITE_COMMAND_SIZE (sizeof(HQVDPLITE_TOP_Params_t) + sizeof(HQVDPLITE_VC1RE_Params_t) + sizeof(HQVDPLITE_FMD_Params_t) + sizeof(HQVDPLITE_CSDI_Params_t) + sizeof(HQVDPLITE_HVSRC_Params_t) + sizeof(HQVDPLITE_IQI_Params_t))
#define HQVDPLITE_STATUS_OFFSET (0)
#define HQVDPLITE_STATUS_SIZE (sizeof(HQVDPLITE_TOPStatus_Params_t) + sizeof(HQVDPLITE_FMDStatus_Params_t) + sizeof(HQVDPLITE_CSDIStatus_Params_t) + sizeof(HQVDPLITE_HVSRCStatus_Params_t) + sizeof(HQVDPLITE_IQIStatus_Params_t))
#endif


#ifndef FW_STXP70

typedef struct {
  HQVDPLITE_TOP_Params_t Top;
  HQVDPLITE_VC1RE_Params_t Vc1re;
  HQVDPLITE_FMD_Params_t Fmd;
  HQVDPLITE_CSDI_Params_t Csdi;
  HQVDPLITE_HVSRC_Params_t Hvsrc;
  HQVDPLITE_IQI_Params_t Iqi;
} HQVDPLITE_CMD_t;

typedef struct {
  HQVDPLITE_TOPStatus_Params_t TopStatus;
  HQVDPLITE_FMDStatus_Params_t FmdStatus;
  HQVDPLITE_CSDIStatus_Params_t CsdiStatus;
  HQVDPLITE_HVSRCStatus_Params_t HvsrcStatus;
  HQVDPLITE_IQIStatus_Params_t IqiStatus;
} HQVDPLITE_STATUS_t;

#endif /* FW_STXP70 */

#endif
