/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "hqvdp_split_algo.h"

#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)

/* Min width that HQVDP can process */
#define HQVDP_MIN_WIDTH 48

/* Max Size that can produce HQVD to 60fps */
#define HQVDP_MAX_WIDTH 2258

int hqvdp_abs (int n)
{
        if (n >= 0)
                return n;
        else
                return -n;
}

int Clamp(int var, int min, int max)
{
        return min(max(var,min),max);
}
/*!
 * /brief give Increment for one dimension size
 * @param[in] SizeInput
 * @param[in] SizeOutput
 * return increment
 */
int getIncrement(int SizeInput, int SizeOutput)
{
        int Incr;
        Incr = (((256 * 32 * SizeInput) / SizeOutput) >> 1) << 1;
        return Incr;
}

/*!
 * /brief give Initial phase
 * @param[in] SizeInput
 * @param[in] SizeOutput
 * @param[in] Incr
 * @param[in] Offset initial phase set by user
 */
int getInitPhase(int SizeInput, int SizeOutput, int Incr, int Offset)
{
        int InitPhase;
        InitPhase = ((int) (256 * 32 * SizeInput) - (int) (SizeOutput * Incr)) / 2 + 256 * Offset;
        return InitPhase;
}


/* HQVDP Programmation */
struct error_split_param  HQVDPSplitProgrammation(struct cmd_origi_param hqvdpFullParam ,
                             struct cmd_split_param *hqvdpLeftParam,
                             struct cmd_split_param *hqvdpRightParam
                             )
{
    struct error_split_param debugError;
    int i;
    int ErrorIncr, ErrorPhaseLeft, ErrorPhaseRight, Error, ErrorMin;
    int IncrHOrigine, IncrHSplit;
    int InitialPhaseHOrigine, InitialPhaseHSplit;
    int HSplitInput=0, HSplitOutput=0, HSplitInput_tmp, HSplitOutput_tmp;
    int InitialphaseLeft=0, InitialphaseRight=0, InitialphaseLeft_tmp,
        InitialphaseRight_tmp;
    int widthIn, widthOut;
    int round;
    widthIn = (int) hqvdpFullParam.input_viewport_width;
    widthOut = (int) hqvdpFullParam.output_width;
    ErrorMin = 0;
    debugError = (struct error_split_param) {0,0,0,0};
    /* calculation of increment and initial phase in Firmware for whole image */
    IncrHOrigine = getIncrement(widthIn, widthOut);
    InitialPhaseHOrigine = getInitPhase(widthIn, widthOut, IncrHOrigine,
        (int) hqvdpFullParam.init_phase_luma);

    /* Optimization of overlap */
    for (i = 4; i < 64; i++) {
      HSplitInput_tmp = 2 * (((widthIn + 2) >> 2) + i);
      if (HSplitInput_tmp < HQVDP_MIN_WIDTH) {
          HSplitInput_tmp = HQVDP_MIN_WIDTH;
      }
      /* calculation of OutputSize for 1/2 image */
      HSplitOutput_tmp = (((2*256 * 32 * HSplitInput_tmp) / (IncrHOrigine)+2)
          >> 2) << 1;
      if (HSplitOutput_tmp >= HQVDP_MAX_WIDTH)
        break;
      if (HSplitOutput_tmp > widthOut)
        break;
      /* calculation of increment and initial phase in Firmware for 1/2 image */
      IncrHSplit = getIncrement(HSplitInput_tmp, HSplitOutput_tmp);
      InitialPhaseHSplit = getInitPhase(HSplitInput_tmp, HSplitOutput_tmp,
          IncrHSplit, 0);

      /*Set initial phase to compensate error of dual HQVDP */
      round = ((InitialPhaseHOrigine - InitialPhaseHSplit) < 0 ? -128 : 128);

      ErrorPhaseLeft = Clamp(
          ((InitialPhaseHOrigine - InitialPhaseHSplit + round) / 256), 0,
          31);

      ErrorPhaseRight = (IncrHSplit * (widthOut - HSplitOutput_tmp)
          - 32 * 256 * (widthIn - HSplitInput_tmp));

      if (ErrorPhaseRight < 0) {
        ErrorPhaseRight = Clamp((-ErrorPhaseRight + 128) / 256, 0, 31);
        if (ErrorPhaseLeft >= ErrorPhaseRight) {
          InitialphaseLeft_tmp = ErrorPhaseLeft;
          InitialphaseRight_tmp = ErrorPhaseLeft - ErrorPhaseRight;
        } else {
          InitialphaseLeft_tmp = ErrorPhaseRight;
          InitialphaseRight_tmp = 0;
        }

      } else {
        ErrorPhaseRight = Clamp((ErrorPhaseRight + 128) / 256, 0, 31);
        if ((ErrorPhaseLeft + ErrorPhaseRight) < 32) {
          InitialphaseLeft_tmp = ErrorPhaseLeft;
          InitialphaseRight_tmp = ErrorPhaseLeft + ErrorPhaseRight;
        } else {
          InitialphaseLeft_tmp = 0;
          InitialphaseRight_tmp = ErrorPhaseRight;
        }

      }
      ErrorPhaseRight = hqvdp_abs(
          IncrHSplit * (widthOut - HSplitOutput_tmp)
              - 32 * 256 * (widthIn - HSplitInput_tmp)
              + 256 * InitialphaseLeft_tmp
              - 256 * InitialphaseRight_tmp);

      ErrorIncr = hqvdp_abs(IncrHOrigine - IncrHSplit);
      ErrorPhaseLeft = hqvdp_abs(
          (InitialphaseLeft_tmp << 8) - InitialPhaseHOrigine
              + InitialPhaseHSplit);

      Error = (ErrorPhaseRight << 2) + ErrorPhaseLeft + (ErrorIncr << 10);

      if (((ErrorMin > Error)) || (i == 4)) {
        ErrorMin = Error;
        HSplitInput = HSplitInput_tmp;
        HSplitOutput = HSplitOutput_tmp;
        InitialphaseRight = InitialphaseRight_tmp;
        InitialphaseLeft = InitialphaseLeft_tmp;
        debugError.ErrorPhaseRight = ErrorPhaseRight;
        debugError.ErrorPhaseLeft = ErrorPhaseLeft;
        debugError.ErrorIncr = ErrorIncr;
        debugError.ErrorAll = ErrorMin;

      }
      if (HSplitInput_tmp >= widthIn)
        break;
    }

    /* Left side programmation*/
    hqvdpLeftParam->input_viewport_width = (uint16_t) HSplitInput;
    hqvdpLeftParam->output_width = (uint16_t) HSplitOutput;
    hqvdpLeftParam->input_viewportx = hqvdpFullParam.input_viewportx;
    hqvdpLeftParam->init_phase_luma = (int8_t) InitialphaseLeft;
    hqvdpLeftParam->crop_xdo = 0;
    hqvdpLeftParam->crop_xds = (uint16_t) (HSplitOutput - widthOut / 2);
    /* Right side programmation*/
    hqvdpRightParam->input_viewport_width = (uint16_t) HSplitInput;
    hqvdpRightParam->output_width = (uint16_t) HSplitOutput;
    hqvdpRightParam->input_viewportx = (uint16_t) (widthIn - HSplitInput)
        + hqvdpFullParam.input_viewportx;
    hqvdpRightParam->init_phase_luma = (int8_t) InitialphaseRight;
    hqvdpRightParam->crop_xdo = (uint16_t) (HSplitOutput - widthOut / 2);
    hqvdpRightParam->crop_xds = 0;
    return debugError;

}



