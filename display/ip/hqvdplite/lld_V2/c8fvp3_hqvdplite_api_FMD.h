/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _C8FVP3_HQVDPLITE_API_FMD_H
#define _C8FVP3_HQVDPLITE_API_FMD_H

#ifdef VERIFICATION_PLATFORM
#include "hqvdp_lld_platform.h"
#endif

/*
* Address Block : FMD
*/


/**
* Register : CONFIG
* Field mode detection parameters
*/

#define FMD_CONFIG_OFFSET                               (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x00)
#define FMD_CONFIG_MASK                                 (0x00000001)

/**
* Bit-field : FMD_ON
* Field mode detection enable bit:
*/

#define FMD_CONFIG_FMD_ON_SHIFT                         (0x00000000)
#define FMD_CONFIG_FMD_ON_WIDTH                         (1)
#define FMD_CONFIG_FMD_ON_MASK                          (0x00000001)

/**
* Register : VIEWPORT_ORI
* Viewport origin specific to the FMD
*/

#define FMD_VIEWPORT_ORI_OFFSET                         (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x04)
#define FMD_VIEWPORT_ORI_MASK                           (0x07FF07FF)

/**
* Bit-field : VIEWPORTX
* it contains the coordinate 'X' of the fmd viewport left corner, expressed in pixels.
*/

#define FMD_VIEWPORT_ORI_VIEWPORTX_SHIFT                (0x00000000)
#define FMD_VIEWPORT_ORI_VIEWPORTX_WIDTH                (11)
#define FMD_VIEWPORT_ORI_VIEWPORTX_MASK                 (0x000007FF)

/**
* Bit-field : VIEWPORTY
* it contains the coordinate 'Y' of the fmd viewport top corner, expressed in pixels.
*/

#define FMD_VIEWPORT_ORI_VIEWPORTY_SHIFT                (0x00000010)
#define FMD_VIEWPORT_ORI_VIEWPORTY_WIDTH                (11)
#define FMD_VIEWPORT_ORI_VIEWPORTY_MASK                 (0x07FF0000)

/**
* Register : VIEWPORT_SIZE
* Viewport size specific to the FMD
*/

#define FMD_VIEWPORT_SIZE_OFFSET                        (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x08)
#define FMD_VIEWPORT_SIZE_MASK                          (0x07FF07FF)

/**
* Bit-field : WIDTH
* it contains the width of the input viewport, expressed in pixels. It's shall be multiple of the with of a block, 40 pixels.
*/

#define FMD_VIEWPORT_SIZE_WIDTH_SHIFT                   (0x00000000)
#define FMD_VIEWPORT_SIZE_WIDTH_WIDTH                   (11)
#define FMD_VIEWPORT_SIZE_WIDTH_MASK                    (0x000007FF)

/**
* Bit-field : HEIGHT
* it contains the height of the input viewport, expressed in pixels. It's shall be multiple of the height of a block, 20 pixels.
*/

#define FMD_VIEWPORT_SIZE_HEIGHT_SHIFT                  (0x00000010)
#define FMD_VIEWPORT_SIZE_HEIGHT_WIDTH                  (11)
#define FMD_VIEWPORT_SIZE_HEIGHT_MASK                   (0x07FF0000)

/**
* Register : NEXT_NEXT_LUMA
* Next next luma buffer base address
*/

#define FMD_NEXT_NEXT_LUMA_OFFSET                       (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x0C)
#define FMD_NEXT_NEXT_LUMA_MASK                         (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma
*/

#define FMD_NEXT_NEXT_LUMA_BASE_ADDR_SHIFT              (0x00000000)
#define FMD_NEXT_NEXT_LUMA_BASE_ADDR_WIDTH              (32)
#define FMD_NEXT_NEXT_LUMA_BASE_ADDR_MASK               (0xFFFFFFFF)

/**
* Register : NEXT_NEXT_RIGHT_LUMA
* Next next right luma buffer base address
*/

#define FMD_NEXT_NEXT_RIGHT_LUMA_OFFSET                 (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x10)
#define FMD_NEXT_NEXT_RIGHT_LUMA_MASK                   (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define FMD_NEXT_NEXT_RIGHT_LUMA_BASE_ADDR_SHIFT        (0x00000000)
#define FMD_NEXT_NEXT_RIGHT_LUMA_BASE_ADDR_WIDTH        (32)
#define FMD_NEXT_NEXT_RIGHT_LUMA_BASE_ADDR_MASK         (0xFFFFFFFF)

/**
* Register : NEXT_NEXT_NEXT_LUMA
* Next next next luma buffer base address
*/

#define FMD_NEXT_NEXT_NEXT_LUMA_OFFSET                  (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x14)
#define FMD_NEXT_NEXT_NEXT_LUMA_MASK                    (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define FMD_NEXT_NEXT_NEXT_LUMA_BASE_ADDR_SHIFT         (0x00000000)
#define FMD_NEXT_NEXT_NEXT_LUMA_BASE_ADDR_WIDTH         (32)
#define FMD_NEXT_NEXT_NEXT_LUMA_BASE_ADDR_MASK          (0xFFFFFFFF)

/**
* Register : NEXT_NEXT_NEXT_RIGHT_LUMA
* Next next next right luma buffer base address
*/

#define FMD_NEXT_NEXT_NEXT_RIGHT_LUMA_OFFSET            (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x18)
#define FMD_NEXT_NEXT_NEXT_RIGHT_LUMA_MASK              (0xFFFFFFFF)

/**
* Bit-field : BASE_ADDR
* This field contains the memory location of the input luma buffer
*/

#define FMD_NEXT_NEXT_NEXT_RIGHT_LUMA_BASE_ADDR_SHIFT   (0x00000000)
#define FMD_NEXT_NEXT_NEXT_RIGHT_LUMA_BASE_ADDR_WIDTH   (32)
#define FMD_NEXT_NEXT_NEXT_RIGHT_LUMA_BASE_ADDR_MASK    (0xFFFFFFFF)

/**
* Register : THRESHOLD_SCD
* Scene change detection threshold
*/

#define FMD_THRESHOLD_SCD_OFFSET                        (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x1C)
#define FMD_THRESHOLD_SCD_MASK                          (0x0003FFFF)

/**
* Bit-field : T_SCENE
* SCD threshold used to define scene_count. This is a threshold of blocks pixels differences sum. Above this threshold, a block is considered as moving
*/

#define FMD_THRESHOLD_SCD_T_SCENE_SHIFT                 (0x00000000)
#define FMD_THRESHOLD_SCD_T_SCENE_WIDTH                 (18)
#define FMD_THRESHOLD_SCD_T_SCENE_MASK                  (0x0003FFFF)

/**
* Register : THRESHOLD_RFD
* Repeat field detection threshold
*/

#define FMD_THRESHOLD_RFD_OFFSET                        (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x20)
#define FMD_THRESHOLD_RFD_MASK                          (0x0003FFFF)

/**
* Bit-field : T_REPEAT
* RFD threshold used to define scene_status. This is a threshold of blocks pixels differences sum. Above this threshold, a block is considered as moving
*/

#define FMD_THRESHOLD_RFD_T_REPEAT_SHIFT                (0x00000000)
#define FMD_THRESHOLD_RFD_T_REPEAT_WIDTH                (18)
#define FMD_THRESHOLD_RFD_T_REPEAT_MASK                 (0x0003FFFF)

/**
* Register : THRESHOLD_MOVE
* Move threshold
*/

#define FMD_THRESHOLD_MOVE_OFFSET                       (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x24)
#define FMD_THRESHOLD_MOVE_MASK                         (0x03FF00FF)

/**
* Bit-field : T_MOVE
* Pixels are said moving pixels when their |A-D | difference is superior to t_mov.
*/

#define FMD_THRESHOLD_MOVE_T_MOVE_SHIFT                 (0x00000000)
#define FMD_THRESHOLD_MOVE_T_MOVE_WIDTH                 (8)
#define FMD_THRESHOLD_MOVE_T_MOVE_MASK                  (0x000000FF)

/**
* Bit-field : T_NNUM_MOV_PIX
* A block is said moving if it contains more than t_num_mov_pix moving pixels. Move status bit is set if at least one block is moving block.
*/

#define FMD_THRESHOLD_MOVE_T_NNUM_MOV_PIX_SHIFT         (0x00000010)
#define FMD_THRESHOLD_MOVE_T_NNUM_MOV_PIX_WIDTH         (10)
#define FMD_THRESHOLD_MOVE_T_NNUM_MOV_PIX_MASK          (0x03FF0000)

/**
* Register : THRESHOLD_CFD
* Consecutive field difference threshold
*/

#define FMD_THRESHOLD_CFD_OFFSET                        (c8fvp3_hqvdplite_api_FMD_BASE_ADDR + 0x28)
#define FMD_THRESHOLD_CFD_MASK                          (0x0000000F)

/**
* Bit-field : T_NOISE
* Min(|A-B|,|A-C|) pixels differences that are inferior to t_noise are considered as noise so not taken into account.
*/

#define FMD_THRESHOLD_CFD_T_NOISE_SHIFT                 (0x00000000)
#define FMD_THRESHOLD_CFD_T_NOISE_WIDTH                 (4)
#define FMD_THRESHOLD_CFD_T_NOISE_MASK                  (0x0000000F)


#ifndef HQVDPLITE_API_FOR_STAPI
typedef struct {
    gvh_u32_t CONFIG;  /* at 0 */
    gvh_u32_t VIEWPORT_ORI;  /* at 4 */
    gvh_u32_t VIEWPORT_SIZE;  /* at 8 */
    gvh_u32_t NEXT_NEXT_LUMA;  /* at 12 */
    gvh_u32_t NEXT_NEXT_RIGHT_LUMA;  /* at 16 */
    gvh_u32_t NEXT_NEXT_NEXT_LUMA;  /* at 20 */
    gvh_u32_t NEXT_NEXT_NEXT_RIGHT_LUMA;  /* at 24 */
    gvh_u32_t THRESHOLD_SCD;  /* at 28 */
    gvh_u32_t THRESHOLD_RFD;  /* at 32 */
    gvh_u32_t THRESHOLD_MOVE;  /* at 36 */
    gvh_u32_t THRESHOLD_CFD;  /* at 40 */
} s_FMD;
#endif /* HQVDPLITE_API_FOR_STAPI */

#ifndef FW_STXP70
typedef struct {
uint32_t Config;  /* at 0 */
uint32_t ViewportOri;  /* at 4 */
uint32_t ViewportSize;  /* at 8 */
uint32_t NextNextLuma;  /* at 12 */
uint32_t NextNextRightLuma;  /* at 16 */
uint32_t NextNextNextLuma;  /* at 20 */
uint32_t NextNextNextRightLuma;  /* at 24 */
uint32_t ThresholdScd;  /* at 28 */
uint32_t ThresholdRfd;  /* at 32 */
uint32_t ThresholdMove;  /* at 36 */
uint32_t ThresholdCfd;  /* at 40 */
} HQVDPLITE_FMD_Params_t;
#endif /* FW_STXP70 */
#endif
