/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*!
 * \file c8fvp3_HVSRC_Lut.c
 * \brief Specifics functions that initialize the Lut of HVSRC
 *
 * These functions initialize the values of Luts for the HVSRC filters
 */

#include "c8fvp3_HVSRC_Lut.h"
#include "c8fvp3_HVSRC_LutRef.h"

void  HVSRC_get_lut(uint8_t StrengthY,
                    uint16_t SizeInY,
                    uint16_t SizeOutY,
                    uint8_t StrengthC,
                    uint16_t SizeInC,
                    uint16_t SizeOutC,
                    uint32_t *LutY,
                    uint32_t *LutC,
                    uint8_t *shiftY,
                    uint8_t *shiftC) {

  uint32_t *myLutY = 0;
  uint32_t *myLutC = 0;
  uint32_t i;

  uint32_t LutThreshold[2][4]= { { 8192,6654,4915,4096} , { 8192,6144,5407,4587} };

  uint32_t LutThresholdIdxY=(StrengthY==HVSRC_LUT_Y_LEGACY)?0:1;
  uint32_t LutThresholdIdxC=(StrengthC==HVSRC_LUT_C_LEGACY)?0:1;

  uint32_t ZoomY = 8192 * SizeOutY/SizeInY;
  uint32_t ZoomC = 8192 * SizeOutC/SizeInC;

  *shiftY=0;
  *shiftC=0;

  if ( ZoomY > LutThreshold[LutThresholdIdxY][0]) {
    myLutY = (uint32_t*) LUT_A_Luma[StrengthY];
    *shiftY = LUTshift_A_Luma[StrengthY];
  } else if (ZoomY == LutThreshold[LutThresholdIdxY][0] ) {
    myLutY = (uint32_t*) LUT_B;
    *shiftY = LUT_B_SHIFT;
  } else if (ZoomY < LutThreshold[LutThresholdIdxY][3] ) {
    myLutY = (uint32_t*) LUT_F_Luma[StrengthY];
    *shiftY = LUTshift_F_Luma[StrengthY];
  } else if (ZoomY < LutThreshold[LutThresholdIdxY][2] ) {
    myLutY = (uint32_t*) LUT_E_Luma[StrengthY];
    *shiftY = LUTshift_E_Luma[StrengthY];
  } else if (ZoomY < LutThreshold[LutThresholdIdxY][1] ) {
    myLutY = (uint32_t*) LUT_D_Luma[StrengthY];
    *shiftY = LUTshift_D_Luma[StrengthY];
  } else { /*if (ZoomY < LutThreshold[LutThresholdIdxY][0] ) {*/
    myLutY = (uint32_t*) LUT_C_Luma[StrengthY];
    *shiftY = LUTshift_C_Luma[StrengthY];
  }

  if ( StrengthC != HVSRC_LUT_C_LEGACY) {
    StrengthC = HVSRC_LUT_C_OPT;
  }

  if ( ZoomC > LutThreshold[LutThresholdIdxC][0]) {
    if (ZoomC == 16384 && ZoomY == 8192) {
      myLutC = (uint32_t*) LUT_Chromax2;
      *shiftC = LUT_CHROMAX2_SHIFT;
    } else {
      myLutC = (uint32_t*) LUT_A_Chroma[StrengthC];
      *shiftC = LUTshift_A_Chroma[StrengthC];
    }
  } else if (ZoomC == LutThreshold[LutThresholdIdxC][0] ) {
    myLutC = (uint32_t*) LUT_B;
    *shiftC = LUT_B_SHIFT;
  } else if (ZoomC < LutThreshold[LutThresholdIdxC][3] ) {
    myLutC = (uint32_t*) LUT_F_Chroma[StrengthC];
    *shiftC = LUTshift_F_Chroma[StrengthC];
  } else if (ZoomC < LutThreshold[LutThresholdIdxC][2] ) {
    myLutC = (uint32_t*) LUT_E_Chroma[StrengthC];
    *shiftC = LUTshift_E_Chroma[StrengthC];
  } else if (ZoomC < LutThreshold[LutThresholdIdxC][1] ) {
    myLutC = (uint32_t*) LUT_D_Chroma[StrengthC];
    *shiftC = LUTshift_D_Chroma[StrengthC];
  } else { /*if (ZoomC < LutThreshold[LutThresholdIdxC][0] ) {*/
    myLutC = (uint32_t*) LUT_C_Chroma[StrengthC];
    *shiftC = LUTshift_C_Chroma[StrengthC];
  }

  for (i=0;i<NBREGCOEF;i++) {
    /*
      global_verif_hal.print_msg("*** INFO  : GET No myLutY[0]", myLutY[i], global_verif_hal.verbose_level);
      global_verif_hal.print_msg("*** INFO  : GET No myLutC[0]", myLutC[i], global_verif_hal.verbose_level);
    */
    LutY[i] = myLutY[i];
    LutC[i] = myLutC[i];
  }

}

void HVSRC_getFilterCoefHV(uint16_t ApiViewportX,
                           uint16_t ApiWidthIn,
                           uint16_t ApiWidthOut,
                           uint16_t ApiHeightIn,
                           uint16_t ApiHeightOut,
                           uint8_t StrengthYV,
                           uint8_t StrengthCV,
                           uint8_t StrengthYH,
                           uint8_t StrengthCH,
                           bool InputIs444,
                           bool OutputIs444,
                           bool OuputIsSbs,
                           bool OuputIsTab,
                           uint32_t *LutYH,
                           uint32_t *LutCH,
                           uint32_t *LutYV,
                           uint32_t *LutCV,
                           uint8_t *shiftYH,
                           uint8_t *shiftCH,
                           uint8_t *shiftYV,
                           uint8_t *shiftCV) {

  uint16_t SizeInC, SizeOutC, SizeInY, SizeOutY;
  uint16_t aligned_viewport_X;


    hor_viewport_alignement((OuputIsSbs || OuputIsTab)?1:0,
                            ApiViewportX,
                            0,
                            ApiWidthIn,
                            &aligned_viewport_X,
                            0,
                            &SizeInY);


  SizeOutY = ApiWidthOut;
  SizeInY  = (OuputIsSbs && InputIs444)?ApiWidthIn:ApiWidthIn & ~1;
  SizeInC  = InputIs444?SizeInY*2:SizeInY;
  SizeOutC = OutputIs444?SizeOutY*2:SizeOutY;



  HVSRC_get_lut(StrengthYH,
                SizeInY,
                SizeOutY,
                StrengthCH,
                SizeInC,
                SizeOutC,
                LutYH,
                LutCH,
                shiftYH,
                shiftCH);

  SizeInY = ApiHeightIn;
  SizeOutY = ApiHeightOut;
  SizeInC  = SizeInY;
  SizeOutC = SizeOutY;

  HVSRC_get_lut(StrengthYV,
                SizeInY,
                SizeOutY,
                StrengthCV,
                SizeInC,
                SizeOutC,
                LutYV,
                LutCV,
                shiftYV,
                shiftCV);
}

