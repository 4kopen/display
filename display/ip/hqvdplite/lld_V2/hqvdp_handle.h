/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef __HQVDP_HANDLE_H__
#define __HQVDP_HANDLE_H__

/******************************************************************************/
/* Handle definition                                                          */
/******************************************************************************/
#include "hqvdp_lld_platform.h"
#include "hqvdp_lld_api.h"


/** @brief struct to store where to pput cmd
    @ingroup lld_priv_struct
*/
struct ctxt_cmd {
        struct hqvdp_lld_mem_desc_s cmd_used;
        struct hqvdp_lld_mem_desc_s cmd_next_vsync;
};

/** @brief status of session
    @ingroup lld_priv_enum
*/
enum hqvdp_state_e {
        open_done, /**< @brief opened has been done */
        conf_done,/**< @brief configuration for next Vsync done */
        stop_done,/**< @brief NULL command has been sent to HQVDP */
        close_done,/**< @brief session has been finished or not properly initialized */
};

/** @brief struct to define handle
    @ingroup lld_priv_struct
*/
struct hqvdp_lld_ctxt {
        /* hardware specific */
        void *base_addr[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE]; /**< @brief IP base address */
        void *video_plug_handle[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE]; /**< @brief link with video plug */
        uint32_t mixer_bit[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE]; /**< @brief bit of CTL mixer associated */
        /* command */
        struct ctxt_cmd ctxt_cmd[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE];
        struct hqvdp_lld_fw_s fw[HQVDP_LLD_MAX_HW_FOR_ONE_PLANE];
        /* status */
        int reserved_resources; /**< @brief number hardware resources available in session */
        int used_resources; /**< @brief number hardware used by current command */
        enum hqvdp_lld_profile_e profile; /**< @brief profile use to open session */
        int motion_buffer_size; /**< @brief size of one motion buffer in bytes */
        /* check */
        enum hqvdp_state_e state; /**< @brief lld state use to return error */
        uint32_t magic;
        bool pixel_repeat; /**< @brief pixel repeat enable or not */
};

#endif /* __HQVDP_HANDLE_H__ */
