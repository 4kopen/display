# Classes required for SoCs containing a HQVDP display
ifneq ($(CONFIG_HQVDPLITE),)

EXTRA_CFLAGS += -DHQVDPLITE_API_FOR_STAPI
ifneq ($(CONFIG_STM_VIRTUAL_PLATFORM),)
EXTRA_CFLAGS += -DVIRTUAL_PLATFORM_TLM
endif

SRC_FILES_NAMES :=
SRC_FILES_NAMES += hqvdp_lld_api.c
SRC_FILES_NAMES += hqvdplite_dmem.c
SRC_FILES_NAMES += hqvdplite_pmem.c
SRC_FILES_NAMES += c8fvp3_FW_download_code.c
SRC_FILES_NAMES += c8fvp3_FW_lld.c
SRC_FILES_NAMES += c8fvp3_strRegAccess.c
SRC_FILES_NAMES += c8fvp3_HVSRC_Lut.c
SRC_FILES_NAMES += c8fvp3_aligned_viewport.c
SRC_FILES_NAMES += c8fvp3_FW_plug.c
SRC_FILES_NAMES += hqvdp_split.c
SRC_FILES_NAMES += hqvdp_split_algo.c
SRC_FILES_NAMES += hqvdp_check.c
SRC_FILES_NAMES += hqvdp_ucode_256_rd.c
SRC_FILES_NAMES += hqvdp_ucode_256_wr.c

STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/hqvdplite/lld_V2/,$(SRC_FILES_NAMES))
endif
