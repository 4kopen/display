/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _HVSRCREGBANK_H_
#define _HVSRCREGBANK_H_

/**
* Component section
* Purpose : defines macros for component
*           specific informations
*/

#define HVSRCregBank_VENDOR                             ("st.com")
#define HVSRCregBank_LIBRARY                            ("hqvdplite")
#define HVSRCregBank_NAME                               ("HVSRCregBank")
#define HVSRCregBank_VERSION                            ("1.0")

/**
* Address Block : HVSRC_pu_reg
*/

#define HVSRCregBank_HVSRC_pu_reg_BASE_ADDR             (0x0)

/**
* Register : HVSRC_INPUT_PICTURE_SIZE
* Input picture size
*/

#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_SIZE      (32)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_OFFSET    (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x00)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_RESET_VALUE (0x00000)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_BITFIELD_MASK (0x07FF07FF)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_RWMASK    (0x07FF07FF)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_ROMASK    (0x00000000)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_WOMASK    (0x00000000)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_UNUSED_MASK (0xF800F800)

/**
* Bit-field : IMG_WIDTH_YIN
* input frame width, expressed in pixels.
* Unsigned value ranging from 48 to 1920 pixels (default: 720). If input is 422, width must always be a multiple of 2.
* Used in every luma PU.
*/

#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_IMG_WIDTH_YIN_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_IMG_WIDTH_YIN_WIDTH (11)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_IMG_WIDTH_YIN_MASK (0x000007FF)

/**
* Bit-field : IMG_WIDTH_CIN
* input frame width, expressed in pixels.
* Unsigned value ranging from 48 to 1920 pixels (default: 720). If input is 422, width must always be a multiple of 2.
* Used in every luma PU.
*/

#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_IMG_WIDTH_CIN_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_IMG_WIDTH_CIN_WIDTH (11)
#define HVSRCregBank_HVSRC_INPUT_PICTURE_SIZE_IMG_WIDTH_CIN_MASK (0x07FF0000)

/**
* Register : HVSRC_OUTPUT_PICTURE_SIZE
* Output picture size
*/

#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_SIZE     (32)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_OFFSET   (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x04)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_RESET_VALUE (0x00000000)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_BITFIELD_MASK (0x07FF07FF)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_RWMASK   (0x07FF07FF)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_ROMASK   (0x00000000)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_WOMASK   (0x00000000)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_UNUSED_MASK (0xF800F800)

/**
* Bit-field : IMG_WIDTH_YOUT
* output frame width, expressed in pixels, must always be a multiple of 2.
* - Unsigned value ranging from max(48;IMG_WIDTH_YIN/8) to min(1920;IMG_WIDTH_YIN*8) - default: 0
* - unitary zoom: IMG_WIDTH_YOUT = IMG_WIDTH_YIN.
* Used in the NewsplineHor(Y/U/V) PU.
*/

#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_IMG_WIDTH_YOUT_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_IMG_WIDTH_YOUT_WIDTH (11)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_IMG_WIDTH_YOUT_MASK (0x000007FF)

/**
* Bit-field : IMG_WIDTH_COUT
* output frame width, expressed in pixels, must always be a multiple of 2.
* - Unsigned value ranging from max(48;IMG_WIDTH_CIN/8) to min(1920;IMG_WIDTH_CIN*8) - default: 0
* - unitary zoom: IMG_WIDTH_COUT = IMG_WIDTH_CIN.
* Used in the NewsplineHor(Y/U/V) PU.
*/

#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_IMG_WIDTH_COUT_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_IMG_WIDTH_COUT_WIDTH (11)
#define HVSRCregBank_HVSRC_OUTPUT_PICTURE_SIZE_IMG_WIDTH_COUT_MASK (0x07FF0000)

/**
* Register : HVSRC_LUMA_INIT
* Luma Horizontal Initial Phase
*/

#define HVSRCregBank_HVSRC_LUMA_INIT_SIZE               (32)
#define HVSRCregBank_HVSRC_LUMA_INIT_OFFSET             (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x08)
#define HVSRCregBank_HVSRC_LUMA_INIT_RESET_VALUE        (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_INIT_BITFIELD_MASK      (0x01FFFFFF)
#define HVSRCregBank_HVSRC_LUMA_INIT_RWMASK             (0x01FFFFFF)
#define HVSRCregBank_HVSRC_LUMA_INIT_ROMASK             (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_INIT_WOMASK             (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_INIT_UNUSED_MASK        (0xFE000000)

/**
* Bit-field : PHASEH
*/

#define HVSRCregBank_HVSRC_LUMA_INIT_PHASEH_OFFSET      (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_INIT_PHASEH_WIDTH       (25)
#define HVSRCregBank_HVSRC_LUMA_INIT_PHASEH_MASK        (0x01FFFFFF)

/**
* Register : HVSRC_CHROMA_INIT
* Chroma Horizontal Initial Phase
*/

#define HVSRCregBank_HVSRC_CHROMA_INIT_SIZE             (32)
#define HVSRCregBank_HVSRC_CHROMA_INIT_OFFSET           (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x0C)
#define HVSRCregBank_HVSRC_CHROMA_INIT_RESET_VALUE      (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_INIT_BITFIELD_MASK    (0x01FFFFFF)
#define HVSRCregBank_HVSRC_CHROMA_INIT_RWMASK           (0x01FFFFFF)
#define HVSRCregBank_HVSRC_CHROMA_INIT_ROMASK           (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_INIT_WOMASK           (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_INIT_UNUSED_MASK      (0xFE000000)

/**
* Bit-field : PHASEH
*/

#define HVSRCregBank_HVSRC_CHROMA_INIT_PHASEH_OFFSET    (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_INIT_PHASEH_WIDTH     (25)
#define HVSRCregBank_HVSRC_CHROMA_INIT_PHASEH_MASK      (0x01FFFFFF)

/**
* Register : HVSRC_HOR_INCR_LUMA
* Luma Horizontal Increment
*/

#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_SIZE           (32)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_OFFSET         (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x10)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_RESET_VALUE    (0x00008000)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_BITFIELD_MASK  (0x0001FFFF)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_RWMASK         (0x0001FFFF)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_ROMASK         (0x00000000)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_WOMASK         (0x00000000)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_UNUSED_MASK    (0xFFFE0000)

/**
* Bit-field : INCRH
*/

#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_INCRH_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_INCRH_WIDTH    (17)
#define HVSRCregBank_HVSRC_HOR_INCR_LUMA_INCRH_MASK     (0x0001FFFF)

/**
* Register : HVSRC_HOR_INCR_CHROMA
* Chroma Horizontal Increment
*/

#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_SIZE         (32)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_OFFSET       (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x14)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_RESET_VALUE  (0x00008000)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_BITFIELD_MASK (0x0001FFFF)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_RWMASK       (0x0001FFFF)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_ROMASK       (0x00000000)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_WOMASK       (0x00000000)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_UNUSED_MASK  (0xFFFE0000)

/**
* Bit-field : INCRH
* chroma horizontal increment
* Unsigned value ranging from 697 to 262 144 (default: 32 768).
* Used in the NewsplineHor(U/V) PU
*/

#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_INCRH_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_INCRH_WIDTH  (17)
#define HVSRCregBank_HVSRC_HOR_INCR_CHROMA_INCRH_MASK   (0x0001FFFF)

/**
* Register : HVSRC_PDELTA_LUMA
* Luma Horizontal Non Linear Zoom Step Definition
*/

#define HVSRCregBank_HVSRC_PDELTA_LUMA_SIZE             (32)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_OFFSET           (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x18)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_RESET_VALUE      (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_BITFIELD_MASK    (0x0001FFFF)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_RWMASK           (0x0001FFFF)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_ROMASK           (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_WOMASK           (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_UNUSED_MASK      (0xFFFE0000)

/**
* Bit-field : PDELTAX
* luma horizontal step correction for panoramic mode
* unsigned value ranging from 0 to 17 460, default: 0.
* Used in the NewsplineHorY PU.
*/

#define HVSRCregBank_HVSRC_PDELTA_LUMA_PDELTAX_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_PDELTAX_WIDTH    (17)
#define HVSRCregBank_HVSRC_PDELTA_LUMA_PDELTAX_MASK     (0x0001FFFF)

/**
* Register : HVSRC_PDELTA_CHROMA
* Chroma Horizontal Non Linear Zoom Step Definition
*/

#define HVSRCregBank_HVSRC_PDELTA_CHROMA_SIZE           (32)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_OFFSET         (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1C)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_RESET_VALUE    (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_BITFIELD_MASK  (0x0001FFFF)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_RWMASK         (0x0001FFFF)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_ROMASK         (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_WOMASK         (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_UNUSED_MASK    (0xFFFE0000)

/**
* Bit-field : PDELTAX
* luma horizontal step correction for panoramic mode
* unsigned value ranging from 0 to 17 460, default: 0.
* Used in the NewsplineHorY PU.
*/

#define HVSRCregBank_HVSRC_PDELTA_CHROMA_PDELTAX_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_PDELTAX_WIDTH  (17)
#define HVSRCregBank_HVSRC_PDELTA_CHROMA_PDELTAX_MASK   (0x0001FFFF)

/**
* Register : HVSRC_LUMA_WX
* Luma Panoramic control Wx
*/

#define HVSRCregBank_HVSRC_LUMA_WX_SIZE                 (32)
#define HVSRCregBank_HVSRC_LUMA_WX_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x20)
#define HVSRCregBank_HVSRC_LUMA_WX_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_WX_BITFIELD_MASK        (0xFFFFFFFF)
#define HVSRCregBank_HVSRC_LUMA_WX_RWMASK               (0xFFFFFFFF)
#define HVSRCregBank_HVSRC_LUMA_WX_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_WX_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_WX_UNUSED_MASK          (0x00000000)

/**
* Bit-field : WAX
*/

#define HVSRCregBank_HVSRC_LUMA_WX_WAX_OFFSET           (0x00000000)
#define HVSRCregBank_HVSRC_LUMA_WX_WAX_WIDTH            (16)
#define HVSRCregBank_HVSRC_LUMA_WX_WAX_MASK             (0x0000FFFF)

/**
* Bit-field : WBX
*/

#define HVSRCregBank_HVSRC_LUMA_WX_WBX_OFFSET           (0x00000010)
#define HVSRCregBank_HVSRC_LUMA_WX_WBX_WIDTH            (16)
#define HVSRCregBank_HVSRC_LUMA_WX_WBX_MASK             (0xFFFF0000)

/**
* Register : HVSRC_CHROMA_WX
* Chroma Panoramic control Wx
*/

#define HVSRCregBank_HVSRC_CHROMA_WX_SIZE               (32)
#define HVSRCregBank_HVSRC_CHROMA_WX_OFFSET             (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x24)
#define HVSRCregBank_HVSRC_CHROMA_WX_RESET_VALUE        (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_WX_BITFIELD_MASK      (0xFFFFFFFF)
#define HVSRCregBank_HVSRC_CHROMA_WX_RWMASK             (0xFFFFFFFF)
#define HVSRCregBank_HVSRC_CHROMA_WX_ROMASK             (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_WX_WOMASK             (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_WX_UNUSED_MASK        (0x00000000)

/**
* Bit-field : WAX
*/

#define HVSRCregBank_HVSRC_CHROMA_WX_WAX_OFFSET         (0x00000000)
#define HVSRCregBank_HVSRC_CHROMA_WX_WAX_WIDTH          (16)
#define HVSRCregBank_HVSRC_CHROMA_WX_WAX_MASK           (0x0000FFFF)

/**
* Bit-field : WBX
*/

#define HVSRCregBank_HVSRC_CHROMA_WX_WBX_OFFSET         (0x00000010)
#define HVSRCregBank_HVSRC_CHROMA_WX_WBX_WIDTH          (16)
#define HVSRCregBank_HVSRC_CHROMA_WX_WBX_MASK           (0xFFFF0000)

/**
* Register : HVSRC_PARAM
* Algo control parameters
*/

#define HVSRCregBank_HVSRC_PARAM_SIZE                   (32)
#define HVSRCregBank_HVSRC_PARAM_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x28)
#define HVSRCregBank_HVSRC_PARAM_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_PARAM_BITFIELD_MASK          (0xF0F0F0FF)
#define HVSRCregBank_HVSRC_PARAM_RWMASK                 (0xF0F0F0FF)
#define HVSRCregBank_HVSRC_PARAM_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_PARAM_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_PARAM_UNUSED_MASK            (0x0F0F0F00)

/**
* Bit-field : H_ACON
*/

#define HVSRCregBank_HVSRC_PARAM_H_ACON_OFFSET          (0x00000000)
#define HVSRCregBank_HVSRC_PARAM_H_ACON_WIDTH           (1)
#define HVSRCregBank_HVSRC_PARAM_H_ACON_MASK            (0x00000001)

/**
* Bit-field : H_OVERSHOOTCONTRAST
*/

#define HVSRCregBank_HVSRC_PARAM_H_OVERSHOOTCONTRAST_OFFSET (0x00000001)
#define HVSRCregBank_HVSRC_PARAM_H_OVERSHOOTCONTRAST_WIDTH (1)
#define HVSRCregBank_HVSRC_PARAM_H_OVERSHOOTCONTRAST_MASK (0x00000002)

/**
* Bit-field : V_ACON
*/

#define HVSRCregBank_HVSRC_PARAM_V_ACON_OFFSET          (0x00000002)
#define HVSRCregBank_HVSRC_PARAM_V_ACON_WIDTH           (1)
#define HVSRCregBank_HVSRC_PARAM_V_ACON_MASK            (0x00000004)

/**
* Bit-field : V_OVERSHOOTCONTRAST
*/

#define HVSRCregBank_HVSRC_PARAM_V_OVERSHOOTCONTRAST_OFFSET (0x00000003)
#define HVSRCregBank_HVSRC_PARAM_V_OVERSHOOTCONTRAST_WIDTH (1)
#define HVSRCregBank_HVSRC_PARAM_V_OVERSHOOTCONTRAST_MASK (0x00000008)

/**
* Bit-field : YH_KOVS
*/

#define HVSRCregBank_HVSRC_PARAM_YH_KOVS_OFFSET         (0x00000004)
#define HVSRCregBank_HVSRC_PARAM_YH_KOVS_WIDTH          (4)
#define HVSRCregBank_HVSRC_PARAM_YH_KOVS_MASK           (0x000000F0)

/**
* Bit-field : YV_KOVS
*/

#define HVSRCregBank_HVSRC_PARAM_YV_KOVS_OFFSET         (0x0000000c)
#define HVSRCregBank_HVSRC_PARAM_YV_KOVS_WIDTH          (4)
#define HVSRCregBank_HVSRC_PARAM_YV_KOVS_MASK           (0x0000F000)

/**
* Bit-field : CH_KOVS
*/

#define HVSRCregBank_HVSRC_PARAM_CH_KOVS_OFFSET         (0x00000014)
#define HVSRCregBank_HVSRC_PARAM_CH_KOVS_WIDTH          (4)
#define HVSRCregBank_HVSRC_PARAM_CH_KOVS_MASK           (0x00F00000)

/**
* Bit-field : CV_KOVS
*/

#define HVSRCregBank_HVSRC_PARAM_CV_KOVS_OFFSET         (0x0000001c)
#define HVSRCregBank_HVSRC_PARAM_CV_KOVS_WIDTH          (4)
#define HVSRCregBank_HVSRC_PARAM_CV_KOVS_MASK           (0xF0000000)

/**
* Register : HVSRC_DL_INIT
* Delay Line init bits
*/

#define HVSRCregBank_HVSRC_DL_INIT_SIZE                 (32)
#define HVSRCregBank_HVSRC_DL_INIT_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2C)
#define HVSRCregBank_HVSRC_DL_INIT_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_DL_INIT_BITFIELD_MASK        (0x00000003)
#define HVSRCregBank_HVSRC_DL_INIT_RWMASK               (0x00000003)
#define HVSRCregBank_HVSRC_DL_INIT_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_DL_INIT_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_DL_INIT_UNUSED_MASK          (0xFFFFFFFC)

/**
* Bit-field : Y
*/

#define HVSRCregBank_HVSRC_DL_INIT_Y_OFFSET             (0x00000000)
#define HVSRCregBank_HVSRC_DL_INIT_Y_WIDTH              (1)
#define HVSRCregBank_HVSRC_DL_INIT_Y_MASK               (0x00000001)

/**
* Bit-field : C
*/

#define HVSRCregBank_HVSRC_DL_INIT_C_OFFSET             (0x00000001)
#define HVSRCregBank_HVSRC_DL_INIT_C_WIDTH              (1)
#define HVSRCregBank_HVSRC_DL_INIT_C_MASK               (0x00000002)

/**
* Register : HVSRC_DL_FLUSH
* Delay Line flush bits
*/

#define HVSRCregBank_HVSRC_DL_FLUSH_SIZE                (32)
#define HVSRCregBank_HVSRC_DL_FLUSH_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x30)
#define HVSRCregBank_HVSRC_DL_FLUSH_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_DL_FLUSH_BITFIELD_MASK       (0x00000003)
#define HVSRCregBank_HVSRC_DL_FLUSH_RWMASK              (0x00000003)
#define HVSRCregBank_HVSRC_DL_FLUSH_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_DL_FLUSH_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_DL_FLUSH_UNUSED_MASK         (0xFFFFFFFC)

/**
* Bit-field : Y
*/

#define HVSRCregBank_HVSRC_DL_FLUSH_Y_OFFSET            (0x00000000)
#define HVSRCregBank_HVSRC_DL_FLUSH_Y_WIDTH             (1)
#define HVSRCregBank_HVSRC_DL_FLUSH_Y_MASK              (0x00000001)

/**
* Bit-field : C
*/

#define HVSRCregBank_HVSRC_DL_FLUSH_C_OFFSET            (0x00000001)
#define HVSRCregBank_HVSRC_DL_FLUSH_C_WIDTH             (1)
#define HVSRCregBank_HVSRC_DL_FLUSH_C_MASK              (0x00000002)

/**
* Register : HVSRC_DL_SKIP
* Delay Line skip bits
*/

#define HVSRCregBank_HVSRC_DL_SKIP_SIZE                 (32)
#define HVSRCregBank_HVSRC_DL_SKIP_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x34)
#define HVSRCregBank_HVSRC_DL_SKIP_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_DL_SKIP_BITFIELD_MASK        (0x00000003)
#define HVSRCregBank_HVSRC_DL_SKIP_RWMASK               (0x00000003)
#define HVSRCregBank_HVSRC_DL_SKIP_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_DL_SKIP_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_DL_SKIP_UNUSED_MASK          (0xFFFFFFFC)

/**
* Bit-field : Y
*/

#define HVSRCregBank_HVSRC_DL_SKIP_Y_OFFSET             (0x00000000)
#define HVSRCregBank_HVSRC_DL_SKIP_Y_WIDTH              (1)
#define HVSRCregBank_HVSRC_DL_SKIP_Y_MASK               (0x00000001)

/**
* Bit-field : C
*/

#define HVSRCregBank_HVSRC_DL_SKIP_C_OFFSET             (0x00000001)
#define HVSRCregBank_HVSRC_DL_SKIP_C_WIDTH              (1)
#define HVSRCregBank_HVSRC_DL_SKIP_C_MASK               (0x00000002)

/**
* Register : HVSRC_HORI_SHIFT
* Shift for Sum of horizontal coefs
*/

#define HVSRCregBank_HVSRC_HORI_SHIFT_SIZE              (32)
#define HVSRCregBank_HVSRC_HORI_SHIFT_OFFSET            (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x38)
#define HVSRCregBank_HVSRC_HORI_SHIFT_RESET_VALUE       (0x00000000)
#define HVSRCregBank_HVSRC_HORI_SHIFT_BITFIELD_MASK     (0x000F000F)
#define HVSRCregBank_HVSRC_HORI_SHIFT_RWMASK            (0x000F000F)
#define HVSRCregBank_HVSRC_HORI_SHIFT_ROMASK            (0x00000000)
#define HVSRCregBank_HVSRC_HORI_SHIFT_WOMASK            (0x00000000)
#define HVSRCregBank_HVSRC_HORI_SHIFT_UNUSED_MASK       (0xFFF0FFF0)

/**
* Bit-field : Y
* sum of horizontal luma coef
*/

#define HVSRCregBank_HVSRC_HORI_SHIFT_Y_OFFSET          (0x00000000)
#define HVSRCregBank_HVSRC_HORI_SHIFT_Y_WIDTH           (4)
#define HVSRCregBank_HVSRC_HORI_SHIFT_Y_MASK            (0x0000000F)

/**
* Bit-field : C
* sum of horizontal chroma coef
*/

#define HVSRCregBank_HVSRC_HORI_SHIFT_C_OFFSET          (0x00000010)
#define HVSRCregBank_HVSRC_HORI_SHIFT_C_WIDTH           (4)
#define HVSRCregBank_HVSRC_HORI_SHIFT_C_MASK            (0x000F0000)

/**
* Register : HVSRC_VERT_SHIFT
* Shift for sum of vertical coefs
*/

#define HVSRCregBank_HVSRC_VERT_SHIFT_SIZE              (32)
#define HVSRCregBank_HVSRC_VERT_SHIFT_OFFSET            (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3C)
#define HVSRCregBank_HVSRC_VERT_SHIFT_RESET_VALUE       (0x00000000)
#define HVSRCregBank_HVSRC_VERT_SHIFT_BITFIELD_MASK     (0x000F000F)
#define HVSRCregBank_HVSRC_VERT_SHIFT_RWMASK            (0x000F000F)
#define HVSRCregBank_HVSRC_VERT_SHIFT_ROMASK            (0x00000000)
#define HVSRCregBank_HVSRC_VERT_SHIFT_WOMASK            (0x00000000)
#define HVSRCregBank_HVSRC_VERT_SHIFT_UNUSED_MASK       (0xFFF0FFF0)

/**
* Bit-field : Y
* sum of vertical luma coefs
*/

#define HVSRCregBank_HVSRC_VERT_SHIFT_Y_OFFSET          (0x00000000)
#define HVSRCregBank_HVSRC_VERT_SHIFT_Y_WIDTH           (4)
#define HVSRCregBank_HVSRC_VERT_SHIFT_Y_MASK            (0x0000000F)

/**
* Bit-field : C
* sum of vertical chroma coefs
*/

#define HVSRCregBank_HVSRC_VERT_SHIFT_C_OFFSET          (0x00000010)
#define HVSRCregBank_HVSRC_VERT_SHIFT_C_WIDTH           (4)
#define HVSRCregBank_HVSRC_VERT_SHIFT_C_MASK            (0x000F0000)

/**
* Register : HVSRC_YV_01
* Luma Vertical filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YV_01_SIZE                   (32)
#define HVSRCregBank_HVSRC_YV_01_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x40)
#define HVSRCregBank_HVSRC_YV_01_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_YV_01_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_01_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_01_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_01_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_01_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* vertical coef 0
*/

#define HVSRCregBank_HVSRC_YV_01_COEFFFILTER_0_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_YV_01_COEFFFILTER_0_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_01_COEFFFILTER_0_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* vertical coef 1
*/

#define HVSRCregBank_HVSRC_YV_01_COEFFFILTER_1_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_YV_01_COEFFFILTER_1_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_01_COEFFFILTER_1_MASK     (0x03FF0000)

/**
* Register : HVSRC_YV_23
* Luma Vertical filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YV_23_SIZE                   (32)
#define HVSRCregBank_HVSRC_YV_23_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x44)
#define HVSRCregBank_HVSRC_YV_23_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_YV_23_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_23_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_23_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_23_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_23_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* vertical coef 2
*/

#define HVSRCregBank_HVSRC_YV_23_COEFFFILTER_2_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_YV_23_COEFFFILTER_2_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_23_COEFFFILTER_2_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* vertical coef 3
*/

#define HVSRCregBank_HVSRC_YV_23_COEFFFILTER_3_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_YV_23_COEFFFILTER_3_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_23_COEFFFILTER_3_MASK     (0x03FF0000)

/**
* Register : HVSRC_YV_45
* Luma Vertical filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YV_45_SIZE                   (32)
#define HVSRCregBank_HVSRC_YV_45_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x48)
#define HVSRCregBank_HVSRC_YV_45_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_YV_45_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_45_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_45_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_45_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_45_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* vertical coef 4
*/

#define HVSRCregBank_HVSRC_YV_45_COEFFFILTER_4_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_YV_45_COEFFFILTER_4_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_45_COEFFFILTER_4_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* vertical coef 5
*/

#define HVSRCregBank_HVSRC_YV_45_COEFFFILTER_5_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_YV_45_COEFFFILTER_5_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_45_COEFFFILTER_5_MASK     (0x03FF0000)

/**
* Register : HVSRC_YV_67
* Luma Vertical filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YV_67_SIZE                   (32)
#define HVSRCregBank_HVSRC_YV_67_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x4C)
#define HVSRCregBank_HVSRC_YV_67_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_YV_67_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_67_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_YV_67_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_67_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_YV_67_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* vertical coef 6
*/

#define HVSRCregBank_HVSRC_YV_67_COEFFFILTER_6_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_YV_67_COEFFFILTER_6_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_67_COEFFFILTER_6_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* vertical coef 7
*/

#define HVSRCregBank_HVSRC_YV_67_COEFFFILTER_7_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_YV_67_COEFFFILTER_7_WIDTH    (10)
#define HVSRCregBank_HVSRC_YV_67_COEFFFILTER_7_MASK     (0x03FF0000)

/**
* Register : HVSRC_CV_01
* Chroma Vertical filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CV_01_SIZE                   (32)
#define HVSRCregBank_HVSRC_CV_01_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x50)
#define HVSRCregBank_HVSRC_CV_01_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_CV_01_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_01_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_01_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_01_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_01_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* vertical coef 0
*/

#define HVSRCregBank_HVSRC_CV_01_COEFFFILTER_0_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_CV_01_COEFFFILTER_0_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_01_COEFFFILTER_0_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* vertical coef 1
*/

#define HVSRCregBank_HVSRC_CV_01_COEFFFILTER_1_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_CV_01_COEFFFILTER_1_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_01_COEFFFILTER_1_MASK     (0x03FF0000)

/**
* Register : HVSRC_CV_23
* Chroma Vertical filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CV_23_SIZE                   (32)
#define HVSRCregBank_HVSRC_CV_23_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x54)
#define HVSRCregBank_HVSRC_CV_23_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_CV_23_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_23_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_23_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_23_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_23_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* vertical coef 2
*/

#define HVSRCregBank_HVSRC_CV_23_COEFFFILTER_2_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_CV_23_COEFFFILTER_2_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_23_COEFFFILTER_2_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* vertical coef 3
*/

#define HVSRCregBank_HVSRC_CV_23_COEFFFILTER_3_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_CV_23_COEFFFILTER_3_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_23_COEFFFILTER_3_MASK     (0x03FF0000)

/**
* Register : HVSRC_CV_45
* Chroma Vertical filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CV_45_SIZE                   (32)
#define HVSRCregBank_HVSRC_CV_45_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x58)
#define HVSRCregBank_HVSRC_CV_45_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_CV_45_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_45_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_45_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_45_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_45_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* vertical coef 4
*/

#define HVSRCregBank_HVSRC_CV_45_COEFFFILTER_4_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_CV_45_COEFFFILTER_4_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_45_COEFFFILTER_4_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* vertical coef 5
*/

#define HVSRCregBank_HVSRC_CV_45_COEFFFILTER_5_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_CV_45_COEFFFILTER_5_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_45_COEFFFILTER_5_MASK     (0x03FF0000)

/**
* Register : HVSRC_CV_67
* Chroma Vertical filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CV_67_SIZE                   (32)
#define HVSRCregBank_HVSRC_CV_67_OFFSET                 (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x5C)
#define HVSRCregBank_HVSRC_CV_67_RESET_VALUE            (0x00000000)
#define HVSRCregBank_HVSRC_CV_67_BITFIELD_MASK          (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_67_RWMASK                 (0x03FF03FF)
#define HVSRCregBank_HVSRC_CV_67_ROMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_67_WOMASK                 (0x00000000)
#define HVSRCregBank_HVSRC_CV_67_UNUSED_MASK            (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* vertical coef 6
*/

#define HVSRCregBank_HVSRC_CV_67_COEFFFILTER_6_OFFSET   (0x00000000)
#define HVSRCregBank_HVSRC_CV_67_COEFFFILTER_6_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_67_COEFFFILTER_6_MASK     (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* vertical coef 7
*/

#define HVSRCregBank_HVSRC_CV_67_COEFFFILTER_7_OFFSET   (0x00000010)
#define HVSRCregBank_HVSRC_CV_67_COEFFFILTER_7_WIDTH    (10)
#define HVSRCregBank_HVSRC_CV_67_COEFFFILTER_7_MASK     (0x03FF0000)

/**
* Register : HVSRC_YH_01_0
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x60)
#define HVSRCregBank_HVSRC_YH_01_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_0_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_0_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_0_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_0_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_0_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_0_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_1
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x64)
#define HVSRCregBank_HVSRC_YH_01_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_1_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_1_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_1_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_1_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_1_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_1_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_2
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x68)
#define HVSRCregBank_HVSRC_YH_01_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_2_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_2_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_2_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_2_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_2_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_2_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_3
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x6C)
#define HVSRCregBank_HVSRC_YH_01_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_3_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_3_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_3_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_3_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_3_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_3_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_4
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x70)
#define HVSRCregBank_HVSRC_YH_01_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_4_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_4_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_4_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_4_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_4_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_4_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_5
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x74)
#define HVSRCregBank_HVSRC_YH_01_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_5_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_5_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_5_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_5_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_5_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_5_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_6
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x78)
#define HVSRCregBank_HVSRC_YH_01_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_6_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_6_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_6_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_6_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_6_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_6_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_7
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x7C)
#define HVSRCregBank_HVSRC_YH_01_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_7_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_7_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_7_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_7_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_7_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_7_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_8
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x80)
#define HVSRCregBank_HVSRC_YH_01_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_8_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_8_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_8_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_8_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_8_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_8_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_9
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_01_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x84)
#define HVSRCregBank_HVSRC_YH_01_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_9_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_9_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_9_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_9_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_9_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_01_9_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_01_10
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_10_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x88)
#define HVSRCregBank_HVSRC_YH_01_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_10_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_10_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_10_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_10_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_10_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_10_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_11
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_11_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x8C)
#define HVSRCregBank_HVSRC_YH_01_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_11_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_11_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_11_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_11_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_11_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_11_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_12
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_12_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x90)
#define HVSRCregBank_HVSRC_YH_01_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_12_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_12_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_12_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_12_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_12_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_12_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_13
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_13_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x94)
#define HVSRCregBank_HVSRC_YH_01_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_13_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_13_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_13_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_13_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_13_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_13_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_14
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_14_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x98)
#define HVSRCregBank_HVSRC_YH_01_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_14_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_14_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_14_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_14_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_14_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_14_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_15
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_15_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x9C)
#define HVSRCregBank_HVSRC_YH_01_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_15_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_15_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_15_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_15_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_15_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_15_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_16
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_16_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xA0)
#define HVSRCregBank_HVSRC_YH_01_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_16_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_16_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_16_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_16_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_16_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_16_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_17
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_17_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xA4)
#define HVSRCregBank_HVSRC_YH_01_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_17_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_17_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_17_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_17_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_17_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_17_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_18
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_18_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xA8)
#define HVSRCregBank_HVSRC_YH_01_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_18_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_18_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_18_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_18_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_18_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_18_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_19
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_19_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xAC)
#define HVSRCregBank_HVSRC_YH_01_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_19_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_19_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_19_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_19_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_19_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_19_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_20
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_20_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xB0)
#define HVSRCregBank_HVSRC_YH_01_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_20_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_20_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_20_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_20_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_20_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_20_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_21
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_21_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xB4)
#define HVSRCregBank_HVSRC_YH_01_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_21_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_21_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_21_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_21_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_21_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_21_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_22
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_22_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xB8)
#define HVSRCregBank_HVSRC_YH_01_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_22_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_22_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_22_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_22_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_22_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_22_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_23
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_23_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xBC)
#define HVSRCregBank_HVSRC_YH_01_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_23_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_23_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_23_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_23_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_23_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_23_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_24
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_24_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xC0)
#define HVSRCregBank_HVSRC_YH_01_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_24_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_24_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_24_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_24_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_24_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_24_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_25
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_25_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xC4)
#define HVSRCregBank_HVSRC_YH_01_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_25_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_25_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_25_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_25_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_25_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_25_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_26
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_26_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xC8)
#define HVSRCregBank_HVSRC_YH_01_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_26_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_26_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_26_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_26_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_26_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_26_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_27
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_27_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xCC)
#define HVSRCregBank_HVSRC_YH_01_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_27_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_27_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_27_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_27_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_27_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_27_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_28
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_28_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xD0)
#define HVSRCregBank_HVSRC_YH_01_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_28_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_28_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_28_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_28_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_28_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_28_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_29
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_29_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xD4)
#define HVSRCregBank_HVSRC_YH_01_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_29_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_29_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_29_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_29_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_29_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_29_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_30
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_30_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xD8)
#define HVSRCregBank_HVSRC_YH_01_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_30_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_30_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_30_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_30_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_30_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_30_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_01_31
* Luma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_YH_01_31_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_01_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xDC)
#define HVSRCregBank_HVSRC_YH_01_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_01_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_YH_01_31_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_01_31_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_31_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_YH_01_31_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_01_31_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_01_31_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_0
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xE0)
#define HVSRCregBank_HVSRC_YH_23_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_0_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_0_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_0_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_0_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_0_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_0_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_1
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xE4)
#define HVSRCregBank_HVSRC_YH_23_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_1_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_1_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_1_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_1_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_1_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_1_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_2
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xE8)
#define HVSRCregBank_HVSRC_YH_23_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_2_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_2_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_2_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_2_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_2_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_2_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_3
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xEC)
#define HVSRCregBank_HVSRC_YH_23_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_3_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_3_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_3_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_3_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_3_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_3_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_4
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xF0)
#define HVSRCregBank_HVSRC_YH_23_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_4_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_4_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_4_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_4_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_4_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_4_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_5
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xF4)
#define HVSRCregBank_HVSRC_YH_23_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_5_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_5_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_5_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_5_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_5_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_5_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_6
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xF8)
#define HVSRCregBank_HVSRC_YH_23_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_6_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_6_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_6_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_6_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_6_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_6_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_7
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0xFC)
#define HVSRCregBank_HVSRC_YH_23_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_7_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_7_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_7_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_7_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_7_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_7_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_8
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x100)
#define HVSRCregBank_HVSRC_YH_23_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_8_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_8_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_8_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_8_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_8_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_8_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_9
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_23_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x104)
#define HVSRCregBank_HVSRC_YH_23_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_9_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_9_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_9_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_9_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_9_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_23_9_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_23_10
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_10_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x108)
#define HVSRCregBank_HVSRC_YH_23_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_10_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_10_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_10_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_10_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_10_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_10_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_11
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_11_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x10C)
#define HVSRCregBank_HVSRC_YH_23_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_11_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_11_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_11_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_11_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_11_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_11_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_12
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_12_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x110)
#define HVSRCregBank_HVSRC_YH_23_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_12_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_12_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_12_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_12_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_12_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_12_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_13
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_13_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x114)
#define HVSRCregBank_HVSRC_YH_23_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_13_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_13_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_13_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_13_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_13_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_13_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_14
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_14_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x118)
#define HVSRCregBank_HVSRC_YH_23_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_14_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_14_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_14_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_14_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_14_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_14_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_15
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_15_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x11C)
#define HVSRCregBank_HVSRC_YH_23_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_15_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_15_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_15_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_15_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_15_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_15_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_16
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_16_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x120)
#define HVSRCregBank_HVSRC_YH_23_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_16_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_16_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_16_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_16_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_16_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_16_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_17
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_17_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x124)
#define HVSRCregBank_HVSRC_YH_23_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_17_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_17_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_17_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_17_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_17_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_17_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_18
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_18_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x128)
#define HVSRCregBank_HVSRC_YH_23_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_18_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_18_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_18_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_18_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_18_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_18_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_19
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_19_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x12C)
#define HVSRCregBank_HVSRC_YH_23_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_19_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_19_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_19_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_19_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_19_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_19_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_20
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_20_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x130)
#define HVSRCregBank_HVSRC_YH_23_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_20_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_20_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_20_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_20_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_20_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_20_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_21
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_21_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x134)
#define HVSRCregBank_HVSRC_YH_23_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_21_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_21_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_21_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_21_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_21_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_21_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_22
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_22_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x138)
#define HVSRCregBank_HVSRC_YH_23_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_22_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_22_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_22_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_22_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_22_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_22_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_23
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_23_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x13C)
#define HVSRCregBank_HVSRC_YH_23_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_23_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_23_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_23_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_23_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_23_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_23_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_24
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_24_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x140)
#define HVSRCregBank_HVSRC_YH_23_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_24_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_24_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_24_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_24_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_24_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_24_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_25
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_25_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x144)
#define HVSRCregBank_HVSRC_YH_23_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_25_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_25_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_25_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_25_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_25_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_25_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_26
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_26_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x148)
#define HVSRCregBank_HVSRC_YH_23_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_26_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_26_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_26_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_26_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_26_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_26_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_27
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_27_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x14C)
#define HVSRCregBank_HVSRC_YH_23_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_27_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_27_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_27_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_27_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_27_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_27_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_28
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_28_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x150)
#define HVSRCregBank_HVSRC_YH_23_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_28_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_28_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_28_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_28_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_28_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_28_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_29
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_29_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x154)
#define HVSRCregBank_HVSRC_YH_23_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_29_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_29_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_29_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_29_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_29_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_29_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_30
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_30_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x158)
#define HVSRCregBank_HVSRC_YH_23_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_30_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_30_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_30_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_30_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_30_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_30_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_23_31
* Luma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_YH_23_31_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_23_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x15C)
#define HVSRCregBank_HVSRC_YH_23_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_23_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_YH_23_31_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_23_31_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_31_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_YH_23_31_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_23_31_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_23_31_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_0
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x160)
#define HVSRCregBank_HVSRC_YH_45_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_0_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_0_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_0_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_0_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_0_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_0_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_1
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x164)
#define HVSRCregBank_HVSRC_YH_45_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_1_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_1_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_1_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_1_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_1_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_1_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_2
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x168)
#define HVSRCregBank_HVSRC_YH_45_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_2_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_2_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_2_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_2_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_2_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_2_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_3
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x16C)
#define HVSRCregBank_HVSRC_YH_45_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_3_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_3_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_3_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_3_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_3_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_3_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_4
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x170)
#define HVSRCregBank_HVSRC_YH_45_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_4_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_4_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_4_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_4_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_4_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_4_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_5
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x174)
#define HVSRCregBank_HVSRC_YH_45_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_5_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_5_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_5_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_5_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_5_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_5_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_6
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x178)
#define HVSRCregBank_HVSRC_YH_45_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_6_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_6_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_6_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_6_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_6_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_6_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_7
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x17C)
#define HVSRCregBank_HVSRC_YH_45_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_7_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_7_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_7_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_7_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_7_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_7_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_8
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x180)
#define HVSRCregBank_HVSRC_YH_45_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_8_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_8_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_8_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_8_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_8_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_8_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_9
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_45_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x184)
#define HVSRCregBank_HVSRC_YH_45_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_9_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_9_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_9_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_9_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_9_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_45_9_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_45_10
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_10_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x188)
#define HVSRCregBank_HVSRC_YH_45_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_10_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_10_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_10_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_10_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_10_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_10_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_11
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_11_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x18C)
#define HVSRCregBank_HVSRC_YH_45_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_11_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_11_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_11_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_11_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_11_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_11_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_12
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_12_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x190)
#define HVSRCregBank_HVSRC_YH_45_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_12_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_12_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_12_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_12_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_12_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_12_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_13
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_13_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x194)
#define HVSRCregBank_HVSRC_YH_45_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_13_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_13_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_13_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_13_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_13_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_13_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_14
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_14_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x198)
#define HVSRCregBank_HVSRC_YH_45_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_14_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_14_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_14_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_14_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_14_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_14_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_15
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_15_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x19C)
#define HVSRCregBank_HVSRC_YH_45_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_15_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_15_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_15_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_15_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_15_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_15_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_16
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_16_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1A0)
#define HVSRCregBank_HVSRC_YH_45_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_16_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_16_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_16_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_16_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_16_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_16_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_17
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_17_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1A4)
#define HVSRCregBank_HVSRC_YH_45_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_17_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_17_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_17_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_17_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_17_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_17_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_18
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_18_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1A8)
#define HVSRCregBank_HVSRC_YH_45_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_18_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_18_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_18_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_18_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_18_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_18_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_19
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_19_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1AC)
#define HVSRCregBank_HVSRC_YH_45_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_19_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_19_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_19_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_19_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_19_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_19_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_20
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_20_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1B0)
#define HVSRCregBank_HVSRC_YH_45_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_20_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_20_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_20_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_20_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_20_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_20_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_21
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_21_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1B4)
#define HVSRCregBank_HVSRC_YH_45_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_21_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_21_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_21_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_21_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_21_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_21_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_22
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_22_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1B8)
#define HVSRCregBank_HVSRC_YH_45_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_22_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_22_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_22_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_22_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_22_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_22_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_23
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_23_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1BC)
#define HVSRCregBank_HVSRC_YH_45_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_23_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_23_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_23_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_23_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_23_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_23_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_24
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_24_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1C0)
#define HVSRCregBank_HVSRC_YH_45_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_24_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_24_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_24_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_24_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_24_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_24_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_25
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_25_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1C4)
#define HVSRCregBank_HVSRC_YH_45_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_25_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_25_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_25_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_25_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_25_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_25_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_26
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_26_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1C8)
#define HVSRCregBank_HVSRC_YH_45_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_26_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_26_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_26_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_26_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_26_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_26_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_27
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_27_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1CC)
#define HVSRCregBank_HVSRC_YH_45_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_27_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_27_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_27_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_27_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_27_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_27_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_28
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_28_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1D0)
#define HVSRCregBank_HVSRC_YH_45_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_28_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_28_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_28_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_28_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_28_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_28_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_29
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_29_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1D4)
#define HVSRCregBank_HVSRC_YH_45_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_29_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_29_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_29_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_29_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_29_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_29_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_30
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_30_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1D8)
#define HVSRCregBank_HVSRC_YH_45_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_30_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_30_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_30_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_30_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_30_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_30_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_45_31
* Luma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_YH_45_31_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_45_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1DC)
#define HVSRCregBank_HVSRC_YH_45_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_45_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_YH_45_31_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_45_31_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_31_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_YH_45_31_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_45_31_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_45_31_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_0
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1E0)
#define HVSRCregBank_HVSRC_YH_67_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_0_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_0_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_0_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_0_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_0_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_0_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_1
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1E4)
#define HVSRCregBank_HVSRC_YH_67_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_1_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_1_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_1_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_1_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_1_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_1_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_2
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1E8)
#define HVSRCregBank_HVSRC_YH_67_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_2_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_2_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_2_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_2_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_2_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_2_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_3
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1EC)
#define HVSRCregBank_HVSRC_YH_67_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_3_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_3_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_3_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_3_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_3_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_3_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_4
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1F0)
#define HVSRCregBank_HVSRC_YH_67_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_4_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_4_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_4_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_4_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_4_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_4_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_5
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1F4)
#define HVSRCregBank_HVSRC_YH_67_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_5_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_5_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_5_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_5_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_5_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_5_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_6
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1F8)
#define HVSRCregBank_HVSRC_YH_67_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_6_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_6_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_6_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_6_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_6_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_6_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_7
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x1FC)
#define HVSRCregBank_HVSRC_YH_67_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_7_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_7_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_7_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_7_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_7_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_7_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_8
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x200)
#define HVSRCregBank_HVSRC_YH_67_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_8_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_8_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_8_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_8_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_8_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_8_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_9
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_YH_67_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x204)
#define HVSRCregBank_HVSRC_YH_67_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_9_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_9_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_9_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_9_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_9_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_YH_67_9_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_YH_67_10
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_10_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x208)
#define HVSRCregBank_HVSRC_YH_67_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_10_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_10_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_10_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_10_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_10_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_10_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_11
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_11_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x20C)
#define HVSRCregBank_HVSRC_YH_67_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_11_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_11_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_11_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_11_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_11_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_11_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_12
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_12_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x210)
#define HVSRCregBank_HVSRC_YH_67_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_12_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_12_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_12_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_12_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_12_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_12_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_13
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_13_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x214)
#define HVSRCregBank_HVSRC_YH_67_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_13_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_13_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_13_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_13_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_13_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_13_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_14
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_14_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x218)
#define HVSRCregBank_HVSRC_YH_67_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_14_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_14_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_14_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_14_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_14_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_14_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_15
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_15_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x21C)
#define HVSRCregBank_HVSRC_YH_67_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_15_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_15_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_15_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_15_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_15_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_15_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_16
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_16_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x220)
#define HVSRCregBank_HVSRC_YH_67_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_16_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_16_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_16_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_16_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_16_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_16_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_17
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_17_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x224)
#define HVSRCregBank_HVSRC_YH_67_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_17_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_17_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_17_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_17_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_17_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_17_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_18
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_18_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x228)
#define HVSRCregBank_HVSRC_YH_67_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_18_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_18_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_18_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_18_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_18_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_18_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_19
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_19_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x22C)
#define HVSRCregBank_HVSRC_YH_67_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_19_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_19_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_19_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_19_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_19_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_19_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_20
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_20_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x230)
#define HVSRCregBank_HVSRC_YH_67_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_20_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_20_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_20_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_20_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_20_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_20_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_21
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_21_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x234)
#define HVSRCregBank_HVSRC_YH_67_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_21_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_21_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_21_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_21_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_21_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_21_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_22
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_22_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x238)
#define HVSRCregBank_HVSRC_YH_67_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_22_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_22_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_22_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_22_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_22_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_22_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_23
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_23_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x23C)
#define HVSRCregBank_HVSRC_YH_67_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_23_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_23_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_23_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_23_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_23_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_23_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_24
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_24_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x240)
#define HVSRCregBank_HVSRC_YH_67_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_24_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_24_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_24_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_24_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_24_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_24_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_25
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_25_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x244)
#define HVSRCregBank_HVSRC_YH_67_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_25_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_25_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_25_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_25_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_25_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_25_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_26
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_26_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x248)
#define HVSRCregBank_HVSRC_YH_67_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_26_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_26_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_26_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_26_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_26_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_26_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_27
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_27_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x24C)
#define HVSRCregBank_HVSRC_YH_67_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_27_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_27_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_27_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_27_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_27_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_27_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_28
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_28_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x250)
#define HVSRCregBank_HVSRC_YH_67_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_28_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_28_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_28_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_28_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_28_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_28_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_29
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_29_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x254)
#define HVSRCregBank_HVSRC_YH_67_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_29_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_29_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_29_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_29_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_29_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_29_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_30
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_30_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x258)
#define HVSRCregBank_HVSRC_YH_67_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_30_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_30_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_30_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_30_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_30_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_30_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_YH_67_31
* Luma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_YH_67_31_SIZE                (32)
#define HVSRCregBank_HVSRC_YH_67_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x25C)
#define HVSRCregBank_HVSRC_YH_67_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_YH_67_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_YH_67_31_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_YH_67_31_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_31_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_YH_67_31_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_YH_67_31_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_YH_67_31_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_0
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x260)
#define HVSRCregBank_HVSRC_CH_01_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_0_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_0_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_0_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_0_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_0_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_0_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_1
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x264)
#define HVSRCregBank_HVSRC_CH_01_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_1_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_1_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_1_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_1_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_1_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_1_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_2
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x268)
#define HVSRCregBank_HVSRC_CH_01_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_2_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_2_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_2_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_2_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_2_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_2_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_3
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x26C)
#define HVSRCregBank_HVSRC_CH_01_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_3_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_3_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_3_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_3_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_3_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_3_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_4
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x270)
#define HVSRCregBank_HVSRC_CH_01_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_4_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_4_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_4_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_4_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_4_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_4_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_5
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x274)
#define HVSRCregBank_HVSRC_CH_01_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_5_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_5_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_5_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_5_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_5_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_5_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_6
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x278)
#define HVSRCregBank_HVSRC_CH_01_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_6_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_6_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_6_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_6_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_6_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_6_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_7
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x27C)
#define HVSRCregBank_HVSRC_CH_01_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_7_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_7_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_7_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_7_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_7_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_7_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_8
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x280)
#define HVSRCregBank_HVSRC_CH_01_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_8_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_8_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_8_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_8_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_8_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_8_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_9
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_01_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x284)
#define HVSRCregBank_HVSRC_CH_01_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_9_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_9_COEFFFILTER_0_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_9_COEFFFILTER_0_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_9_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_9_COEFFFILTER_1_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_01_9_COEFFFILTER_1_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_01_10
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_10_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x288)
#define HVSRCregBank_HVSRC_CH_01_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_10_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_10_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_10_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_10_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_10_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_10_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_11
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_11_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x28C)
#define HVSRCregBank_HVSRC_CH_01_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_11_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_11_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_11_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_11_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_11_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_11_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_12
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_12_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x290)
#define HVSRCregBank_HVSRC_CH_01_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_12_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_12_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_12_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_12_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_12_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_12_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_13
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_13_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x294)
#define HVSRCregBank_HVSRC_CH_01_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_13_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_13_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_13_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_13_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_13_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_13_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_14
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_14_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x298)
#define HVSRCregBank_HVSRC_CH_01_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_14_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_14_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_14_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_14_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_14_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_14_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_15
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_15_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x29C)
#define HVSRCregBank_HVSRC_CH_01_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_15_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_15_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_15_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_15_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_15_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_15_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_16
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_16_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2A0)
#define HVSRCregBank_HVSRC_CH_01_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_16_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_16_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_16_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_16_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_16_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_16_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_17
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_17_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2A4)
#define HVSRCregBank_HVSRC_CH_01_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_17_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_17_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_17_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_17_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_17_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_17_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_18
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_18_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2A8)
#define HVSRCregBank_HVSRC_CH_01_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_18_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_18_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_18_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_18_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_18_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_18_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_19
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_19_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2AC)
#define HVSRCregBank_HVSRC_CH_01_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_19_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_19_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_19_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_19_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_19_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_19_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_20
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_20_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2B0)
#define HVSRCregBank_HVSRC_CH_01_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_20_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_20_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_20_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_20_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_20_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_20_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_21
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_21_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2B4)
#define HVSRCregBank_HVSRC_CH_01_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_21_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_21_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_21_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_21_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_21_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_21_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_22
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_22_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2B8)
#define HVSRCregBank_HVSRC_CH_01_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_22_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_22_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_22_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_22_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_22_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_22_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_23
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_23_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2BC)
#define HVSRCregBank_HVSRC_CH_01_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_23_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_23_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_23_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_23_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_23_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_23_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_24
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_24_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2C0)
#define HVSRCregBank_HVSRC_CH_01_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_24_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_24_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_24_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_24_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_24_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_24_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_25
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_25_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2C4)
#define HVSRCregBank_HVSRC_CH_01_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_25_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_25_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_25_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_25_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_25_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_25_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_26
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_26_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2C8)
#define HVSRCregBank_HVSRC_CH_01_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_26_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_26_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_26_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_26_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_26_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_26_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_27
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_27_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2CC)
#define HVSRCregBank_HVSRC_CH_01_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_27_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_27_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_27_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_27_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_27_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_27_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_28
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_28_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2D0)
#define HVSRCregBank_HVSRC_CH_01_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_28_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_28_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_28_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_28_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_28_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_28_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_29
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_29_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2D4)
#define HVSRCregBank_HVSRC_CH_01_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_29_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_29_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_29_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_29_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_29_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_29_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_30
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_30_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2D8)
#define HVSRCregBank_HVSRC_CH_01_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_30_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_30_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_30_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_30_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_30_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_30_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_01_31
* Chroma Horizontal filter Phase 01 Coef
*/

#define HVSRCregBank_HVSRC_CH_01_31_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_01_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2DC)
#define HVSRCregBank_HVSRC_CH_01_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_01_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_0
* horizontal coef 0
*/

#define HVSRCregBank_HVSRC_CH_01_31_COEFFFILTER_0_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_01_31_COEFFFILTER_0_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_31_COEFFFILTER_0_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_1
* horizontal coef 1
*/

#define HVSRCregBank_HVSRC_CH_01_31_COEFFFILTER_1_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_01_31_COEFFFILTER_1_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_01_31_COEFFFILTER_1_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_0
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2E0)
#define HVSRCregBank_HVSRC_CH_23_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_0_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_0_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_0_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_0_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_0_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_0_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_1
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2E4)
#define HVSRCregBank_HVSRC_CH_23_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_1_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_1_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_1_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_1_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_1_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_1_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_2
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2E8)
#define HVSRCregBank_HVSRC_CH_23_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_2_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_2_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_2_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_2_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_2_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_2_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_3
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2EC)
#define HVSRCregBank_HVSRC_CH_23_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_3_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_3_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_3_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_3_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_3_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_3_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_4
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2F0)
#define HVSRCregBank_HVSRC_CH_23_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_4_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_4_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_4_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_4_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_4_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_4_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_5
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2F4)
#define HVSRCregBank_HVSRC_CH_23_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_5_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_5_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_5_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_5_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_5_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_5_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_6
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2F8)
#define HVSRCregBank_HVSRC_CH_23_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_6_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_6_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_6_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_6_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_6_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_6_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_7
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x2FC)
#define HVSRCregBank_HVSRC_CH_23_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_7_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_7_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_7_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_7_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_7_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_7_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_8
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x300)
#define HVSRCregBank_HVSRC_CH_23_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_8_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_8_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_8_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_8_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_8_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_8_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_9
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_23_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x304)
#define HVSRCregBank_HVSRC_CH_23_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_9_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_9_COEFFFILTER_2_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_9_COEFFFILTER_2_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_9_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_9_COEFFFILTER_3_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_23_9_COEFFFILTER_3_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_23_10
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_10_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x308)
#define HVSRCregBank_HVSRC_CH_23_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_10_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_10_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_10_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_10_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_10_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_10_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_11
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_11_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x30C)
#define HVSRCregBank_HVSRC_CH_23_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_11_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_11_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_11_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_11_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_11_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_11_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_12
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_12_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x310)
#define HVSRCregBank_HVSRC_CH_23_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_12_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_12_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_12_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_12_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_12_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_12_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_13
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_13_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x314)
#define HVSRCregBank_HVSRC_CH_23_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_13_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_13_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_13_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_13_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_13_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_13_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_14
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_14_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x318)
#define HVSRCregBank_HVSRC_CH_23_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_14_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_14_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_14_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_14_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_14_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_14_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_15
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_15_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x31C)
#define HVSRCregBank_HVSRC_CH_23_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_15_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_15_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_15_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_15_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_15_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_15_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_16
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_16_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x320)
#define HVSRCregBank_HVSRC_CH_23_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_16_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_16_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_16_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_16_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_16_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_16_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_17
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_17_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x324)
#define HVSRCregBank_HVSRC_CH_23_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_17_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_17_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_17_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_17_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_17_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_17_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_18
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_18_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x328)
#define HVSRCregBank_HVSRC_CH_23_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_18_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_18_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_18_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_18_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_18_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_18_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_19
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_19_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x32C)
#define HVSRCregBank_HVSRC_CH_23_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_19_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_19_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_19_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_19_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_19_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_19_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_20
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_20_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x330)
#define HVSRCregBank_HVSRC_CH_23_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_20_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_20_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_20_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_20_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_20_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_20_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_21
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_21_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x334)
#define HVSRCregBank_HVSRC_CH_23_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_21_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_21_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_21_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_21_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_21_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_21_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_22
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_22_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x338)
#define HVSRCregBank_HVSRC_CH_23_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_22_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_22_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_22_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_22_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_22_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_22_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_23
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_23_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x33C)
#define HVSRCregBank_HVSRC_CH_23_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_23_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_23_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_23_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_23_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_23_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_23_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_24
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_24_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x340)
#define HVSRCregBank_HVSRC_CH_23_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_24_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_24_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_24_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_24_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_24_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_24_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_25
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_25_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x344)
#define HVSRCregBank_HVSRC_CH_23_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_25_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_25_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_25_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_25_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_25_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_25_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_26
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_26_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x348)
#define HVSRCregBank_HVSRC_CH_23_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_26_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_26_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_26_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_26_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_26_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_26_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_27
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_27_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x34C)
#define HVSRCregBank_HVSRC_CH_23_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_27_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_27_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_27_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_27_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_27_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_27_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_28
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_28_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x350)
#define HVSRCregBank_HVSRC_CH_23_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_28_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_28_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_28_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_28_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_28_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_28_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_29
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_29_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x354)
#define HVSRCregBank_HVSRC_CH_23_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_29_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_29_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_29_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_29_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_29_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_29_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_30
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_30_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x358)
#define HVSRCregBank_HVSRC_CH_23_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_30_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_30_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_30_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_30_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_30_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_30_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_23_31
* Chroma Horizontal filter Phase 23 Coef
*/

#define HVSRCregBank_HVSRC_CH_23_31_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_23_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x35C)
#define HVSRCregBank_HVSRC_CH_23_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_23_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_2
* horizontal coef 2
*/

#define HVSRCregBank_HVSRC_CH_23_31_COEFFFILTER_2_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_23_31_COEFFFILTER_2_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_31_COEFFFILTER_2_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_3
* horizontal coef 3
*/

#define HVSRCregBank_HVSRC_CH_23_31_COEFFFILTER_3_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_23_31_COEFFFILTER_3_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_23_31_COEFFFILTER_3_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_0
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x360)
#define HVSRCregBank_HVSRC_CH_45_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_0_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_0_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_0_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_0_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_0_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_0_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_1
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x364)
#define HVSRCregBank_HVSRC_CH_45_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_1_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_1_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_1_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_1_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_1_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_1_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_2
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x368)
#define HVSRCregBank_HVSRC_CH_45_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_2_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_2_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_2_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_2_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_2_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_2_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_3
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x36C)
#define HVSRCregBank_HVSRC_CH_45_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_3_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_3_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_3_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_3_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_3_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_3_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_4
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x370)
#define HVSRCregBank_HVSRC_CH_45_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_4_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_4_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_4_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_4_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_4_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_4_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_5
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x374)
#define HVSRCregBank_HVSRC_CH_45_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_5_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_5_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_5_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_5_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_5_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_5_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_6
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x378)
#define HVSRCregBank_HVSRC_CH_45_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_6_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_6_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_6_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_6_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_6_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_6_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_7
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x37C)
#define HVSRCregBank_HVSRC_CH_45_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_7_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_7_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_7_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_7_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_7_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_7_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_8
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x380)
#define HVSRCregBank_HVSRC_CH_45_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_8_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_8_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_8_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_8_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_8_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_8_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_9
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_45_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x384)
#define HVSRCregBank_HVSRC_CH_45_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_9_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_9_COEFFFILTER_4_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_9_COEFFFILTER_4_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_9_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_9_COEFFFILTER_5_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_45_9_COEFFFILTER_5_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_45_10
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_10_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x388)
#define HVSRCregBank_HVSRC_CH_45_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_10_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_10_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_10_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_10_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_10_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_10_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_11
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_11_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x38C)
#define HVSRCregBank_HVSRC_CH_45_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_11_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_11_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_11_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_11_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_11_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_11_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_12
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_12_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x390)
#define HVSRCregBank_HVSRC_CH_45_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_12_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_12_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_12_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_12_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_12_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_12_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_13
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_13_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x394)
#define HVSRCregBank_HVSRC_CH_45_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_13_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_13_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_13_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_13_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_13_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_13_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_14
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_14_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x398)
#define HVSRCregBank_HVSRC_CH_45_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_14_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_14_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_14_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_14_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_14_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_14_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_15
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_15_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x39C)
#define HVSRCregBank_HVSRC_CH_45_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_15_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_15_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_15_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_15_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_15_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_15_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_16
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_16_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3A0)
#define HVSRCregBank_HVSRC_CH_45_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_16_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_16_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_16_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_16_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_16_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_16_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_17
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_17_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3A4)
#define HVSRCregBank_HVSRC_CH_45_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_17_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_17_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_17_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_17_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_17_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_17_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_18
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_18_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3A8)
#define HVSRCregBank_HVSRC_CH_45_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_18_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_18_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_18_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_18_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_18_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_18_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_19
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_19_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3AC)
#define HVSRCregBank_HVSRC_CH_45_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_19_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_19_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_19_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_19_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_19_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_19_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_20
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_20_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3B0)
#define HVSRCregBank_HVSRC_CH_45_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_20_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_20_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_20_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_20_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_20_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_20_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_21
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_21_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3B4)
#define HVSRCregBank_HVSRC_CH_45_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_21_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_21_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_21_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_21_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_21_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_21_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_22
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_22_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3B8)
#define HVSRCregBank_HVSRC_CH_45_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_22_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_22_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_22_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_22_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_22_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_22_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_23
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_23_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3BC)
#define HVSRCregBank_HVSRC_CH_45_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_23_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_23_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_23_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_23_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_23_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_23_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_24
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_24_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3C0)
#define HVSRCregBank_HVSRC_CH_45_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_24_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_24_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_24_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_24_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_24_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_24_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_25
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_25_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3C4)
#define HVSRCregBank_HVSRC_CH_45_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_25_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_25_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_25_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_25_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_25_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_25_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_26
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_26_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3C8)
#define HVSRCregBank_HVSRC_CH_45_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_26_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_26_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_26_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_26_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_26_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_26_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_27
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_27_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3CC)
#define HVSRCregBank_HVSRC_CH_45_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_27_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_27_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_27_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_27_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_27_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_27_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_28
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_28_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3D0)
#define HVSRCregBank_HVSRC_CH_45_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_28_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_28_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_28_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_28_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_28_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_28_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_29
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_29_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3D4)
#define HVSRCregBank_HVSRC_CH_45_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_29_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_29_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_29_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_29_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_29_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_29_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_30
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_30_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3D8)
#define HVSRCregBank_HVSRC_CH_45_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_30_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_30_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_30_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_30_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_30_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_30_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_45_31
* Chroma Horizontal filter Phase 45 Coef
*/

#define HVSRCregBank_HVSRC_CH_45_31_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_45_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3DC)
#define HVSRCregBank_HVSRC_CH_45_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_45_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_4
* horizontal coef 4
*/

#define HVSRCregBank_HVSRC_CH_45_31_COEFFFILTER_4_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_45_31_COEFFFILTER_4_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_31_COEFFFILTER_4_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_5
* horizontal coef 5
*/

#define HVSRCregBank_HVSRC_CH_45_31_COEFFFILTER_5_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_45_31_COEFFFILTER_5_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_45_31_COEFFFILTER_5_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_0
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_0_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_0_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3E0)
#define HVSRCregBank_HVSRC_CH_67_0_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_0_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_0_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_0_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_0_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_0_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_0_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_0_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_0_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_0_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_0_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_0_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_1
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_1_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_1_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3E4)
#define HVSRCregBank_HVSRC_CH_67_1_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_1_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_1_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_1_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_1_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_1_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_1_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_1_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_1_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_1_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_1_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_1_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_2
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_2_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_2_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3E8)
#define HVSRCregBank_HVSRC_CH_67_2_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_2_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_2_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_2_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_2_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_2_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_2_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_2_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_2_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_2_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_2_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_2_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_3
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_3_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_3_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3EC)
#define HVSRCregBank_HVSRC_CH_67_3_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_3_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_3_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_3_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_3_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_3_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_3_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_3_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_3_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_3_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_3_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_3_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_4
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_4_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_4_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3F0)
#define HVSRCregBank_HVSRC_CH_67_4_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_4_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_4_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_4_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_4_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_4_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_4_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_4_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_4_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_4_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_4_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_4_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_5
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_5_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_5_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3F4)
#define HVSRCregBank_HVSRC_CH_67_5_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_5_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_5_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_5_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_5_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_5_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_5_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_5_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_5_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_5_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_5_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_5_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_6
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_6_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_6_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3F8)
#define HVSRCregBank_HVSRC_CH_67_6_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_6_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_6_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_6_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_6_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_6_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_6_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_6_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_6_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_6_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_6_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_6_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_7
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_7_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_7_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x3FC)
#define HVSRCregBank_HVSRC_CH_67_7_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_7_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_7_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_7_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_7_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_7_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_7_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_7_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_7_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_7_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_7_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_7_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_8
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_8_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_8_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x400)
#define HVSRCregBank_HVSRC_CH_67_8_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_8_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_8_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_8_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_8_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_8_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_8_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_8_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_8_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_8_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_8_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_8_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_9
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_9_SIZE                 (32)
#define HVSRCregBank_HVSRC_CH_67_9_OFFSET               (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x404)
#define HVSRCregBank_HVSRC_CH_67_9_RESET_VALUE          (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_9_BITFIELD_MASK        (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_9_RWMASK               (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_9_ROMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_9_WOMASK               (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_9_UNUSED_MASK          (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_9_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_9_COEFFFILTER_6_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_9_COEFFFILTER_6_MASK   (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_9_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_9_COEFFFILTER_7_WIDTH  (10)
#define HVSRCregBank_HVSRC_CH_67_9_COEFFFILTER_7_MASK   (0x03FF0000)

/**
* Register : HVSRC_CH_67_10
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_10_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_10_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x408)
#define HVSRCregBank_HVSRC_CH_67_10_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_10_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_10_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_10_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_10_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_10_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_10_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_10_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_10_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_10_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_10_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_10_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_11
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_11_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_11_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x40C)
#define HVSRCregBank_HVSRC_CH_67_11_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_11_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_11_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_11_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_11_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_11_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_11_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_11_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_11_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_11_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_11_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_11_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_12
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_12_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_12_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x410)
#define HVSRCregBank_HVSRC_CH_67_12_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_12_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_12_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_12_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_12_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_12_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_12_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_12_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_12_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_12_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_12_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_12_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_13
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_13_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_13_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x414)
#define HVSRCregBank_HVSRC_CH_67_13_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_13_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_13_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_13_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_13_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_13_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_13_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_13_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_13_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_13_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_13_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_13_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_14
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_14_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_14_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x418)
#define HVSRCregBank_HVSRC_CH_67_14_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_14_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_14_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_14_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_14_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_14_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_14_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_14_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_14_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_14_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_14_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_14_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_15
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_15_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_15_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x41C)
#define HVSRCregBank_HVSRC_CH_67_15_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_15_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_15_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_15_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_15_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_15_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_15_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_15_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_15_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_15_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_15_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_15_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_16
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_16_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_16_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x420)
#define HVSRCregBank_HVSRC_CH_67_16_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_16_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_16_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_16_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_16_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_16_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_16_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_16_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_16_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_16_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_16_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_16_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_17
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_17_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_17_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x424)
#define HVSRCregBank_HVSRC_CH_67_17_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_17_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_17_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_17_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_17_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_17_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_17_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_17_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_17_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_17_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_17_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_17_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_18
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_18_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_18_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x428)
#define HVSRCregBank_HVSRC_CH_67_18_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_18_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_18_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_18_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_18_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_18_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_18_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_18_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_18_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_18_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_18_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_18_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_19
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_19_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_19_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x42C)
#define HVSRCregBank_HVSRC_CH_67_19_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_19_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_19_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_19_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_19_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_19_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_19_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_19_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_19_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_19_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_19_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_19_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_20
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_20_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_20_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x430)
#define HVSRCregBank_HVSRC_CH_67_20_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_20_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_20_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_20_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_20_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_20_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_20_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_20_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_20_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_20_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_20_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_20_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_21
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_21_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_21_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x434)
#define HVSRCregBank_HVSRC_CH_67_21_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_21_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_21_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_21_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_21_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_21_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_21_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_21_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_21_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_21_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_21_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_21_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_22
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_22_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_22_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x438)
#define HVSRCregBank_HVSRC_CH_67_22_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_22_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_22_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_22_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_22_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_22_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_22_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_22_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_22_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_22_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_22_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_22_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_23
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_23_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_23_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x43C)
#define HVSRCregBank_HVSRC_CH_67_23_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_23_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_23_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_23_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_23_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_23_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_23_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_23_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_23_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_23_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_23_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_23_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_24
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_24_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_24_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x440)
#define HVSRCregBank_HVSRC_CH_67_24_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_24_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_24_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_24_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_24_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_24_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_24_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_24_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_24_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_24_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_24_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_24_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_25
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_25_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_25_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x444)
#define HVSRCregBank_HVSRC_CH_67_25_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_25_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_25_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_25_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_25_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_25_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_25_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_25_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_25_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_25_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_25_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_25_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_26
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_26_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_26_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x448)
#define HVSRCregBank_HVSRC_CH_67_26_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_26_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_26_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_26_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_26_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_26_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_26_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_26_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_26_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_26_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_26_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_26_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_27
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_27_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_27_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x44C)
#define HVSRCregBank_HVSRC_CH_67_27_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_27_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_27_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_27_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_27_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_27_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_27_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_27_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_27_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_27_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_27_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_27_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_28
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_28_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_28_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x450)
#define HVSRCregBank_HVSRC_CH_67_28_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_28_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_28_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_28_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_28_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_28_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_28_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_28_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_28_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_28_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_28_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_28_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_29
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_29_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_29_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x454)
#define HVSRCregBank_HVSRC_CH_67_29_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_29_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_29_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_29_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_29_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_29_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_29_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_29_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_29_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_29_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_29_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_29_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_30
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_30_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_30_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x458)
#define HVSRCregBank_HVSRC_CH_67_30_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_30_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_30_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_30_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_30_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_30_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_30_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_30_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_30_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_30_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_30_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_30_COEFFFILTER_7_MASK  (0x03FF0000)

/**
* Register : HVSRC_CH_67_31
* Chroma Horizontal filter Phase 67 Coef
*/

#define HVSRCregBank_HVSRC_CH_67_31_SIZE                (32)
#define HVSRCregBank_HVSRC_CH_67_31_OFFSET              (HVSRCregBank_HVSRC_pu_reg_BASE_ADDR + 0x45C)
#define HVSRCregBank_HVSRC_CH_67_31_RESET_VALUE         (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_31_BITFIELD_MASK       (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_31_RWMASK              (0x03FF03FF)
#define HVSRCregBank_HVSRC_CH_67_31_ROMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_31_WOMASK              (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_31_UNUSED_MASK         (0xFC00FC00)

/**
* Bit-field : COEFFFILTER_6
* horizontal coef 6
*/

#define HVSRCregBank_HVSRC_CH_67_31_COEFFFILTER_6_OFFSET (0x00000000)
#define HVSRCregBank_HVSRC_CH_67_31_COEFFFILTER_6_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_31_COEFFFILTER_6_MASK  (0x000003FF)

/**
* Bit-field : COEFFFILTER_7
* horizontal coef 7
*/

#define HVSRCregBank_HVSRC_CH_67_31_COEFFFILTER_7_OFFSET (0x00000010)
#define HVSRCregBank_HVSRC_CH_67_31_COEFFFILTER_7_WIDTH (10)
#define HVSRCregBank_HVSRC_CH_67_31_COEFFFILTER_7_MASK  (0x03FF0000)

#endif /** _HVSRCREGBANK_H_ */
