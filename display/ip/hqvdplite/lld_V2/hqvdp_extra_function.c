/*
 * This file is part of HQVDP Low-Level Driver.
 *
 * Copyright 2015, STMicroelectronics - All Rights Reserved
 *
 * License type: GPLv2.0
 *
 * HQVDP Low-Level Driver is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * HQVDP Low-Level Driver is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HQVDP Low-Level Driver. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "hqvdp_extra_function.h"

#include "hqvdp_mailbox_macros.h"
#include "hqvdplite_ip_mapping.h"
#include "hqvdp_lld_platform.h"
#include "c8fvp3_FW_lld_global.h"
#include "c8fvp3_FW_lld.h"


bool c8fvp3_IsCmdPending(void *base_addr)
{
        uint32_t pending_cmd;
        yield();

        pending_cmd = REG32_READ(base_addr,
                                 HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_SOFT_VSYNC_OFFSET)
                                 & mailbox_SOFT_VSYNC_CMD_PENDING_MASK;

        if ( pending_cmd == mailbox_SOFT_VSYNC_CMD_PENDING_MASK ) {
                TRC(TRC_ID_HQVDPLITE_LLD,
                    "(HQVDP %p) [EXTRA] Processing is finished",
                    base_addr);
                return 1;
        } else {
                return 0;
        }

}

bool hqvdp_extra_is_cmd_pending(hqvdp_lld_hdl_t hdl)
{
        int i;
        int pending_cnt = 0;

        for (i=0; i < hdl->reserved_resources; i++ ) {
                if (c8fvp3_IsCmdPending(hdl->base_addr[i]))
                        pending_cnt++;
        }
        /* not command pending, if no HQVDP hw has pending command */
        if ( pending_cnt == 0 )
                return false;
        else
                return true;
}

bool hqvdp_extra_is_ready(hqvdp_lld_hdl_t hdl)
{
        int i;
        int ready_cnt = 0;

        for (i=0; i < hdl->reserved_resources; i++ ) {
                if (c8fvp3_IsReady(hdl->base_addr[i]))
                        ready_cnt++;
        }
        /* ready if all hqvdp are ready */
        if ( ready_cnt == hdl->reserved_resources )
                return true;
        else
                return false;
}

void c8fvp3_ClearIrqToHost(void *base_addr) {
        REG32_WRITE(base_addr,
                    HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_IRQ_TO_HOST_OFFSET, 0);
        /* debug */
        TRC(TRC_ID_HQVDPLITE_LLD, "(HQVDP %p) [EXTRA] done",
            base_addr);
}

bool c8fvp3_GetEopStatus(void *base_addr) {
        uint32_t eop;

        yield();

        eop = REG32_READ(base_addr, HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_INFO_HOST_OFFSET)
              & mailbox_INFO_HOST_FW_EOP_MASK;

        if ( eop ) {
                TRC(TRC_ID_HQVDPLITE_LLD,
                    "(HQVDP %p) [EXTRA] Processing is finished",
                    base_addr);
                return true;
        } else {
                return false;
        }
}

bool hqvdp_extra_get_eop_status(hqvdp_lld_hdl_t hdl)
{
        int i;
        int eop_cnt = 0;

        for (i=0; i < hdl->reserved_resources; i++ ) {
                if(c8fvp3_GetEopStatus(hdl->base_addr[i]))
                        eop_cnt++;
        }
        /* eop if wll hqvdp have finished processing */
        if (eop_cnt == hdl->reserved_resources)
                return true;
        else
                return false;
}

void c8fvp3_ClearInfoToHost(void *base_addr) {
        /* read modify write INFO to Host */
        REG32_WRITE(base_addr,
                    HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_INFO_HOST_OFFSET,
                    REG32_READ(base_addr,
                               HQVDP_LITE_MAILBOX_REGS_OFFSET + mailbox_INFO_HOST_OFFSET)
                    & ~mailbox_INFO_HOST_FW_EOP_MASK);
        /* debug */
        TRC(TRC_ID_HQVDPLITE_LLD, "(HQVDP %p) [EXTRA] done",
            base_addr);
}


