/************************************************************************
This file is part of the Display Engine.
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

Display Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Display Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Display Engine. If not, write to the Free Software Foundation,
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplayNode.h>

extern "C" {
#include "blitter.h"
}

#include "buffercopy.h"

#define PICTURE_BUFFER_ALIGN    64
#define BUFFERCOPY_BLITTER_ID   0

#define UHD_MAX_WIDTH           4096
#define UHD_MAX_HEIGHT          2176


static bool is_copy_buffer_used = false;
static DMA_Area copy_buffer_dma_area;
static uint32_t copy_buffer_total_size = 0;
static void* copy_buffer_mutex = NULL;


// Bz83837: Allocation of CopyBuffer is now done at module loading time.
//          We allocate a single pair of primary + secondary buffers
//          "Flush with copy" and "Plane's Pause" will share the use of those buffers
//          If not available:
//          - A Flush ALL will be done instead of the "Flush with copy"
//          - The pause will not happen
bool CBufferCopy::AllocateCopiedPictBuffers(void)
{
    uint32_t primary_buffer_size;
    uint32_t secondary_buffer_size;

    if(copy_buffer_mutex != NULL)
    {
        TRC( TRC_ID_ERROR, "Mutex already created");
        return false;
    }

    copy_buffer_mutex = vibe_os_create_mutex();
    if(copy_buffer_mutex == 0)
    {
      TRC( TRC_ID_ERROR, "Failed to create Copy Buffer mutex" );
      return false;
    }

    // Primary and secondary picture buffers must be allocated in same area
    // due to security constraint (for minimizing the number of regions)

    // primary_buffer_size = (UHD_MAX_WIDTH * UHD_MAX_HEIGHT) (3 / 2) * (10 / 8)
    //   3 / 2 is for 4:2:0 on 8 bits: For each pixel there is one Byte of Luma + 0.5 Byte of Chroma
    //  10 / 8 is for 10 bits sources.
    primary_buffer_size = (UHD_MAX_WIDTH * UHD_MAX_HEIGHT * 3 * 10) / (8 * 2);
    // In case of UHD source we assume that a decimation of at least H2V2 is used thus the size is reduced by at least 4
    secondary_buffer_size = primary_buffer_size / 4,

    copy_buffer_total_size = primary_buffer_size + secondary_buffer_size;
    TRC(TRC_ID_MAIN_INFO, "Allocate %d Bytes for copy buffer", copy_buffer_total_size);

    vibe_os_zero_memory(&copy_buffer_dma_area, sizeof(copy_buffer_dma_area));

    vibe_os_allocate_dma_area(&copy_buffer_dma_area,
                               copy_buffer_total_size,
                               PICTURE_BUFFER_ALIGN,
                               (STM_DMA_AREA_ALLOC_FLAGS)(SDAAF_UNCACHED|SDAAF_SECURE));
    if (!copy_buffer_dma_area.pMemory)
    {
        TRC( TRC_ID_ERROR, "Could not allocate %d Bytes for copy buffer!", copy_buffer_total_size);
        vibe_os_zero_memory(&copy_buffer_dma_area, sizeof(copy_buffer_dma_area));
        copy_buffer_total_size = 0;
        return false;
    }

    return true;
}

bool CBufferCopy::FreeCopiedPictBuffers(void)
{
    if(is_copy_buffer_used)
    {
        // "Flush with copy" or "plane pause" should be ended before unloading the modules
        TRC( TRC_ID_ERROR, "Copy buffer is still in use!");
    }

    if(copy_buffer_dma_area.pMemory)
    {
        TRC( TRC_ID_MAIN_INFO, "deleting copy buffer" );
        vibe_os_free_dma_area(&copy_buffer_dma_area);
    }

    if(copy_buffer_mutex != NULL)
    {
        vibe_os_delete_mutex(copy_buffer_mutex);
        copy_buffer_mutex = NULL;
    }

    return true;
}


bool CBufferCopy::GetCopiedPictBuffers(uint32_t            primary_buffer_size,
                                       uint32_t            secondary_buffer_size,
                                       CopiedPictBuffers **dst_copied_buffers)
{
    CopiedPictBuffers *p_dst_copied_buffers = 0;

    vibe_os_lock_mutex(copy_buffer_mutex);
    if(is_copy_buffer_used)
    {
        // NB this is not really an error since multi-threading can lead to concurrent attempts
        // to get the copy buffer
        TRC( TRC_ID_MAIN_INFO, "Copy Buffer already in use");
        vibe_os_unlock_mutex(copy_buffer_mutex);
        return false;
    }

    if (primary_buffer_size + secondary_buffer_size == 0)
    {
        TRC( TRC_ID_ERROR, "primary_buffer_size and secondary_buffer_size are null!");
        vibe_os_unlock_mutex(copy_buffer_mutex);
        return false;
    }

    if (primary_buffer_size + secondary_buffer_size > copy_buffer_total_size)
    {
        TRC( TRC_ID_ERROR, "Copy Buffer not big enough to receive %d Bytes!", primary_buffer_size + secondary_buffer_size);
        vibe_os_unlock_mutex(copy_buffer_mutex);
        return false;
    }

    p_dst_copied_buffers = new CopiedPictBuffers;
    if (!p_dst_copied_buffers)
    {
        TRC( TRC_ID_ERROR, "Could not allocate CopiedPictBuffers for copying pictures");
        vibe_os_unlock_mutex(copy_buffer_mutex);
        return false;
    }
    vibe_os_zero_memory(p_dst_copied_buffers, sizeof(CopiedPictBuffers));

    p_dst_copied_buffers->primary_size        = primary_buffer_size;
    p_dst_copied_buffers->primary_phys_addr   = primary_buffer_size ? (char *)copy_buffer_dma_area.ulPhysical : 0;

    p_dst_copied_buffers->secondary_size      = secondary_buffer_size;
    p_dst_copied_buffers->secondary_phys_addr = secondary_buffer_size ? (char *)copy_buffer_dma_area.ulPhysical + p_dst_copied_buffers->primary_size : 0;

    is_copy_buffer_used = true;
    TRC( TRC_ID_MAIN_INFO, "Copy buffer in use" );
    vibe_os_unlock_mutex(copy_buffer_mutex);

    *dst_copied_buffers = p_dst_copied_buffers;
    return true;
}

// This release the Copy Buffer but doesn't free the memory (this is done by FreeCopiedPictBuffers)
void CBufferCopy::ReleaseCopiedPictBuffers(CopiedPictBuffers *dst_copied_buffers)
{
    if(dst_copied_buffers != NULL)
    {
        TRC( TRC_ID_MAIN_INFO, "deleting copied picture buffers" );
        delete dst_copied_buffers;
        vibe_os_lock_mutex(copy_buffer_mutex);
        is_copy_buffer_used = false;
        vibe_os_unlock_mutex(copy_buffer_mutex);
        TRC( TRC_ID_MAIN_INFO, "Copy buffer no more used" );
    }
    else
    {
        TRC( TRC_ID_ERROR, "Invalid dst_copied_buffers!");
    }
}

bool CBufferCopy::PerformCopy(CDisplayNode                  *src_node_to_copy,
                              stm_buffer_src_picture_desc_t *src_selected_picture,
                              void                          *dst_phys_addr)
{
    stm_blitter_h                       blitter_handle      = 0;  // blitter handle
    stm_blitter_surface_h               blitter_src_surface = 0;  // blitter source surface
    stm_blitter_surface_h               blitter_dst_surface = 0;  // blitter destination surface

    stm_blitter_surface_format_t        format          = STM_BLITTER_SF_ARGB;
    stm_blitter_surface_colorspace_t    colorspace      = STM_BLITTER_SCS_RGB;
    stm_blitter_surface_address_t       buffer_address;
    unsigned long                       buffer_size;
    stm_blitter_dimension_t             dimension;
    unsigned long                       stride;

    stm_blitter_rect_t                  src_rect;
    stm_blitter_point_t                 dst_point;

    stm_blitter_serial_t                serial;

    int                                 result;
    bool                                status = false;

    // Check input parameters
    if (!src_node_to_copy)
    {
        TRC(TRC_ID_ERROR, "No picture buffer to copy");
        return false;
    }

    if (!src_selected_picture)
    {
        TRC(TRC_ID_ERROR, "invalid source selected picture to copy");
        return false;
    }

    if (!dst_phys_addr)
    {
        TRC(TRC_ID_ERROR, "invalid destination address");
        return false;
    }

    // Get blitter handle
    result = stm_blitter_get_device(BUFFERCOPY_BLITTER_ID, &blitter_handle);
    if (result != 0)
    {
        TRC( TRC_ID_ERROR, "Failed to get a handle on BlitterId %d result=%d", BUFFERCOPY_BLITTER_ID, result );
        goto ending_copy;
    }

    // Use the blitter in DMA copy mode, so source and destination surfaces must have same features
    // Here, we choose ARGB format whatever the source picture to copy
    // Keep same buffer_size and pitch/stride values as source picture and calculate others: width & height

    // Set source surface
    buffer_address.base        = src_selected_picture->video_buffer_addr;
    buffer_address.cbcr_offset = 0; // No chroma for ARGB

    buffer_size                = src_selected_picture->video_buffer_size;
    stride                     = src_selected_picture->pitch;

    dimension.h = (src_selected_picture->video_buffer_size / src_selected_picture->pitch);
    dimension.w = (src_selected_picture->pitch / 4); // Why divide by 4 because 1 pixel = 4 x 8 bytes = ARGB

    TRC( TRC_ID_MAIN_INFO, "copy src adr=%lx offset=%lx size=%lx w=%ld h=%ld stride=%lu",
            buffer_address.base, buffer_address.cbcr_offset, buffer_size, dimension.w, dimension.h, stride);

    result = stm_blitter_surface_get(format, colorspace, &buffer_address, buffer_size, &dimension, stride,
            &blitter_src_surface);
    if (result != 0)
    {
        TRC( TRC_ID_ERROR, "couldn't create source surface: 0x%p", blitter_src_surface );
        goto ending_copy;
    }

    // Set destination surface
    buffer_address.base        = (unsigned long)dst_phys_addr;

    TRC( TRC_ID_MAIN_INFO, "copy dst adr=%lx offset=%lx size=%lx w=%ld h=%ld stride=%lu",
            buffer_address.base, buffer_address.cbcr_offset, buffer_size, dimension.w, dimension.h, stride);

    result = stm_blitter_surface_get(format, colorspace, &buffer_address, buffer_size, &dimension, stride,
            &blitter_dst_surface);
    if (result != 0)
    {
        TRC( TRC_ID_ERROR, "couldn't create destination surface: 0x%p", blitter_dst_surface );
        blitter_dst_surface = 0;
        goto ending_copy;
    }

    // Set fence flag for waiting blitting operation to finish without polling
    result = stm_blitter_surface_add_fence(blitter_dst_surface);
    if (result != 0)
    {
        TRC( TRC_ID_ERROR, "Failed, setting fence flag" );
        goto ending_copy;
    }

    // Request simple copy without rescaling
    src_rect.position.x = 0;
    src_rect.position.y = 0;
    src_rect.size = dimension;

    dst_point.x = 0;
    dst_point.y = 0;
    result = stm_blitter_surface_blit(blitter_handle, blitter_src_surface, &src_rect,
            blitter_dst_surface, &dst_point, 1);
    if (result != 0)
    {
        TRC( TRC_ID_ERROR, "Failed, configuring simple copy" );
        goto ending_copy;
    }

    // Created waiting point
    result = stm_blitter_surface_get_serial(blitter_dst_surface, &serial);
    if (result != 0)
    {
        TRC( TRC_ID_ERROR, "Failed, getting serial of simple copy" );
        goto ending_copy;
    }

    // Wait end of simple copy
    result = stm_blitter_wait(blitter_handle, STM_BLITTER_WAIT_FENCE, serial);
    if (result != 0)
    {
        TRC( TRC_ID_ERROR, "Failed, waiting serial of simple copy" );
        goto ending_copy;
    }

    status = true;

ending_copy:
    // Release source surface if not done
    if (blitter_src_surface)
        stm_blitter_surface_put(blitter_src_surface);
    blitter_src_surface = 0;

    // Release destination surface if not done
    if (blitter_dst_surface)
        stm_blitter_surface_put(blitter_dst_surface);
    blitter_dst_surface = 0;

    // Release blitter handle
    if (blitter_handle)
        stm_blitter_put (blitter_handle);
    blitter_handle = 0;

    return status;
}

bool CBufferCopy::PerformCopiedPictBuffers(CDisplayNode *src_node_to_copy,
                                           CopiedPictBuffers *dst_copied_buffers)
{
    // Check input parameters
    if (!src_node_to_copy)
    {
        TRC(TRC_ID_ERROR, "No picture buffer to copy");
        return false;
    }

    if (!dst_copied_buffers)
    {
        TRC(TRC_ID_ERROR, "invalid destination address");
        return false;
    }

    // Copy primary picture
    if (dst_copied_buffers->primary_phys_addr)
    {
        TRC(TRC_ID_MAIN_INFO, "Start BLITTER copy of primary picture");

        if(!PerformCopy(src_node_to_copy, &(src_node_to_copy->m_bufferDesc.src.primary_picture),
                dst_copied_buffers->primary_phys_addr))
        {
            TRC(TRC_ID_ERROR, "Fail to copy primary picture");
            return false;
        }

        TRC(TRC_ID_MAIN_INFO, "End BLITTER copy of primary picture");

    }

    // Copy secondary picture
    if (dst_copied_buffers->secondary_phys_addr)
    {
        TRC(TRC_ID_MAIN_INFO, "Start BLITTER copy of secondary picture");

        if(!PerformCopy(src_node_to_copy, &(src_node_to_copy->m_bufferDesc.src.secondary_picture),
                dst_copied_buffers->secondary_phys_addr))
        {
            TRC(TRC_ID_ERROR, "Fail to copy secondary picture");
            return false;
        }

        TRC(TRC_ID_MAIN_INFO, "End BLITTER copy of secondary picture");
    }

    return true;
}

bool CBufferCopy::AllocateCopiedNode(CDisplayNode ** ppDestNode,
        CDisplayNode * pSrcNode,
        struct CopiedPictBuffers * pDstCopiedBuffers )
{
    // Allocate new node
    *ppDestNode  = new CDisplayNode;
    if(!*ppDestNode)
    {
        TRC( TRC_ID_ERROR, "Failed to allocate a CDisplayNode!" );
        return false;
    }

    // Copy node parameters
    (*ppDestNode)->m_pictureId           = 0; // specific value to identified copied picture
    (*ppDestNode)->m_bufferDesc          = pSrcNode->m_bufferDesc;
    (*ppDestNode)->m_srcPictureType      = pSrcNode->m_srcPictureType;
    (*ppDestNode)->m_srcPictureTypeChar  = pSrcNode->m_srcPictureTypeChar;
    (*ppDestNode)->m_doNotDisplay        = pSrcNode->m_doNotDisplay;
    (*ppDestNode)->m_firstFieldOnly      = pSrcNode->m_firstFieldOnly;
    (*ppDestNode)->m_repeatFirstField    = pSrcNode->m_repeatFirstField;
    (*ppDestNode)->m_firstFieldType      = pSrcNode->m_firstFieldType;

    // Update some parameters
    (*ppDestNode)->m_bufferDesc.src.primary_picture.video_buffer_addr   = (uint32_t)pDstCopiedBuffers->primary_phys_addr;
    (*ppDestNode)->m_bufferDesc.src.secondary_picture.video_buffer_addr = (uint32_t)pDstCopiedBuffers->secondary_phys_addr;
    (*ppDestNode)->m_bufferDesc.info.nfields            = 1;
    (*ppDestNode)->m_bufferDesc.info.puser_data         = pDstCopiedBuffers; // create a structure with all info needed that will be delete by callback
    (*ppDestNode)->m_bufferDesc.info.display_callback   = NULL;
    (*ppDestNode)->m_bufferDesc.info.completed_callback = NULL;
    return true;
}
