/************************************************************************
This file is part of the Display Engine.
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

Display Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Display Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Display Engine.  If not, write to the Free Software Foundation,
51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

************************************************************************/

/*
 * @file buffercopy.cpp
 * @brief utility functions to make a copy of a display node
 */

#ifndef BUFFERCOPY_H
#define BUFFERCOPY_H

struct CopiedPictBuffers
{
    void     *primary_phys_addr;
    uint32_t  primary_size;

    void     *secondary_phys_addr;
    uint32_t  secondary_size;
};

class CBufferCopy {
public:
    static bool AllocateCopiedPictBuffers(void);
    static bool FreeCopiedPictBuffers(void);

    static bool GetCopiedPictBuffers(uint32_t            primary_buffer_size,
                                     uint32_t            secondary_buffer_size,
                                     CopiedPictBuffers **dst_copied_buffers);
    static void ReleaseCopiedPictBuffers(CopiedPictBuffers *dst_copied_buffers);

    static bool PerformCopiedPictBuffers(CDisplayNode *src_node_to_copy,
                                         CopiedPictBuffers *dst_copied_buffers);
    static bool AllocateCopiedNode(CDisplayNode ** ppDestNode,
                                  CDisplayNode * pSrcNode,
                                  struct CopiedPictBuffers * pDstCopiedBuffers);

protected:
    static bool PerformCopy(CDisplayNode *src_node_to_copy,
                            stm_buffer_src_picture_desc_t *src_selected_picture,
                            void *dst_phys_addr);
};

#endif /* BUFFERCOPY_H */
