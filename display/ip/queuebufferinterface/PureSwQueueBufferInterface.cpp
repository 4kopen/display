/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2011-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-08-21
***************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include "PureSwQueueBufferInterface.h"
#include "display/ip/buffercopy/buffercopy.h"

// extern "C" section
extern "C" {

struct private_wait_flush_param_s
{
  CPureSwQueueBufferInterface *pQueueBufferInterface;
  const void * const  user;
};

/* Evaluate if the conditions are met to complete the Flush.
   NB: The condition is specific to each kind of Flush (FLUSH_ALL, FLUSH_PARTIAL_WITH_COPY)

   This function returns:
   '0' if the wait should continue
   '1' if the wait is finished and the flush can be completed
*/
int evaluate_flush_condition(void *obj)
{
    struct private_wait_flush_param_s *flush_param = (struct private_wait_flush_param_s *)obj;

    if (flush_param == 0 || flush_param->pQueueBufferInterface == 0)
    {
        TRC( TRC_ID_ERROR, "Critical error! Invalid flush_param = 0x%p", flush_param );
        // stop the wait
        return 1;
    }

    return flush_param->pQueueBufferInterface->EvaluateFlushCondition(flush_param->user);
}


}
// extern "C" section

void ReleaseCopiedPictBuffersCallback(void* user_data, const stm_buffer_presentation_stats_t *stat)
{
    CopiedPictBuffers *dst_copied_buffers = (CopiedPictBuffers *)user_data;

    TRC( TRC_ID_MAIN_INFO, "deleting copied picture buffers" );
    CBufferCopy::ReleaseCopiedPictBuffers(dst_copied_buffers);
}



///////////////////////////////////////////////////////////////////////////////
// Base class for G2 and G3 compositor queue buffer source interface
//

CPureSwQueueBufferInterface::CPureSwQueueBufferInterface ( uint32_t interfaceID, CDisplaySource * pSource, bool isConnectionToVideoPlaneAllowed )
    : CQueueBufferInterface(interfaceID, pSource)
{
  TRC( TRC_ID_MAIN_INFO, "Create PureSwQueueBufferInterface with Id = %d", interfaceID );

  m_isConnectionToVideoPlaneAllowed = isConnectionToVideoPlaneAllowed;

  m_user                   = 0;
  m_IncomingPictureCounter = 0;
  m_lastPresentationTime   = 0;
  m_ulTimingID             = 0;
  m_LatencyParams          = 0;
  m_LatencyPlaneIds        = 0;
  m_GhostPictures          = 0;

  ResetTriplet(&m_picturesPreparedForNextVSync);
  ResetTriplet(&m_picturesUsedByHw);

  vibe_os_zero_memory( &m_outputInfo, sizeof( m_outputInfo ));

  m_flushAllWaitQueue       = 0;
  m_flushStatus             = NO_FLUSH;
  m_blitterId               = 0;

  m_nodeToCopy              = 0;
  m_nodeCopied              = 0;

  m_flushStartTime          = 0;
  m_blitterCopyStartTime    = 0;

  TRC( TRC_ID_MAIN_INFO, "Created Queue = 0x%p", this );
}


CPureSwQueueBufferInterface::~CPureSwQueueBufferInterface()
{
  TRC( TRC_ID_MAIN_INFO, "QueueBuffer Interface 0x%p Destroyed", this );

  vibe_os_release_queue_event(m_flushAllWaitQueue);

  vibe_os_free_memory(m_LatencyParams);
  m_LatencyParams = 0;
  vibe_os_free_memory(m_LatencyPlaneIds);
  m_LatencyPlaneIds = 0;
}

// This function is called by functions where m_pDisplayDevice->VSyncLock() is already taken
void CPureSwQueueBufferInterface::UpdateOutputInfo(CDisplayPlane *pPlane)
{

    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    if( pPlane != 0 )
    {
        m_ulTimingID = pPlane->GetTimingID();
        m_outputInfo = pPlane->GetOutputInfo();
        TRC( TRC_ID_MAIN_INFO, "Source %d: New TimingID=%u", m_interfaceID, m_ulTimingID);
    }
}

bool CPureSwQueueBufferInterface::Create(void)
{
  uint32_t max_num_planes = 0;
  char     queue_name[QUEUE_NAME_MAX_LENGTH];

  TRCIN( TRC_ID_MAIN_INFO, "" );

  if (!CQueueBufferInterface::Create())
    return false;

  // Create display queue
  vibe_os_snprintf (queue_name, sizeof(queue_name), "src%d_displayQ", m_interfaceID);
  if (!m_displayQueue.CreateQueue(queue_name))
  {
    TRC(TRC_ID_ERROR, "Cannot create %s", queue_name);
  }

  max_num_planes = GetParentSource()->GetMaxNumConnectedPlanes();

  m_LatencyParams = (stm_display_latency_params_t*)vibe_os_allocate_memory(max_num_planes * sizeof(stm_display_latency_params_t));
  if(!m_LatencyParams)
  {
    TRC( TRC_ID_ERROR, "failed to allocate m_LatencyParams" );
    return false;
  }
  vibe_os_zero_memory(m_LatencyParams, max_num_planes * sizeof(stm_display_latency_params_t));

  m_LatencyPlaneIds = (uint32_t*)vibe_os_allocate_memory(max_num_planes * sizeof(uint32_t));
  if(!m_LatencyPlaneIds)
  {
    TRC( TRC_ID_ERROR, "failed to allocate m_LatencyPlaneIds" );
    return false;
  }
  vibe_os_zero_memory(m_LatencyPlaneIds, max_num_planes * sizeof(uint32_t));

  // Create wait queue (one per source for delaying flush all until no more picture displayed)
  if(vibe_os_allocate_queue_event(&m_flushAllWaitQueue) != 0)
  {
    TRC( TRC_ID_ERROR, "failed to allocate m_flushAllWaitQueue" );
    return false;
  }

  TRCOUT( TRC_ID_MAIN_INFO, "" );

  return true;
}

QueueBufferInterfaceResults CPureSwQueueBufferInterface::QueueBuffer(const stm_display_buffer_t* const pBuffer,
                                                                     const void                * const user)
{
    CDisplayNode               *pDisplayNode = NULL;
    QueueBufferInterfaceResults result = QBI_OK;
    bool                        status;

    /* null pointer which shouldn't happen */
    if(!pBuffer)
    {
        TRC( TRC_ID_ERROR, "Invalid pBuffer!" );
        return QBI_INVALID_QUEUED_BUFFER;
    }

    if(!m_user)
    {
        /* It is now mandatory to lock a source to reserve it for our own use otherwise there is a risk
           that someones else tries to use the same source */
        TRC( TRC_ID_ERROR, "Source %d used without being locked by user 0x%p!", m_interfaceID, user);
        return QBI_NOT_LOCKED;
    }

    if(m_user != user)
    {
        /* This buffer queueing action is asked by a user that haven't lock this source */
        TRC( TRC_ID_ERROR, "Source %d: this source is locked by 0x%p, so discard request of user 0x%p!", m_interfaceID, m_user, user);
        return QBI_NOT_LOCKED_BY_ASKER;
    }

    m_pDisplayDevice->VSyncLock();
    // If a flush is on-going, all queue buffer request are refused.
    // Completed_callback of this buffer will be never called.
    if (m_flushStatus != NO_FLUSH)
    {
        m_pDisplayDevice->VSyncUnlock();
        TRC( TRC_ID_ERROR, "Source %d: flush on-going, so discard request of user 0x%p about buffer presentation_time=%lld user_data=0x%p!",
            m_interfaceID, m_user, pBuffer->info.presentation_time, pBuffer->info.puser_data);
        return QBI_FLUSH_ON_GOING;
    }
    m_pDisplayDevice->VSyncUnlock();

    pDisplayNode  = new CDisplayNode;
    if(!pDisplayNode)
    {
        TRC( TRC_ID_ERROR, "Source %d: Failed to allocate a CDisplayNode!", m_interfaceID );
        return QBI_NO_MEMORY;
    }

    // Fill the node
    pDisplayNode->m_bufferDesc = *pBuffer;
    pDisplayNode->m_pictureId  = ++m_IncomingPictureCounter;
    if(pDisplayNode->m_bufferDesc.info.nfields == 0)
    {
        // In case of interlaced streams, when the Streaming Engine is running Frame Rate Conversion,
        // some pictures are pushed with nfield == 0.
        // The node can be used as reference picture by DEI but must not be displayed.
        pDisplayNode->m_doNotDisplay = true;
    }
    FillNodeInfo(pDisplayNode);

    // And queue it
    m_pDisplayDevice->VSyncLock();
    status = m_displayQueue.QueueDisplayNode(pDisplayNode);
    if(status)
    {
        // The picture has been successfully queued
        m_Statistics.PicQueued++;
        m_Statistics.PicOwned++;
        TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: %d%c nfields=%d 0x%p (Owned:%d) presentation_time=%lld user_data=0x%p",
            m_interfaceID,
            pDisplayNode->m_pictureId,
            pDisplayNode->m_srcPictureTypeChar,
            pDisplayNode->m_bufferDesc.info.nfields,
            pDisplayNode,
            m_Statistics.PicOwned,
            pDisplayNode->m_bufferDesc.info.presentation_time,
            pDisplayNode->m_bufferDesc.info.puser_data);
    }
    m_pDisplayDevice->VSyncUnlock();
    if(!status)
    {
        result = QBI_QUEUEING_BUFFER_FAILED;
        goto error_queue_buffer;
    }

    // With Interlaced sources, it is usual that only the first field contains time stamps.
    // The other one has a null "presentation_time" so it should be displayed ASAP
    if(pDisplayNode->m_bufferDesc.info.presentation_time != 0)
    {
        if(m_lastPresentationTime != 0)
        {
            TRC( TRC_ID_PICT_QUEUE_RELEASE, "Delta with last time-stamped picture : %lld us",
                 pDisplayNode->m_bufferDesc.info.presentation_time - m_lastPresentationTime);
        }
        m_lastPresentationTime = pDisplayNode->m_bufferDesc.info.presentation_time;
    }

    if(IS_TRC_ID_ENABLED(TRC_ID_BUFFER_DESCRIPTOR))
    {
        PrintBufferDescriptor(pDisplayNode);
    }

    return result;

error_queue_buffer:
    TRC( TRC_ID_ERROR, "failed!" );

    if (pDisplayNode)
    {
        delete pDisplayNode;
    }

    return result;
}

// !!! m_vsyncLock should be taken when this function is called !!!
void CPureSwQueueBufferInterface::FlushAllNodesExceptUsed(void)
{
    CDisplayNode   *pNode;
    int             nodeReleasedNb = 0;

    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    // Start from the beginning of the queue and release all the nodes except used by hardware
    // or already programmed for next VSync
    pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();

    while(pNode)
    {
        CDisplayNode *pReleasedNode = pNode;

        // Get the next node BEFORE releasing the current node (otherwise we lose the reference)
        pNode = (CDisplayNode*)pNode->GetNextNode();

        // Nodes already programmed or used by hardware should not be deleted now.
        // They will be deleted normally at next VSync (for m_picturesUsedByHw) or
        // next-next VSync (for m_picturesPreparedForNextVSync).
        if((m_picturesUsedByHw.pPrevNode == pReleasedNode))
        {
            continue;
        }

        if((m_picturesUsedByHw.pCurNode == pReleasedNode))
        {
            continue;
        }

        if((m_picturesUsedByHw.pNextNode == pReleasedNode))
        {
            continue;
        }

        if((m_picturesPreparedForNextVSync.pPrevNode == pReleasedNode))
        {
            continue;
        }

        if((m_picturesPreparedForNextVSync.pCurNode == pReleasedNode))
        {
            continue;
        }

        if((m_picturesPreparedForNextVSync.pNextNode == pReleasedNode))
        {
            continue;
        }

        // Release the node
        ReleaseDisplayNode(pReleasedNode, 0);
        nodeReleasedNb++;
    }

    TRC( TRC_ID_MAIN_INFO, "Source %d: %d nodes released", m_interfaceID, nodeReleasedNb );

    if(IS_TRC_ID_ENABLED(TRC_ID_PRINT_DISPLAY_QUEUE))
    {
        PrintDisplayQueue();
    }
}

CDisplayNode* CPureSwQueueBufferInterface::GetLocalBufferAllocactedInQueue(void)
{
    CDisplayNode *pNode;

    pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();
    while (pNode)
    {
        if (pNode->m_localBufferAllocated)
        {
            return pNode;
        }
        pNode = (CDisplayNode*)pNode->GetNextNode();
    }
    return 0;
}


QueueBufferInterfaceResults CPureSwQueueBufferInterface::FlushStart(flush_requested_t  &requestedFlush,
                                                                    const void * const user)
{
    m_flushStartTime = vibe_os_get_system_time();
    TRC(TRC_ID_FLUSH_TIMING, "Source %d FlushStart=%lld", m_interfaceID, m_flushStartTime );

    TRCIN( TRC_ID_MAIN_INFO, "Source %d: requestedFlush=%d user=0x%p", m_interfaceID, requestedFlush, user );

    if(!m_user)
    {
        /* It is now mandatory to lock a source to reserve it for our own use otherwise there is a risk
           that someones else tries to use the same source */
        TRC( TRC_ID_ERROR, "Source %d used without being locked by user 0x%p!", m_interfaceID, user);
        return QBI_NOT_LOCKED;
    }

    if(m_user != user)
    {
        /* This buffer queueing action is asked by a user that haven't lock this source */
        TRC( TRC_ID_ERROR, "Source %d: this source is locked by 0x%p, so discard request of user 0x%p!", m_interfaceID, m_user, user);
        return QBI_NOT_LOCKED_BY_ASKER;
    }

    ///////////////////////////////////////    Critical section      ///////////////////////////////////////////
    m_pDisplayDevice->VSyncLock();

    // If a flush is on-going, all new flush request are refused.
    if (m_flushStatus != NO_FLUSH)
    {
        m_pDisplayDevice->VSyncUnlock();
        TRC( TRC_ID_ERROR, "Source %d: user 0x%p request discarded due to flush on-going!", m_interfaceID, m_user);
        return QBI_FLUSH_ON_GOING;
    }

    // Start processing of flush requested
    switch (requestedFlush)
    {
        case FLUSH_ALL:
        {
            // Flush All Requested = flush all buffers from the display queue
            m_flushStatus      = FLUSH_ALL;

            // Flush all nodes that are not used, used means currently processed by hardware or programmed for next VSync
            FlushAllNodesExceptUsed();

            m_lastPresentationTime   = 0;
            m_IncomingPictureCounter = 0;
            break;
        }
        case FLUSH_PARTIAL:
        {
            // Partial Flush Requested = flush all buffers from the display queue except currently used.
            // No wait, so in case of interlaced stream, system could switch to next field at next VSync.

            // Flush all nodes that are not used, used means currently processed by hardware or programmed for next VSync
            FlushAllNodesExceptUsed();

            // All nodes have been released, except nodes that are currently used.
            // In case of interlaced streams, next node is likely still there for DEI purpose.
            // To ensure ensure that we don't move to next node at next VSync, mark it as 'do not display'.
            if(m_picturesPreparedForNextVSync.pNextNode)
            {
                m_picturesPreparedForNextVSync.pNextNode->m_doNotDisplay = true;
            }

            break;
        }
        case FLUSH_PARTIAL_WITH_COPY:
        {
            CDisplayNode *pNode;

            // Flush all nodes that are not used, used means currently processed by hardware or programmed for next VSync
            FlushAllNodesExceptUsed();

            // It is to know if a copied buffer was already in the queue
            pNode = GetLocalBufferAllocactedInQueue();

            if (pNode)
            {
                if (pNode == m_picturesPreparedForNextVSync.pCurNode)
                {
                    // Nothing to do, the node for next VSync is already copied in the local buffer
                    m_pDisplayDevice->VSyncUnlock();
                    TRC( TRC_ID_MAIN_INFO, "Avoid reallocation, copied buffer is currently programmed for next VSync");
                    return QBI_FLUSH_NOTHING_TO_DO;
                }
                else
                {
                    // Will force a flush all for preventing allocation of another local buffer
                    requestedFlush      = FLUSH_ALL;
                    m_flushStatus       = FLUSH_ALL;

                    TRC( TRC_ID_MAIN_INFO, "Avoid reallocation, copied buffer is in queue but no more programmed for next VSync");
                }
            }
            else
            {
              // Partial Flush With Copy Requested = flush all buffers from the display queue except currently programmed
              // This buffer will be copied before being released
              m_flushStatus       = FLUSH_PARTIAL_WITH_COPY;
              m_nodeCopied        = 0;

              // Save the node to copy, can be null if flush arrives before first buffer queued or first buffer displayed
              m_nodeToCopy = m_picturesPreparedForNextVSync.pCurNode;
              if (m_nodeToCopy)
              {
                  // Update the node to copy for not delaying copied node to be display as soon as available
                  m_nodeToCopy->m_bufferDesc.info.nfields = 0;
                  TRC( TRC_ID_MAIN_INFO, "Source %d Picture to copy %u%c - nfield=%d pres=%lld PTS=%lld",
                                      m_interfaceID, m_nodeToCopy->m_pictureId, m_nodeToCopy->m_srcPictureTypeChar,
                                      m_nodeToCopy->m_bufferDesc.info.nfields, m_nodeToCopy->m_bufferDesc.info.presentation_time,
                                      m_nodeToCopy->m_bufferDesc.info.PTS);

                  // Tag all node(s) remaining in the display queue for never displaying them, except the
                  // one to copy. In case of interlaced stream, it prevents from switching to next field at next VSync.
                  pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();
                  while (pNode)
                  {
                      if (pNode != m_nodeToCopy)
                      {
                          pNode->m_doNotDisplay = true;
                      }
                      pNode = (CDisplayNode*)pNode->GetNextNode();
                  }
              }
              else
              {
                  TRC( TRC_ID_MAIN_INFO, "Source %d Picture to copy is NULL, flush will be converted in flush All", m_interfaceID);
              }
            }

            break;
        }
        default:
            m_pDisplayDevice->VSyncUnlock();
            TRC( TRC_ID_ERROR, "Source %d: user 0x%p ask unsupported requestedFlush=%d, so discard it!", m_interfaceID, m_user, requestedFlush);
            return QBI_INVALID_FLUSH;
    }

    m_pDisplayDevice->VSyncUnlock();
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    TRCOUT( TRC_ID_MAIN_INFO, "Source %d:", m_interfaceID );

    return QBI_OK;
}

/* Evaluate if the conditions are met to complete the Flush */
bool CPureSwQueueBufferInterface::EvaluateFlushCondition(const void * const user)
{
    bool          flush_done = false;

    ///////////////////////////////////////    Critical section      ///////////////////////////////////////////
    m_pDisplayDevice->VSyncLock();

    // If a flush all was requested, check that display_queue is empty to exit from waiting
    switch (m_flushStatus)
    {
        case FLUSH_ALL:
        {
            // Wait until no more picture get used
            if ((m_picturesUsedByHw.pPrevNode == 0)             &&
                (m_picturesUsedByHw.pCurNode  == 0)             &&
                (m_picturesUsedByHw.pNextNode == 0)             &&
                (m_picturesPreparedForNextVSync.pPrevNode == 0) &&
                (m_picturesPreparedForNextVSync.pCurNode  == 0) &&
                (m_picturesPreparedForNextVSync.pNextNode == 0))
            {
                TRC(TRC_ID_MAIN_INFO, "Source %d: No more src pictures used. Flush can be completed", m_interfaceID);
                flush_done = true;
            }
            else
            {
                // Some source pictures are still used so the wait should continue
                TRC(TRC_ID_MAIN_INFO, "Source %d: Some source pictures are still used", m_interfaceID);
            }
            break;
        }

        case FLUSH_PARTIAL_WITH_COPY:
        {
            if(m_outputInfo.isOutputStarted)
            {
                if (m_nodeCopied)
                {
                    if ((m_picturesUsedByHw.pPrevNode == 0)                        &&
                        (m_picturesUsedByHw.pCurNode  == m_nodeCopied)             &&
                        (m_picturesUsedByHw.pNextNode == 0)                        &&
                        (m_picturesPreparedForNextVSync.pPrevNode == 0)            &&
                        (m_picturesPreparedForNextVSync.pCurNode  == m_nodeCopied) &&
                        (m_picturesPreparedForNextVSync.pNextNode == 0) )
                    {
                        // The copied picture is now displayed so waiting is finished.
                        TRC(TRC_ID_MAIN_INFO, "Source %d: Wakeup because picture copied is displayed", m_interfaceID);
                        flush_done    = true;
                    }
                    else
                    {
                        TRC(TRC_ID_MAIN_INFO, "Source %d: No Wakeup because picture copied is not yet displayed", m_interfaceID);
                    }
                }
                else
                {
                    // m_nodeCopied is not yet available (copy on-going)
                }
            }
            else
            {
                // The output is not (or no more) started so the FLUSH_PARTIAL_WITH_COPY cannot be completed
                // Return true to stop waiting
                // m_flushStatus will be set to "NO_FLUSH" by WaitFlushCompletion()
                TRC(TRC_ID_MAIN_INFO, "Source %d: FLUSH_PARTIAL_WITH_COPY cancelled because output stopped", m_interfaceID);
                flush_done    = true;
                // The "copied picture" will be released as usual by ReleaseNodesNoMoreNeeded()
            }
            break;
        }

        default :
        {
            TRC( TRC_ID_ERROR, "Source %d: invalid m_flushStatus=%d, user 0x%p!", m_interfaceID, m_flushStatus, m_user);
            m_flushStatus = NO_FLUSH;
            flush_done    = true;
        }
    }

    m_pDisplayDevice->VSyncUnlock();
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    return flush_done;
}


bool CPureSwQueueBufferInterface::CreateAndEnqueueCopiedPictBuffer(CDisplayNode                  *src_node_to_copy,
                                                                   CopiedPictBuffers             *dst_copied_buffers)
{
    CDisplayNode *pDisplayNode;
    bool          result;

    // Check input parameters
    if (!src_node_to_copy)
    {
      TRC(TRC_ID_ERROR, "No picture buffer to copy");
      return false;
    }

    if (!dst_copied_buffers)
    {
      TRC(TRC_ID_ERROR, "invalid destination address");
      return false;
    }

    // Allocate new node
    pDisplayNode  = new CDisplayNode;
    if(!pDisplayNode)
    {
        TRC( TRC_ID_ERROR, "Source %d: Failed to allocate a CDisplayNode!", m_interfaceID );
        return false;
    }

    // Copy node parameters
    pDisplayNode->m_pictureId             = 0; // specific value to identified copied picture
    pDisplayNode->m_bufferDesc            = src_node_to_copy->m_bufferDesc;
    pDisplayNode->m_srcPictureType        = src_node_to_copy->m_srcPictureType;
    pDisplayNode->m_srcPictureTypeChar    = src_node_to_copy->m_srcPictureTypeChar;
    pDisplayNode->m_doNotDisplay          = src_node_to_copy->m_doNotDisplay;
    pDisplayNode->m_localBufferAllocated  = true;
    pDisplayNode->m_firstFieldOnly        = src_node_to_copy->m_firstFieldOnly;
    pDisplayNode->m_repeatFirstField      = src_node_to_copy->m_repeatFirstField;
    pDisplayNode->m_firstFieldType        = src_node_to_copy->m_firstFieldType;

    // Update some parameters
    pDisplayNode->m_bufferDesc.src.flags |= STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY;
    pDisplayNode->m_bufferDesc.src.primary_picture.video_buffer_addr   = (uint32_t)dst_copied_buffers->primary_phys_addr;
    pDisplayNode->m_bufferDesc.src.secondary_picture.video_buffer_addr = (uint32_t)dst_copied_buffers->secondary_phys_addr;
    pDisplayNode->m_bufferDesc.info.nfields            = 1;
    pDisplayNode->m_bufferDesc.info.puser_data         = dst_copied_buffers; // create a structure with all info needed that will be delete by callback
    pDisplayNode->m_bufferDesc.info.display_callback   = 0;
    pDisplayNode->m_bufferDesc.info.completed_callback = ReleaseCopiedPictBuffersCallback; // specific function to set for releasing buffer

    m_pDisplayDevice->VSyncLock();

    result = m_displayQueue.QueueDisplayNode(pDisplayNode);
    if (result)
    {
        // The picture has been successfully queued
        m_nodeCopied = pDisplayNode;
        m_Statistics.PicQueued++;
        m_Statistics.PicOwned++;
        TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: %d%c nfields=%d 0x%p (Owned:%d) presentation_time=%lld user_data=0x%p",
            m_interfaceID,
            pDisplayNode->m_pictureId,
            pDisplayNode->m_srcPictureTypeChar,
            pDisplayNode->m_bufferDesc.info.nfields,
            pDisplayNode,
            m_Statistics.PicOwned,
            pDisplayNode->m_bufferDesc.info.presentation_time,
            pDisplayNode->m_bufferDesc.info.puser_data);
    }
    else
    {
        m_pDisplayDevice->VSyncUnlock();

        TRC( TRC_ID_ERROR, "Source %d: Failed to queue the copied node!", m_interfaceID );
        delete pDisplayNode;

        return false;
    }

    m_pDisplayDevice->VSyncUnlock();

    return true;
}

bool CPureSwQueueBufferInterface::FlushPartialWithCopy(void)
{
    CopiedPictBuffers *copied_pict_buffers = 0;

    if(!m_nodeToCopy)
    {
        TRC( TRC_ID_MAIN_INFO, "No picture buffer to copy" );
        return false;
    }

    if(!CBufferCopy::GetCopiedPictBuffers(m_nodeToCopy->m_bufferDesc.src.primary_picture.video_buffer_size,
                                          m_nodeToCopy->m_bufferDesc.src.secondary_picture.video_buffer_size,
                                         &copied_pict_buffers))
    {
        return false;
    }

    if (!CBufferCopy::PerformCopiedPictBuffers(m_nodeToCopy,
                                  copied_pict_buffers))
    {
        goto copy_error;
    }

    if (!CreateAndEnqueueCopiedPictBuffer(m_nodeToCopy,
                                          copied_pict_buffers))
    {
        goto copy_error;
    }
    return true;

copy_error:
    TRC(TRC_ID_ERROR, "Failed to prepare the CopiedPictBuffer!");
    if (copied_pict_buffers)
    {
        CBufferCopy::ReleaseCopiedPictBuffers(copied_pict_buffers);
    }
    return false;
}

QueueBufferInterfaceResults CPureSwQueueBufferInterface::WaitFlushCompletion(flush_requested_t  requestedFlush,
                                                                             const void * const user)
{
    QueueBufferInterfaceResults result;

    switch (requestedFlush)
    {
        case FLUSH_ALL:
            // This function will wait until no more picture is in use by HW
            result = WaitFlushCondition(user);
            if (result == QBI_OK)
            {
                // The Flush_all can now be completed. Every picture will be released
                result = CompleteFlushAll();
            }
            break;

        case FLUSH_PARTIAL_WITH_COPY:
            // FlushPartialWithCopy() will perform the following:
            // - allocate a secure frame buffer and register it to secure driver.
            // - copy the content of the picture "prepared for next Vsync" (the primary and secondary pictures are copied)
            //   into the newly allocated frame buffer. The copy is done by the Blitter.
            // - create and enqueue a new specific node referencing the pictures copied.
            if (FlushPartialWithCopy() == false)
            {
                // Failed to do a copy of the "prepared for next Vsync". Backup plan: Do a FLUSH_ALL instead
                m_pDisplayDevice->VSyncLock();
                // convert it into a flush all
                m_flushStatus = FLUSH_ALL;
                m_pDisplayDevice->VSyncUnlock();

                // This function will wait until no more picture is in used by HW
                result = WaitFlushCondition(user);
                if (result == QBI_OK)
                {
                    // The Flush_all can now be completed. Every picture will be released
                    result = CompleteFlushAll();
                }
            }
            else
            {
                // Copy was successful. Wait until the copied picture gets displayed
                result = WaitFlushCondition(user);

                // The copied picture is now displayed. When the display of this picture will be over,
                // this secure frame buffer will be released and deallocated by the usual process.
                m_pDisplayDevice->VSyncLock();
                m_flushStatus = NO_FLUSH;
                m_pDisplayDevice->VSyncUnlock();
            }
            break;

        case FLUSH_PARTIAL:
            // This flush can be done immediately so no need to wait
            result = QBI_OK;
            break;

        default:
            TRC( TRC_ID_ERROR, "Invalid flush mode! (%d)", requestedFlush);
            m_pDisplayDevice->VSyncLock();
            m_flushStatus = NO_FLUSH;
            m_pDisplayDevice->VSyncUnlock();
            result = QBI_INVALID_FLUSH;
            break;
    }

    TRC(TRC_ID_FLUSH_TIMING, "Source %d FlushDuration=%lld", m_interfaceID, (vibe_os_get_system_time() - m_flushStartTime) );

    return result;
}


QueueBufferInterfaceResults CPureSwQueueBufferInterface::WaitFlushCondition(const void * const user)
{
    struct private_wait_flush_param_s flush_param = {this, user};

    TRC( TRC_ID_MAIN_INFO, "Source %d: Start waiting for flush condition (TimingID=%u)", m_interfaceID, m_ulTimingID);

    // NB: we don't wait for the same thing depending if we are doing a FLUSH_ALL or a FLUSH_PARTIAL_WITH_COPY

    // Wait that evaluate_flush_condition becomes true or a breaking signal
    if (vibe_os_wait_event(m_flushAllWaitQueue,
                           evaluate_flush_condition,
                           &flush_param,
                           STMIOS_WAIT_TIME_FOREVER) == -ERESTARTSYS)
    {
        // Was interrupted by a signal.
        // As a signal is pending, trying to take VSyncLock will return an error, so don't do it.
        // But it is mandatory to reset m_flushStatus for accepting new flush or queuebuffer request.
        m_flushStatus = NO_FLUSH;
        TRC( TRC_ID_ERROR, "Source %d: Flush aborted due to a signal!", m_interfaceID);
        return QBI_RESTARTSYS;
    }

    return QBI_OK;
}

QueueBufferInterfaceResults CPureSwQueueBufferInterface::CompleteFlushAll(void)
{
    CDisplayNode *pNode;
    CDisplayNode *pNodeToRelease;
    uint32_t      count = 0;

    // The VSyncLock should be taken when doing operations on the Display Queue
    m_pDisplayDevice->VSyncLock();

    pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();
    while (pNode != 0)
    {
        pNodeToRelease = pNode;
        TRC(TRC_ID_PICT_QUEUE_RELEASE, "Source %d: FLUSH_ALL release node %d%c (0x%p)",
            m_interfaceID,
            pNodeToRelease->m_pictureId,
            pNodeToRelease->m_srcPictureTypeChar,
            pNodeToRelease );

        pNode = (CDisplayNode*) pNode->GetNextNode();
        ReleaseDisplayNode(pNodeToRelease, 0);
        count++;
    }

    // Reset m_flushStatus for accepting new actions on this source
    m_flushStatus = NO_FLUSH;

    m_pDisplayDevice->VSyncUnlock();

    TRC( TRC_ID_MAIN_INFO, "Source %d: FLUSH_ALL done. Released %d nodes", m_interfaceID, count);

    if(IS_TRC_ID_ENABLED(TRC_ID_PRINT_DISPLAY_QUEUE))
    {
        PrintDisplayQueue();
    }

    return QBI_OK;
}


bool CPureSwQueueBufferInterface::IsEmpty()
{
    if(m_displayQueue.GetFirstNode())
    {
        TRC( TRC_ID_MAIN_INFO, "false");
        return false;
    }
    else
    {
        TRC( TRC_ID_MAIN_INFO, "true");
        return true;
    }
}



int CPureSwQueueBufferInterface::GetBufferFormats(const stm_pixel_format_t** formats) const
{
  int             format   = 0;
  uint32_t        num_planes_Ids = 0;
  uint32_t        PlanesID[/*CINTERFACE_MAX_PLANES_PER_SOURCE*/1];
  CDisplaySource       * pDS  = GetParentSource();
  const CDisplayDevice * pDev = pDS->GetParentDevice();

  // Get the connected planes ids
  num_planes_Ids = pDS->GetConnectedPlaneID(PlanesID, N_ELEMENTS(PlanesID));

  // Call Plane's QueueBuffer method
  for(uint32_t i=0; (i<num_planes_Ids) ; i++)
  {
    CDisplayPlane* pDP = pDev->GetPlane(PlanesID[i]);
    if (pDP)
    {
      TRC( TRC_ID_MAIN_INFO, "GetBufferFormats forwarded to Plane %s", pDP->GetName() );
      format = pDP->GetFormats(formats);
    }
  }

  return format;
}

bool CPureSwQueueBufferInterface::IsConnectionPossible(CDisplayPlane *pPlane) const
{
  bool isAllowed = false;

  if (GetParentSource()->GetNumConnectedPlanes() == 0)
  {
    // This source is connected to no plane, so this plane can be connected.
      isAllowed = true;
    }
  else
  {
    // This source is already connected to another plane, so only same TimingID planes are allowed.
    // Warning: The sourceTimingId can be null if his plane is not yet connected to an output
    //          The newPlaneTimingId can be null if it is not yet connected to an ouput
    // So we prevent both cases, else additionnal check should be done in stm_display_plane_connect_to_output()
    uint32_t sourceTimingId = GetParentSource()->GetTimingID();
    uint32_t newPlaneTimingId  = pPlane->GetTimingID();
    if ( (sourceTimingId != 0) &&
         (sourceTimingId == newPlaneTimingId)
       )
    {
      isAllowed = true;
    }
  }

  // Last check for video plane, this is perhaps useless all video planes can be connected to source
  if( isAllowed )
  {
    // Check if video plane can be connect to the source
    if (pPlane->isVideoPlane())
    {
      isAllowed = m_isConnectionToVideoPlaneAllowed;
    }
  }

  return isAllowed;
}

QueueBufferInterfaceResults CPureSwQueueBufferInterface::LockUse(void *user)
{
    QueueBufferInterfaceResults result = QBI_ALREADY_LOCKED;

    if(m_user == user)
    {
        // Allow multiple calls to lock with the same user handle
        result = QBI_OK;
    }
    else
    {
        if(!m_user)
        {
            TRC(TRC_ID_MAIN_INFO, "Source %d: A new user 0x%p is locking", m_interfaceID, user );
            m_user = user;

            // Update the status of the source
            if(m_pDisplaySource)
            {
                m_pDisplaySource->SetStatus(STM_STATUS_QUEUE_LOCKED, true);
            }

            result = QBI_OK;
        }
    }

    return result;
}

QueueBufferInterfaceResults CPureSwQueueBufferInterface::Unlock(void *user)
{
    CDisplayNode               *pNode  = 0;
    QueueBufferInterfaceResults result = QBI_OK;

    m_pDisplayDevice->VSyncLock();

    if (m_user == user)
    {
        if (m_flushStatus == NO_FLUSH)
        {
            pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();
            if (pNode == 0)
            {
                // This source can be unlocked, no flush on-going and display queue is empty
                m_user = 0;

                // Update the status of the source
                if(m_pDisplaySource)
                {
                    m_pDisplaySource->SetStatus(STM_STATUS_QUEUE_LOCKED, false);
                }

                TRC(TRC_ID_MAIN_INFO, "Source %d: Unlocked by user = 0x%p", m_interfaceID, user);
            }
            else
            {
                TRC(TRC_ID_ERROR, "Source %d: User 0x%p cannot unlock this source, remains nodes", m_interfaceID, user);
                result = QBI_NODES_REMAINING;
            }
        }
        else
        {
            TRC(TRC_ID_ERROR, "Source %d: User 0x%p cannot unlock this source, flush on-going", m_interfaceID, user);
            result = QBI_FLUSH_ON_GOING;
        }
    }
    else
    {
        TRC(TRC_ID_ERROR, "Source %d: User 0x%p cannot unlock this source, is locked by user 0x%p", m_interfaceID, user, m_user);
        result = QBI_NOT_LOCKED_BY_ASKER;
    }

    if(result == QBI_OK)
    {
        TRC(TRC_ID_MAIN_INFO, "Reset source listener");
        SetListener(0, 0);
    }

    m_pDisplayDevice->VSyncUnlock();

    return result;
}

QueueBufferInterfaceResults CPureSwQueueBufferInterface::Release(void *user)
{
    if ((m_user) && (m_user == user) )
    {
        // This source should be unlocked before releasing the handle.
        TRC(TRC_ID_ERROR, "Source %d: User 0x%p cannot released its locked source, unlock it before", m_interfaceID, user);
        return QBI_STILL_LOCKED;
    }

    return QBI_OK;
}


/*
 * Generic handling of OutputVSync in threaded IRQ.
 * This may be overloaded in CDisplayPlane daughter classes
 * depending if OutputVsync should be handled in IRQ.
 */
void CPureSwQueueBufferInterface::OutputVSyncThreadedIrqUpdateHW(bool isDisplayInterlaced, bool isTopFieldOnDisplay, const stm_time64_t &vsyncTime)
{
    bool                       isPictureRepeated;

    if(!m_displayQueue.GetFirstNode())
    {
        // The Queue is empty: Nothing to do
        return;
    }

    TRC(TRC_ID_VSYNC, "VSync %c on Source %d at %llu",
            isDisplayInterlaced ? (isTopFieldOnDisplay ? 'T' : 'B') : ' ',
            m_interfaceID,
            vsyncTime);

    // A new VSync has happened: The pictures prepared for next VSync become the pictures used by the HW
    m_picturesUsedByHw = m_picturesPreparedForNextVSync;

    if(IS_TRC_ID_ENABLED(TRC_ID_PRINT_DISPLAY_QUEUE))
    {
        PrintDisplayQueue();
    }

    // A new triplet is going to be chosen for next VSync
    ResetTriplet(&m_picturesPreparedForNextVSync);

    if (m_picturesUsedByHw.pCurNode != 0)
    {
        SendDisplayCallback(m_picturesUsedByHw.pCurNode, vsyncTime);
    }

    // Now find the picture(s) to use at next VSync
    m_picturesPreparedForNextVSync.pCurNode = SelectPictureForNextVSync(isDisplayInterlaced, isTopFieldOnDisplay, vsyncTime);

    // Only if at least one deinterlacer plane is connected to this source
    if(GetParentSource()->hasADeinterlacer()
       &&
       m_picturesPreparedForNextVSync.pCurNode
       &&
      (m_picturesPreparedForNextVSync.pCurNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED)
       &&
      (!(m_picturesPreparedForNextVSync.pCurNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY)))
    {
        // Look for the previous and next nodes
        m_picturesPreparedForNextVSync.pPrevNode = GetPreviousNode(m_picturesPreparedForNextVSync.pCurNode);
        m_picturesPreparedForNextVSync.pNextNode = (CDisplayNode*)m_picturesPreparedForNextVSync.pCurNode->GetNextNode();
    }

    if(m_outputInfo.isOutputStarted)
    {
        ReleaseNodesNoMoreNeeded(vsyncTime);
    }
    else
    {
        // When output is stopped, no node is in use so we only release the nodes with a presentationTime that is over
        ReleaseOutdatedNodes(vsyncTime);
    }

    if (m_picturesPreparedForNextVSync.pCurNode)
    {
        if(m_bIsFrozen)
        {
            // When display is (or is going to) low power state, we should stop all plane activity
            // At generic CDisplayPlane level we continue to work normally but the picture is not presented to the HW plane
            ResetTriplet(&m_picturesPreparedForNextVSync);
        }
        else
        {
            isPictureRepeated = (m_picturesPreparedForNextVSync.pCurNode == m_picturesUsedByHw.pCurNode);

            PresentDisplayNode( m_picturesPreparedForNextVSync.pPrevNode,   // Previous node (Optional)
                                m_picturesPreparedForNextVSync.pCurNode,    // Current node
                                m_picturesPreparedForNextVSync.pNextNode,   // Next node (Optional)
                                isPictureRepeated,
                                isDisplayInterlaced,
                                isTopFieldOnDisplay,
                                vsyncTime);

            if(m_picturesPreparedForNextVSync.pCurNode->m_bufferDesc.info.nfields > 0)
                m_picturesPreparedForNextVSync.pCurNode->m_bufferDesc.info.nfields--;
        }
    }
    else
    {
        // No valid candidate was found
        ResetTriplet(&m_picturesPreparedForNextVSync);
    }

    if( (m_flushStatus == FLUSH_ALL) ||
        (m_flushStatus == FLUSH_PARTIAL_WITH_COPY) )
    {
        // A Flush is pending: Wake up "WaitQueue" to re-evaluate if the flush can be completed
        vibe_os_wake_up(m_flushAllWaitQueue);
    }

    return;
}


uint32_t CPureSwQueueBufferInterface::FillParamLatencyInfo(uint16_t *output_change_status)
{
    const CDisplayDevice *pDD               = 0;
    CDisplaySource *pDS                     = 0;
    CDisplayPlane  *pDP                     = 0;
    int             num_planes              = 0;
    int             index                   = 0;
    uint32_t        output_id               = STM_INVALID_OUTPUT_ID;


    // Fill m_LatencyParams information
    pDS = GetParentSource();
    if( pDS != 0 )
    {
        pDD = pDS->GetParentDevice();
        if ( pDD != 0)
        {
            num_planes = pDS->GetConnectedPlaneID(m_LatencyPlaneIds, pDS->GetMaxNumConnectedPlanes() );
            for ( index = 0; index < num_planes; index++)
            {
                pDP = pDD->GetPlane(m_LatencyPlaneIds[index]);
                if( pDP != 0 )
                {
                    if ( pDP->GetConnectedOutputID(&output_id, 1) == 0)
                    {
                        output_id = STM_INVALID_OUTPUT_ID;
                    }

                    m_LatencyParams[index].output_id            = output_id;
                    m_LatencyParams[index].plane_id             = m_LatencyPlaneIds[index];
                    m_LatencyParams[index].output_latency_in_us = 0;
                    m_LatencyParams[index].output_change        = m_isOutputModeChanged;

                    TRC( TRC_ID_DISPLAY_CB_LATENCY, "Source %d: m_LatencyParams[%d] output_id=%u  plane_id=%u latency=%u output_change=%s",
                                                    m_interfaceID,
                                                    index,
                                                    m_LatencyParams[index].output_id,
                                                    m_LatencyParams[index].plane_id,
                                                    m_LatencyParams[index].output_latency_in_us,
                                                    m_LatencyParams[index].output_change?"true":"false");
                }
            }
        }
    }

    // Special case of num_planes equal to 0
    if ( num_planes == 0 )
    {
        m_LatencyParams[0].output_id            = STM_INVALID_OUTPUT_ID;
        m_LatencyParams[0].plane_id             = STM_INVALID_OUTPUT_ID;
        m_LatencyParams[0].output_latency_in_us = 0;
        m_LatencyParams[0].output_change        = 0;

        TRC( TRC_ID_DISPLAY_CB_LATENCY, "Source %d: m_LatencyParams[%d] output_id=%u  plane_id=%u latency=%u output_change=%s",
                                        m_interfaceID,
                                        0,
                                        m_LatencyParams[0].output_id,
                                        m_LatencyParams[0].plane_id,
                                        m_LatencyParams[0].output_latency_in_us,
                                        m_LatencyParams[0].output_change?"true":"false");
    }

    // Update output_change_status
    if (output_change_status)
    {
        // Reset flags
        *output_change_status = 0;

        // Check if output mode changed
        if(m_isOutputModeChanged)
        {
            *output_change_status |= OUTPUT_DISPLAY_MODE_CHANGE;
            TRC( TRC_ID_MAIN_INFO, "Source %d: OUTPUT_DISPLAY_MODE_CHANGE", m_interfaceID);
            TRC( TRC_ID_DISPLAY_CB_LATENCY, "Source %d: OUTPUT_DISPLAY_MODE_CHANGE", m_interfaceID);
            m_isOutputModeChanged = false;
        }

        // Check if the connections changed
        if (m_areConnectionsChanged)
        {
            *output_change_status |= OUTPUT_CONNECTION_CHANGE;
            TRC( TRC_ID_MAIN_INFO, "Source %d: OUTPUT_CONNECTION_CHANGE", m_interfaceID);
            TRC( TRC_ID_DISPLAY_CB_LATENCY, "Source %d: OUTPUT_CONNECTION_CHANGE", m_interfaceID);
            m_areConnectionsChanged = false;
        }
    }

    return ( num_planes );
}

void CPureSwQueueBufferInterface::SendDisplayCallback(CDisplayNode *pNode, const stm_time64_t &vsyncTime)
{
  uint32_t         nb_element           = 0;
  uint16_t         output_change_status = 0;

  TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: %d%c (display_callback == %p) (buffer displayed flag == %s)",
       m_interfaceID, pNode->m_pictureId, pNode->m_srcPictureTypeChar, pNode->m_bufferDesc.info.display_callback,
       pNode->m_bufferDesc.info.stats.status & STM_STATUS_BUF_DISPLAYED ? "true" : "false");

  // A valid picture (= m_picturesUsedByHw.curNode) is displayed by the HW. Check if it is the first time that we display this picture
  if ( (pNode->m_bufferDesc.info.display_callback) &&
       (!(pNode->m_bufferDesc.info.stats.status & STM_STATUS_BUF_DISPLAYED) ) )
  {
    TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: %d%c display_callback at %llu",  m_interfaceID, pNode->m_pictureId, pNode->m_srcPictureTypeChar, vsyncTime);

    nb_element = FillParamLatencyInfo(&output_change_status);

    pNode->m_bufferDesc.info.stats.status |= STM_STATUS_BUF_DISPLAYED;
    pNode->m_bufferDesc.info.display_callback(pNode->m_bufferDesc.info.puser_data,
                                            vsyncTime,
                                            output_change_status,
                                            nb_element,
                                            m_LatencyParams);
  }
}


// !!! m_vsyncLock should be taken when this function is called !!!
CDisplayNode * CPureSwQueueBufferInterface::SelectPictureForNextVSync(bool                  isDisplayInterlaced,
                                                                      bool                  isTopFieldOnDisplay,
                                                                      const stm_time64_t   &vsyncTime)
{
    CDisplayNode *pNodeCandidateForDisplay = 0;
    bool          isNextVSyncTop;

    // Check that VSyncLock is already taken before accessing to shared variables
    DEBUG_CHECK_VSYNC_LOCK_PROTECTION(m_pDisplayDevice);

    // If the output is stopped, no picture is presented to the planes.
    if(!m_outputInfo.isOutputStarted)
    {
        return pNodeCandidateForDisplay;
    }

    // If flush all is requested, next picture is null to display nothing
    if (m_flushStatus == FLUSH_ALL)
    {
        TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Flush all requested so nothing to display",
                m_interfaceID);
        return pNodeCandidateForDisplay;
    }

    if (m_picturesUsedByHw.pCurNode == 0)
    {
        // No picture is currently displayed: Get the first picture of the queue (if any)
        pNodeCandidateForDisplay = (CDisplayNode*)m_displayQueue.GetFirstNode();
        if (pNodeCandidateForDisplay)
        {
            TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: No picture currently displayed. Try the 1st node of the queue %d%c (0x%p)",
                        m_interfaceID,
                        pNodeCandidateForDisplay->m_pictureId,
                        pNodeCandidateForDisplay->m_srcPictureTypeChar,
                        pNodeCandidateForDisplay );
        }
    }
    else
    {
        // A picture is currently displayed. Check if should continue to display it
        if (m_picturesUsedByHw.pCurNode->m_bufferDesc.info.nfields > 0)
        {
            pNodeCandidateForDisplay = m_picturesUsedByHw.pCurNode;
            TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Keep same node %d%c (0x%p) on display (nfields=%d)",
                        m_interfaceID,
                        m_picturesUsedByHw.pCurNode->m_pictureId,
                        m_picturesUsedByHw.pCurNode->m_srcPictureTypeChar,
                        m_picturesUsedByHw.pCurNode,
                        m_picturesUsedByHw.pCurNode->m_bufferDesc.info.nfields );
        }
        else
        {
            // Get the next picture in the queue
            pNodeCandidateForDisplay = (CDisplayNode*) m_picturesUsedByHw.pCurNode->GetNextNode();
            if (pNodeCandidateForDisplay)
            {
                TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Try next node: %d%c (0x%p)",
                        m_interfaceID,
                        pNodeCandidateForDisplay->m_pictureId,
                        pNodeCandidateForDisplay->m_srcPictureTypeChar,
                        pNodeCandidateForDisplay );
            }
        }
    }

    // If node is tagged as 'do not display' then try to find a node that is to be displayed
    if( pNodeCandidateForDisplay )
    {
        TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Check if node %d%c (0x%p) is skipped by FRC",
                m_interfaceID,
                pNodeCandidateForDisplay->m_pictureId,
                pNodeCandidateForDisplay->m_srcPictureTypeChar,
                pNodeCandidateForDisplay);

        while ((pNodeCandidateForDisplay != 0) && (pNodeCandidateForDisplay->m_doNotDisplay))
        {
            /* It is the tempory solution for SE to test its FRC implementation, it should be removed for Main-VID in future */
            TRC( TRC_ID_PICTURE_SCHEDULING, "Source %d: Skip node %d%c (nfields=%d 0x%p) for FRC",
                    m_interfaceID,
                    pNodeCandidateForDisplay->m_pictureId,
                    pNodeCandidateForDisplay->m_srcPictureTypeChar,
                    pNodeCandidateForDisplay->m_bufferDesc.info.nfields,
                    pNodeCandidateForDisplay);

            pNodeCandidateForDisplay = (CDisplayNode*) pNodeCandidateForDisplay->GetNextNode();
        }
    }

    if (pNodeCandidateForDisplay)
    {
        // The flag STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY has been set by StreamingEngine when the picture has been queued.
        // The output mode may have changed in the meantime so we should check if we are still in I to I use case
        if ( (pNodeCandidateForDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) &&
             (m_outputInfo.isDisplayInterlaced)                                             &&
             (pNodeCandidateForDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY))
        {
            // In case of I->I use case, fields should be presented on the right output polarities (to avoid field inversions)
            TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Picture %d%c (0x%p) selected. Check polarities",
                    m_interfaceID,
                    pNodeCandidateForDisplay->m_pictureId,
                    pNodeCandidateForDisplay->m_srcPictureTypeChar,
                    pNodeCandidateForDisplay);

            // The next VSync will have the opposite polarity of current polarity
            isNextVSyncTop = !isTopFieldOnDisplay;

            if ( ( (pNodeCandidateForDisplay->m_srcPictureType == GNODE_BOTTOM_FIELD) &&  isNextVSyncTop) ||
                 ( (pNodeCandidateForDisplay->m_srcPictureType == GNODE_TOP_FIELD)    && !isNextVSyncTop) )
            {
                TRC(TRC_ID_PICTURE_SCHEDULING, "Repeat current field to avoid a field inversion");
                pNodeCandidateForDisplay = 0;
            }
        }
    }

    if (pNodeCandidateForDisplay)
    {
        // We have found a picture from the active user. Check if it is time to display it
        stm_time64_t presentation_time = pNodeCandidateForDisplay->m_bufferDesc.info.presentation_time;
        stm_time64_t presentation_time_limit;

        if ( (pNodeCandidateForDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) &&
             (m_outputInfo.isDisplayInterlaced)                                             &&
             (pNodeCandidateForDisplay->m_bufferDesc.src.flags & STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY))
        {
            // when polarity needs to be respected for displaying this field
            presentation_time_limit = (vsyncTime + (m_outputInfo.outputVSyncDurationInUs * 2) );
        }
        else
        {
            // when there is no polarity to respect for displaying this field or frame
            presentation_time_limit = (vsyncTime + (m_outputInfo.outputVSyncDurationInUs * 3/2) );
        }

        TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Check if it is time to display %d%c (0x%p)",
              m_interfaceID,
              pNodeCandidateForDisplay->m_pictureId,
              pNodeCandidateForDisplay->m_srcPictureTypeChar,
              pNodeCandidateForDisplay);

        if( (presentation_time != 0LL) &&
            (presentation_time > presentation_time_limit) )
        {
            TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Too early to display, presentation_time=%lld,"
                " vsyncTime=%lld, deltaInMs=%lld, VSyncDurationInUs=%lld, presentation_time_limit=%lld",
                    m_interfaceID,
                    presentation_time,
                    vsyncTime,
                    vibe_os_div64(presentation_time - vsyncTime,1000),
                    m_outputInfo.outputVSyncDurationInUs,
                    presentation_time_limit);
            pNodeCandidateForDisplay = 0;
        }
    }

    // Warning: pNodeCandidateForDisplay needs to be checked again as it might have been set to zero in previous if.
    if (!pNodeCandidateForDisplay)
    {
        // No valid candidate was found. If we have a CurrentNode, continue to display it
        if (m_picturesUsedByHw.pCurNode)
        {
            pNodeCandidateForDisplay = m_picturesUsedByHw.pCurNode;
            TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: No valid candidate found. Keep node %d%c (0x%p) on display",
                    m_interfaceID,
                    pNodeCandidateForDisplay->m_pictureId,
                    pNodeCandidateForDisplay->m_srcPictureTypeChar,
                    pNodeCandidateForDisplay );
        }
        else
        {
            // Nothing to display!
            pNodeCandidateForDisplay = 0;
        }
    }

    if(pNodeCandidateForDisplay)
    {
        TRC(TRC_ID_PICTURE_SCHEDULING, "Source %d: Selected node %d%c (0x%p)",
                m_interfaceID,
                pNodeCandidateForDisplay->m_pictureId,
                pNodeCandidateForDisplay->m_srcPictureTypeChar,
                pNodeCandidateForDisplay );
    }

    return pNodeCandidateForDisplay;
}


void CPureSwQueueBufferInterface::ResetTriplet(display_triplet_t *pTriplet)
{
    pTriplet->pPrevNode = 0;
    pTriplet->pCurNode  = 0;
    pTriplet->pNextNode = 0;
}


/* To search the previous node for de-interlacer:
 * The node should be in front of the current display node in the queue
 */
CDisplayNode* CPureSwQueueBufferInterface::GetPreviousNode(CDisplayNode *pCurNode)
{
    CDisplayNode *pNode = 0;
    CDisplayNode *pPrevNode = 0;

    if(!pCurNode)
        return pPrevNode;

    /* Get the first node in the queue (if any) */
    pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();

    /* Set the previous node used by DEI which should be one node in front of the current node in the queue */
    while (pNode != 0)
    {
        if(pNode == pCurNode)
            break;
        pPrevNode = pNode;
        pNode = (CDisplayNode*) pNode->GetNextNode();
    }

    TRC( TRC_ID_PICTURE_SCHEDULING, "pPrevNode 0x%p", pPrevNode);
    return pPrevNode;
}

void CPureSwQueueBufferInterface::PresentDisplayNode(CDisplayNode *pPrevNode,
                                                     CDisplayNode *pCurrNode,
                                                     CDisplayNode *pNextNode,
                                                     bool isPictureRepeated,
                                                     bool isDisplayInterlaced,
                                                     bool isTopFieldOnDisplay,
                                                     const stm_time64_t &vsyncTime)
{
    uint32_t               num_planes_ids;
    uint32_t               planes_ids[CINTERFACE_MAX_PLANES_PER_SOURCE];
    CDisplaySource*        pDS  = GetParentSource();

    // Present the picture to every connected planes
    if ( pDS != 0 )
    {
        const CDisplayDevice* pDD = pDS->GetParentDevice();
        if ( pDD != 0)
        {
            num_planes_ids = pDS->GetConnectedPlaneID(planes_ids, N_ELEMENTS(planes_ids));
            for(uint32_t i=0; (i<num_planes_ids) ; i++)
            {
                CDisplayPlane* pDP = pDD->GetPlane(planes_ids[i]);
                if (pDP)
                {
                    pDP->PresentDisplayNode(pPrevNode,
                                        pCurrNode,
                                        pNextNode,
                                        isPictureRepeated,
                                        isDisplayInterlaced,
                                        isTopFieldOnDisplay,
                                        vsyncTime);
                }
            }
            if (num_planes_ids == 0)
            {
                m_GhostPictures++;
                if (m_GhostPictures > GHOST_PICTURE_THRESHOLD)
                {
                    TRC(TRC_ID_MAIN_INFO, "Ghost source has been detected" );
                }
            }
            else
            {
                m_GhostPictures = 0;
            }
        }
    }
}

bool CPureSwQueueBufferInterface::FillSrcPictureType(CDisplayNode *pNode)
{
    if((pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED))
    {
        if((pNode->m_bufferDesc.src.flags &
           (STM_BUFFER_SRC_TOP_FIELD_ONLY|STM_BUFFER_SRC_BOTTOM_FIELD_ONLY)) == STM_BUFFER_SRC_TOP_FIELD_ONLY)
        {
            pNode->m_srcPictureType = GNODE_TOP_FIELD;
            pNode->m_srcPictureTypeChar = 'T';
        }
        else if ((pNode->m_bufferDesc.src.flags &
                 (STM_BUFFER_SRC_TOP_FIELD_ONLY|STM_BUFFER_SRC_BOTTOM_FIELD_ONLY)) == STM_BUFFER_SRC_BOTTOM_FIELD_ONLY)
        {
            pNode->m_srcPictureType   = GNODE_BOTTOM_FIELD;
            pNode->m_srcPictureTypeChar = 'B';
        }
        else
        {
            DASSERTF(false,("Invalid Picture Type!\n"), false);
        }
    }
    else
    {
        pNode->m_srcPictureType = GNODE_PROGRESSIVE;
        pNode->m_srcPictureTypeChar = 'F';
    }

    return true;
}

// Information filled when the buffer is received (QueueBuffer)
bool CPureSwQueueBufferInterface::FillNodeInfo(CDisplayNode *pNode)
{
    bool isSrcInterlaced;


    // Workaround for Bug83264. Applied only if:
    // - Source and Output are Interlaced
    // - Source is SD
    // - Plane doesn't have a Deinterlacer
    // - Source video is in slow motion
    // - Source doesn't have temporal discontinuities
    if( (pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED)     &&
        (m_outputInfo.isDisplayInterlaced)                              &&
        (pNode->m_bufferDesc.src.visible_area.height <= 576)            &&
        (GetParentSource()->hasADeinterlacer() == false)                &&
        (pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_SLOW_MOTION)    &&
        !(pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY) )
    {
        TRC( TRC_ID_MAIN_INFO, "Source forced as Progressive" );
        pNode->m_bufferDesc.src.flags &= (~STM_BUFFER_SRC_INTERLACED);
        pNode->m_bufferDesc.src.flags &= (~STM_BUFFER_SRC_TOP_FIELD_ONLY);
        pNode->m_bufferDesc.src.flags &= (~STM_BUFFER_SRC_BOTTOM_FIELD_ONLY);
    }

    isSrcInterlaced = ((pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) == STM_BUFFER_SRC_INTERLACED);

    if (!FillSrcPictureType(pNode))
    {
        TRC( TRC_ID_ERROR, "Failed to get the Src Picture type!" );
        return false;
    }

    /* set field info */
    if(isSrcInterlaced)
    {
        /* set repeatFirstField */
        pNode->m_repeatFirstField = ((pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_REPEAT_FIRST_FIELD) != 0);

        /* set firstFieldType and firstFieldOnly flag */
        if((pNode->m_bufferDesc.src.flags & (STM_BUFFER_SRC_TOP_FIELD_ONLY|STM_BUFFER_SRC_BOTTOM_FIELD_ONLY)) != 0)
        {
            pNode->m_firstFieldType   = ((pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_TOP_FIELD_ONLY) != 0)?GNODE_TOP_FIELD:GNODE_BOTTOM_FIELD;
            pNode->m_firstFieldOnly   = true;
        }
        else
        {
            pNode->m_firstFieldType   = ((pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_BOTTOM_FIELD_FIRST) != 0)?GNODE_BOTTOM_FIELD:GNODE_TOP_FIELD;
        }
    }

    if( (pNode->m_bufferDesc.src.pixel_aspect_ratio.numerator == 0) ||
        (pNode->m_bufferDesc.src.pixel_aspect_ratio.denominator == 0) )
    {
        // Invalid source pixel aspect ratio: Use a default one
        pNode->m_bufferDesc.src.pixel_aspect_ratio.numerator = 1;
        pNode->m_bufferDesc.src.pixel_aspect_ratio.denominator = 1;
    }

    return true;
}

void CPureSwQueueBufferInterface::ReleaseUselessNodeNotDisplayed(const stm_time64_t & vsyncTime)
{
    CDisplayNode *pPrevNode = 0;
    CDisplayNode *pCurrNode = 0;
    CDisplayNode *pNextNode = 0;

    // For improving quality of deinterlacer, skipped nodes due to framerate conversion
    // can be used by the deinterlacer that's why they are enqueued. But normally, if
    // 3 consecutive nodes are skipped nodes (nfields==0), the one in the middle could
    // never be used.
    pNextNode = (CDisplayNode*)m_displayQueue.GetFirstNode();
    while (pNextNode != 0)
    {
        if(pPrevNode && pCurrNode)
        {
            // Here, pPrevNode and pCurrNode and pNextNode are not NULL
            // Check if all of them must not be displayed
            if(pPrevNode->m_doNotDisplay && pCurrNode->m_doNotDisplay && pNextNode->m_doNotDisplay)
            {
                // Check if it is really not used
                if( (pCurrNode != m_picturesUsedByHw.pPrevNode) &&
                    (pCurrNode != m_picturesUsedByHw.pCurNode) &&
                    (pCurrNode != m_picturesUsedByHw.pNextNode) &&
                    (pCurrNode != m_picturesPreparedForNextVSync.pPrevNode) &&
                    (pCurrNode != m_picturesPreparedForNextVSync.pCurNode) &&
                    (pCurrNode != m_picturesPreparedForNextVSync.pNextNode) )
                {
                    // Release the current buffer
                    TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: ReleaseNode that cannot be used %d%c (0x%p)",
                        m_interfaceID,
                        pCurrNode->m_pictureId,
                        pCurrNode->m_srcPictureTypeChar,
                        pCurrNode);
                    ReleaseDisplayNode(pCurrNode, vsyncTime);

                    // pPrevNode must not changed so hack pCurrNode
                    pCurrNode = pPrevNode;
                }
                else
                {
                    TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: WARNING cannot ReleaseNode %d%c (0x%p)",
                        m_interfaceID,
                        pCurrNode->m_pictureId,
                        pCurrNode->m_srcPictureTypeChar,
                        pCurrNode);
                }
            }
        }

        pPrevNode = pCurrNode;
        pCurrNode = pNextNode;
        pNextNode = (CDisplayNode*)pNextNode->GetNextNode();
    }

    if(IS_TRC_ID_ENABLED(TRC_ID_PRINT_DISPLAY_QUEUE))
    {
        PrintDisplayQueue();
    }
}

/*
    Example of pictures used at one time:

                                        10T -> 11B -> 12T -> 13B -> (14T) -> 15B -> 16T -> 17B
    Pictures used by HW:                               P      C       N
    Pictures prepared for next VSync:                                 P       C      N

    In this example, all the pictures older than m_picturesUsedByHw.pPrevNode (= 12T) will not be used anymore and can be released.
    So 10T and 11B are going to be released by CDisplayPlane::ReleaseNodesNoMoreNeeded()

    Legend:
     P = Previous DEI field
     C = Current DEI field
     N = Next DEI field
     ( ) = Picture skipped by the Frame Rate Conversion. It can be used as reference by the Deinterlacer but it should not be displayed.
*/
void CPureSwQueueBufferInterface::ReleaseNodesNoMoreNeeded(const stm_time64_t & vsyncTime)
{
    CDisplayNode *pNode = 0;
    CDisplayNode *pOldestNodeUsedByTheDisplay = 0;
    CDisplayNode *pOldestNodeUsedByPreparedForNextVSync = 0;
    CDisplayNode *pNodeToRelease = 0;

    // Parse the queue for releasing node skipped for framerate conversion that could not be used normally
    // It is mandatory for not filling all the queue with nodes skipped for FRC.
    ReleaseUselessNodeNotDisplayed(vsyncTime);

    // The releases should be done only if the display is started (m_picturesUsedByHw.pCurNode not null)
    if(!m_picturesUsedByHw.pCurNode)
    {
        return;
    }

    // Determine what is the oldest picture used by the Display
    // m_picturesUsedByHw and m_picturesPreparedForNextVSync contains oldest pictures used.
    // Due to swap, it is possible that at previous VSync, a standalone source has set only m_picturesUsedByHw.pCurNode (no plane so no deinterlacer)
    // and at next Vsync, this source come back connected to a deinterlacer and repeat the same node so m_picturesPreparedForNextVSync.pCurNode=m_picturesUsedByHw.pCurNode
    // but additionnal node will be necessary m_picturesPreparedForNextVSync.pNextNode and is really the oldest node to keep.
    if(m_picturesUsedByHw.pPrevNode)
    {
        pOldestNodeUsedByTheDisplay = m_picturesUsedByHw.pPrevNode;
    }
    else
    {
        // NB: We have already tested that m_picturesUsedByHw.pCurNode is not null
        pOldestNodeUsedByTheDisplay = m_picturesUsedByHw.pCurNode;
    }

    if(m_picturesPreparedForNextVSync.pPrevNode)
    {
        pOldestNodeUsedByPreparedForNextVSync = m_picturesPreparedForNextVSync.pPrevNode;
    }
    else
    {
        // NB: m_picturesPreparedForNextVSync.pCurNode can be null if not picture selected for next VSync,
        // in this case m_picturesPreparedForNextVSync.pNextNode/pPrevNode are also null
        pOldestNodeUsedByPreparedForNextVSync = m_picturesPreparedForNextVSync.pCurNode;
    }

    // All the pictures present in the queue and older than "pOldestNodeUsedByTheDisplay" or "pOldestNodeUsedByPreparedForNextVSync"
    // will not be needed anymore and can be released now

    /* Get the first picture of the queue (if any) */
    pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();

    while (pNode != 0)
    {
        /* Release all the nodes until "pOldestNodeUsedByTheDisplay" or "pOldestNodeUsedByPreparedForNextVSync" is found */
        if(pNode == pOldestNodeUsedByTheDisplay || pNode == pOldestNodeUsedByPreparedForNextVSync)
        {
            break;
        }

        pNodeToRelease = pNode;

        // Get next node before releasing the current one
        pNode = (CDisplayNode*) pNode->GetNextNode();

        TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: ReleaseNode %d%c (0x%p)",
                m_interfaceID,
                pNodeToRelease->m_pictureId,
                pNodeToRelease->m_srcPictureTypeChar,
                pNodeToRelease);
        ReleaseDisplayNode(pNodeToRelease, vsyncTime);
    }

    if(IS_TRC_ID_ENABLED(TRC_ID_PRINT_DISPLAY_QUEUE))
    {
        PrintDisplayQueue();
    }
}


void CPureSwQueueBufferInterface::ReleaseOutdatedNodes(const stm_time64_t & vsyncTime)
{
    CDisplayNode *pNode;
    CDisplayNode *pFirstNodeNotOutdated = 0;    // First node of the DisplayQueue with a presentationTime that is not null and not over
    CDisplayNode *pNodeToRelease;
    uint32_t      count = 0;

    TRC( TRC_ID_MAIN_INFO, "Source %d contains %d pictures. current_time=%llu", m_interfaceID, m_Statistics.PicOwned, vsyncTime);

    // The Output is stopped
    // Wait that the source gets fully stopped (no more picture in use)
    if( (m_picturesPreparedForNextVSync.pPrevNode) ||
        (m_picturesPreparedForNextVSync.pCurNode)  ||
        (m_picturesPreparedForNextVSync.pNextNode) ||
        (m_picturesUsedByHw.pPrevNode)             ||
        (m_picturesUsedByHw.pCurNode)              ||
        (m_picturesUsedByHw.pNextNode) )
    {
        // Source not yet stopped. We will try again at next VSync
        return;
    }

    // Source stopped: We can now release the outdated nodes

    // Go through the DisplayQueue and look for the first picture with a presentationTime
    // which is not over and not NULL.
    // NB: Bottom fields usually have a null presentationTime

    pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();
    while(pNode != 0)
    {
        if( (pNode->m_bufferDesc.info.presentation_time != 0) &&
            (pNode->m_bufferDesc.info.presentation_time >=  vsyncTime) )
        {
            // Found the first picture of the queue which is not outdated
            pFirstNodeNotOutdated = pNode;
            break;
        }
        pNode = (CDisplayNode*) pNode->GetNextNode();
    }

    if(pFirstNodeNotOutdated)
    {
        // All the nodes before pFirstNodeNotOutdated can be released because their presentationTime is over

        // Release all the node until pFirstNodeNotOutdated
        pNode = (CDisplayNode*)m_displayQueue.GetFirstNode();
        count = 0;

        while(pNode != pFirstNodeNotOutdated)
        {
            pNodeToRelease = pNode;
            TRC(TRC_ID_PICT_QUEUE_RELEASE, "Source %d: release outdated node %d%c (0x%p) with presentationTime %llu",
                m_interfaceID,
                pNodeToRelease->m_pictureId,
                pNodeToRelease->m_srcPictureTypeChar,
                pNodeToRelease,
                pNodeToRelease->m_bufferDesc.info.presentation_time);

            pNode = (CDisplayNode*) pNode->GetNextNode();
            ReleaseDisplayNode(pNodeToRelease, vsyncTime);

            count++;
        }

        TRC( TRC_ID_MAIN_INFO, "Source %d: Released %d outdated nodes", m_interfaceID, count);
    }
    else
    {
        TRC( TRC_ID_MAIN_INFO, "Source %d: No outdated node found", m_interfaceID);
    }

    if(IS_TRC_ID_ENABLED(TRC_ID_PRINT_DISPLAY_QUEUE))
    {
        PrintDisplayQueue();
    }
}


// This function will send the completed_callback and release a node from the queue
void CPureSwQueueBufferInterface::ReleaseDisplayNode(CDisplayNode *pNodeToRelease, const stm_time64_t &vsyncTime)
{
    stm_buffer_presentation_t *pInfo;

    if(!pNodeToRelease)
    {
        TRC( TRC_ID_ERROR, "Invalid pNodeToRelease!" );
        return;
    }

    if( (pNodeToRelease == m_picturesPreparedForNextVSync.pPrevNode) ||
        (pNodeToRelease == m_picturesPreparedForNextVSync.pCurNode) ||
        (pNodeToRelease == m_picturesPreparedForNextVSync.pNextNode) ||
        (pNodeToRelease == m_picturesUsedByHw.pPrevNode) ||
        (pNodeToRelease == m_picturesUsedByHw.pCurNode) ||
        (pNodeToRelease == m_picturesUsedByHw.pNextNode) )
    {
        TRC( TRC_ID_ERROR, "Problem, release a used node 0x%p - PreparedForNextVSync Prev=0x%p Cur=0x%p Next=0x%p UsedByHw Prev=0x%p Cur=0x%p Next=0x%p!",
            pNodeToRelease, m_picturesPreparedForNextVSync.pPrevNode,
            m_picturesPreparedForNextVSync.pCurNode,m_picturesPreparedForNextVSync.pNextNode,
            m_picturesUsedByHw.pPrevNode, m_picturesUsedByHw.pCurNode, m_picturesUsedByHw.pNextNode);
    }

    pInfo = &pNodeToRelease->m_bufferDesc.info;
    if(pInfo)
    {
        pInfo->stats.vsyncTime = vsyncTime;
        pInfo->stats.status = 0;
        if(pInfo->completed_callback)
        {
            TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: completed_callback node %d%c (0x%p) from user 0x%p (user_data=0x%p)",
                m_interfaceID,
                pNodeToRelease->m_pictureId,
                pNodeToRelease->m_srcPictureTypeChar,
                pNodeToRelease,
                m_user,
                pInfo->puser_data);
            pInfo->completed_callback(pInfo->puser_data, &pInfo->stats);
        }
        else // a NULL CompletedCallback is normal with a buffer taqged persistent
            if(!(pInfo->ulFlags & STM_BUFFER_PRESENTATION_PERSISTENT))
                TRC( TRC_ID_ERROR, "Invalid PresentationData! Missing CompletedCallback with a non persistent presentation!" );
    }
    else
    {
        TRC( TRC_ID_MAIN_INFO, "Node with null pInfo: Impossible to notify the owner of this buffer" );
    }

    m_Statistics.PicOwned--;
    m_Statistics.PicReleased++;
    TRC( TRC_ID_PICT_QUEUE_RELEASE, "Source %d: Release %d%c 0x%p (Owned:%d)",
                m_interfaceID,
                pNodeToRelease->m_pictureId,
                pNodeToRelease->m_srcPictureTypeChar,
                pNodeToRelease,
                m_Statistics.PicOwned);

    // This ReleaseDisplayNode() function can be called in 2 contexts:
    // - During the OutputVSyncThreadedIrqUpdateHW. In that case "m_vsyncLock" is taken to ensure that nobody else is updating the queue.
    // - During the flush function. In that case there is a mechanism in the Flush function to do all the critical code in a protected section. The queue is cut
    //   in 2 parts. The head of the queue is still in use (and can be used by the OutputVSyncThreadedIrqUpdateHW) whereas the tail is no more seen by the OutputVSyncThreadedIrqUpdateHW
    //   so it is safe to release it without taking the "m_vsyncLock"
    if (!m_displayQueue.ReleaseDisplayNode(pNodeToRelease))
    {
        TRC( TRC_ID_ERROR, "Failed to release node %d%c (0x%p)!",
            pNodeToRelease->m_pictureId,
            pNodeToRelease->m_srcPictureTypeChar,
            pNodeToRelease);
    }
}


// This function is called only if the trace TRC_ID_BUFFER_DESCRIPTOR gets enabled
void CPureSwQueueBufferInterface::PrintBufferDescriptor(const CDisplayNode *pNode)
{
    char Htext[25];
    char Vtext[25];

    if(!pNode)
    {
        return;
    }

    // Print a selection of interesting info about this node
    // Group them to reduce size of the log
    TRCBL(TRC_ID_BUFFER_DESCRIPTOR);

    TRC( TRC_ID_BUFFER_DESCRIPTOR, "Source %d: Received Picture %d %c",
            m_interfaceID,
            pNode->m_pictureId,
            pNode->m_srcPictureTypeChar);

    TRC(TRC_ID_BUFFER_DESCRIPTOR, "Primary picture: W=%d H=%d P=%d color_fmt=%d",
        pNode->m_bufferDesc.src.primary_picture.width,
        pNode->m_bufferDesc.src.primary_picture.height,
        pNode->m_bufferDesc.src.primary_picture.pitch,
        pNode->m_bufferDesc.src.primary_picture.color_fmt);

    TRC(TRC_ID_BUFFER_DESCRIPTOR, "Secondary picture: W=%d H=%d P=%d color_fmt=%d",
        pNode->m_bufferDesc.src.secondary_picture.width,
        pNode->m_bufferDesc.src.secondary_picture.height,
        pNode->m_bufferDesc.src.secondary_picture.pitch,
        pNode->m_bufferDesc.src.secondary_picture.color_fmt);

    TRC(TRC_ID_BUFFER_DESCRIPTOR, "visible_area: X=%d Y=%d W=%d H=%d",
        pNode->m_bufferDesc.src.visible_area.x,
        pNode->m_bufferDesc.src.visible_area.y,
        pNode->m_bufferDesc.src.visible_area.width,
        pNode->m_bufferDesc.src.visible_area.height);

    TRC(TRC_ID_BUFFER_DESCRIPTOR, "pixel_aspect_ratio: %d / %d",
        pNode->m_bufferDesc.src.pixel_aspect_ratio.numerator,
        pNode->m_bufferDesc.src.pixel_aspect_ratio.denominator);

    switch(pNode->m_bufferDesc.src.horizontal_decimation_factor)
    {
        case STM_NO_DECIMATION:
        {
            vibe_os_snprintf(Htext, sizeof(Htext), "H1");
            break;
        }
        case STM_DECIMATION_BY_TWO:
        {
            vibe_os_snprintf(Htext, sizeof(Htext), "H2");
            break;
        }
        case STM_DECIMATION_BY_FOUR:
        {
            vibe_os_snprintf(Htext, sizeof(Htext), "H4");
            break;
        }
        case STM_DECIMATION_BY_EIGHT:
        {
            vibe_os_snprintf(Htext, sizeof(Htext), "H8");
            break;
        }
        default:
        {
            TRC(TRC_ID_ERROR, "Invalid horizontal decimation factor");
            vibe_os_snprintf(Htext, sizeof(Htext), "H?");
            break;
        }
    }

    switch(pNode->m_bufferDesc.src.vertical_decimation_factor)
    {
        case STM_NO_DECIMATION:
        {
            vibe_os_snprintf(Vtext, sizeof(Vtext), "V1");
            break;
        }
        case STM_DECIMATION_BY_TWO:
        {
            vibe_os_snprintf(Vtext, sizeof(Vtext), "V2");
            break;
        }
        case STM_DECIMATION_BY_FOUR:
        {
            vibe_os_snprintf(Vtext, sizeof(Vtext), "V4");
            break;
        }
        case STM_DECIMATION_BY_EIGHT:
        {
            vibe_os_snprintf(Vtext, sizeof(Vtext), "V8");
            break;
        }
        default:
        {
            TRC(TRC_ID_ERROR, "Invalid vertical decimation factor");
            vibe_os_snprintf(Vtext, sizeof(Vtext), "V?");
            break;
        }
    }

    TRC(TRC_ID_BUFFER_DESCRIPTOR, "src_frame_rate=%d %c hdecim=%s, vdecim=%s",
        (pNode->m_bufferDesc.src.src_frame_rate.denominator ? (1000 * pNode->m_bufferDesc.src.src_frame_rate.numerator) / pNode->m_bufferDesc.src.src_frame_rate.denominator : 0),
        ( (pNode->m_bufferDesc.src.flags & STM_BUFFER_SRC_INTERLACED) ? 'I' : 'P'),
        Htext,
        Vtext);

    TRC(TRC_ID_BUFFER_DESCRIPTOR, "flags:");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_INTERLACED)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_INTERLACED");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_BOTTOM_FIELD_FIRST)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_BOTTOM_FIELD_FIRST");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_REPEAT_FIRST_FIELD)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_REPEAT_FIRST_FIELD");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_TOP_FIELD_ONLY)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_TOP_FIELD_ONLY");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_BOTTOM_FIELD_ONLY)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_BOTTOM_FIELD_ONLY");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_INTERPOLATE_FIELDS)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_INTERPOLATE_FIELDS");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_COLORSPACE_709)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_COLORSPACE_709");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_PREMULTIPLIED_ALPHA)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_PREMULTIPLIED_ALPHA");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_LIMITED_RANGE_ALPHA)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_LIMITED_RANGE_ALPHA");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_PAN_AND_SCAN)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_PAN_AND_SCAN");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_CONST_ALPHA)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_CONST_ALPHA");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_REPEATED_PICTURE)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_REPEATED_PICTURE");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_TEMPORAL_DISCONTINUITY");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_VC1_POSTPROCESS_LUMA)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_VC1_POSTPROCESS_LUMA");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_VC1_POSTPROCESS_CHROMA)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_VC1_POSTPROCESS_CHROMA");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_CHARACTERISTIC_DISCONTINUITY)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_CHARACTERISTIC_DISCONTINUITY");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_FORCE_DISPLAY)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_FORCE_DISPLAY");
    if(pNode->m_bufferDesc.src.flags&STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY)
        TRC(TRC_ID_BUFFER_DESCRIPTOR, "STM_BUFFER_SRC_RESPECT_PICTURE_POLARITY");


    TRC(TRC_ID_BUFFER_DESCRIPTOR, "presentation_time=%lld, PTS=%lld, nfields=%d, ulFlags=0x%x, puser_data=0x%p",
        pNode->m_bufferDesc.info.presentation_time,
        pNode->m_bufferDesc.info.PTS,
        pNode->m_bufferDesc.info.nfields,
        pNode->m_bufferDesc.info.ulFlags,
        pNode->m_bufferDesc.info.puser_data);

    TRCBL(TRC_ID_BUFFER_DESCRIPTOR);
}

void CPureSwQueueBufferInterface::PrintDisplayQueue()
{
#define IS_NODE_IN_TRIPLET(n,t)             ((n)==t.pPrevNode || (n)==t.pCurNode || (n)==t.pNextNode)
#define IS_NODE_USED_BY_HW(n)               IS_NODE_IN_TRIPLET(n,m_picturesUsedByHw)
#define IS_NODE_PREPARED_FOR_NEXT_VSYNC(n)  IS_NODE_IN_TRIPLET(n,m_picturesPreparedForNextVSync)
#define IS_NODE_NOT_TO_BE_DISPLAYED(n)      ((n)->m_doNotDisplay)
#define IS_NODE_ON_DISPLAY(n)               ((n)==m_picturesUsedByHw.pCurNode)

    TRC( TRC_ID_PRINT_DISPLAY_QUEUE, "----- Source %d: Display queue content -----", m_interfaceID);

    const CDisplayNode* pNode = (const CDisplayNode*)m_displayQueue.GetFirstNode();

    if(!pNode)
    {
        TRC( TRC_ID_PRINT_DISPLAY_QUEUE, "Source %d: Display queue is empty", m_interfaceID);
        return;
    }

    while(pNode!=0)
    {
        TRC( TRC_ID_PRINT_DISPLAY_QUEUE, "Source %d: Node %d%c (%c %c) %s %s",
             m_interfaceID, pNode->m_pictureId, pNode->m_srcPictureTypeChar,
             IS_NODE_USED_BY_HW(pNode)              ? 'U' : '-',
             IS_NODE_PREPARED_FOR_NEXT_VSYNC(pNode) ? 'P' : '-',
             IS_NODE_ON_DISPLAY(pNode)              ? "(On Display)" : "",
             IS_NODE_NOT_TO_BE_DISPLAYED(pNode)     ? "(Do Not Display)" : "");
        pNode = (const CDisplayNode*)pNode->GetNextNode();
    }
}

/* end of file */

