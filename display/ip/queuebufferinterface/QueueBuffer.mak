ifneq ($(CONFIG_QUEUE_BUFFER),)

# STM Blitter include path
EXTRA_CFLAGS += -I$(CONFIG_BLITTER_PATH)/linux/kernel/include/linux/stm/

STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/queuebufferinterface/,  \
						PureSwQueueBufferInterface.cpp)

endif
