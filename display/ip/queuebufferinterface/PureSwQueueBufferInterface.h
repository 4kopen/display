/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2011-2014 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2014-05-30
***************************************************************************/

#ifndef PURE_SW_QUEUE_BUFFER_INTERFACE_H
#define PURE_SW_QUEUE_BUFFER_INTERFACE_H


#include <display/generic/QueueBufferInterface.h>
#include <display/ip/buffercopy/buffercopy.h>

#define GHOST_PICTURE_THRESHOLD 500

///////////////////////////////////////////////////////////////////////////////
// Base class for G2 and G3 compositor queue buffer source interface

class CPureSwQueueBufferInterface: public CQueueBufferInterface
{
public:
    CPureSwQueueBufferInterface ( uint32_t interfaceID, CDisplaySource * pSource, bool isConnectionToVideoPlaneAllowed = true );
    virtual ~CPureSwQueueBufferInterface();

    bool                                Create(void);

    virtual uint32_t                    GetTimingID(void) const { return m_ulTimingID; }

    virtual void                        UpdateOutputInfo(CDisplayPlane *pPlane);


    virtual QueueBufferInterfaceResults QueueBuffer(const stm_display_buffer_t * const ,
                                                    const void                 * const );

    virtual bool                        IsEmpty();

    virtual QueueBufferInterfaceResults FlushStart(flush_requested_t  &requestedFlush,
                                                   const void * const user);
    virtual QueueBufferInterfaceResults WaitFlushCompletion(flush_requested_t  requestedFlush,
                                                            const void * const user);
    virtual QueueBufferInterfaceResults WaitFlushCondition(const void * const user);
    virtual QueueBufferInterfaceResults CompleteFlushAll(void);

    virtual QueueBufferInterfaceResults LockUse(void *user);
    virtual QueueBufferInterfaceResults Unlock(void *user);
    virtual QueueBufferInterfaceResults Release(void *user);

    int                                 GetBufferFormats(const stm_pixel_format_t **) const;
    bool                                IsConnectionPossible(CDisplayPlane *pPlane) const;

    virtual void                        OutputVSyncThreadedIrqUpdateHW(bool isDisplayInterlaced, bool isTopFieldOnDisplay, const stm_time64_t &vsyncTime);

    bool                                EvaluateFlushCondition(const void * const user);

protected:
    bool                                m_isConnectionToVideoPlaneAllowed;
    void                               *m_user;                     // Token indicating the user of this plane

    CDisplayQueue                       m_displayQueue;

    display_triplet_t                   m_picturesPreparedForNextVSync;
    display_triplet_t                   m_picturesUsedByHw;

    uint32_t                            m_IncomingPictureCounter;   // Free running counter incremented each time that a new picture is queued
    stm_time64_t                        m_lastPresentationTime;

    stm_display_latency_params_t        *m_LatencyParams;
    uint32_t                            *m_LatencyPlaneIds;

    uint32_t                            m_ulTimingID;               // The timing ID of the VTG pacing the planes connected to this source
    output_info_t                       m_outputInfo;               // Information about the current display mode and display aspect ratio

    uint32_t                            m_GhostPictures;            // number of ghost pictures (provided by a source without being connected to any plan)

    VIBE_OS_WaitQueue_t                 m_flushAllWaitQueue;       // Flush All wait queue for blocking the caller thread until all wanted node(s) are released
    flush_requested_t                   m_flushStatus;             // Indicate if a flush is on-going

    CDisplayNode                       *m_nodeToCopy;
    CDisplayNode                       *m_nodeCopied;

    void                                FlushAllNodesExceptUsed(void);

    void SendDisplayCallback            (CDisplayNode *pNode, const stm_time64_t &vsyncTime);
    CDisplayNode * SelectPictureForNextVSync(bool                 isDisplayInterlaced,
                                              bool                isTopFieldOnDisplay,
                                              const stm_time64_t &vsyncTime);
    bool FillSrcPictureType             (CDisplayNode *pNode);
    bool FillNodeInfo                   (CDisplayNode *pNode);
    void SetPreviousNode                (CDisplayNode *pNode, const stm_time64_t &vsyncTime);
    void ReleaseDisplayNode             (CDisplayNode *pNodeToRelease, const stm_time64_t &vsyncTime);
    void ReleaseUselessNodeNotDisplayed (const stm_time64_t & vsyncTime);
    void ReleaseNodesNoMoreNeeded       (const stm_time64_t & vsyncTime);
    void ReleaseOutdatedNodes           (const stm_time64_t & vsyncTime);

    CDisplayNode*   GetPreviousNode     (CDisplayNode *pCurNode);
    void            ResetTriplet        (display_triplet_t *pTriplet);

    void PresentDisplayNode             (CDisplayNode *pPrevNode,
                                         CDisplayNode *pCurrNode,
                                         CDisplayNode *pNextNode,
                                         bool isPictureRepeated,
                                         bool isDisplayInterlaced,
                                         bool isTopFieldOnDisplay,
                                         const stm_time64_t &vsyncTime);
    uint32_t                             FillParamLatencyInfo(uint16_t *output_change_status);

    void PrintBufferDescriptor          (const CDisplayNode *pNode);
    void PrintDisplayQueue              ();

    CDisplayNode*                        GetLocalBufferAllocactedInQueue(void);


private:
    CPureSwQueueBufferInterface(const CPureSwQueueBufferInterface&);
    CPureSwQueueBufferInterface& operator=(const CPureSwQueueBufferInterface&);

    uint32_t            m_blitterId; // Blitter Hardware Identifier for copying buffers

    bool                FlushPartialWithCopy(void);
    bool                CreateAndEnqueueCopiedPictBuffer(CDisplayNode                  *src_node_to_copy,
                                                         CopiedPictBuffers             *dst_copied_buffers);

    vibe_time64_t       m_flushStartTime;
    vibe_time64_t       m_blitterCopyStartTime;
};

#endif /* PURE_SW_QUEUE_BUFFER_INTERFACE_H */

