/*
* This file is part of Display Engine
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: GPLv2
*
* Display Engine is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* Display Engine is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Display Engine. If not, see
* <http://www.gnu.org/licenses/>.
*/

#ifndef _GDP_PLANE_H
#define _GDP_PLANE_H

#include <display/generic/DisplayPlane.h>
#include <display/ip/gdp/GdpReg.h>
#include <display/ip/gdp/GdpDisplayInfo.h>

struct GENERIC_GDP_LLU_NODE
{
	uint32_t GDPn_CTL;   /* 0x00 */
	uint32_t GDPn_AGC;   /* 0x04 */
	uint32_t GDPn_HSRC;  /* 0x08 */
	uint32_t GDPn_VPO;   /* 0x0C */
/***** Next 128 bit *************/
	uint32_t GDPn_VPS;   /* 0x10 */
	uint32_t GDPn_PML;   /* 0x14 */
	uint32_t GDPn_PMP;   /* 0x18 */
	uint32_t GDPn_SIZE;  /* 0x1C */
/***** Next 128 bit *************/
	uint32_t GDPn_VSRC;  /* 0x20 */
	uint32_t GDPn_NVN;   /* 0x24 */
	uint32_t GDPn_KEY1;  /* 0x28 */
	uint32_t GDPn_KEY2;  /* 0x2C */
/***** Next 128 bit *************/
	uint32_t GDPn_HFP;   /* 0x30 */
	uint32_t GDPn_PPT;   /* 0x34 */
	uint32_t GDPn_VFP;   /* 0x38 7109Cut3 Vertical filter pointer */
	uint32_t GDPn_CML;   /* 0x3C 7109Cut3 Clut pointer            */
/***** Next 128 bit *************/
	uint32_t GDPn_CROP;  /* 0x40 H418 Horizontal Crop   */
	uint32_t GDPn_BT0;   /* 0x44 H418 BT2020_0 register */
	uint32_t GDPn_BT1;   /* 0x48 H418 BT2020_0 register */
	uint32_t GDPn_BT2;   /* 0x4C H418 BT2020_0 register */
/***** Next 128 bit *************/
	uint32_t GDPn_BT3;   /* 0x50 H418 BT2020_0 register */
	uint32_t GDPn_BT4;   /* 0x54 H418 BT2020_0 register */
	uint32_t Reserved1;  /* 0x58 align to 128 bits      */
	uint32_t Reserved2;  /* 0x5C align to 128 bits      */
};

#define DEBUGGDP(pGDP)\
{   \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_CTL  = %#.8x  GDPn_AGC  = %#.8x", (pGDP)->GDPn_CTL,  (pGDP)->GDPn_AGC ); \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_HSRC = %#.8x  GDPn_VPO  = %#.8x", (pGDP)->GDPn_HSRC, (pGDP)->GDPn_VPO ); \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_VPS  = %#.8x  GDPn_PML  = %#.8x", (pGDP)->GDPn_VPS,  (pGDP)->GDPn_PML ); \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_PMP  = %#.8x  GDPn_SIZE = %#.8x", (pGDP)->GDPn_PMP,  (pGDP)->GDPn_SIZE ); \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_VSRC = %#.8x  GDPn_NVN  = %#.8x", (pGDP)->GDPn_VSRC, (pGDP)->GDPn_NVN  ); \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_KEY1 = %#.8x  GDPn_KEY2 = %#.8x", (pGDP)->GDPn_KEY1, (pGDP)->GDPn_KEY2 ); \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_HFP  = %#.8x  GDPn_PPT  = %#.8x", (pGDP)->GDPn_HFP,  (pGDP)->GDPn_PPT ); \
  PLANE_TRC( TRC_ID_GDP_REG, "  GDPn_VFP  = %#.8x  GDPn_CML  = %#.8x", (pGDP)->GDPn_VFP,  (pGDP)->GDPn_CML ); \
}

typedef struct
{
  bool                      isValid;
  GENERIC_GDP_LLU_NODE      topNode;
  GENERIC_GDP_LLU_NODE      botNode;
  stm_buffer_presentation_t info;
  BufferNodeType            nodeType;
  uint32_t                  pictureId;
  bool                      areIOWindowsValid;
} GdpSetup_t;

class CGdpPlane: public CDisplayPlane
{
  friend class CVBIPlane;
  public:
    CGdpPlane(const char     *name,
              uint32_t        id,
              const CDisplayDevice *pDev,
              const stm_plane_capabilities_t caps,
              uint32_t        baseAddr,
              const char     *pixClockName,
              const char     *procClockName,
              const char     *defaultParentClockName,
              bool            bHasClut = true);

    ~CGdpPlane();

    virtual bool GetCompoundControlRange(stm_display_plane_control_t selector, stm_compound_control_range_t *range);
    virtual bool IsFeatureApplicable( stm_plane_feature_t feature, bool *applicable) const;

    virtual bool Create(void);

    virtual DisplayPlaneResults SetControl(stm_display_plane_control_t control, uint32_t value);
    virtual DisplayPlaneResults GetControl(stm_display_plane_control_t control, uint32_t *value) const;
    virtual DisplayPlaneResults SetCompoundControl(stm_display_plane_control_t  ctrl, void * newVal);
    virtual DisplayPlaneResults GetCompoundControl(stm_display_plane_control_t  ctrl, void * currentVal);

    virtual void ClearContextFlags                   (void);
    virtual void PresentDisplayNode(CDisplayNode *pPrevNode,
                                    CDisplayNode *pCurrNode,
                                    CDisplayNode *pNextNode,
                                    bool isPictureRepeated,
                                    bool isInterlaced,
                                    bool isTopFieldOnDisplay,
                                    const stm_time64_t &vsyncTime);

    bool SetFirstGDPNodeOwner(CGdpPlane *);
    CGdpPlane *GetFirstGDPNodeOwner(void) const { return m_FirstGDPNodeOwner; }
    CGdpPlane *GetNextGDPNodeOwner(void) const { return m_NextGDPNodeOwner; }
    bool IsFirstGDPNodeOwner(void) const { return (m_FirstGDPNodeOwner == this); }
    bool IsLastGDPNodeOwner(void) const { return (m_NextGDPNodeOwner == 0); }

    bool isNVNInNodeList(uint32_t nvn, int list);

    uint32_t GetHWNVN(void) { return vibe_os_read_register(m_GDPBaseAddr, GDPn_NVN_OFFSET); }

    /*
     * Power Managment stuff
     */
    virtual void Freeze(void);
    virtual void Resume(void);

    virtual uint32_t GetIrrXout(void);

  protected:
    // Perform all the actions to disable this GDP plane
    virtual void DisablePlane(void);

    CGdpPlane(
        const char          *name,
        uint32_t             id,
        const stm_plane_capabilities_t caps,
        CGdpPlane *linktogdp);

    bool setNodeColourFmt(GENERIC_GDP_LLU_NODE       &topNode,
                          GENERIC_GDP_LLU_NODE       &botNode);

    virtual bool setNodeColourKeys(GENERIC_GDP_LLU_NODE       &topNode,
                                   GENERIC_GDP_LLU_NODE       &botNode);

    bool setNodeAlphaGain(GENERIC_GDP_LLU_NODE       &topNode,
                          GENERIC_GDP_LLU_NODE       &botNode);

    virtual bool setNodeResizeAndFilters(GENERIC_GDP_LLU_NODE     &topNode,
                                         GENERIC_GDP_LLU_NODE     &botNode);

    virtual bool setOutputViewport(GENERIC_GDP_LLU_NODE           &topNode,
                           GENERIC_GDP_LLU_NODE                   &botNode);

    bool setMemoryAddressing(GENERIC_GDP_LLU_NODE                 &topNode,
                             GENERIC_GDP_LLU_NODE                 &botNode);

    bool  setNodeFlickerFilter(GENERIC_GDP_LLU_NODE               &topNode,
                              GENERIC_GDP_LLU_NODE                &botNode);

    bool setNodeGamutMatrix(GENERIC_GDP_LLU_NODE                  &topNode,
                              GENERIC_GDP_LLU_NODE                &botNode);

    virtual bool SetupHDROutFormat(void);

    bool  setNodePixelRepeat(GENERIC_GDP_LLU_NODE                 &topNode,
                              GENERIC_GDP_LLU_NODE                &botNode);

    void WriteConfigForNextVsync(GENERIC_GDP_LLU_NODE *nextfieldsetup,
                                 uint32_t              nextflags);
    void writeFieldSetup(GENERIC_GDP_LLU_NODE *fieldsetup, uint32_t flags);

    void updateBaseAddress(void);

    void CalculateViewport(void);

    void CalculateHorizontalScaling(void);

    void CalculateVerticalScaling(void);

    void AdjustBufferInfoForScaling(void);
    virtual bool   AdjustIOWindowsForHWConstraints      (CDisplayNode * pNodeToDisplay, CDisplayInfo *pDisplayInfo) const;
    virtual bool   IsScalingPossible(CDisplayNode* pCurrNode, CDisplayInfo* pDisplayInfo);

    virtual bool   InitializeClocks(void);

    bool FillDisplayInfo(void);

    void ResetGdpSetup(void);

    bool PrepareGdpSetup(void);
    bool SetupDynamicGdpSetup(void);

    bool ApplyGdpSetup(bool isDisplayInterlaced,
                       bool isTopFieldOnDisplay,
                 const stm_time64_t &vsyncTime);

    bool SetupProgressiveNode(void);

    bool SetupSimpleInterlacedNode(void);

    void ProcessLastVsyncStatus(const stm_time64_t &vsyncTime, CDisplayNode *pNodeDisplayed);

    virtual void createDummyNode(GENERIC_GDP_LLU_NODE *);
    //virtual bool RegisterStatistics(void);
    uint32_t PackRegister(uint16_t hi,uint16_t lo) const { return (hi<<16) | lo; }

    virtual void setNodeVisbility(GENERIC_GDP_LLU_NODE *node, bool bIgnoreOnMixer, bool bForceOnMixer);

    void         MaskPlane(void);

    // Manage HDR Gain and Offset
    void FillHDRGainOffset (stm_hdr_gain_offset_t       * const dst,
                             const stm_hdr_gain_offset_t * const src) const;

    CGdpPlane *m_FirstGDPNodeOwner;
    CGdpPlane *m_NextGDPNodeOwner;

    DMA_Area m_HFilter;
    DMA_Area m_VFilter;
    DMA_Area m_FlickerFilter;
    DMA_Area m_Registers[2];
    DMA_Area m_DummyBuffer;

    stm_plane_ff_state_t m_FlickerFilterState;
    stm_plane_ff_mode_t  m_FlickerFilterMode;

    bool  m_bHasVFilter;
    bool  m_bHasFlickerFilter;
    bool  m_bHasClut;
    bool  m_bHas4_13_precision; /* otherwise 4.8 */
    bool  m_b4k2k;
    uint32_t *m_GDPBaseAddr;

    uint32_t m_ulGain;
    uint32_t m_ulAlphaRamp;
    uint32_t m_ulStaticAlpha[2];

    uint32_t m_ulDirectBaseAddress;
    uint32_t m_ulQueueBaseAddress;

    /* Retreive previous plane visibility state when Plane is frozen */
    bool            m_wasEnabled;

    bool            m_bClockInitDone;

    CDisplayNode   *m_pNodeToDisplay;
    CGdpDisplayInfo m_gdpDisplayInfo;

    GdpSetup_t      m_NextGdpSetup;
    GdpSetup_t      m_CurrentGdpSetup;

    bool  m_IsTransparencyChanged;
    bool  m_IsGainChanged;
    bool  m_IsColorKeyChanged;

    /* If this plane supports Issue Rate Regulation */
    bool  m_bHasIrrSupported;

    /* Hardware Gamut LUT support for BT709 -> BT2020 conversion */
    const uint32_t             *m_pGamutMatrix;

    /* Default Parent clock rate associated to this plane */
    unsigned long   m_DefaultParentRate;

    /* HDROut support through Gain/Offset adjustments */
    uint32_t        m_ulHDRGain;
    uint32_t        m_ulHDROffset;
    stm_hdr_gain_offset_t m_HDRGainOffset;

    /* Pixel Repeat (Pixel Doubling) */
    bool  m_bHasPixelRepeat;

    /* GDP Processing clock rate in MHz */
    uint32_t                    m_clockFreqInMHz;

    /* GDP Processing and Pixel clock tolerances in Ns (+/-50 ppm) */
    uint64_t                    m_procClockToleranceInNs;

  private:
    /* Default Parent clock associated to this plane */
    struct vibe_clk  m_DefaultParentClock;

    CGdpPlane(const CGdpPlane&);
    CGdpPlane& operator=(const CGdpPlane&);
    void EnableGdpHW(void);
    void DisableHW(void);

    // Due to HW Bug GDP need to be set to AUX when Disabled
    bool SetDefaultPixelClockParent(void);

    DMA_Area *GetGDPRegisters(unsigned int index) { return (index<2)?&m_Registers[index]:0; }

    void InitializeState(bool bHasClut, const char *defaultParentClockName);

    unsigned int GetMaxLinesSkippableByHw(unsigned int srcPicturePitch) const;

    bool IsScalingPossibleByHw(CDisplayInfo*        pDisplayInfo);
    bool IsScalingPossibleBySkippingLines(
                    CDisplayNode*                   pCurrNode,
                    CDisplayInfo*                   pDisplayInfo);
    bool IsHwProcessingTimeOk(
                    uint32_t                        vhsrcInputWidth,
                    uint32_t                        vhsrcInputHeight,
                    uint32_t                        vhsrcOutputWidth,
                    uint32_t                        vhsrcOutputHeight) const;

    void TruncateSourceToHWLimits(CDisplayInfo* pDisplayInfo);

    bool InitIrrParams(void);
    bool ApplyIrrSetup(void);
    bool UpdateIrrSetup(void);

    uint64_t m_IrrLinkCapacity;
    uint32_t m_fifo_size;
    uint64_t m_NextRBW;
    uint64_t m_PrevRBW;
};


#endif // _GDP_PLANE_H
