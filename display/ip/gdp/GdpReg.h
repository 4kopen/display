/***********************************************************************
 *
 * File: display/is/GdpReg.h
 * Copyright (c) 2004, 2005, 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef GENERICGDPREG_H
#define GENERICGDPREG_H

//CURSOR
#define CUR_CTL_REG_OFFSET   0x00
#define CUR_VPO_REG_OFFSET   0x0c
#define CUR_PML_REG_OFFSET   0x14
#define CUR_PMP_REG_OFFSET   0x18
#define CUR_SIZE_REG_OFFSET  0x1C
#define CUR_CML_REG_OFFSET   0x20
#define CUR_AWS_REG_OFFSET   0x28
#define CUR_AWE_REG_OFFSET   0x2C

//GDP - Only some of the registers are defined as common
#define GDPn_NVN_OFFSET    0x24
#define GDPn_PKZ_OFFSET    0xfc

/*
 * Modern GDP implementations use these instead of the PKZ register to
 * configure memory bus transactions.
 */
#define GDPn_PAS           0xEC
#define GDPn_MAOS          0xF0
#define GDPn_MIOS          0xF4
#define GDPn_MACS          0xF8
#define GDPn_MAMS          0xFC


/*
 * GDP plugs registers
 */
#define GDPn_MST           0xC4
#define GDPn_IVC_RESET     0xC8
#define GDPn_PLUG_G1       0xF000
#define GDPn_PLUG_G2       0xF004
#define GDPn_PLUG_G3       0xF008
#define GDPn_PLUG_C1_1     0xF020
#define GDPn_PLUG_C1_2     0xF024
#define GDPn_PLUG_C1_3     0xF028
#define GDPn_PLUG_IVC1_1   0xF120
#define GDPn_PLUG_IVC1_2   0xF124

#endif //GENERICGDPREG_H
