/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015.09.11
***************************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/generic/DisplayPlane.h>
#include "VideoPlug.h"

//Video PLUG Register offsets
#define PLUG_CTL          0x000
#define PLUG_ALP          0x004
#define PLUG_CLF          0x008
#define PLUG_VPO          0x00c
#define PLUG_VPS          0x010
#define PLUG_KEY1         0x028
#define PLUG_KEY2         0x02c
#define PLUG_MPR(n)       (0x030 + 4*(n))
#define PLUG_CROP         0x06c
#define PLUG_BC           0x070
#define PLUG_TINT         0x074
#define PLUG_CSAT         0x078
#define PLUG_BT(n)        (0x080 + 4*(n))

//Video PLUG register bit definitions
#define PLUG_CTL__IGNORE_MX2                        0x80000000
#define PLUG_CTL__IGNORE_MX1                        0x40000000
#define PLUG_CTL__SIGNED_CHROMA                     0x04000000
#define PLUG_CTL__KEY_CR_IGNORE                     0x00000000
#define PLUG_CTL__KEY_CR_INRANGE_MATCH              0x00100000
#define PLUG_CTL__KEY_CR_OUTRANGE_MATCH             0x00300000
#define PLUG_CTL__KEY_Y_IGNORE                      0x00000000
#define PLUG_CTL__KEY_Y_INRANGE_MATCH               0x00040000
#define PLUG_CTL__KEY_Y_OUTRANGE_MATCH              0x000c0000
#define PLUG_CTL__KEY_CB_IGNORE                     0x00000000
#define PLUG_CTL__KEY_CB_INRANGE_MATCH              0x00010000
#define PLUG_CTL__KEY_CB_OUTRANGE_MATCH             0x00030000
#define PLUG_CTL__COLOR_FILL_ENABLE                 0x00008000
#define PLUG_CTL__KEY_ENABLE                        0x00004000
#define PLUG_CTL__ALPHA_V_BORDER_ENABLE             0x00002000
#define PLUG_CTL__ALPHA_H_BORDER_ENABLE             0x00001000
#define PLUG_CTL__PSI_SAT_ENABLE                    0x00000004
#define PLUG_CTL__PSI_TINT_ENABLE                   0x00000002
#define PLUG_CTL__PSI_BC_ENABLE                     0x00000001
#define PLUG_CTL__PIXEL_REPEAT                      0x00000080

#define PLUG_ALP__ALPHA_TRANSPARENT                 0x00000000
#define PLUG_ALP__ALPHA_OPAQUE                      0x00000080
#define PLUG_ALP__ALPHA_DISCARD_AIN                 0x00000400
#define PLUG_ALP__VIDEO_COLOR_FILL_RED_SHIFT        16
#define PLUG_ALP__VIDEO_COLOR_FILL_RED_MASK         0xFFFF0000

#define PLUG_CLF__VIDEO_COLOR_FILL_BLUE_SHIFT       0
#define PLUG_CLF__VIDEO_COLOR_FILL_BLUE_MASK        0x0000FFFF
#define PLUG_CLF__VIDEO_COLOR_FILL_GREEN_SHIFT      16
#define PLUG_CLF__VIDEO_COLOR_FILL_GREEN_MASK       0xFFFF0000

#define PLUG_BRIGHTNESS_MASK                        0xff
#define PLUG_BRIGHTNESS_SHIFT                       0x0
#define PLUG_CONTRAST_MASK                          0xff
#define PLUG_CONTRAST_SHIFT                         0x8
#define PLUG_TINT_MASK                              0x3f
#define PLUG_TINT_SHIFT                             0x2
#define PLUG_CSAT_MASK                              0x3f
#define PLUG_CSAT_SHIFT                             0x2

#define CONTRAST_MIN                                0
#define CONTRAST_MAX                                255
#define CONTRAST_DEFAULT                            128
#define BRIGHTNESS_MIN                              0
#define BRIGHTNESS_MAX                              255
#define BRIGHTNESS_DEFAULT                          128
#define TINT_MIN                                    0
#define TINT_MAX                                    255
#define TINT_DEFAULT                                128
#define SATURATION_MIN                              0
#define SATURATION_MAX                              255
#define SATURATION_DEFAULT                          128

#define GET_RED(rgb)                                ( ((rgb) & 0x00FF0000) >> 16 )
#define GET_GREEN(rgb)                              ( ((rgb) & 0x0000FF00) >> 8  )
#define GET_BLUE(rgb)                               ( ((rgb) & 0x000000FF) >> 0  )
#define RGB(r,g,b)                                  ( (((r) << 16) & 0x00FF0000) | (((g) << 8) & 0x0000FF00) | (((b) << 0) & 0x000000FF) )
#define TO_10_BITS(c)                               ( ((c) << 2) & 0x000003FC )

#define COLOR_FILL_DEFAULT                          RGB(0,0,255)

#define PLUG_MPR_REGS_NB                            4
#define PLUG_BT_REGS_NB                             5

#define PLUG_TRC(id,fmt,args...)                    TRC   (id, "%s - " fmt, m_planeName, ##args)
#define PLUG_TRCIN(id,fmt,args...)                  TRCIN (id, "%s - " fmt, m_planeName, ##args)
#define PLUG_TRCOUT(id,fmt,args...)                 TRCOUT(id, "%s - " fmt, m_planeName, ##args)

enum VideoPlugColorSpace_e
{
    VIDEO_PLUG_COLOR_SPACE_BT601       = 0,
    VIDEO_PLUG_COLOR_SPACE_BT709       = 1,
    VIDEO_PLUG_COLOR_SPACE_BT2020      = 2,
    VIDEO_PLUG_COLOR_SPACE_NB          = 3
};

#if !defined(CONFIG_DISPLAY_REMOVE_TRACES)
static const char* color_space_str[VIDEO_PLUG_COLOR_SPACE_NB] =
{
    "BT.601",
    "BT.709",
    "BT.2020"
};
#endif

/*
 * YCbCr to RGB conversion matrices.
 * The coefficients are as close to the required values as possible given the
 * available representation.
 */
static const uint32_t mpr601_table[PLUG_MPR_REGS_NB] = {
  0x0a800000,
  0x0aaf0000, /* R = Y+1.3711Cr          */
  0x094e0754, /* G = Y-0.6992Cr-0.3359Cb */
  0x00000add  /* B = Y+1.7344Cb          */
};

static const uint32_t mpr709_table[PLUG_MPR_REGS_NB] = {
  0x0a800000,
  0x0ac50000, /* R = Y+1.5391Cr          */
  0x07160545, /* G = Y-0.4570Cr-0.1836Cb */
  0x00000ae8  /* B = Y+1.8125Cb          */
};

static const uint32_t mpr2020_table[PLUG_MPR_REGS_NB] = {
  0x0a800000,
  0x0abd0000, /* R = Y + 1.4746*Cr             */
  0x096e0557, /* G = Y - 0.1646*Cr - 0.5714*Cb */
  0x00000af1  /* B = Y + 1.8814*Cb             */
};

static const uint32_t* mpr_table[VIDEO_PLUG_COLOR_SPACE_NB] =
{
  mpr601_table,
  mpr709_table,
  mpr2020_table
};


/*
 * Color space conversion matrices.
 */
struct gamut_conversion_s
{
    const char*     name;                   /* string associated to this gamut conversion */
    const uint32_t  table[PLUG_BT_REGS_NB]; /* gamut conversion coefficients table        */
};

/* No gamut conversion table */
static const gamut_conversion_s no_gamut_conversion =
{
    "no conversion",
    {
        0x00000000, /* Coef1 = 0.0   ; Coef0 = 0.0 */
        0x00000000, /* Coef3 = 0.0   ; Coef2 = 0.0 */
        0x00000000, /* Coef5 = 0.0   ; Coef4 = 0.0 */
        0x00000000, /* Coef7 = 0.0   ; Coef6 = 0.0 */
        0x00000000  /* BT Enable = 0 ; Coef8 = 0.0 */
    }
};

/* BT.709 to BT.2020-NCL (Non Constant Luminance) gamut conversion table */
static const gamut_conversion_s bt709_to_bt2020_conversion =
{
    "BT.709 to BT.2020",
    {
        0x0a8a1414, /* Coef1 = 0.3293 ; Coef0 = 0.6274 */
        0x02360163, /* Coef3 = 0.0691 ; Coef2 = 0.0433 */
        0x005d1d6d, /* Coef5 = 0.0114 ; Coef4 = 0.9195 */
        0x02d10086, /* Coef7 = 0.0880 ; Coef6 = 0.0164 */
        0x00011ca9  /* BT Enable = 1  ; Coef8 = 0.8956 */
    }
};

/* BT.2020-NCL (Non Constant Luminance) to BT.709 gamut conversion table */
static const gamut_conversion_s bt2020_to_bt709_conversion =
{
    "BT.2020 to BT.709",
    {
        0xed323523, /* Coef1 = -0.5876 ; Coef0 =  1.6605 */
        0xfc03fdac, /* Coef3 = -0.1246 ; Coef2 = -0.0728 */
        0xffbc2441, /* Coef5 = -0.0083 ; Coef4 =  1.1329 */
        0xfcc8ff6b, /* Coef7 = -0.1006 ; Coef6 = -0.0182 */
        0x000123cc  /* BT Enable = 1   ; Coef8 =  1.1187 */
    }
};

/* Gamut conversion description table    */
/* NB: BT.601 is handled as BT.709       */
static const gamut_conversion_s* gamut_conversion[VIDEO_PLUG_COLOR_SPACE_NB][VIDEO_PLUG_COLOR_SPACE_NB] =
{
    /*              BT.601                       BT.709                       BT.2020                    */
    /* BT.601  */ { &no_gamut_conversion,        &no_gamut_conversion,        &bt709_to_bt2020_conversion },
    /* BT.709  */ { &no_gamut_conversion,        &no_gamut_conversion,        &bt709_to_bt2020_conversion },
    /* BT.2020 */ { &bt2020_to_bt709_conversion, &bt2020_to_bt709_conversion, &no_gamut_conversion        }
};


static inline VideoPlugColorSpace_e GetFrameColorSpace(uint32_t flags)
{
    return flags & STM_BUFFER_SRC_COLORSPACE_709    ? VIDEO_PLUG_COLOR_SPACE_BT709  :
           flags & STM_BUFFER_SRC_COLORSPACE_BT2020 ? VIDEO_PLUG_COLOR_SPACE_BT2020 :
                                                      VIDEO_PLUG_COLOR_SPACE_BT601;
}


static inline VideoPlugColorSpace_e GetOutputColorSpace(stm_ycbcr_colorspace_t outputColorSpace)
{
    return outputColorSpace == STM_YCBCR_COLORSPACE_601 ? VIDEO_PLUG_COLOR_SPACE_BT601  :
           outputColorSpace == STM_YCBCR_COLORSPACE_709 ? VIDEO_PLUG_COLOR_SPACE_BT709  :
           outputColorSpace == STM_COLORSPACE_BT2020    ? VIDEO_PLUG_COLOR_SPACE_BT2020 :
                                                          VIDEO_PLUG_COLOR_SPACE_NB;
}


CVideoPlug::CVideoPlug(CDisplayDevice* pDev,
                       uint32_t        plugOffset,
                       bool            bEnablePSI,
                       bool            bHasHwInputCrop,
                       bool            bHasIgnoreOnMixer,
                       bool            bHasPixelRepeat,
                       bool            bHasGamutConverter)
{
  m_plugBaseAddr        = (uint32_t *)((uint8_t *)pDev->GetCtrlRegisterBase() + plugOffset);
  m_planeName           = "";
  m_bEnablePSI          = bEnablePSI;
  m_bColorFillEnabled   = false;
  m_ulBrightness        = BRIGHTNESS_DEFAULT;
  m_ulContrast          = CONTRAST_DEFAULT;
  m_ulSaturation        = SATURATION_DEFAULT;
  m_ulTint              = TINT_DEFAULT;
  m_ulColorFill         = COLOR_FILL_DEFAULT;
  m_bHasHwInputCrop     = bHasHwInputCrop;
  m_bHasIgnoreOnMixer   = bHasIgnoreOnMixer;
  m_bHasPixelRepeat     = bHasPixelRepeat;
  m_bHasGamutConverter  = bHasGamutConverter;

  CreatePSISetup();
}


CVideoPlug::~CVideoPlug(void)
{
}


void CVideoPlug::SetPlaneName(const char* planeName)
{
  m_planeName = planeName ? planeName : "";
}


bool CVideoPlug::CreatePlugSetup(VideoPlugSetup                &plugSetup,
                                 const stm_display_buffer_t    * const pFrame,
                                 const stm_viewport_t          &viewport,
                                 stm_pixel_format_t            src_color_fmt,
                                 uint32_t                      transparency,
                                 const stm_color_key_config_t  * const pColorKeyConfig,
                                 const VideoPlugCrop           * const pCrop,
                                 bool                          bPixelRepeat,
                                 stm_ycbcr_colorspace_t        outputColorSpace)
{
  uint8_t ucRCR = 0;
  uint8_t ucGY  = 0;
  uint8_t ucBCB = 0;

  plugSetup.VIDn_VPO = viewport.startPixel | (viewport.startLine << 16);
  plugSetup.VIDn_VPS = viewport.stopPixel  | (viewport.stopLine  << 16);
  plugSetup.VIDn_CROP = pCrop == 0 ? 0 : (pCrop->left | (pCrop->right << 16));

  PLUG_TRC( TRC_ID_VIDEO_PLUG, "VPO = %#08x, VPS = %#08x", plugSetup.VIDn_VPO, plugSetup.VIDn_VPS );

  plugSetup.VIDn_CTL = 0;

  const VideoPlugColorSpace_e frame_cs = GetFrameColorSpace(pFrame->src.flags);
  PLUG_TRC( TRC_ID_VIDEO_PLUG, "Frame color space : %s", color_space_str[frame_cs]);

  plugSetup.pVIDn_MPR = mpr_table[frame_cs];

  if(m_bHasGamutConverter)
  {
      const VideoPlugColorSpace_e output_cs = GetOutputColorSpace(outputColorSpace);

      if(output_cs >= VIDEO_PLUG_COLOR_SPACE_NB)
      {
          PLUG_TRC( TRC_ID_ERROR, "Unsupported output color space %d", outputColorSpace);
          return false;
      }

      PLUG_TRC( TRC_ID_VIDEO_PLUG, "Output color space : %s", color_space_str[output_cs]);
      PLUG_TRC( TRC_ID_VIDEO_PLUG, "Color space conversion : %s", gamut_conversion[frame_cs][output_cs]->name);

      plugSetup.pVIDn_BT = gamut_conversion[frame_cs][output_cs]->table;
  }

  if(m_bHasPixelRepeat && bPixelRepeat)
    {
      plugSetup.VIDn_CTL |= PLUG_CTL__PIXEL_REPEAT;
    }

  if(transparency<=255)
    plugSetup.VIDn_ALP = ((transparency+1)>>1)|PLUG_ALP__ALPHA_DISCARD_AIN;
  else
    plugSetup.VIDn_ALP = PLUG_ALP__ALPHA_OPAQUE|PLUG_ALP__ALPHA_DISCARD_AIN;

  if(m_bColorFillEnabled)
    {
      // The mixer works on 10 bits, hence program 10 bits RGB color filling
      uint32_t red   = TO_10_BITS( GET_RED  (m_ulColorFill) );
      uint32_t green = TO_10_BITS( GET_GREEN(m_ulColorFill) );
      uint32_t blue  = TO_10_BITS( GET_BLUE (m_ulColorFill) );

      PLUG_TRC( TRC_ID_VIDEO_PLUG, "Setting Color Fill 0x%08X, 10 bits RGB=(0x%X,0x%X,0x%X)", m_ulColorFill, red, green, blue);

      plugSetup.VIDn_ALP |= (red << PLUG_ALP__VIDEO_COLOR_FILL_RED_SHIFT) & PLUG_ALP__VIDEO_COLOR_FILL_RED_MASK;

      plugSetup.VIDn_CLF  =   ((green << PLUG_CLF__VIDEO_COLOR_FILL_GREEN_SHIFT) & PLUG_CLF__VIDEO_COLOR_FILL_GREEN_MASK)
                            | ((blue  << PLUG_CLF__VIDEO_COLOR_FILL_BLUE_SHIFT ) & PLUG_CLF__VIDEO_COLOR_FILL_BLUE_MASK );

      plugSetup.VIDn_CTL |= PLUG_CTL__COLOR_FILL_ENABLE;
    }
  else
  {
      plugSetup.VIDn_CLF =  0;
  }

  // The video composition does not support destination colour keying
  if((pFrame->dst.ulFlags & (STM_BUFFER_DST_COLOR_KEY | STM_BUFFER_DST_COLOR_KEY_INV)) != 0)
    return false;

  /*
   * Color Key management.
   */
  if(pColorKeyConfig)
  {
    // If colour keying not required, do nothing.
    if (pColorKeyConfig->enable == 0)
    {
      // Disable CKEY before going on
      plugSetup.VIDn_CTL &= ~(PLUG_CTL__KEY_ENABLE            |
                              PLUG_CTL__KEY_CR_INRANGE_MATCH  |
                              PLUG_CTL__KEY_CR_OUTRANGE_MATCH |
                              PLUG_CTL__KEY_Y_INRANGE_MATCH   |
                              PLUG_CTL__KEY_Y_OUTRANGE_MATCH  |
                              PLUG_CTL__KEY_CB_INRANGE_MATCH  |
                              PLUG_CTL__KEY_CB_OUTRANGE_MATCH);
      plugSetup.VIDn_KEY1 = 0;
      plugSetup.VIDn_KEY2 = 0;
      return true;
    }

    //Get Min Key value
    if (!(pColorKeyConfig->flags & SCKCF_MINVAL)
        || !CDisplayPlane::GetRGBYCbCrKey (ucRCR, ucGY, ucBCB,
                                           pColorKeyConfig->minval,
                                           src_color_fmt,
                                           pColorKeyConfig->format == SCKCVF_RGB))
      {
        PLUG_TRC( TRC_ID_MAIN_INFO, "Min key value not obtained" );
        return false;
      }
    plugSetup.VIDn_KEY1 = (ucBCB | (ucGY<<8) | (ucRCR<<16));

    //Get Max Key value
    if (!(pColorKeyConfig->flags & SCKCF_MAXVAL)
        || !CDisplayPlane::GetRGBYCbCrKey (ucRCR, ucGY, ucBCB,
                                           pColorKeyConfig->maxval,
                                           src_color_fmt,
                                           pColorKeyConfig->format == SCKCVF_RGB))
      {
        PLUG_TRC( TRC_ID_MAIN_INFO, "Max key value not obtained" );
        return false;
      }
    plugSetup.VIDn_KEY2 = (ucBCB | (ucGY<<8) | (ucRCR<<16));

    uint32_t ulCtrl = PLUG_CTL__KEY_ENABLE;

    switch (pColorKeyConfig->r_info) {
    case SCKCCM_DISABLED: break;
    case SCKCCM_ENABLED: ulCtrl |= PLUG_CTL__KEY_CR_INRANGE_MATCH; break;
    case SCKCCM_INVERSE: ulCtrl |= PLUG_CTL__KEY_CR_OUTRANGE_MATCH; break;
    default: return false;
    }

    switch (pColorKeyConfig->g_info) {
    case SCKCCM_DISABLED: break;
    case SCKCCM_ENABLED: ulCtrl |= PLUG_CTL__KEY_Y_INRANGE_MATCH; break;
    case SCKCCM_INVERSE: ulCtrl |= PLUG_CTL__KEY_Y_OUTRANGE_MATCH; break;
    default: return false;
    }

    switch (pColorKeyConfig->b_info) {
    case SCKCCM_DISABLED: break;
    case SCKCCM_ENABLED: ulCtrl |= PLUG_CTL__KEY_CB_INRANGE_MATCH; break;
    case SCKCCM_INVERSE: ulCtrl |= PLUG_CTL__KEY_CB_OUTRANGE_MATCH; break;
    default: return false;
    }
    plugSetup.VIDn_CTL |= ulCtrl;
  }
  else
  {
    // Disable CKEY before going on
    plugSetup.VIDn_CTL &= ~(PLUG_CTL__KEY_ENABLE            |
                            PLUG_CTL__KEY_CR_INRANGE_MATCH  |
                            PLUG_CTL__KEY_CR_OUTRANGE_MATCH |
                            PLUG_CTL__KEY_Y_INRANGE_MATCH   |
                            PLUG_CTL__KEY_Y_OUTRANGE_MATCH  |
                            PLUG_CTL__KEY_CB_INRANGE_MATCH  |
                            PLUG_CTL__KEY_CB_OUTRANGE_MATCH);
    plugSetup.VIDn_KEY1 = 0;
    plugSetup.VIDn_KEY2 = 0;
  }

  return true;
}


bool CVideoPlug::SetControl(stm_display_plane_control_t control, uint32_t value)
{
  PLUG_TRC( TRC_ID_MAIN_INFO, "" );

  switch(control)
    {
    case PLANE_CTRL_BRIGHTNESS:
      if(!m_bEnablePSI || value > BRIGHTNESS_MAX)
        return false;
      m_ulBrightness = value;
      CreatePSISetup();
      break;

    case PLANE_CTRL_SATURATION:
      if(!m_bEnablePSI || value > SATURATION_MAX)
        return false;
      m_ulSaturation = value;
      CreatePSISetup();
      break;

    case PLANE_CTRL_CONTRAST:
      if(!m_bEnablePSI || value > CONTRAST_MAX)
        return false;
      m_ulContrast = value;
      CreatePSISetup();
      break;

    case PLANE_CTRL_TINT:
      if(!m_bEnablePSI || value > TINT_MAX)
        return false;
      m_ulTint = value;
      CreatePSISetup();
      break;

    case PLANE_CTRL_COLOR_FILL_MODE:
      PLUG_TRC( TRC_ID_VIDEO_PLUG, "PLANE_CTRL_COLOR_FILL_MODE %u", value);
      switch(static_cast<stm_plane_mode_t>(value))
        {
          case MANUAL_MODE:
            break;
          default:
            return false;
        }
      break;

    case PLANE_CTRL_COLOR_FILL_STATE:
      PLUG_TRC( TRC_ID_VIDEO_PLUG, "PLANE_CTRL_COLOR_FILL_STATE %u", value);
      m_bColorFillEnabled = value != 0 ? true : false;
      break;

    case PLANE_CTRL_COLOR_FILL_VALUE:
      PLUG_TRC( TRC_ID_VIDEO_PLUG, "PLANE_CTRL_COLOR_FILL_VALUE %u (0x%.8X) (R=%u, G=%u, B=%u)", value, value, GET_RED(value), GET_GREEN(value), GET_BLUE(value));
      m_ulColorFill = value;
      break;

    default:
      return false;
    }

  return true;
}


bool CVideoPlug::GetControl(stm_display_plane_control_t control, uint32_t *value) const
{
  PLUG_TRC( TRC_ID_VIDEO_PLUG, "" );

  switch(control)
    {
    case PLANE_CTRL_BRIGHTNESS:
      if(!m_bEnablePSI)
        return false;
      *value = m_ulBrightness;
      break;
    case PLANE_CTRL_SATURATION:
      if(!m_bEnablePSI)
        return false;
      *value = m_ulSaturation;
      break;
    case PLANE_CTRL_CONTRAST:
      if(!m_bEnablePSI)
        return false;
      *value = m_ulContrast;
      break;
    case PLANE_CTRL_TINT:
      if(!m_bEnablePSI)
        return false;
      *value = m_ulTint;
      break;
    case PLANE_CTRL_COLOR_FILL_MODE:
        *value = MANUAL_MODE;
        break;
      break;
    case PLANE_CTRL_COLOR_FILL_STATE:
      *value = static_cast<uint32_t>(m_bColorFillEnabled);
      break;
    case PLANE_CTRL_COLOR_FILL_VALUE:
      *value = m_ulColorFill;
      break;

    default:
      return false;
    }

  return true;
}

bool CVideoPlug::GetControlRange(stm_display_plane_control_t control_id, stm_display_plane_control_range_t *range)
{
  PLUG_TRC( TRC_ID_MAIN_INFO, "" );

  switch(control_id)
    {
    case PLANE_CTRL_BRIGHTNESS:
      if(!m_bEnablePSI)
        return false;
      range->min_val = BRIGHTNESS_MIN;
      range->max_val = BRIGHTNESS_MAX;
      range->step = 1;
      range->default_val = BRIGHTNESS_DEFAULT;
      break;
    case PLANE_CTRL_SATURATION:
      if(!m_bEnablePSI)
        return false;
      range->min_val = SATURATION_MIN;
      range->max_val = SATURATION_MAX;
      range->step = 1;
      range->default_val = SATURATION_DEFAULT;
      break;
    case PLANE_CTRL_CONTRAST:
      if(!m_bEnablePSI)
        return false;
      range->min_val = CONTRAST_MIN;
      range->max_val = CONTRAST_MAX;
      range->step = 1;
      range->default_val = CONTRAST_DEFAULT;
      break;
    case PLANE_CTRL_TINT:
      if(!m_bEnablePSI)
        return false;
      range->min_val = TINT_MIN;
      range->max_val = TINT_MAX;
      range->step = 1;
      range->default_val = TINT_DEFAULT;
      break;

    default:
        return false;
    }

  return true;
}

void CVideoPlug::CreatePSISetup(void)
{
  signed long brightness = (signed long)m_ulBrightness - 128; // Convert unsigned 8bit to signed 8bit
  signed long tint       = (signed long)(m_ulTint/4) - 32;    // Convert unsigned 8bit to signed 6bit

  PLUG_TRC( TRC_ID_MAIN_INFO, "" );

  m_VIDn_BC   = brightness & PLUG_BRIGHTNESS_MASK;
  m_VIDn_BC  |= (m_ulContrast & PLUG_CONTRAST_MASK) << PLUG_CONTRAST_SHIFT;
  m_VIDn_CSAT = ((m_ulSaturation/4) & PLUG_CSAT_MASK) << PLUG_CSAT_SHIFT; // Convert to unsigned 6bit
  m_VIDn_TINT = (tint & PLUG_TINT_MASK) << PLUG_TINT_SHIFT;
}


void CVideoPlug::WritePlugSetup(const VideoPlugSetup &setup, bool visible)
{
  uint32_t ctl = setup.VIDn_CTL;

  if(m_bEnablePSI)
    {
      ctl |= (PLUG_CTL__PSI_SAT_ENABLE |
              PLUG_CTL__PSI_BC_ENABLE  |
              PLUG_CTL__PSI_TINT_ENABLE);
    }

  WritePLUGReg(PLUG_VPO,  setup.VIDn_VPO);
  WritePLUGReg(PLUG_VPS,  setup.VIDn_VPS);
  if(m_bHasHwInputCrop)
  {
    /* This register exist only on sti8416/stiH418 SoC */
    WritePLUGReg(PLUG_CROP, setup.VIDn_CROP);
  }
  WritePLUGReg(PLUG_ALP,  setup.VIDn_ALP);
  if(!visible)
  {
    if(m_bHasIgnoreOnMixer)
    {
      /* Ignore data coming from this pipe at Mixer level */
      ctl |= (PLUG_CTL__IGNORE_MX1 | PLUG_CTL__IGNORE_MX2);
    }
    else
    {
      /* Make plane fully transparent */
      WritePLUGReg(PLUG_ALP, PLUG_ALP__ALPHA_DISCARD_AIN);
    }
  }
  WritePLUGReg(PLUG_KEY1, setup.VIDn_KEY1);
  WritePLUGReg(PLUG_KEY2, setup.VIDn_KEY2);

  WritePLUGReg(PLUG_CLF, setup.VIDn_CLF);

  if(m_bEnablePSI)
    {
      WritePLUGReg(PLUG_BC,   m_VIDn_BC);
      WritePLUGReg(PLUG_CSAT, m_VIDn_CSAT);
      WritePLUGReg(PLUG_TINT, m_VIDn_TINT);
    }

  for(int i=0; i<PLUG_MPR_REGS_NB; i++)
  {
      WritePLUGReg(PLUG_MPR(i), setup.pVIDn_MPR[i]);
  }

  if(m_bHasGamutConverter)
  {
      for(int i=0; i<PLUG_BT_REGS_NB; i++)
      {
          WritePLUGReg(PLUG_BT(i), setup.pVIDn_BT[i]);
      }
  }

  /* Write control register in last statement */
  WritePLUGReg(PLUG_CTL, ctl);
}
