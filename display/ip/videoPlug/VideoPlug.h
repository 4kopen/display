/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2013-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015.09.11
***************************************************************************/

#ifndef _VIDEOPLUG_H
#define _VIDEOPLUG_H

#include <display/ip/stmviewport.h>
#include <display/generic/DisplayDevice.h>

struct VideoPlugSetup
{
  uint32_t        VIDn_CTL;
  uint32_t        VIDn_VPO;
  uint32_t        VIDn_VPS;
  uint32_t        VIDn_CROP;
  uint32_t        VIDn_ALP;
  uint32_t        VIDn_CLF;
  uint32_t        VIDn_KEY1;
  uint32_t        VIDn_KEY2;
  const uint32_t* pVIDn_MPR;
  const uint32_t* pVIDn_BT;
};

struct VideoPlugCrop
{
  uint16_t        left;
  uint16_t        right;
};

class CVideoPlug
{
 public:
  CVideoPlug(CDisplayDevice* pDev,
             uint32_t        plugOffset,
             bool            bEnablePSI,
             bool            bHasHwInputCrop = false,
             bool            bHasIgnoreOnMixer = true,
             bool            bHasPixelRepeat = false,
             bool            bHasGamutConverter = false);

  virtual ~CVideoPlug(void);

  void SetPlaneName(const char* planeName);

  bool CreatePlugSetup(VideoPlugSetup                  &plugSetup,
                       const stm_display_buffer_t    * const pFrame,
                       const stm_viewport_t          &viewport,
                       stm_pixel_format_t            src_color_fmt,
                       uint32_t                      transparency,
                       const stm_color_key_config_t  * const pColorKeyConfig = 0,
                       const VideoPlugCrop           * const pCrop = 0,
                       bool                          bPixelRepeat = false,
                       stm_ycbcr_colorspace_t        outputColorSpace = STM_YCBCR_COLORSPACE_709);

  void WritePlugSetup(const VideoPlugSetup &, bool);

  bool SetControl(stm_display_plane_control_t, uint32_t);
  bool GetControl(stm_display_plane_control_t, uint32_t *) const;
  bool GetControlRange(stm_display_plane_control_t control_id, stm_display_plane_control_range_t *range);

 protected:
  uint32_t*   m_plugBaseAddr;
  const char* m_planeName;

  bool     m_bEnablePSI;
  bool     m_bColorFillEnabled;

  uint32_t m_ulBrightness;
  uint32_t m_ulSaturation;
  uint32_t m_ulContrast;
  uint32_t m_ulTint;
  uint32_t m_ulColorFill;

  uint32_t m_VIDn_CSAT;
  uint32_t m_VIDn_TINT;
  uint32_t m_VIDn_BC;

  /*
   * Hardware capabilities.
   */
  bool m_bHasHwInputCrop;
  bool m_bHasIgnoreOnMixer;
  bool m_bHasPixelRepeat;
  bool m_bHasGamutConverter;

  void  CreatePSISetup(void);

  void  WritePLUGReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_plugBaseAddr, reg, val); }
};

#endif // _VIDEOPLUG_H
