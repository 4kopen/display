# Classes for the HDMI frame formatter management, HDMI phy and infoframe
# configuration.

ifneq ($(CONFIG_HDMI_TX),)
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/hdmi/,         \
                   stmhdmi.cpp                                       \
                   stmiframemanager.cpp)

ifneq ($(CONFIG_HDMI_DMA_IFRAME),)
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/hdmi/,         \
                   stmdmaiframes.cpp)
endif

ifneq ($(CONFIG_HDMI_V29_IFRAME),)
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/hdmi/,         \
                   stmv29iframes.cpp)
endif

ifneq ($(CONFIG_HDMITX3G4_PHY),)
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/hdmi/,         \
                   stmhdmitx3g4_c28_phy.cpp)
endif

ifneq ($(CONFIG_HDMITX6G0_PHY),)
STM_SRC_FILES += $(addprefix $(SRC_TOPDIR)/display/ip/hdmi/,         \
                   stmhdmitx6g0_c28_phy.cpp)
endif

endif

