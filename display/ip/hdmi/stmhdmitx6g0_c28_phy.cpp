/***********************************************************************
 *
 * File: display/ip/hdmi/stmhdmitx6g0_c28_phy.cpp
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/generic/DisplayDevice.h>

#include <display/ip/hdmi/stmhdmiregs.h> // For Phy (P/D)LL Status which appears in the HDMI status

#include "stmhdmitx6g0_c28_phy.h"

#define STM_HDMI_SRZ_CFG            0x504
#define STM_HDMI_SRZ_PWR_CFG        0x508
#define STM_HDMI_SRZ_PLL_CFG        0x510
#define STM_HDMI_SRZ_STR_1          0x518
#define STM_HDMI_SRZ_STR_2          0x51C
#define STM_HDMI_SRZ_CALCODE_EXT    0x530
#define STM_HDMI_SRZ_TX_RSVR_BITS   0x560

/* *********************************** */
/* register : STM_HDMI_SRZ_CFG         */
/* *********************************** */

#define SRZ_CFG_EN                          (1L<<0)

#define SRZ_CFG_EN_PE_C0_SHIFT              (4)
#define SRZ_CFG_EN_PE_C0_MASK               (0x7<<SRZ_CFG_EN_PE_C0_SHIFT)

#define SRZ_CFG_EN_PE_C1_SHIFT              (8)
#define SRZ_CFG_EN_PE_C1_MASK               (0x7<<SRZ_CFG_EN_PE_C1_SHIFT)

#define SRZ_CFG_EN_PE_C2_SHIFT              (12)
#define SRZ_CFG_EN_PE_C2_MASK               (0x7<<SRZ_CFG_EN_PE_C2_SHIFT)

#define SRZ_CFG_EXTERNAL_DATA               (1L<<16)
#define SRZ_CFG_RBIAS_EXT                   (1L<<17)
#define SRZ_CFG_EN_SINK_TERM_DETECTION      (1L<<18)

#define SRZ_CFG_ISNKCTRL_SHIFT              (20)
#define SRZ_CFG_ISNKCTRL_MASK               (0x3<<SRZ_CFG_ISNKCTRL_SHIFT)

#define SRZ_CFG_EN_SRC_TERMINATION_SHIFT    (24)
#define SRZ_CFG_EN_SRC_TERMINATION_VAL_BLW_165MHZ             (0x0) /* to be also used for 165MHz */
#define SRZ_CFG_EN_SRC_TERMINATION_VAL_BWN_165MHZ_340MHZ      (0x2) /* to be also used for 340MHz */
#define SRZ_CFG_EN_SRC_TERMINATION_VAL_ABV_340MHZ             (0x3)

#define SRZ_CFG_CKCH_LOWSW_EN_SHIFT          (29)
#define SRZ_CFG_CKCH_LOWSW_EN_VAL_500MV      (0x0) /* for below or equal 3.4 Gbps */
#define SRZ_CFG_CKCH_LOWSW_EN_VAL_300MV      (0x1) /* for above 3.4 Gbps */

#define SRZ_CFG_CKBY10_OR_40_SHIFT             (30)
#define SRZ_CFG_CKBY10_OR_40_VAL_DIV_BY_10     (0x0) /* for below or equal 3.4 Gbps */
#define SRZ_CFG_CKBY10_OR_40_VAL_DIV_BY_40     (0x1) /* for above 3.4 Gbps */

#define SRZ_CFG_DATA20BIT10BIT_SHIFT           (31)
#define SRZ_CFG_DATA20BIT10BIT_VAL_10_BPC      (0x0)
#define SRZ_CFG_DATA20BIT10BIT_VAL_20_BPC      (0x1)


/* *********************************** */
/* register : STM_HDMI_SRZ_PWR_CFG     */
/* *********************************** */

#define SRZ_PWR_CFG_EXT_DATACK_EN              (1L<<5)
#define SRZ_PWR_CFG_EXT_DATACK                 (1L<<6)
#define SRZ_PWR_CFG_PEBYPASS_ENABLE_N          (1L<<19)


/* *********************************** */
/* register : STM_HDMI_SRZ_PLL_CFG     */
/* *********************************** */

#define PLL_CFG_EN         (1L<<0)
#define PLL_CFG_NDIV_SHIFT (8)
#define PLL_CFG_IDF_SHIFT  (16)
#define PLL_CFG_ODF_SHIFT  (24)

#define ODF_DIV_1          (0)
#define ODF_DIV_2          (1)
#define ODF_DIV_4          (2)
#define ODF_DIV_8          (3)
#define ODF_DIV_16         (4)

#define STM_HDMI_IDF_1     (1)
#define STM_HDMI_IDF_2     (2)
#define STM_HDMI_IDF_4     (4)
#define STM_HDMI_IDF_8     (8)
#define STM_HDMI_IDF_16    (16)

#define STM_HDMI_NDIV_10   (10)
#define STM_HDMI_NDIV_15   (15)
#define STM_HDMI_NDIV_20   (20)
#define STM_HDMI_NDIV_25   (25)
#define STM_HDMI_NDIV_30   (30)
#define STM_HDMI_NDIV_40   (40)
#define STM_HDMI_NDIV_50   (50)

#define STM_HDMI_MULT_5     (500)
#define STM_HDMI_MULT_6_25  (625)
#define STM_HDMI_MULT_7_5   (750)
#define STM_HDMI_MULT_10    (1000)
#define STM_HDMI_MULT_12_5  (1250)
#define STM_HDMI_MULT_15    (1500)
#define STM_HDMI_MULT_20    (2000)
#define STM_HDMI_MULT_25    (2500)
#define STM_HDMI_MULT_30    (3000)
#define STM_HDMI_MULT_40    (4000)
#define STM_HDMI_MULT_50    (5000)
#define STM_HDMI_MULT_60    (6000)
#define STM_HDMI_MULT_80    (8000)


/* *********************************** */
/* register : STM_HDMI_SRZ_STR_1       */
/* *********************************** */

#define SRZ_STR1_PEXC0       (0)
#define SRZ_STR1_MSK_PEXC0   (0xFFF<<SRZ_STR1_PEXC0)

#define SRZ_STR1_PEXC1       (16)
#define SRZ_STR1_MSK_PEXC1   (0xFFF<<SRZ_STR1_PEXC1)


/* *********************************** */
/* register : STM_HDMI_SRZ_STR_2       */
/* *********************************** */

#define SRZ_STR2_PEXC2       (0)
#define SRZ_STR2_MSK_PEXC2   (0xFFF<<SRZ_STR2_PEXC2)


/* *********************************** */
/* register : STM_HDMI_SRZ_CALCODE_EXT */
/* *********************************** */

#define SRZ_CALCODE_EXT_SHIFT       (0)
#define SRZ_CALCODE_EXT_MASK        (0xFFFFFFF<<SRZ_CALCODE_EXT_SHIFT)


/* ************************************ */
/* register : STM_HDMI_SRZ_TX_RSVR_BITS */
/* ************************************ */

#define SRZ_TX_RSVR_BITS_BELOW_340MHZ       (0)
#define SRZ_TX_RSVR_BITS_ABOVE_340MHZ_CUT_1_0       (0x01000000)
#define SRZ_TX_RSVR_BITS_ABOVE_340MHZ_CUT_1_X       (0x0800000)


/* *********************************** */
/* Configuration                       */
/* *********************************** */


/* Config 0 => STM_HDMI_SRZ_CFG */
#define STM_HDMI_SRZ_CONFIG_0_MASK  ( SRZ_CFG_EN_PE_C0_MASK| \
	                                    SRZ_CFG_EN_PE_C1_MASK| \
	                                    SRZ_CFG_EN_PE_C2_MASK| \
	                                    SRZ_CFG_ISNKCTRL_MASK)
/* Config 1 => STM_HDMI_SRZ_STR_1 */
#define STM_HDMI_SRZ_CONFIG_1_MASK  (SRZ_STR1_MSK_PEXC0 | SRZ_STR1_MSK_PEXC1)

/* Config 2 => STM_HDMI_SRZ_STR_2 */
#define STM_HDMI_SRZ_CONFIG_2_MASK  (SRZ_STR2_MSK_PEXC2)

/* Config 3 => STM_HDMI_SRZ_CALCODE_EXT */
#define STM_HDMI_SRZ_CONFIG_3_MASK  (SRZ_CALCODE_EXT_MASK)


#define STM_HDMI_THOLD_CLK_600MHZ  (600000000)
#define STM_HDMI_THOLD_CLK_340MHZ  (340000000)
#define STM_HDMI_THOLD_CLK_165MHZ  (165000000)


/*
 * Functional specification recommended values
 */
 struct plldividers_s
{
  uint32_t min;
  uint32_t max;
  uint32_t idf;
  uint32_t odf;
};

static struct plldividers_s plldividers[] = {
    { 0,           37500000,  STM_HDMI_IDF_1,  ODF_DIV_16 },
    { 37500000,    75000000,  STM_HDMI_IDF_2,  ODF_DIV_8  },
    { 75000000,   150000000,  STM_HDMI_IDF_4,  ODF_DIV_4  },
    {150000000,   300000000,  STM_HDMI_IDF_8,  ODF_DIV_2  },
    {300000000,   600000000, STM_HDMI_IDF_16,  ODF_DIV_1  }
};


 struct specificplldividers_s
{
  uint32_t px_pllin_clk;
  uint32_t req_mult;
  uint32_t idf;
  uint32_t ndiv;
  uint32_t odf;
};

static struct specificplldividers_s specificplldividers[] = {
    { 13500000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_40,  ODF_DIV_16 },
    { 13500000,  STM_HDMI_MULT_25  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_50,  ODF_DIV_16 },
    { 13500000,  STM_HDMI_MULT_30  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_30,  ODF_DIV_8 },
    { 13500000,  STM_HDMI_MULT_40  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_40,  ODF_DIV_8 },
    { 13500000,  STM_HDMI_MULT_50  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_50,  ODF_DIV_8 },
    { 13500000,  STM_HDMI_MULT_60  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_30,  ODF_DIV_4 },
    { 13500000,  STM_HDMI_MULT_80  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_40,  ODF_DIV_4 },
    { 16875000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_40,  ODF_DIV_16 },
    { 16875000,  STM_HDMI_MULT_40  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_40,  ODF_DIV_8 },
    { 20250000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_20,  ODF_DIV_8 },
    { 20250000,  STM_HDMI_MULT_40  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_20,  ODF_DIV_4 },
    { 21760000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_20,  ODF_DIV_8 },
    { 25200000,  STM_HDMI_MULT_15  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_15,  ODF_DIV_8 },
    { 25200000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_20,  ODF_DIV_8 },
    { 27000000,  STM_HDMI_MULT_15  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_15,  ODF_DIV_8 },
    { 27000000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_20,  ODF_DIV_8 },
    { 27000000,  STM_HDMI_MULT_25  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_25,  ODF_DIV_8 },
    { 27000000,  STM_HDMI_MULT_30  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_15,  ODF_DIV_4 },
    { 27000000,  STM_HDMI_MULT_50  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_25,  ODF_DIV_4 },
    { 27000000,  STM_HDMI_MULT_60  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_15,  ODF_DIV_2 },
    { 27000000,  STM_HDMI_MULT_80  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_20,  ODF_DIV_2 },
    { 33750000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_20,  ODF_DIV_8 },
    { 40500000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_10,  ODF_DIV_4 },
    { 40500000,  STM_HDMI_MULT_40  , STM_HDMI_IDF_1,  STM_HDMI_NDIV_10,  ODF_DIV_2 },
    { 54000000,  STM_HDMI_MULT_15  , STM_HDMI_IDF_2,  STM_HDMI_NDIV_15,  ODF_DIV_4 },
    { 54000000,  STM_HDMI_MULT_30  , STM_HDMI_IDF_2,  STM_HDMI_NDIV_15,  ODF_DIV_2 },
    { 59400000,  STM_HDMI_MULT_15  , STM_HDMI_IDF_2,  STM_HDMI_NDIV_15,  ODF_DIV_4 },
    { 72000000,  STM_HDMI_MULT_12_5, STM_HDMI_IDF_4,  STM_HDMI_NDIV_25,  ODF_DIV_4 },
    { 72000000,  STM_HDMI_MULT_15  , STM_HDMI_IDF_4,  STM_HDMI_NDIV_30,  ODF_DIV_4 },
    { 74250000,  STM_HDMI_MULT_12_5, STM_HDMI_IDF_4,  STM_HDMI_NDIV_25,  ODF_DIV_4 },
    { 74250000,  STM_HDMI_MULT_15  , STM_HDMI_IDF_4,  STM_HDMI_NDIV_30,  ODF_DIV_4 },
    { 74250000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_4,  STM_HDMI_NDIV_40,  ODF_DIV_4 },
    { 81000000,  STM_HDMI_MULT_20  , STM_HDMI_IDF_2,  STM_HDMI_NDIV_10,  ODF_DIV_2 },
    { 108000000, STM_HDMI_MULT_15  , STM_HDMI_IDF_4,  STM_HDMI_NDIV_15,  ODF_DIV_2 },
    { 118800000, STM_HDMI_MULT_15  , STM_HDMI_IDF_4,  STM_HDMI_NDIV_15,  ODF_DIV_2 },
    { 148500000, STM_HDMI_MULT_12_5, STM_HDMI_IDF_8,  STM_HDMI_NDIV_25,  ODF_DIV_2 },
    { 148500000, STM_HDMI_MULT_15  , STM_HDMI_IDF_8,  STM_HDMI_NDIV_30,  ODF_DIV_2 },
    { 148500000, STM_HDMI_MULT_20  , STM_HDMI_IDF_8,  STM_HDMI_NDIV_40,  ODF_DIV_2 },
    { 148500000, STM_HDMI_MULT_25  , STM_HDMI_IDF_8,  STM_HDMI_NDIV_25,  ODF_DIV_1 },
    { 148500000, STM_HDMI_MULT_30  , STM_HDMI_IDF_8,  STM_HDMI_NDIV_30,  ODF_DIV_1 },
    { 148500000, STM_HDMI_MULT_40  , STM_HDMI_IDF_8,  STM_HDMI_NDIV_40,  ODF_DIV_1 },
    { 150000000, STM_HDMI_MULT_50  , STM_HDMI_IDF_4,  STM_HDMI_NDIV_20,  ODF_DIV_1 },
    { 297000000, STM_HDMI_MULT_12_5, STM_HDMI_IDF_16, STM_HDMI_NDIV_25,  ODF_DIV_1 },
    { 297000000, STM_HDMI_MULT_15  , STM_HDMI_IDF_16, STM_HDMI_NDIV_30,  ODF_DIV_1 },
    { 297000000, STM_HDMI_MULT_20  , STM_HDMI_IDF_16, STM_HDMI_NDIV_40,  ODF_DIV_1 },
    { 300000000, STM_HDMI_MULT_20  , STM_HDMI_IDF_16, STM_HDMI_NDIV_40,  ODF_DIV_1 },
    { 594000000, STM_HDMI_MULT_5   , STM_HDMI_IDF_16, STM_HDMI_NDIV_20,  ODF_DIV_2 },
    { 594000000, STM_HDMI_MULT_6_25, STM_HDMI_IDF_16, STM_HDMI_NDIV_25,  ODF_DIV_2 },
    { 594000000, STM_HDMI_MULT_7_5 , STM_HDMI_IDF_16, STM_HDMI_NDIV_30,  ODF_DIV_2 }
};




CSTmHDMITx6G0_C28_Phy::CSTmHDMITx6G0_C28_Phy(CDisplayDevice *pDev,
                                                     uint32_t phyoffset): CSTmHDMIPhy()
{
  TRCIN( TRC_ID_HDMI, "" );

  m_pDevRegs = (uint32_t *)pDev->GetCtrlRegisterBase();
  m_uPhyOffset = phyoffset;

  TRCOUT( TRC_ID_HDMI, "" );
}


CSTmHDMITx6G0_C28_Phy::~CSTmHDMITx6G0_C28_Phy()
{
}


bool CSTmHDMITx6G0_C28_Phy::Start(const stm_display_mode_t *const pModeLine,
                                      uint32_t outputFormat)
{
  TRCIN( TRC_ID_HDMI, "" );

  /*
   * We have arranged in the chip specific PreConfiguration for the input
   * clock to the PHY PLL to be the normal TMDS clock for 24bit colour, taking
   * into account pixel repetition. There is an exception for 420 mode where
   * PHY PLL is half normal TMDS clock for 24bit colour (to avoid x6.25 for DC).
   * We have been passed a modeline where the pixelclock parameter is in fact that
   * TMDS clock in the case of the pixel doubled/quadded modes.
   */

  uint32_t ckpxpll = pModeLine->mode_timing.pixel_clock_freq;
  uint32_t tmdsck;
  uint32_t idf;
  uint32_t odf;
  uint32_t pixrep=0;
  uint32_t pllctrl = 0;
  uint32_t major=0;
  uint32_t minor=0;
  unsigned i;
  unsigned req_mult_by_100=0;

  bool bFoundPLLDivides = false;

  vibe_os_get_chip_version(&major,&minor);

  if(outputFormat & STM_VIDEO_OUT_420)
  {
    ckpxpll = (ckpxpll>>1); /* PX_PLLIN_CLK is half pixel clock */
  }

  switch(pModeLine->mode_params.flags & STM_MODE_FLAGS_HDMI_PIXELREP_MASK)
  {
      case STM_MODE_FLAGS_HDMI_PIXELREP_2X:
        ckpxpll /= 2;
        pixrep = 1;
        req_mult_by_100 = STM_HDMI_MULT_20;
        break;
      case STM_MODE_FLAGS_HDMI_PIXELREP_4X:
        ckpxpll /= 4;
        pixrep = 2;
        req_mult_by_100 = STM_HDMI_MULT_40;
        break;
      default:
        pixrep = 0;
        req_mult_by_100 = STM_HDMI_MULT_10;
        break;
  }

  for(i=0;i<N_ELEMENTS(plldividers);i++)
  {
    if(ckpxpll >= plldividers[i].min && ckpxpll < plldividers[i].max)
    {
      idf = plldividers[i].idf;
      odf = plldividers[i].odf-pixrep;
      bFoundPLLDivides = true;
      break;
    }
  }

  if(!bFoundPLLDivides)
  {
    TRC( TRC_ID_HDMI, "input TMDS clock speed (%u) not supported",ckpxpll );
    return false;
  }

  switch(outputFormat & STM_VIDEO_OUT_DEPTH_MASK)
  {
    case STM_VIDEO_OUT_30BIT:
      tmdsck = (ckpxpll * 5) / 4; // 1.25x
      req_mult_by_100 = (req_mult_by_100 * 5) / 4;
      pllctrl |= STM_HDMI_NDIV_25 << PLL_CFG_NDIV_SHIFT; // (50*4)/16 = x12.5
      TRC( TRC_ID_HDMI, "Selected 30bit colour" );
      break;
    case STM_VIDEO_OUT_36BIT:
      tmdsck = (ckpxpll * 3) / 2; // 1.5x
      req_mult_by_100 = (req_mult_by_100 * 3) / 2;
      pllctrl |= STM_HDMI_NDIV_30 << PLL_CFG_NDIV_SHIFT; // (60*4)/16 = x15
      TRC( TRC_ID_HDMI, "Selected 36bit colour" );
      break;
    case STM_VIDEO_OUT_48BIT:
      tmdsck = ckpxpll * 2;
      req_mult_by_100 = req_mult_by_100 * 2;
      pllctrl |= STM_HDMI_NDIV_20 << PLL_CFG_NDIV_SHIFT;
      if(odf > 0)
      {
        odf--; // reduce the output divide by a factor of 2
      }
      else
      {
        TRC( TRC_ID_HDMI, "Input clock out of range for 48bit color" );
        return false;
      }

      TRC( TRC_ID_HDMI, "Selected 48bit colour" );
      break;
    default:
    case STM_VIDEO_OUT_24BIT:
      tmdsck = ckpxpll;
      pllctrl |= STM_HDMI_NDIV_20 << PLL_CFG_NDIV_SHIFT;
      TRC( TRC_ID_HDMI, "Selected 24bit colour" );
      break;
  }


  if(tmdsck > STM_HDMI_THOLD_CLK_600MHZ)
  {
    TRC( TRC_ID_HDMI, "Output TMDS clock (%u) out of range",tmdsck );
    return false;
  }

  /* check if combination is in the table of specific configurations */
  for(i=0;i<N_ELEMENTS(specificplldividers);i++)
  {
    if((ckpxpll==specificplldividers[i].px_pllin_clk) && (req_mult_by_100==specificplldividers[i].req_mult))
    {
      pllctrl = (specificplldividers[i].ndiv) << PLL_CFG_NDIV_SHIFT ;
      idf = specificplldividers[i].idf;
      odf = specificplldividers[i].odf;
      break;
    }
    if (ckpxpll<specificplldividers[i].px_pllin_clk)
      break;
  }

  pllctrl |= idf << PLL_CFG_IDF_SHIFT;
  pllctrl |= odf << PLL_CFG_ODF_SHIFT;

  uint32_t cfg = (SRZ_CFG_EN                       |
                  SRZ_CFG_EXTERNAL_DATA            |
                  SRZ_CFG_RBIAS_EXT                |
                  SRZ_CFG_EN_SINK_TERM_DETECTION   |
                  (SRZ_CFG_DATA20BIT10BIT_VAL_10_BPC<<SRZ_CFG_DATA20BIT10BIT_SHIFT));
  uint32_t tx_rsvr_bits = SRZ_TX_RSVR_BITS_BELOW_340MHZ;


  if (tmdsck > STM_HDMI_THOLD_CLK_340MHZ)
  {
    cfg |= (SRZ_CFG_EN_SRC_TERMINATION_VAL_ABV_340MHZ << SRZ_CFG_EN_SRC_TERMINATION_SHIFT) |
           (SRZ_CFG_CKCH_LOWSW_EN_VAL_300MV << SRZ_CFG_CKCH_LOWSW_EN_SHIFT) |
           (SRZ_CFG_CKBY10_OR_40_VAL_DIV_BY_40 << SRZ_CFG_CKBY10_OR_40_SHIFT);
    if( (major==1) && (minor==0))
    {
      tx_rsvr_bits = SRZ_TX_RSVR_BITS_ABOVE_340MHZ_CUT_1_0;
    }
    else
    {
      tx_rsvr_bits = SRZ_TX_RSVR_BITS_ABOVE_340MHZ_CUT_1_X;
    }
  }
  else if (tmdsck > STM_HDMI_THOLD_CLK_165MHZ)
  {
    cfg |= (SRZ_CFG_EN_SRC_TERMINATION_VAL_BWN_165MHZ_340MHZ << SRZ_CFG_EN_SRC_TERMINATION_SHIFT) |
         (SRZ_CFG_CKCH_LOWSW_EN_VAL_500MV << SRZ_CFG_CKCH_LOWSW_EN_SHIFT) |
         (SRZ_CFG_CKBY10_OR_40_VAL_DIV_BY_10 << SRZ_CFG_CKBY10_OR_40_SHIFT);
  }
  else
  {
    cfg |= (SRZ_CFG_EN_SRC_TERMINATION_VAL_BLW_165MHZ << SRZ_CFG_EN_SRC_TERMINATION_SHIFT) |
           (SRZ_CFG_CKCH_LOWSW_EN_VAL_500MV << SRZ_CFG_CKCH_LOWSW_EN_SHIFT) |
           (SRZ_CFG_CKBY10_OR_40_VAL_DIV_BY_10 << SRZ_CFG_CKBY10_OR_40_SHIFT);
  }

  /*
   * To configure the source termination and pre-emphasis appropriately for
   * different high speed TMDS clock frequencies a phy configuration
   * table must be provided, tailored to the SoC and board combination.
   */
  if(m_pPHYConfig != 0)
  {
    i = 0;
    while(!((m_pPHYConfig[i].min_tmds_freq == 0) && (m_pPHYConfig[i].max_tmds_freq == 0)))
    {
      if((m_pPHYConfig[i].min_tmds_freq <= tmdsck) && (m_pPHYConfig[i].max_tmds_freq >= tmdsck))
      {
        /*
         * Or in the externally provided configuration bits, ensuring the bits
         * internally set are not changed.
         */
        cfg |= (m_pPHYConfig[i].config[0] & STM_HDMI_SRZ_CONFIG_0_MASK);
        WritePhyReg(STM_HDMI_SRZ_CFG, cfg);
        WritePhyReg(STM_HDMI_SRZ_PWR_CFG, 0);
        WritePhyReg(STM_HDMI_SRZ_STR_1, m_pPHYConfig[i].config[1] & STM_HDMI_SRZ_CONFIG_1_MASK);
        WritePhyReg(STM_HDMI_SRZ_STR_2, m_pPHYConfig[i].config[2] & STM_HDMI_SRZ_CONFIG_2_MASK);
        WritePhyReg(STM_HDMI_SRZ_CALCODE_EXT, m_pPHYConfig[i].config[3] & STM_HDMI_SRZ_CONFIG_3_MASK);
        WritePhyReg(STM_HDMI_SRZ_TX_RSVR_BITS, tx_rsvr_bits);

        TRC( TRC_ID_HDMI, "Setting serializer CFG:0x%08x PWR_CFG:0x%08x STR1:0x%08x STR2:0x%08x CALCODE_EXT:0x%08x TX_RSVR_BITS:0x%08x",
                             cfg,
                             0,
                             m_pPHYConfig[i].config[1] & STM_HDMI_SRZ_CONFIG_1_MASK,
                             m_pPHYConfig[i].config[2] & STM_HDMI_SRZ_CONFIG_2_MASK,
                             m_pPHYConfig[i].config[3] & STM_HDMI_SRZ_CONFIG_3_MASK,
                             tx_rsvr_bits);
        TRCOUT(TRC_ID_HDMI, "" );
        /*
         * Configure and power up the PHY PLL
         */
        TRC( TRC_ID_HDMI, "pllctrl = 0x%08x", pllctrl );
        WritePhyReg(STM_HDMI_SRZ_PLL_CFG, (pllctrl | PLL_CFG_EN));

        while((ReadPhyReg(STM_HDMI_STA) & STM_HDMI_STA_PLL_LCK) == 0);

        if(!(major==1 && minor==0))
        {
          ApplyReset();
        }
        TRC( TRC_ID_HDMI, "got PHY PLL Lock" );
        return true;
      }
      i++;
    }
  }

  /*
   * Default, power up the serializer with no pre-emphasis or
   * output swing correction
   */
  WritePhyReg(STM_HDMI_SRZ_CFG, cfg);
  WritePhyReg(STM_HDMI_SRZ_PWR_CFG, 0);
  WritePhyReg(STM_HDMI_SRZ_STR_1, 0);
  WritePhyReg(STM_HDMI_SRZ_STR_2, 0);
  WritePhyReg(STM_HDMI_SRZ_CALCODE_EXT, 0);
  WritePhyReg(STM_HDMI_SRZ_TX_RSVR_BITS, 0);

  TRCOUT( TRC_ID_HDMI, "" );
  return true;
}

void CSTmHDMITx6G0_C28_Phy::SetPll( uint32_t pllctrl)
{
  TRCIN( TRC_ID_HDMI, "" );

  WritePhyReg(STM_HDMI_SRZ_PLL_CFG, (pllctrl ));
  WritePhyReg(STM_HDMI_SRZ_PLL_CFG, (pllctrl | PLL_CFG_EN));

  TRCOUT( TRC_ID_HDMI, "" );

}

void CSTmHDMITx6G0_C28_Phy::Stop(void)
{
  TRCIN( TRC_ID_HDMI, "" );

  WritePhyReg(STM_HDMI_SRZ_CFG, (SRZ_CFG_EN_SINK_TERM_DETECTION));

  WritePhyReg(STM_HDMI_SRZ_PLL_CFG, 0);
  while((ReadPhyReg(STM_HDMI_STA) & STM_HDMI_STA_PLL_LCK) != 0);

  TRCOUT( TRC_ID_HDMI, "" );
}
