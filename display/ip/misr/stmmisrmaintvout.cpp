/***********************************************************************
 *
 * File: display/ip/misr/stmmisrmaintvout.cpp
 * Copyright (c) 2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/
#include <stm_display.h>
#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplayPlane.h>
#include <display/generic/DisplayDevice.h>
#include <display/ip/displaytiming/stmvtg.h>
#include <display/ip/stmmisrviewport.h>

#include "stmmisrmaintvout.h"


//////////////////////////////////////////////////////////////////////////////
//
// HD, SD Progressive and SD Interlaced MISR Output on main HD Dacs
//
CSTmMisrMainTVOut::CSTmMisrMainTVOut(CDisplayDevice *pDev,
                                     uint32_t MisrPFCtrlReg,
                                     uint32_t MisrHDCtrlReg,
                                     uint32_t MisrSDCtrlReg,
                                     uint32_t MisrDVOCtrlReg): CSTmMisrTVOut(pDev, MisrPFCtrlReg, MisrHDCtrlReg, MisrSDCtrlReg, MisrDVOCtrlReg)
{
  TRCIN( TRC_ID_MAIN_INFO, "" );
  TRCOUT( TRC_ID_MAIN_INFO, "" );
}


CSTmMisrMainTVOut::~CSTmMisrMainTVOut() {}

void CSTmMisrMainTVOut::ReadMisrSigns(const stm_time64_t LastVTGEvtTime, uint32_t  LastVTGEvt, const uint32_t format)
{
  if((m_MisrHD.isMisrCaptureStarted)||(m_MisrSD.isMisrCaptureStarted)||(m_MisrPF.isMisrCaptureStarted)||(m_MisrDVO.isMisrCaptureStarted))
  {
    m_MisrData.LastVsyncTime       = LastVTGEvtTime;
    m_MisrData.VTGEvt              = LastVTGEvt;

    if(m_MisrHD.isMisrCaptureStarted && (format & (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_RGB)))
      ReadMisrSign(&m_MisrHD, m_MisrHDCtrlReg);

    if(m_MisrSD.isMisrCaptureStarted && (format & (STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC)))
      ReadMisrSign(&m_MisrSD, m_MisrSDCtrlReg);

    if(m_MisrPF.isMisrCaptureStarted)
      ReadMisrSign(&m_MisrPF, m_MisrPFCtrlReg);

    if(m_MisrDVO.isMisrCaptureStarted)
        ReadMisrSign(&m_MisrDVO, m_MisrDVOCtrlReg);
  }
}

void CSTmMisrMainTVOut::UpdateMisrControlValue(const stm_display_mode_t *pCurrentMode, const uint32_t format)
{
  UpdateMisrCtrl(&m_MisrPF, pCurrentMode);
  UpdateMisrCtrl(&m_MisrDVO, pCurrentMode);
  
  if(format & (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_RGB))
    UpdateMisrCtrl(&m_MisrHD, pCurrentMode);

  if(format & (STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC))
    UpdateMisrCtrl(&m_MisrSD, pCurrentMode);
}

TuningResults CSTmMisrMainTVOut::SetMisrControlValue(uint16_t service, void *inputList, const stm_display_mode_t *pCurrentMode)
{
  /*This function will be updated after test module in enhanced to pass more parameters than just service*/
  SetTuningInputData_t *input = (SetTuningInputData_t *)inputList;
  static char misr_pf[]     = "MISR_MAIN_PF";
  static char misr_hd_out[] = "MISR_HD_OUT";
  static char misr_main_hdout[] = "MISR_MAIN_HDOUT";
  static char misr_main_sdout[] = "MISR_MAIN_SDOUT";
  static char misr_dvo[] = "MISR_DVO";

  TRC( TRC_ID_UNCLASSIFIED, "for service: %x", service );

  if(pCurrentMode == NULL)
  {
    return TUNING_SERVICE_NOT_SUPPORTED;
  }

  if(vibe_os_memcmp((void*)input->ServiceStr, (void*) misr_pf, sizeof(misr_pf))==0)
  {
    TRC( TRC_ID_UNCLASSIFIED, "service MISR_MAIN_PF" );
    return (SetMisrCtrl(&m_MisrPF, m_MisrPFCtrlReg, input, pCurrentMode));
  }

  if((vibe_os_memcmp((void*)input->ServiceStr, (void*) misr_hd_out, sizeof(misr_hd_out))==0)
  || (vibe_os_memcmp((void*)input->ServiceStr, (void*) misr_main_hdout, sizeof(misr_main_hdout))==0))
  {
    TRC( TRC_ID_UNCLASSIFIED, "service MISR_MAIN_HDOUT" );
    return (SetMisrCtrl(&m_MisrHD, m_MisrHDCtrlReg, input, pCurrentMode));
  }

  if(vibe_os_memcmp((void*)input->ServiceStr, (void*) misr_main_sdout, sizeof(misr_main_sdout) ) == 0)
  {
    TRC( TRC_ID_UNCLASSIFIED, "service MISR_MAIN_SDOUT" );
    return (SetMisrCtrl(&m_MisrSD, m_MisrSDCtrlReg, input, pCurrentMode));
  }
  
  if(vibe_os_memcmp((void*)input->ServiceStr, (void*) misr_dvo, sizeof(misr_dvo) ) == 0)
  {
    TRC( TRC_ID_UNCLASSIFIED, "service MISR_DVO" );
    return (SetMisrCtrl(&m_MisrDVO, m_MisrDVOCtrlReg, input, pCurrentMode));
  }

  /*We could not find valid service so far, print error and return false*/
  TRC( TRC_ID_UNCLASSIFIED, "Invalid Service: %s", input->ServiceStr );
  return TUNING_INVALID_PARAMETER;
}

TuningResults CSTmMisrMainTVOut::GetMisrCapability(void *outputList, uint32_t outputListSize)
{
    char *string = (char *)outputList;
    uint32_t lengthOfString = outputListSize;
    if(string != NULL)
    {
        lengthOfString = vibe_os_snprintf(string, outputListSize, "MISR_MAIN_PF,MISR_DVO,MISR_HD_OUT,MISR_MAIN_HDOUT,MISR_MAIN_SDOUT");
    }
    return TUNING_OK;
}

