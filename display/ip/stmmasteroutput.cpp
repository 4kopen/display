/***********************************************************************
 *
 * File: display/ip/stmmasteroutput.cpp
 * Copyright (c) 2000-2009 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <display/generic/DisplayDevice.h>
#include <display/generic/DisplayPlane.h>
#include <display/generic/DisplayMixer.h>

#include <display/ip/stmdenc.h>
#include <display/ip/displaytiming/stmvtg.h>

#include "stmmasteroutput.h"


CSTmMasterOutput::CSTmMasterOutput(const char     *name,
                                   uint32_t        id,
                                   CDisplayDevice *pDev,
                                   CSTmDENC       *pDENC,
                                   CSTmVTG        *pVTG,
                                   CDisplayMixer  *pMixer) : COutput(name, id, pDev, (uint32_t)pVTG)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  m_pDevReg = (uint32_t*)pDev->GetCtrlRegisterBase();

  m_pDENC     = pDENC;
  m_pVTG      = pVTG;
  m_pMixer    = pMixer;

  m_ulCapabilities |= OUTPUT_CAPS_DISPLAY_TIMING_MASTER;
  m_ulCapabilities |= m_pMixer->GetCapabilities();

  if(m_pDENC)
    m_ulCapabilities |= m_pDENC->GetCapabilities();

  m_bUsingDENC  = false;

  /*
   * Analogue outputs don't have hot plug status, so we say they are always
   * connected.
   */
  m_displayStatus = STM_DISPLAY_CONNECTED;

  vibe_os_zero_memory(&m_PendingMode, sizeof(stm_display_mode_t));
  m_PendingMode.mode_id = STM_TIMING_MODE_RESERVED;

  m_ulBrightness = 0x80;
  m_ulSaturation = 0x80;
  m_ulContrast   = 0x80;
  m_ulHue        = 0x80;

  /*
   * Just pick some common default numbers to avoid divide by zero exceptions
   */
  m_maxDACVoltage  = 1400; // in mV
  m_DACSaturation  = 1023; // use the full 10bit DAC range
  RecalculateDACSetup();

  m_VTGErrorCount = 1;

  m_WaitQueueEvent               = (VIBE_OS_WaitQueue_t)0;
  m_bWakeUpVSyncCounterQueue     = false;
  m_OutputChangeEvent.event_id   = 0;
  m_OutputChangeEvent.object     = (stm_object_h)0;

  if(InitWaitQueue())
  {
    OUTPUT_TRC( TRC_ID_ERROR, "failed to initialize output events" );
  }

  m_bPendingStart = false;

  m_ulStopVSyncCounter = 0;

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


CSTmMasterOutput::~CSTmMasterOutput()
{
  TermWaitQueue();
}


bool CSTmMasterOutput::Stop(void)
{
  /* Don't wait for Vsync Counting if no planes are connected to the Mixer */
  uint32_t MixerStopVSyncCount = 0;

  OUTPUT_TRCIN( TRC_ID_MAIN_INFO, "VTG=%s", m_pVTG->GetName());

  /*
   * Planes could not be re-activated by a VSync, they need a new UpdateFromOutput()
   * with another reason than STM_PLANE_OUTPUT_STOPPED. So no need to protect with m_lock.
   */

  if(m_pMixer->HasEnabledPlanes())
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "mixer has enabled planes" );
    m_pMixer->UpdateFromOutput(this, STM_PLANE_OUTPUT_STOPPED);

    /*
     * It is necessary to wait for 5 Vsyncs in order to ensure the connected
     *  Planes has actually stopped properly.
     */
    MixerStopVSyncCount = 5;
  }

  vibe_os_lock_resource(m_lock);

  this->DisableDACs();

  vibe_os_unlock_resource(m_lock);

  StopMixer(MixerStopVSyncCount);

  if(m_bUsingDENC)
    m_pDENC->Stop();

  /*
   * Paranoia part 2. Force a vsync to be generated to ensure the mixer and
   * DENC double buffered registers have switched to the stopped values.
   */
  m_pVTG->ResetCounters();

  StopVTG(2);

  vibe_os_lock_resource(m_lock);
  {
    m_PendingMode.mode_id = STM_TIMING_MODE_RESERVED;
    m_bUsingDENC = false;

    COutput::Stop();
  }
  vibe_os_unlock_resource(m_lock);

  OUTPUT_TRCOUT( TRC_ID_MAIN_INFO, "VTG=%s", m_pVTG->GetName());
  return true;
}


const stm_display_mode_t* CSTmMasterOutput::SupportedMode(const stm_display_mode_t *mode) const
{
  /*
   * Check if mode is supported by slaved outputs
   */
  for(uint32_t i=0; i < m_pDisplayDevice->GetNumberOfOutputs(); i++)
  {
    if(m_pSlavedOutputs[i])
    {
      if(m_pSlavedOutputs[i]->SupportedMode(mode))
        return mode;
    }
  }

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "no matching mode found : %ux%u-%u%c - std=%x",
          mode->mode_params.active_area_width, mode->mode_params.active_area_height,
          (mode->mode_params.vertical_refresh_rate/1000),
          (mode->mode_params.scan_type == STM_INTERLACED_SCAN ? 'i' : ' '),
          mode->mode_params.output_standards );

  return 0;
}


void CSTmMasterOutput::WaitForVSyncCounterEvent(const uint32_t VSyncCount)
{
  /*
   * Maximum Timeout used with this method is 5 VSync period
   * multiplied by 2
   * Maximum VSync period is assumed to be 20ms (50Hz systems period)
   */
  static const int vsync_timeout_ms = (2 * 5 * 20);

  if (VSyncCount)
  {
    int ret = -1;
    m_ulStopVSyncCounter = VSyncCount;
    ret = vibe_os_wait_queue_event( m_WaitQueueEvent, &m_ulStopVSyncCounter
                                , 0, STMIOS_WAIT_COND_EQUAL, vsync_timeout_ms);
    if (ret == 1 )
    {
      /*
       * The condition was evaluated to true after the timeout elapsed.
       * This is not critical as the condition to exit is statified.
       */
      TRCOUT( TRC_ID_MAIN_INFO, "WARNING : Timeout (%d ms) is reached - Remaining %d VSyncs !!", vsync_timeout_ms, m_ulStopVSyncCounter );
    }
    else if (ret <= 0 )
    {
      /*
       * The the condition was evaluated to false after the timeout elapsed
       * or the wait was interrupted by a signal (-ERESTARTSYS is received).
       * This is critical as hardware might be not running properly!
       */
      TRCOUT( TRC_ID_ERROR, "FATAL : Timeout (%d ms) is reached - Remaining %d VSyncs !!", vsync_timeout_ms, m_ulStopVSyncCounter );
    }
  }
}

void CSTmMasterOutput::StopMixer(const uint32_t VSyncCount)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  WaitForVSyncCounterEvent(VSyncCount);

  vibe_os_lock_resource(m_lock);
  {
    m_pMixer->Stop();
  }
  vibe_os_unlock_resource(m_lock);

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}

void CSTmMasterOutput::StopVTG(const uint32_t VSyncCount)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  WaitForVSyncCounterEvent(VSyncCount);

  m_pVTG->Stop();

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}

bool CSTmMasterOutput::TryModeChange(const stm_display_mode_t* pModeLine)
{
  if(!m_bIsStarted)
    return false;

  OUTPUT_TRC( TRC_ID_TVOUT, "current/new mode: %d/%d", m_CurrentOutputMode.mode_id, pModeLine->mode_id );

  if(pModeLine->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK)
  {
    if(!m_pDENC)
      return false;

    if(AreModesIdentical(m_CurrentOutputMode,*pModeLine))
    {
      if(m_CurrentOutputMode.mode_params.output_standards != pModeLine->mode_params.output_standards)
      {
        if(m_bUsingDENC)
        {
          OUTPUT_TRC( TRC_ID_TVOUT, "changing DENC standard" );
          m_pDENC->Start(this, pModeLine);
        }
      }

      /*
       * Always keep a note of any changed standard (or flags) in case the DENC
       * gets re-started later due to an output format re-configuration.
       */
      COutput::Start(pModeLine);

      return true;
    }
  }
  else
  {
    /*
     * This supports changing between a select number of very closely
     * related modes, which requires only the fsynth register to be
     * changed and possibly the HDMI video info frame to be updated.
     * However getting all the hardware updated at the beginning of the
     * same vsync and glitch free is a challenge, so we have to
     * synchronize things in the VTG interrupt handler.
     */
    OUTPUT_TRC( TRC_ID_TVOUT, "trying VTG update" );

    vibe_os_lock_resource(m_lock);

    /*
     * Don't go any further if we already have a mode update pending,
     * the caller will have to stop the output if they really want to
     * do the change, which will cancel the outstanding update.
     */
    if(m_PendingMode.mode_id != STM_TIMING_MODE_RESERVED)
    {
      OUTPUT_TRC( TRC_ID_ERROR, "Failed, VTG update already pending m_PendingMode ID = %d m_CurrentMode ID = %d",m_PendingMode.mode_id,m_CurrentOutputMode.mode_id );
      vibe_os_unlock_resource(m_lock);
      return false;
    }

    m_PendingMode = *pModeLine;
    vibe_os_unlock_resource(m_lock);

    if(m_pVTG->RequestModeUpdate(pModeLine))
      return true;

    /*
     * Cancel the failed pending update
     */
    m_PendingMode.mode_id = STM_TIMING_MODE_RESERVED;
  }

  OUTPUT_TRC( TRC_ID_TVOUT, "failed, new mode is not compatible" );

  return false;
}


OutputResults CSTmMasterOutput::Start(const stm_display_mode_t* pModeLine)
{
  OUTPUT_TRCIN( TRC_ID_MAIN_INFO, "m_pVTG=%s, m_pMixer=%s", m_pVTG->GetName(), m_pMixer->GetName());

  // Check this object has been sanely set up
  ASSERTF((m_pVTG && m_pMixer),
          ("CSTmMasterOutput::Start Error: Output not initialised correctly\n"));

  ASSERTF(pModeLine, ("CSTmMasterOutput::Start Error: NULL mode line\n"));

  if(m_bIsSuspended)
  {
    OUTPUT_TRCOUT( TRC_ID_ERROR, "output is suspended" );
    return STM_OUT_BUSY;
  }


  /*
   * First try to change the display mode on the fly, if that works there is
   * nothing else to do.
   */
  if(TryModeChange(pModeLine))
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "mode change successful" );

    /* update MISR configuration to match new mode */
    UpdateMisrCtrl();

    m_bPendingStart = false;
    OUTPUT_TRCOUT( TRC_ID_MAIN_INFO, "");
    return STM_OUT_OK;
  }

  if(!m_pMixer->Start(pModeLine))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Mixer start failed" );
    goto stop_and_exit;
  }

  /*
   * Set all Slaved Outputs VTGSyncs Before Starting the VTG
   */
  if(!SetSlavedOutputsVTGSyncs(pModeLine))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Can't Set VTG Syncs for slaved ouptuts" );
    goto stop_and_exit;
  }

  /*
   * Assuming there are no IT because VTG is already stopped just before here
   * No need to lock for Vsync IRQ here
   */
  if(!m_pVTG->Start(pModeLine))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "VTG start failed" );
    goto stop_and_exit;
  }

  /*
   * Now we need to lock for VSync IRQ
   */
  vibe_os_lock_resource(m_lock);

  COutput::Start(pModeLine);
  m_bPendingStart = false;

  vibe_os_unlock_resource(m_lock);

  /*
   * Make sure that the current output format and DAC configuration are
   * applied, now that the mode is active, then turn the DACs on.
   */
  if(!this->UpdateOutputFormat(m_ulOutputFormat))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Requested output format cannot be configured" );
    goto stop_and_exit;
  }

  this->EnableDACs();

  /* update MISR configuration to match new mode */
  UpdateMisrCtrl();

  /*
   * Notify Mixer for this Output start :
   * m_CurrentOutputMode should be valid at this point
   * This should be done outside the locked resources state
   */
  m_pMixer->UpdateFromOutput(this, STM_PLANE_OUTPUT_STARTED);

  OUTPUT_TRCOUT( TRC_ID_MAIN_INFO, "");
  return STM_OUT_OK;

stop_and_exit:
  /*
   * Cleanup any partially successful hardware setup.
   */
  this->Stop();

  OUTPUT_TRCOUT( TRC_ID_MAIN_INFO, "");
  return STM_OUT_INVALID_VALUE;
}


void CSTmMasterOutput::Suspend(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(m_bIsSuspended)
    return;

  m_pMixer->Suspend();

  if(m_bIsStarted)
  {
    /*
     * If the output was properly running it is necessary to wait for vsyncs
     * in order to ensure the AWG has actually stopped. It also allows the
     * mixer enable register to latch 0 (all planes disabled) into the hardware.
     *
     * So we just wait here... but waiting for only one VSync is not always
     * enough, so we wait for 2 VSyncs (frame duration) to ensure hardware
     * update is done.
     */
    StopVTG(2);
  }

  COutput::Suspend();

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTmMasterOutput::Resume(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(!m_bIsSuspended)
    return;

  m_pMixer->Resume();

  if(m_bIsStarted)
  {
    if(m_pVTG)
      m_pVTG->Start(GetCurrentDisplayMode());

    /*
     * Restore the output format configuration in case the hardware was in reset
     * during the suspend.
     */
    this->SetOutputFormat(m_ulOutputFormat);
    /*
     * Enable the physical outputs.
     */
    this->EnableDACs();
  }

  COutput::Resume();

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTmMasterOutput::UpdateOutputMode(const stm_display_mode_t &mode)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );
  if(m_bIsStarted)
    m_CurrentOutputMode = mode;

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


bool CSTmMasterOutput::HandleInterrupts(void)
{
  stm_time64_t now         = vibe_os_get_system_time();
  uint32_t     timingevent = m_pVTG->GetInterruptStatus();

  if(timingevent == STM_TIMING_EVENT_NONE)
  {
    m_LastVTGEvent = STM_TIMING_EVENT_NONE;
    return true;
  }

  vibe_os_lock_resource(m_lock);

  if(!m_bIsStarted)
  {
    /*
     * Ignore timing events that arrive while we are still in the process of
     * starting the output.
     */
    vibe_os_unlock_resource(m_lock);
    return true;
  }

  if (timingevent != STM_TIMING_EVENT_LINE)
  {
    /*
     * This isn't a line only event, so we may have some work to do.
     *
     * TODO: update for clock doubled 3D modes.
     *
     * First check if we have had an on the fly mode change complete on this
     * vsync. If so then reset the VSync timing information (we may have changed
     * refresh frequency).
     */
    if((m_PendingMode.mode_id != STM_TIMING_MODE_RESERVED) &&
        AreModesIdentical(m_PendingMode, m_pVTG->GetCurrentMode()))
    {
      OUTPUT_TRC( TRC_ID_TVOUT, "changing display mode m_PendingMode ID = %d", m_PendingMode.mode_id );
      COutput::Start(&m_PendingMode);
      m_PendingMode.mode_id = STM_TIMING_MODE_RESERVED;
    }
/*
 * System time is not accurate in case of virtual platform
 * So no need to do this
 */
#if !defined(CONFIG_STM_VIRTUAL_PLATFORM)
    if(m_LastVTGEventTime != 0)
    {
      stm_time64_t timediff = now - m_LastVTGEventTime;

      if(timediff < 0LL)
      {
        OUTPUT_TRC( TRC_ID_ERROR, "backwards time detected, last time = %lld now = %lld", m_LastVTGEventTime, now );
      }
      else if(timediff >= (m_fieldframeDuration*2LL))
      {
        if(UNLIKELY (m_VTGErrorCount == 0))
        {
          OUTPUT_TRC( TRC_ID_ERROR, "time discontinuity detected, vsync interval = %lld field duration = %lld", timediff, m_fieldframeDuration );
          m_VTGErrorCount = 60;
        }
        else
        {
          m_VTGErrorCount--;
        }
      }
    }
#endif
  }

  m_LastVTGEvent     = timingevent;
  m_LastVTGEventTime = now;

  if(m_ulStopVSyncCounter)
  {
    if(m_ulStopVSyncCounter == 1)
    {
      // We are about to reach the VSync counter value. Wake up "m_WaitQueueEvent" to complete StopMixer/StopVTG.
      m_bWakeUpVSyncCounterQueue = true;
    }
    m_ulStopVSyncCounter--;
  }

  vibe_os_unlock_resource(m_lock);

  if(m_bWakeUpVSyncCounterQueue)
  {
    m_bWakeUpVSyncCounterQueue = false;
    vibe_os_wake_up_queue_event(m_WaitQueueEvent);
  }

  return true;
}


void CSTmMasterOutput::SoftReset(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(m_bIsStarted)
  {
    m_pVTG->ResetCounters();
  }

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}

void CSTmMasterOutput::UpdateHW(void)
{
  uint32_t mixer_pixel_fifo_status;
  // This is just for information. There is no action to be taken because of failure
  // in the mixer during the previous vsync.
  m_pMixer->GetLastVsyncStatus(&mixer_pixel_fifo_status);

  if(m_pDENC && (m_pDENC->GetOwner() == this))
    m_pDENC->UpdateHW(m_LastVTGEvent);

  if(m_bIsStarted)
    SetMisrData(m_LastVTGEventTime, m_LastVTGEvent);
}

void CSTmMasterOutput::SetMisrData(const stm_time64_t LastVTGEvtTime, uint32_t  LastVTGEvt){}

void CSTmMasterOutput::UpdateMisrCtrl(void){}

bool CSTmMasterOutput::CanShowPlane(const CDisplayPlane *plane)
{
  if(plane->GetParentDevice()->GetID() != m_pDisplayDevice->GetID())
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Plane and output do not belong to the same device" );
    return false;
  }

  /* FIXME: Actually when loading the Media-Controller module, this last is
   * doing Connect/disconnect each available plane to each available Output.
   * So Checking for supported modes by CGMS standard will make connection
   * impossible for all the life of the box even if we do not install a
   * supported mode. As result we may replace this by a test on the Output
   * format to allow VBIPlane only for YUV Output
   */
  if(plane->isVbiPlane())
  {
    if((m_ulOutputFormat & (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_YUV)) == 0)
    {
      OUTPUT_TRC( TRC_ID_TVOUT, "VBI Plane is not supported by this output (format=0x%x)", m_ulOutputFormat );
      return false;
    }
  }

  return m_pMixer->PlaneValid(plane);
}


bool CSTmMasterOutput::ShowPlane(const CDisplayPlane *plane)
{
  OUTPUT_TRC( TRC_ID_TVOUT, "\"%s\" ID:%u", plane->GetName(), plane->GetID() );
  if(plane->GetParentDevice()->GetID() != m_pDisplayDevice->GetID())
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Plane and output do not belong to the same device" );
    return false;
  }

  return m_pMixer->EnablePlane(plane);
}


void CSTmMasterOutput::HidePlane(const CDisplayPlane *plane)
{
  OUTPUT_TRC( TRC_ID_TVOUT, "\"%s\" ID:%u", plane->GetName(), plane->GetID() );
  if(plane->GetParentDevice()->GetID() != m_pDisplayDevice->GetID())
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Plane and output do not belong to the same device" );
    return;
  }

  m_pMixer->DisablePlane(plane);

  if(!m_pMixer->HasEnabledPlanes())
  {
    /*
     * No more planes are connected to the Mixer. Unblock any
     * possible WaitQueue for StopMixer() to speed up the
     * Output::Stop() sequence.
     */
    vibe_os_lock_resource(m_lock);
    if(m_ulStopVSyncCounter)
    {
      // Set value = 1 to allow Mixer register latching before getting
      // WaitQueue notification in CSTmMasterOutput::HandleInterrupts().
      m_ulStopVSyncCounter = 1;
    }
    vibe_os_unlock_resource(m_lock);
  }
}


bool CSTmMasterOutput::SetPlaneDepth(const CDisplayPlane *plane, int depth, bool activate)
{
  OUTPUT_TRC( TRC_ID_TVOUT, "%u depth = %d",plane->GetID(),depth );

  if(plane->GetParentDevice()->GetID() != m_pDisplayDevice->GetID())
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Plane and output do not belong to the same device" );
    return false;
  }

  return m_pMixer->SetPlaneDepth(plane, depth, activate);
}


bool CSTmMasterOutput::GetPlaneDepth(const CDisplayPlane *plane, int *depth) const
{
  OUTPUT_TRC( TRC_ID_TVOUT, "%u",plane->GetID() );

  if(plane->GetParentDevice()->GetID() != m_pDisplayDevice->GetID())
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Plane and output do not belong to the same device" );
    return false;
  }

  return m_pMixer->GetPlaneDepth(plane, depth);
}


uint32_t CSTmMasterOutput::SetControl(stm_output_control_t ctrl, uint32_t newVal)
{
  uint32_t ret = STM_OUT_OK;

  switch(ctrl)
  {
    case OUTPUT_CTRL_YCBCR_COLORSPACE:
    case OUTPUT_CTRL_BACKGROUND_ARGB:
    case OUTPUT_CTRL_BACKGROUND_VISIBILITY:
      ret = m_pMixer->SetControl(ctrl, newVal);
      break;

    case OUTPUT_CTRL_VIDEO_OUT_SELECT:
    {
      if(this->SetOutputFormat(newVal))
      {
        m_ulOutputFormat = newVal;
        if(m_bIsStarted)
          this->EnableDACs();
      }
      else
        ret = STM_OUT_INVALID_VALUE;

      break;
    }
    case OUTPUT_CTRL_CLOCK_ADJUSTMENT:
    {
      /*
       * The value passed in is actually signed 2's complement
       */
      if (m_ulOutputFormat&(STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC))
      {
        if ((int)newVal < -20 && (int)newVal >= -500)
        {
          OUTPUT_TRC( TRC_ID_TVOUT, "Clamping adjustement to -20 to have correct subcarrier frequency" );
          newVal = -20;
        }
        if ((int)newVal > 20  && (int)newVal <= 500)
        {
          OUTPUT_TRC( TRC_ID_TVOUT, "Clamping adjustement to 20 to have correct subcarrier frequency" );
          newVal = 20;
        }
      }
      if(!m_pVTG->SetAdjustment((int)newVal))
        ret = STM_OUT_NO_CTRL;
      break;
    }
    case OUTPUT_CTRL_BRIGHTNESS:
    {
      m_ulBrightness = newVal;
      if(m_bIsStarted)
        ProgramPSIControls();

      break;
    }
    case OUTPUT_CTRL_SATURATION:
    {
      m_ulSaturation = newVal;
      if(m_bIsStarted)
        ProgramPSIControls();

      break;
    }
    case OUTPUT_CTRL_CONTRAST:
    {
      m_ulContrast = newVal;
      if(m_bIsStarted)
        ProgramPSIControls();

      break;
    }
    case OUTPUT_CTRL_HUE:
    {
      m_ulHue = newVal;
      if(m_bIsStarted)
        ProgramPSIControls();

      break;
    }
    case OUTPUT_CTRL_CLIP_SIGNAL_RANGE:
    {
      if(newVal> STM_SIGNAL_VIDEO_RANGE)
      {
        ret = STM_OUT_INVALID_VALUE;
      }
      else
      {
        m_signalRange = (stm_display_signal_range_t)newVal;
        if(m_bIsStarted && m_pDENC && m_bUsingDENC)
          m_pDENC->SetControl(OUTPUT_CTRL_CLIP_SIGNAL_RANGE, (uint32_t)m_signalRange);
      }
      break;
    }
    case OUTPUT_CTRL_DAC123_MAX_VOLTAGE:
    {
      OUTPUT_TRC( TRC_ID_TVOUT, "max dac voltage = %u",newVal );
      m_maxDACVoltage = newVal;

      RecalculateDACSetup();
      if(m_pDENC && (m_pDENC->GetOwner()==this)) m_pDENC->SetControl(ctrl,newVal);

      this->SetOutputFormat(m_ulOutputFormat);

      break;
    }
    case OUTPUT_CTRL_MAX_PIXEL_CLOCK:
    {
      if(newVal < 600000000)
        m_ulMaxPixClock = newVal;
      else
        ret = STM_OUT_INVALID_VALUE;

      break;
    }
    case OUTPUT_CTRL_HDMI_SINK_HDR_FMT:
    {
      if(m_ulCapabilities & OUTPUT_CAPS_HDR_FORMAT)
      {
        uint32_t new_hdr_fmts = (newVal & STM_HDMI_SINK_HDR_FMT_MASK);
        if(new_hdr_fmts != m_ulSinkHDRFormats)
        {

          COutput::SetSinkSupportedHDRFormats(new_hdr_fmts);

          /* Update Slaved Outputs formats */
          for(uint32_t i=0; i < m_pDisplayDevice->GetNumberOfOutputs(); i++)
          {
            if(m_pSlavedOutputs[i] && (m_pSlavedOutputs[i]->GetCapabilities() & OUTPUT_CAPS_HDR_FORMAT))
              m_pSlavedOutputs[i]->SetSinkSupportedHDRFormats(new_hdr_fmts);
          }
        }
      }
      else
      {
        OUTPUT_TRC( TRC_ID_ERROR, "OUTPUT_CTRL_HDMI_SINK_HDR_FMT control is not supported!");
        ret = STM_OUT_NOT_SUPPORTED;
      }
      break;
    }
    case OUTPUT_CTRL_HDR_MODE_SELECT:
    {
      if(m_ulCapabilities & OUTPUT_CAPS_HDR_FORMAT)
      {
        if( newVal > (uint32_t)STM_OUTPUT_HDR_MODE_AUTO)
        {
          OUTPUT_TRC( TRC_ID_ERROR, "Invalid OUTPUT_CTRL_HDR_MODE_SELECT!");
          ret = STM_OUT_INVALID_VALUE;
        }
        else
        {
          m_HDRMode = (stm_output_hdr_mode_t)newVal;
          /* Update Slaved Outputs HDR Mode */
          for(uint32_t i=0; i < m_pDisplayDevice->GetNumberOfOutputs(); i++)
          {
            if(m_pSlavedOutputs[i] && (m_pSlavedOutputs[i]->GetCapabilities() & OUTPUT_CAPS_HDR_FORMAT))
              m_pSlavedOutputs[i]->UpdateOutputHDRMode(m_HDRMode);
          }
        }
      }
      else
      {
        OUTPUT_TRC( TRC_ID_ERROR, "OUTPUT_CTRL_HDR_MODE_SELECT not supported!");
        ret = STM_OUT_NOT_SUPPORTED;
      }
      break;
    }
    default:
        ret = (m_pDENC && (m_pDENC->GetOwner()==this))?m_pDENC->SetControl(ctrl, newVal):STM_OUT_BUSY;
  }

  /*
   * Following Output controls should be transmitted also to connected planes
   */
  switch(ctrl)
  {
    case OUTPUT_CTRL_YCBCR_COLORSPACE:
    {
      if((ret == STM_OUT_OK) && (m_OutputColorSpace != static_cast<stm_ycbcr_colorspace_t>(newVal)))
      {
        m_OutputColorSpace = static_cast<stm_ycbcr_colorspace_t>(newVal);
        m_pMixer->UpdateFromOutput(this, STM_PLANE_OUTPUT_UPDATED);
      }
      break;
    }
    default:
      break;
  }

  return ret;
}


uint32_t CSTmMasterOutput::GetControl(stm_output_control_t ctrl, uint32_t *val) const
{
  switch(ctrl)
  {
    case OUTPUT_CTRL_YCBCR_COLORSPACE:
    case OUTPUT_CTRL_BACKGROUND_ARGB:
    case OUTPUT_CTRL_BACKGROUND_VISIBILITY:
      return m_pMixer->GetControl(ctrl,val);

    case OUTPUT_CTRL_CLOCK_ADJUSTMENT:
    {
      *val = (uint32_t)m_pVTG->GetAdjustment(); /* Note this is actually signed */
      break;
    }
    case OUTPUT_CTRL_VIDEO_SOURCE_SELECT:
      *val = m_VideoSource;
      break;
    case OUTPUT_CTRL_AUDIO_SOURCE_SELECT:
      *val = m_AudioSource;
      break;
    case OUTPUT_CTRL_VIDEO_OUT_SELECT:
      *val = m_ulOutputFormat;
      break;
    case OUTPUT_CTRL_BRIGHTNESS:
      *val = m_ulBrightness;
      break;
    case OUTPUT_CTRL_SATURATION:
      *val = m_ulSaturation;
      break;
    case OUTPUT_CTRL_CONTRAST:
      *val = m_ulContrast;
      break;
    case OUTPUT_CTRL_HUE:
      *val = m_ulHue;
      break;
    case OUTPUT_CTRL_CLIP_SIGNAL_RANGE:
      *val = m_signalRange;
      break;
    case OUTPUT_CTRL_DAC123_MAX_VOLTAGE:
      *val = m_maxDACVoltage;
      break;
    case OUTPUT_CTRL_MAX_PIXEL_CLOCK:
      *val = m_ulMaxPixClock;
      break;
    case OUTPUT_CTRL_HDR_MODE_SELECT:
      if((m_ulCapabilities & OUTPUT_CAPS_HDR_FORMAT) == 0)
      {
        *val = 0;
        OUTPUT_TRC( TRC_ID_ERROR, "OUTPUT_CTRL_HDR_MODE_SELECT not supported!");
        return STM_OUT_NOT_SUPPORTED;
      }
      *val = m_HDRMode;
      break;
    case OUTPUT_CTRL_HDMI_SINK_HDR_FMT:
      if(m_ulCapabilities & OUTPUT_CAPS_HDR_FORMAT)
      {
        *val = m_ulSinkHDRFormats;
      }
      else
      {
        *val = 0;
        OUTPUT_TRC( TRC_ID_ERROR, "OUTPUT_CTRL_HDMI_SINK_HDR_FMT not supported!");
        return STM_OUT_NOT_SUPPORTED;
      }
      break;
      break;
    default:
      if(!m_pDENC || (m_pDENC->GetOwner()!=this))
      {
        *val = 0;
        return STM_OUT_NO_CTRL;
      }
      return m_pDENC->GetControl(ctrl,val);
  }

  return STM_OUT_OK;
}


uint32_t CSTmMasterOutput::SetCompoundControl(stm_output_control_t ctrl, void *newVal)
{
  uint32_t ret = STM_OUT_NO_CTRL;
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );
  switch(ctrl)
  {
    case OUTPUT_CTRL_DAC_FILTER_COEFFICIENTS:
    {
      const stm_display_filter_setup_t *f = (const stm_display_filter_setup_t *)newVal;
      ret = this->SetFilterCoefficients(f)?STM_OUT_OK:STM_OUT_INVALID_VALUE;
      break;
    }
    case OUTPUT_CTRL_VPS:
    {
      if(m_pDENC)
        ret = m_pDENC->SetVPS((stm_display_vps_t*)newVal)?STM_OUT_OK:STM_OUT_INVALID_VALUE;
      break;
    }
    case OUTPUT_CTRL_HDR_FORMAT:
    {
      if(m_ulCapabilities & OUTPUT_CAPS_HDR_FORMAT)
      {
        if(IsHDRModeAuto())
        {
          ret = STM_OUT_BUSY;
        }
        else
        {
          stm_hdr_format_t *hdr_fmt = (stm_hdr_format_t *)newVal;
          if(SetOutputHDRFormat(hdr_fmt))
            ret = STM_OUT_OK;
          else
            ret = STM_OUT_INVALID_VALUE;
        }
      }
      break;
    }
    default:
      ret = COutput::SetCompoundControl(ctrl, newVal);
  }

  /*
   * Following Output controls should be transmitted also to connected planes
   */
  switch(ctrl)
  {
    case OUTPUT_CTRL_DISPLAY_ASPECT_RATIO:
    case OUTPUT_CTRL_HDR_FORMAT:
    {
      if(ret == STM_OUT_OK)
        m_pMixer->UpdateFromOutput(this, STM_PLANE_OUTPUT_UPDATED);
      break;
    }
    default:
      break;
  }

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return ret;
}


bool CSTmMasterOutput::SetFilterCoefficients(const stm_display_filter_setup_t *f)
{
  bool ret = false;
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );
  switch(f->type)
  {
    case DENC_COEFF_LUMA:
    case DENC_COEFF_CHROMA:
      if(m_pDENC)
        ret = m_pDENC->SetFilterCoefficients(f);
      break;
    default:
      break;
  }
  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return ret;
}


void CSTmMasterOutput::ProgramPSIControls(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );
  /*
   * Default is to defer this down to the DENC, future pipelines may
   * support control on HD DACs as well.
   */
  if(m_pDENC && m_bUsingDENC)
  {
    m_pDENC->SetControl(OUTPUT_CTRL_BRIGHTNESS,m_ulBrightness);
    m_pDENC->SetControl(OUTPUT_CTRL_CONTRAST,  m_ulContrast);
    m_pDENC->SetControl(OUTPUT_CTRL_SATURATION,m_ulSaturation);
    m_pDENC->SetControl(OUTPUT_CTRL_HUE,       m_ulHue);
  }

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}

void CSTmMasterOutput::RecalculateDACSetup(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  /*
   * Calculate DAC levels for synchronisation insertion and rescaling.
   * We need to set things up to give the correct output voltages
   * based on the maximum voltage range of the DACs. This is partly
   * dependent on the reference voltage setup on a specific board, so this
   * can be set by the OUTPUT_CTRL_DAC_MAX_VOLTAGE and OUTPUT_CTRL_DAC_SATURATION
   * controls. The voltage is specified in mV.
   */

  /*
   * RGB and Y active signals are offset by 0.321V to allow
   * for negative sync pulses.
   */
  m_DAC_321mV = DIV_ROUNDED((m_DACSaturation*321),m_maxDACVoltage);

  OUTPUT_TRC( TRC_ID_TVOUT, "321mv = 0x%x (%u)",m_DAC_321mV,m_DAC_321mV );

  /*
   * Sync pulses are +-43IRE from the blanking level, where
   * 43IRE = 0.301V
   */
  m_DAC_43IRE = DIV_ROUNDED((m_DACSaturation*301),m_maxDACVoltage);

  OUTPUT_TRC( TRC_ID_TVOUT, "43IRE = 0x%x (%u)",m_DAC_43IRE,m_DAC_43IRE );

  /*
   * All video signals (including Pr & Pb) have a 0.7V range
   */
  m_DAC_700mV = DIV_ROUNDED((m_DACSaturation*700),m_maxDACVoltage);

  OUTPUT_TRC( TRC_ID_TVOUT, "700mV = 0x%x (%u)",m_DAC_700mV,m_DAC_700mV );

  /*
   * We assume that for RGB we map the full range of output from
   * the compositor (0-1023) to the DAC_Video_Range
   */
  m_DAC_RGB_Scale = DIV_ROUNDED(m_DAC_700mV,100);


#define CONVERT_TO_10BIT(x) (((x)<<2) | ((x)>>6))
  const uint32_t Compositor_Y_White_Value = CONVERT_TO_10BIT(235);
  const uint32_t Compositor_Y_Black_Value = CONVERT_TO_10BIT(16);
  const uint32_t Compositor_C_Max_Value   = CONVERT_TO_10BIT(240);
  const uint32_t Compositor_C_Mid_Value   = CONVERT_TO_10BIT(128);
  const uint32_t Compositor_C_Min_Value   = CONVERT_TO_10BIT(16);

  OUTPUT_TRC( TRC_ID_TVOUT, "Yw = 0x%x (%u)",Compositor_Y_White_Value,Compositor_Y_White_Value );
  OUTPUT_TRC( TRC_ID_TVOUT, "Yb = 0x%x (%u)",Compositor_Y_Black_Value,Compositor_Y_Black_Value );
  OUTPUT_TRC( TRC_ID_TVOUT, "Cmax = 0x%x (%u)",Compositor_C_Max_Value,Compositor_C_Max_Value );
  OUTPUT_TRC( TRC_ID_TVOUT, "Cmin = 0x%x (%u)",Compositor_C_Min_Value,Compositor_C_Min_Value );

  /*
   * Chroma is centered around 0.653V
   */
  const uint32_t DAC_653mV = DIV_ROUNDED((m_DACSaturation*653),m_maxDACVoltage);
  OUTPUT_TRC( TRC_ID_TVOUT, "653mv = 0x%x (%u)",DAC_653mV,DAC_653mV );

  /*
   * We scale the _active_ range of luma and chroma from the compositor to
   * the DAC_Video_Range. Then we offset the output so that the Black/0 level
   * chroma levels are located at the appropriate voltages.
   */
  const uint32_t scale_100_percent = 1024; // Register value for 100% scale

  m_DAC_Y_Scale   = DIV_ROUNDED((scale_100_percent * m_DAC_700mV), (Compositor_Y_White_Value - Compositor_Y_Black_Value));
  m_DAC_Y_Offset  = m_DAC_321mV - DIV_ROUNDED((Compositor_Y_Black_Value * m_DAC_Y_Scale) , scale_100_percent);

  m_DAC_C_Scale   = DIV_ROUNDED_UP((scale_100_percent * m_DAC_700mV), (Compositor_C_Max_Value - Compositor_C_Min_Value));
  m_DAC_C_Offset  = DAC_653mV - DIV_ROUNDED((Compositor_C_Mid_Value * m_DAC_C_Scale), scale_100_percent);

  OUTPUT_TRC( TRC_ID_TVOUT, "Yscale = 0x%x (%u)",m_DAC_Y_Scale,m_DAC_Y_Scale );
  OUTPUT_TRC( TRC_ID_TVOUT, "Yoff   = 0x%x (%u)",m_DAC_Y_Offset,m_DAC_Y_Offset );
  OUTPUT_TRC( TRC_ID_TVOUT, "Cscale = 0x%x (%u)",m_DAC_C_Scale,m_DAC_C_Scale );
  OUTPUT_TRC( TRC_ID_TVOUT, "Coff   = 0x%x (%u)",m_DAC_C_Offset,m_DAC_C_Offset );

  /*
   * Used in the AWG based devices to shift its signal output on the chroma
   * channels. Although we don't support syncs on chroma at the moment, this is
   * still needed to get the correct chroma blanking level, where the syncs
   * would otherwise be.
   */
  m_AWG_Y_C_Offset = DAC_653mV - m_DAC_Y_Offset;

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


void CSTmMasterOutput::SetClockReference(stm_clock_ref_frequency_t refClock, int error_ppm)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  m_pVTG->SetClockReference(refClock,error_ppm);

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


stm_display_metadata_result_t CSTmMasterOutput::QueueMetadata(stm_display_metadata_t *m)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  switch(m->type)
  {
    case STM_METADATA_TYPE_PICTURE_INFO:
    case STM_METADATA_TYPE_TELETEXT:
    case STM_METADATA_TYPE_CLOSED_CAPTION:
      if(m_pDENC && m_bUsingDENC)
        return m_pDENC->QueueMetadata(m);
      else
        return STM_METADATA_RES_QUEUE_UNAVAILABLE;

    default:
      return STM_METADATA_RES_UNSUPPORTED_TYPE;
  }
}


void CSTmMasterOutput::FlushMetadata(stm_display_metadata_type_t type)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  switch(type)
  {
    case STM_METADATA_TYPE_PICTURE_INFO:
    case STM_METADATA_TYPE_TELETEXT:
    case STM_METADATA_TYPE_CLOSED_CAPTION:
      if(m_pDENC && m_bUsingDENC)
        m_pDENC->FlushMetadata(type);

      break;
    default:
      break;
  }

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


int CSTmMasterOutput::InitWaitQueue(void)
{
  vibe_os_allocate_queue_event(&m_WaitQueueEvent);
  if(!m_WaitQueueEvent)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Failed to allocate waitqueue event!" );
    return -ENOMEM;
  }

  return 0;
}


int CSTmMasterOutput::TermWaitQueue(void)
{
  if(m_WaitQueueEvent)
    vibe_os_release_queue_event(m_WaitQueueEvent);

  return 0;
}


int CSTmMasterOutput::NotifyEvent(stm_output_events_t event_id)
{
  int result = 0;

  m_OutputChangeEvent.object   = (stm_object_h)this;
  m_OutputChangeEvent.event_id = event_id;
  result = stm_event_signal (&m_OutputChangeEvent);
  if(result)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "output %s event notification failed!", GetName() );
  }
  else
    OUTPUT_TRC( TRC_ID_TVOUT, "notify %s mode change", GetName());

  return result;
}

void CSTmMasterOutput::GetOutputColorspace(stm_ycbcr_colorspace_t &color_space) const
{
  /*
   * For new if the color space is in auto mode than we go check for
   * the output standard and select appropriate CS.
   */
  color_space = m_OutputColorSpace;
  if(color_space == STM_YCBCR_COLORSPACE_AUTO_SELECT)
  {
    if((m_CurrentOutputMode.mode_params.output_standards & (STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861))
    || (m_CurrentOutputMode.mode_params.output_standards & STM_OUTPUT_STD_HD_MASK))
    {
      color_space = STM_YCBCR_COLORSPACE_709;
    }
    else
    {
      color_space = STM_YCBCR_COLORSPACE_601;
    }
  }
}


bool CSTmMasterOutput::GetOutputHDRFormat(stm_hdr_format_t &hdr_fmt) const
{
  OUTPUT_TRCIN( TRC_ID_UNCLASSIFIED, "" );

  if(m_ulCapabilities & OUTPUT_CAPS_HDR_FORMAT)
  {
    hdr_fmt = m_HDRFormat;
    OUTPUT_TRCOUT( TRC_ID_UNCLASSIFIED, "" );
    return true;
  }

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "HDR format is not supported on this output" );
  return false;
}


bool CSTmMasterOutput::SetOutputHDRFormat(stm_hdr_format_t *hdr_fmt)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(m_ulCapabilities & OUTPUT_CAPS_HDR_FORMAT)
  {
    vibe_os_lock_resource(m_lock);
    m_HDRFormat = *hdr_fmt;
    vibe_os_unlock_resource(m_lock);

    /*
     * Update slaved outputs respecting HDR format capability
     */
    for(uint32_t i=0; i < m_pDisplayDevice->GetNumberOfOutputs(); i++)
    {
      if(m_pSlavedOutputs[i])
      {
        if(m_pSlavedOutputs[i]->GetCapabilities() & OUTPUT_CAPS_HDR_FORMAT)
          m_pSlavedOutputs[i]->SetOutputHDRFormat(hdr_fmt);
      }
    }

    OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
    return true;
  }

  OUTPUT_TRCOUT( TRC_ID_ERROR, "HDR format is not supported on this output" );
  return false;
}

