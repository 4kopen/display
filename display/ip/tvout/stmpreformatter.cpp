/***********************************************************************
 *
 * File: display/ip/tvout/stmpreformatter.cpp
 * Copyright (c) 2011 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplayDevice.h>

#include "stmpreformatter.h"

#define TVO_CSC_MAT_OFFSET             0x00
#define TVO_ADAPTIVE_DCMTN_FLTR_CFG    0x20
#define TVO_INPUT_VID_FORMAT           0x30

#define TVO_INPUT_FMT_SIGNED           (0x1<<0)
#define TVO_SYNC_EXT                   (0x0<<4)

#define PF_VDF_MODE_MASK               (0x100)
#define PF_VDF_ENABLE                  (0x100)
#define PF_VDF_DISABLE                 (0)

#define PF_ADF_MODE_MASK               (7)
#define PF_SELECT_ADAPTIVE_FILTER      0
#define PF_SELECT_LINEAR_FILTER        1
#define PF_SELECT_EVEN_SAMPLE_DROP     3
#define PF_SELECT_ODD_SAMPLE_DROP      7

const static uint32_t adf_mode_select[VOUT_PF_ADF_MODE_NBR] = {
    PF_SELECT_ADAPTIVE_FILTER
  , PF_SELECT_LINEAR_FILTER
  , PF_SELECT_EVEN_SAMPLE_DROP
  , PF_SELECT_ODD_SAMPLE_DROP
};


/*
 * Values from TVOut functional spec, done like this to avoid having to convert
 * the 2's compliment negative numbers by hand.
 *
 * Following is the correct description of coefs:
 * Register fields C11 C12 C13 C21 C22 C23 C31 C32 C33
 * Matrix coeffs   C31 C32 C33 C11 C12 C13 C21 C22 C23
 */
static const stm_vout_pf_mat_coef_t pf_conversion_matrix =
{
  /* YCbCr to RGB */
  {
    /* 601 YCbCr to RGB (Not defined) */
    { 0x00000000, 0x00000000, 0x00000000, 0x00000000
    , 0x00000000, 0x00000000, 0x00000000, 0x00000000
    }
    /* 709 YCbCr to RGB (Not defined) */
  , { 0x00000000, 0x00000000, 0x00000000, 0x00000000
    , 0x00000000, 0x00000000, 0x00000000, 0x00000000
    }
    /* BT2020 YCbCr to RGB (Not defined) */
  , { 0x00000000, 0x00000000, 0x00000000, 0x00000000
    , 0x00000000, 0x00000000, 0x00000000, 0x00000000
    }
  },
  /* RGB to YCbCr */
  {
    /* 601 RGB to YCbCr */
    { 0xF927082E, 0x04C9FEAB, 0x01D30964, 0xFA95FD3D,
      0x0000082E, 0x00002000, 0x00002000, 0x00000000
    }
    /* 709 RGB to YCbCr */
  , { 0xF891082F, 0x0367FF40, 0x01280B71, 0xF9B1FE20,
      0x0000082F, 0x00002000, 0x00002000, 0x00000000
    }
    /* BT2020 RGB to YCbCr */
  , { 0xF8A50800, 0x0434FF5C, 0x00F20AD9, 0xFA3CFDC4,
      0x00000800, 0x00002000, 0x00002000, 0x00000000
    }
  }
};

CSTmPreformatter::CSTmPreformatter(const char                   *name,
                                   CDisplayDevice               *pDev,
                                   uint32_t                      ulPFRegs,
                                   uint32_t                      caps)

{
  PF_TRCIN( TRC_ID_PF, "");

  m_name             = name;
  m_pDevRegs         = (uint32_t*)pDev->GetCtrlRegisterBase();
  m_ulPFRegOffset    = ulPFRegs;
  m_pPFConversionMat = &pf_conversion_matrix;
  m_ulCapabilities   = caps;
  m_adf_mode         = ADF_INVALID_MODE;
  m_bIsVDFEnabled    = false;

  PF_TRCOUT( TRC_ID_PF, "" );
}


CSTmPreformatter::~CSTmPreformatter() {}


bool CSTmPreformatter::SetColorSpaceConversion(stm_ycbcr_colorspace_t colorspaceMode, stm_vout_pf_mat_direction_t direction)
{
  int input=0;

  PF_TRC( TRC_ID_PF, "" );

  /*
   * Input to TVOut is signed RGB
   */
  WriteReg(TVO_INPUT_VID_FORMAT, (TVO_INPUT_FMT_SIGNED | TVO_SYNC_EXT));

  if(colorspaceMode == STM_YCBCR_COLORSPACE_709)
    input = 1;
  else if(colorspaceMode == STM_COLORSPACE_BT2020)
    input = 2;

  for(uint32_t i=0; i<8; i++)
  {
    switch(direction)
    {
      case VOUT_PF_CONV_YUV_TO_RGB:
        WriteReg((TVO_CSC_MAT_OFFSET + 4*i), m_pPFConversionMat->ycbcr_to_rgb[input][i]);
        PF_TRC( TRC_ID_PF, "WriteReg - Add=0x%08X, val=0x%08X.", (TVO_CSC_MAT_OFFSET + 4*i), m_pPFConversionMat->ycbcr_to_rgb[input][i] );
        break;
      case VOUT_PF_CONV_RGB_TO_YUV:
        WriteReg((TVO_CSC_MAT_OFFSET + 4*i), m_pPFConversionMat->rgb_to_ycbcr[input][i]);
        PF_TRC( TRC_ID_PF, "WriteReg - Add=0x%08X, val=0x%08X.", (TVO_CSC_MAT_OFFSET + 4*i), m_pPFConversionMat->rgb_to_ycbcr[input][i] );
        break;
      default:
        PF_TRC( TRC_ID_ERROR, "Invalid pre-formatter conversion request" );
        return false;
    }
  }

  PF_TRCOUT( TRC_ID_PF, "" );
  return true;
}


bool CSTmPreformatter::SetAdaptiveDecimationFilterMode(stm_vout_pf_adf_mode_t adf_mode)
{
  PF_TRCIN( TRC_ID_PF, "" );

  if(!(m_ulCapabilities&VOUT_PF_CAPS_ADF))
  {
    PF_TRC( TRC_ID_ERROR, "Preformatter does not have Adaptive Decimation Filter !!" );
    m_adf_mode = ADF_INVALID_MODE;
    return false;
  }
  if(adf_mode>=VOUT_PF_ADF_MODE_NBR)
  {
    PF_TRC( TRC_ID_ERROR, "Invalid Adaptive Decimation Filter mode !!" );
    m_adf_mode = ADF_INVALID_MODE;
    return false;
  }

  uint32_t val = ReadReg(TVO_ADAPTIVE_DCMTN_FLTR_CFG);
  val &= ~PF_ADF_MODE_MASK;
  val |= adf_mode_select[adf_mode];
  WriteReg(TVO_ADAPTIVE_DCMTN_FLTR_CFG, val);

  m_adf_mode = adf_mode;

  PF_TRCOUT( TRC_ID_PF, "" );
  return true;
}


bool CSTmPreformatter::GetAdaptiveDecimationFilterMode(stm_vout_pf_adf_mode_t *adf_mode)
{
  PF_TRCIN( TRC_ID_PF, "" );

  if(!(m_ulCapabilities&VOUT_PF_CAPS_ADF))
  {
    PF_TRC( TRC_ID_ERROR, "Preformatter does not have Adaptive Decimation Filter !!" );
    return false;
  }
  if(m_adf_mode == ADF_INVALID_MODE)
  {
    PF_TRC( TRC_ID_ERROR, "Invalid Adaptive Decimation Filter mode !!" );
    return false;
  }

  *adf_mode = m_adf_mode;

  PF_TRCOUT( TRC_ID_PF, "" );
  return true;
}

bool CSTmPreformatter::EnableVerticalDecimationFilter(bool enable)
{
  PF_TRCIN( TRC_ID_PF, "" );

  if(!(m_ulCapabilities&VOUT_PF_CAPS_VDF))
  {
    PF_TRC( TRC_ID_ERROR, "Preformatter does not have Vertical Decimation Filter !!" );
    return false;
  }

  uint32_t val = ReadReg(TVO_ADAPTIVE_DCMTN_FLTR_CFG);
  val &= ~PF_VDF_MODE_MASK;
  val |= enable?PF_VDF_ENABLE:PF_VDF_DISABLE;
  WriteReg(TVO_ADAPTIVE_DCMTN_FLTR_CFG, val);
  m_bIsVDFEnabled = enable;

  PF_TRCOUT( TRC_ID_PF, "" );
  return true;
}
