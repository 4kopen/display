/***********************************************************************
 *
 * File: display/ip/tvout/stmvip.cpp
 * Copyright (c) 2011 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplayDevice.h>

#include "stmvip.h"

#define TVO_VIP_CTL_OFFSET                      0x00                    /* VIP Configuration Register */
#define TVO_VIP_REORDER_R_SHIFT                 24
#define TVO_VIP_REORDER_G_SHIFT                 20
#define TVO_VIP_REORDER_B_SHIFT                 16
#define TVO_VIP_REORDER_WIDTH                   2
#define TVO_VIP_REORDER_Y_G_SEL                 0
#define TVO_VIP_REORDER_CB_B_SEL                1
#define TVO_VIP_REORDER_CR_R_SEL                2
#define TVO_VIP_CLIP_SHIFT                      8
#define TVO_VIP_CLIP_WIDTH                      3
#define TVO_VIP_CLIP_DISABLED                   0
#define TVO_VIP_CLIP_EAV_SAV                    1
#define TVO_VIP_CLIP_LIMITED_RANGE_RGB_Y        2
#define TVO_VIP_CLIP_LIMITED_RANGE_CB_CR        3
#define TVO_VIP_CLIP_PROG_RANGE                 4
#define TVO_VIP_RND_SHIFT                       4
#define TVO_VIP_RND_WIDTH                       2
#define TVO_VIP_RND_8BIT_ROUNDED                0
#define TVO_VIP_RND_10BIT_ROUNDED               1
#define TVO_VIP_RND_12BIT_ROUNDED               2

#define TVO_VIP_SEL_INPUT_SHIFT                 0
#define TVO_VIP_SEL_INPUT_WIDTH                 4
#define TVO_VIP_SEL_INPUT_MAIN                  0
#define TVO_VIP_SEL_INPUT_AUX                   8
#define TVO_VIP_SEL_INPUT_CONVERTED             0
#define TVO_VIP_SEL_INPUT_BYPASSED              1
#define TVO_VIP_SEL_INPUT_MAIN_FILTERED         2
#define TVO_VIP_SEL_INPUT_DENC_DAC_123          13
#define TVO_VIP_SEL_INPUT_DENC_DAC_456          14
#define TVO_VIP_SEL_INPUT_FORCE_COLOR           15

#define TVO_VIP_FORCE_COLOR_0                   0x04                    /* CB_B and Y_G input Force */
#define TVO_VIP_FORCE_COLOR_1                   0x08                    /* CR_R input Force */
#define TVO_VIP_FORCE_COLOR_MASK                0xFFF                   /* Force color mask */
#define TVO_VIP_FORCE_COLOR_B_CB_SHIFT          0
#define TVO_VIP_FORCE_COLOR_G_Y_SHIFT           16
#define TVO_VIP_FORCE_COLOR_R_CR_SHIFT          0
#define TVO_VIP_FORCE_COLOR_WIDTH               12

#define TVO_VIP_CLIP_VALUE_B_CB                 0x0C                    /* Clip value range in case of B_CB */
#define TVO_VIP_CLIP_VALUE_G_Y                  0x10                    /* Clip value range in case of G_Y */
#define TVO_VIP_CLIP_VALUE_R_CR                 0x14                    /* Clip value range in case of R_CR */
#define TVO_VIP_CLIP_MIN_SHIFT                  0
#define TVO_VIP_CLIP_MAX_SHIFT                  16
#define TVO_VIP_CLIP_VALUE_WIDTH                12


#define TVO_VIP_SYNC_SEL_OFFSET                 0x18
#define TVO_VIP_SYNC_SEL_VGA_OFFSET             0x21C
#define TVO_VIP_SYNC_SEL_VGA_DVO_PADS_OFFSET    0x218
#define TVO_VIP_SYNC_SEL_WIDTH                  5
#define TVO_VIP_SYNC_DENC_SHIFT                 0
#define TVO_VIP_SYNC_SDDCS_SHIFT                8
#define TVO_VIP_SYNC_SDVTG_SHIFT                16
#define TVO_VIP_SYNC_TTXT_SHIFT                 24
#define TVO_VIP_SYNC_HDF_SHIFT                  0
#define TVO_VIP_SYNC_HDDCS_SHIFT                8
#define TVO_VIP_SYNC_HDMI_SHIFT                 0
#define TVO_VIP_SYNC_DVO_SHIFT                  0
#define TVO_VIP_SYNC_DVO_HS_PAD_SHIFT           8
#define TVO_VIP_SYNC_DVO_VS_PAD_SHIFT           16
#define TVO_VIP_SYNC_DVO_BNOT_PAD_SHIFT         24
#define TVO_VIP_SYNC_VGA_DVO_HS_PAD_SHIFT       8
#define TVO_VIP_SYNC_VGA_DVO_VS_PAD_SHIFT       16
#define TVO_VIP_SYNC_VGA_HS_PAD_SHIFT           0
#define TVO_VIP_SYNC_VGA_VS_PAD_SHIFT           8
#define TVO_VIP_SYNC_SEL_OFFSET_MAIN            0
#define TVO_VIP_SYNC_SEL_OFFSET_AUX             (2 << 3)

#define TVO_VIP_DFV_OFFSET                      0x50                    /* Soft Reset */
#define TVO_VIP_SOFT_RESET                      (1L << 0)

#define TVO_VIP_SYNC_TYPE_VGA             (TVO_VIP_SYNC_TYPE_VGA_HS_PAD|TVO_VIP_SYNC_TYPE_VGA_VS_PAD)
#define TVO_VIP_SYNC_TYPE_VGA_DVO_PADS    (TVO_VIP_SYNC_TYPE_VGA_DVO_HS_PAD|TVO_VIP_SYNC_TYPE_VGA_DVO_VS_PAD)

int tvo_vip_sync_shift [TVO_VIP_SYNC_NBR] =
{
    TVO_VIP_SYNC_DENC_SHIFT             /* TVO_VIP_SYNC_DENC_IDX */
  , TVO_VIP_SYNC_SDDCS_SHIFT            /* TVO_VIP_SYNC_SDDCS_IDX */
  , TVO_VIP_SYNC_SDVTG_SHIFT            /* TVO_VIP_SYNC_SDVTG_IDX */
  , TVO_VIP_SYNC_TTXT_SHIFT             /* TVO_VIP_SYNC_TTXT_IDX */
  , TVO_VIP_SYNC_HDF_SHIFT              /* TVO_VIP_SYNC_HDF_IDX */
  , TVO_VIP_SYNC_HDDCS_SHIFT            /* TVO_VIP_SYNC_HDDCS_IDX */
  , TVO_VIP_SYNC_HDMI_SHIFT             /* TVO_VIP_SYNC_HDMI_IDX */
  , TVO_VIP_SYNC_DVO_SHIFT              /* TVO_VIP_SYNC_DVO_IDX */
  , TVO_VIP_SYNC_DVO_HS_PAD_SHIFT       /* TVO_VIP_SYNC_DVO_HS_PAD_IDX */
  , TVO_VIP_SYNC_DVO_VS_PAD_SHIFT       /* TVO_VIP_SYNC_DVO_VS_PAD_IDX */
  , TVO_VIP_SYNC_DVO_BNOT_PAD_SHIFT     /* TVO_VIP_SYNC_DVO_BNOT_PAD_IDX */
  , TVO_VIP_SYNC_VGA_DVO_HS_PAD_SHIFT   /* TVO_VIP_SYNC_VGA_DVO_HS_PAD_IDX */
  , TVO_VIP_SYNC_VGA_DVO_VS_PAD_SHIFT   /* TVO_VIP_SYNC_VGA_DVO_VS_PAD_IDX */
  , TVO_VIP_SYNC_VGA_HS_PAD_SHIFT       /* TVO_VIP_SYNC_VGA_HS_PAD_IDX */
  , TVO_VIP_SYNC_VGA_VS_PAD_SHIFT       /* TVO_VIP_SYNC_VGA_VS_PAD_IDX */
};


CSTmVIP::CSTmVIP(const char                    *name,
                 CDisplayDevice                *pDev,
                 uint32_t                       ulVIPRegs,
                 CSTmPreformatter              *pMainPreformatter,
                 CSTmPreformatter              *pAuxPreformatter,
                 uint32_t                       ulSyncType,
                 const stm_display_sync_id_t   *sync_sel_map,
                 uint32_t                       capabilities)
{
  VIP_TRCIN( TRC_ID_VIP, "" );

  m_name           = name;
  m_pDevRegs       = (uint32_t*)pDev->GetCtrlRegisterBase();
  m_ulVIPRegOffset = ulVIPRegs;

  m_pMainPreformatter  = pMainPreformatter;
  m_pAuxPreformatter   = pAuxPreformatter;

  m_ulSyncType   = ulSyncType;
  m_sync_sel_map = sync_sel_map;

  /*
   * Default channel reorder, no swaps. This can be overridden by calling
   * SetColorChannelOrder
   */
  m_ulRChannel = TVO_VIP_REORDER_CR_R_SEL;
  m_ulGChannel = TVO_VIP_REORDER_Y_G_SEL;
  m_ulBChannel = TVO_VIP_REORDER_CB_B_SEL;

  m_ulCapabilities = capabilities;

  m_ulVipCfg = 0;

  m_bHwInitDone = false;

  VIP_TRCOUT( TRC_ID_VIP, "");
}


CSTmVIP::~CSTmVIP() {}


void CSTmVIP::SoftReset(void)
{
  VIP_TRCIN( TRC_ID_VIP, "" );

  WriteReg(TVO_VIP_DFV_OFFSET, TVO_VIP_SOFT_RESET);

  VIP_TRCOUT( TRC_ID_VIP, "" );
}


void CSTmVIP::SetColorChannelOrder(tvo_vip_input_color_channel_t red_output,
                                   tvo_vip_input_color_channel_t green_output,
                                   tvo_vip_input_color_channel_t blue_output)
{
  m_ulRChannel = red_output;
  m_ulGChannel = green_output;
  m_ulBChannel = blue_output;
}

const char *CSTmVIP::source_names[] = {
        "MAIN"
      , "AUX"
      , "DENC123"
      , "DENC456"
      , "MAIN_FILTERED_422"
    };

bool CSTmVIP::SetInputParams(const tvo_vip_video_source_t source, uint32_t format, const stm_display_signal_range_t clipping)
{
  VIP_TRC( TRC_ID_VIP, "VIP Input is %s, format = 0x%x", source_names[source], format );

  uint32_t InputSel = 0;
  uint32_t Rounding = 0;
  uint32_t Clipping = 0;

  m_ulVipCfg = ReadReg(TVO_VIP_CTL_OFFSET);

  SetBitField(m_ulVipCfg, TVO_VIP_REORDER_R_SHIFT, TVO_VIP_REORDER_WIDTH, m_ulRChannel);
  SetBitField(m_ulVipCfg, TVO_VIP_REORDER_G_SHIFT, TVO_VIP_REORDER_WIDTH, m_ulGChannel);
  SetBitField(m_ulVipCfg, TVO_VIP_REORDER_B_SHIFT, TVO_VIP_REORDER_WIDTH, m_ulBChannel);

  switch(clipping)
  {
    case STM_SIGNAL_FULL_RANGE:
      VIP_TRC( TRC_ID_VIP, "full range output (no clipping)" );
      Clipping = TVO_VIP_CLIP_DISABLED;
      break;
    case STM_SIGNAL_FILTER_SAV_EAV:
      VIP_TRC( TRC_ID_VIP, "clip EAV/SAV");
      Clipping = TVO_VIP_CLIP_EAV_SAV;
      break;
    case STM_SIGNAL_VIDEO_RANGE:
      if(format & STM_VIDEO_OUT_RGB)
      {
        VIP_TRC( TRC_ID_VIP, "clip RGB/Y range");
        Clipping = TVO_VIP_CLIP_LIMITED_RANGE_RGB_Y;
      }
      else
      {
        VIP_TRC( TRC_ID_VIP, "clip CB/CR range");
        Clipping = TVO_VIP_CLIP_LIMITED_RANGE_CB_CR;
      }
      break;
  }

  SetBitField(m_ulVipCfg, TVO_VIP_CLIP_SHIFT, TVO_VIP_CLIP_WIDTH, Clipping);

  switch (format & STM_VIDEO_OUT_DEPTH_MASK)
  {
    case STM_VIDEO_OUT_24BIT:
      VIP_TRC( TRC_ID_VIP, "24bit color, rounding input to 8bit");
      Rounding = TVO_VIP_RND_8BIT_ROUNDED;
      break;
    case STM_VIDEO_OUT_30BIT:
      VIP_TRC( TRC_ID_VIP, "30bit color, rounding input to 10bit");
      Rounding = TVO_VIP_RND_10BIT_ROUNDED;
      break;
    case STM_VIDEO_OUT_36BIT:
    case STM_VIDEO_OUT_48BIT:
      VIP_TRC( TRC_ID_VIP, "36/48bit color, rounding input to 12bit");
      Rounding = TVO_VIP_RND_12BIT_ROUNDED;
      break;
    default:
      VIP_TRC( TRC_ID_VIP, "Unsupported colordepth rounding input to 8bit");
      Rounding = TVO_VIP_RND_8BIT_ROUNDED;
      break;
  }
  SetBitField(m_ulVipCfg, TVO_VIP_RND_SHIFT      , TVO_VIP_RND_WIDTH    , Rounding);

  switch(source)
  {
    case TVO_VIP_MAIN_VIDEO:
      InputSel |= TVO_VIP_SEL_INPUT_MAIN;

      if(  ( (m_ulCapabilities & TVO_VIP_BYPASS_INPUT_IS_RGB) && (format & STM_VIDEO_OUT_RGB))
        || (!(m_ulCapabilities & TVO_VIP_BYPASS_INPUT_IS_RGB) && (format & STM_VIDEO_OUT_YUV)) )
      {
        if(m_ulCapabilities & TVO_VIP_INPUT_IS_INVERTED)
          InputSel &= ~TVO_VIP_SEL_INPUT_BYPASSED;
        else
          InputSel |= TVO_VIP_SEL_INPUT_BYPASSED;
      }
      else
      {
        if(m_ulCapabilities & TVO_VIP_INPUT_IS_INVERTED)
          InputSel |= TVO_VIP_SEL_INPUT_BYPASSED;
        else
          InputSel &= ~TVO_VIP_SEL_INPUT_BYPASSED;
      }

      SelectSync(TVO_VIP_MAIN_SYNCS);

      break;
    case TVO_VIP_AUX_VIDEO:
      InputSel |= TVO_VIP_SEL_INPUT_AUX;

      if(  ( (m_ulCapabilities & TVO_VIP_BYPASS_INPUT_IS_RGB) && (format & STM_VIDEO_OUT_RGB))
        || (!(m_ulCapabilities & TVO_VIP_BYPASS_INPUT_IS_RGB) && (format & STM_VIDEO_OUT_YUV)) )
      {
        if(m_ulCapabilities & TVO_VIP_INPUT_IS_INVERTED)
          InputSel &= ~TVO_VIP_SEL_INPUT_BYPASSED;
        else
          InputSel |= TVO_VIP_SEL_INPUT_BYPASSED;
      }
      else
      {
        if(m_ulCapabilities & TVO_VIP_INPUT_IS_INVERTED)
          InputSel |= TVO_VIP_SEL_INPUT_BYPASSED;
        else
          InputSel &= ~TVO_VIP_SEL_INPUT_BYPASSED;
      }

      SelectSync(TVO_VIP_AUX_SYNCS);

      break;
    case TVO_VIP_DENC123:
      InputSel |= TVO_VIP_SEL_INPUT_DENC_DAC_123;
      /*
       * Note the DENC output is already in the required YCbCr or RGB format
       * The syncs will be set by the caller in this case because they may be
       * either Main or Aux depending on the DENC usage.
       */
      break;
    case TVO_VIP_DENC456:
      InputSel |= TVO_VIP_SEL_INPUT_DENC_DAC_456;
      break;
    case TVO_VIP_MAIN_FILTERED:
      if(  (!(m_ulCapabilities & TVO_VIP_HAS_MAIN_FILTERED_422_INPUT) || !(format & STM_VIDEO_OUT_422))
         &&(!(m_ulCapabilities & TVO_VIP_HAS_MAIN_FILTERED_420_INPUT) || !(format & STM_VIDEO_OUT_420)))
      {
        VIP_TRC( TRC_ID_VIP, "Capabilities(0x%x) and/or format(0x%x) are not ok !!", m_ulCapabilities, format);
        return false;
      }

      if ((format & STM_VIDEO_OUT_420) && !(m_pMainPreformatter->GetCapabilities()&VOUT_PF_CAPS_VDF))
      {
        VIP_TRC( TRC_ID_VIP, "Preformatter does not support 420 down-sampling !!!");
        return false;
      }

      /*
       * TODO: Enabling 420 sampling should not be done if another VIP is using 422 sampling(and vice versa).
       * Currently, only HDMI VIP is using adaptive filter, so this could be done later.
       */
      m_pMainPreformatter->EnableVerticalDecimationFilter((format & STM_VIDEO_OUT_420)?true:false);

      InputSel |= TVO_VIP_SEL_INPUT_MAIN_FILTERED;
      SelectSync(TVO_VIP_MAIN_SYNCS);
      break;
  }

  SetBitField(m_ulVipCfg, TVO_VIP_SEL_INPUT_SHIFT, TVO_VIP_SEL_INPUT_WIDTH, InputSel);

  VIP_TRC( TRC_ID_VIP, "VipCfg = 0x%x", m_ulVipCfg );

  WriteReg(TVO_VIP_CTL_OFFSET, m_ulVipCfg);

  VIP_TRCOUT( TRC_ID_VIP, "" );
  return true;
}


void CSTmVIP::InitializeHardware(void)
{
  TRCIN( TRC_ID_VIP, "" );
  /* This will be executed only once */
  if(!m_bHwInitDone)
  {
    /* Reset VIP Registers */
    WriteReg(TVO_VIP_CTL_OFFSET, m_ulVipCfg);

    m_bHwInitDone = true;
  }

  TRCOUT( TRC_ID_VIP, "" );
}


void CSTmVIP::SelectSync(const tvo_vip_sync_source_t sync_source)
{
  VIP_TRCIN( TRC_ID_VIP, "Input is %s", ((sync_source == TVO_VIP_MAIN_SYNCS)? "Main":"Aux") );

  int      idx = 0;
  uint32_t SyncSel;
  uint32_t SyncSelMainOrAux;
  uint32_t SyncType = m_ulSyncType;

  InitializeHardware();

  SyncSel = ReadReg(TVO_VIP_SYNC_SEL_OFFSET);

  SyncSelMainOrAux = (sync_source == TVO_VIP_MAIN_SYNCS)?TVO_VIP_SYNC_SEL_OFFSET_MAIN:TVO_VIP_SYNC_SEL_OFFSET_AUX;

  SyncType &= ~(TVO_VIP_SYNC_TYPE_VGA|TVO_VIP_SYNC_TYPE_VGA_DVO_PADS);
  for(idx=0; idx < TVO_VIP_SYNC_NBR; idx++)
  {
    if(SyncType & (1 << idx))
      SetBitField(SyncSel, tvo_vip_sync_shift[idx], TVO_VIP_SYNC_SEL_WIDTH, (SyncSelMainOrAux | m_sync_sel_map[idx]));
  }

  VIP_TRC( TRC_ID_VIP, "SyncSel = 0x%x, type = 0x%x", SyncSel, m_ulSyncType );

  WriteReg(TVO_VIP_SYNC_SEL_OFFSET, SyncSel);

  if ((m_ulSyncType & TVO_VIP_SYNC_TYPE_HDF)&&(m_ulSyncType & TVO_VIP_SYNC_TYPE_VGA))
  {
    SyncSel = ReadReg(TVO_VIP_SYNC_SEL_VGA_OFFSET);

    SyncSelMainOrAux = (sync_source == TVO_VIP_MAIN_SYNCS)?TVO_VIP_SYNC_SEL_OFFSET_MAIN:TVO_VIP_SYNC_SEL_OFFSET_AUX;

    SyncType = m_ulSyncType&TVO_VIP_SYNC_TYPE_VGA;
    for(idx=0; idx < TVO_VIP_SYNC_NBR; idx++)
    {
      if(SyncType & (1 << idx))
        SetBitField(SyncSel, tvo_vip_sync_shift[idx], TVO_VIP_SYNC_SEL_WIDTH, (SyncSelMainOrAux | m_sync_sel_map[idx]));
    }

    VIP_TRC( TRC_ID_VIP, "VGA PADs SyncSel = 0x%x", SyncSel );

    WriteReg(TVO_VIP_SYNC_SEL_VGA_OFFSET, SyncSel);
  }
  else if ((m_ulSyncType & TVO_VIP_SYNC_TYPE_HDF)&&(m_ulSyncType & TVO_VIP_SYNC_TYPE_VGA_DVO_PADS))
  {
    SyncSel = ReadReg(TVO_VIP_SYNC_SEL_VGA_DVO_PADS_OFFSET);

    SyncSelMainOrAux = (sync_source == TVO_VIP_MAIN_SYNCS)?TVO_VIP_SYNC_SEL_OFFSET_MAIN:TVO_VIP_SYNC_SEL_OFFSET_AUX;

    SyncType = m_ulSyncType&TVO_VIP_SYNC_TYPE_VGA_DVO_PADS;
    for(idx=0; idx < TVO_VIP_SYNC_NBR; idx++)
    {
      if(SyncType & (1 << idx))
        SetBitField(SyncSel, tvo_vip_sync_shift[idx], TVO_VIP_SYNC_SEL_WIDTH, (SyncSelMainOrAux | m_sync_sel_map[idx]));
    }

    VIP_TRC( TRC_ID_VIP, "VGA PADs(DVO) SyncSel = 0x%x", SyncSel );

    WriteReg(TVO_VIP_SYNC_SEL_VGA_DVO_PADS_OFFSET, SyncSel);
  }
  VIP_TRCOUT( TRC_ID_VIP, "");
}


bool CSTmVIP::SetForceColor(const bool EnableFC, uint32_t R_Cr, uint32_t G_Y, uint32_t B_Cb)
{
  VIP_TRCIN( TRC_ID_VIP, "HDF ForceColor is %s, R_Cr=0x%x G_Y=0x%x B_Cb=0x%x", (EnableFC? "Enabled":"Disabled"), R_Cr, G_Y, B_Cb );

  uint32_t VipCfg;
  uint32_t force_color_0 = 0;
  uint32_t force_color_1 = 0;

  VipCfg = m_ulVipCfg;

  if(EnableFC)
  {
    SetBitField(force_color_1, TVO_VIP_FORCE_COLOR_R_CR_SHIFT, TVO_VIP_FORCE_COLOR_WIDTH  , R_Cr);
    SetBitField(force_color_0, TVO_VIP_FORCE_COLOR_G_Y_SHIFT , TVO_VIP_FORCE_COLOR_WIDTH  , G_Y );
    SetBitField(force_color_0, TVO_VIP_FORCE_COLOR_B_CB_SHIFT, TVO_VIP_FORCE_COLOR_WIDTH  , B_Cb);
    SetBitField(VipCfg       , TVO_VIP_SEL_INPUT_SHIFT   , TVO_VIP_SEL_INPUT_WIDTH, TVO_VIP_SEL_INPUT_FORCE_COLOR);
  }

  WriteReg(TVO_VIP_CTL_OFFSET      , VipCfg);
  WriteReg(TVO_VIP_FORCE_COLOR_0, force_color_0);
  WriteReg(TVO_VIP_FORCE_COLOR_1, force_color_1);

  VIP_TRCOUT( TRC_ID_VIP, "" );
  return true;
}


bool CSTmVIP::SetAdaptiveDecimationFilterMode(stm_vout_pf_adf_mode_t  filter_mode)
{
  VIP_TRCIN( TRC_ID_VIP, "");

  if ((m_ulCapabilities & TVO_VIP_HAS_MAIN_FILTERED_422_INPUT) == 0)
  {
    VIP_TRC( TRC_ID_ERROR, "422 sampling is not supported" );
    return false;
  }

  if ((m_ulVipCfg & TVO_VIP_SEL_INPUT_MAIN_FILTERED) != TVO_VIP_SEL_INPUT_MAIN_FILTERED)
  {
    VIP_TRC( TRC_ID_ERROR, "422 sampling is not selected");
    return false;
  }

  if (!(m_pMainPreformatter->SetAdaptiveDecimationFilterMode(filter_mode)))
    return false;

  VIP_TRCOUT( TRC_ID_VIP, "");
  return true;

}
