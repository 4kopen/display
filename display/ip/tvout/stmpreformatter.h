/***********************************************************************
 *
 * File: display/ip/tvout/stmpreformatter.h
 * Copyright (c) 2011 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STM_REFORMATTER_H
#define _STM_REFORMATTER_H

#include <stm_display.h>

class CDisplayDevice;

typedef struct stm_vout_pf_mat_coef_s
{
  uint32_t ycbcr_to_rgb[3][8];
  uint32_t rgb_to_ycbcr[3][8];
} stm_vout_pf_mat_coef_t;


typedef enum stm_vout_pf_mat_direction_e
{
    VOUT_PF_CONV_YUV_TO_RGB
  , VOUT_PF_CONV_RGB_TO_YUV
} stm_vout_pf_mat_direction_t;

typedef enum stm_vout_pf_adf_mode_e
{
    VOUT_PF_ADF_MODE_ADF
  , VOUT_PF_ADF_MODE_LINEAR
  , VOUT_PF_ADF_MODE_EVEN_SAMPLE_DROP
  , VOUT_PF_ADF_MODE_ODD_SAMPLE_DROP
  , VOUT_PF_ADF_MODE_NBR
} stm_vout_pf_adf_mode_t;

#define ADF_INVALID_MODE    VOUT_PF_ADF_MODE_NBR

/* Public trace macros: exposed here so that derived classes can use them */
/* These macros add the Preformatter name at the beginning of all traces        */
#define PF_TRC(id,fmt,args...)       TRC   (id, "%s - " fmt, GetName(), ##args)
#define PF_TRCIN(id,fmt,args...)     TRCIN (id, "%s - " fmt, GetName(), ##args)
#define PF_TRCOUT(id,fmt,args...)    TRCOUT(id, "%s - " fmt, GetName(), ##args)
#define PF_TRCBL(id)                 TRCBL (id, "%s", GetName())

typedef enum stm_vout_pf_capabilities_e
{
    VOUT_PF_CAPS_ADF               = (1L<<0) /* has Adaptive Decimation Filter for 422 sampling */
  , VOUT_PF_CAPS_VDF               = (1L<<1) /* has Vertical Decimation Filter for 420 sampling */
  , VOUT_PF_CAPS_COMPO_SYNC_SELECT = (1L<<2) /* can select vtg sync feeding compositor */
} stm_vout_pf_capabilities_t;

class CSTmPreformatter
{
public:
  CSTmPreformatter(const char                   *name,
                   CDisplayDevice               *pDev,
                   uint32_t                      ulPFRegs,
                   uint32_t                      capabilities = 0);

  virtual ~CSTmPreformatter(void);

  const char *GetName(void) const { return m_name ? m_name : ""; }

  bool SetColorSpaceConversion(stm_ycbcr_colorspace_t colorspaceMode, stm_vout_pf_mat_direction_t direction = VOUT_PF_CONV_RGB_TO_YUV);
  bool SetAdaptiveDecimationFilterMode(stm_vout_pf_adf_mode_t  filter_mode);
  bool GetAdaptiveDecimationFilterMode(stm_vout_pf_adf_mode_t *filter_mode);

  bool EnableVerticalDecimationFilter(bool enable);
  bool IsVDFEnabled(void){ return m_bIsVDFEnabled; };

  uint32_t GetCapabilities(void){ return m_ulCapabilities; };

protected:
  const char                   *m_name;
  uint32_t                     *m_pDevRegs;
  uint32_t                      m_ulPFRegOffset;
  uint32_t                      m_ulCapabilities;
  const stm_vout_pf_mat_coef_t *m_pPFConversionMat;
  stm_vout_pf_adf_mode_t        m_adf_mode;
  bool                          m_bIsVDFEnabled;

  void WriteReg(uint32_t reg, uint32_t val) { vibe_os_write_register(m_pDevRegs, (m_ulPFRegOffset + reg), val);}
  uint32_t ReadReg(uint32_t reg) { return vibe_os_read_register(m_pDevRegs, (m_ulPFRegOffset + reg)); }

private:
  CSTmPreformatter(const CSTmPreformatter&);
  CSTmPreformatter& operator=(const CSTmPreformatter&);
};


#endif //_STM_REFORMATTER_H
