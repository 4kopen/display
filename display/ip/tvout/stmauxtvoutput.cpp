/***********************************************************************
 *
 * file: display/ip/tvout/stmauxtvoutput.cpp
 * Copyright (c) 2009-2011 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <stm_display.h>

#include <vibe_os.h>
#include <vibe_debug.h>
#include <display/generic/DisplayDevice.h>
#include <display/generic/DisplayMixer.h>

#include <display/ip/stmdenc.h>
#include <display/ip/displaytiming/stmvtg.h>
#include <display/ip/hdf/stmhdf.h>

#include "stmauxtvoutput.h"

//////////////////////////////////////////////////////////////////////////////
//
// HD, SD Progressive and SD Interlaced Output on Aux pipeline
//
CSTmAuxTVOutput::CSTmAuxTVOutput(
    const char            *name,
    uint32_t               id,
    CDisplayDevice        *pDev,
    CSTmVTG               *pVTG,
    CSTmDENC              *pDENC,
    CDisplayMixer         *pMixer,
    CSTmHDFormatter       *pHDFormatter): CSTmMasterOutput(name, id, pDev, pDENC, pVTG, pMixer)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "pDENC = %p , pHDFormatter = %p", pDENC, pHDFormatter);

  m_ulCapabilities |= (OUTPUT_CAPS_SD_ANALOG             |
                       OUTPUT_CAPS_ED_ANALOG             |
                       OUTPUT_CAPS_HD_ANALOG             |
                       OUTPUT_CAPS_EXTERNAL_SYNC_SIGNALS |
                       OUTPUT_CAPS_CVBS_YC_EXCLUSIVE     |
                       OUTPUT_CAPS_YPbPr_EXCLUSIVE       |
                       OUTPUT_CAPS_RGB_EXCLUSIVE         |
                       OUTPUT_CAPS_SD_RGB_CVBS_YC        |
                       OUTPUT_CAPS_SD_YPbPr_CVBS_YC);

  m_pHDFormatter = pHDFormatter;
  m_uExternalSyncShift = 0;
  m_bInvertExternalSyncs = false;
  m_uDENCSyncOffset = 0;
  m_bHwInitDone = false;

  /* Set default Output format */
  m_ulOutputFormat = (STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC);

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}


CSTmAuxTVOutput::~CSTmAuxTVOutput() {}


void CSTmAuxTVOutput::InitializeHardware(void)
{
  if(!m_bHwInitDone)
  {
    TRCIN( TRC_ID_TVOUT, "" );

    m_bHwInitDone = true;

    TRCOUT( TRC_ID_TVOUT, "" );
  }
}


/*
 * Helper for mode validity check :
 *
 * This helper is used to check if the given mode can be supported by this output or not.
 * It is currently used by both this output and by the STKPI :
 *
 * 1. Used by this output in order to check if installed Mode is compatible or not with the
 *   new output's format : See call in CSTmAuxTVOutput::SetOutputFormat()
 *
 * 2. Used by STKPI when trying to start this output with a new display mode :
 *   See call in CSTmAuxTVOutput::SupportedMode()
 */
const stm_display_mode_t* CSTmAuxTVOutput::CheckSupportedMode(const stm_display_mode_t *mode) const
{
  OUTPUT_TRCIN( TRC_ID_UNCLASSIFIED, "" );

  if (m_pHDFormatter)
  {
    if(mode->mode_params.output_standards & STM_OUTPUT_STD_NTG5)
    {
      OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "Looking for valid NTG5 mode, pixclock = %u", mode->mode_timing.pixel_clock_freq);
      return mode;
    }

    if(mode->mode_params.output_standards & (STM_OUTPUT_STD_HD_MASK | STM_OUTPUT_STD_XGA))
    {
      OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "Looking for valid HD/XGA mode, pixclock = %u", mode->mode_timing.pixel_clock_freq);

      if(mode->mode_params.output_standards & STM_OUTPUT_STD_AS4933)
      {
        OUTPUT_TRCOUT( TRC_ID_TVOUT, "No support for AS4933 yet");
        return 0;
      }

      if(mode->mode_timing.pixel_clock_freq < 65000000 ||
         mode->mode_timing.pixel_clock_freq > 148500000)
      {
        OUTPUT_TRC( TRC_ID_TVOUT, "pixel clock out of range");
        return 0;
      }

      return mode;
    }

    /*
     * We support SD  progressive modes at 27Mhz or 27.027Mhz. We also report
     * support for SD interlaced modes over HDMI where the clock is doubled and
     * pixels are repeated. Finally we support VGA (640x480p@59.94Hz and 60Hz)
     * for digital displays (25.18MHz pixclock)
     */
    if(mode->mode_params.output_standards & (STM_OUTPUT_STD_ED_MASK | STM_OUTPUT_STD_VGA))
    {
      OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "Looking for valid SD progressive mode");
      if(mode->mode_timing.pixel_clock_freq < 25174800 ||
         mode->mode_timing.pixel_clock_freq > 27027000)
      {
        OUTPUT_TRC( TRC_ID_TVOUT, "pixel clock out of range");
        return 0;
      }

      return mode;
    }
  }


  /*
   * We support SD interlaced modes based on a 13.5Mhz pixel clock
   */
  if(mode->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK)
  {
    OUTPUT_TRC( TRC_ID_UNCLASSIFIED, "Looking for valid SD interlaced mode");
    if(mode->mode_timing.pixel_clock_freq < 13500000 ||
       mode->mode_timing.pixel_clock_freq > 13513500)
    {
      OUTPUT_TRC( TRC_ID_TVOUT, "pixel clock out of range , pixclock = %u\n", mode->mode_timing.pixel_clock_freq);
      return 0;
    }
    if(m_pDENC && ((m_pDENC->GetOwner() == this)||(m_pDENC->GetOwner() == 0)))
    {
      return mode;
    }
  }

  return 0;
}


const stm_display_mode_t* CSTmAuxTVOutput::SupportedMode(const stm_display_mode_t *mode) const
{
  const stm_display_mode_t* supported_mode = 0;
  OUTPUT_TRCIN( TRC_ID_UNCLASSIFIED, "" );

  if(!IsModeTimingValid(*mode))
  {
    OUTPUT_TRCOUT( TRC_ID_ERROR, "Invalid given mode : %ux%u-%u%c - std=%x",
            mode->mode_params.active_area_width, mode->mode_params.active_area_height,
            (mode->mode_params.vertical_refresh_rate/1000),
            (mode->mode_params.scan_type == STM_INTERLACED_SCAN ? 'i' : ' '),
            mode->mode_params.output_standards );
    return 0;
  }

  supported_mode = CheckSupportedMode(mode);

  if(!supported_mode)
  {
    /*
     * Check if given mode is already supported by Slaved output
     */
    supported_mode = CSTmMasterOutput::SupportedMode(mode);
  }

  OUTPUT_TRCOUT( TRC_ID_UNCLASSIFIED, "" );
  return supported_mode;
}


OutputResults CSTmAuxTVOutput::Start(const stm_display_mode_t *mode)
{
  OutputResults retval;
  const stm_display_mode_t *supported;

  OUTPUT_TRCIN( TRC_ID_MAIN_INFO, "VTG=%s", m_pVTG->GetName());

  /* for non-custom modes, check if the standard is valid for that mode. */
  supported = GetModeParamsLine(mode->mode_id);
  if(supported)
  {
    stm_output_standards_e requested =
      static_cast<stm_output_standards_e>(mode->mode_params.output_standards);

    if (!(requested & supported->mode_params.output_standards))
    {
      OUTPUT_TRC( TRC_ID_ERROR, "" );
      return STM_OUT_INVALID_VALUE;
    }
  }

  /*
   * Init the HW: this is will be executed only once
   */
  InitializeHardware();

  /*
   * Check compatibility between new Mode and current output format
   */
  retval = CheckOutputConfiguration(mode, m_ulOutputFormat);
  if(retval != STM_OUT_OK)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "New mode is not compatible with current output format" );
    return retval;
  }

  if(m_bIsStarted)
  {
    m_bPendingStart = true;
    Stop();
  }

  /*
   * Make sure we can use the VTG, on older SoCs it may be used by the main
   * path doing the "main to DENC" case as a slaved VTG.
   */
  if(m_pVTG->IsStarted())
  {
    OUTPUT_TRC( TRC_ID_ERROR, "VTG required but it is in use by the main output" );
    return STM_OUT_BUSY;
  }

  /*
   * Determine if we are going to grab the DENC, we need to do this first
   * so subsequent logic depending on m_bUsingDENC will do the right thing.
   */
  if(mode->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK)
  {
    if(!m_pDENC->IsStarted())
      m_bUsingDENC = (m_ulOutputFormat != 0);
    if(m_bUsingDENC)
      OUTPUT_TRC( TRC_ID_TVOUT, "Using DENC %p", m_pDENC );
  }

  if(!SetVTGSyncs(mode))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "SetVTGSyncs failed" );
    return STM_OUT_INVALID_VALUE;
  }

  /*
   * Note: We call the base class Start() even when the output is already
   *       started to allow on-the-fly mode changes that require only the pixel
   *       clock frequency or DENC standard to be modified.
   */
  retval=CSTmMasterOutput::Start(mode);

  if(retval != STM_OUT_OK)
  {
    OUTPUT_TRC( TRC_ID_ERROR, "failed to start master output" );
  }

  /*
   * Must do this after we start the VTGs and the FSynth otherwise
   * you get the wrong FSynth frequency for the clock dividers.
   */
  ConfigureDisplayClocks(mode, m_ulOutputFormat);

  /* Notify subscriber about output mode change */
  if(CSTmMasterOutput::NotifyEvent(STM_OUTPUT_START_EVENT))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "failed to notify output mode change event" );
  }

  OUTPUT_TRCOUT( TRC_ID_MAIN_INFO, "");
  return retval;
}


bool CSTmAuxTVOutput::Stop(void)
{
  /* Don't wait for Vsync Counting if no planes are connected to the Mixer */
  uint32_t MixerStopVSyncCount = 0;

  OUTPUT_TRCIN( TRC_ID_MAIN_INFO, "VTG=%s", m_pVTG->GetName());

  /* Notify subscriber about output stop */
  if(CSTmMasterOutput::NotifyEvent(STM_OUTPUT_STOP_EVENT))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "failed to notify output stop event" );
  }

  /*
   * Planes could not be re-activated by a VSync, they need a new UpdateFromOutput()
   * with another reason than STM_PLANE_OUTPUT_STOPPED. So no need to protect with m_lock.
   */
  if(m_pMixer->HasEnabledPlanes())
  {
    OUTPUT_TRC( TRC_ID_TVOUT, "mixer has enabled planes" );
    m_pMixer->UpdateFromOutput(this, STM_PLANE_OUTPUT_STOPPED);

    /*
     * It is necessary to wait for 5 Vsyncs in order to ensure the connected
     *  Planes has actually stopped properly.
     */
    MixerStopVSyncCount = 5;
  }

  vibe_os_lock_resource(m_lock);

  if(!m_bPendingStart)
  {
    // Stop slaved digital outputs.
    StopSlavedOutputs();
  }

  DisableDACs();

  // Stop HD Path.
  if(m_pHDFormatter->GetOwner() == this)
    m_pHDFormatter->Stop();

  // Stop SD Path.
  if(m_pDENC->GetOwner() == this)
    m_pDENC->Stop();

  vibe_os_unlock_resource(m_lock);

  StopMixer(MixerStopVSyncCount);

  if(m_bIsStarted)
  {
    /*
     * If the output was properly running it is necessary to wait for vsyncs
     * in order to ensure the AWG has actually stopped. It also allows the
     * mixer enable register to latch 0 (all planes disabled) into the hardware.
     *
     * So we just wait here... but waiting for only one VSync is not always
     * enough, so we wait for 2 VSyncs (frame duration) to ensure hardware
     * update is done.
     */
    StopVTG(2);

    vibe_os_lock_resource(m_lock);
    {
      m_PendingMode.mode_id = STM_TIMING_MODE_RESERVED;
      COutput::Stop();
    }
    vibe_os_unlock_resource(m_lock);

  }

  DisableClocks();

  m_bUsingDENC = false;

  OUTPUT_TRCOUT( TRC_ID_MAIN_INFO, "");

  return true;
}


bool CSTmAuxTVOutput::RestartDENC(uint32_t format)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(m_pDENC->IsStarted())
  {
    if(m_bUsingDENC)
    {
      /*
       * If the denc was previously started correctly then we should report an error here !!
       */
      OUTPUT_TRC( TRC_ID_ERROR, "DENC already in use !!" );
      return false;
    }

    OUTPUT_TRC( TRC_ID_TVOUT, "DENC already started." );
    return true;
  }

  OUTPUT_TRC( TRC_ID_TVOUT, "output currently disabled, restarting DENC" );

  if(!m_pDENC->SetMainOutputFormat(format))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "Requested format (0x%x) not supported on the DENC",format );
    goto failed;
  }

  if(!m_pDENC->Start(this, &m_CurrentOutputMode))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "DENC start failed" );
    goto failed;
  }

  /*
   * Now we have started the DENC, set this output's PSI and
   * signal range control setup.
   */
  this->ProgramPSIControls();
  m_pDENC->SetControl(OUTPUT_CTRL_CLIP_SIGNAL_RANGE, (uint32_t)m_signalRange);

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return true;
failed:
  m_bUsingDENC = false;
  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return false;
}


/*
 * Helpers for CheckOutputConfiguration below
 */
const OutputResults CSTmAuxTVOutput::CheckOutputConfiguration(const stm_display_mode_t *mode, const uint32_t format) const
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "mode_id = %d, std=0x%x, format = 0x%x", mode->mode_id, mode->mode_params.output_standards, format );

  /*
   * Output format STM_VIDEO_OUT_NONE is always ok ;o)
   */
  if(format == STM_VIDEO_OUT_NONE)
    goto exit_ok;

  /***********************************************************
   * STEP 1 : Check if format can be supported by given mode *
   * Exit with error : STM_OUT_INVALID_VALUE                 *
   ***********************************************************/
  /*
   * We support CVBS and YC formats only for SD Modes
   */
  if( (format & (STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC))
  && ((mode->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK) == 0))
  {
    OUTPUT_TRCOUT( TRC_ID_ERROR, "CVBS/YC formats are available only for SD Modes !!" );
    return STM_OUT_INVALID_VALUE;
  }

  /***********************************************************
   * STEP 2 : Check if all required HW IPs are available     *
   * Exit with error : STM_OUT_BUSY                          *
   ***********************************************************/
  /*
   * Check if DENC is available or when required by this output
   */
  if(format & (STM_VIDEO_OUT_CVBS | STM_VIDEO_OUT_YC))
  {
    if(m_pDENC->IsStarted() && (m_pDENC->GetOwner() != this))
    {
      OUTPUT_TRCOUT( TRC_ID_ERROR, "DENC is already in use !!" );
      return STM_OUT_BUSY;
    }
  }
  /*
   * Check if HDFormatter is available or when required by this output
   */
  if(format & (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_YUV))
  {
    if(m_pHDFormatter->IsStarted() && (m_pHDFormatter->GetOwner() != this))
    {
      OUTPUT_TRCOUT( TRC_ID_ERROR, "HDFormatter is already in use !!" );
      return STM_OUT_BUSY;
    }
  }

  /*******************************************************************
   * STEP 3 : Check for not supported uses cases : HW/SW limitations *
   * Exit with error : STM_OUT_NOT_SUPPORTED                         *
   *******************************************************************/
  /*
   * SW Limitation : Bug 65626 !!
   * We support HD-RGB formats only with VGA standard !!
   */
  if ((format & STM_VIDEO_OUT_RGB) && !(m_ulCapabilities & OUTPUT_CAPS_RGB_EXCLUSIVE)
      && !(mode->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK))
  {
    OUTPUT_TRCOUT( TRC_ID_ERROR, "No VGA support, no sync pads" );
    return STM_OUT_NOT_SUPPORTED;
  }

  /*
   * HW Limitation : DENC Pix clock is hardwired to STM_CLK_PIX_AUX clock
   * This analog output can be used only when denc is free or already
   * in use by this output (STM_CLK_PIX_AUX is used by this pipeline)
   */
  if(m_pDENC->IsStarted() && (m_pDENC->GetOwner() != this))
  {
    OUTPUT_TRCOUT( TRC_ID_ERROR, "SD Mode is not supported on this output while DENC is in use by another output!!" );
    return STM_OUT_NOT_SUPPORTED;
  }

exit_ok:
  OUTPUT_TRCOUT( TRC_ID_TVOUT, "Selected Output format is ok" );
  return STM_OUT_OK;
}


/*
 * Helpers for SetOutputFormat below
 */
bool CSTmAuxTVOutput::SetOutputFormat(uint32_t format)
{
  OutputResults Result;
  OUTPUT_TRCIN( TRC_ID_TVOUT, "format = 0x%x", format );

  /*
   * Don't change the output setup unless the display is running.
   */
  if(!m_bIsStarted)
  {
    OUTPUT_TRCOUT( TRC_ID_TVOUT, "No display mode, nothing to do" );
    return true;
  }

  /*
   * Check if format can be configured in this output
   */
  Result = CheckOutputConfiguration(&m_CurrentOutputMode, format);

  /*
   * If user has provided unconsistant parameters then exit without doing anything
   */
  if(Result != STM_OUT_OK)
    return false;

  /*
   * Always stop the HD Formatter if we were currently using it.
   */
  if(m_pHDFormatter->GetOwner() == this)
      m_pHDFormatter->Stop();

  /*
   * Always stop the DENC if we were currently using it.
   */
  if(m_pDENC->GetOwner() == this)
    m_pDENC->Stop();

  if(!UpdateOutputFormat(format))
    return false;

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return true;
}


/*
 * Helpers for UpdateOutputFormat below
 */
bool CSTmAuxTVOutput::UpdateOutputFormat(uint32_t format)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "format = 0x%x", format );

  /*
   * Disable this output in case of :
   *
   * 1. Requested output configuration is not compatible with other outputs
   * 2. No selected format
   * 3. Current display mode can not be supported by this output.
   */
  if((format == STM_VIDEO_OUT_NONE) || !CheckSupportedMode(&m_CurrentOutputMode))
  {
    DisableDACs();

    DisableClocks();

    OUTPUT_TRCOUT( TRC_ID_TVOUT, "Analog output is disabled" );
    return true;
  }

  /*
   * SD Modes : Initially indicate that we are now using the DENC, so RestartDENC,
   * ConfigureOutput and ConfigureDisplayClocks will do the right thing.
   */
  if(m_CurrentOutputMode.mode_params.output_standards & STM_OUTPUT_STD_SD_MASK)
    m_bUsingDENC = !m_pDENC->IsStarted();

  /* Configure VIPs */
  if(!ConfigureOutput(format))
    return false;

  /* Configure VTG Syncs */
  if(!SetVTGSyncs(&m_CurrentOutputMode))
    return false;

  /* Configure clocks */
  ConfigureDisplayClocks(&m_CurrentOutputMode, format);

  /* Restart DENC if SD modes */
  if(m_CurrentOutputMode.mode_params.output_standards & STM_OUTPUT_STD_SD_MASK)
  {
    if(!RestartDENC(format))
      return false;
  }

  /*
   * Switch HD formatter for RGB(SCART) or YPrPb output from the aux pipeline
   */
  if((format & (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_YUV)) == 0)
  {
    OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
    return true;
  }

  OUTPUT_TRC( TRC_ID_TVOUT, "configuring HDFormatter %p", m_pHDFormatter );

  /* Start HDFormatter */
  if(!m_pHDFormatter->Start(this, &m_CurrentOutputMode))
  {
    OUTPUT_TRC( TRC_ID_ERROR, "HDFormatter Start Should Never Fail as we have already checked the owner" );
    return false;
  }

  /* Set AWG YC Offset */
  m_pHDFormatter->SetAWGYCOffset(m_AWG_Y_C_Offset);
  /* Set Denc source from Aux */
  m_pHDFormatter->SetDencSource(false);
  /* Set HDFormatter Input Format for Aux SCART */
  m_pHDFormatter->SetInputParams(false, format);
  /* Set HDFormatter OutputFormat */
  m_pHDFormatter->SetOutputFormat(format);

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return true;
}


uint32_t CSTmAuxTVOutput::SetControl(stm_output_control_t ctrl, uint32_t val)
{
  OUTPUT_TRC( TRC_ID_TVOUT, "ctrl = %d val = %u", ctrl, val );
  switch(ctrl)
  {
    case OUTPUT_CTRL_VIDEO_SOURCE_SELECT:
    {
      /* Just to maintain the API */
      if(val != STM_VIDEO_SOURCE_AUX_COMPOSITOR)
        return STM_OUT_INVALID_VALUE;
      break;
    }
    case OUTPUT_CTRL_AUDIO_SOURCE_SELECT:
    {
      /* Just to maintain the API */
      if(val != STM_AUDIO_SOURCE_NONE)
        return STM_OUT_INVALID_VALUE;
      break;
    }
    case OUTPUT_CTRL_EXTERNAL_SYNC_SHIFT:
    {
	  /*
       * Will take effect on the next output restart
       */
      m_uExternalSyncShift = val;
      break;
    }
    case OUTPUT_CTRL_EXTERNAL_SYNC_INVERT:
    {
	  /*
       * Will take effect on the next output restart
       */
      m_bInvertExternalSyncs = (val!=0);
      break;
    }
    default:
      return CSTmMasterOutput::SetControl(ctrl,val);
  }

  return STM_OUT_OK;
}


uint32_t CSTmAuxTVOutput::GetControl(stm_output_control_t ctrl, uint32_t *val) const
{
  switch(ctrl)
  {
    case OUTPUT_CTRL_EXTERNAL_SYNC_SHIFT:
      *val = m_uExternalSyncShift;
      break;
    case OUTPUT_CTRL_EXTERNAL_SYNC_INVERT:
      *val = m_bInvertExternalSyncs;
      break;
    case OUTPUT_CTRL_DAC_POWER_DOWN:
      *val = m_bDacPowerDown?1:0;
      break;
    default:
      return CSTmMasterOutput::GetControl(ctrl,val);
  }

  return STM_OUT_OK;
}


uint32_t CSTmAuxTVOutput::SetCompoundControl(stm_output_control_t ctrl, void *newVal)
{
  uint32_t ret = STM_OUT_NO_CTRL;
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );
  switch(ctrl)
  {
    case OUTPUT_CTRL_VIDEO_OUT_CALIBRATION:
    {
      /*
       * For SD Modes we're always trying to calibrate both DENC and HDF
       */
      if(m_pDENC && (m_pDENC->GetOwner() == this))
        ret = m_pDENC->SetCompoundControl(ctrl,newVal);

      if((ret == STM_OUT_OK) || (ret == STM_OUT_NO_CTRL))
      {
        if(m_pHDFormatter && (m_pHDFormatter->GetOwner() == this))
          ret = m_pHDFormatter->SetCompoundControl(ctrl,newVal);
      }
      break;
    }
    default:
      ret = CSTmMasterOutput::SetCompoundControl(ctrl,newVal);
      break;
  }
  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return ret;
}


uint32_t CSTmAuxTVOutput::GetCompoundControl(stm_output_control_t ctrl, void *val) const
{
  uint32_t ret = STM_OUT_NO_CTRL;

  switch(ctrl)
  {
    case OUTPUT_CTRL_VIDEO_OUT_CALIBRATION:
    {
      /*
       * For SD Modes we're always trying to get both DENC and HDF calibration values
       */
      if(m_pDENC && (m_pDENC->GetOwner() == this))
          ret = m_pDENC->GetCompoundControl(ctrl,val);

      if((ret == STM_OUT_OK) || (ret == STM_OUT_NO_CTRL))
      {
        if(m_pHDFormatter && (m_pHDFormatter->GetOwner() == this))
          ret = m_pHDFormatter->GetCompoundControl(ctrl,val);
      }
      break;
    }
    default:
      ret = CSTmMasterOutput::GetCompoundControl(ctrl,val);
      break;
  }

  return ret;
}


bool CSTmAuxTVOutput::SetFilterCoefficients(const stm_display_filter_setup_t *f)
{
  bool ret = false;
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );
  switch(f->type)
  {
    case HDF_COEFF_2X_LUMA ... HDF_COEFF_4X_CHROMA:
    {
      /* Verify if the HDFormatter is owned by the AUX output or no one use it to config HDFormatter filter coefficients */
      if(m_pHDFormatter&&((m_pHDFormatter->GetOwner() == this) || (m_pHDFormatter->GetOwner() == 0)))
      {
        ret = m_pHDFormatter->SetFilterCoefficients(f);
      }
      break;
    }
    case DENC_COEFF_LUMA:
    case DENC_COEFF_CHROMA:
    {
      /* Verify if the m_pDENC is owned by the AUX output or no one use it to config m_pDENC filter coefficients */
      if(m_pDENC&&((m_pDENC->GetOwner() == this) || (m_pDENC->GetOwner() == 0)))
      {
        ret = m_pDENC->SetFilterCoefficients(f);
      }
      break;
    }
    default:
      break;
  }

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
  return ret;
}


void CSTmAuxTVOutput::Resume(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(!m_bIsSuspended)
    return;

  if(m_bBypassHwInit)
  {
    if(!m_bHwInitDone)
    {
      COutput::Resume();
      return;
    }
  }

  if(m_bIsStarted)
  {
    if(m_ulOutputFormat & (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_YUV))
      m_pHDFormatter->Resume();

    SetVTGSyncs(GetCurrentDisplayMode());
    SetSlavedOutputsVTGSyncs(GetCurrentDisplayMode());
  }

  CSTmMasterOutput::Resume();

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}



void CSTmAuxTVOutput::Suspend(void)
{
  OUTPUT_TRCIN( TRC_ID_TVOUT, "" );

  if(m_bIsSuspended)
    return;

  if(m_bBypassHwInit)
  {
    if(!m_bHwInitDone)
    {
      COutput::Suspend();
      return;
    }
  }

  SetOutputFormat(STM_VIDEO_OUT_NONE);

  CSTmMasterOutput::Suspend();

  OUTPUT_TRCOUT( TRC_ID_TVOUT, "" );
}
