/***********************************************************************
 *
 * File: private/include/display_device_priv.h
 * Copyright (c) 2013 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef DISPLAY_DEVICE_PRIV_H
#define DISPLAY_DEVICE_PRIV_H

#if defined(__cplusplus)
extern "C" {
#endif

#define STKPI_LOCK(pDev)            pDev->StkpiLock(__PRETTY_FUNCTION__)
#define STKPI_UNLOCK(pDev)          pDev->StkpiUnlock(__PRETTY_FUNCTION__)

#define IS_DEVICE_SUSPENDED(pDev)   pDev->PmIsSuspended(__PRETTY_FUNCTION__)

/*!
 * \brief Structure that contains the device configuration.
 *
 * \apis ::stm_display_device_create()
 */
typedef struct stm_device_configuration_s
{
  uint32_t              id;
  uint32_t              major;
  uint32_t              minor;
  bool                  no_hw_init;
} stm_device_configuration_t;


/*****************************************************************************
 * C private interface for display device
 */

/*!
 * Create the display device.
 *
 * \param id     A device identifier: 0..n..
 * \param device The returned device handle.
 *
 * \returns 0       Success
 * \returns -ENODEV The device could not be created
 * \returns -EFAULT The device parameter pointer is an invalids addresses
 */
extern int stm_display_device_create(stm_device_configuration_t *device_cfg, stm_display_device_h *device);

/*!
 * Destroy the display device.
 *
 * \param device The device handle.
 *
 */
extern void stm_display_device_destroy(stm_display_device_h device);

/*!
 * Register the display device power hooks.
 *
 * \param dev Display device to query.
 * \param get A pointer to the power pm_runtime_get() function
 * \param put A pointer to the power pm_runtime_put() function
 *
 * \returns 0       Success
 * \returns -EINTR  The call was interrupted while obtaining the device lock
 * \returns -EINVAL The device handle was invalid
 * \returns -EFAULT The power hooks parameter pointers are an invalids addresses
 */
extern int stm_display_device_register_pm_runtime_hooks(stm_display_device_h dev, int (*get)(const uint32_t id), int (*put)(const uint32_t id));

/*!
 * Get the display device power status.
 *
 * \param dev Display device to query.
 * \param pm_state A pointer to the power state's value
 *
 * \returns 0       Success
 * \returns -EINTR  The call was interrupted while obtaining the device lock
 * \returns -EINVAL The device handle was invalid
 * \returns -EFAULT The power status parameter pointer is an invalid address
 */
extern int stm_display_device_get_power_state(stm_display_device_h dev, uint32_t *pm_state);

/*!
 * Power down video DACs.
 *
 * \param dev Display device to query.
 *
 * \returns 0       Success
 * \returns -EINTR  The call was interrupted while obtaining the device lock
 * \returns -EINVAL The device handle was invalid
 */
extern int stm_display_device_power_down_video_dacs(stm_display_device_h dev);

/*!
 * Remove output capabilities which are not supported by h/w.
 * for example, if we can not claim VGA sync pads then remove VGA caps.
 *
 * \param dev Display device to query.
 * \param caps bit mask of output caps to remove.
 *
 * \returns -EINTR  The call was interrupted while obtaining the device lock
 * \returns -EINVAL The device handle was invalid
 */
extern int stm_display_output_remove_capabilities(stm_display_device_h dev, uint32_t caps);

/*!
 * Update all programming, for hardware managed by a device that is clocked by
 * the provided video timing generator identifier, for the next video frame or
 * field. Suitable video timing generator identifiers can be obtained, for
 * instance for a display output timing generator using
 * ::stm_display_output_get_timing_identifier(). Timing identifiers may also be
 * obtained for display sources that manage an independently clocked buffer
 * queue.
 *
 * This is intended to be called as a result of a VSync interrupt for the given
 * timing generator by the OS/platform component that provides system support
 * for the CoreDisplay kernel API, which includes the OS specific first level
 * interrupt handlers for the display hardware.
 *
 *
 * \note This call can result in memory allocated from the OS heap being
 * released. If that is illegal in interrupt context then this must be called
 * from a high priority task context instead. On Linux it must happen as part
 * of the VSync interrupt handling as scheduling tasklets or kernel threads
 * can be delayed too long.
 *
 * \param device   device to update
 * \param timing_id timing generator we are updating for.
 */
extern void stm_display_device_update_vsync_irq(stm_display_device_h device, uint32_t timing_id);

/*!
 * Same as stm_display_device_update_vsync_irq() but for operations for Vsync event in threaded IRQ context.
 */
extern void stm_display_device_update_vsync_threaded_irq(stm_display_device_h dev, uint32_t timing_id);

/*!
 * Freeze the given display device. This may cause the API
 * implementation to shut down all of its resources. Ressources are not
 * released and they are putted in reset state after this call completes.
 *
 * \param device           A handle to the device object to freeze
 *
 * \returns 0       Success
 * \returns -EINTR  The call was interrupted while obtaining the device lock.
 * \returns -EINVAL The device handle was invalid.
 */
extern int stm_display_device_freeze(stm_display_device_h device);

/*!
 * Suspend the given display device. This may cause the API
 * implementation to shut down all of its resources. Ressources are not
 * released and they are putted in reset state after this call completes.
 *
 * \param device           A handle to the device object to suspend
 *
 * \returns 0       Success
 * \returns -EINTR  The call was interrupted while obtaining the device lock.
 * \returns -EINVAL The device handle was invalid.
 */
extern int stm_display_device_suspend(stm_display_device_h device);

/*!
 * Resume the given display device. This may cause the API
 * implementation to restart all of its resources. Some of the ressources
 * are not restarted with the original previous configuration as they
 * were putted in reset state.
 *
 * \param device           A handle to the device object to resume
 *
 * \returns 0       Success
 * \returns -EINTR  The call was interrupted while obtaining the device lock.
 * \returns -EINVAL The device handle was invalid.
 */
extern int stm_display_device_resume(stm_display_device_h device);

/*!
 * Infor the display device that shutting down process is now started
 * so it is no more needed to check the power state.
 *
 * \param device           A handle to the device object
 *
 * \returns 0       Success
 * \returns -EINTR  The call was interrupted while obtaining the device lock.
 * \returns -EINVAL The device handle was invalid.
 */
extern int stm_display_device_shutting_down(stm_display_device_h device);


#if defined(__cplusplus)
}
#endif

#endif
