/************************************************************************
Copyright (C) 2010 STMicroelectronics. All Rights Reserved.

This file is part of the Display Engine.

Display Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Display Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Display Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Display Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef _STM_DISPLAY_SOURCE_QUEUE_H
#define _STM_DISPLAY_SOURCE_QUEUE_H

#include "stm_display_common.h"

#if defined(__cplusplus)
extern "C" {
#endif

/*! \file stm_display_source_queue.h
 *  \brief C interface to in-memory display source buffer queues
 */

/*!
 * \brief Source Queue Interface handle type
 */

typedef struct stm_display_source_queue_s *stm_display_source_queue_h;


typedef enum stm_display_source_queue_event_e
{
    SRC_QUEUE_OUTPUT_DISPLAY_MODE_CHANGE        = (1L<<0), /*!< The output display mode has changed                                                 */
    SRC_QUEUE_CONNECTION_CHANGE                 = (1L<<1)  /*!< Some connections between this source and the planes or outputs have changed         */
}stm_display_source_queue_event_t;


/* "event"     = bit-field indicating what events have occured (Or'd stm_display_source_queue_event_t values)       */
/* "user_data" = context data passed to stm_display_source_queue_set_listener() and given back by the listener.     */
typedef void (*stm_display_source_queue_listener)(uint32_t event, void* user_data);


/*****************************************************************************
 * C interface to in-memory source buffer queues
 *
 */

/*!
 * This function fills an array with a list of supported pixel formats that can
 * be read by this memory source. This will be the list of pixel formats
 *  supported by the planes connected to this source.
 *
 * \note The list itself is owned by the source and must not be modified.
 *
 * \param q           Interface to query.
 * \param formats     Pointer to an array to fill in with the supported
 *                    pixel formats.
 * \param max_formats Max number of elements in the array.
 *
 * \returns           The number of formats filled in the array on success.
 * \returns -EINVAL   The device handle was invalid.
 * \returns -EINTR    The call was interrupted while obtaining the device lock.
 * \returns -EFAULT   The parameter pointer is an invalid address.
 *
 */
extern int MUST_CHECK stm_display_source_queue_get_pixel_formats(stm_display_source_queue_h q,
                                                      stm_pixel_format_t *formats,
                                                      uint32_t max_formats);

/*!
 *  This function obtains exclusive use of the buffer queue for use by the given
 *  interface pointer.
 *
 * \param   q         Queue interface to lock.
 *
 * \returns 0         On success.
 * \returns -ENOLCK   The queue has already been locked for use by another
 *                    interface pointer.
 * \returns -EINVAL   The device handle is invalid.
 * \returns -EINTR    The call was interrupted while obtaining the device lock.
 * \returns -EAGAIN   The device is suspended, action is not done.
 *
 */
extern int MUST_CHECK stm_display_source_queue_lock(stm_display_source_queue_h q, ...);

/*!
 * Release this interface pointer's exclusive use of buffer queue. If the
 * interface pointer does not currently own the buffer queue lock this call has
 * no effect, this is not considered an error.
 * For the interface pointer owning the buffer queue, it is mandatory to empty
 * the buffer queue by a preliminary call to stm_display_source_queue_flush(),
 * else an error is returned.
 *
 * \param q The interface to unlock
 *
 *
 * \returns 0         On success.
 * \returns -ENOLCK   This interface pointer doesn't have exclusive access to
 *                    the buffer queue ( thanks to a preliminary call to
 *                    stm_display_source_queue_lock() ).
 * \returns -EINVAL   The device handle was invalid.
 * \returns -EINTR    The call was interrupted while obtaining the device lock.
 * \returns -EAGAIN   The device is suspended, action is not done.
 *
 */
extern int MUST_CHECK stm_display_source_queue_unlock(stm_display_source_queue_h q);

/*!
 * This function queues a buffer to be read by the source. Only the interface
 * pointer that owns the lock can call this function (the function would fail
 * otherwise).
 * If a flush is on-going, this fonction returns an error.
 * In case of error, the callback, indicating that the buffer is no more used,
 * will be never called.
 *
 * \note: The buffer descriptor contains information about the source, as well
 * as some callback functions to inform the caller when the buffer is displayed
 * and not needed anymore.

 * \param q The interface to queue the buffer on.
 * \param b The buffer descriptor to queue.
 *
 * \returns 0         On success.
 * \returns -ENOLCK   This interface pointer doesn't have exclusive access to
 *                    the buffer queue ( thanks to a preliminary call to
 *                    stm_display_source_queue_lock() ).
 * \returns -EINTR    The call was interrupted while obtaining the device lock.
 * \returns -EFAULT   The address of the buffer pointer is not valid.
 * \returns -EINVAL   The queue handle is invalid.
 * \returns -EAGAIN   The device is suspended, action is not done.
 *
 */
extern int MUST_CHECK stm_display_source_queue_buffer(stm_display_source_queue_h q,
                                           const stm_display_buffer_t *b);

/*!
 * This function removes buffers from the source's queue.
 * If flush_buffers_on_display is true, all the buffers are flushed.
 * But, if some buffers are currently used by hardware, this function will wait
 * one or two Vsync until they are no more used.
 * If it is False, all the buffers are flushed except the ones currently
 * displayed by a video planes.
 * Flush_buffers_on_display will be set to false when the client needs to
 * perform a Pause or a change of the playing speed. The display will then be
 * frozen on the current picture.
 * The display will restart when the client queues some new buffers to display.
 * Only the interface pointer that owns the lock can call this function
 * (the function would fail otherwise)
 *
 *
 * \param q                        Queue interface to flush.
 * \param flush_buffers_on_display Indicates if all of the buffers should be
 *                                 flushed, including the ones on display.
 *
 * \returns 0               On success.
 * \returns -ENOLCK         This interface pointer doesn't have exclusive access to
 *                          the buffer queue ( thanks to a preliminary call to
 *                          stm_display_source_queue_lock() ).
 * \returns -EINVAL         The device handle was invalid.
 * \returns -EINTR          The call was interrupted while obtaining the device lock.
 * \returns -ERESTARTSYS    The call was interrupted while waiting all buffers to be flushed.
 * \returns -EAGAIN         The device is suspended, action is not done.
 *
 */
extern int MUST_CHECK stm_display_source_queue_flush(stm_display_source_queue_h q,
                                          bool flush_buffers_on_display);

/*!
 * This function makes an internal copy of the currently displayed picture
 * before flushing the queue and releasing all the buffers injected by the
 * user of this source's queue. This is the main difference with the API
 * stm_display_source_queue_flush(queue, FALSE).
 * So this function will wait that the copy is done, then this new internal
 * buffer is injected in the source's queue and the copied buffer gets released.
 * The use case dedicated for this feature is the ZAP between 2 channels for
 * preventing to have both channels memory pools used during this transition.
 * The display will restart when the client queues some new buffers to display.
 * Only the interface pointer that owns the lock can call this function
 * (the function would fail otherwise)
 *
 *
 * \param q                 Queue interface to flush.
 *
 * \returns 0               On success.
 * \returns -ENOLCK         This interface pointer doesn't have exclusive access to
 *                          the buffer queue ( thanks to a preliminary call to
 *                          stm_display_source_queue_lock() ).
 * \returns -EINVAL         The device handle was invalid.
 * \returns -EINTR          The call was interrupted while obtaining the device lock.
 * \returns -ERESTARTSYS    The call was interrupted while waiting all buffers to be flushed.
 * \returns -EAGAIN         The device is suspended, action is not done.
 *
 */
extern int stm_display_source_queue_flush_with_copy(stm_display_source_queue_h q);

/*!
 * This function allows a user of a source queue to be notified
 * when an event happened on this source.
 *
 * The callback function will be called when one or more events happens.
 * Several events can happen at the same time so the events defined
 * in "stm_display_source_queue_event_t" are bit-vectored.
 *
 * Only the user who has locked this source queue interface can set a listener.
 *
 * The user can unsubscribe by calling the same function with a null listener.
 *
 *
 * \param q                                     Queue interface handle
 * \param stm_display_source_queue_listener     User callback function that will be
 *                                              notified when an even happens.
 * \param user_data                             User-data that will be passed back when calling
 *                                              the callback function
 *
 * \returns 0         On success.
 * \returns -EINVAL   The device handle was invalid.
 * \returns -EINTR    The call was interrupted while obtaining the device lock.
 * \returns -ENOLCK   This interface pointer doesn't have exclusive access to
 *                    the buffer queue ( thanks to a preliminary call to
 *                    stm_display_source_queue_lock() ).
 *
 */
extern int stm_display_source_queue_set_listener(stm_display_source_queue_h         q,
                                                 stm_display_source_queue_listener  listener,
                                                 void *                             user_data);


/*!
 * Release the given queue interface.
 *
 * If this interface has locked the source's queue and pushed some buffers in the past, it is
 * mandatory to flush then unlock it before calling this release function.
 *
 * \note The pointer is invalid after this call completes successfully.
 *
 * \param q Queue interface to release
 *
 * \returns 0         On success.
 * \returns -ENOLCK   This interface pointer doesn't have exclusive access to
 *                    the buffer queue ( thanks to a preliminary call to
 *                    stm_display_source_queue_lock() ).
 * \returns -EINVAL   The device handle was invalid.
 * \returns -EINTR    The call was interrupted while obtaining the device lock.
 * \returns -EAGAIN   The device is suspended, action is not done.
 *
 */
extern int MUST_CHECK stm_display_source_queue_release(stm_display_source_queue_h q);

#if defined(__cplusplus)
}
#endif

#endif /* _STM_DISPLAY_SOURCE_QUEUE_H */
