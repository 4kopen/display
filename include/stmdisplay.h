/************************************************************************
Copyright (C) 2006-2010 STMicroelectronics. All Rights Reserved.

This file is part of the Display Engine.

Display Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Display Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Display Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Display Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef _STMDISPLAY_H
#define _STMDISPLAY_H

/*! \file stmdisplay.h
 *  \brief This is the coredisplay API header file, this is all you need to include
*/
#include "stm_display.h"

#endif /* _STMDISPLAY_H */

