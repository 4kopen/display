/************************************************************************
Copyright (C) 2014 STMicroelectronics. All Rights Reserved.

This file is part of the Display Engine.

Display Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Display Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Display Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Display Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef STM_DISPLAY_COMMON_H_
#define STM_DISPLAY_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__KERNEL__)
#include <linux/compiler.h>
#if defined(CONFIG_STM_DISPLAY_MUST_CHECK)
#define MUST_CHECK __must_check
#else
#define MUST_CHECK
#endif
#else
#define MUST_CHECK
#endif

#ifdef __cplusplus
}
#endif

#endif /* STM_DISPLAY_COMMON_H_ */
