/************************************************************************
Copyright (C) 2015 STMicroelectronics. All Rights Reserved.

This file is part of the Display Engine.

Display Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Display Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Display Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Display Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef STM_DISPLAY_CP_H
#define STM_DISPLAY_CP_H

#if defined(__cplusplus)
extern "C" {
#endif

/**************************************************************************//**
 * \file stm_display_cp.h
 * \brief C Interface to display copy protection objects
 */

/*!
 * \brief Max user data lenght
 */
#define STMCP_MAX_USER_DATA               17 /* for MV 7.01 and CGMS TypeB */


/*!
 * \brief CP types
 */
typedef enum stmcp_type_s
{
  STMCP_TYPE_CGMS_SD                        /*!< CGMS-SD : PAL/NTSC - CVBS format                       */
, STMCP_TYPE_CGMS_HD                        /*!< CGMS-HD : 480p60/720p60/1080i60 - YUV format           */
, STMCP_TYPE_MACROVISION_SD                 /*!< CGMS-SD : PAL/NTSC - CVBS foramt                       */
, STMCP_TYPE_MACROVISION_HD                 /*!< CGMS-HD : 480p60/576p50 - YUV format                   */
, STMCP_TYPE_DCS                            /*!< DCS     : Not yet supported                            */
, STMCP_TYPE_UNKNOWN
, STMCP_TYPE_COUNT = STMCP_TYPE_UNKNOWN
} stmcp_type_t;


/*!
 * \brief CGMS SD predefined Modes
 */
typedef enum stmcp_cgms_sd_mode_s
{
  STMCP_CGMS_SD_COPY_FREELY                 /*!< Copying is always allowed                              */
, STMCP_CGMS_SD_COPY_NOMORE                 /*!< Only two copies are allowed                            */
, STMCP_CGMS_SD_COPY_ONCE                   /*!< Only one copy are allowed                              */
, STMCP_CGMS_SD_COPY_NEVER                  /*!< Copying is not allowed at all                          */
, STMCP_CGMS_SD_USER_DEFINED                /*!< User defined mode                                      */
} stmcp_cgms_sd_mode_t;


/*!
 * \brief CGMS HD predefined Modes
 */
typedef enum stmcp_cgms_hd_mode_s
{
  STMCP_CGMS_HD_TYPE_A                      /*!< CGMS-HD Type A : data len is 3 bytes                   */
, STMCP_CGMS_HD_TYPE_B                      /*!< CGMS-HD Type B : data len is 17 bytes                  */
} stmcp_cgms_hd_mode_t;


/*!
 * \brief Macrovision HD predefined Modes
 */
typedef enum stmcp_macrovision_hd_mode_s
{
  STMCP_MACROVISION_HD_AGC_OFF              /*!< Macrovision AGC-OFF                                    */
, STMCP_MACROVISION_HD_AGC_ON               /*!< Macrovision AGC-ON                                     */
} stmcp_macrovision_hd_mode_t;


/*!
 * \brief CP handle type
 */
typedef struct stm_display_cp_s * stm_display_cp_h ;


/******************************************************************************
 * C Interface to display copy protection objects
 */

/*!
 *  This function returns a cp handle that can be used in subsequent function
 *  calls that operate on a cp object. The first request for a handle to a
 *  particular cp identifier will cause that cp object to be
 *  initialized. The call will return -ENODEV if the requested cp
 *  identifier does not exist in a particular implementation or if the cp
 *  object cannot be initialized. CP identifiers are a contiguous integer
 *  sequence starting from 0; most SoC implementations will contain only one
 *  cp per type.
 *
 * \param  type    A CP type.
 * \param  id      A CP identifier: 0..n.
 * \param  cp      The returned cp handle.
 *
 * \returns 0         Success
 * \returns -EINTR    The call was interrupted while obtaining the cp lock.
 * \returns -EFAULT   The source output parameter pointer is an invalid address.
 * \returns -ENOTSUP  The source type is not supported by this device.
 * \returns -ENODEV   The source identifier does not exist on this device.
 *
 */
extern int stm_display_cp_open( const stmcp_type_t      type,
                                const int               id,
                                stm_display_cp_h *cp );

/*!
 * This function closes the given cp handle. This may cause the API
 * implementation to shut down and release all of its resources when the last
 * cp handle for a particular cp object is closed. The cp handle
 * is invalid after this call completes. Calling this function with a NULL
 * handle is allowed and will not cause an error or system failure.
 *
 * \param cp      The handle to close
 *
 */
extern void stm_display_cp_close(stm_display_cp_h cp);

/*!
 *  This function will enable copy protection on selected handle.
 *
 * \param  cp      The returned cp handle.
 *
 * \returns 0         Success
 * \returns -EINVAL   The device handle was invalid.
 * \returns -EALREADY CP is already enabled on selected handle.
 *
 */
extern int stm_display_cp_enable ( stm_display_cp_h cp );

/*!
 *  This function will disable copy protection on selected handle.
 *
 * \param  cp      The cp handle to enable
 *
 * \returns 0         Success
 * \returns -EINVAL   The device handle was invalid.
 * \returns -EALREADY CP is already disabled on selected handle.
 *
 */
extern int stm_display_cp_disable ( stm_display_cp_h cp );

/*!
 *  This function will set copy protection user data on selected handle.
 *
 * \param  cp      The cp handle to set its user data
 * \param  mode    CP mode to be set
 * \param  @data   Data pointer
 * \param  len     Data lenght
 *
 * \returns 0         Success
 * \returns -EINVAL   The device handle was invalid.
 * \returns -EFAULT   The data parameter pointer is an invalid address.
 *
 */
extern int stm_display_cp_set_data( stm_display_cp_h            cp,
                                    const unsigned int          mode,
                                    const unsigned char * const data,
                                    const unsigned int          len );

#if defined(__cplusplus)
}
#endif

#endif /* STM_DISPLAY_CP_H */
