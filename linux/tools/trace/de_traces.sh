#!/bin/sh

# This script is copied to the target by the build system.
# It helps the activation of some Display Engine traces.

de_version=1.0


################################################################################
# Enables ONLY DisplayEngine's error traces (This is the DEFAULT configuration)
function __de_default_traces
{
    #Disable all the traces
    echo 0 0 > /sys/kernel/debug/vibe_trace_mask
    echo TRC_ID_BY_CONSOLE > /sys/kernel/debug/vibe_trace_set

    # and enable only errors
    echo TRC_ID_ERROR > /sys/kernel/debug/vibe_trace_set

    echo -e "DE traces reset to default. Only errors are enabled"
}

################################################################################
# Disables ALL DE traces (even errors)
function __de_no_traces
{
    echo 0 0 > /sys/kernel/debug/vibe_trace_mask
    echo TRC_ID_BY_CONSOLE > /sys/kernel/debug/vibe_trace_set

    echo -e "WARNING: Every DE errors are now disabled"
    echo -e "Use the trace group \"default\" to get the DE errors enabled again"
}

################################################################################
function __de_api_trace
{
    trace_level=$1

    case $trace_level in
        DISABLE|D)
            echo TRC_ID_API_DEVICE              > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_API_OUTPUT              > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_API_PLANE               > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_API_SOURCE              > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_API_SOURCE_PIXEL_STREAM > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_API_SOURCE_QUEUE        > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_API_PIXEL_CAPTURE       > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_API_VXI                 > /sys/kernel/debug/vibe_trace_clear
            echo -e "DE API traces disabled"
            ;;

        *)
            echo TRC_ID_API_DEVICE              > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_API_OUTPUT              > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_API_PLANE               > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_API_SOURCE              > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_API_SOURCE_PIXEL_STREAM > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_API_SOURCE_QUEUE        > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_API_PIXEL_CAPTURE       > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_API_VXI                 > /sys/kernel/debug/vibe_trace_set
            echo -e "DE API traces enabled"
            ;;
    esac
}

################################################################################
function __de_video_not_smooth
{
    trace_level=$1

    case $trace_level in
        DISABLE|D)
            echo TRC_ID_MAIN_INFO               > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_PICT_QUEUE_RELEASE      > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_PICTURE_SCHEDULING      > /sys/kernel/debug/vibe_trace_clear
            echo -e "Traces for video not smooth or video freeze disabled"
            ;;

        *)
            echo TRC_ID_MAIN_INFO               > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_PICT_QUEUE_RELEASE      > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_PICTURE_SCHEDULING      > /sys/kernel/debug/vibe_trace_set
            echo -e "Traces for video not smooth or video freeze enabled"
            ;;
    esac
}

################################################################################
function __de_video_quality
{
    trace_level=$1

    case $trace_level in
        DISABLE|D)
            echo TRC_ID_MAIN_INFO               > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_BUFFER_DESCRIPTOR       > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_CONTEXT_CHANGE          > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_HQVDPLITE_DUMP_LLD_CMD  > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_VDP_REG_DUMP            > /sys/kernel/debug/vibe_trace_clear
            echo -e "Traces for video quality issues disabled"
            ;;

        *)
            echo TRC_ID_MAIN_INFO               > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_BUFFER_DESCRIPTOR       > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_CONTEXT_CHANGE          > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_HQVDPLITE_DUMP_LLD_CMD  > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_VDP_REG_DUMP            > /sys/kernel/debug/vibe_trace_set
            echo -e "Traces for video quality issues enabled"
            ;;
    esac
}

################################################################################
function __de_gfx_symptoms
{
    trace_level=$1

    case $trace_level in
        DISABLE|D)
            echo TRC_ID_MAIN_INFO               > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_GDP_PLANE               > /sys/kernel/debug/vibe_trace_clear
            echo TRC_ID_GDP_PLUS_PLANE          > /sys/kernel/debug/vibe_trace_clear
            echo -e "Traces for GFX issues disabled"
            ;;

        *)
            echo TRC_ID_MAIN_INFO               > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_GDP_PLANE               > /sys/kernel/debug/vibe_trace_set
            echo TRC_ID_GDP_PLUS_PLANE          > /sys/kernel/debug/vibe_trace_set
            echo -e "Traces for GFX issues enabled"
            ;;
    esac
}

################################################################################
function __de_trace_help
{
    echo -e " "
    echo -e "  DisplayEngine Debug Script (version $de_version) "
    echo -e " "
    echo -e "  Usage:   sdk2_debug_traces --module=<de> --type=<trace_group_val> --level=<level_val>"
    echo -e " "
    echo -e "  Set DisplayEngine Trace Groups (specified with --type) and Level (specified with --level) for Debugging."
    echo -e " "
    echo -e "   --type=<trace_group_val>:"
    echo -e "    Specify the trace group to be used. **Mandatory**"
    echo -e "    Possible <trace_group_val>:"
    echo -e "      * default            : Restore the default DisplayEngine traces (only errors are enabled)"
    echo -e "      * api                : Enable every STPKI functions traces"
    echo -e "      * notrace            : Disable every DisplayEngine traces (even errors are disabled so this is NOT recommended)"
    echo -e " "
    echo -e "      For symptoms on Video planes:"
    echo -e "      * video_not_smooth   : Enable this for presentation issues (video freeze, jerkyness, Frame Rate Conversion problem...)"
    echo -e "      * video_quality      : Enable this for corrupted image on Main or Aux video plane"
    echo -e "                             NB: When compo_capture is used to mirror the main display on aux output, this is a graphic plane that is used to display the video on Aux output so you should also enable the trace group \"gfx_symptoms\"."
    echo -e " "
    echo -e "      For symptoms on GFX planes    :"
    echo -e "      * gfx_symptoms       : Enable this for issue with GFX plane"

#    echo -e " "
#    echo -e "      For symptoms on Display output: TBD"
#    echo -e "      For symptoms on Compo Capture : TBD"
#    echo -e "      For symptoms on HDMI          : TBD"

    echo -e " "
    echo -e "   --level=<level_val>: "
    echo -e "    Level used to enable or disable the specified trace_group."
    echo -e "    Possible <level_val>:"
    echo -e "    * (D)ISABLE            : trace_group disabled"
    echo -e "    * (A)UTO               : trace_group enabled"
    echo -e "    If no level provided, AUTO value will be used"
    echo -e " "
    echo -e "   --help/-h/-?"
    echo -e "    Prints this information"

    echo -e " "
    echo -e " Example of command:"
    echo -e "    sdk2_debug_trace --module=de --type=video_quality --level=AUTO"
    echo -e " "

    ##echo -e "\nAvailable TRC_IDs are : "
    ##cat /etc/debug_scripts/de_trc_id_list.txt
}

################################################################################
# Function used by master script to get information on List of available DE traces
function process_de_help
{
    __de_trace_help
}

################################################################################
# Function used by master script to enable / disable DE traces
# Arg 1 : Group (Trace group to be enabled : e.g. "api")
# Arg 2 : Tracing level:
#           * (D)ISABLE
#           * (A)UTO
#           If no level provided, AUTO value will be used
#
################################################################################
function process_de_trace
{
    echo -e "Calling process_de_trace with args : $@"

    # Check number of arguments
    case "$#" in
        1)
            # No trace_level specified. Use default level (=AUTO)
            trace_group=$1
            trace_level=AUTO
            ;;

        2)
            trace_group=$1
            trace_level=$2
            ;;

        *)
            echo -e "Invalid number of arguments!!!\n"
            __de_trace_help
            return
            ;;
    esac


    case $trace_group in
        api)
            __de_api_trace $trace_level
        ;;

        video_not_smooth)
            __de_video_not_smooth $trace_level
        ;;

        video_quality)
            __de_video_quality $trace_level
        ;;

        gfx_symptoms)
            __de_gfx_symptoms $trace_level
        ;;

        # Some more trace groups can be added here



        none)
        # Called when we are asked to enable or disable "all" traces.
        # Not implemented yet
        ;;

        default)
            __de_default_traces
        ;;

        notrace)
            # Warning: Even Errors will be disabled!
            __de_no_traces
        ;;

        *)
            echo -e "\n\nInvalid trace group!!!\n\n"
            __de_trace_help
        ;;
    esac

}

################################################################################

