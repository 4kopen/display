/***********************************************************************
 *
 * File: linux/kernel/drivers/video/stmfboutconfig.c
 * Copyright (c) 2007 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/version.h>
#include <linux/fb.h>
#include <asm/uaccess.h>

#include <stm_display.h>

#include "stmfb.h"
#include "stmfbinfo.h"
#include "linux/kernel/drivers/stm/hdmi/stmhdmi.h"

#include <vibe_debug.h>

void
stmfb_initialise_output_config (stm_display_output_h                        out,
                                struct stmfbio_output_configuration * const config,
                                const int                                   initialise_hardware)
{
  uint32_t caps;
  uint32_t tmp;
  uint32_t powerdowndacs=0;

  config->activate = STMFBIO_ACTIVATE_ON_NEXT_CHANGE;

  if(stm_display_output_get_capabilities(out,&caps)<0)
  {
    TRC( TRC_ID_ERROR,"Failed to get output caps");
    return;
  }

  if(caps & OUTPUT_CAPS_MIXER_BACKGROUND)
  {
    config->caps |= STMFBIO_OUTPUT_CAPS_MIXER_BACKGROUND;
    if (stm_display_output_get_control(out, OUTPUT_CTRL_BACKGROUND_ARGB, &tmp) < 0)
    {
      TRC(TRC_ID_ERROR, "Failed to get background color");
      config->mixer_background = 0;
    }
    else
    {
      config->mixer_background = tmp;
    }
  }

  if(stm_display_output_get_control(out, OUTPUT_CTRL_DAC_POWER_DOWN, &powerdowndacs)<0)
  {
    TRC(TRC_ID_ERROR, "Failed to get video dacs power status !!");
    return;
  }

  if(powerdowndacs)
  {
    TRC( TRC_ID_ERROR,"Video DACs are powered down !!");
    return;
  }

  config->caps = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;

  if((caps & (OUTPUT_CAPS_SD_ANALOG | OUTPUT_CAPS_DISPLAY_TIMING_MASTER)) == (OUTPUT_CAPS_SD_ANALOG | OUTPUT_CAPS_DISPLAY_TIMING_MASTER))
  {
    config->caps |= STMFBIO_OUTPUT_CAPS_SDTV_ENCODING;
  }

  config->analogue_config = STMFBIO_OUTPUT_ANALOGUE_CLIP_VIDEORANGE;

  if(stm_display_output_get_control(out, OUTPUT_CTRL_VIDEO_OUT_SELECT, &tmp)<0)
  {
    TRC( TRC_ID_ERROR,"Failed to get video output select control");
    return;
  }

  /*
   * The video out select defines are the same as the output signals
   * configuration define for the analogue outputs.
   */
  config->analogue_config = (tmp & STMFBIO_OUTPUT_ANALOGUE_MASK);

  if(caps & OUTPUT_CAPS_SD_ANALOG)
  {
    config->caps |= (STMFBIO_OUTPUT_CAPS_BRIGHTNESS |
                     STMFBIO_OUTPUT_CAPS_SATURATION |
                     STMFBIO_OUTPUT_CAPS_CONTRAST   |
                     STMFBIO_OUTPUT_CAPS_HUE);

    if(stm_display_output_get_control(out, OUTPUT_CTRL_BRIGHTNESS, &tmp) < 0)
    {
      TRC( TRC_ID_ERROR, "stm_display_output_get_control() fails!");
    }
    config->brightness = tmp;
    if(stm_display_output_get_control(out, OUTPUT_CTRL_SATURATION, &tmp) < 0)
    {
      TRC( TRC_ID_ERROR,"stm_display_output_get_control() fails!");
    }
    config->saturation = tmp;
    if(stm_display_output_get_control(out, OUTPUT_CTRL_CONTRAST, &tmp) < 0)
    {
      TRC( TRC_ID_ERROR,"stm_display_output_get_control() fails!");
    }
    config->contrast = tmp;
    if(stm_display_output_get_control(out, OUTPUT_CTRL_HUE, &tmp) < 0)
    {
      TRC( TRC_ID_ERROR,"stm_display_output_get_control() fails!");
    }
    config->hue = tmp;
  }

  /*
   * Set the default analogue output to be limited to the standard video range
   * where the hardware can enforce that.
   */
  if(initialise_hardware)
  {
    if(stm_display_output_set_control(out, OUTPUT_CTRL_CLIP_SIGNAL_RANGE, STM_SIGNAL_VIDEO_RANGE) < 0)
    {
      TRC( TRC_ID_ERROR, "stm_display_output_set_control() fails!");
    }
  }

  if(caps & OUTPUT_CAPS_PLANE_MIXER)
  {
    config->caps |= STMFBIO_OUTPUT_CAPS_MIXER_COLORSPACE;
    config->mixer_colorspace = STMFBIO_OUTPUT_MIXER_COLORSPACE_AUTO; /* default to auto mode */
  }

  /*
   * Fill output's HDR Format data.
   */
  if(caps & OUTPUT_CAPS_HDR_FORMAT)
  {
    stm_hdr_format_t hdr_format;

    if(stm_display_output_get_compound_control(out, OUTPUT_CTRL_HDR_FORMAT, (void *)&hdr_format) < 0)
    {
      TRC( TRC_ID_ERROR,"stm_display_output_get_compound_control() fails!");
    }
    else
    {
      config->caps |= STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT;
      config->hdr_format.eotf_type                                       = (stmfbio_eotf_type_t)hdr_format.eotf_type;
      config->hdr_format.is_st2086_metadata_present                      = (__u8)hdr_format.is_st2086_metadata_present;
      config->hdr_format.is_hdr_metadata_present                         = (__u8)hdr_format.is_hdr_metadata_present;
      config->hdr_format.st2086_metadata.display_primaries_x_0           = (__u16)hdr_format.st2086_metadata.display_primaries_x_0;
      config->hdr_format.st2086_metadata.display_primaries_y_0           = (__u16)hdr_format.st2086_metadata.display_primaries_y_0;
      config->hdr_format.st2086_metadata.display_primaries_x_1           = (__u16)hdr_format.st2086_metadata.display_primaries_x_1;
      config->hdr_format.st2086_metadata.display_primaries_y_1           = (__u16)hdr_format.st2086_metadata.display_primaries_y_1;
      config->hdr_format.st2086_metadata.display_primaries_x_2           = (__u16)hdr_format.st2086_metadata.display_primaries_x_2;
      config->hdr_format.st2086_metadata.display_primaries_y_2           = (__u16)hdr_format.st2086_metadata.display_primaries_y_2;
      config->hdr_format.st2086_metadata.white_point_x                   = (__u16)hdr_format.st2086_metadata.white_point_x;
      config->hdr_format.st2086_metadata.white_point_y                   = (__u16)hdr_format.st2086_metadata.white_point_y;
      config->hdr_format.st2086_metadata.max_display_mastering_luminance = (__u16)hdr_format.st2086_metadata.max_display_mastering_luminance;
      config->hdr_format.st2086_metadata.min_display_mastering_luminance = (__u16)hdr_format.st2086_metadata.min_display_mastering_luminance;
      config->hdr_format.hdr_metadata.maxCLL                             = (__u16)hdr_format.hdr_metadata.maxCLL;
      config->hdr_format.hdr_metadata.maxFALL                            = (__u16)hdr_format.hdr_metadata.maxFALL;
    }
  }

  /*
   * Fill output's HDR Mode info.
   */
  if(caps & OUTPUT_CAPS_HDR_FORMAT)
  {
    stm_output_hdr_mode_t hdr_mode = STM_OUTPUT_HDR_MODE_MANUAL;

    if(stm_display_output_get_control(out, OUTPUT_CTRL_HDR_MODE_SELECT, (uint32_t *)&hdr_mode) < 0)
    {
      config->hdr_mode  = STMFBIO_OUTPUT_HDR_MODE_UNKNOWN;
      TRC( TRC_ID_ERROR,"stm_display_output_get_control(OUTPUT_CTRL_HDR_MODE_SELECT) fails!");
    }
    else
    {
      config->caps     |= STMFBIO_OUTPUT_CAPS_MIXER_HDR_MODE;
      config->hdr_mode  = (stmfbio_output_hdr_mode_t)hdr_mode;
    }
  }
}


int
stmfb_get_output_configuration (struct stmfbio_output_configuration * const c,
                                struct stmfb_info                   * const i)
{
  switch(c->outputid)
  {
    case STMFBIO_OUTPUTID_MAIN:
    {
      /*
       * First, update the current HDMI config from the HDMI device if we do
       * not have a pending update.
       */
      if(i->hdmi_fops && (i->main_config.activate != STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
      {
        int ret;
        __u32 disabled;
        mm_segment_t oldfs = get_fs();
        set_fs(KERNEL_DS);
        ret = i->hdmi_fops->unlocked_ioctl(&i->hdmi_dev, STMHDMIIO_GET_DISABLED, (unsigned long)&disabled);
        set_fs(oldfs);

        if(ret < 0)
          return (ret == -EINTR && signal_pending(current))?-ERESTARTSYS:ret;

        i->main_config.hdmi_config &= ~(STMFBIO_OUTPUT_HDMI_ENABLED | STMFBIO_OUTPUT_HDMI_DISABLED);
        i->main_config.hdmi_config |= (disabled == 0)?STMFBIO_OUTPUT_HDMI_ENABLED:STMFBIO_OUTPUT_HDMI_DISABLED;
      }

      /*
       * Sync output's HDR Format data since it could be changed by other
       * interfaces in addition to the STMFBIO one.
       */
      if(i->main_config.caps & STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT)
      {
        stm_hdr_format_t hdr_format;

        if(stm_display_output_get_compound_control(i->hFBMainOutput, OUTPUT_CTRL_HDR_FORMAT, (void *)&hdr_format) < 0)
        {
          TRC( TRC_ID_ERROR,"stm_display_output_get_compound_control() fails!");
          memset(&i->main_config.hdr_format, 0, sizeof(stmfbio_hdr_format_t));
        }
        else
        {
          i->main_config.hdr_format.eotf_type                                       = (stmfbio_eotf_type_t)hdr_format.eotf_type;
          i->main_config.hdr_format.is_st2086_metadata_present                      = (__u8)hdr_format.is_st2086_metadata_present;
          i->main_config.hdr_format.is_hdr_metadata_present                         = (__u8)hdr_format.is_hdr_metadata_present;
          i->main_config.hdr_format.st2086_metadata.display_primaries_x_0           = (__u16)hdr_format.st2086_metadata.display_primaries_x_0;
          i->main_config.hdr_format.st2086_metadata.display_primaries_y_0           = (__u16)hdr_format.st2086_metadata.display_primaries_y_0;
          i->main_config.hdr_format.st2086_metadata.display_primaries_x_1           = (__u16)hdr_format.st2086_metadata.display_primaries_x_1;
          i->main_config.hdr_format.st2086_metadata.display_primaries_y_1           = (__u16)hdr_format.st2086_metadata.display_primaries_y_1;
          i->main_config.hdr_format.st2086_metadata.display_primaries_x_2           = (__u16)hdr_format.st2086_metadata.display_primaries_x_2;
          i->main_config.hdr_format.st2086_metadata.display_primaries_y_2           = (__u16)hdr_format.st2086_metadata.display_primaries_y_2;
          i->main_config.hdr_format.st2086_metadata.white_point_x                   = (__u16)hdr_format.st2086_metadata.white_point_x;
          i->main_config.hdr_format.st2086_metadata.white_point_y                   = (__u16)hdr_format.st2086_metadata.white_point_y;
          i->main_config.hdr_format.st2086_metadata.max_display_mastering_luminance = (__u16)hdr_format.st2086_metadata.max_display_mastering_luminance;
          i->main_config.hdr_format.st2086_metadata.min_display_mastering_luminance = (__u16)hdr_format.st2086_metadata.min_display_mastering_luminance;
          i->main_config.hdr_format.hdr_metadata.maxCLL                             = (__u16)hdr_format.hdr_metadata.maxCLL;
          i->main_config.hdr_format.hdr_metadata.maxFALL                            = (__u16)hdr_format.hdr_metadata.maxFALL;
        }
      }

      /*
       * Sync output's HDR Mode info since it could be changed by other
       * interfaces in addition to the STMFBIO one.
       */
      if(i->main_config.caps & STMFBIO_OUTPUT_CAPS_MIXER_HDR_MODE)
      {
        stm_output_hdr_mode_t hdr_mode = STM_OUTPUT_HDR_MODE_MANUAL;

        if(stm_display_output_get_control(i->hFBMainOutput, OUTPUT_CTRL_HDR_MODE_SELECT, (uint32_t *)&hdr_mode) < 0)
        {
          TRC( TRC_ID_ERROR,"stm_display_output_get_control(OUTPUT_CTRL_HDR_MODE_SELECT) fails!");
          i->main_config.hdr_mode  = STMFBIO_OUTPUT_HDR_MODE_UNKNOWN;
        }
        else
        {
          i->main_config.hdr_mode  = (stmfbio_output_hdr_mode_t)hdr_mode;
        }
      }

      TRC( TRC_ID_STMFB_DEBUG,"returning main configuration (address = %p)",&i->main_config);
      *c = i->main_config;
      break;
    }
    default:
      TRC( TRC_ID_ERROR,"invalid output id = %d",(int)c->outputid);
      return -EINVAL;
  }

  return 0;
}


static void stmfbio_set_sdtv_encoding(struct stmfb_info *i,
                                      stm_display_output_h o,
                                      uint32_t outputcaps,
                                      struct stmfbio_output_configuration *c,
                                      struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;

  TRC( TRC_ID_STMFB_DEBUG," ");

  switch(c->sdtv_encoding)
  {
    case STMFBIO_OUTPUT_STD_PAL_BDGHI:
    case STMFBIO_OUTPUT_STD_PAL_M:
    case STMFBIO_OUTPUT_STD_PAL_N:
    case STMFBIO_OUTPUT_STD_PAL_Nc:
    case STMFBIO_OUTPUT_STD_NTSC_M:
    case STMFBIO_OUTPUT_STD_NTSC_J:
    case STMFBIO_OUTPUT_STD_NTSC_443:
    case STMFBIO_OUTPUT_STD_SECAM:
    case STMFBIO_OUTPUT_STD_PAL_60:
      break;
    default:
      failed = STMFBIO_OUTPUT_CAPS_SDTV_ENCODING;
      TRC( TRC_ID_ERROR,"Invalid SD TV Encoding");
  }

  if(!(outputcaps & OUTPUT_CAPS_SD_ANALOG))
  {
    failed = STMFBIO_OUTPUT_CAPS_SDTV_ENCODING;
    TRC( TRC_ID_ERROR,"Output doesn't support changing SD analogue standard");
  }

  if((failed == 0) && (c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
    newconfig->sdtv_encoding = c->sdtv_encoding;

  c->failed |= failed;

  if((failed != 0) || c->activate != STMFBIO_ACTIVATE_IMMEDIATE)
    return;

  if(i->current_videomode_valid)
  {
    stm_display_mode_t new_mode = i->current_videomode;
    stm_display_mode_t mode;

    /* We must make sure to not have an invalid standard description
       in the mode we are setting. This could happen, since
       sdtv_encoding might e.g. still contain the previous SD standard,
       which could be incompatible with the new requested mode. */
    if (stm_display_output_get_display_mode(o,new_mode.mode_id,
                                            &mode)==0)
    {
      uint32_t newstandard = c->sdtv_encoding;

      if(new_mode.mode_params.output_standards & STM_OUTPUT_STD_SMPTE293M)
        newstandard |= STM_OUTPUT_STD_SMPTE293M;

      newstandard &= mode.mode_params.output_standards;
      new_mode.mode_params.output_standards = newstandard;

      if(stm_display_output_start(o,&new_mode)<0)
        failed = STMFBIO_OUTPUT_CAPS_SDTV_ENCODING;
      else
        i->current_videomode = new_mode;
    }
    else
      failed = STMFBIO_OUTPUT_CAPS_SDTV_ENCODING;
  }

  if(failed == 0)
    newconfig->sdtv_encoding = c->sdtv_encoding;
  else
    c->failed |= failed;
}


static void stmfbio_set_analogue_config(struct stmfb_info *i,
                                       stm_display_output_h o,
                                       uint32_t outputcaps,
                                       struct stmfbio_output_configuration *c,
                                       struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;
  __u32 tmp;
  stm_display_signal_range_t range;

  TRC( TRC_ID_STMFB_DEBUG,": outcaps = 0x%08x analogue config = 0x%08x",outputcaps,c->analogue_config);

  /*
   * Component YPrPb and RGB (SCART) are mutually exclusive
   */
  tmp = (STMFBIO_OUTPUT_ANALOGUE_YPrPb|STMFBIO_OUTPUT_ANALOGUE_RGB);
  if((c->analogue_config & tmp) == tmp)
  {
    TRC( TRC_ID_ERROR,"RGB and YPrPb are mutually exclusive");
    failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
  }

  if(c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_RGB)
  {
    if(!(outputcaps & (OUTPUT_CAPS_SD_RGB_CVBS_YC | OUTPUT_CAPS_RGB_EXCLUSIVE)))
    {
      TRC( TRC_ID_ERROR,"RGB not supported");
      failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
    }
    else if(((c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_MASK) != STMFBIO_OUTPUT_ANALOGUE_RGB) &&
            !(outputcaps & OUTPUT_CAPS_SD_RGB_CVBS_YC))
    {
      TRC( TRC_ID_ERROR,"RGB+CVBS/YC (SCART) not supported");
      failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
    }
  }

  if(c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_YPrPb)
  {
    if(!(outputcaps & (OUTPUT_CAPS_SD_YPbPr_CVBS_YC | OUTPUT_CAPS_YPbPr_EXCLUSIVE)))
    {
      TRC( TRC_ID_ERROR,"YPrPb not supported");
      failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
    }
    else if(((c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_MASK) != STMFBIO_OUTPUT_ANALOGUE_YPrPb) &&
            !(outputcaps & OUTPUT_CAPS_SD_YPbPr_CVBS_YC))
    {
      TRC( TRC_ID_ERROR,"YPrPb+CVBS/YC not supported");
      failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
    }
  }

  tmp = (OUTPUT_CAPS_CVBS_YC_EXCLUSIVE |
         OUTPUT_CAPS_SD_RGB_CVBS_YC    |
         OUTPUT_CAPS_SD_YPbPr_CVBS_YC);

  if((c->analogue_config & (STMFBIO_OUTPUT_ANALOGUE_CVBS|STMFBIO_OUTPUT_ANALOGUE_YC)) && !(outputcaps & tmp))
  {
    TRC( TRC_ID_ERROR,"CVBS and Y/C unavailable");
    failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
  }

  if((failed == 0) && (c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
    newconfig->analogue_config = c->analogue_config;

  c->failed |= failed;

  if((failed != 0) || c->activate != STMFBIO_ACTIVATE_IMMEDIATE)
    return;

  /*
   * Note that the STMFBIO_OUTPUT_SIGNAL_ defines have been deliberately made
   * the same as the STM_VIDEO_OUT_ defines for analogue signals.
   */
  tmp = c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_MASK;

  if(stm_display_output_set_control(o, OUTPUT_CTRL_VIDEO_OUT_SELECT, tmp)<0)
    failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;

  range = (c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_CLIP_FULLRANGE)?STM_SIGNAL_FULL_RANGE:STM_SIGNAL_VIDEO_RANGE;
  if(stm_display_output_set_control(o, OUTPUT_CTRL_CLIP_SIGNAL_RANGE, range)<0)
    failed = STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;

  if((c->caps & STMFBIO_OUTPUT_CAPS_MIXER_COLORSPACE) == 0)
  {
    /* Old interface used to change the output colorspace */
    c->mixer_colorspace = (c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_MASK) >> 9;
    if(c->mixer_colorspace >= STMFBIO_OUTPUT_MIXER_COLORSPACE_LAST)
    {
      TRC( TRC_ID_ERROR,"Invalid ColorSpace value (cs = %#.16lx)",
           (c->analogue_config & STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_MASK));
      failed |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
    }

    if(failed == 0)
    {
      /* Use the new interface to change the output colorspace */
      c->caps |= STMFBIO_OUTPUT_CAPS_MIXER_COLORSPACE;
    }
  }

  if(failed == 0)
    newconfig->analogue_config = c->analogue_config;
  else
    c->failed |= failed;
}


static void stmfbio_set_dvo_config(struct stmfb_info *i,
                                    struct stmfbio_output_configuration *c,
                                    struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;
  __u32 format = 0;
  stm_display_signal_range_t range;
  __u32 sync   = 0;
  __u32 filter = 0;
  __u32 inv_clk = 0;

  TRC( TRC_ID_STMFB_DEBUG,"dvo_config = 0x%08x",c->dvo_config);

  if(!i->hFBDVO)
    failed = STMFBIO_OUTPUT_CAPS_DVO_CONFIG;

  if((failed == 0) && (c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
    newconfig->dvo_config = c->dvo_config;

  c->failed |= failed;

  if((failed != 0) || (c->activate != STMFBIO_ACTIVATE_IMMEDIATE))
    return;

  if(i->current_videomode_valid)
  {
    int ret;
    /*
     * If the display is running, just do a start or stop. It doesn't matter
     * if they are called when already in the correct state. We only take note
     * of the error return if a signal is pending.
     */
    if(!(c->dvo_config & STMFBIO_OUTPUT_DVO_DISABLED))
      ret = stm_display_output_start(i->hFBDVO, &i->current_videomode);
    else
      ret = stm_display_output_stop(i->hFBDVO);

    if(ret<0 && signal_pending(current))
      failed = STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
  }

  switch(c->dvo_config & STMFBIO_OUTPUT_DVO_MODE_MASK)
  {
    case STMFBIO_OUTPUT_DVO_YUV_444_16BIT:
      format = (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_444 | STM_VIDEO_OUT_16BIT);
      break;
    case STMFBIO_OUTPUT_DVO_YUV_444_24BIT:
      format = (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_444 | STM_VIDEO_OUT_24BIT);
      break;
    case STMFBIO_OUTPUT_DVO_YUV_422_16BIT:
      format = (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_422 | STM_VIDEO_OUT_16BIT);
      break;
    case STMFBIO_OUTPUT_DVO_ITUR656:
      format = (STM_VIDEO_OUT_YUV | STM_VIDEO_OUT_ITUR656);
      break;
    case STMFBIO_OUTPUT_DVO_RGB_24BIT:
      format = (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_24BIT);
      break;
  }

  if(stm_display_output_set_control(i->hFBDVO, OUTPUT_CTRL_VIDEO_OUT_SELECT,format)<0)
    failed = STMFBIO_OUTPUT_CAPS_DVO_CONFIG;

  sync = (c->dvo_config & STMFBIO_OUTPUT_DVO_EMBEDDED_SYNC)?1:0;
  if(stm_display_output_set_control(i->hFBDVO, OUTPUT_CTRL_DVO_ALLOW_EMBEDDED_SYNCS, sync)<0)
    failed = STMFBIO_OUTPUT_CAPS_DVO_CONFIG;

  inv_clk = (c->dvo_config & STMFBIO_OUTPUT_DVO_INVERT_DATA_CLOCK)?1:0;
  if(stm_display_output_set_control(i->hFBDVO, OUTPUT_CTRL_DVO_INVERT_DATA_CLOCK, inv_clk)<0)
    failed = STMFBIO_OUTPUT_CAPS_DVO_CONFIG;

  range = (c->dvo_config & STMFBIO_OUTPUT_DVO_CLIP_FULLRANGE)?STM_SIGNAL_FILTER_SAV_EAV:STM_SIGNAL_VIDEO_RANGE;
  if(stm_display_output_set_control(i->hFBDVO, OUTPUT_CTRL_CLIP_SIGNAL_RANGE, range)<0)
    failed = STMFBIO_OUTPUT_CAPS_DVO_CONFIG;

  filter = (c->dvo_config & STMFBIO_OUTPUT_DVO_CHROMA_FILTER_ENABLED)?1:0;
  if(stm_display_output_set_control(i->hFBDVO, OUTPUT_CTRL_422_CHROMA_FILTER, filter)<0)
    failed = STMFBIO_OUTPUT_CAPS_DVO_CONFIG;

  if(failed == 0)
    newconfig->dvo_config = c->dvo_config;
  else
    c->failed |= failed;
}


static void stmfbio_set_hdmi_config(struct stmfb_info *i,
                                    struct stmfbio_output_configuration *c,
                                    struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;
  __u32 video  = 0;
  int hdmidisable;
  stm_display_signal_range_t range;
  mm_segment_t oldfs = get_fs();

  TRC( TRC_ID_STMFB_DEBUG,": hdmi_config = 0x%08x",c->hdmi_config);

  if((i->hFBHDMI == NULL) || (i->hdmi_fops == NULL))
    failed = STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
  else if(((c->hdmi_config & STMFBIO_OUTPUT_HDMI_COLOURDEPTH_MASK) != STMFBIO_OUTPUT_HDMI_COLOURDEPTH_24BIT))
  {
    uint32_t caps;

    if((stm_display_output_get_capabilities(i->hFBHDMI, &caps)<0) ||
       ((caps & OUTPUT_CAPS_HDMI_DEEPCOLOR) == 0))
    {
      TRC( TRC_ID_ERROR,": No deepcolour support available");
      failed = STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
    }
  }

  if((c->hdmi_config & STMFBIO_OUTPUT_HDMI_YUV) &&
     (c->hdmi_config & STMFBIO_OUTPUT_HDMI_422) &&
     (c->hdmi_config & STMFBIO_OUTPUT_HDMI_COLOURDEPTH_MASK) != STMFBIO_OUTPUT_HDMI_COLOURDEPTH_24BIT)
  {
    TRC( TRC_ID_ERROR,": Deepcolour not valid in YUV 422 output mode");
    failed = STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
  }

  if((failed == 0) && (c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
    newconfig->hdmi_config = c->hdmi_config;

  c->failed |= failed;

  if((failed != 0) || (c->activate != STMFBIO_ACTIVATE_IMMEDIATE))
    return;

  if(c->hdmi_config & STMFBIO_OUTPUT_HDMI_YUV)
  {
    video |= STM_VIDEO_OUT_YUV;
    if(c->hdmi_config & STMFBIO_OUTPUT_HDMI_422)
    {
      video |= STM_VIDEO_OUT_422;
    }
    else
    {
      if(c->hdmi_config & STMFBIO_OUTPUT_HDMI_420)
        video |= STM_VIDEO_OUT_420;
      else
        video |= STM_VIDEO_OUT_444;
    }
  }
  else
  {
    video |= STM_VIDEO_OUT_RGB;
  }

  switch(c->hdmi_config & STMFBIO_OUTPUT_HDMI_COLOURDEPTH_MASK)
  {
    case STMFBIO_OUTPUT_HDMI_COLOURDEPTH_30BIT:
      video |= STM_VIDEO_OUT_30BIT;
      break;
    case STMFBIO_OUTPUT_HDMI_COLOURDEPTH_36BIT:
      video |= STM_VIDEO_OUT_36BIT;
      break;
    case STMFBIO_OUTPUT_HDMI_COLOURDEPTH_48BIT:
      video |= STM_VIDEO_OUT_48BIT;
      break;
    default:
      video |= STM_VIDEO_OUT_24BIT;
      break;
  }

  set_fs(KERNEL_DS);
  if(i->hdmi_fops->unlocked_ioctl(&i->hdmi_dev, STMHDMIIO_SET_VIDEO_FORMAT, video)<0)
    failed = STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;

  hdmidisable = (c->hdmi_config & STMFBIO_OUTPUT_HDMI_DISABLED)?1:0;
  if(i->hdmi_fops->unlocked_ioctl(&i->hdmi_dev, STMHDMIIO_SET_DISABLED, hdmidisable)<0)
    failed = STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
  set_fs(oldfs);

  range = (c->hdmi_config & STMFBIO_OUTPUT_HDMI_CLIP_FULLRANGE)?STM_SIGNAL_FULL_RANGE:STM_SIGNAL_VIDEO_RANGE;
  if(stm_display_output_set_control(i->hFBHDMI, OUTPUT_CTRL_CLIP_SIGNAL_RANGE, range)<0)
    failed = STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;

  if(failed == 0)
    newconfig->hdmi_config = c->hdmi_config;
  else
    c->failed |= failed;

}


static void stmfbio_set_mixer_background(stm_display_output_h o,
                                         uint32_t  outcaps,
                                         struct stmfbio_output_configuration *c,
                                         struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;

  TRC( TRC_ID_STMFB_DEBUG," ");

  if(!(outcaps & OUTPUT_CAPS_MIXER_BACKGROUND))
    failed = STMFBIO_OUTPUT_CAPS_MIXER_BACKGROUND;

  if((failed == 0) && (c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
    newconfig->mixer_background = c->mixer_background;

  c->failed |= failed;

  if((failed != 0) || (c->activate != STMFBIO_ACTIVATE_IMMEDIATE))
    return;

  if(stm_display_output_set_control(o, OUTPUT_CTRL_BACKGROUND_ARGB, c->mixer_background)<0)
  {
    failed = STMFBIO_OUTPUT_CAPS_MIXER_BACKGROUND;
  }

  if(failed == 0)
    newconfig->mixer_background = c->mixer_background;
  else
    c->failed |= failed;

}


static void stmfbio_set_mixer_colorspace(stm_display_output_h o,
                                         uint32_t  outcaps,
                                         struct stmfbio_output_configuration *c,
                                         struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;
  stm_ycbcr_colorspace_t colorspace;

  TRCIN( TRC_ID_STMFB_DEBUG," ");

  if((!(outcaps & OUTPUT_CAPS_PLANE_MIXER))
  || (c->mixer_colorspace >= STMFBIO_OUTPUT_MIXER_COLORSPACE_LAST))
  {
    TRC( TRC_ID_ERROR,"Invalid ColorSpace value (cs = %#.16x)", c->mixer_colorspace);
    c->failed |= STMFBIO_OUTPUT_CAPS_MIXER_COLORSPACE;
    return;
  }

  /*
   * Assuming STMFBIO_OUTPUT_MIXER_COLORSPACE_XXX are matching correct
   * CS values in stm_ycbcr_colorspace_t enum.
   */
  colorspace          = (stm_ycbcr_colorspace_t)c->mixer_colorspace;

  /* Clear and than update output color space flags */
  c->analogue_config &= ~STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_MASK;
  c->analogue_config |= (c->mixer_colorspace << 9);

  /* Update new output configuration */
  if((failed == 0) && (c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
  {
    newconfig->mixer_colorspace = c->mixer_colorspace;
    newconfig->analogue_config  = c->analogue_config;
  }

  c->failed |= failed;

  if(c->activate != STMFBIO_ACTIVATE_IMMEDIATE)
    return;

  if(stm_display_output_set_control(o, OUTPUT_CTRL_YCBCR_COLORSPACE, colorspace)<0)
  {
    TRC( TRC_ID_ERROR,"CTRL_YCBCR_COLORSPACE output control failed (ctl value = %#.8x)!", colorspace);
    failed = STMFBIO_OUTPUT_CAPS_MIXER_COLORSPACE;
  }

  if(failed == 0)
  {
    newconfig->mixer_colorspace = c->mixer_colorspace;
    newconfig->analogue_config  = c->analogue_config;
  }

  c->failed |= failed;

  TRCOUT( TRC_ID_STMFB_DEBUG," ");
}

static void stmfbio_set_hdr_mode (stm_display_output_h o,
                                         uint32_t  outcaps,
                                         struct stmfbio_output_configuration *c,
                                         struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;
  stm_output_hdr_mode_t hdr_mode;

  TRCIN( TRC_ID_STMFB_DEBUG," ");

  if(!(outcaps & OUTPUT_CAPS_HDR_FORMAT))
  {
    if(c->hdr_mode == STMFBIO_OUTPUT_HDR_MODE_AUTO)
    {
      TRC( TRC_ID_ERROR,"HDR Mode is not supported!");
      c->failed |= STMFBIO_OUTPUT_CAPS_MIXER_HDR_MODE;
    }
    return;
  }

  if(c->hdr_mode == STMFBIO_OUTPUT_HDR_MODE_UNKNOWN)
    return;

  /* Update new stmfb HDR Mode configuration */
  if(c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE)
  {
    newconfig->hdr_mode = c->hdr_mode;
  }

  if(c->activate != STMFBIO_ACTIVATE_IMMEDIATE)
    return;

  /*
   * Fill output's HDR Format data.
   */
  hdr_mode = (stm_output_hdr_mode_t)c->hdr_mode;

  /* Apply new output's HDR Format configuration */
  if(stm_display_output_set_control(o, OUTPUT_CTRL_HDR_MODE_SELECT, hdr_mode)<0)
  {
    TRC( TRC_ID_ERROR,"OUTPUT_CTRL_HDR_MODE_SELECT output control failed (mode=%d)!", (int)hdr_mode);
    failed = STMFBIO_OUTPUT_CAPS_MIXER_HDR_MODE;
  }

  if(failed == 0)
  {
    newconfig->hdr_mode = c->hdr_mode;
  }

  c->failed |= failed;

  TRCOUT( TRC_ID_STMFB_DEBUG," ");
}


static void stmfbio_set_hdr_format (stm_display_output_h o,
                                         uint32_t  outcaps,
                                         struct stmfbio_output_configuration *c,
                                         struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;
  stm_hdr_format_t hdr_format;

  TRCIN( TRC_ID_STMFB_DEBUG," ");

  if(!(outcaps & OUTPUT_CAPS_HDR_FORMAT))
  {
    if(c->hdr_format.eotf_type != STMFBIO_EOTF_GAMMA_SDR)
    {
      TRC( TRC_ID_ERROR,"HDR Format is not supported!");
      c->failed |= STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT;
    }
    return;
  }
  else if(newconfig->hdr_mode == STMFBIO_OUTPUT_HDR_MODE_AUTO)
  {
    /*
     * Trying to set HDR format while the output HDR mode is configured
     * in Auto mode is not allowed.
     */
    TRC( TRC_ID_ERROR,"HDR Format can't be changed while output's HDR mode "
         "is in Auto mode supported!");
    c->failed |= STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT;
    return;
  }

  /*
   * Fill output's HDR Format data.
   */
  hdr_format.eotf_type                     = (stm_eotf_type_t)c->hdr_format.eotf_type;
  hdr_format.is_st2086_metadata_present    = (bool)c->hdr_format.is_st2086_metadata_present;
  hdr_format.is_hdr_metadata_present       = (bool)c->hdr_format.is_hdr_metadata_present;

  if(hdr_format.is_st2086_metadata_present)
  {
    hdr_format.st2086_metadata.display_primaries_x_0           = (uint16_t)c->hdr_format.st2086_metadata.display_primaries_x_0;
    hdr_format.st2086_metadata.display_primaries_y_0           = (uint16_t)c->hdr_format.st2086_metadata.display_primaries_y_0;
    hdr_format.st2086_metadata.display_primaries_x_1           = (uint16_t)c->hdr_format.st2086_metadata.display_primaries_x_1;
    hdr_format.st2086_metadata.display_primaries_y_1           = (uint16_t)c->hdr_format.st2086_metadata.display_primaries_y_1;
    hdr_format.st2086_metadata.display_primaries_x_2           = (uint16_t)c->hdr_format.st2086_metadata.display_primaries_x_2;
    hdr_format.st2086_metadata.display_primaries_y_2           = (uint16_t)c->hdr_format.st2086_metadata.display_primaries_y_2;
    hdr_format.st2086_metadata.white_point_x                   = (uint16_t)c->hdr_format.st2086_metadata.white_point_x;
    hdr_format.st2086_metadata.white_point_y                   = (uint16_t)c->hdr_format.st2086_metadata.white_point_y;
    hdr_format.st2086_metadata.max_display_mastering_luminance = (uint16_t)c->hdr_format.st2086_metadata.max_display_mastering_luminance;
    hdr_format.st2086_metadata.min_display_mastering_luminance = (uint16_t)c->hdr_format.st2086_metadata.min_display_mastering_luminance;
  }

  if(hdr_format.is_hdr_metadata_present)
  {
    hdr_format.hdr_metadata.maxCLL   = (uint16_t)c->hdr_format.hdr_metadata.maxCLL;
    hdr_format.hdr_metadata.maxFALL  = (uint16_t)c->hdr_format.hdr_metadata.maxFALL;
  }

  /* Update new stmfb HDR Format configuration */
  if(c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE)
  {
    newconfig->hdr_format        = c->hdr_format;
  }

  if(c->activate != STMFBIO_ACTIVATE_IMMEDIATE)
    return;

  /* Apply new output's HDR Format configuration */
  if(stm_display_output_set_compound_control(o, OUTPUT_CTRL_HDR_FORMAT, (void *)&hdr_format)<0)
  {
    TRC( TRC_ID_ERROR,"OUTPUT_CTRL_HDR_FORMAT output compound control failed!");
    failed = STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT;
  }

  if(failed == 0)
  {
    newconfig->hdr_format  = c->hdr_format;
  }

  c->failed |= failed;

  TRCOUT( TRC_ID_STMFB_DEBUG," ");
}


static void stmfbio_set_psi(stm_display_output_h o,
                            uint32_t  outcaps,
                            struct stmfbio_output_configuration *c,
                            struct stmfbio_output_configuration *newconfig)
{
  __u32 failed = 0;

  TRC( TRC_ID_STMFB_DEBUG,"");

  if(!(outcaps & OUTPUT_CAPS_SD_ANALOG) && (c->caps & STMFBIO_OUTPUT_CAPS_BRIGHTNESS))
    failed |= STMFBIO_OUTPUT_CAPS_BRIGHTNESS;

  if(!(outcaps & OUTPUT_CAPS_SD_ANALOG) && (c->caps & STMFBIO_OUTPUT_CAPS_SATURATION))
    failed |= STMFBIO_OUTPUT_CAPS_SATURATION;

  if(!(outcaps & OUTPUT_CAPS_SD_ANALOG) && (c->caps & STMFBIO_OUTPUT_CAPS_CONTRAST))
    failed |= STMFBIO_OUTPUT_CAPS_CONTRAST;

  if(!(outcaps & OUTPUT_CAPS_SD_ANALOG) && (c->caps & STMFBIO_OUTPUT_CAPS_HUE))
    failed |= STMFBIO_OUTPUT_CAPS_HUE;

  if((failed == 0) && (c->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE))
  {
    if(c->caps & STMFBIO_OUTPUT_CAPS_BRIGHTNESS)
      newconfig->brightness = c->brightness;

    if(c->caps & STMFBIO_OUTPUT_CAPS_SATURATION)
      newconfig->saturation = c->saturation;

    if(c->caps & STMFBIO_OUTPUT_CAPS_CONTRAST)
      newconfig->contrast = c->contrast;

    if(c->caps & STMFBIO_OUTPUT_CAPS_HUE)
      newconfig->hue = c->hue;
  }

  c->failed |= failed;

  if((failed != 0) || (c->activate != STMFBIO_ACTIVATE_IMMEDIATE))
    return;

  if(c->caps & STMFBIO_OUTPUT_CAPS_BRIGHTNESS)
  {
    TRC( TRC_ID_STMFB_DEBUG,"brightness = %u",(unsigned)c->brightness);

    if(stm_display_output_set_control(o, OUTPUT_CTRL_BRIGHTNESS, c->brightness)<0)
      failed |= STMFBIO_OUTPUT_CAPS_BRIGHTNESS;
    else
      newconfig->brightness = c->brightness;
  }

  if(c->caps & STMFBIO_OUTPUT_CAPS_SATURATION)
  {
    TRC( TRC_ID_STMFB_DEBUG,"saturation = %u",(unsigned)c->saturation);

    if(stm_display_output_set_control(o, OUTPUT_CTRL_SATURATION, c->saturation)<0)
      failed |= STMFBIO_OUTPUT_CAPS_SATURATION;
    else
      newconfig->saturation = c->saturation;
  }

  if(c->caps & STMFBIO_OUTPUT_CAPS_CONTRAST)
  {
    TRC( TRC_ID_STMFB_DEBUG,"contrast = %u",(unsigned)c->contrast);

    if(stm_display_output_set_control(o, OUTPUT_CTRL_CONTRAST, c->contrast)<0)
      failed |= STMFBIO_OUTPUT_CAPS_CONTRAST;
    else
      newconfig->contrast = c->contrast;
  }

  if(c->caps & STMFBIO_OUTPUT_CAPS_HUE)
  {
    TRC( TRC_ID_STMFB_DEBUG,"hue = %u",(unsigned)c->hue);

    if(stm_display_output_set_control(o, OUTPUT_CTRL_HUE, c->hue)<0)
      failed |= STMFBIO_OUTPUT_CAPS_HUE;
    else
      newconfig->hue = c->hue;
  }

  c->failed |= failed;

}


int
stmfb_set_output_configuration (struct stmfbio_output_configuration * const c,
                                struct stmfb_info                   * const i)
{
  stm_display_output_h o = NULL;
  struct stmfbio_output_configuration *newconfig = NULL;
  uint32_t outcaps;
  int ret = 0;

  c->failed = 0;

  switch(c->outputid)
  {
    case STMFBIO_OUTPUTID_MAIN:
      TRC( TRC_ID_STMFB_DEBUG,"Updating main output configuration");

      o = i->hFBMainOutput;
      newconfig = &i->main_config;
      break;
    default:
      TRC( TRC_ID_ERROR,"Invalid output configuration id %d",(int)c->outputid);
      break;
  }

  if(!o)
    return -EINVAL;


  if((ret = stm_display_output_get_capabilities(o,&outcaps))<0)
  {
    TRC( TRC_ID_ERROR,"Cannot get output capabilities");
    return ret;
  }

  if(c->caps & STMFBIO_OUTPUT_CAPS_SDTV_ENCODING)
    stmfbio_set_sdtv_encoding(i, o, outcaps, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG)
    stmfbio_set_analogue_config(i, o, outcaps, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_HDMI_CONFIG)
    stmfbio_set_hdmi_config(i, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_DVO_CONFIG)
    stmfbio_set_dvo_config(i, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_MIXER_BACKGROUND)
    stmfbio_set_mixer_background(o, outcaps, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_PSI_MASK)
    stmfbio_set_psi(o, outcaps, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_MIXER_COLORSPACE)
    stmfbio_set_mixer_colorspace(o, outcaps, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_MIXER_HDR_MODE)
    stmfbio_set_hdr_mode(o, outcaps, c, newconfig);

  if(c->caps & STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT)
    stmfbio_set_hdr_format(o, outcaps, c, newconfig);

  if(c->failed)
  {
    TRC( TRC_ID_ERROR,"Failed mask = 0x%08x",c->failed);
    if(signal_pending(current))
      return -EINTR;
    else
      return -EINVAL;
  }

  return 0;
}
