/*********************** This file is part of Display_engine

COPYRIGHT (C) 2003, 2004, 2005 STMicroelectronics - All Rights Reserved
License type: GPLv2.0

Copyright (C) ST Microelectronics
Copyright (c) 1998-2000 Ilario Nardinocchi (nardinoc@CS.UniBO.IT)
Copyright (c) 1999 Jakub Jelinek (jakub@redhat.com)

Display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

Display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Display_engine; if not, write to the Free Software

Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

The Display_engine Library may alternatively be licensed under a proprietary
license from ST.

This file was created by STMicroelectronics on 2005-07-19
and modified by STMicroelectronics on 2015-10-20

***********************************************************************/


#include <linux/version.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/pm_runtime.h>
#include <linux/fb.h>
#include <asm/div64.h>

#include <stm_display.h>

#include "stmfb.h"
#include "stmfbinfo.h"

#include <vibe_debug.h>


/*****************************************************************************
 * Processing of framebuffer var structures to ST core driver information
 */

struct colour_desc_entry {
   stm_pixel_format_t fmt;
   int      bitdepth;
   int      aoffset;
   int      alength;
   int      roffset;
   int      rlength;
   int      goffset;
   int      glength;
   int      boffset;
   int      blength;
   int      nonstd;
#if defined(STMFB_DEBUG_VAR)
   const char *name;
#endif
};


#if defined(STMFB_DEBUG_VAR)
#define E(f,d,ao,al,ro,rl,go,gl,bo,bl,n) \
  { f, d, ao, al, ro, rl, go, gl, bo, bl, n, #f }
#else
#define E(f,d,ao,al,ro,rl,go,gl,bo,bl,n) \
  { f, d, ao, al, ro, rl, go, gl, bo, bl, n }
#endif
static const struct colour_desc_entry colour_formats[] = {
  E(SURF_CLUT8,       8,  0, 0,  0, 0,  0, 0,  0, 0, 0),
  E(SURF_RGB565,     16,  0, 0, 11, 5,  5, 6,  0, 5, 0),
  E(SURF_ARGB1555,   16, 15, 1, 10, 5,  5, 5,  0, 5, 0),
  E(SURF_ARGB4444,   16, 12, 4,  8, 4,  4, 4,  0, 4, 0),
  E(SURF_RGB888,     24,  0, 0, 16, 8,  8, 8,  0, 8, 0),
  E(SURF_ARGB8565,   24, 16, 8, 11, 5,  5, 6,  0, 5, 0),
  E(SURF_ARGB8888,   32, 24, 8, 16, 8,  8, 8,  0, 8, 0),
  /* unusual formats */
  E(SURF_BGRA8888,   32,  0, 8,  8, 8, 16, 8, 24, 8, 0),
  E(SURF_YCBCR422R,  16,  0, 0,  0, 0,  0, 0,  0, 0, 1),
  E(SURF_CRYCB888,   24,  0, 0,  0, 0,  0, 0,  0, 0, 2),
  E(SURF_ACRYCB8888, 32, 24, 8,  0, 0,  0, 0,  0, 0, 0),
  E(SURF_ACLUT88,    16,  8, 8,  0, 0,  0, 0,  0, 0, 0),
};
#undef E


struct standard_list_entry
{
  stm_display_mode_id_t           mode;
  uint32_t                     tvStandard;
  enum stmfbio_output_standard stmfbio_standard;
  enum stmfbio_output_standard_ex stmfbio_standard_ex;
#if defined(STMFB_DEBUG_VAR)
  const char *name;
#endif
};

#if defined(STMFB_DEBUG_VAR)
#  define E(m,std,o,x,name)  { m, std, o, x, name }
#else
#  define E(m,std,o,x,name)  { m, std, o, x }
#endif
static const struct standard_list_entry stmfbio_standard_list[] = {
  E(STM_TIMING_MODE_480I59940_13500,        STM_OUTPUT_STD_NTSC_M,       STMFBIO_STD_NTSC_M,         STMFBIO_STD_EX_UNKNOWN,          "NTSC-M"      ),
  E(STM_TIMING_MODE_480I59940_13500,        STM_OUTPUT_STD_NTSC_J,       STMFBIO_STD_NTSC_M_JP,      STMFBIO_STD_EX_UNKNOWN,          "NTSC-J"      ),
  E(STM_TIMING_MODE_480I59940_13500,        STM_OUTPUT_STD_NTSC_443,     STMFBIO_STD_NTSC_443,       STMFBIO_STD_EX_UNKNOWN,          "NTSC-443"    ),
  E(STM_TIMING_MODE_480I59940_13500,        STM_OUTPUT_STD_PAL_M,        STMFBIO_STD_PAL_M,          STMFBIO_STD_EX_UNKNOWN,          "PAL-M"       ),
  E(STM_TIMING_MODE_480I59940_13500,        STM_OUTPUT_STD_PAL_60,       STMFBIO_STD_PAL_60,         STMFBIO_STD_EX_UNKNOWN,          "PAL-60"      ),
  E(STM_TIMING_MODE_576I50000_13500,        STM_OUTPUT_STD_PAL_BDGHI,    STMFBIO_STD_PAL,            STMFBIO_STD_EX_UNKNOWN,          "PAL"         ),
  E(STM_TIMING_MODE_576I50000_13500,        STM_OUTPUT_STD_PAL_N,        STMFBIO_STD_PAL_N,          STMFBIO_STD_EX_UNKNOWN,          "PAL-N"       ),
  E(STM_TIMING_MODE_576I50000_13500,        STM_OUTPUT_STD_PAL_Nc,       STMFBIO_STD_PAL_Nc,         STMFBIO_STD_EX_UNKNOWN,          "PAL-Nc"      ),
  E(STM_TIMING_MODE_576I50000_13500,        STM_OUTPUT_STD_SECAM,        STMFBIO_STD_SECAM,          STMFBIO_STD_EX_UNKNOWN,          "SECAM"       ),
  E(STM_TIMING_MODE_480P60000_27027,        STM_OUTPUT_STD_SMPTE293M,    STMFBIO_STD_480P_60,        STMFBIO_STD_EX_UNKNOWN,          "480p@60"     ),
  E(STM_TIMING_MODE_480P59940_27000,        STM_OUTPUT_STD_SMPTE293M,    STMFBIO_STD_480P_59_94,     STMFBIO_STD_EX_UNKNOWN,          "480p@59.94"  ),
  E(STM_TIMING_MODE_576P50000_27000,        STM_OUTPUT_STD_SMPTE293M,    STMFBIO_STD_576P_50,        STMFBIO_STD_EX_UNKNOWN,          "576p"        ),
  E(STM_TIMING_MODE_1080P60000_148500,      STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_60,       STMFBIO_STD_EX_UNKNOWN,          "1080p@60"    ),
  E(STM_TIMING_MODE_1080P59940_148352,      STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_59_94,    STMFBIO_STD_EX_UNKNOWN,          "1080p@59.94" ),
  E(STM_TIMING_MODE_1080P50000_148500,      STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_50,       STMFBIO_STD_EX_UNKNOWN,          "1080p@50"    ),
  E(STM_TIMING_MODE_1080P30000_74250,       STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_30,       STMFBIO_STD_EX_UNKNOWN,          "1080p@30"    ),
  E(STM_TIMING_MODE_1080P29970_74176,       STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_29_97,    STMFBIO_STD_EX_UNKNOWN,          "1080p@29.97" ),
  E(STM_TIMING_MODE_1080P25000_74250,       STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_25,       STMFBIO_STD_EX_UNKNOWN,          "1080p@25"    ),
  E(STM_TIMING_MODE_1080P24000_74250,       STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_24,       STMFBIO_STD_EX_UNKNOWN,          "1080p@24"    ),
  E(STM_TIMING_MODE_1080P23976_74176,       STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080P_23_976,   STMFBIO_STD_EX_UNKNOWN,          "1080p@23.976"),
  E(STM_TIMING_MODE_1080I60000_74250,       STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080I_60,       STMFBIO_STD_EX_UNKNOWN,          "1080i@60"    ),
  E(STM_TIMING_MODE_1080I59940_74176,       STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080I_59_94,    STMFBIO_STD_EX_UNKNOWN,          "1080i@59.94" ),
  E(STM_TIMING_MODE_1080I50000_74250_274M,  STM_OUTPUT_STD_SMPTE274M,    STMFBIO_STD_1080I_50,       STMFBIO_STD_EX_UNKNOWN,          "1080i@50"    ),
  E(STM_TIMING_MODE_720P60000_74250,        STM_OUTPUT_STD_SMPTE296M,    STMFBIO_STD_720P_60,        STMFBIO_STD_EX_UNKNOWN,          "720p@60"     ),
  E(STM_TIMING_MODE_720P59940_74176,        STM_OUTPUT_STD_SMPTE296M,    STMFBIO_STD_720P_59_94,     STMFBIO_STD_EX_UNKNOWN,          "720p@59.94"  ),
  E(STM_TIMING_MODE_720P50000_74250,        STM_OUTPUT_STD_SMPTE296M,    STMFBIO_STD_720P_50,        STMFBIO_STD_EX_UNKNOWN,          "720p@50"     ),
  E(STM_TIMING_MODE_480P60000_25200,        STM_OUTPUT_STD_VGA,          STMFBIO_STD_VGA_60,         STMFBIO_STD_EX_UNKNOWN,          "VGA@60"      ),
  E(STM_TIMING_MODE_480P59940_25180,        STM_OUTPUT_STD_VGA,          STMFBIO_STD_VGA_59_94,      STMFBIO_STD_EX_UNKNOWN,          "VGA@59.94"   ),
  E(STM_TIMING_MODE_768P60000_65000,        STM_OUTPUT_STD_XGA,          STMFBIO_STD_XGA_60,         STMFBIO_STD_EX_UNKNOWN,          "XGA@60"      ),
  E(STM_TIMING_MODE_768P70000_75000,        STM_OUTPUT_STD_XGA,          STMFBIO_STD_XGA_70,         STMFBIO_STD_EX_UNKNOWN,          "XGA@70"      ),
  E(STM_TIMING_MODE_768P75000_78750,        STM_OUTPUT_STD_XGA,          STMFBIO_STD_XGA_75,         STMFBIO_STD_EX_UNKNOWN,          "XGA@75"      ),
  E(STM_TIMING_MODE_768P85000_94500,        STM_OUTPUT_STD_XGA,          STMFBIO_STD_XGA_85,         STMFBIO_STD_EX_UNKNOWN,          "XGA@85"      ),
  E(STM_TIMING_MODE_1024P60000_108000,      STM_OUTPUT_STD_XGA,          STMFBIO_STD_XGA_60,         STMFBIO_STD_EX_UNKNOWN,          "SXGA@60"     ),
  E(STM_TIMING_MODE_540P59940_36343,        STM_OUTPUT_STD_NTG5,         STMFBIO_STD_QFHD3660,       STMFBIO_STD_EX_UNKNOWN,          "QFHD3660"    ),
  E(STM_TIMING_MODE_540P49993_36343,        STM_OUTPUT_STD_NTG5,         STMFBIO_STD_QFHD3650,       STMFBIO_STD_EX_UNKNOWN,          "QFHD3650"    ),
  E(STM_TIMING_MODE_540P59945_54857_WIDE,   STM_OUTPUT_STD_NTG5,         STMFBIO_STD_WQFHD5660,      STMFBIO_STD_EX_UNKNOWN,          "WQFHD5660"   ),
  E(STM_TIMING_MODE_540P49999_54857_WIDE,   STM_OUTPUT_STD_NTG5,         STMFBIO_STD_WQFHD5650,      STMFBIO_STD_EX_UNKNOWN,          "WQFHD5650"   ),
  E(STM_TIMING_MODE_540P59945_54857,        STM_OUTPUT_STD_NTG5,         STMFBIO_STD_QFHD5660,       STMFBIO_STD_EX_UNKNOWN,          "QFHD5660"    ),
  E(STM_TIMING_MODE_540P49999_54857,        STM_OUTPUT_STD_NTG5,         STMFBIO_STD_QFHD5650,       STMFBIO_STD_EX_UNKNOWN,          "QFHD5650"    ),
  E(STM_TIMING_MODE_540P29970_18000,        STM_OUTPUT_STD_NTG5,         STMFBIO_STD_QFHD1830,       STMFBIO_STD_EX_UNKNOWN,          "QFHD1830"    ),
  E(STM_TIMING_MODE_540P25000_18000,        STM_OUTPUT_STD_NTG5,         STMFBIO_STD_QFHD1825,       STMFBIO_STD_EX_UNKNOWN,          "QFHD1825"    ),
  E(STM_TIMING_MODE_2160P29970_296703,      STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861, STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_29_97,      "4K2Kp@29.97" ),
  E(STM_TIMING_MODE_2160P30000_297000,      STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861, STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_30,         "4K2Kp@30"    ),
  E(STM_TIMING_MODE_2160P29970_296703_WIDE, STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_29_97_WIDE, "W4K2Kp@29.97"),
  E(STM_TIMING_MODE_2160P30000_297000_WIDE, STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_30_WIDE,    "W4K2Kp@30"   ),
  E(STM_TIMING_MODE_2160P25000_297000,      STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861, STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_25,         "4K2Kp@25"    ),
  E(STM_TIMING_MODE_2160P25000_297000_WIDE, STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_25_WIDE,    "W4K2Kp@25"   ),
  E(STM_TIMING_MODE_2160P23980_296703,      STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861, STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_23_98,      "4K2Kp@23.98" ),
  E(STM_TIMING_MODE_2160P24000_297000,      STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861, STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_24,         "4K2Kp@24"    ),
  E(STM_TIMING_MODE_2160P23980_296703_WIDE, STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_23_98_WIDE, "W4K2Kp@23.98"),
  E(STM_TIMING_MODE_2160P24000_297000_WIDE, STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861, STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_24_WIDE,    "W4K2Kp@24"   ),
  E(STM_TIMING_MODE_2160P50000_594000,      STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_50,         "4K2Kp@50"    ),
  E(STM_TIMING_MODE_2160P50000_594000_WIDE, STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_50_WIDE,    "W4K2Kp@50"   ),
  E(STM_TIMING_MODE_2160P60000_594000,      STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_60,         "4K2Kp@60"    ),
  E(STM_TIMING_MODE_2160P59940_593406,      STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_59_94,      "4K2Kp@59.94" ),
  E(STM_TIMING_MODE_2160P60000_594000_WIDE, STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_60_WIDE,    "W4K2Kp@60"   ),
  E(STM_TIMING_MODE_2160P59940_593406_WIDE, STM_OUTPUT_STD_CEA861,       STMFBIO_STD_UNKNOWN,        STMFBIO_STD_EX_2160P_59_94_WIDE, "W4K2Kp@59.94"),
};
#undef E


#if defined(STMFB_DEBUG_VAR)
static void
stmfb_display_var (const char                     *prefix,
                   const struct fb_var_screeninfo *var)
{
  TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfb: ----%3s---- struct fb_var_screeninfo var = %p ---------------",
          prefix, var);

  TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfb: resolution: %ux%ux%u (virtual %ux%u+%u+%u)",
      var->xres, var->yres, var->bits_per_pixel,
      var->xres_virtual, var->yres_virtual,
      var->xoffset, var->yoffset);

  TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfb: color: %c %d R(%u,%u,%u), G(%u,%u,%u), B(%u,%u,%u), T(%u,%u,%u)",
      var->grayscale?'G':'C', var->nonstd,
      var->red.offset,    var->red.length,   var->red.msb_right,
      var->green.offset,  var->green.length, var->green.msb_right,
      var->blue.offset,   var->blue.length,  var->blue.msb_right,
      var->transp.offset, var->transp.length,var->transp.msb_right);

  TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfb: timings: %ups (%u,%u)-(%u,%u)+%u+%u",
    var->pixclock,
    var->left_margin, var->upper_margin, var->right_margin,
    var->lower_margin, var->hsync_len, var->vsync_len);

  TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfb: activate %08x accel_flags %08x sync %08x vmode %08x",
    var->activate, var->accel_flags, var->sync, var->vmode);

  TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfb: -------------------------------------------------------------------");
}


static void
stmfb_display_planeconfig (const char                         *prefix,
                           const struct stmfbio_plane_config2 *c)
{
  int         x;
  const char *name = "";

  for(x = 0; x < ARRAY_SIZE (colour_formats); ++x)
    if (colour_formats[x].fmt == c->format) {
      name = colour_formats[x].name;
      break;
    }

  TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfbio_plane_config %3s (@ %p) %u,%u-%ux%u (rescale to %u,%u-%ux%u) %u (%s) @ %lu pitch %u %dbpp",
          prefix, c,
          c->source.x, c->source.y, c->source.dim.w, c->source.dim.h,
          c->dest.x, c->dest.y, c->dest.dim.w, c->dest.dim.h,
          c->format, name, c->baseaddr, c->pitch, c->bitdepth);
}


static void
stmfb_display_outputstandard (const char                           *prefix,
                              const enum stmfbio_output_standard    s,
                              const enum stmfbio_output_standard_ex sx)
{
  int x;

  for (x = 0; x < ARRAY_SIZE (stmfbio_standard_list); ++x) {
    if ((stmfbio_standard_list[x].stmfbio_standard & s) == s) {
      TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfbio_output_standard %3s %llx (%s)",
              prefix, s, stmfbio_standard_list[x].name);
      return;
    }
    if ((stmfbio_standard_list[x].stmfbio_standard_ex & sx) == sx) {
      TRC(TRC_ID_STMFB_DEBUG_VAR, "stmfbio_output_standard(extended) %3s %llx (%s)",
              prefix, (unsigned long long)sx, stmfbio_standard_list[x].name);
      return;
    }
  }

  TRC(TRC_ID_ERROR, "stmfbio_output_standard %3s %llx unknown!", prefix, s);
  TRC(TRC_ID_ERROR, "stmfbio_output_standard(extended) %3s %llx unknown!", prefix, (unsigned long long)sx);
}

#else

static inline void __attribute__((nonnull)) stmfb_display_var(const char *prefix, const struct fb_var_screeninfo* var) { }
static inline void __attribute__((nonnull)) stmfb_display_planeconfig(const char *prefix, const struct stmfbio_plane_config2 *c) { }
static inline void __attribute__((nonnull(1))) stmfb_display_outputstandard(const char *prefix, const enum stmfbio_output_standard s, const enum stmfbio_output_standard_ex sx) { }

#endif


u32
stmfb_bitdepth_for_pixelformat (stm_pixel_format_t format)
{
  int x;
  for (x = 0; x < ARRAY_SIZE (colour_formats); ++x)
    if (colour_formats[x].fmt == format)
      return colour_formats[x].bitdepth;
  return 0;
}

u32
stmfb_pitch_for_config (const struct stmfbio_plane_config2 *c)
{
  /* Potential problem with this - if we have to pad the display for some
     reason, this will be incorrect. */
  return c->source.dim.w * (c->bitdepth >> 3);
}


int
stmfb_verify_baseaddress (const struct stmfb_info            *i,
                          const struct stmfbio_plane_config2 *c,
                          unsigned long                       plane_size,
                          unsigned long                       baseaddr)
{
  if (c == &i->current_planeconfig && !i->current_planeconfig_valid)
  {
    TRC( TRC_ID_ERROR,"current plane config not valid");
    return -EAGAIN;
  }

  if (baseaddr < i->ulPFBBase
      /* fight hackers -> overflow? */
      || (baseaddr + plane_size) < i->ulPFBBase
      /* pitch in combination with plane height is too too big */
      || (baseaddr + plane_size) > (i->ulPFBBase + i->ulFBSize)) {
    TRC( TRC_ID_ERROR,"Invalid base address: %lx (fb @ %lx %lu bytes (end @ %lx) for plane size %lu",
             baseaddr, i->ulPFBBase, i->ulFBSize,
             i->ulPFBBase +i->ulFBSize, plane_size);
    return -EINVAL;
  }

  return 0;
}

int
stmfb_verify_planeinfo (const struct stmfb_info         *i,
                        const struct stmfbio_planeinfo2 *plane)
{
  const struct stmfbio_plane_config2 * const c = &plane->config;
  const stm_pixel_format_t           *plane_formats;
  int                                 n_plane_Formats;
  stm_compound_control_range_t        inputRange;
  int                                 x;

  if(!i)
    return -ENODEV;

  /*
   * Check if there is a dedicated plane associate to this framebuffer
   * device.
   */
  if(!i->hFBPlane)
  {
    TRC( TRC_ID_STMFB_DEBUG,"No plane reserved for this fb device!");
    return signal_pending(current)?-ERESTARTSYS:-ENOMEM;
  }

  if(!plane || plane->layerid != 0)
    return -EINVAL;

  stmfb_display_planeconfig ("verify", &plane->config);

  /* check allowed maximum source size supported by the hardware */
  x=stm_display_plane_get_compound_control_range(i->hFBPlane, PLANE_CTRL_INPUT_WINDOW_VALUE, &inputRange);
  if (signal_pending (current))
    return -ERESTARTSYS;
  if (x < 0)
    return -EIO;

  if (c->source.dim.w <  inputRange.min_val.width
      || c->source.dim.w > inputRange.max_val.width
      || c->source.dim.h < inputRange.min_val.height
      || c->source.dim.h > inputRange.max_val.height
     )
    /* FIXME: or should we just clamp here? */
    return -EINVAL;

  /*
   * Low level driver is doing auto destination clamping in case of
   * scaling issue therfore we don't need to check rescale factors here.
   */

  /* check that the surface format is actually supported by the hardware. */
  n_plane_Formats = stm_display_plane_get_image_formats (i->hFBPlane,
                                                         &plane_formats);
  if (signal_pending (current))
    return -ERESTARTSYS;
  if(n_plane_Formats < 0)
    return -EIO;

  for (x = 0; x < n_plane_Formats; ++x)
    if (plane_formats[x] == c->format)
      break;
  if (x == n_plane_Formats)
    return -EINVAL;

  /* yes, it is, now verify pitch */
  if (!c->pitch)
    return -EINVAL;

  if (!c->bitdepth)
    return -EINVAL;
  if (c->pitch < c->source.dim.w * (c->bitdepth >> 3))
    /* pitch too small */
    return -EINVAL;

  /* and now for the base address */
  /* If the video memory hasn't been set yet, we are trying to find a default
     mode from the module parameters line, so ignore this test. */
  if (!i->ulPFBBase)
    return 0;
  return stmfb_verify_baseaddress (i, c, c->pitch * c->source.dim.h, c->baseaddr);
}

int
stmfb_outputinfo_to_videomode (const struct stmfb_info         * const i,
                               const struct stmfbio_outputinfo * const output,
                               stm_display_mode_t              * const vm)
{
  int x;
  int r;

  if(!i)
    return -ENODEV;

  if(!output || output->outputid != STMFBIO_OUTPUTID_MAIN || !vm)
    return -EINVAL;

  stmfb_display_outputstandard ("decode in", output->standard, output->standard_ex);

  vm->mode_id = STM_TIMING_MODE_RESERVED;

  for (x = 0; x < ARRAY_SIZE (stmfbio_standard_list); ++x) {
    if (((stmfbio_standard_list[x].stmfbio_standard & output->standard) == output->standard)&&
        ((stmfbio_standard_list[x].stmfbio_standard_ex & output->standard_ex) == output->standard_ex)) {
#if defined(STMFB_DEBUG_VAR)
      TRC( TRC_ID_STMFB_DEBUG_VAR,"found mode(%d): %u tvStandard: 0x%x stmfbid: 0x%.16llx extendedId 0x%.16llx %s",
               x, stmfbio_standard_list[x].mode,
               stmfbio_standard_list[x].tvStandard, output->standard,
               (unsigned long long)output->standard_ex, stmfbio_standard_list[x].name);
#else
      TRC( TRC_ID_STMFB_DEBUG_VAR,"found mode(%d): %u tvStandard: 0x%x stmfbid: 0x%.16llx extendedId 0x%.16llx",
               x, stmfbio_standard_list[x].mode,
               stmfbio_standard_list[x].tvStandard, output->standard, (unsigned long long)output->standard_ex);
#endif

      r = stm_display_output_get_display_mode (i->hFBMainOutput,
                                               stmfbio_standard_list[x].mode, vm);
      if ( r )
        return r;

      break;
    }
  }

  if(signal_pending(current))
    return -ERESTARTSYS;

  if(vm->mode_id == STM_TIMING_MODE_RESERVED)
  {
    TRC( TRC_ID_ERROR,"stmfb_std %.16llx extendedId %.16llx not supported",
             output->standard, (unsigned long long)output->standard_ex);
    return -EINVAL;
  }

  vm->mode_params.output_standards = stmfbio_standard_list[x].tvStandard;
  vm->mode_params.flags = STM_MODE_FLAGS_NONE;

  return 0;
}

static int
stmfb_fbdev_screeninfo_to_planeconfig (const struct stmfb_info        *i,
                                       const struct fb_var_screeninfo * const v,
                                       struct stmfbio_plane_config2   * const config)
{
  int c;

  /*
   * Skip checking real versus virtual resolutions in case user did
   * provide specific virtual resolution.
   */
  if(!i->user_buffer_size)
  {
    /* we do not support panning in the X direction so the real and virtual
       sizes must be the same */
    if ((v->xres != v->xres_virtual) || (v->yres > v->yres_virtual)) {
      TRC( TRC_ID_ERROR,"(virtual) resolution invalid: xres/_virt: %u/%u yres/_virt: %u/%u",
               v->xres, v->xres_virtual, v->yres, v->yres_virtual);
      return -EINVAL;
    }
    /* check that the panning offsets are usable */
    if(v->xoffset != 0 || (v->yoffset + v->yres) > v->yres_virtual) {
      TRC( TRC_ID_ERROR,"requested offsets > virtual: x/yoffset: %u/%u, yres/_virt: %u/%u",
               v->xoffset, v->yoffset, v->yres, v->yres_virtual);
      return -EINVAL;
    }
  }

  config->dest.x = config->source.x = 0;
  config->dest.y = config->source.y = 0;
  config->dest.dim.w = config->source.dim.w = v->xres;
  config->dest.dim.h = config->source.dim.h = v->yres;

  /* User specific virtual resolution to be used if provided */
  if(i->user_buffer_size)
  {
    config->source.dim.w = i->buffer_size.w;
    config->source.dim.h = i->buffer_size.h;
  }

  /* search for the exact pixel format, only look at the bit length of each
     component so we can use fbset -rgba to change mode */
  for (c = 0; c < ARRAY_SIZE (colour_formats); ++c)
  {
    if(v->transp.length == colour_formats[c].alength
       && v->red.length == colour_formats[c].rlength
       && v->green.length == colour_formats[c].glength
       && v->blue.length == colour_formats[c].blength
       && v->nonstd == colour_formats[c].nonstd) {

      if((v->transp.offset && v->transp.offset != colour_formats[c].aoffset)
         || (v->red.offset && v->red.offset != colour_formats[c].roffset)
         || (v->green.offset && v->green.offset != colour_formats[c].goffset)
         || (v->blue.offset && v->blue.offset != colour_formats[c].boffset))
        continue;

      /* the colour format matches */
      break;
    }
  }

  if(c < ARRAY_SIZE (colour_formats)
     && colour_formats[c].bitdepth == v->bits_per_pixel)
    config->format = colour_formats[c].fmt;
  else
  {
    /* The vars bitdepth and rgba specification is not consistent. Catching
       this allows us to just use fbset -depth to change quickly between the
       colour depths without having to specify -rgba as well or to use
       fb.modes settings which only pass the bitdepth (colour lengths come in
       as 0/0/0/0 in that case) */
    switch(v->bits_per_pixel)
    {
      case  8: config->format = SURF_CLUT8;    break;
      case 16: config->format = SURF_RGB565;   break;
      case 24: config->format = SURF_RGB888;   break;
      case 32: config->format = SURF_ARGB8888; break;
      default:
        TRC( TRC_ID_ERROR,"invalid bits_per_pixel (%d)", v->bits_per_pixel);
        return -EINVAL;
    }
  }

  /* now the pitch */
  config->bitdepth = stmfb_bitdepth_for_pixelformat (config->format);
  config->pitch = stmfb_pitch_for_config (config);

  config->baseaddr = i->ulPFBBase + (v->yoffset * config->pitch);

  return 0;
}

static int
stmfb_fbdev_screeninfo_to_videomode (const struct stmfb_info        * const i,
                                     const struct fb_var_screeninfo * const v,
                                     stm_display_mode_t             * const vm)
{
  unsigned long long pixclock;
  unsigned long      totallines, totalpixels;
  stm_scan_type_t    scanType;
  unsigned long      outputStandards;

  if (!i)
    return -ENODEV;

  switch (v->vmode & FB_VMODE_MASK)
  {
    case FB_VMODE_NONINTERLACED:
      scanType = STM_PROGRESSIVE_SCAN;
      break;
    case FB_VMODE_INTERLACED:
      scanType = STM_INTERLACED_SCAN;
      break;
    case FB_VMODE_DOUBLE:
    default:
        TRC( TRC_ID_ERROR,"Unsupported FB VMODE flags: %u", v->vmode);
        return -EINVAL;
  }

  /* Convert pixelclock from picoseconds to Hz.
     Note that on the sh4 we can do an unsigned 64bit/32bit divide. */
  pixclock = 1000000000000ULL;
  do_div (pixclock, v->pixclock);

  totallines  = v->yres + v->upper_margin + v->lower_margin + v->vsync_len;
  totalpixels = v->xres + v->left_margin + v->right_margin + v->hsync_len;

  vm->mode_id = STM_TIMING_MODE_RESERVED;

  if(stm_display_output_find_display_mode (i->hFBMainOutput,
                                           v->xres,
                                           v->yres,
                                           totallines,
                                           totalpixels,
                                           (unsigned long) pixclock,
                                           scanType,
                                           vm)<0)
  {
    TRC( TRC_ID_ERROR,"Unable to find appropirate display mode : %dx%d - %lu - %lu - %llu - %d",
    v->xres, v->yres, totallines, totalpixels, pixclock, (int)scanType);
    return signal_pending(current)?-ERESTARTSYS:-EINVAL;
  }

  /*
   * We have to make a sensible decision about the TV standard to use
   * if the timing mode can support more than one. The Linux framebuffer
   * setup gives us no help with this. If the configured or default encoding
   * isn't supported on the mode, fall back to the most sensible default.
   */
  outputStandards = vm->mode_params.output_standards;

  if(outputStandards==STM_OUTPUT_STD_UNDEFINED)
  {
    return 0;
  }

  if(outputStandards & i->main_config.sdtv_encoding)
    vm->mode_params.output_standards = i->main_config.sdtv_encoding;
  else if(outputStandards & i->default_sd_encoding)
    vm->mode_params.output_standards = i->default_sd_encoding;
  else if(outputStandards & STM_OUTPUT_STD_NTSC_M)
    vm->mode_params.output_standards = STM_OUTPUT_STD_NTSC_M;
  else if(outputStandards & STM_OUTPUT_STD_PAL_BDGHI)
    vm->mode_params.output_standards = STM_OUTPUT_STD_PAL_BDGHI;
  else if((outputStandards & STM_OUTPUT_STD_HD_MASK) != 0)
    vm->mode_params.output_standards = (outputStandards & STM_OUTPUT_STD_HD_MASK);
  else if(outputStandards & STM_OUTPUT_STD_VESA_MASK)
    vm->mode_params.output_standards = (outputStandards & STM_OUTPUT_STD_VESA_MASK);
  else if(outputStandards & STM_OUTPUT_STD_NTG5)
    vm->mode_params.output_standards = STM_OUTPUT_STD_NTG5;
  else if(outputStandards & STM_OUTPUT_STD_ED_MASK)
    vm->mode_params.output_standards = (outputStandards & STM_OUTPUT_STD_ED_MASK);
  else if(outputStandards == STM_OUTPUT_STD_CEA861)
    vm->mode_params.output_standards = STM_OUTPUT_STD_CEA861; /* For CEA only digital modes like 1080p120 */
  else if(outputStandards == (STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861))
    vm->mode_params.output_standards = (STM_OUTPUT_STD_HDMI_LLC_EXT|STM_OUTPUT_STD_CEA861); /* Depends on sink capbility HDMI 1.4 or HDMI 2.0 */
  else
    BUG();

  /*
   * TODO: 3D configuration
   */
  vm->mode_params.flags = STM_MODE_FLAGS_NONE;

  return 0;
}

/*
 * stmfb_decode_var
 * Get the video params out of 'var' and construct a hw specific video
 * mode record.
 */
int
stmfb_decode_var (struct fb_var_screeninfo       * const v,
                  stm_display_mode_t             * const vm,
                  struct stmfbio_planeinfo2      * const plane,
                  const struct stmfb_info        * const i)
{
  int ret = 0;

  stmfb_display_var("var in", v);

  if(vm)
    ret = stmfb_fbdev_screeninfo_to_videomode (i, v, vm);

  if(!ret)
  {
    plane->layerid = 0;

    plane->activate = 0;
    switch (v->activate & FB_ACTIVATE_MASK) {
      case FB_ACTIVATE_NOW:
        plane->activate = STMFBIO_ACTIVATE_IMMEDIATE;
        break;
      case FB_ACTIVATE_TEST:
        plane->activate = STMFBIO_ACTIVATE_TEST;
        break;
      default:
        break;
    }
    if (v->activate & FB_ACTIVATE_VBL)
      plane->activate |= STMFBIO_ACTIVATE_VBL;

    /* User specific virtual resolution to be used if provided */
    if(i->user_buffer_size)
    {
      v->xres_virtual = i->buffer_size.w;
      v->yres_virtual = i->buffer_size.h;
    }

    ret = stmfb_fbdev_screeninfo_to_planeconfig (i, v, &plane->config);
    if(!ret)
    {
      /*
       * Check if there is a dedicated plane associate to this framebuffer
       * device.
       */
      if(i->hFBPlane)
      {
        ret = stmfb_verify_planeinfo (i, plane);
        if (!ret)
        {
          /* If the video memory hasn't been set yet, we are trying to find a
             default mode from the module parameters line, so ignore this
             test. */
          if (i->ulFBSize)
          {
            ret = stmfb_verify_baseaddress (i, &plane->config,
                                            v->yres_virtual * plane->config.pitch,
                                            i->ulPFBBase);
            if (ret)
            {
              TRC( TRC_ID_ERROR,"Insufficient memory for requested virtual %u x %u pitch: %u",
                       v->xres_virtual, v->yres_virtual, plane->config.pitch);
              TRC( TRC_ID_ERROR,"            %lu available, %u requested",
                       i->ulFBSize, v->yres_virtual * plane->config.pitch);
            }
          }
        }
      }
    }
  }

#if defined(STMFB_DEBUG_VAR)
  if (ret)
    TRC(TRC_ID_ERROR, "Unable to match the above!");
#endif

  return ret;
}

int
stmfb_get_outputstandards (struct stmfb_info              * const i,
                           struct stmfbio_outputstandards * const stds)
{
  stm_display_mode_t dummy;
  uint32_t caps;

  stds->all_standards = STMFBIO_STD_UNKNOWN;
  stds->all_standards_ex = STMFBIO_STD_EX_UNKNOWN;

  if (stm_display_output_get_capabilities (i->hFBMainOutput, &caps) < 0) {
    TRC( TRC_ID_ERROR,"Unable to get output capabilities");
    return signal_pending (current) ? -ERESTARTSYS : -EIO;
  }

  TRC( TRC_ID_STMFB_DEBUG_VAR,"Output capabilities: 0x%x", caps);

  if (caps & OUTPUT_CAPS_SD_ANALOG) {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"adding SD standards");
    stds->all_standards |= STMFBIO_STD_SD;
  }

  if (caps & OUTPUT_CAPS_ED_ANALOG) {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"adding ED standards");
    stds->all_standards |= STMFBIO_STD_ED;
  }

  if (caps & OUTPUT_CAPS_HD_ANALOG) {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"adding HD standards");
    stds->all_standards |= (STMFBIO_STD_VESA
                            | STMFBIO_STD_SMPTE296M
                            | STMFBIO_STD_1080P_23_976
                            | STMFBIO_STD_1080P_24
                            | STMFBIO_STD_1080P_25
                            | STMFBIO_STD_1080P_29_97
                            | STMFBIO_STD_1080P_30
                            | STMFBIO_STD_1080I_60
                            | STMFBIO_STD_1080I_59_94
                            | STMFBIO_STD_1080I_50);

    /*
     * Test to see if the high framerate 1080p modes are available by trying
     * to get the mode descriptor for one.
     */
    if (!stm_display_output_get_display_mode (i->hFBMainOutput,
                                             STM_TIMING_MODE_1080P60000_148500, &dummy))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"adding fast 1080p standards");
      stds->all_standards |= (STMFBIO_STD_1080P_60
                              | STMFBIO_STD_1080P_59_94
                              | STMFBIO_STD_1080P_50);
    }
    else
    {
      if(signal_pending(current))
        return -ERESTARTSYS;
    }

    if (i->hFBHDMI)
    {
      uint32_t hdmi_caps;
      if (stm_display_output_get_capabilities (i->hFBHDMI, &hdmi_caps) < 0) {
        TRC( TRC_ID_ERROR,"Unable to get output capabilities");
        return signal_pending (current) ? -ERESTARTSYS : -EIO;
      }

      caps |= hdmi_caps;
    }
  }

  if (caps & OUTPUT_CAPS_UHD_DIGITAL) {
    /*
     * Test to see if the ultra high framerate 4K2KP modes are available by
     * trying to get the mode descriptor for one.
     */
    if (!stm_display_output_get_display_mode (i->hFBMainOutput,
                                             STM_TIMING_MODE_2160P30000_297000, &dummy))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"adding UHD/4K(low frame rate) Progressive on HDMI output standards");
      stds->all_standards_ex |= STMFBIO_STD_EX_2160P_LOW_FRAMERATE;
    }
    else
    {
      if(signal_pending(current))
        return -ERESTARTSYS;
    }

    if (!stm_display_output_get_display_mode (i->hFBMainOutput,
                                             STM_TIMING_MODE_2160P60000_594000, &dummy))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"adding UHD/4K(high frame rate) Progressive on HDMI output standards");
      stds->all_standards_ex |= STMFBIO_STD_EX_2160P_HIGH_FRAMERATE;
    }
    else
    {
      if(signal_pending(current))
        return -ERESTARTSYS;
    }
  }

  return 0;
}

int
stmfb_videomode_to_outputinfo (const stm_display_mode_t  * const vm,
                               struct stmfbio_outputinfo * const output)
{
  unsigned long tvStandard = vm->mode_params.output_standards;
  int           x;

  output->outputid = STMFBIO_OUTPUTID_MAIN;
  output->activate = STMFBIO_ACTIVATE_IMMEDIATE;

  for (x = 0; x < ARRAY_SIZE (stmfbio_standard_list); ++x)
  {
    if (stmfbio_standard_list[x].mode == vm->mode_id
        && stmfbio_standard_list[x].tvStandard == tvStandard)
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"found mode(%d): %u tvStandard: 0x%lx stmfbid: 0x%.16llx extendedID 0x%.16llx",
               x, vm->mode_id, tvStandard,
               stmfbio_standard_list[x].stmfbio_standard,
               (unsigned long long)stmfbio_standard_list[x].stmfbio_standard_ex);
      output->standard = stmfbio_standard_list[x].stmfbio_standard;
      output->standard_ex = stmfbio_standard_list[x].stmfbio_standard_ex;

      return 0;
    }
  }

  /* STM_TIMING_MODE_CUSTOM used for panel output on Newman  */
  if (vm->mode_id == STM_TIMING_MODE_CUSTOM)
  {
    if (vm->mode_params.active_area_width == 1920 &&
        vm->mode_params.active_area_height== 1080 &&
        vm->mode_params.vertical_refresh_rate == 60000) {
      output->standard = STMFBIO_STD_1080P_60;
      output->standard_ex = STMFBIO_STD_EX_UNKNOWN;
    }
    return 0;
  }

  return -EINVAL;
}

static void
stmfbio_planeconfig_to_fb_var_screeninfo (const struct stmfb_info            * const i,
                                          const struct stmfbio_plane_config2 * const config,
                                          struct fb_var_screeninfo           * const v)
{
  int c;

  stmfb_display_planeconfig ("encode in", config);

  v->xres           = config->dest.dim.w;
  v->xres_virtual   = config->source.dim.w;
  v->xoffset        = 0;
  v->yres           = config->dest.dim.h;
  v->yres_virtual   = config->source.dim.h;
  v->yoffset        = (config->baseaddr - i->ulPFBBase) / config->pitch;
  v->grayscale      = 0;

  /* User specific virtual resolution to be used if provided */
  if(i->user_buffer_size)
  {
    v->xres_virtual = i->buffer_size.w;
    v->yres_virtual = i->buffer_size.h;
  }

  for (c = 0; c < ARRAY_SIZE (colour_formats); c++)
  {
    if (colour_formats[c].fmt == config->format)
    {
      v->bits_per_pixel = colour_formats[c].bitdepth;

      v->transp.offset = colour_formats[c].aoffset;
      v->transp.length = colour_formats[c].alength;
      v->red.offset    = colour_formats[c].roffset;
      v->red.length    = colour_formats[c].rlength;
      v->green.offset  = colour_formats[c].goffset;
      v->green.length  = colour_formats[c].glength;
      v->blue.offset   = colour_formats[c].boffset;
      v->blue.length   = colour_formats[c].blength;
      v->nonstd        = colour_formats[c].nonstd;

      break;
    }
  }
  if (unlikely (c == ARRAY_SIZE (colour_formats)))
  {
    v->bits_per_pixel = 0;
    memset (&v->red,    0, sizeof (v->red));
    memset (&v->green,  0, sizeof (v->green));
    memset (&v->blue,   0, sizeof (v->blue));
    memset (&v->transp, 0, sizeof (v->transp));
  }

  /* Height/Width of picture in mm */
  v->height = v->width = -1;
}

static void
stmfbio_videomode_to_fb_var_screeninfo (const stm_display_mode_t * const vm,
                                        struct fb_var_screeninfo * const v)
{
  u64 timingtmp;

  /* Convert pixel clock (Hz) to pico seconds, correctly rounding around 1/2 */
  timingtmp = 1000000000000ULL + (vm->mode_timing.pixel_clock_freq / 2);
  do_div (timingtmp, vm->mode_timing.pixel_clock_freq);
  v->pixclock     = (u32) timingtmp;

  v->xres         = vm->mode_params.active_area_width;
  v->hsync_len    = vm->mode_timing.hsync_width;
  v->left_margin  = (vm->mode_params.active_area_start_pixel
                     - v->hsync_len);
  v->right_margin = (vm->mode_timing.pixels_per_line
                     - vm->mode_params.active_area_start_pixel
                     - vm->mode_params.active_area_width);

  v->yres         = vm->mode_params.active_area_height;
  v->vsync_len    = vm->mode_timing.vsync_width;

  if (STM_INTERLACED_SCAN == vm->mode_params.scan_type) {
    v->vsync_len   *= 2;
    v->upper_margin = ((vm->mode_params.active_area_start_line - 1) * 2)
                       - v->vsync_len;
  } else {
    v->upper_margin = (vm->mode_params.active_area_start_line - 1)
                       - v->vsync_len;
  }

  v->lower_margin = (vm->mode_timing.lines_per_frame
                     - vm->mode_params.active_area_height
                     - v->vsync_len
                     - v->upper_margin);

  v->sync = 0;
  if (vm->mode_timing.hsync_polarity)
    v->sync |= FB_SYNC_HOR_HIGH_ACT;
  if (vm->mode_timing.vsync_polarity)
    v->sync |= FB_SYNC_VERT_HIGH_ACT;

  v->vmode = ((vm->mode_params.scan_type == STM_PROGRESSIVE_SCAN)
              ? FB_VMODE_NONINTERLACED : FB_VMODE_INTERLACED);
}

/*
 *  stmfb_encode_var
 *  Fill a 'var' structure based on the values in 'vm' and maybe other
 *  values read out of the hardware. This is the reverse of stmfb_decode_var
 */
int
stmfb_encode_var (struct fb_var_screeninfo        * const v,
                  const stm_display_mode_t        * const vm,
                  const struct stmfbio_planeinfo2 * const plane,
                  const struct stmfb_info         * const i)
{
  memset(v, 0, sizeof(struct fb_var_screeninfo));

  stmfbio_videomode_to_fb_var_screeninfo (vm, v);
  stmfbio_planeconfig_to_fb_var_screeninfo (i, &plane->config, v);

  stmfb_display_var("encode out", v);

  return 0;
}

/*
 *  stmfb_encode_mode
 *  Create a valid 'var' and plane config from 'vm' read from Hardware
 *  This is used to get output video timing that has been set from a non
 *  framebuffer interface "v4l2"
 */
int
stmfb_encode_mode (const stm_display_mode_t * const vm,
                          struct stmfb_info  * const i)
{

  struct stmfbio_planeinfo2  plane;
  struct fb_var_screeninfo *v = &i->info.var;

  memset(&plane, 0, sizeof(plane));
  stmfbio_videomode_to_fb_var_screeninfo (vm, v);
  i->info.mode = (struct fb_videomode *) fb_match_mode (v,
                                                        &i->info.modelist);
  BUG_ON (!i->info.mode);

  i->current_videomode = *vm;
  i->current_videomode_valid = 1;

  v->xres_virtual = v->xres;
  v->yres_virtual = v->yres;

  stmfb_fbdev_screeninfo_to_planeconfig (i, v, &plane.config);

  /* User specific virtual resolution to be used if provided */
  if(i->user_buffer_size)
  {
    v->xres_virtual = i->buffer_size.w;
    v->yres_virtual = i->buffer_size.h;
  }

  i->current_planeconfig = plane.config;
  i->current_planeconfig_valid = 1;

  return 0;
}

/******************************************************************************
 * Extended var information to control ST specific plane properties
 */
int
stmfb_queuebuffer (struct stmfb_info * const i)
{
  int res = 0;

  if(i->hQueueInterface == NULL)
  {
    return signal_pending(current)?-ERESTARTSYS:-EIO;
  }
  spin_lock_irq(&(i->framebufferSpinLock));
  i->num_outstanding_updates++;
  spin_unlock_irq(&(i->framebufferSpinLock));

  TRC( TRC_ID_STMFB_DEBUG_VAR,"pa sz: %x/%u, fmt: %d %ubpp pitch/lines: %u/%u",
          i->current_buffer_setup.src.primary_picture.video_buffer_addr,
          i->current_buffer_setup.src.primary_picture.video_buffer_size,
          i->current_buffer_setup.src.primary_picture.color_fmt,
          i->current_buffer_setup.src.primary_picture.pixel_depth,
          i->current_buffer_setup.src.primary_picture.pitch,
          i->current_buffer_setup.src.primary_picture.height);

  if(stm_display_source_queue_buffer(i->hQueueInterface,&i->current_buffer_setup)<0)
  {
    spin_lock_irq(&(i->framebufferSpinLock));
    i->num_outstanding_updates--;
    spin_unlock_irq(&(i->framebufferSpinLock));

    if(signal_pending(current))
      res = -ERESTARTSYS;
    else
      res = -EINVAL;
  }

  TRC( TRC_ID_STMFB_DEBUG_VAR,"finished res = %x",res);

  return res;
}


static int
stmfb_decode_var_ex (struct stmfbio_var_screeninfo_ex2 * const v,
                     stm_display_buffer_t              * const b,
                     struct stmfb_info                 * const i)
{
  stm_display_plane_h  plane;
  int                  layerid = v->layerid;

  /*
   * Check if there is a dedicated plane associate to this framebuffer
   * device.
   */
  if(!i->hFBPlane)
  {
    TRC( TRC_ID_STMFB_DEBUG,"No plane capabilities for this fb device!");
    return signal_pending(current)?-ERESTARTSYS:-ENODEV;
  }

  /*
   * Currently only support the framebuffer plane, but are leaving open the
   * possibility of accessing other planes in the future.
   */
  if(layerid != 0 || v->activate == STMFBIO_ACTIVATE_TEST)
    return -EINVAL;

  plane = i->hFBPlane;

  v->failed = 0;

  if(v->caps & STMFBIO_VAR_CAPS_COLOURKEY)
  {
    stm_color_key_config_t *config = &b->src.ColorKey;
    /* Update the color key enable/disable state */
    config->flags  |= SCKCF_ENABLE;
    if (v->colourKeyFlags & STMFBIO_COLOURKEY_FLAGS_ENABLE)
    {
      config->enable  = 1;

      config->flags  |= SCKCF_FORMAT;
      config->format  = SCKCVF_RGB;

      config->flags  |= (SCKCF_MINVAL | SCKCF_MAXVAL);
      config->minval  = v->min_colour_key;
      config->maxval  = v->max_colour_key;

      config->flags |= (SCKCF_R_INFO | SCKCF_G_INFO | SCKCF_B_INFO);
      if(v->colourKeyFlags & STMFBIO_COLOURKEY_FLAGS_INVERT)
      {
        TRC( TRC_ID_STMFB_DEBUG_VAR,"Inverse colour key.");
        config->r_info  = SCKCCM_INVERSE;
        config->g_info  = SCKCCM_INVERSE;
        config->b_info  = SCKCCM_INVERSE;
      }
      else
      {
        TRC( TRC_ID_STMFB_DEBUG_VAR,"Normal colour key.");
        config->r_info  = SCKCCM_ENABLED;
        config->g_info  = SCKCCM_ENABLED;
        config->b_info  = SCKCCM_ENABLED;
      }
    }
    else
    {
      config->enable = 0;
    }

    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      b->src.flags &= ~STM_BUFFER_SRC_COLOR_KEY;
      if(stm_display_plane_set_compound_control(plane, PLANE_CTRL_SRC_COLOR_VALUE, config)<0)
      {
        v->failed |= STMFBIO_VAR_CAPS_COLOURKEY;
      }
      else
      {
         if(stm_display_plane_set_control(plane, PLANE_CTRL_SRC_COLOR_STATE, CONTROL_ON)<0)
         {
           v->failed |= STMFBIO_VAR_CAPS_COLOURKEY;
         }
      }
    }
    /* Use queued buffer color key configuration */
    else if (v->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_SRC_COLOR_STATE, CONTROL_OFF)<0)
      {
        v->failed |= STMFBIO_VAR_CAPS_COLOURKEY;
      }
      else
      {
        /*
         * Enable Color Key configuration into buffer descriptor.
         */
        b->src.flags |= STM_BUFFER_SRC_COLOR_KEY;
      }
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_COLOURKEY))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"ColorKey %s (min_colour_key = %08x - max_colour_key = %08x - flags = %08x)",
              config->enable ? "Disabled":"Enabled",
              v->min_colour_key, v->max_colour_key,
              v->colourKeyFlags);
      i->current_var_ex.min_colour_key = v->min_colour_key;
      i->current_var_ex.max_colour_key = v->max_colour_key;
      i->current_var_ex.colourKeyFlags = v->colourKeyFlags;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_FLICKER_FILTER)
  {
    if((b->src.primary_picture.pixel_depth == 8 || b->src.primary_picture.color_fmt == SURF_ACLUT88)
       && (v->ff_state != STMFBIO_FF_OFF))
      v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;


    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      switch(v->ff_state)
      {
        case STMFBIO_FF_OFF:
          TRC( TRC_ID_STMFB_DEBUG_VAR,"Flicker Filter Off.");
          if(stm_display_plane_set_control(plane, PLANE_CTRL_FLICKER_FILTER_STATE,
             PLANE_FLICKER_FILTER_DISABLE) < 0)
            v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
          break;

        case STMFBIO_FF_SIMPLE:
          TRC( TRC_ID_STMFB_DEBUG_VAR,"Flicker Filter Simple.");
          if(stm_display_plane_set_control(plane, PLANE_CTRL_FLICKER_FILTER_MODE,
             PLANE_FLICKER_FILTER_SIMPLE) < 0)
            v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
          if(stm_display_plane_set_control(plane, PLANE_CTRL_FLICKER_FILTER_STATE,
             PLANE_FLICKER_FILTER_ENABLE) < 0)
            v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
          break;

        case STMFBIO_FF_ADAPTIVE:
          TRC( TRC_ID_STMFB_DEBUG_VAR,"Flicker Filter Adaptive.");
          if(stm_display_plane_set_control(plane, PLANE_CTRL_FLICKER_FILTER_MODE,
             PLANE_FLICKER_FILTER_ADAPTIVE) < 0)
            v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
          if(stm_display_plane_set_control(plane, PLANE_CTRL_FLICKER_FILTER_STATE,
             PLANE_FLICKER_FILTER_ENABLE) < 0)
            v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
          break;

        default:
          TRC( TRC_ID_STMFB_DEBUG_VAR,"Flicker Filter state invalid.");
          v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
          break;
      }
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_FLICKER_FILTER))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Flicker Filter Mode = %d.",v->ff_state);
      i->current_var_ex.ff_state = v->ff_state;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_PREMULTIPLIED)
  {
    if(v->premultiplied_alpha)
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Premultiplied Alpha.");
      b->src.flags |= STM_BUFFER_SRC_PREMULTIPLIED_ALPHA;
    }
    else
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Non-Premultiplied Alpha.");
      b->src.flags &= ~STM_BUFFER_SRC_PREMULTIPLIED_ALPHA;
    }

    i->current_var_ex.premultiplied_alpha = v->premultiplied_alpha;
  }

  if(v->caps & STBFBIO_VAR_CAPS_RESCALE_COLOUR_TO_VIDEO_RANGE)
  {
    if(v->rescale_colour_to_video_range)
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Rescale colour to video output range.");
      b->dst.ulFlags |= STM_BUFFER_DST_RESCALE_TO_VIDEO_RANGE;
    }
    else
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Full colour output range.");
      b->dst.ulFlags &= ~STM_BUFFER_DST_RESCALE_TO_VIDEO_RANGE;
    }

    i->current_var_ex.rescale_colour_to_video_range = v->rescale_colour_to_video_range;
  }


  if(v->caps & STMFBIO_VAR_CAPS_OPACITY)
  {

    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      b->src.flags &= ~STM_BUFFER_SRC_CONST_ALPHA;
      if(stm_display_plane_set_control(plane, PLANE_CTRL_TRANSPARENCY_VALUE, v->opacity)<0)
      {
        v->failed |= STMFBIO_VAR_CAPS_OPACITY;
      }
      else
      {
        if(stm_display_plane_set_control(plane, PLANE_CTRL_TRANSPARENCY_STATE, CONTROL_ON)<0)
        {
          v->failed |= STMFBIO_VAR_CAPS_OPACITY;
        }
        else
        {
          TRC( TRC_ID_STMFB_DEBUG_VAR, "IMMEDIATE: Opacity = %d.",v->opacity);
        }
      }
    }
    /* Use queued buffer opacity */
    else if (v->activate == STMFBIO_ACTIVATE_ON_NEXT_CHANGE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_TRANSPARENCY_STATE, CONTROL_OFF)<0)
      {
        v->failed |= STMFBIO_VAR_CAPS_OPACITY;
      }
      else
      {
        b->src.flags     |= STM_BUFFER_SRC_CONST_ALPHA;
        b->src.ulConstAlpha = v->opacity;
        TRC( TRC_ID_STMFB_DEBUG_VAR, "ON_NEXT_CHANGE: Opacity = %d.",v->opacity);
      }
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_OPACITY))
    {
      if(v->opacity != i->current_var_ex.opacity)
      {
#ifdef CONFIG_PM_RUNTIME
        if(v->opacity == 0) /* Full transparent : allow disabling HW */
          pm_runtime_put_sync(&i->platformDevice->dev);
        if(i->current_var_ex.opacity == 0)
          pm_runtime_get_sync(&i->platformDevice->dev);
#endif
        i->current_var_ex.opacity = v->opacity;
      }
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_GAIN)
  {
    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_GLOBAL_GAIN_VALUE, v->gain)<0)
        v->failed |= STMFBIO_VAR_CAPS_GAIN;
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_GAIN))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Gain = %d.",v->gain);
      i->current_var_ex.gain = v->gain;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_BRIGHTNESS)
  {
    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_BRIGHTNESS, v->brightness)<0)
        v->failed |= STMFBIO_VAR_CAPS_BRIGHTNESS;
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_BRIGHTNESS))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Brightness = %d.",v->brightness);
      i->current_var_ex.brightness = v->brightness;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_SATURATION)
  {
    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_SATURATION, v->saturation)<0)
        v->failed |= STMFBIO_VAR_CAPS_SATURATION;
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_SATURATION))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Saturation = %d.",v->saturation);
      i->current_var_ex.saturation = v->saturation;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_CONTRAST)
  {
    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_CONTRAST, v->contrast)<0)
        v->failed |= STMFBIO_VAR_CAPS_CONTRAST;
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_CONTRAST))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Contrast = %d.",v->contrast);
      i->current_var_ex.contrast = v->contrast;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_TINT)
  {
    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_TINT, v->tint)<0)
        v->failed |= STMFBIO_VAR_CAPS_TINT;
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_TINT))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Tint = %d.",v->tint);
      i->current_var_ex.tint = v->tint;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_ALPHA_RAMP)
  {
    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_control(plane, PLANE_CTRL_ALPHA_RAMP, v->alpha_ramp[0] | (v->alpha_ramp[1]<<8))<0)
        v->failed |= STMFBIO_VAR_CAPS_ALPHA_RAMP;
    }

    if(!(v->failed & STMFBIO_VAR_CAPS_ALPHA_RAMP))
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Alpha Ramp = [%d,%d].",v->alpha_ramp[0],v->alpha_ramp[1]);
      i->current_var_ex.alpha_ramp[0] = v->alpha_ramp[0];
      i->current_var_ex.alpha_ramp[1] = v->alpha_ramp[1];
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_ZPOSITION)
  {
    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_depth(plane, i->hFBMainOutput, v->z_position, 1)<0)
      {
        TRC( TRC_ID_ERROR,"FAILED: Set Z Position = %d.",v->z_position);
        v->failed |= STMFBIO_VAR_CAPS_ZPOSITION;
      }
      else
      {
        if(stm_display_plane_get_depth(plane, i->hFBMainOutput, &i->current_var_ex.z_position)<0)
        {
          TRC( TRC_ID_ERROR,"FAILED: Get Z Position");
          v->failed |= STMFBIO_VAR_CAPS_ZPOSITION;
        }
        else
        {
          TRC( TRC_ID_STMFB_DEBUG_VAR,"Real Z Position = %d.",i->current_var_ex.z_position);
        }
      }
    }
    else
    {
      TRC( TRC_ID_STMFB_DEBUG_VAR,"Z Position = %d.",v->z_position);
      i->current_var_ex.z_position = v->z_position;
    }
  }

  if(v->caps & STMFBIO_VAR_CAPS_FULLSCREEN)
  {
    stm_plane_mode_t plane_mode = MANUAL_MODE;

    plane_mode = v->fullscreen ? AUTO_MODE : MANUAL_MODE;

    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      if(stm_display_plane_set_control(i->hFBPlane,
                  PLANE_CTRL_OUTPUT_WINDOW_MODE, plane_mode) <0)
      {
        TRC( TRC_ID_ERROR,"FAILED: Set plane output window auto = %d.",plane_mode);
        v->failed |= STMFBIO_VAR_CAPS_FULLSCREEN;
      }

      if(!(v->failed & STMFBIO_VAR_CAPS_FULLSCREEN))
      {
        TRC( TRC_ID_STMFB_DEBUG_VAR,"plane output window mode = %d.",plane_mode);
        i->current_var_ex.fullscreen = v->fullscreen;
      }
     }
     else
     {
       TRC( TRC_ID_STMFB_DEBUG_VAR,"plane output window mode = %d.",plane_mode);
       i->current_var_ex.fullscreen = v->fullscreen;
     }
  }
  if(v->failed != 0)
  {
    if(signal_pending(current))
      return -EINTR;
    else
      return -EINVAL;
  }
  i->current_var_ex.activate = v->activate;

  return 0;
}


int
stmfb_set_var_ex (struct stmfbio_var_screeninfo_ex2 * const v,
                  struct stmfb_info                 * const i)
{
  stm_display_plane_h  plane;
  int                  layerid = v->layerid;
  int                  ret;

  /*
   * Check if there is a dedicated plane associate to this framebuffer
   * device.
   */
  if(!i->hFBPlane)
  {
    TRC( TRC_ID_STMFB_DEBUG,"No plane capabilities for this fb device!");
    return signal_pending(current)?-ERESTARTSYS:-ENODEV;
  }

  /*
   * Currently only support the framebuffer plane, but are leaving open the
   * possibility of accessing other planes in the future.
   */
  if(layerid != 0)
    return -EINVAL;

  plane = i->hFBPlane;

  v->failed = v->caps & ~i->current_var_ex.caps;
  if(v->failed != 0)
    return -EINVAL;

  if(v->activate == STMFBIO_ACTIVATE_TEST)
  {
    /*
     * Test is if a Z position change is valid.
     */
    if(v->caps & STMFBIO_VAR_CAPS_ZPOSITION)
    {
      ret = stm_display_plane_set_depth(plane, i->hFBMainOutput, v->z_position, 0);
      if(ret<0)
      {
        v->failed |= STMFBIO_VAR_CAPS_ZPOSITION;
        return ret;
      }
    }

    /*
     * Check that flicker filtering is not being switched on when the
     * framebuffer is in 8bit CLUT mode, flicker filtering is only usable
     * in true RGB modes.
     */
    if(v->caps & STMFBIO_VAR_CAPS_FLICKER_FILTER)
    {
      if((i->info.var.bits_per_pixel == 8) && (v->ff_state != STMFBIO_FF_OFF))
      {
        TRC( TRC_ID_ERROR,"Cannot enable flicker filter in 8bit modes");
        v->failed |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
        return -EINVAL;
      }
    }

  }
  else
  {
    if((ret = stmfb_decode_var_ex(v, &i->current_buffer_setup, i))<0)
      return ret;

    if(v->activate == STMFBIO_ACTIVATE_IMMEDIATE)
    {
      BUG_ON (!i->current_planeconfig_valid);

      ret = stmfb_queuebuffer(i);
      if(ret<0)
      {
        v->failed |= v->caps & (STMFBIO_VAR_CAPS_COLOURKEY                     |
                                STMFBIO_VAR_CAPS_FLICKER_FILTER                |
                                STMFBIO_VAR_CAPS_PREMULTIPLIED                 |
                                STMFBIO_VAR_CAPS_OPACITY);
        return ret;
      }
      else
      {
        TRC( TRC_ID_STMFB_DEBUG_VAR,"Queued buffer, num_outstanding_updates = %d", i->num_outstanding_updates);
        /*
         * Don't let too many updates back up on the plane, but on the other
         * hand don't block by default.
         */
        wait_event(i->framebuffer_updated_wait_queue, i->num_outstanding_updates < 4);
      }
    }
  }

  TRC( TRC_ID_STMFB_DEBUG_VAR,"Finished");

  return 0;
}


int
stmfb_encode_var_ex (struct stmfbio_var_screeninfo_ex2 * const v,
                     const struct stmfb_info           * const i)
{
  stm_display_plane_h  plane;
  int                  layerid = v->layerid;
  bool isApplicable = false;

  /*
   * Check if there is a dedicated plane associate to this framebuffer
   * device.
   */
  if(!i->hFBPlane)
  {
    TRC( TRC_ID_STMFB_DEBUG,"No plane capabilities for this fb device!");
    return 0;
  }

  /*
   * Currently only support the framebuffer plane, but are leaving open the
   * possibility of accessing other planes in the future.
   */
  if(layerid != 0)
    return -EINVAL;

  plane = i->hFBPlane;

  memset(v, 0, sizeof(struct stmfbio_var_screeninfo_ex2));
  v->layerid = layerid;

  if(stm_display_plane_is_feature_applicable(plane, PLANE_FEAT_SRC_COLOR_KEY, &isApplicable) < 0)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"PLANE_FEAT_SRC_COLOR_KEY is not applicable");
  }

  if(isApplicable)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane has Color Keying");
    v->caps |= STMFBIO_VAR_CAPS_COLOURKEY;
    v->colourKeyFlags = 0;
  }

  if(stm_display_plane_is_feature_applicable(plane, PLANE_FEAT_FLICKER_FILTER, &isApplicable) < 0)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"PLANE_FEAT_FLICKER_FILTER is not applicable");
  }

  if(isApplicable)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane has Flicker Filter");
    v->caps |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
    v->ff_state = STMFBIO_FF_OFF;
  }

/* TODO: FIXME PLANE_CAPS_RESCALE_TO_VIDEO_RANGE need to be matched with new features */
#if 0
  if(caps.ulCaps & PLANE_CAPS_RESCALE_TO_VIDEO_RANGE)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane can rescale to video range");
    v->caps |= STBFBIO_VAR_CAPS_RESCALE_COLOUR_TO_VIDEO_RANGE;
    v->rescale_colour_to_video_range = 0;
  }
#endif

  if(stm_display_plane_is_feature_applicable(plane, PLANE_FEAT_TRANSPARENCY, &isApplicable) < 0)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"PLANE_FEAT_TRANSPARENCY is not applicable");
  }
  if(isApplicable)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane has global alpha");
    v->caps |= STMFBIO_VAR_CAPS_OPACITY;
    v->opacity = 255; /* fully opaque */
#ifdef CONFIG_PM_RUNTIME
    pm_runtime_get_sync(&i->platformDevice->dev);
#endif

    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane supports premultipled alpha");
    v->caps |= STMFBIO_VAR_CAPS_PREMULTIPLIED;
    v->premultiplied_alpha = 1;

    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane supports alpha ramp");
    v->caps |= STMFBIO_VAR_CAPS_ALPHA_RAMP;
    v->alpha_ramp[0] = 0;
    v->alpha_ramp[1] = 255;
  }

  if(stm_display_plane_is_feature_applicable(plane, PLANE_FEAT_GLOBAL_COLOR, &isApplicable) < 0)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"PLANE_FEAT_GLOBAL_COLOR is not applicable");
  }

  if(isApplicable)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane has gain control");
    v->caps |= STMFBIO_VAR_CAPS_GAIN;
    v->gain = 255; /* 100% */
  }

  v->caps |= STMFBIO_VAR_CAPS_ZPOSITION;
  if(stm_display_plane_get_depth(plane, i->hFBMainOutput, &v->z_position) < 0)
  {
    TRC( TRC_ID_ERROR,"stm_display_plane_get_depth() fails!");
  }
  if(stm_display_plane_is_feature_applicable(plane, PLANE_FEAT_WINDOW_MODE, &isApplicable) < 0)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"PLANE_FEAT_WINDOW_MODE is not applicable");
  }
  if(isApplicable)
  {
    TRC( TRC_ID_STMFB_DEBUG_VAR,"Plane has fullscreen mode support");

    v->caps |= STMFBIO_VAR_CAPS_FULLSCREEN;
    v->fullscreen = 0;
  }

  if(signal_pending(current))
    return -ERESTARTSYS;
  else
    return 0;
}


static uint32_t stmfbio_to_3d_flags[STMFBIO_LINE_ALTERNATIVE+1] = {
  STM_MODE_FLAGS_NONE,
  STM_MODE_FLAGS_3D_SBS_HALF,
  STM_MODE_FLAGS_3D_TOP_BOTTOM,
  STM_MODE_FLAGS_3D_FRAME_PACKED,
  STM_MODE_FLAGS_3D_FIELD_ALTERNATIVE,
  STM_MODE_FLAGS_3D_FRAME_SEQUENTIAL,
  STM_MODE_FLAGS_3D_LL_RR,
  STM_MODE_FLAGS_3D_LINE_ALTERNATIVE
};


void
stmfb_set_videomode_3d_flags(struct stmfb_info  * const i,
                             stm_display_mode_t * const vm)
{
  if ( i->current_3d_config.mode > STMFBIO_LINE_ALTERNATIVE )
    {
      BUG();
      return;
    }

  vm->mode_params.flags &= ~STM_MODE_FLAGS_3D_MASK;
  vm->mode_params.flags |= stmfbio_to_3d_flags[i->current_3d_config.mode];
}
