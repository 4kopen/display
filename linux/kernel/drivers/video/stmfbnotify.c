/***********************************************************************
 *
 * File: linux/kernel/drivers/video/stmfbnotify.c
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/eventfd.h>
#include <linux/kobject.h>
#include <linux/wait.h>
#include <linux/workqueue.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <linux/err.h>

#include <linux/fb.h>
#include <stm_display.h>

#include <vibe_debug.h>

#include "stmfb.h"
#include "stmfbinfo.h"

#include <stm_event.h>
#include <stm_registry.h>




struct stmfb_event
{
    struct stmfb_info  *info;
    int                 fd;
    struct eventfd_ctx *eventfd;
    struct work_struct  remove;
    wait_queue_t        wq;
    wait_queue_head_t  *wqh;
    poll_table          pt;
    struct list_head    list;
};

static void stmfb_notify_event(struct stmfb_info * const info, int event)
{
    struct list_head *tmp;

    list_for_each(tmp, &info->event_list)
    {
        struct stmfb_event *e    = list_entry(tmp, struct stmfb_event, list);
        struct file        *file = eventfd_fget(e->fd);

        if (!IS_ERR(file))
        {
            __u64 value = 0;
            /* reset to zero the counter */
            file->f_op->write(file, (const char *)&value, sizeof(__u64), NULL);
            fput(file);
        }
        eventfd_signal(e->eventfd, event);
    }
}


static void stmfb_event_callback(int evt_num, stm_event_info_t *events)
{
    int evt_cnt;
    struct stmfb_info  *i = (struct stmfb_info *) events->cookie;

    spin_lock(&i->event_lock);

    for (evt_cnt = 0; evt_cnt < evt_num; evt_cnt++)
    {
        if (events[evt_cnt].event.event_id == STM_OUTPUT_MODE_CHANGE_EVENT)
        {
            stmfb_notify_event(i, FB_EVENT_MODE_CHANGE);
            TRC( TRC_ID_STMFB, "Output mode change event processed\n");
        }
    }

    spin_unlock(&i->event_lock);
}


int stmfb_init_event_notification(struct stmfb_info *info)
{
    int ret = 0;
    stm_object_h                   output_type;
    stm_event_subscription_h       subs;
    stm_event_subscription_entry_t subs_entry;

    /*
     * Get the output object type, it is required to subscribe to output events
     */
    ret = stm_registry_get_object_type(info->hFBMainOutput, &output_type);
    if (ret)
    {
        TRC( TRC_ID_ERROR, "failed to get output type\n");
        goto subs_failed;
    }

    /*
     * Create a subscription and register for it's events
     */
    subs_entry.object     = output_type;
    subs_entry.event_mask = STM_OUTPUT_MODE_CHANGE_EVENT;
    subs_entry.cookie     = info;

    ret = stm_event_subscription_create(&subs_entry, 1, &subs);
    if (ret)
    {
        TRC( TRC_ID_ERROR, "event subscription failed\n");
        goto subs_failed;
    }

    /*
     * Set the call back function for subscribed events
     */
    ret = stm_event_set_handler(subs, (stm_event_handler_t)stmfb_event_callback);

    if (ret)
    {
        TRC( TRC_ID_ERROR, "failed to register the signal handler\n");
        goto set_handler_failed;
    }

    info->subs = subs;

    spin_lock_init(&info->event_lock);
    INIT_LIST_HEAD(&info->event_list);

    return 0;

set_handler_failed:
    if (stm_event_subscription_delete(subs))
    {
        TRC( TRC_ID_ERROR, "failed to delete subscription\n");
    }
subs_failed:
    return ret;
}


/*
 * Called on eventfd close.
 */
static void stmfb_event_remove(struct work_struct *work)
{
    struct stmfb_event *event = container_of(work, struct stmfb_event, remove);
    kfree(event);
}


static int stmfb_event_wakeup(wait_queue_t *wq, unsigned int mode, int sync,
                             void *key)
{
    struct stmfb_event *event = container_of(wq, struct stmfb_event, wq);
    unsigned long flags       = (unsigned long)key;

    if (flags & POLLHUP)
    {
       __remove_wait_queue(event->wqh, &event->wq);
       spin_lock(&event->info->event_lock);
       list_del(&event->list);
       spin_unlock(&event->info->event_lock);
       schedule_work(&event->remove);
    }

    return 0;
}


/*
 * Make the poll() sleeping.
 */
static void stmfb_event_ptable_queue_proc(struct file *file,
                                         wait_queue_head_t *wqh, poll_table *pt)
{
    struct stmfb_event *event = container_of(pt, struct stmfb_event, pt);

    event->wqh = wqh;
    add_wait_queue(wqh, &event->wq);
}


/*
 * Store a new eventfd by using a sysfs key for input.
 */
int stmfb_notify_store(struct device           *device,
                       struct device_attribute *attr,
                       const char              *buf,
                       size_t                   n)
{
    unsigned int        fd  = 0;
    int                 ret = 0;
    struct file        *file;
    struct stmfb_event *event;
    struct stmfb_info  *info = dev_get_drvdata(device);

    event = kmalloc(sizeof(*event), GFP_KERNEL);
    if (!event)
        return -ENOMEM;

    ret = sscanf(buf, "%u", &fd);
    if (ret < 1)
    {
        ret = -EINVAL;
        goto out_free;
    }

    file = eventfd_fget(fd);
    if (IS_ERR(file))
    {
        ret = -EINVAL;
        goto out_free;
    }

    event->eventfd = eventfd_ctx_fileget(file);
    if (!event->eventfd)
    {
        ret = PTR_ERR(event->eventfd);
        goto out_fput;
    }

    event->info = info;
    event->fd   = fd;

    INIT_LIST_HEAD(&event->list);
    INIT_WORK(&event->remove, stmfb_event_remove);

    /* specific init functions */
    init_waitqueue_func_entry(&event->wq, stmfb_event_wakeup);
    init_poll_funcptr(&event->pt, stmfb_event_ptable_queue_proc);

    /* check for poll() */
    if (file->f_op->poll(file, &event->pt) & POLLHUP)
    {
        ret = 0;
        goto out_fput;
    }

    /* add the new client to the event list */
    spin_lock(&info->event_lock);
    list_add(&event->list, &info->event_list);
    spin_unlock(&info->event_lock);

    fput(file);

    return n;

out_fput:
    fput(file);

out_free:
    kfree(event);

    return ret;
}


void stmfb_cleanup_event_notification(struct stmfb_info *info)
{

    if (stm_event_subscription_delete(info->subs))
    {
        TRC(TRC_ID_ERROR, "failed to delete subscription: ");
    }


    info->subs = 0;

    /* notify possible waiting events */
    stmfb_notify_event(info, FB_EVENT_FB_UNREGISTERED);

}
