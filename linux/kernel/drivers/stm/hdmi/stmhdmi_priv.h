/************************************************************************
Copyright (C) 2000-2010 STMicroelectronics. All Rights Reserved.

This file is part of the Display Engine.

Display Engine is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

Display Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Display Engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

The Display Engine Library may alternatively be licensed under a
proprietary license from ST.
************************************************************************/

#ifndef _STM_HDMI_PRIV_H
#define _STM_HDMI_PRIV_H

#include <vibe_debug.h>

#ifdef DEBUG
#define DPRINTK(a,b...) TRC(TRC_ID_MAIN_INFO, "stmhdmi: %s: " a, __FUNCTION__ ,##b)
#define DPRINTK_NOPREFIX(a,b...) TRC(TRC_ID_MAIN_INFO, a, ##b)
#else
#define DPRINTK(a,b...)
#define DPRINTK_NOPREFIX(a,b...)
#endif

#endif
