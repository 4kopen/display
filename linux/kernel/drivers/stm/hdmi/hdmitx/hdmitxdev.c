/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/hdmi/hdmitx/hdmitxdev.c
 * Copyright (c) 2005-2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/version.h>
#include <linux/compiler.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>
#include <linux/kthread.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/platform_device.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <linux/gpio.h>

#include <asm/uaccess.h>
#include <linux/semaphore.h>

#include <stm_display.h>
#include <linux/stm/stmcoredisplay.h>
#include <linux/stm/hdmiplatform.h>
#include <linux/stm/stmcorehdmi.h>
#include <linux/stm/stmcorelink.h>

#include "stmhdmi.h"
#include "stmhdmi_priv.h"

#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/err.h>

#include <vibe_debug.h>
#include <thread_vib_settings.h>

static dev_t firstdevice;
static char *hdmi0;


module_param(hdmi0, charp, 0444);
MODULE_PARM_DESC(hdmi0, "[enable|disable]");

static int thread_vib_hdmid[2] = { THREAD_VIB_HDMID_POLICY, THREAD_VIB_HDMID_PRIORITY };
module_param_array_named(thread_VIB_Hdmid,thread_vib_hdmid, int, NULL, 0644);
MODULE_PARM_DESC(thread_VIB_Hdmid, "VIB-Hdmid thread:s(Mode),p(Priority)");

struct of_device_id stmhdmi_match[] = {
    { .compatible = "st,hdmi" },
    {},
};

extern int stmhdmi_manager(void *data);
extern long stmhdmi_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
extern int stmhdmi_determine_link_connectivity(struct stm_hdmi *hdmi);

int stmhdmi_open_display_device(struct stm_hdmi *dev);
int stmhdmi_close_display_device(struct stm_hdmi *dev);

int stmhdmi_open(struct inode *inode,struct file *filp)
{
  struct stm_hdmi *hdmi = container_of(inode->i_cdev, struct stm_hdmi, cdev);
  if (stmhdmi_open_display_device(hdmi) <0)
    return -ENODEV;
  filp->private_data = hdmi;
  return 0;
}

int stmhdmi_release(struct inode *inode,struct file *filp)
{
  struct stm_hdmi *hdmi = filp->private_data;
  stmhdmi_close_display_device(hdmi);
  return 0;
}

/*
 * Reading from the HDMI device returns the full raw EDID data read from
 * the downstream device.
 */
static ssize_t stmhdmi_read(struct file *filp, char __user *buf, size_t sz, loff_t *off)
{
  struct stm_hdmi *dev = (struct stm_hdmi *)filp->private_data;
  ssize_t retval = 0;
  stm_event_subscription_h       subscription = NULL;
  stm_event_subscription_entry_t subscription_entry;
  stm_event_info_t               evt_info;
  unsigned int                   number_of_events;

  if(*off >= (1+dev->edid_info.base_raw[STM_EDID_EXTENSION])*sizeof(edid_block_t))
    return retval;

  if(*off + sz > (1+dev->edid_info.base_raw[STM_EDID_EXTENSION])*sizeof(edid_block_t))
    sz = (1+dev->edid_info.base_raw[STM_EDID_EXTENSION])*sizeof(edid_block_t) - *off;

  if(mutex_lock_interruptible(&dev->lock))
    return -ERESTARTSYS;

  if((filp->f_flags & O_NONBLOCK)&&(dev->edid_info.display_type == STM_DISPLAY_INVALID))
      retval = -EAGAIN;
  else if(dev->edid_info.display_type == STM_DISPLAY_INVALID)
  {
    TRC(TRC_ID_HDMI, "Create subscription : hdmi_object : %p\n", dev->hdmi_output);

    subscription_entry.cookie = NULL;
    subscription_entry.object = dev->hdmi_output;
    subscription_entry.event_mask = HDMI_EVENT_DISPLAY_CONNECTED;

    retval = stm_event_subscription_create(&subscription_entry, 1, &subscription);
    if(retval)
    {
      TRC(TRC_ID_ERROR,"Error can't create event subscription\n");
      goto evt_subs_failed;
    }

    retval = stm_event_set_wait_queue(subscription, &dev->status_wait_queue, true);
    if(retval)
    {
      TRC(TRC_ID_ERROR,"Unable to set wait queue for HDMI Tx events\n");
      goto evt_set_queue_failed;
    }

    mutex_unlock(&dev->lock);

    retval = stm_event_wait(subscription, -1, 1, &number_of_events, &evt_info);
    if(mutex_lock_interruptible(&dev->lock))
      return -ERESTARTSYS;
  }

  if(dev->edid_info.display_type != STM_DISPLAY_INVALID)
  {
    /* First check if we need data from the basic block */
    if (*off < sizeof(edid_block_t))
    {
      size_t base_sz = (*off + sz) < sizeof(edid_block_t)?sz : sizeof(edid_block_t) - *off;
      if(copy_to_user(buf,
                      dev->edid_info.base_raw + *off,
                      base_sz))
      {
        retval = -EFAULT;
      }
      else
      {
        *off += base_sz;
        buf += base_sz;
        retval += base_sz;
        sz -= base_sz;
      }
    }
    /*
     * Do we need data from the extension block
     * We already ensured at the beginning that we would not overflow
     */
    if (sz && dev->edid_info.extension_raw)
    {
      loff_t ext_off = *off - sizeof(edid_block_t);
      if(dev->edid_info.extension_raw != NULL)
      {
        if(copy_to_user(buf,
                        *(dev->edid_info.extension_raw) + ext_off,
                        sz))
        {
          retval = -EFAULT;
        }
        else
        {
          *off += sz;
          retval += sz;
        }
      }
    }
  }

evt_set_queue_failed:
  if(subscription)
  {
    if(stm_event_subscription_delete(subscription))
      TRC(TRC_ID_ERROR,"Failed to delete event subscription\n");
  }
evt_subs_failed:
  mutex_unlock(&dev->lock);
  return retval;
}


struct file_operations hdmi_fops = {
  .owner = THIS_MODULE,
  .open  = stmhdmi_open,
  .release = stmhdmi_release,
  .read  = stmhdmi_read,
  .unlocked_ioctl = stmhdmi_ioctl
};


/******************************************************************************/

static void stmhdmi_vsync_cb(stm_vsync_context_handle_t context, uint32_t timingevent, stm_time64_t vsynctime)
{
  stm_display_output_connection_status_t  hdmi_status;
  struct stm_hdmi *hdmi_data = (struct stm_hdmi *)context;
  /*
  if((timingevent & (STM_TIMING_EVENT_TOP_FIELD|STM_TIMING_EVENT_FRAME)) == 0)
    return;
  */

  if(stm_display_output_get_connection_status(hdmi_data->hdmi_output, &hdmi_status) < 0)
  {
    TRC(TRC_ID_ERROR,  "stmhdmi_vsync_cb(): error calling stm_display_output_get_connection_status()\n");
  }

  if((hdmi_status != hdmi_data->status) && ( hdmi_status!= STM_DISPLAY_DISCONNECTED))
  {
    hdmi_data->status         = hdmi_status;
    hdmi_data->status_changed = 1;
    wake_up(&hdmi_data->status_wait_queue);
  }

  if (hdmi_data->scdc_scrambling_status_polling_value>0)
  {
    /* recommendation of the spec is to poll the scrambling status during 200ms */
    /* but the onluy recommendation is to trigg an error after 200ms if         */
    /* scrambling status not set                                                */
    if (hdmi_data->scdc_scrambling_status_polling_value == 1)
    {
      if (hdmi_data->scdc_scrambling_status_polling_req == false)
      {
        hdmi_data->scdc_scrambling_status_polling_req = true;
        wake_up(&hdmi_data->status_wait_queue);
      }
    }
    hdmi_data->scdc_scrambling_status_polling_value--;
  }

  if (hdmi_data->scdc_update_flag_polling_period > 0)
  {
    hdmi_data->scdc_update_flag_polling_value--;
    if (hdmi_data->scdc_update_flag_polling_value == 0)
    {
      if (hdmi_data->scdc_update_flag_polling_req == false)
      {
        hdmi_data->scdc_update_flag_polling_req = true;
        wake_up(&hdmi_data->status_wait_queue);
      }
      hdmi_data->scdc_update_flag_polling_value = hdmi_data->scdc_update_flag_polling_period;
    }
  }
}


/******************************************************************************/

int stmhdmi_destroy(struct stm_hdmi *hdmi)
{
  struct stmcore_display_pipeline_data display_pipeline;

  if(!hdmi)
    return 0;

  /*
   * Terminate HDMI management thread
   */
  if(hdmi->thread != NULL)
  {
    kthread_stop(hdmi->thread);
    wake_up(&hdmi->status_wait_queue);
    hdmi->thread = NULL;
  }

  if(stmcore_get_display_pipeline(hdmi->pipeline_id, &display_pipeline)<0)
  {
    TRC(TRC_ID_ERROR, "HDMI display pipeline (%u) not available, check your board setup\n",hdmi->pipeline_id);
    BUG();
    return -EINVAL;
  }
  /*
   * TODO: the register/unregister vsync callback routines should take the
   *       pipeline ID not an internal data structure pointer.
   */
  stmcore_unregister_vsync_callback(display_pipeline.display_runtime, &hdmi->vsync_cb_info);

  if(hdmi->class_device)
  {
    device_unregister(hdmi->class_device);
    hdmi->class_device = NULL;
  }

  cdev_del(&hdmi->cdev);

  if(hdmi->hdmi_output)
  {
    stm_display_output_close(hdmi->hdmi_output);
    hdmi->hdmi_output = NULL;
  }
  if(hdmi->main_output)
  {
    stm_display_output_close(hdmi->main_output);
    hdmi->main_output = NULL;
  }
  if(hdmi->link)
  {
    stm_display_link_close (hdmi->link);
    hdmi->link = NULL;
  }

  if(hdmi->cd_device)
  {
    stm_display_device_close(hdmi->cd_device);
    hdmi->cd_device = NULL;
  }
  hdmi->device_use_count = 0;

  kfree(hdmi->spd_metadata);
  kfree(hdmi);

  return 0;
}


/****************************************************************************
 * Device initialization helpers
 */
static int __init stmhdmi_create_spd_metadata(struct stm_hdmi *hdmi)
{
  if((hdmi->spd_metadata = kzalloc(sizeof(stm_display_metadata_t)+sizeof(stm_hdmi_info_frame_t),GFP_KERNEL))==0)
  {
    TRC(TRC_ID_ERROR,"Cannot allocate SPD metadata\n");
    return -ENOMEM;
  }

  hdmi->spd_metadata->size = sizeof(stm_display_metadata_t)+sizeof(stm_hdmi_info_frame_t);
  hdmi->spd_metadata->type = STM_METADATA_TYPE_SPD_IFRAME;

  hdmi->spd_frame = (stm_hdmi_info_frame_t*)&hdmi->spd_metadata->data[0];
  hdmi->spd_frame->type    = HDMI_SPD_INFOFRAME_TYPE;
  hdmi->spd_frame->version = HDMI_SPD_INFOFRAME_VERSION;
  hdmi->spd_frame->length  = HDMI_SPD_INFOFRAME_LENGTH;
  strncpy(&hdmi->spd_frame->data[HDMI_SPD_INFOFRAME_VENDOR_START],"STMicro",HDMI_SPD_INFOFRAME_VENDOR_LENGTH);
  strncpy(&hdmi->spd_frame->data[HDMI_SPD_INFOFRAME_PRODUCT_START],"STLinux",HDMI_SPD_INFOFRAME_PRODUCT_LENGTH);
  hdmi->spd_frame->data[HDMI_SPD_INFOFRAME_SPI_OFFSET] = HDMI_SPD_INFOFRAME_SPI_PC;

  return 0;
}


static struct stm_hdmi * __init stmhdmi_create_hdmi_dev_struct(void)
{
  struct stm_hdmi *hdmi;
  hdmi = kzalloc(sizeof(struct stm_hdmi), GFP_KERNEL);
  if(!hdmi)
    return NULL;

  mutex_init(&(hdmi->lock));
  mutex_init(&(hdmi->hdmilock));
  spin_lock_init(&(hdmi->spinlock));
  init_waitqueue_head(&(hdmi->status_wait_queue));

  return hdmi;
}


static int __init stmhdmi_register_device(struct stm_hdmi *hdmi,
                                   int id,
                                   struct stmcore_display_pipeline_data *display_pipeline)
{
  cdev_init(&(hdmi->cdev),&hdmi_fops);
  hdmi->cdev.owner = THIS_MODULE;
  kobject_set_name(&(hdmi->cdev.kobj),"hdmi%d.0",id);

  if(cdev_add(&(hdmi->cdev),MKDEV(MAJOR(firstdevice),id),1))
    return -ENODEV;

  hdmi->class_device = device_create(display_pipeline->class_device->class,
                                     display_pipeline->class_device,
                                     hdmi->cdev.dev,
                                     hdmi,
                                     "%s", kobject_name(&hdmi->cdev.kobj));

  if(IS_ERR(hdmi->class_device))
  {
    hdmi->class_device = NULL;
    return -ENODEV;
  }

#if defined(SDK2_ENABLE_HDMI_TX_ATTRIBUTES)
  if(stmhdmi_create_class_device_files(hdmi, display_pipeline->class_device))
    return -ENODEV;
#endif

  return 0;
}

void *stmhdmi_get_device_tree_data(struct platform_device *pdev)
{
  struct device_node *hdmi_root_node_p = NULL;
  struct stm_hdmi_platform_data *hdmi_platform_data;
  struct device_node *link_root_node_p = NULL;
  pdev->id =0;

  hdmi_root_node_p = of_find_compatible_node(NULL, NULL, "st,hdmi");
  if ( !hdmi_root_node_p)
   return NULL;

  hdmi_platform_data = devm_kzalloc(&pdev->dev,sizeof(struct stm_hdmi_platform_data), GFP_KERNEL);
  if(!hdmi_platform_data)
  {
    TRC(TRC_ID_MAIN_INFO, "%s : Unable to allocate platform data\n", __FUNCTION__);
    return ERR_PTR(-ENOMEM);
  }

  of_property_read_u32(hdmi_root_node_p, "pipeline-id",  &(hdmi_platform_data->pipeline_id));
  hdmi_platform_data->device_id = of_alias_get_id(pdev->dev.of_node, "hdmi");
  pdev->id = hdmi_platform_data->device_id;

  hdmi_platform_data->link_device_id = 0;
  link_root_node_p = of_parse_phandle(hdmi_root_node_p, "link-dev",0);
  if(link_root_node_p)
  {
    of_property_read_u32(link_root_node_p, "device-id", &hdmi_platform_data->link_device_id);
    DPRINTK("Using link device %d\n",hdmi_platform_data->link_device_id);
  }

  return (void*)(hdmi_platform_data);
}

/****************************************************************************
 * Platform driver implementation
 */
static int stmhdmi_probe(struct platform_device *pdev)
{
  struct stm_hdmi_platform_data *platform_data;
  struct stmcore_display_pipeline_data display_pipeline;
  stm_display_link_type_t             link_type;
  stm_display_link_capability_t       link_capability;

  struct stm_hdmi      *hdmi;
  char                 *paramstring;

  char               thread_name[14];
  int                sched_policy;
  struct sched_param sched_param;

  platform_data = (struct stm_hdmi_platform_data*)stmhdmi_get_device_tree_data(pdev);

  if(!platform_data)
  {
    TRC(TRC_ID_MAIN_INFO, "platform data pointer is NULL\n");
    return -EINVAL;
  }

  if(pdev->id != 0)
  {
    TRC(TRC_ID_ERROR, "only one HDMI TX output device supported\n");
    BUG();
    return -EINVAL;
  }

  if(stmcore_get_display_pipeline(platform_data->pipeline_id, &display_pipeline)<0)
  {
    TRC(TRC_ID_ERROR, "HDMI display pipeline (%u) not available, check your board setup\n",platform_data->pipeline_id);
    BUG();
    return -EINVAL;
  }

  if((hdmi = stmhdmi_create_hdmi_dev_struct()) == NULL)
    return -ENOMEM;

  TRC(TRC_ID_HDMI,"new hdmi structure = %p\n",hdmi);

  hdmi->display_device_id = display_pipeline.device_id;
  if(stm_display_open_device(hdmi->display_device_id, &hdmi->cd_device)<0)
  {
    TRC(TRC_ID_ERROR, "HDMI cannot open display device = %u\n",platform_data->device_id);
    goto exit_nodev;
  }


  hdmi->link_device_id = platform_data->link_device_id;
  hdmi->pipeline_id  = platform_data->pipeline_id;
  hdmi->video_type   = (STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_24BIT);

  switch(pdev->id)
  {
    case 0:
      paramstring = hdmi0;
      break;
    default:
      paramstring = NULL;
      break;
  }

  if(paramstring)
  {
    if(paramstring[0] == 'd' || paramstring[0] == 'D')
    {
      TRC(TRC_ID_HDMI, "hdmi%d.0 is initially disabled, use 'stfbset -e hdmi' to enable it\n",pdev->id);
      hdmi->disable = 1;
    }
  }

  hdmi->stop_output = 1;
  hdmi->Current_hdcpversion=0;
  hdmi->hdcp_power_changed=false;
  hdmi->hdcp_enable = 0;
  hdmi->enc_enable  = 1;
  hdmi->hdcp_status = 0;
  hdmi->hdcp_stopped = 0;
  hdmi->edid_updated = 0;
  hdmi->current_hdcp_status =0;
  hdmi->max_pixel_repeat = 1;
  hdmi->non_strict_edid_semantics = 0;
  hdmi->avmute = 0;
  hdmi->device =NULL;
  hdmi->device_use_count =0;
  /*
   * Set the default CEA selection behaviour to use the aspect ratio in the EDID
   */
  hdmi->cea_mode_from_edid = 1;

  /*
   * Note that we are trusting the output identifiers are valid
   * and pointing to correct output types.
   */

  if(stm_display_device_open_output(hdmi->cd_device, display_pipeline.main_output_id, &(hdmi->main_output)) < 0)
  {
    TRC(TRC_ID_ERROR, "Cannot open display output main = %d",display_pipeline.main_output_id);
  }
  if(stm_display_device_open_output(hdmi->cd_device, display_pipeline.hdmi_output_id, &(hdmi->hdmi_output)) < 0)
  {
    TRC(TRC_ID_ERROR, "Cannot open display output hdmi = %d",display_pipeline.hdmi_output_id);
  }

  if(hdmi->main_output == NULL || hdmi->hdmi_output == NULL)
  {
    TRC(TRC_ID_ERROR, "Cannot get display outputs main = %d, hdmi = %d",
            display_pipeline.main_output_id,display_pipeline.hdmi_output_id);
    goto exit_nodev;
  }

  hdmi->hdmi_output_id = display_pipeline.hdmi_output_id;

  if(stm_display_output_get_capabilities(hdmi->hdmi_output, &hdmi->capabilities)<0)
  {
    TRC(TRC_ID_ERROR, "Cannot get hdmi output capabilities");
    goto exit_nodev;
  }

  if(!(hdmi->capabilities & OUTPUT_CAPS_HDMI))
  {
    TRC(TRC_ID_ERROR, "Provided HDMI output identifier doesn't support HDMI??");
    goto exit_nodev;
  }

  if(stmhdmi_create_spd_metadata(hdmi))
  {
    stmhdmi_destroy(hdmi);
    return -ENOMEM;
  }

  /*
   * If we split the HDMI management into another module then we should change
   * the owner field in the callback info to THIS_MODULE. However this is
   * linked into the coredisplay module at the moment we do not want to have
   * another reference to ourselves.
   */
  INIT_LIST_HEAD(&(hdmi->vsync_cb_info.node));
  hdmi->vsync_cb_info.owner   = NULL;
  hdmi->vsync_cb_info.context = hdmi;
  hdmi->vsync_cb_info.cb      = stmhdmi_vsync_cb;
  if(stmcore_register_vsync_callback(display_pipeline.display_runtime, &hdmi->vsync_cb_info)<0)
  {
    TRC(TRC_ID_ERROR, "Cannot register hdmi vsync callback\n");
    goto exit_nodev;
  }
  /*
   * Open display link connection
   */
  if ( stm_display_link_open (hdmi->link_device_id, &hdmi->link)<0 )
  {
    TRC(TRC_ID_ERROR, "Unable to open the DISPLAY link %d\n",hdmi->link_device_id);
    goto exit_nodev;
  }

  /*
   * Get display link type
   */
  stm_display_link_get_type(hdmi->link, &link_type);
  if ( link_type != STM_DISPLAY_LINK_TYPE_HDMI )
  {
    TRC(TRC_ID_ERROR, "Display link is not HDMI type\n");
    goto exit_nodev;
  }

  /*
   * Check display link capability
   */
  if ( stm_display_link_get_capability (hdmi->link, &link_capability)<0 )
  {
    TRC(TRC_ID_ERROR, "Display link is not HDMI type\n");
    goto exit_nodev;
  }

  if (link_capability.rxsense)
  {
    if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_RXSENSE_ENABLE, 1)<0)
    {
      TRC(TRC_ID_ERROR, "Can't enable rxsense\n");
      goto exit_nodev;
    }
  }
  hdmi->link_capability = link_capability;

  /* Create the thread */
  snprintf(thread_name, sizeof(thread_name), "VIB-Hdmid/%d", pdev->id);
  hdmi->thread = kthread_create(stmhdmi_manager, hdmi, thread_name);
  if (hdmi->thread == NULL)
  {
    TRC(TRC_ID_ERROR, "Cannot start hdmi thread id = %d\n",pdev->id);
    stmhdmi_destroy(hdmi);
    return -ENOMEM;
  }

  /* Set thread scheduling settings */
  sched_policy               = thread_vib_hdmid[0];
  sched_param.sched_priority = thread_vib_hdmid[1];
  if ( sched_setscheduler(hdmi->thread, sched_policy, &sched_param) )
  {
    TRC(TRC_ID_ERROR, "FAILED to set thread scheduling parameters: name=%s, policy=%d, priority=%d", \
        thread_name, sched_policy,sched_param.sched_priority);
  }

  if(stmhdmi_register_device(hdmi, pdev->id, &display_pipeline))
  {
    goto exit_nodev;
  }

    platform_set_drvdata(pdev,hdmi);

  /* Set wake-up capability for hdmi through sysfs*/
  device_set_wakeup_capable(&pdev->dev,true);

  stm_display_device_close(hdmi->cd_device);
  hdmi->cd_device = NULL;
  hdmi->cd_device_use_count = 0;

  /* Wake up the thread */
  wake_up_process(hdmi->thread);

  return 0;

exit_nodev:
  stmhdmi_destroy(hdmi);
  return -ENODEV;
}


static int __exit stmhdmi_remove(struct platform_device *pdev)
{
  struct stm_hdmi *i = (struct stm_hdmi *)platform_get_drvdata(pdev);

  TRC(TRC_ID_HDMI,"\n");

  if(!i)
    return 0;

  /*
   * Ensure display device is opened before closing its ressources
   */
  if(!i->cd_device)
  {
    if(stm_display_open_device(i->display_device_id, &i->cd_device)<0)
    {
      TRC(TRC_ID_ERROR, "Unable to open the DISPLAY %d\n",i->display_device_id);
      i->cd_device = NULL;
      return -ENODEV;
    }
  }

  stmhdmi_destroy(i);

  platform_set_drvdata(pdev,NULL);

  return 0;
}


static void stmhdmi_shutdown(struct platform_device *pdev)
{
  return;
}

static struct platform_driver stmhdmi_driver = {
   .probe    = stmhdmi_probe,
   .shutdown = stmhdmi_shutdown,
   .remove   = __exit_p(stmhdmi_remove),
   .driver   = {
       .name     = "hdmi",
       .owner    = THIS_MODULE
   },
   .driver.of_match_table = of_match_ptr(stmhdmi_match),
};


static void __exit stmhdmi_exit(void)
{
  platform_driver_unregister(&stmhdmi_driver);
  unregister_chrdev_region(firstdevice,1);
}


static int __init stmhdmi_init(void)
{
  if(alloc_chrdev_region(&firstdevice,0,1,"hdmi")<0)
  {
    TRC(TRC_ID_ERROR, "%s: unable to allocate device numbers\n",__FUNCTION__);
    return -ENODEV;
  }

  platform_driver_register(&stmhdmi_driver);

  return 0;
}


/******************************************************************************
 *  Modularization
 */

module_init(stmhdmi_init);
module_exit(stmhdmi_exit);

MODULE_LICENSE("GPL");
