/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/hdmi/hdmitx/hdmitxkthread.c
 * Copyright (c) 2005-2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/
#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/freezer.h>
#include <linux/slab.h>
#include <linux/fb.h>
#include <linux/i2c.h>
#include <linux/kthread.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <stm_common.h>
#include <stm_event.h>
#include <linux/gpio.h>
#include <asm/uaccess.h>
#include <linux/semaphore.h>

#include <stm_display.h>
#include <stm_display_link.h>
#include <linux/stm/stmcoredisplay.h>
#include <linux/stm/stmcorehdmi.h>
#include <linux/stm/stmcorelink.h>
#include "stm_registry.h"
#include "stmhdmi.h"
#include "stmhdmi_priv.h"

#include <vibe_debug.h>

#define DISPLAY_LINK_EVT_NUMBER  (4)    /* Display link event Number: HPD, Rx sense and HDCP events */
#define HDCP_STATUS_HDMI_MODE    (0x1<<12)
#define GET_HDCP_STATUS_HDMI_MODE(x)            ((x&HDCP_STATUS_HDMI_MODE)==HDCP_STATUS_HDMI_MODE)
#define SINK_STATUS(x) (((unsigned int)(x)[0]) | (((unsigned int)(x)[1]) << 8))
#define HDMI_WAIT_1_MS_BWN_BIT_CLK_RATIO_WRITE_AND_TMDS_CLK_RESUME (1000)
#define HDMI_WAIT_1_MS_BWN_STOP_HDMI_AND_START_HDMI_FOR_4K30_4K30_TRANSITION (1000)
#define HDMI_WAIT_2_MS_BWN_STOP_HDMI_AND_START_HDMI_FOR_4K60_SDED_TRANSITION (2000)
#define HDCP_VERSION(version)  (version==4)?STM_DISPLAY_LINK_HDCP_PROTOCOL_2_2:STM_DISPLAY_LINK_HDCP_PROTOCOL_1_X
#define HDCP_STOPPED 0
#define HDCP_RUNNING 1

static int vic1_24bit[4]={31,75,16,76};
static int vic2_24bit[6]={96,106,101,97,107,102};
static int vicsded_24bit[6]={1,2,5,6,17,20};
static int vic4kp30_24bit[9]={93,94,95,98,99,100,103,104,105};

static int vic1_30_36bit[12]={19,68,20,4,5,69,32,72,33,73,34,74};
static int vic2_30_36bit[9]={93,103,94,104,95,105,98,99,100};

#if defined(SDK2_ENABLE_HDMI_TX_ATTRIBUTES)
static inline void hdmi_sysfs_notify(struct kobject *kobj, const char *dir, const char *attr)
{
  sysfs_notify(kobj, dir, attr);
}
#else
static inline void hdmi_sysfs_notify(struct kobject *kobj, const char *dir, const char *attr) {}
#endif

#define HPD_STATE_TO_STR(state) \
  (state == STM_DISPLAY_LINK_HPD_STATE_LOW ? "Low" : (state == STM_DISPLAY_LINK_HPD_STATE_HIGH ? "High" : "Unknown"))
#define RXSENSE_STATE_TO_STR(state) \
  (state == STM_DISPLAY_LINK_RXSENSE_STATE_INACTIVE ? "Inactive" : (state == STM_DISPLAY_LINK_RXSENSE_STATE_ACTIVE ? "Active" : "Unknown"))
#define HDCP_STATE_TO_STR(state) \
   (state== HDCP_STOPPED ? "HDCP_STOPPED": "HDCP_RUNNING")

static int hdmi_open_device(struct stm_hdmi *dev)
{
  #ifdef CONFIG_PM_RUNTIME
  /*
   * DPM : Open required devices
   */
  mutex_lock(&dev->hdmilock);
  if(!dev->device)
  {
    if(stm_display_open_device(dev->display_device_id, &dev->device)<0)
    {
      TRC(TRC_ID_ERROR, "Unable to open the LINK %d\n",dev->display_device_id);
      mutex_unlock(&dev->hdmilock);
      return -ENODEV;
    }
    dev->device_use_count ++;
  }
  mutex_unlock(&dev->hdmilock);
  #endif
  return 0;
}

static int hdmi_release_device(struct stm_hdmi *dev)
{
  #ifdef CONFIG_PM_RUNTIME
  /*
   * DPM : Close used devices
   */
  mutex_lock(&dev->hdmilock);
  if(dev->device_use_count > 0)
    dev->device_use_count --;
  if(dev->device && (dev->device_use_count == 0))
  {
    stm_display_device_close(dev->device);
    dev->device = NULL;
  }
  mutex_unlock(&dev->hdmilock);
  #endif
  return 0;
}

/******************************************************************************
 * Functions to check the EDID for a valid mode and enable the HDMI output
 */
static int stmhdmi_configure_hdmi_formatting(struct stm_hdmi *hdmi, stm_display_mode_t *mode)
{
  uint32_t video_type;
  uint32_t colour_depth;
  uint32_t quantization = 0;
  uint32_t support_deepcolour;
  uint32_t char_rate_div_10 = 0;
  int32_t  retVal = 0;

  /*
   * We are connected to a CEA conformant HDMI device. In this case the spec
   * says we must do HDMI; if the device does not support YCrCb then force
   * RGB output.
   */
  if((hdmi->edid_info.cea_capabilities & (STM_CEA_CAPS_YUV|STM_CEA_CAPS_422|STM_CEA_CAPS_420))
   ||((hdmi->edid_info.display_type == STM_DISPLAY_HDMI) && (hdmi->edid_updated == false)))
  {
    video_type = hdmi->video_type & (STM_VIDEO_OUT_RGB |
                                     STM_VIDEO_OUT_YUV |
                                     STM_VIDEO_OUT_422 |
                                     STM_VIDEO_OUT_444 |
                                     STM_VIDEO_OUT_420);
  }
  else
  {
    video_type = STM_VIDEO_OUT_RGB;
  }

  TRC(TRC_ID_HDMI,"Setting HDMI output format %s\n",(video_type&STM_VIDEO_OUT_RGB)?"RGB":"YUV");

  if (hdmi->non_strict_edid_semantics)
  {
    switch(hdmi->video_type & STM_VIDEO_OUT_DEPTH_MASK)
    {
      case STM_VIDEO_OUT_30BIT:
        colour_depth = STM_VIDEO_OUT_30BIT;
        break;
      case STM_VIDEO_OUT_36BIT:
        colour_depth = STM_VIDEO_OUT_36BIT;
        break;
      case STM_VIDEO_OUT_48BIT:
        colour_depth = STM_VIDEO_OUT_48BIT;
        break;
      case STM_VIDEO_OUT_24BIT:
      default:
        colour_depth = STM_VIDEO_OUT_24BIT;
        break;
    }

    if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_SINK_SUPPORTS_DEEPCOLOR, 1) < 0)
    {
      TRC(TRC_ID_ERROR, "Unable to set sink deepcolor state on HDMI output");
    }
  }
  else
  {
    support_deepcolour = ((hdmi->edid_info.hdmi_vsdb_flags & STM_HDMI_VSDB_DC_MASK)
                        ||(hdmi->edid_info.hdmi_hf_dc_flags & STM_HDMI_HF_VSDB_DC_MASK))?1:0;

    if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_SINK_SUPPORTS_DEEPCOLOR, support_deepcolour) < 0)
    {
      TRC(TRC_ID_ERROR, "Unable to set sink deepcolor state on HDMI output, HW probably doesn't support it");
      support_deepcolour = 0;
    }

    /*
     * Filter the requested colour depth based on the EDID and colour format,
     * falling back to standard colour if there is any mismatch.
     */
    if(!support_deepcolour               ||
       (video_type & STM_VIDEO_OUT_422)  ||
       ((video_type & STM_VIDEO_OUT_444) && !(hdmi->edid_info.hdmi_vsdb_flags & STM_HDMI_VSDB_DC_Y444))||
       ((video_type & STM_VIDEO_OUT_420) && !(hdmi->edid_info.hdmi_hf_dc_flags & STM_HDMI_HF_VSDB_DC_MASK)))
    {
      TRC(TRC_ID_HDMI,"Deepcolour not supported in requested colour format\n");
      colour_depth = STM_VIDEO_OUT_24BIT;
    }
    else
    {
      switch(hdmi->video_type & STM_VIDEO_OUT_DEPTH_MASK)
      {
        case STM_VIDEO_OUT_30BIT:
          if((hdmi->edid_info.hdmi_vsdb_flags & STM_HDMI_VSDB_DC_30BIT)||
            (hdmi->edid_info.hdmi_hf_dc_flags & STM_HDMI_HF_VSDB_DC_30BIT_420))
            colour_depth = STM_VIDEO_OUT_30BIT;
          else
            colour_depth = STM_VIDEO_OUT_24BIT;
          break;
        case STM_VIDEO_OUT_36BIT:
          if((hdmi->edid_info.hdmi_vsdb_flags & STM_HDMI_VSDB_DC_36BIT)||
            (hdmi->edid_info.hdmi_hf_dc_flags & STM_HDMI_HF_VSDB_DC_36BIT_420))
            colour_depth = STM_VIDEO_OUT_36BIT;
          else
            colour_depth = STM_VIDEO_OUT_24BIT;
          break;
        case STM_VIDEO_OUT_48BIT:
          if((hdmi->edid_info.hdmi_vsdb_flags & STM_HDMI_VSDB_DC_48BIT)||
            (hdmi->edid_info.hdmi_hf_dc_flags & STM_HDMI_HF_VSDB_DC_48BIT_420))
            colour_depth = STM_VIDEO_OUT_48BIT;
          else
            colour_depth = STM_VIDEO_OUT_24BIT;
          break;
        case STM_VIDEO_OUT_24BIT:
        default:
          colour_depth = STM_VIDEO_OUT_24BIT;
          break;
      }
    }
  }

  /* char_rate for STM_VIDEO_OUT_24BIT */
  if (hdmi->video_type & STM_VIDEO_OUT_420)
  {
    char_rate_div_10 = mode->mode_timing.pixel_clock_freq >> 1;
  }
  else
  {
    char_rate_div_10 = mode->mode_timing.pixel_clock_freq;
  }

  switch(colour_depth)
  {
    case STM_VIDEO_OUT_24BIT:
      TRC(TRC_ID_HDMI,"Setting 24bit colour\n");
      break;
    case STM_VIDEO_OUT_30BIT:
      char_rate_div_10 =  (5 * char_rate_div_10) >> 2; /* x 1.25 */
      TRC(TRC_ID_HDMI,"Setting 30bit colour\n");
      break;
    case STM_VIDEO_OUT_36BIT:
      char_rate_div_10 =  (3 * char_rate_div_10) >> 1; /* x 1.5 */
      TRC(TRC_ID_HDMI,"Setting 36bit colour\n");
      break;
    case STM_VIDEO_OUT_48BIT:
      char_rate_div_10 =  char_rate_div_10 << 1; /* x 2 */
      TRC(TRC_ID_HDMI,"Setting 48bit colour\n");
      break;
  }

  switch(colour_depth)
  {
    case STM_VIDEO_OUT_24BIT:
      TRC(TRC_ID_HDMI,"Setting 24bit colour\n");
      break;
    case STM_VIDEO_OUT_30BIT:
      TRC(TRC_ID_HDMI,"Setting 30bit colour\n");
      break;
    case STM_VIDEO_OUT_36BIT:
      TRC(TRC_ID_HDMI,"Setting 36bit colour\n");
      break;
    case STM_VIDEO_OUT_48BIT:
      TRC(TRC_ID_HDMI,"Setting 48bit colour\n");
      break;
  }

  if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_VIDEO_OUT_SELECT, (STM_VIDEO_OUT_HDMI | video_type | colour_depth)) < 0)
  {
    TRC(TRC_ID_ERROR, "error calling stm_display_output_set_control(OUTPUT_CTRL_VIDEO_OUT_SELECT)\n");
  }
  if(hdmi->edid_info.cea_vcdb_flags & STM_CEA_VCDB_QS_RGB_QUANT)
    quantization |= (uint32_t)STM_VCDB_QUANTIZATION_RGB;

  if(hdmi->edid_info.cea_vcdb_flags & STM_CEA_VCDB_QY_YCC_QUANT)
    quantization |= (uint32_t)STM_VCDB_QUANTIZATION_YCC;

  if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_VCDB_QUANTIZATION_SUPPORT, quantization) < 0)
  {
    TRC(TRC_ID_ERROR, "error calling stm_display_output_set_control(OUTPUT_CTRL_VCDB_QUANTIZATION_SUPPORT)\n");
  }

  if(hdmi->cea_mode_from_edid)
  {
    uint32_t tmp = 0;
    switch(hdmi->edid_info.tv_aspect)
    {
      case ASPECT_RATIO_4_3:
        tmp = STM_AVI_VIC_4_3;
        break;
      case ASPECT_RATIO_16_9:
        tmp = STM_AVI_VIC_16_9;
        break;
      case ASPECT_RATIO_64_27:
        tmp = STM_AVI_VIC_64_27;
        break;
      case ASPECT_RATIO_256_135:
        tmp = STM_AVI_VIC_256_135;
        break;
    default:
      break;
    }
    if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_AVI_VIC_SELECT, tmp) < 0)
    {
      TRC(TRC_ID_ERROR, "error calling stm_display_output_set_control(OUTPUT_CTRL_AVI_VIC_SELECT)\n");
    }
  }

  /* scrambling activation if                  */
  /*   - scrambling supported below 340 Mcsc   */
  /*   - Character Rate > 340 Mcsc             */

  if (hdmi->scdcs_info.scdcs_valid == true)
  {
    uint8_t  scdc_wr_byte = 0x00;

    /* update flags reset */
    scdc_wr_byte = STM_HDMI_SCDCS_UPDATE_0_BIT_STATUS_UPDATE | STM_HDMI_SCDCS_UPDATE_0_BIT_CED_UPDATE;

    if (stm_display_link_scdc_write ( hdmi->link,
                       STM_HDMI_SCDCS_OFFSET_UPDATE_0,
                       1,
                       &scdc_wr_byte ) < 0)
    {
      TRC(TRC_ID_ERROR, "SCDC Write Error \n");
      hdmi->scdcs_info.scdcs_valid = false;
    }
  }

  if (hdmi->scdcs_info.scdcs_valid == true)
  {
    uint8_t  tmds_config = 0x00;
    /* scrambling activation / deactivation on the sink side if current status not correct */

    /* The maximum time period between the write to the Scrambling_Enable bit and the  */
    /*    transmission of a scrambled video signal shall be 100 ms.                    */
    if (char_rate_div_10 > STM_HDMI_THRESHOLD_CHAR_RATE_SCRAMBLING_DIV_10)
    {
      /* scrambling & clock division */
      tmds_config = STM_HDMI_SCDCS_TMDS_CONFIG_BIT_SCRAMBLING_ENABLE | STM_HDMI_SCDCS_TMDS_CONFIG_BIT_CLOCK_RATIO;
    }
    else if (hdmi->edid_info.hdmi_hf_vsdb_flags & STM_HDMI_HF_VSDB_LTE_340MCSC_SCRAMBLE)
    {
      /* scrambling only */
      tmds_config = STM_HDMI_SCDCS_TMDS_CONFIG_BIT_SCRAMBLING_ENABLE ;
    }
    else
    {
      /* no scrambling & no clock division */
      tmds_config = 0x00 ;
    }

    if (tmds_config != hdmi->scdcs_info.curr_tmds_config)
    {
      hdmi->scdcs_info.tmds_config_to_be_send = true;
      hdmi->scdcs_info.curr_tmds_config = tmds_config;
    }

    if ((retVal = stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_HDMI_SCRAMBLING,
           (tmds_config & STM_HDMI_SCDCS_TMDS_CONFIG_BIT_SCRAMBLING_ENABLE) ? 1 : 0))<0)
      TRC(TRC_ID_ERROR, "stm_display_output_set_control(OUTPUT_CTRL_HDMI_SCRAMBLING) returns %d", retVal);

    /* polling once every Video Field (recommended), at least once every 250 ms (mandatory) */
    /*   -1 to get margin                                                                   */
    hdmi->scdc_update_flag_polling_period = (mode->mode_params.vertical_refresh_rate/4000) - 1;
    hdmi->scdc_update_flag_polling_value = hdmi->scdc_update_flag_polling_period;

    /* the Source should continue to poll the Scrambling_Status                             */
    /* bit up to a maximum period of 200 ms from the start of scrambled video transmission  */
    /* +1 : to garantee at leat 200 ms elapsed .. */
    hdmi->scdc_scrambling_status_polling_value = (tmds_config & STM_HDMI_SCDCS_TMDS_CONFIG_BIT_SCRAMBLING_ENABLE) ?
           ((mode->mode_params.vertical_refresh_rate/5000) + 1) : 0;
  }
  else
  {
    retVal = stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_HDMI_SCRAMBLING,0);
  }

  TRC(TRC_ID_HDMI,"Finished\n");
  return 0;
}

#define FORCE_CLOCK_FOR_4K_2K      0x01
#define FORCE_CLOCK_FOR_4K_4K      0x02
#define FORCE_CLOCK_FOR_4K_SDED    0x04
#define FORCE_CLOCK_FOR_4K30_4K30  0x08

static int stmhdmi_determine_force_clock_required(struct stm_hdmi    *hdmi,
                                stm_display_mode_t *mode)
{
  uint32_t video_type;
  bool vic1_source=false;
  bool vic2_source=false;
  bool vic1_destination=false;
  bool vic2_destination=false;
  bool viced_destination=false;
  bool vic4k30_destination=false;
  bool vic4k30_source=false;
  int return_val = 0;

  int i, j;
  stm_display_mode_t current_hdmi_mode;

  TRC(TRC_ID_HDMI,"\n");

  /*
   * Find out if the HDMI output is still running, we may be handling a deferred
   * disconnection hotplug event.
   */
  if(stm_display_output_get_current_display_mode(hdmi->hdmi_output, &current_hdmi_mode)<0)
  {
    current_hdmi_mode.mode_id = STM_TIMING_MODE_RESERVED;
  }
  TRC(TRC_ID_HDMI,"Current mode ID is %d\n",current_hdmi_mode.mode_id);

  if (stm_display_output_get_control(hdmi->hdmi_output, OUTPUT_CTRL_VIDEO_OUT_SELECT, &video_type)<0)
  {
    TRC(TRC_ID_ERROR,"OUTPUT_CTRL_VIDEO_OUT_SELECT is failing\n");
  }
  if ((video_type & STM_VIDEO_OUT_24BIT)==STM_VIDEO_OUT_24BIT)
  {
    for ( i=0; i< STM_AR_INDEX_COUNT ;i++)
    {
      for (j=0; j<4; j++)
      {
        if (current_hdmi_mode.mode_params.hdmi_vic_codes[i]==vic1_24bit[j])
          vic1_source=true;
      }
      for ( j=0; j<6; j++)
      {
        if (current_hdmi_mode.mode_params.hdmi_vic_codes[i]==vic2_24bit[j])
          vic2_source=true;
      }
      for ( j=0; j<9; j++)
      {
        if (current_hdmi_mode.mode_params.hdmi_vic_codes[i]==vic4kp30_24bit[j])
        {
          vic4k30_source=true;
        }
      }
    }
    for ( i=0; i< STM_AR_INDEX_COUNT ;i++)
    {
      for (j=0; j<4; j++)
      {
        if (mode->mode_params.hdmi_vic_codes[i]==vic1_24bit[j])
          vic1_destination=true;
      }
      for ( j=0; j<6; j++)
      {
        if (mode->mode_params.hdmi_vic_codes[i]==vic2_24bit[j])
          vic2_destination=true;
      }
      for ( j=0; j<6; j++)
      {
        if (mode->mode_params.hdmi_vic_codes[i]==vicsded_24bit[j])
          viced_destination=true;
      }
      for ( j=0; j<9; j++)
      {
        if (current_hdmi_mode.mode_params.hdmi_vic_codes[i]==vic4kp30_24bit[j])
        {
          vic4k30_destination=true;
        }
      }
    }

    if (( vic1_source & vic2_destination) || ( vic2_source & vic1_destination))
    {
      return_val = FORCE_CLOCK_FOR_4K_2K;
    }
    if (vic2_source & viced_destination)
    {
      return_val = FORCE_CLOCK_FOR_4K_SDED;
    }
    if (vic2_source & vic2_destination)
    {
      return_val = FORCE_CLOCK_FOR_4K_4K;
    }
    if (vic4k30_source & vic4k30_destination)
    {
      return_val = FORCE_CLOCK_FOR_4K30_4K30;
    }
  }
  else if (((video_type & STM_VIDEO_OUT_30BIT)==STM_VIDEO_OUT_30BIT) || ((video_type & STM_VIDEO_OUT_36BIT)==STM_VIDEO_OUT_36BIT))
  {
    for ( i=0; i< STM_AR_INDEX_COUNT ;i++)
    {
      for (j=0; j<12; j++)
      {
        if (current_hdmi_mode.mode_params.hdmi_vic_codes[i]==vic1_30_36bit[j])
          vic1_source=true;
      }
      for ( j=0; j<9; j++)
      {
        if (current_hdmi_mode.mode_params.hdmi_vic_codes[i]==vic2_30_36bit[j])
          vic2_source=true;
      }
    }
    for ( i=0; i< STM_AR_INDEX_COUNT ;i++)
    {
      for (j=0; j<12; j++)
      {
        if (mode->mode_params.hdmi_vic_codes[i]==vic1_30_36bit[j])
          vic1_destination=true;
      }
      for ( j=0; j<9; j++)
      {
        if (mode->mode_params.hdmi_vic_codes[i]==vic2_30_36bit[j])
          vic2_destination=true;
      }
    }
    if (( vic1_source & vic2_destination) || ( vic2_source & vic1_destination))
    {
      return_val = FORCE_CLOCK_FOR_4K_2K;
    }
  }
  return (return_val);
}

static int send_event(struct stm_hdmi * hdmi, unsigned int type)
{
  stm_event_t  event;
  event.event_id = type;
  event.object = (void *)hdmi->hdmi_output;
  return stm_event_signal(&event);
}

static int stmhdmi_determine_pixel_repetition(struct stm_hdmi    *hdmi,
                                              stm_display_mode_t *mode)

{
  int repeat = hdmi->max_pixel_repeat;
  uint32_t saved_flags = mode->mode_params.flags;
  stm_display_mode_t m;

  TRC(TRC_ID_HDMI,"\n");

  if((mode->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK)||
     (mode->mode_params.output_standards & STM_OUTPUT_STD_ED_MASK))
  {
    if(!hdmi->non_strict_edid_semantics)
    {
      if((mode->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK) && (hdmi->max_pixel_repeat == 1))
      {
        //No pixel repetition 1 with SD modes, so we force it to 2
        repeat = 2;
      }
      while(repeat>1)
      {
        TRC(TRC_ID_HDMI, "Trying pixel repeat = %d\n",repeat);
        if(!stm_display_output_find_display_mode(hdmi->main_output,
                                                 mode->mode_params.active_area_width*repeat,
                                                 mode->mode_params.active_area_height,
                                                 mode->mode_timing.lines_per_frame,
                                                 mode->mode_timing.pixels_per_line*repeat,
                                                 mode->mode_timing.pixel_clock_freq*repeat,
                                                 mode->mode_params.scan_type,
                                                 &m))
        {
          int n;
          TRC(TRC_ID_HDMI,"Found SoC timing mode for pixel repeated mode %d\n",m.mode_id);
          for(n=0;n<hdmi->edid_info.num_modes;n++)
          {
            const stm_display_mode_t *tmp = &hdmi->edid_info.display_modes[n];

            if(tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_4_3] == m.mode_params.hdmi_vic_codes[STM_AR_INDEX_4_3] &&
               tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_16_9] == m.mode_params.hdmi_vic_codes[STM_AR_INDEX_16_9] &&
               tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_64_27] == m.mode_params.hdmi_vic_codes[STM_AR_INDEX_64_27] &&
               tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_256_135] == m.mode_params.hdmi_vic_codes[STM_AR_INDEX_256_135])
            {
              TRC(TRC_ID_HDMI,"Matched pixel repeated mode %d with display EDID supported modes\n",tmp->mode_id);
              *mode = m;
              break;
            }
          }

          if(n<hdmi->edid_info.num_modes)
          break;
        }

        repeat /= 2;
      }
      switch(repeat)
      {
        case 4:
          mode->mode_params.flags = (saved_flags | STM_MODE_FLAGS_HDMI_PIXELREP_4X);
          break;
        case 2:
          mode->mode_params.flags = (saved_flags | STM_MODE_FLAGS_HDMI_PIXELREP_2X);
          break;
        case 1:
          mode->mode_params.flags = saved_flags;
          break;
        default:
          TRC(TRC_ID_HDMI,"No pixel repeated mode supported with display EDID\n");
          return -EINVAL;
         break;
      }
    }
    else
    {
      repeat = hdmi->max_pixel_repeat;
      if((mode->mode_params.output_standards & STM_OUTPUT_STD_SD_MASK) && (hdmi->max_pixel_repeat == 1))
      {
        //No pixel repetition 1 with SD modes, so we force it to 2
        repeat = 2;
      }
      TRC(TRC_ID_HDMI,"Trying to find %dx pixel repeat for non-strict EDID semantics\n", repeat);
      if(!stm_display_output_find_display_mode(hdmi->main_output,
                                               mode->mode_params.active_area_width*repeat,
                                               mode->mode_params.active_area_height,
                                               mode->mode_timing.lines_per_frame,
                                               mode->mode_timing.pixels_per_line*repeat,
                                               mode->mode_timing.pixel_clock_freq*repeat,
                                               mode->mode_params.scan_type,
                                               &m))
      {
        TRC(TRC_ID_HDMI,"Found SoC timing mode for %dx pixel repeated mode %d\n", repeat, m.mode_id);
        *mode = m;
        switch(repeat)
        {
          case 4:
            mode->mode_params.flags = (saved_flags | STM_MODE_FLAGS_HDMI_PIXELREP_4X);
            break;
          case 2:
            mode->mode_params.flags = (saved_flags | STM_MODE_FLAGS_HDMI_PIXELREP_2X);
            break;
          default:
            mode->mode_params.flags = saved_flags;
            break;
        }
      }
      else
      {
        TRC(TRC_ID_HDMI,"No pixel repeated mode supported for non-strict EDID semantics\n");
        return -EINVAL;
      }
    }
  }
  else
  {
    TRC(TRC_ID_HDMI,"Using no pixel repetition for HD mode\n");
  }
  return 0;
}

static int stmhdmi_start_hdcp(struct stm_hdmi *hdmi)
{
  int retVal=0;
  stm_display_link_hdcp_protocol_t hdcp_mode;
  stm_display_hdcp_signal_t hdcp_state;
  static bool hdcp_protocol_changed=false;
  static uint8_t  hdcpversion;
  retVal = stm_display_link_get_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_MODE, &hdcp_mode);
  if (hdcp_mode == STM_DISPLAY_LINK_HDCP_PROTOCOL_AUTO)
  {
    if ((retVal = stm_display_link_hdcp_get_sink_hdcp_version(hdmi->link, &hdcpversion, 0)) <0)
    {
       TRC(TRC_ID_ERROR, " Unable to retrieve HDCP version\n");
    }
  }
  else if (hdcp_mode == STM_DISPLAY_LINK_HDCP_PROTOCOL_1_X)
  {
    hdcpversion = 0;
  }
  else if (hdcp_mode == STM_DISPLAY_LINK_HDCP_PROTOCOL_2_2)
  {
    hdcpversion = 4;
  }

  if (hdcpversion != hdmi->Current_hdcpversion)
  {
    TRC(TRC_ID_HDMI, " HDCP protocol has been changed, new protocol is %s",(HDCP_VERSION(hdcpversion) == STM_DISPLAY_LINK_HDCP_PROTOCOL_2_2)?"HDCP2.2":"HDCP1.4");

    if((retVal = stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_PROTOCOL, HDCP_VERSION(hdcpversion)))<0)
      TRC(TRC_ID_ERROR, " Error in HDCP Set CTRL PROTOCOL\n");

    hdmi->Current_hdcpversion=hdcpversion ;
    hdcp_protocol_changed=true;
  }
  else
  {
    hdcp_protocol_changed=false;
  }
  /*
   * Stop HDCP 2.2 as it has been already enabled after doing unplug/plug.
   * Now, we plug HDCP 1.4 sink whereas, HDCP 2.2 instance is idle, we force to stop it.
   * This is in context of sink HDCP 2.2 -> sink HDCP 1.4 switching.
   */
  if (((HDCP_VERSION(hdcpversion)) == STM_DISPLAY_LINK_HDCP_PROTOCOL_1_X) && (hdcp_protocol_changed))
  {
    if ((retVal = stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_STOP, STM_DISPLAY_LINK_HDCP_PROTOCOL_2_2)) <0)
      TRC(TRC_ID_ERROR, " Error in HDCP 2.2 STOP\n");
  }

  if ((hdmi->hdcp_enable) && (!hdmi->current_hdcp_status))
  {
    if ((retVal = stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_FORCE_COLOR, 1))<0)
      TRC(TRC_ID_ERROR, "stm_display_output_set_control(OUTPUT_CTRL_FORCE_COLOR,1) line %d returns %d", __LINE__, retVal);

    if (stm_display_link_get_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_ENABLE, &hdcp_state) <0)
    {
      TRC(TRC_ID_HDMI, " error calling STM_DISPLAY_LINK_CTRL_HDCP_ENABLE control\n");
    }

    if ((hdcp_protocol_changed)|| (hdmi->hdcp_power_changed)||(hdcp_state==HDCP_STOPPED))
    {
     /*
      * The HDCP protocol has to be changed as the HDCP sink capabilities is changed
      * We need to start HDCP with the HDCP corresponding protocol
      */
      TRC(TRC_ID_HDMI, " HDCP protocol has been changed HDCP1.4 <->HDCP 2.2, NEW HDCP authentication has to be started");

      if ((retVal = stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_ENABLE,
          (hdmi->edid_info.display_type == STM_DISPLAY_HDMI)?1:2))<0)
        TRC(TRC_ID_ERROR, " Error in HDCP enabling");
        hdmi->hdcp_power_changed=false;
    }
    else
    {
     /*
      * HDCP protocol is kept, So HDCP signal is sent to trigger HDCP starting
      */
      TRC(TRC_ID_HDMI, " HDCP protocol has been kept, Trigger HDCP encryption");
      if ((retVal = stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_SIGNAL,
         (hdmi->edid_info.display_type == STM_DISPLAY_HDMI)?STM_DISPLAY_HDMI_HDCP_START_SIG:STM_DISPLAY_DVI_HDCP_START_SIG))<0)
        TRC(TRC_ID_ERROR, " Error in HDCP enabling");
    }
    if (retVal >= 0)
      hdmi->current_hdcp_status =true;
  }
  else
  {
    TRC(TRC_ID_HDMI," HDCP is not started by user application\n");
  }
  return retVal;
}

static int stmhdmi_stop_hdcp(struct stm_hdmi *hdmi, stm_display_hdcp_signal_t hdcp_state)
{
  int retVal=0;

  if ((hdmi->hdcp_enable) && (hdmi->current_hdcp_status))
  {
     TRC(TRC_ID_HDMI, " HDCP STOP required");
     if (hdcp_state==STM_DISPLAY_HDCP_STOP_SIG)
     {
        if ((retVal = stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_SIGNAL, STM_DISPLAY_HDCP_STOP_SIG)) <0)
          TRC(TRC_ID_ERROR, " Error in HDCP disabling\n");
     }
     else
     {
        if ((retVal = stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_SIGNAL, STM_DISPLAY_HDCP_SUSPEND_SIG)) <0)
          TRC(TRC_ID_ERROR, " Error in HDCP disabling\n");
        hdmi->hdcp_power_changed=true;
     }
     if (retVal >= 0)
       hdmi->current_hdcp_status =false;
  }
  else
  {
    TRC(TRC_ID_HDMI," HDCP is not already started or encrytion is not already enabled on desired output\n");
  }
  return retVal;
}
static int stmhdmi_stop_output(struct stm_hdmi    *hdmi, stm_display_hdcp_signal_t hdcp_state)
{
  int retVal=0;
  if ((retVal=stmhdmi_stop_hdcp(hdmi, hdcp_state)) < 0)
  {
    TRC(TRC_ID_ERROR," Unable to stop HDCP on HDMI Tx\n");
    return retVal;
  }
  if(!hdmi->stop_output)
  {
    if ((retVal = stm_display_output_stop(hdmi->hdmi_output)) <0)
    {
      TRC(TRC_ID_ERROR," Unable to stop HDMI Tx interface\n");
      return retVal;
    }
    hdmi->stop_output = true;
    hdmi_sysfs_notify(&(hdmi->class_device->kobj), NULL, "tmds_status");
  }
  hdmi_release_device(hdmi);

  return 0;
}


static int stmhdmi_start_output(struct stm_hdmi    *hdmi,
                                stm_display_mode_t *mode)
{
  int retVal=0;
  int force_clock_required=0;
  stm_display_mode_t current_hdmi_mode;

  TRC(TRC_ID_HDMI,"\n");

  /*
   * Find out if the HDMI output is still running, we may be handling a deferred
   * disconnection hotplug event.
   */
  if(stm_display_output_get_current_display_mode(hdmi->hdmi_output, &current_hdmi_mode)<0)
  {
    current_hdmi_mode.mode_id = STM_TIMING_MODE_RESERVED;
  }
  TRC(TRC_ID_HDMI,"Current mode ID is %d\n",current_hdmi_mode.mode_id);

  /*
   * If we got a request to restart the output, without a disconnect first,
   * then really do a restart if the mode is different by stopping the
   * output first. If the mode is exactly the same just call start which
   * will reset the output's connection state to connected.
   */
  if((!hdmi->stop_output)&& ((current_hdmi_mode.mode_id != mode->mode_id) || (current_hdmi_mode.mode_params.output_standards != mode->mode_params.output_standards)))
  {
    force_clock_required = stmhdmi_determine_force_clock_required(hdmi, mode);
    if (force_clock_required & (FORCE_CLOCK_FOR_4K_2K|FORCE_CLOCK_FOR_4K_4K|FORCE_CLOCK_FOR_4K_SDED))
    {
      /*
       * This magic intermediate phy and pll configuration is required to overcome issues with 4k<->2k switching
       */
      retVal = stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_HDMI_FORCE_CLOCK, 1);
    }
    else
    {
      retVal = stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_HDMI_FORCE_CLOCK, 0);
    }
    if (stm_display_output_stop(hdmi->hdmi_output) < 0)
      TRC(TRC_ID_ERROR, "can not stop HDMI output properly");
    hdmi->stop_output = true;
    if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_DISPLAY_MODE, STM_DISPLAY_INVALID)<0)
    {
      retVal = -EINVAL;
    }
    if (force_clock_required & (FORCE_CLOCK_FOR_4K_SDED))
    {
      vibe_os_stall_execution(HDMI_WAIT_2_MS_BWN_STOP_HDMI_AND_START_HDMI_FOR_4K60_SDED_TRANSITION);
    }

    if (force_clock_required & (FORCE_CLOCK_FOR_4K30_4K30))
    {
      vibe_os_stall_execution(HDMI_WAIT_1_MS_BWN_STOP_HDMI_AND_START_HDMI_FOR_4K30_4K30_TRANSITION);
    }
  }

  TRC(TRC_ID_HDMI,"Starting Video Mode %ux%u%s\n",mode->mode_params.active_area_width,
                                          mode->mode_params.active_area_height,
                                          (mode->mode_params.scan_type == STM_INTERLACED_SCAN)?"i":"p");

  if (hdmi->scdcs_info.tmds_config_to_be_send == true)
  {
    if (stm_display_link_scdc_write ( hdmi->link, STM_HDMI_SCDCS_OFFSET_TMDS_CONFIG, 1,&hdmi->scdcs_info.curr_tmds_config ) < 0)
    {
        TRC(TRC_ID_ERROR, "SCDC Write Error \n");
        hdmi->scdcs_info.scdcs_valid = false;
        hdmi->scdcs_info.curr_tmds_config = 0x00; /* no scrambling */
    }
    TRC(TRC_ID_HDMI,"WR SCDCS tmds_config 0:0x%x \n", hdmi->scdcs_info.curr_tmds_config);
    hdmi->scdcs_info.tmds_config_to_be_send = false;
    vibe_os_stall_execution(HDMI_WAIT_1_MS_BWN_BIT_CLK_RATIO_WRITE_AND_TMDS_CLK_RESUME);
  }

  if(stm_display_output_start(hdmi->hdmi_output, mode)<0)
  {
    retVal = -EINVAL;
    goto exit;
  }

  if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_DISPLAY_MODE,
     (hdmi->edid_info.display_type == STM_DISPLAY_HDMI)?DISPLAY_MODE_HDMI:DISPLAY_MODE_DVI)<0)
  {
    retVal = -EINVAL;
    goto exit;
  }

  hdmi->stop_output = false;

exit:
  return retVal;
}


static int stmhdmi_enable_mode_by_timing_limits(struct stm_hdmi    *hdmi,
                                                stm_display_mode_t *mode)
{
  uint32_t hfreq,vfreq;

  TRC(TRC_ID_HDMI,"Falling back refresh range limits for DVI monitor\n");

  if(mode->mode_timing.pixel_clock_freq > (hdmi->edid_info.max_clock*1000000))
  {
    TRC(TRC_ID_HDMI,"Pixel clock (%uHz) out of range\n",mode->mode_timing.pixel_clock_freq);
    return -EINVAL;
  }

  /*
   * Check H & V Refresh instead
   */
  hfreq = mode->mode_timing.pixel_clock_freq / mode->mode_timing.pixels_per_line;
  vfreq = mode->mode_params.vertical_refresh_rate / 1000;

  if((vfreq < hdmi->edid_info.min_vfreq) || (vfreq > hdmi->edid_info.max_vfreq))
  {
    TRC(TRC_ID_HDMI,"Vertical refresh (%uHz) out of range\n",vfreq);
    return -EINVAL;
  }

  if((hfreq < (hdmi->edid_info.min_hfreq*1000)) || (hfreq > (hdmi->edid_info.max_hfreq*1000)))
  {
    TRC(TRC_ID_HDMI,"Horizontal refresh (%uKHz) out of range\n",hfreq/1000);
    return -EINVAL;
  }

  TRC(TRC_ID_HDMI,"Starting DVI Video Mode %ux%u hfreq = %uKHz vfreq = %uHz\n",mode->mode_params.active_area_width,
                                                                       mode->mode_params.active_area_height,
                                                                       hfreq/1000,
                                                                       vfreq);

  return stmhdmi_start_output(hdmi, mode);
}


static int stmhdmi_enable_link(struct stm_hdmi *hdmi)
{
  int n, edid_4_3_3D_flag, edid_16_9_3D_flag, edid_64_27_3D_flag;
  int STM_MODE_FLAGS_3D;
  stm_display_mode_t mode;
  STM_MODE_FLAGS_3D =   STM_MODE_FLAGS_3D_SBS_HALF | STM_MODE_FLAGS_3D_TOP_BOTTOM | STM_MODE_FLAGS_3D_FRAME_PACKED;

  if(hdmi->edid_info.display_type == STM_DISPLAY_INVALID)
  {
    TRC(TRC_ID_HDMI,"Invalid EDID: Not enabling link\n");
    return -ENODEV;
  }

  /*
   * Get the master output's mode, this is what we want to set
   */
  if(stm_display_output_get_current_display_mode(hdmi->main_output, &mode)<0)
    return 0;

  /*
   * Clear force color when application hadn't been assert it
   * However, it was asserted by HDMI manager after getting some failure on HDMI Tx side
   */
  if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_FORCED_RGB_VALUE, 0x0000FF)<0)
     return -EINVAL;
  if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_FORCE_COLOR, 0)<0)
     return -EINVAL;

  BUG_ON(mode.mode_id == STM_TIMING_MODE_RESERVED);

  if(mode.mode_id < STM_TIMING_MODE_CUSTOM)
  {
    /*
     * If the master is in a known standard mode, replace the output standard
     * mask with the original template version so we can see if it is a CEA861
     * mode or not.
     */
    stm_display_mode_t tmp;
    if(stm_display_output_get_display_mode(hdmi->main_output, mode.mode_id, &tmp)<0)
      return -EINVAL;

    mode.mode_params.output_standards = tmp.mode_params.output_standards;
  }
  /*
   * Make sure pixel repetition mode flags are cleared
   */
  mode.mode_params.flags &= ~(STM_MODE_FLAGS_HDMI_PIXELREP_2X | STM_MODE_FLAGS_HDMI_PIXELREP_4X);

  if(hdmi->edid_info.display_type == STM_DISPLAY_DVI)
  {
    /*
     * We believe we now have a computer monitor or a TV with a DVI input not
     * a HDMI input. So set the output format to DVI and force RGB.
     */

    TRC(TRC_ID_HDMI,"Setting DVI/RGB output\n");

    if (stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_VIDEO_OUT_SELECT, (STM_VIDEO_OUT_DVI | STM_VIDEO_OUT_RGB | STM_VIDEO_OUT_24BIT)) != 0)
      TRC(TRC_ID_ERROR, "error calling stm_display_output_set_control(OUTPUT_CTRL_VIDEO_OUT_SELECT)\n");
    /*
     * DVI devices do not support deepcolor.
     */
    if (stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_SINK_SUPPORTS_DEEPCOLOR, 0) != 0)
      TRC(TRC_ID_ERROR, "error calling stm_display_output_set_control(OUTPUT_CTRL_SINK_SUPPORTS_DEEPCOLOR)\n");
  }
  else
  {
     if(stmhdmi_configure_hdmi_formatting(hdmi,&mode)!=0)
       return -EINVAL;
  }

  if(stmhdmi_determine_pixel_repetition(hdmi,&mode)!=0)
    return -EINVAL;

  /*
   * Allow pure VESA and NTG5 standards as well as CEA861, anything else is
   * not supported. For modes like VGA, which are defined for both CEA and VESA
   * then we prefer CEA so we get complete AVI infoframe behaviour.
   *
   * Note: do this _after_ looking for pixel repeated modes as that requires
   * and may modify the tvStandard.
   */
  if((mode.mode_params.output_standards & STM_OUTPUT_STD_CEA861) == 0)
  {
    TRC(TRC_ID_HDMI,"Non CEA861 mode\n");
    if(mode.mode_params.output_standards & STM_OUTPUT_STD_VESA_MASK)
      mode.mode_params.output_standards = (mode.mode_params.output_standards & STM_OUTPUT_STD_VESA_MASK);
    else if(mode.mode_params.output_standards & STM_OUTPUT_STD_NTG5)
      mode.mode_params.output_standards = STM_OUTPUT_STD_NTG5;
    else if(mode.mode_params.output_standards & STM_OUTPUT_STD_HDMI_LLC_EXT)
      mode.mode_params.output_standards = STM_OUTPUT_STD_HDMI_LLC_EXT;
    else
      return -EINVAL;
  }
  else
  {
    /*
     * Clear any standard flags other than for HDMI
     */
    mode.mode_params.output_standards = STM_OUTPUT_STD_CEA861;
    TRC(TRC_ID_HDMI,"Configuring CEA861 mode\n");
  }

  /*
   * If we have been asked not to check against the EDID, just start
   * the mode.
   */
  if(hdmi->non_strict_edid_semantics)
  {
    TRC(TRC_ID_HDMI,"Non strict EDID semantics selected, starting mode\n");
    return stmhdmi_start_output(hdmi, &mode);
  }

  for(n=0;n<hdmi->edid_info.num_modes;n++)
  {
    const stm_display_mode_t *tmp = &hdmi->edid_info.display_modes[n];

    /*
     * We match up the analogue display mode with the EDID mode by using the
     * HDMI video codes.
     */

    if((tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_4_3] == mode.mode_params.hdmi_vic_codes[STM_AR_INDEX_4_3]) &&
       (tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_16_9] == mode.mode_params.hdmi_vic_codes[STM_AR_INDEX_16_9])&&
       (tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_64_27] == mode.mode_params.hdmi_vic_codes[STM_AR_INDEX_64_27])&&
       (tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_256_135] == mode.mode_params.hdmi_vic_codes[STM_AR_INDEX_256_135]))
    {
      edid_64_27_3D_flag = hdmi->edid_info.cea_codes[tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_64_27]].supported_3d_flags;
      edid_16_9_3D_flag = hdmi->edid_info.cea_codes[tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_16_9]].supported_3d_flags;
      edid_4_3_3D_flag = hdmi->edid_info.cea_codes[tmp->mode_params.hdmi_vic_codes[STM_AR_INDEX_4_3]].supported_3d_flags;
      if( (mode.mode_params.flags & STM_MODE_FLAGS_3D) != 0)
      {
        if ((mode.mode_params.hdmi_vic_codes[STM_AR_INDEX_64_27] &&
        (((mode.mode_params.flags & edid_64_27_3D_flag & STM_MODE_FLAGS_3D_SBS_HALF) == STM_MODE_FLAGS_3D_SBS_HALF) ||
        ((mode.mode_params.flags & edid_64_27_3D_flag & STM_MODE_FLAGS_3D_TOP_BOTTOM) == STM_MODE_FLAGS_3D_TOP_BOTTOM) ||
        ((mode.mode_params.flags & edid_64_27_3D_flag & STM_MODE_FLAGS_3D_FRAME_PACKED) == STM_MODE_FLAGS_3D_FRAME_PACKED))) ||
        (mode.mode_params.hdmi_vic_codes[STM_AR_INDEX_16_9] &&
        (((mode.mode_params.flags & edid_16_9_3D_flag & STM_MODE_FLAGS_3D_SBS_HALF) == STM_MODE_FLAGS_3D_SBS_HALF) ||
        ((mode.mode_params.flags & edid_16_9_3D_flag & STM_MODE_FLAGS_3D_TOP_BOTTOM) == STM_MODE_FLAGS_3D_TOP_BOTTOM) ||
        ((mode.mode_params.flags & edid_16_9_3D_flag & STM_MODE_FLAGS_3D_FRAME_PACKED) == STM_MODE_FLAGS_3D_FRAME_PACKED))) ||
        (mode.mode_params.hdmi_vic_codes[STM_AR_INDEX_4_3] &&
        (((mode.mode_params.flags & edid_4_3_3D_flag & STM_MODE_FLAGS_3D_SBS_HALF) == STM_MODE_FLAGS_3D_SBS_HALF) ||
        ((mode.mode_params.flags & edid_4_3_3D_flag & STM_MODE_FLAGS_3D_TOP_BOTTOM) == STM_MODE_FLAGS_3D_TOP_BOTTOM) ||
        ((mode.mode_params.flags & edid_4_3_3D_flag & STM_MODE_FLAGS_3D_FRAME_PACKED) == STM_MODE_FLAGS_3D_FRAME_PACKED))))
        {
          return stmhdmi_start_output(hdmi, &mode);
        }
      }
      else
      {
        return stmhdmi_start_output(hdmi, &mode);
      }
    }
  }

  TRC(TRC_ID_HDMI,"Current video mode not reported as supported by display\n");

  /*
   * We are still trying to only set modes indicated as valid in the EDID. So
   * for DVI devices allow modes that are inside the timing limits.
   */
  if(hdmi->edid_info.display_type == STM_DISPLAY_DVI)
    return stmhdmi_enable_mode_by_timing_limits(hdmi, &mode);

  return -EINVAL;
}


/*******************************************************************************
 * Kernel management thread state machine helper functions.
 */

static void stmhdmi_start(struct stm_hdmi *hdmi)
{
  hdmi_open_device(hdmi);
  if(!hdmi->disable)
  {
    TRC(TRC_ID_HDMI,"Starting HDMI Output hdmi = %p\n",hdmi);

    if(stmhdmi_enable_link(hdmi) != 0)
    {
      TRC(TRC_ID_HDMI, "Disabling HDMI HW\n");
      /*
       * If we had an error starting the output, do a stop just in case it was
       * already running. Be careful, HDCP has to be stopped first when
       * it was already enabled. This might happen for instance if we got a restart
       * due to a change in the EDID mode semantics and the current mode is
       * no longer allowed on the display.
       */
      if (stmhdmi_stop_hdcp(hdmi, STM_DISPLAY_HDCP_STOP_SIG) != 0)
        TRC(TRC_ID_ERROR, "HDMI manager can not stop HDCP properly\n");

      if (stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_VIDEO_OUT_SELECT, STM_VIDEO_OUT_NONE)!=0)
          TRC(TRC_ID_ERROR, "stmhdmi_start_display: error in stm_display_output_set_control()\n");

      if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_DISPLAY_MODE, STM_DISPLAY_INVALID)<0)
        TRC(TRC_ID_ERROR, "HDMI manager can not set Display mode properly\n");

      hdmi->stop_output = true;
      hdmi_sysfs_notify(&(hdmi->class_device->kobj), NULL, "tmds_status");
      hdmi_release_device(hdmi);
    }
    else
    {
      if ((hdmi->hdcp_enable) && (!hdmi->current_hdcp_status))
      {
        if (stmhdmi_start_hdcp(hdmi) !=0)
        {
          if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_FORCE_COLOR, 1)<0)
            TRC(TRC_ID_ERROR, "error in stm_display_output_set_control(OUTPUT_CTRL_FORCE_COLOR,1) line %d",
                __LINE__);
          TRC(TRC_ID_ERROR, "HDMI manager can not start HDCP properly");
        }
      }
    }
  }
  else
  {
    TRC(TRC_ID_HDMI,"HDMI Output disabled\n");

    if ( stmhdmi_stop_output(hdmi, STM_DISPLAY_HDCP_STOP_SIG) <0)
      TRC(TRC_ID_ERROR, "Unable to stop HDMI Tx properly\n");

    if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_DISPLAY_MODE, STM_DISPLAY_INVALID)<0)
      TRC(TRC_ID_ERROR, "HDMI manager can not set Display mode properly");
  }
}

static void stmhdmi_stop(struct stm_hdmi *hdmi, stm_display_hdcp_signal_t hdcp_state)
{
  if (!hdmi->stop_output)
  {
    TRC(TRC_ID_HDMI,"Stoping HDMI Output hdmi = %p\n",hdmi);

    if (stmhdmi_stop_output(hdmi, hdcp_state) <0)
      TRC(TRC_ID_ERROR, "Unable to stop HDMI Tx properly\n");

    if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_DISPLAY_MODE, STM_DISPLAY_INVALID)<0)
      TRC(TRC_ID_ERROR, "HDMI manager can not set Display mode properly\n");

    hdmi_sysfs_notify(&(hdmi->class_device->kobj), NULL, "tmds_status");
  }
}


static void stmhdmi_disconnect(struct stm_hdmi *hdmi)
{
  stmhdmi_invalidate_edid_info(hdmi);
  stmhdmi_invalidate_scdc_info(hdmi);
}


static void stmhdmi_connect(struct stm_hdmi *hdmi)
{
  TRC(TRC_ID_HDMI,"HDMI Output connected\n");
 /*
  * We always re-read the EDID, as we might have received a hotplug
  * event just because the user changed the TV setup and the EDID
  * information has now changed.
  */
  if (hdmi->edid_updated)
    return;

  stmhdmi_invalidate_scdc_info(hdmi);

  if(stmhdmi_read_edid(hdmi) != 0)
  {
    TRC(TRC_ID_HDMI,"EDID Read Error, setup safe EDID\n");
    stmhdmi_safe_edid(hdmi);
  }

  if (hdmi->edid_info.hdmi_hf_vsdb_flags & STM_HDMI_HF_VSDB_SCDC_PRESENT)
  {
    stmhdmi_init_scdcs(hdmi);
  }

  stmhdmi_dump_edid(hdmi);
  /*
   * Schedule the sending of the product identifier HDMI info frame.
   */
  if(hdmi->spd_frame->data[HDMI_SPD_INFOFRAME_SPI_OFFSET] != HDMI_SPD_INFOFRAME_SPI_DISABLE)
  {
    if(stm_display_output_queue_metadata(hdmi->hdmi_output, hdmi->spd_metadata) < 0)
    {
      TRC(TRC_ID_ERROR, "stmhdmi_connect: error in stm_display_output_queue_metadata()\n");
    }
  }
}


int stmhdmi_determine_link_connectivity(struct stm_hdmi *hdmi)
{
  int retVal  = 0;

  if ( (retVal = stm_display_link_hpd_get_state(hdmi->link, &hdmi->hpd_state)) != 0 )
  {
    TRC(TRC_ID_ERROR, "Error can't get HPD status\n");
    return retVal;
  }

  if ( (retVal = stm_display_link_rxsense_get_state(hdmi->link, &hdmi->rxsense_state)) != 0 )
  {
    TRC(TRC_ID_ERROR, "Error can't get RxSense status\n");
    return retVal;
  }

  return ((hdmi->hpd_state == STM_DISPLAY_LINK_HPD_STATE_HIGH) && (!hdmi->link_capability.rxsense || (hdmi->rxsense_state == STM_DISPLAY_LINK_RXSENSE_STATE_ACTIVE)));
}

int stmhdmi_manager(void *data)
{
  stm_event_info_t                       hdmitx_event[DISPLAY_LINK_EVT_NUMBER];
  uint32_t                               event_count;
  uint32_t                               number_of_entries=0;
  uint32_t                               hdcp_protocol;
  uint32_t                               hdcp_state;
  static uint8_t                         hdcpversion;
  stm_event_subscription_h               event_subscription;
  stm_event_subscription_entry_t         subscription_entry[2];
  stm_event_subscription_entry_t        *subscription_p;
  stm_display_link_hdcp_protocol_t hdcp_mode;
  int                                    n, retVal = 0;
  int                                    timeout_ms=500;
  struct stm_hdmi                       *hdmi = (struct stm_hdmi *)data;
  stm_display_output_connection_status_t hdmi_status;
  stm_object_h                           hMasterOutput;
  struct stmlink* linkp;
  linkp = (struct stmlink *)hdmi->link ;



  TRC(TRC_ID_HDMI,"Start HDMI Manager\n");

  /*
   * Check the initial states of HPD, HDCP and Rx sense from display link
   */
  if ( (retVal = stm_display_link_hpd_get_state(hdmi->link, &hdmi->hpd_state)) != 0 )
  {
    TRC(TRC_ID_ERROR, "Error can't get HPD status");
    return retVal;
  }
  if ( (retVal = stm_display_link_rxsense_get_state(hdmi->link, &hdmi->rxsense_state)) != 0 )
  {
    TRC(TRC_ID_ERROR, "Error can't get RxSense status\n");
    return retVal;
  }

  /*
   * Subscribe to HPD, Rx sense and HDCP events
   */
  subscription_p = &subscription_entry[number_of_entries];
  subscription_p->cookie = NULL;
  subscription_p->object = (stm_object_h)hdmi->link;
  subscription_p->event_mask = STM_DISPLAY_LINK_HPD_STATE_CHANGE_EVT|
                               STM_DISPLAY_LINK_RXSENSE_STATE_CHANGE_EVT|
                               STM_DISPLAY_LINK_HDCP_STATUS_CHANGE_EVT |
                               STM_DISPLAY_LINK_HDCP_STATE_EVT;
  number_of_entries++;

  subscription_p = &subscription_entry[number_of_entries];
  subscription_p->cookie = NULL;

  if((retVal = stm_registry_get_object_type((stm_object_h)hdmi->main_output, &subscription_p->object)!=0))
  {
    TRC(TRC_ID_ERROR, "Error can't get object type\n");
    return retVal;
  }

  hMasterOutput = subscription_p->object;
  subscription_p->event_mask = STM_OUTPUT_STOP_EVENT;

  if ((retVal = stm_event_subscription_create(subscription_entry, number_of_entries,
      &event_subscription))!=0)
  {
    TRC(TRC_ID_ERROR, "Error can't create event subscription\n");
    return retVal;
  }

  if ((retVal = stm_event_set_wait_queue(event_subscription, &hdmi->status_wait_queue, true))!=0)
  {
    TRC(TRC_ID_ERROR, "Error can't set wait queue event\n");
    return retVal;
  }

  set_freezable();

  TRC(TRC_ID_HDMI,"Starting HDMI Thread for info = %p\n",hdmi);

  while(1)
  {
    wait_event_freezable_timeout(hdmi->status_wait_queue,
                                ((stm_event_wait(event_subscription, 0 , 0, NULL, NULL)!= 0) || (hdmi->scdc_update_flag_polling_req) ||
                                 (hdmi->scdc_scrambling_status_polling_req) || (hdmi->status_changed != 0) || kthread_should_stop()),  msecs_to_jiffies(timeout_ms));

    mutex_lock(&(hdmi->lock));

    if(kthread_should_stop())
    {
      TRC(TRC_ID_MAIN_INFO, "HDMI Thread terminating for info = %p\n",hdmi);
      mutex_unlock(&(hdmi->lock));
      break;
    }

    retVal = stm_event_wait(event_subscription, 0 , DISPLAY_LINK_EVT_NUMBER, &event_count, hdmitx_event);

   /*
    * Handle a real HDMI, HPD and HDCP state change depending on the received event
    */
    for (n=0;n<event_count;n++)
    {
      if (hdmitx_event[n].event.object == hMasterOutput)
      {
        if((hdmitx_event[n].event.event_id == STM_OUTPUT_STOP_EVENT)&&(stmhdmi_determine_link_connectivity(hdmi)))
        {
          if (stmhdmi_stop_hdcp(hdmi, STM_DISPLAY_HDCP_STOP_SIG) != 0)
            TRC(TRC_ID_ERROR,"HDMI manager can not stop HDCP properly\n");
          if (stm_display_output_stop(hdmi->hdmi_output) < 0)
            TRC(TRC_ID_ERROR, "can not stop HDMI output properly");
          hdmi->stop_output = true;
          if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_DISPLAY_MODE, STM_DISPLAY_INVALID)<0)
          {
            retVal = -EINVAL;
          }
        }
      }
      else
      {
        switch(hdmitx_event[n].event.event_id)
        {
          case STM_DISPLAY_LINK_HPD_STATE_CHANGE_EVT:
          {
            if ( (retVal = stm_display_link_hpd_get_state(hdmi->link, &hdmi->hpd_state)) != 0 )
            {
              TRC(TRC_ID_ERROR, "Error can't get HPD status\n");
              break;
            }
            TRC(TRC_ID_HDMI, "Event %d is retrieved : HPD event = %s", n, HPD_STATE_TO_STR(hdmi->hpd_state));

            if ( hdmi->hpd_state == STM_DISPLAY_LINK_HPD_STATE_HIGH)
            {
              if ((retVal = stm_display_link_hdcp_get_sink_hdcp_version(hdmi->link, &hdcpversion, 0)) <0)
              {
                 TRC(TRC_ID_ERROR, " Unable to retrieve HDCP version\n");
              }
              stmhdmi_connect(hdmi);
              if ((retVal = send_event(hdmi, HDMI_EVENT_DISPLAY_CONNECTED & (~HDMI_EVENT_DISPLAY_PRIVATE_START)))!=0)
              {
                  TRC(TRC_ID_ERROR, "Error can't send event\n");
              }
              if ((!hdmi->link_capability.rxsense) ||( hdmi->rxsense_state == STM_DISPLAY_LINK_RXSENSE_STATE_ACTIVE))
                stmhdmi_start(hdmi);
            }
            else
            {
              stmhdmi_disconnect(hdmi);
              if ((retVal = send_event(hdmi, HDMI_EVENT_DISPLAY_DISCONNECTED & (~HDMI_EVENT_DISPLAY_PRIVATE_START)))!=0)
              {
                  TRC(TRC_ID_ERROR, "Error can't send event\n");
              }
              if (hdmi->hpd_state == STM_DISPLAY_LINK_HPD_STATE_LOW)
                stmhdmi_stop(hdmi, STM_DISPLAY_HDCP_STOP_SIG);
              else
                stmhdmi_stop(hdmi, STM_DISPLAY_HDCP_SUSPEND_SIG);
            }
            hdmi_sysfs_notify(&(hdmi->class_device->kobj), NULL, "hotplug");
            break;
        }

        case STM_DISPLAY_LINK_RXSENSE_STATE_CHANGE_EVT:
        {
          if ( (retVal = stm_display_link_rxsense_get_state(hdmi->link, &hdmi->rxsense_state)) != 0 )
          {
            TRC(TRC_ID_ERROR, "Error can't get RxSense status");
            break;
          }
          TRC(TRC_ID_HDMI, "Event %d is retrieved : Rx sense event = %s", n, RXSENSE_STATE_TO_STR(hdmi->rxsense_state));
          hdmi_sysfs_notify(&(hdmi->class_device->kobj), NULL, "rxsense_status");

          if (hdmi->rxsense_state == STM_DISPLAY_LINK_RXSENSE_STATE_ACTIVE)
          {
            if (hdmi->hpd_state == STM_DISPLAY_LINK_HPD_STATE_HIGH)
            {
              stmhdmi_start(hdmi);
            }
            if ((retVal = send_event(hdmi, HDMI_EVENT_RXSENSE_ACTIVE & (~HDMI_EVENT_DISPLAY_PRIVATE_START)))!=0)
            {
              TRC(TRC_ID_ERROR, "Error can't send event\n");
            }
          }
          else
          {
            stmhdmi_stop(hdmi, STM_DISPLAY_HDCP_STOP_SIG);
            if ((retVal = send_event(hdmi, HDMI_EVENT_RXSENSE_INACTIVE & (~HDMI_EVENT_DISPLAY_PRIVATE_START)))!=0)
            {
              TRC(TRC_ID_ERROR, "Error can't send event\n");
            }
          }
          break;
        }
        case STM_DISPLAY_LINK_HDCP_STATUS_CHANGE_EVT:
        {
          if ((retVal = stm_display_link_hdcp_get_status(hdmi->link, &hdmi->hdcp_status)) != 0)
          {
            TRC(TRC_ID_ERROR, "Can't get HDCP status\n");
          }
          TRC(TRC_ID_HDMI, "Event %d is retrieved :HDCP event =%d\n", n, hdmi->hdcp_status );
          hdmi_sysfs_notify(&(linkp->class_device->kobj), NULL, "hdcp_status");
          if (((hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_SUCCESS_ENCRYPTING) ||
              (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_SUCCESS_NOT_ENCRYPTING)))
          {
            if ((retVal = stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_FORCE_COLOR, 0)) < 0)
                TRC(TRC_ID_ERROR, "stm_display_output_set_control(OUTPUT_CTRL_FORCE_COLOR,0) line %d returns %d",
                    __LINE__, retVal);
          }
          else if(hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_REAUTH_REQ) {
              if(stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_SIGNAL,1)<0)
                {
                  TRC(TRC_ID_ERROR, " Error in Enabling HDCP\n");
                }
            }
          else if ((hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_KSV_REVOKED) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_NO_ACK) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_V_CHECK) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_R0_CHECK) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_FIFO_NOT_READY) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_WDT_TIMEOUT) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_LINK_INTEGRITY) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_ENHANCED_LINK_VERIFICATION)||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_MAX_DEVICE_COUNT_EXCEEDED) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_MAX_CASCADE_EXCEEDED)||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_BKSV_INVALID) ||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_CSM_READY)||
                    (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_FAIL_UNKNOWN))
          {
            if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_FORCE_COLOR, 1) < 0)
            {
              TRC(TRC_ID_ERROR, "line %d can't set output control OUTPUT_CTRL_FORCE_COLOR to 1", __LINE__);
            }
          }
          else if (hdmi->hdcp_status == STM_DISPLAY_LINK_HDCP_STATUS_DISABLED)
          {
            if(stm_display_output_set_control(hdmi->hdmi_output, OUTPUT_CTRL_FORCE_COLOR, 0) < 0)
            {
              TRC(TRC_ID_ERROR, "line %d can't set output control OUTPUT_CTRL_FORCE_COLOR to 0", __LINE__);
            }
          }
          break;
         }
         case STM_DISPLAY_LINK_HDCP_STATE_EVT:
         {
            /*
             * Switch off hdcp_enable and current_hdcp_status flags.
             * --> hdcp_enable is asserted when user requires hdcp (hdcp-control -e) and de-asserted when user no more requires hdcp (hdcp-control -d)
             * --> current_hdcp_status gives the current HDCP status (enabled or not) depending on the use-case. This flag can be true or false when hdcp_enable =true.
             *     it is kept false when hdcp_enable =false.
             * The flags switch off is applicable only when user do hdcp-control -d and not applicable
             * ---> In Hot plug context or Rx sense scenario which implies HPD=0 or Rxsense =0 which implies HDCP stop (HDCP stop not done by user)
             * ---> In HDMI interface configuration change (like hdmi-control -D/-E), hdcp will be stopped in this use case. So, hdmi->status_changed will be asserted (HDCP stop not done by user)
             * ---> In video format switch, HDCP will be stopped typically to change clock domain from some use-cases which implies stop output, hdmi->stop_output will be asserted (HDCP stop not done by user)
             */
            if (stm_display_link_get_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_ENABLE, &hdcp_state) <0)
            {
              TRC(TRC_ID_HDMI, " error calling STM_DISPLAY_LINK_CTRL_HDCP_ENABLE control\n");
            }
            TRC(TRC_ID_HDMI, "Event %d is retrieved : HDCP STATE event = %s\n", n, HDCP_STATE_TO_STR(hdcp_state));
            if (hdcp_state == HDCP_STOPPED)
            {
              hdmi->hdcp_enable = false;
              hdmi->current_hdcp_status = false;
            }
            else
            {
              hdmi->hdcp_enable = true;
              hdmi->current_hdcp_status = true;
              hdmi->hdcp_stopped = false;
              if (stm_display_link_hdcp_get_sink_hdcp_version(hdmi->link, &hdmi->Current_hdcpversion, 0) <0)
              {
                TRC(TRC_ID_HDMI, "Unable to get HDCP2 version\n");
              }
            }
            break;
         }
         default:
           break;
        }
      }
    }
    /*
     * HDMI interface configuration change requires a check under edid indication and start again based on HDMI connectivity
     */
    if (likely(hdmi->status_changed))
    {
      if(stm_display_output_get_connection_status(hdmi->hdmi_output, &hdmi_status) == 0)
      {
        if (stmhdmi_determine_link_connectivity(hdmi))
        {
          stmhdmi_connect(hdmi);
          stmhdmi_start(hdmi);
        }
      }
      hdmi->status_changed = 0;
    }

    /*
     *Setup HDCP sink version polling mechanism to give a chance to TV in order to recover from an error and relax.
     *  This use-case could happen when HDCP1.4 is already enabled and source is able to retrieve new HDCP sink version different to previous one
     * Source triggers HDCP 2.2 whenever the value retrieved is equal to 0x4 which implies HDCP2.2 support and no more 1.4.
     */
    retVal = stm_display_link_get_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_MODE, &hdcp_mode);
    if (hdcp_mode == STM_DISPLAY_LINK_HDCP_PROTOCOL_AUTO)
    {
      if ((hdmi->hdcp_enable) && (hdmi->current_hdcp_status) && (hdmi->hdcp_status!= STM_DISPLAY_LINK_HDCP_STATUS_IN_PROGRESS))
      {
        if((stm_display_link_get_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_PROTOCOL, &hdcp_protocol))<0)
          TRC(TRC_ID_ERROR, " Error in HDCP Set CTRL PROTOCOL\n");

        if (hdcp_protocol == STM_DISPLAY_LINK_HDCP_PROTOCOL_1_X)
        {
          if((stm_display_link_hdcp_get_sink_hdcp_version(hdmi->link, &hdcpversion, 0))>=0)
          {
            if ((HDCP_VERSION(hdcpversion)) == STM_DISPLAY_LINK_HDCP_PROTOCOL_2_2)
            {
              if ((stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_SIGNAL, STM_DISPLAY_HDCP_STOP_SIG))>=0)
              {
                if((stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_PROTOCOL, STM_DISPLAY_LINK_HDCP_PROTOCOL_2_2))>=0)
                {
                  if(stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_HDCP_ENABLE,(hdmi->edid_info.display_type == STM_DISPLAY_HDMI)?1:2)<0)
                  {
                     TRC(TRC_ID_ERROR, " Error in Enabling HDCP\n");
                  }
                }
                else
                {
                  TRC(TRC_ID_ERROR, " Error in Setting HDCP 2.2\n");
                }
              }
              else
              {
                TRC(TRC_ID_ERROR, " Error in stopping HDCP\n");
              }
            }
          }
          else
          {
            TRC(TRC_ID_ERROR, " Error in Getting HDCP sink version\n");
          }
        }
      }
    }

    if (hdmi->scdc_update_flag_polling_req)
    {
      uint8_t scdc_update_flag[2];
      uint8_t scdc_wr_byte = STM_HDMI_SCDCS_UPDATE_0_BIT_STATUS_UPDATE | STM_HDMI_SCDCS_UPDATE_0_BIT_CED_UPDATE;

      hdmi->scdc_update_flag_polling_req=false;

      stm_display_link_scdc_read ( hdmi->link,
                     STM_DISPLAY_LINK_SCDC_UPDATE_READ,
                     0,
                     2,
                     scdc_update_flag);

      if (scdc_update_flag[0] != 0)
      {
        /* reset of flags */
        if ((retVal = stm_display_link_scdc_write (hdmi->link,
                            STM_HDMI_SCDCS_OFFSET_UPDATE_0,
                            1,
                            &scdc_wr_byte )) < 0)
        {
          TRC(TRC_ID_ERROR, "SCDC Write Error \n");
          hdmi->scdcs_info.scdcs_valid = false;
        }
      }
      if (scdc_update_flag[0] & STM_HDMI_SCDCS_UPDATE_0_BIT_STATUS_UPDATE)
      {
        uint8_t status_flag_0;
        stm_display_link_scdc_read ( hdmi->link,
                       STM_DISPLAY_LINK_SCDC_COMBINED_FORMAT_READ,
                       STM_HDMI_SCDCS_OFFSET_STATUS_FLAGS_0,
                       1,
                       &status_flag_0);
        TRC(TRC_ID_HDMI, "HDMI sink - update of SCDCS status flag : Clock_Detected:%d Ch0_Locked:%d Ch1_Locked:%d Ch2_Locked:%d\n",
            (status_flag_0 & STM_HDMI_SCDCS_STATUS_FLAGS_0_BIT_CLOCK_DETECTED) ? 1 : 0,
            (status_flag_0 & STM_HDMI_SCDCS_STATUS_FLAGS_0_BIT_CH0_LOCKED) ? 1 : 0,
            (status_flag_0 & STM_HDMI_SCDCS_STATUS_FLAGS_0_BIT_CH1_LOCKED) ? 1 : 0,
            (status_flag_0 & STM_HDMI_SCDCS_STATUS_FLAGS_0_BIT_CH2_LOCKED) ? 1 : 0 );
      }

      if (scdc_update_flag[0] & STM_HDMI_SCDCS_UPDATE_0_BIT_CED_UPDATE)
      {
        stmhdmi_read_scdcs_character_error_detection_field(hdmi);
      }
    }


    if (hdmi->scdc_scrambling_status_polling_req)
    {
      uint8_t scrambler_status;

      hdmi->scdc_scrambling_status_polling_req = false;
      stm_display_link_scdc_read ( hdmi->link,
                     STM_DISPLAY_LINK_SCDC_COMBINED_FORMAT_READ,
                     STM_HDMI_SCDCS_OFFSET_SCRAMBLER_STATUS,
                     1,
                     &scrambler_status);
      TRC(TRC_ID_HDMI, "RD SCDCS : scrambling_status_polling scrambling status : 0x%x \n", scrambler_status);

      if (!(scrambler_status & STM_HDMI_SCDCS_SCRAMBLER_STATUS_BIT_SCRAMBLING_STATUS))
      {
        TRC(TRC_ID_ERROR, "HDMI sink - incorrect scrambling status : scrambled control code sequences not detected \n");
      }
    }

    mutex_unlock(&(hdmi->lock));
    continue;
  }

  if ((retVal = stm_event_subscription_delete(event_subscription))!=0)
  {
    TRC(TRC_ID_ERROR, "Error can't delete event subscription\n");
    return retVal;
  }

  if (stm_display_link_set_ctrl(hdmi->link, STM_DISPLAY_LINK_CTRL_DISPLAY_MODE, STM_DISPLAY_INVALID)<0)
    TRC(TRC_ID_ERROR, "HDMI manager can not set Display mode properly\n");

  stmhdmi_disconnect(hdmi);
  if (stmhdmi_stop_output(hdmi, STM_DISPLAY_HDCP_STOP_SIG) <0)
    TRC(TRC_ID_ERROR, "Unable to stop HDMI Tx properly\n");
  return 0;
}

