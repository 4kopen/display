/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/hdmi/hdmiscdc.c
 * Copyright (c) 2014 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/sched.h>

#include <stm_display.h>
#include <linux/stm/stmcorehdmi.h>
#include "stmhdmi_priv.h"

/******************************************************************************
 * Functions to access SCDCS elements
 */

static int stmhdmi_read_scdcs_manufacturer_specific_field(struct stm_hdmi *hdmi)
{
  int retVal;
  uint8_t scdc_buf[STM_HDMI_SCDCS_MANUFACTURER_SPECIFIC_LENGTH];

  /* Sink info reading */
  if ((retVal = stm_display_link_scdc_read ( hdmi->link,
                 STM_DISPLAY_LINK_SCDC_COMBINED_FORMAT_READ,
                 STM_HDMI_SCDCS_OFFSET_MANUFACTURER_SPECIFIC,
                 STM_HDMI_SCDCS_MANUFACTURER_SPECIFIC_LENGTH,
                 scdc_buf)) < 0)
  {
    DPRINTK("SCDC Read Error \n");
    hdmi->scdcs_info.scdcs_valid = false;
  }

  memcpy(hdmi->scdcs_info.manufacturer_ieee_oui,
      &scdc_buf[STM_HDMI_SCDCS_MANUFACTURER_OUI_IDX],
      STM_HDMI_SCDCS_MANUFACTURER_OUI_LENGTH);
  memcpy(hdmi->scdcs_info.device_id_string,
      &scdc_buf[STM_HDMI_SCDCS_MANUFACTURER_DEV_ID_STR_IDX],
      STM_HDMI_SCDCS_MANUFACTURER_DEV_ID_STR_LENGTH);
  hdmi->scdcs_info.device_id_hw_rev = scdc_buf[STM_HDMI_SCDCS_MANUFACTURER_DEV_ID_HW_REV_IDX];
  memcpy(hdmi->scdcs_info.device_id_sw_rev,
      &scdc_buf[STM_HDMI_SCDCS_MANUFACTURER_DEV_ID_SW_REV_IDX],
      STM_HDMI_SCDCS_MANUFACTURER_DEV_ID_SW_REV_LENGTH);
  /* TODO : error management (TMDS link error, SCDCS access error ...) */

  return retVal;
}

int stmhdmi_read_scdcs_character_error_detection_field(struct stm_hdmi *hdmi)
{
  int retVal;
  uint8_t scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_LENGTH];
  uint8_t                                chksum;
  uint8_t                                idx;

  if ((retVal = stm_display_link_scdc_read ( hdmi->link,
                 STM_DISPLAY_LINK_SCDC_COMBINED_FORMAT_READ,
                 STM_HDMI_SCDCS_OFFSET_ERR_DETECTION,
                 STM_HDMI_SCDCS_ERR_DETECTION_LENGTH,
                 scdc_buf)) < 0) {
    DPRINTK("SCDC Read Error \n");
    hdmi->scdcs_info.scdcs_valid = false;
    return retVal;
  }

  TRC(TRC_ID_MAIN_INFO, "HDMI sink - update of SCDCS character error detection counters :");
  TRC(TRC_ID_MAIN_INFO, "\t channel_0:%d (valid:%d)",
          scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_0_L_IDX]+
          ((scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_0_H_IDX] & 0x7F)<<8),
          (scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_0_H_IDX]) >> 7);
  TRC(TRC_ID_MAIN_INFO, "\t channel_1:%d (valid:%d)",
        scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_1_L_IDX]+
        ((scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_1_H_IDX] & 0x7F)<<8),
        (scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_1_H_IDX]) >> 7);
  TRC(TRC_ID_MAIN_INFO, "\t channel_2:%d (valid:%d)",
        scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_2_L_IDX]+
        ((scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_2_H_IDX] & 0x7F)<<8),
        (scdc_buf[STM_HDMI_SCDCS_ERR_DETECTION_2_H_IDX]) >> 7);

  chksum = 0;
  for (idx = 0 ; idx<STM_HDMI_SCDCS_ERR_DETECTION_LENGTH ; idx++)
  {
    chksum += scdc_buf[idx];
  }
  TRC(TRC_ID_MAIN_INFO, "\t chksum :%s",(chksum==0)?"OK":"NOK");
  /* TODO : error management (TMDS link error, SCDCS access error ...) */

  return retVal;
}

void stmhdmi_init_scdcs(struct stm_hdmi *hdmi)
{
  uint8_t scdc_wr_byte = STM_HDMI_SCDCS_SOURCE_VERSION;

  if (stm_display_link_scdc_read ( hdmi->link,
                 STM_DISPLAY_LINK_SCDC_COMBINED_FORMAT_READ,
                 STM_HDMI_SCDCS_OFFSET_SINK_VERSION,
                 1,
                 &hdmi->scdcs_info.sink_version)  >= 0)
  {
    if ((hdmi->scdcs_info.sink_version == STM_HDMI_SCDCS_SINK_VERSION_EXPECTED) ||
        (hdmi->scdcs_info.sink_version == STM_HDMI_SCDCS_SINK_VERSION_TOLERATED ))
      hdmi->scdcs_info.scdcs_valid = true;
    else
      {
      hdmi->scdcs_info.scdcs_valid = false ;
      return;
      }
  }

  stmhdmi_read_scdcs_manufacturer_specific_field(hdmi);

  /* Initialisation Source version */
  if (stm_display_link_scdc_write ( hdmi->link,
                     STM_HDMI_SCDCS_OFFSET_SOURCE_VERSION,
                     1,
                     &scdc_wr_byte ) < 0)
  {
    DPRINTK("SCDC Write Error \n");
    hdmi->scdcs_info.scdcs_valid = false;
    return;
  }

  /* Default initialisation TMDS Config */
  hdmi->scdcs_info.curr_tmds_config = 0x00 ;
  if (stm_display_link_scdc_write(hdmi->link,
                   STM_HDMI_SCDCS_OFFSET_TMDS_CONFIG,
                   1,
                   &hdmi->scdcs_info.curr_tmds_config) < 0)
  {
    DPRINTK("SCDC Write Error \n");
    hdmi->scdcs_info.scdcs_valid = false;
    return;
  }

  /* Default initialisation Config 0 */
  scdc_wr_byte = 0x00;
  if ( stm_display_link_scdc_write(hdmi->link,
                     STM_HDMI_SCDCS_OFFSET_CONFIG_0,
                     1,
                     &scdc_wr_byte) < 0)
  {
    DPRINTK("SCDC Write Error \n");
    hdmi->scdcs_info.scdcs_valid = false;
    return;
  }
}

void stmhdmi_invalidate_scdc_info(struct stm_hdmi *hdmi)
{
  memset(&hdmi->scdcs_info, 0, sizeof(struct stm_scdcs));

  hdmi->scdc_scrambling_status_polling_req = false;
  hdmi->scdc_scrambling_status_polling_value = 0;
  hdmi->scdc_update_flag_polling_req = false;
  hdmi->scdc_update_flag_polling_value = 0;
  hdmi->scdc_update_flag_polling_period = 0;
}

