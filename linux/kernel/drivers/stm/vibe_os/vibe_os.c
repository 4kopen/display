/***************************************************************************
This file is part of display_engine
COPYRIGHT (C) 2000-2015 STMicroelectronics - All Rights Reserved
License type: GPLv2

display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  display_engine; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

This file was last modified by STMicroelectronics on 2015-07-29
***************************************************************************/

#include <linux/version.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/spinlock.h>
#include <linux/semaphore.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/firmware.h>
#include <linux/device.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/math64.h>
#include <linux/dma-mapping.h>
#include <linux/kthread.h>
#include <linux/workqueue.h>
#include <linux/sched.h>
#include <linux/clk.h>
#include <linux/clk-private.h>
#include <linux/platform_device.h>
#include <linux/kref.h>
#include <asm/uaccess.h> /* copy_to_user definition */

#include <vibe_debug.h>

#if defined(CONFIG_DEBUG_FS)
#include <linux/debugfs.h>
#endif

#if defined(CONFIG_KPTRACE)
#include <linux/relay.h>
#include <trace/kptrace.h>
#endif

#if defined(CONFIG_MTT)
#include <linux/mtt.h>
#if !defined(CONFIG_DISPLAY_REMOVE_TRACES)
static mtt_comp_handle_t vibe_mtt_handle;
#endif
#endif

#if defined(CONFIG_BPA2)
#include <linux/bpa2.h>
#else
#error Kernel must have the BPA2 memory allocator configured
#endif

#include <asm/cacheflush.h>
#include <asm/io.h>
#include <asm/processor.h>
#include <asm/atomic.h>
#include <asm/uaccess.h>

#if defined(CONFIG_ARM)
#include <asm/mach-types.h>
/*
 * ARM has mis-named this interface relative to all the other platforms
 * that define it (including x86). However this is convenient because we
 * on ARM we are missing all the cache management functions. So we are going
 * to define the cached flag as uncached write combined, which for the display
 * driver's usage is what we really want anyway.
 */
#define ioremap_cache ioremap_wc

/*
 * After v2.6.32, we no longer have flush_ioremap_region on ARM
 * TODO: Decide if we need to ensure a BPA allocated page is not stale
 *       in the cache after ioremapping.
 */
#define flush_ioremap_region(phys, virt, offset, size) do { } while (0)

/*
 * Also define the missing invalidate_ioremap_region to {} so we don't have to
 * conditionalize the code.
 */
#define invalidate_ioremap_region(phys, virt, offset, size) do { } while (0)

#endif

#include <linux/st-socinfo.h>
#include <linux/gpio.h>

#include <stm_display.h>

#include <vibe_os.h>

#if defined(CONFIG_ST_FDMA)
#include <linux/platform_data/dma-st-fdma.h>
#endif /* defined(CONFIG_ST_FDMA) */

struct DMA_Channel
{
#if defined(CONFIG_ST_FDMA)
  const char *name;
#endif /* defined(CONFIG_ST_FDMA) */
  struct dma_chan *channel;
};

static const int debug_io_rw          = 0; /* Set to 1 to trace with delay all register reads and writes for debug */
static const int disable_io_rw        = 0; /* Set to 1 to disable all register reads and writes for debug */

static const int dmaarea_alloc_debug  = 0;
static const int dmaarea_access_debug = 0;

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
static const uint32_t GUARD_SIZE  = sizeof(uint32_t) * 2; // Size of one guard zone. There are 2 guard zones: total size = 2*GUARD_SIZE.
static const uint32_t botguard0  = 0x12345678;
static const uint32_t botguard1  = 0x87654321;
static const uint32_t topguard0  = 0x89abcdef;
static const uint32_t topguard1  = 0xfedcba98;
static const uint32_t guardfreed = 0x000f3eed;
#else
static const uint32_t GUARD_SIZE  = 0;
#endif


/* trace */
#define MAX_LENGTH_ARG 255
#define MAX_LENGTH_FCT 128
#define MAX_STRING_LENGTH 50


/************************************************************/

#ifdef CONFIG_DISPLAY_ASYNC_TRACES

#include <thread_vib_settings.h>

#define ASYNC_PRINT_MAX_STRING_LENGTH   300
#define ASYNC_PRINT_SLOTS_NBR           200

typedef struct async_print_slot_s
{
  char    text[ASYNC_PRINT_MAX_STRING_LENGTH];
} async_print_slot_t;

typedef struct async_trace_s
{
    VIBE_OS_WaitQueue_t wait_queue;
    void*               print_thread_desc;
    bool                print_trace_lost;
    uint32_t            print_read_index;
    uint32_t            print_write_index;
    async_print_slot_t  slots[ASYNC_PRINT_SLOTS_NBR];
    spinlock_t          lock;
} async_trace_t;

static async_trace_t async_trace = { 0 };

static bool         start_async_print_thread(void);
static bool         stop_async_print_thread(void);
static void         async_print_thread(void *data);

static int thread_vib_asyncprint[2] = { THREAD_VIB_ASYNCPRINT_POLICY, THREAD_VIB_ASYNCPRINT_PRIORITY };
module_param_array_named(thread_VIB_AsyncPrint,thread_vib_asyncprint, int, NULL, 0644);
MODULE_PARM_DESC(thread_VIB_AsyncPrint, "VIB-AsyncPrint thread:s(Mode),p(Priority)");

#endif


typedef struct _RESOURCE_LOCK
{
  spinlock_t    theLock;
  unsigned long lockFlags;
} RESOURCE_LOCK;


struct stm_dma_transfer_handle
{
  int channel;
#if defined(CONFIG_ST_FDMA)
  struct st_dma_params *params;
#endif /* defined(CONFIG_ST_FDMA) */
};


#if defined(CONFIG_STM_VIRTUAL_PLATFORM)
/*
 * The virtual platform developers want to prevent firmwares being loaded.
 * They have been told that (a) this is wrong and (b) this change will not
 * actually stop a lot of firmwares actually being loaded, because they
 * are currently hardcoded into the source. But for the moment this is a
 * minimal version of the change they wanted.
 */
const int DISABLE_FIRMWARE_LOADING = 1;
#else
const int DISABLE_FIRMWARE_LOADING = 0;
#endif

static const char *video_memory_partition  = "coredisplay-video";
static const char *system_memory_partition = "coredisplay-video"; /*"bigphysarea";*/

#if defined(CONFIG_FW_LOADER) || defined(CONFIG_FW_LOADER_MODULE)
#define FIRMWARE_NAME_MAX 30
typedef struct
{
  struct list_head       fw_list;
  struct platform_device *pdev;
  STMFirmware            firmware;
  const struct firmware *lnx_firmware;
  char                   name[FIRMWARE_NAME_MAX + 1];

  struct kref            kref;
} STMFirmware_private;

struct firmware_data {
  struct list_head fw_cache;
};

static struct firmware_data firmware_data = {
  .fw_cache = LIST_HEAD_INIT (firmware_data.fw_cache),
};
#endif

/*
 * We need a device for the IOS implementation in order to be able to use
 * the dma-mapping interfaces.
 */
static void ios_dev_release (struct device * device) { }

static struct device ios_device = {
  .init_name         = "stm-vibe_os",
  .coherent_dma_mask = DMA_BIT_MASK(32),
  .release           = ios_dev_release
};

int vibe_os_init(void)
{
  int   failure;

  if(!device_is_registered(&ios_device))
  {
    failure = device_register (&ios_device);
    if (unlikely (failure))
    {
      dev_err (&ios_device, "Device registration [%s] failed: %d\n", dev_name(&ios_device), failure);
      return -1;
    }
#if defined(CONFIG_FW_LOADER) || defined(CONFIG_FW_LOADER_MODULE)
    dev_set_drvdata(&ios_device, &firmware_data);
#endif
    ios_device.of_node = of_find_compatible_node(NULL,NULL,"st,display");
  }

#ifdef CONFIG_DISPLAY_ASYNC_TRACES
  if (!start_async_print_thread())
  {
      dev_err(&ios_device, "Failed to start the VIBE ASYNC thread\n");
      return -1;
  }
#endif

  return 0;
}

#if defined(CONFIG_FW_LOADER) || defined(CONFIG_FW_LOADER_MODULE)
static void firmware_release (struct device * device);
#endif

void vibe_os_release(void)
{
#ifdef CONFIG_DISPLAY_ASYNC_TRACES
  if (!stop_async_print_thread())
  {
      dev_err(&ios_device, "Failed to stop the VIBE ASYNC thread\n");
  }
#endif

  if (likely (device_is_registered(&ios_device)))
  {
#if defined(CONFIG_FW_LOADER) || defined(CONFIG_FW_LOADER_MODULE)
    firmware_release(&ios_device);
#endif
    of_node_put(ios_device.of_node);
    device_unregister(&ios_device);
  }
}


bool vibe_os_create_thread(VIBE_OS_ThreadFct_t          fct,
                         void                          *parameter,
                         const char                    *name,
                         const VIBE_OS_thread_settings *thread_settings,
                         void                         **thread_desc)
{
  struct task_struct **thread = (struct task_struct **) thread_desc;
  int                  sched_policy;
  struct sched_param   sched_param;

  /* Create thread */
  *thread = kthread_create(fct, parameter, name);
  if( *thread == NULL)
  {
    TRC(TRC_ID_ERROR, "FAILED to create thread %s", name);
    return false;
  }

  /* Set thread scheduling settings */
  if( thread_settings == NULL )
  {
    TRC(TRC_ID_ERROR, "FAILED no thread_settings for thread %s", name);
    return false;
  }
  sched_policy               = thread_settings->policy;
  sched_param.sched_priority = thread_settings->priority;
  if ( sched_setscheduler(*thread, sched_policy, &sched_param) )
  {
    TRC(TRC_ID_ERROR, "FAILED to set thread scheduling parameters: name=%s, policy=%d, priority=%d", \
        name, sched_policy, sched_param.sched_priority);
    return false;
  }

  return true;
}

int vibe_os_stop_thread(void *thread_desc)
{
  struct task_struct *thread = (struct task_struct *) thread_desc;

  return (kthread_stop(thread));
}

bool vibe_os_wake_up_thread(void *thread_desc)
{
  struct task_struct *thread = (struct task_struct *) thread_desc;

  wake_up_process(thread);

  return true;
}

bool vibe_os_thread_should_stop(void)
{
    return (kthread_should_stop());
}


void vibe_os_sleep_ms(uint32_t ms)
{
  /* Implemented as per Linux kernel recommendations:                 */
  /* When sleeping for few msecs (up to 20ms), use usleep_range.      */
  /* https://www.kernel.org/doc/Documentation/timers/timers-howto.txt */
  if (ms > 0)
  {
    if (ms < 20)
    {
        usleep_range(1000*ms, 1000*(ms+1));
    }
    else
    {
      msleep(ms);
    }
  }
}


uint32_t vibe_os_read_register(volatile uint32_t *baseAddress, uint32_t offset)
{
  if(unlikely(debug_io_rw))
  {
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_read_register(%p,0x%08x)", baseAddress, offset);
    mdelay(1);
  }
  if(likely(!disable_io_rw))
    return readl(baseAddress+(offset>>2));
  else
    return 0;
}


void vibe_os_write_register(volatile uint32_t *baseAddress, uint32_t offset, uint32_t val)
{
  if(unlikely(debug_io_rw))
  {
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_write_register(%p,0x%08x,0x%08x)",baseAddress,offset,val);
    mdelay(1);
  }

  if(likely(!disable_io_rw))
    writel(val, baseAddress+(offset>>2));
}


uint8_t vibe_os_read_byte_register(volatile uint8_t *reg)
{
  if(unlikely(debug_io_rw))
  {
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_read_byte_register(%p)",reg);
    mdelay(1);
  }

  if(likely(!disable_io_rw))
    return readb(reg);
  else
    return 0;
}


void vibe_os_write_byte_register(volatile uint8_t *reg,uint8_t val)
{
  if(unlikely(debug_io_rw))
  {
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_write_byte_register(%p,0x%08x)",reg,(uint32_t)val);
    mdelay(1);
  }

  if(likely(!disable_io_rw))
    writeb(val, reg);
}


vibe_time64_t vibe_os_get_system_time(void)
{
  struct timespec ts;

  getrawmonotonic(&ts);

  return ktime_to_us(timespec_to_ktime(ts));
}


vibe_time64_t vibe_os_get_one_second(void)
{
  return USEC_PER_SEC;
}


void vibe_os_stall_execution(uint32_t ulDelay)
{
  uint32_t ulms = ulDelay/1000;
  uint32_t ulus = ulDelay%1000;

  /* if the delay is greater than one millisecond then use mdelay for each
   * full millisecond as calling udelay with a large number can cause wrapping
   * problems on fast machines.
   */
  if (ulms)
    mdelay(ulms);

  /* Mop up any fraction of a millisecond with a direct call to udelay */
  if (ulus)
    udelay(ulus);

}


void* vibe_os_map_memory(uint32_t ulBaseAddr, uint32_t ulSize)
{
  return ioremap(ulBaseAddr, ulSize);
}


void vibe_os_unmap_memory(void* pVMem)
{
  iounmap(pVMem);
}


/* vibe_os_zero_memory() must not be called for initializing a C++ object
 * because the virtual destructor table will be corrupted.
 * The C++ object constructor must be used for initializing it.
 * This functions is dedicated for structure or table of pointers initialization.
 */
void vibe_os_zero_memory(void* pMem, uint32_t count)
{
  memset(pMem, 0, count);
}

void vibe_os_memset(void* pMem, int val, uint32_t size)
{
  memset(pMem, val, size);
}

int vibe_os_memcmp(const void *pMem1, const void *pMem2, uint32_t size)
{
    return memcmp(pMem1, pMem2, size);
}


void vibe_os_memcpy(void *pMem1, const void *pMem2, uint32_t size)
{
  memcpy(pMem1, pMem2, size);
}

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
int vibe_os_check_dma_area_guards(DMA_Area *mem)
{
  uint32_t *guard,*topguard;

  if(!mem->pMemory)
    return 0;

  if(((uint32_t)mem->pMemory & 0x3) != 0)
  {
    TRC(TRC_ID_ERROR, "vibe_os_check_dma_area_guards: unaligned mem = %p pMemory = %p",mem,mem->pMemory);
    return -1;
  }

  guard = (uint32_t*)mem->pMemory;
  topguard = (uint32_t *)(mem->pData+mem->ulDataSize);
  if(mem->ulFlags & SDAAF_IOREMAPPED)
  {
    if((readl(guard) != botguard0) || (readl(guard+1) != botguard1))
    {
      TRC(TRC_ID_ERROR, "vibe_os_check_dma_area_guards: bad guard before data mem = %p pMemory = %p guard[0] = 0x%x guard[1] = 0x%x",mem,mem->pMemory,guard[0],guard[1]);
      return -1;
    }
    if((readl(topguard) != topguard0) || (readl(topguard+1) != topguard1))
    {
      TRC(TRC_ID_ERROR, "vibe_os_check_dma_area_guards: bad guard after data mem = %p pMemory = %p guard[0] = 0x%x guard[1] = 0x%x",mem,mem->pMemory,topguard[0],topguard[1]);
      return -1;
    }
  }
  else
  {
    if((guard[0] != botguard0) || (guard[1] != botguard1))
    {
      TRC(TRC_ID_ERROR, "vibe_os_check_dma_area_guards: bad guard before data mem = %p pMemory = %p guard[0] = 0x%x guard[1] = 0x%x",mem,mem->pMemory,guard[0],guard[1]);
      return -1;
    }
    if((topguard[0] != topguard0) || (topguard[1] != topguard1))
    {
      TRC(TRC_ID_ERROR, "vibe_os_check_dma_area_guards: bad guard after data mem = %p pMemory = %p guard[0] = 0x%x guard[1] = 0x%x",mem,mem->pMemory,topguard[0],topguard[1]);
      return -1;
    }
  }

  return 0;
}
#endif

/*
 * Internal DMA area flushing used by the Flush, Memset and MemcpyTo entrypoints
 */
static inline void flush_dma_area(DMA_Area *mem, uint32_t offset, uint32_t count)
{
  if(mem->ulFlags & SDAAF_IOREMAPPED)
  {
    if (!(mem->ulFlags & SDAAF_UNCACHED))
      flush_ioremap_region(mem->ulPhysical, mem->pData, offset, count);
  }
  else
  {
    uint32_t dataOffset = (uint32_t)mem->pData - (uint32_t)mem->pMemory;
    dma_sync_single_range_for_device(&ios_device, mem->allocHandle,
                                     dataOffset+offset, count, DMA_TO_DEVICE);
  }
  /*
   * And stick in a memory barrier particularly for when we are doing
   * uncached write combined.
   */
  wmb();
}


void vibe_os_flush_dma_area(DMA_Area *mem, uint32_t offset, uint32_t count)
{
  if(dmaarea_access_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_flush_dma_area: mem = %p, pMemory = %p, pData = %p, size = %u offset = %u count = %u",mem,mem->pMemory,mem->pData,mem->ulDataSize,offset,count);

  if((offset > mem->ulDataSize) ||
     (count  > mem->ulDataSize) ||
     ((offset+count) > mem->ulDataSize))
  {
    TRC(TRC_ID_ERROR, "vibe_os_flush_dma_area: access out of range, size = %u offset = %u count = %u",mem->ulDataSize,offset,count);
  }

  flush_dma_area(mem,offset,count);
}


void vibe_os_memset_dma_area(DMA_Area *mem, uint32_t offset, int val, uint32_t count)
{
  if(dmaarea_access_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_memset_dma_area: mem = %p, pMemory = %p, pData = %p, size = %u offset = %u count = %u",mem,mem->pMemory,mem->pData,mem->ulDataSize,offset,count);

  if((offset > mem->ulDataSize) ||
     (count  > mem->ulDataSize) ||
     ((offset+count) > mem->ulDataSize))
  {
    TRC(TRC_ID_ERROR, "vibe_os_memset_dma_area: access out of range, size = %u offset = %u count = %u",mem->ulDataSize,offset,count);
    return;
  }

  if(mem->ulFlags & SDAAF_IOREMAPPED)
  {
    memset_io(mem->pData+offset, val, count);
  }
  else
  {
    memset(mem->pData+offset, val, count);
  }

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  vibe_os_check_dma_area_guards(mem);
#endif

  flush_dma_area(mem, offset, count);
}


void vibe_os_memcpy_to_dma_area(DMA_Area *mem, uint32_t offset, const void* pSrc, uint32_t count)
{
  if(dmaarea_access_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_memcpy_to_dma_area: mem = %p, pMemory = %p, pData = %p, size = %u offset = %u count = %u",mem,mem->pMemory,mem->pData,mem->ulDataSize,offset,count);

  if((offset > mem->ulDataSize) ||
     (count  > mem->ulDataSize) ||
     ((offset+count) > mem->ulDataSize))
  {
    TRC(TRC_ID_ERROR, "vibe_os_memcpy_to_dma_area: access out of range, size = %u offset = %u count = %u",mem->ulDataSize,offset,count);
    return;
  }

  if(mem->ulFlags & SDAAF_IOREMAPPED)
  {
    memcpy_toio(mem->pData+offset, pSrc, count);
  }
  else
  {
    memcpy(mem->pData+offset, pSrc, count);
  }

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  vibe_os_check_dma_area_guards(mem);
#endif

  flush_dma_area(mem, offset, count);
}


void vibe_os_memcpy_from_dma_area(DMA_Area *mem, uint32_t offset, void* pDst, uint32_t count)
{
  if(dmaarea_access_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_memcpy_from_dma_area: mem = %p, pMemory = %p, pData = %p, size = %u offset = %u count = %u",mem,mem->pMemory,mem->pData,mem->ulDataSize,offset,count);

  if((offset > mem->ulDataSize) ||
     (count  > mem->ulDataSize) ||
     ((offset+count) > mem->ulDataSize))
  {
    TRC(TRC_ID_ERROR, "vibe_os_memcpy_from_dma_area: access out of range, size = %u offset = %u count = %u",mem->ulDataSize,offset,count);
    return;
  }

  if(mem->ulFlags & SDAAF_IOREMAPPED)
  {
    if (!(mem->ulFlags & SDAAF_UNCACHED))
      invalidate_ioremap_region(mem->ulPhysical, mem->pData, offset, count);

    memcpy_fromio(pDst, mem->pData+offset, count);
  }
  else
  {
    uint32_t dataOffset = (uint32_t)mem->pData - (uint32_t)mem->pMemory;
    dma_sync_single_range_for_device(&ios_device, mem->allocHandle,
                                     dataOffset+offset, count, DMA_FROM_DEVICE);
    memcpy(pDst, mem->pData+offset, count);
  }
}


/* Do do not use this function with a C++ object as it will not call the
 * constructor or destructor (new/delete must be used for C++ object).
 * Also this function can not be mixed with new/delete.
 */
void *vibe_os_allocate_memory(uint32_t size)
{
  char  *mem   = kmalloc(size+GUARD_SIZE*2, GFP_KERNEL);
#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  uint32_t *guard = (uint32_t *)mem;
#endif

  if(!mem)
  {
    TRC(TRC_ID_ERROR, "vibe_os_allocate_memory: unable to kmalloc %u bytes",size);
    return 0;
  }

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  guard[0] = size;
  guard[1] = botguard1;

  guard = (uint32_t *)(mem+size+GUARD_SIZE);

  guard[0] = topguard0;
  guard[1] = topguard1;
#endif

  return mem+GUARD_SIZE;
}


/* Do do not use this function with a C++ object as it will not call the
 * constructor or destructor (new/delete must be used for C++ object).
 * Also this function can not be mixed with new/delete.
 */
void vibe_os_free_memory(void *mem)
{
  uint32_t *guard;
#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  uint32_t *topguard;
  uint32_t size;
#endif

  if(!mem)
    return;

  guard = (uint32_t *)((char*)mem - GUARD_SIZE);

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  size  = guard[0];

  if(guard[1] != botguard1)
  {
    TRC(TRC_ID_ERROR, "vibe_os_free_memory: bad guard before data mem = %p allocation size = %u guard[1] = 0x%x",mem,size,guard[1]);
    TRC(TRC_ID_ERROR, "vibe_os_free_memory: freeing anyway\n");
  }
  else
  {
    topguard = (uint32_t *)((char*)mem+size);
    if((topguard[0] != topguard0) || (topguard[1] != topguard1))
    {
      TRC(TRC_ID_ERROR, "vibe_os_free_memory: bad guard after data mem = %p topguard[0] = 0x%x topguard[1] = 0x%x",mem,topguard[0],topguard[1]);
      TRC(TRC_ID_ERROR, "vibe_os_free_memory: freeing anyway\n");
    }
  }

  guard[0] = guardfreed;
  guard[1] = guardfreed;
#endif

  kfree(guard);

  return;
}


void vibe_os_allocate_bpa2_dma_area(DMA_Area *mem, uint32_t align)
{
#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  uint32_t         *guard;
#endif
  uint32_t          nBytes;
  uint32_t          nPages;
  struct bpa2_part *part;
  const char       *part_name = NULL;
  uint32_t          dataOffset;

  if(dmaarea_alloc_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_allocate_bpa2_dma_area: mem = %p, align = %u",mem, align);

  /* Allow room for top and bottom memory guards and for alignment */
  nBytes = mem->ulDataSize + GUARD_SIZE*2 + align;
  nPages = (nBytes + PAGE_SIZE - 1) / PAGE_SIZE;

  /*
   * Adjust allocation size as bpa2 allocates whole pages
   */
  mem->ulAllocatedSize = nPages*PAGE_SIZE;

  if (mem->ulFlags & SDAAF_VIDEO_MEMORY)
    part_name = video_memory_partition;
  else if (mem->ulFlags & SDAAF_SECURE)
    part_name = video_memory_partition;
  else
    part_name = system_memory_partition;

  part = bpa2_find_part(part_name);
  if(part)
    mem->allocHandle = bpa2_alloc_pages(part, nPages, 1, GFP_KERNEL);
  else
    TRC(TRC_ID_ERROR, "%s: BPA2 partition '%s' not available in this kernel",
           __PRETTY_FUNCTION__, part_name);

  if(mem->allocHandle == 0)
  {
    if(dmaarea_alloc_debug)
      TRC(TRC_ID_UNCLASSIFIED, "vibe_os_allocate_bpa2_dma_area: allocation failed");

    memset(mem, 0, sizeof(DMA_Area));
    return;
  }

  /*
   * Remap physical.
   */
  mem->pMemory = (mem->ulFlags & SDAAF_UNCACHED)
                 ? ioremap_nocache(mem->allocHandle, mem->ulAllocatedSize)
                 : ioremap_cache(mem->allocHandle, mem->ulAllocatedSize);

  if(mem->pMemory == 0)
  {
    if(dmaarea_alloc_debug)
      TRC(TRC_ID_UNCLASSIFIED, "vibe_os_allocate_bpa2_dma_area: ioremap failed");

    bpa2_free_pages(part, mem->allocHandle);
    memset(mem, 0, sizeof(DMA_Area));
    return;
  }

  mem->ulFlags |= SDAAF_IOREMAPPED;

  if (!(mem->ulFlags & SDAAF_UNCACHED))
    flush_ioremap_region(mem->allocHandle, mem->pMemory, 0, mem->ulAllocatedSize);

  /* Move start of data up to allow room for the bottom memory guard */
  mem->pData = (char*)((uint32_t)mem->pMemory + GUARD_SIZE);

  /* Now align the data area in the standard fashion */
  if(align>0)
    mem->pData = (char*)(((uint32_t)mem->pData + (align-1)) & ~(align-1));

  /* Adjust the physical for the alignment */
  dataOffset = (uint32_t)mem->pData - (uint32_t)mem->pMemory;
  mem->ulPhysical = mem->allocHandle + dataOffset;

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  /*
   * Write guards using IO functions
   */
  guard = (uint32_t*)mem->pMemory;
  writel(botguard0, guard);
  writel(botguard1, guard+1);

  guard = (uint32_t*)(mem->pData+mem->ulDataSize);
  writel(topguard0, guard);
  writel(topguard1, guard+1);
#endif
}


void vibe_os_allocate_kernel_dma_area(DMA_Area *mem, uint32_t align)
{
#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  uint32_t *guard;
#endif
  uint32_t  dataOffset;

  if(dmaarea_alloc_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_allocate_kernel_dma_area: mem = %p, align = %u",mem, align);

  /*
   * Allow room for top and bottom memory guards and for alignment.
   *
   * NOTE: kmalloc always returns at least a cacheline aligned pointer
   */
  mem->ulAllocatedSize = mem->ulDataSize + GUARD_SIZE*2 + align;

  if((mem->pMemory = kmalloc(mem->ulAllocatedSize, GFP_KERNEL)) == NULL)
  {
    memset(mem, 0, sizeof(DMA_Area));
    return;
  }

  /* Move start of data up to allow room for the bottom memory guard */
  mem->pData = (char*)((uint32_t)mem->pMemory + GUARD_SIZE);

  /* Now align the data area in the standard fashion */
  if(align>0)
    mem->pData = (char*)(((uint32_t)mem->pData + (align-1)) & ~(align-1));

  mem->allocHandle = dma_map_single(&ios_device, mem->pMemory,mem->ulAllocatedSize, DMA_BIDIRECTIONAL );
  /* Adjust the physical for the data start alignment */
  dataOffset = (uint32_t)mem->pData - (uint32_t)mem->pMemory;
  mem->ulPhysical = mem->allocHandle + dataOffset;

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  /*
   * Write guards directly
   */
  guard = (uint32_t*)mem->pMemory; /* Note: NOT pData!!! */
  guard[0] = botguard0;
  guard[1] = botguard1;

  guard = (uint32_t*)(mem->pData+mem->ulDataSize);
  guard[0] = topguard0;
  guard[1] = topguard1;
#endif

  dma_sync_single_for_device(&ios_device, mem->allocHandle, mem->ulAllocatedSize, DMA_TO_DEVICE);
}


void vibe_os_allocate_dma_area(DMA_Area *mem, uint32_t size, uint32_t align, STM_DMA_AREA_ALLOC_FLAGS flags)
{
  if(dmaarea_alloc_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_allocate_dma_area: mem = %p, size = %u, align = %u, flags = 0x%x",mem, size, align, flags);

  if(!mem)
  {
    TRC(TRC_ID_ERROR, "error: mem parameter == NULL");
    return;
  }

  memset(mem, 0, sizeof(DMA_Area));

  // Allocate DMA area size of 0
  if(size == 0)
  {
    TRC(TRC_ID_ERROR, "vibe_os_allocate_dma_area: error: size==0");
    return;
  }

  mem->ulDataSize      = size;
  mem->ulFlags         = flags & ~SDAAF_IOREMAPPED;

  if((mem->ulDataSize > (PAGE_SIZE-GUARD_SIZE*2-align)) ||
     (flags & (SDAAF_VIDEO_MEMORY | SDAAF_UNCACHED | SDAAF_SECURE)))
  {
    vibe_os_allocate_bpa2_dma_area(mem, align);
  }
  else
  {
    vibe_os_allocate_kernel_dma_area(mem, align);
  }

  if(dmaarea_alloc_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_allocate_dma_area: mem = %p, pMemory = %p (%s), pData = %p,\n\t allocated size = %u data size = %u",mem,mem->pMemory,(flags&SDAAF_UNCACHED)?"uncached":"cached",mem->pData,mem->ulAllocatedSize,mem->ulDataSize);
}


void vibe_os_free_dma_area(DMA_Area *mem)
{
#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  uint32_t *guard;
#endif

  if(dmaarea_alloc_debug)
    TRC(TRC_ID_UNCLASSIFIED, "vibe_os_free_dma_area: mem = %p, pMemory = %p, pData = %p, size = %u",mem,mem->pMemory,mem->pData,mem->ulDataSize);

  if(!mem->pMemory)
    return;

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
  if(vibe_os_check_dma_area_guards(mem)<0)
    TRC(TRC_ID_ERROR, "vibe_os_free_dma_area: ******************* freeing corrupted memory ***************");
#endif

  if (mem->ulFlags & SDAAF_IOREMAPPED)
  {
    struct bpa2_part *part;

#ifdef CONFIG_DISPLAY_MEMORY_GUARD
    guard = (uint32_t*)mem->pMemory;
    writel(guardfreed, guard);
    writel(guardfreed, guard+1);
#endif

    part = bpa2_find_part((mem->ulFlags & SDAAF_VIDEO_MEMORY)?video_memory_partition:system_memory_partition);

    if(part)
    {
      iounmap(mem->pMemory);
      bpa2_free_pages(part, mem->allocHandle);
    }
  }
  else
  {
#ifdef CONFIG_DISPLAY_MEMORY_GUARD
    guard = (uint32_t*)mem->pMemory;
    guard[0] = guardfreed;
    guard[1] = guardfreed;
#endif

    dma_unmap_single(&ios_device, mem->allocHandle, mem->ulAllocatedSize, DMA_BIDIRECTIONAL);
    kfree(mem->pMemory);
  }

  memset(mem, 0, sizeof(DMA_Area));
}

#if defined(CONFIG_ST_FDMA)

DMA_Channel *vibe_os_get_dma_channel(const char* channel_name)
{
  DMA_Channel *chan = kzalloc(sizeof(DMA_Channel), GFP_KERNEL);
  dma_cap_mask_t mask;
  int channel_name_len = 0;

  if(!chan)
    return NULL;

  dma_cap_zero(mask);
  dma_cap_set(DMA_SLAVE, mask);

  /* Request a matching dma channel */
#define CHANNEL_NAME_MAX_LEN 20
  channel_name_len = strnlen(channel_name, CHANNEL_NAME_MAX_LEN);
  if (channel_name && channel_name_len > 0 && channel_name_len < CHANNEL_NAME_MAX_LEN)
  {
    chan->name = channel_name;
  } else {
    TRC(TRC_ID_MAIN_INFO, "A channel name was not provided; Requesting channel 'unpaced'");
    chan->name = "unpaced";
  }

  chan->channel = dma_request_slave_channel(&ios_device, chan->name);
  if(!chan->channel)
  {
    TRC(TRC_ID_MAIN_INFO, "Unable to find a DMA channel called %s under display node !", chan->name);
    goto error_exit;
  }

  return chan;

error_exit:
  kfree(chan);
  return NULL;
}


void vibe_os_release_dma_channel(DMA_Channel *chan)
{
  if(!chan)
    return;

  dma_release_channel(chan->channel);
  kfree(chan);
}


void vibe_os_stop_dma_channel(DMA_Channel *chan)
{
  if(!chan)
    return;

  dmaengine_terminate_all(chan->channel);
}


void* vibe_os_create_dma_transfer(DMA_Channel *chan, DMA_transfer *transfer_list, STM_DMA_TRANSFER_PACING pacing, uint32_t bytes_per_req, uint8_t buswidth)
{
  struct dma_async_tx_descriptor *desc;
  struct dma_slave_config config;
  struct scatterlist *sg;
  unsigned long tx_flags;
  int list_length;
  int i;

  if(!chan)
  {
    TRC(TRC_ID_ERROR, "error: chan==NULL");
    return NULL;
  }

  if(!transfer_list)
    return NULL;

if (!buswidth || !bytes_per_req)
  {
    TRC(TRC_ID_ERROR, "error: buswidth and bytes_pre_req can't be NULL !!");
    return NULL;
  }
  list_length = 0;
  while(transfer_list[list_length].src)
  {
    /*
     * We are only supporting memory to device register transfers so the
     * destination must be the same physical address.
     */
    if((transfer_list[list_length].dst != transfer_list[0].dst) ||
       (transfer_list[list_length].dst_offset != transfer_list[0].dst_offset) )
    {
#ifdef DEBUG
      TRC(TRC_ID_MAIN_INFO, "transfer list entry %d is not consistent with a SG FDMA hardware transfer",list_length);
#endif
      return NULL;
    }

    list_length++;
  }

  if(!list_length)
    return NULL;

#ifdef DEBUG
    TRC(TRC_ID_MAIN_INFO, "transfer list has %d entries",list_length);
#endif

  config.direction = DMA_MEM_TO_DEV;
  config.dst_addr = transfer_list[0].dst->ulPhysical + transfer_list[0].dst_offset;
  if(pacing != SDTP_UNPACED)
  {
      /* buswidth and maxburst params can be updated each time we create
       * a new dma transfer, even if we use the same channel;
       * so we place them here instead of fixing them as properties of
       * the channel itself */
      config.dst_addr_width = buswidth;
      config.dst_maxburst   = bytes_per_req / buswidth;
  }

  if(dmaengine_slave_config(chan->channel, &config))
  {
    TRC(TRC_ID_MAIN_INFO, "unable to set DMA channel slave config");
    return NULL;
  }

  sg = kmalloc(sizeof(struct scatterlist)*list_length, GFP_KERNEL);
  if(!sg)
    return NULL;

  sg_init_table(sg,list_length);

  for(i=0;i<list_length;i++)
  {
    sg_dma_address(&sg[i]) = transfer_list[i].src->ulPhysical + transfer_list[i].src_offset;
    sg_dma_len(&sg[i]) = transfer_list[i].size;
#ifdef DEBUG
    TRC(TRC_ID_MAIN_INFO, "sg[%d] = %d bytes @ 0x%lx",i,(int)sg_dma_len(&sg[i]),(unsigned long)sg_dma_address(&sg[i]));
#endif

  }

  tx_flags = DMA_COMPL_SKIP_SRC_UNMAP | DMA_COMPL_SKIP_DEST_UNMAP;

  desc = dmaengine_prep_slave_sg(chan->channel,
                  sg, list_length, DMA_MEM_TO_DEV, tx_flags);

#ifdef DEBUG
  if(!desc)
    TRC(TRC_ID_MAIN_INFO, "failed to prep async slave sg transfer");
#endif

  if(desc)
  {
    /*
     * We can only take a callback at the end of the SG transaction, which is
     * OK that is the usage we actually want in practice, rather than actually
     * getting a callback after every element of the transfer list.
     */
    desc->callback = transfer_list[list_length-1].completed_cb;
    desc->callback_param = transfer_list[list_length-1].cb_param;
  }

  kfree(sg);
  return desc;
}


void  vibe_os_delete_dma_transfer(void *handle)
{
  struct dma_async_tx_descriptor *desc = (struct dma_async_tx_descriptor *)handle;

  if(!desc)
    return;

  /*
   * This just sets the ACK flag in the descriptor, it will be recycled the
   * next time a descriptor is created or if the channel is released.
   */
  async_tx_ack(desc);
}


int vibe_os_start_dma_transfer(void *handle)
{
  struct dma_async_tx_descriptor *desc = (struct dma_async_tx_descriptor *)handle;
  int err;
  if(!desc)
  {
    TRC(TRC_ID_ERROR, "error: desc==NULL");
    return -EINVAL;
  }
  err = dmaengine_submit(desc);
  dma_async_issue_pending(desc->chan);
  return dma_submit_error(err)?-1:0;
}

#endif /* defined(CONFIG_ST_FDMA) */


#if !defined(CONFIG_ST_FDMA)
DMA_Channel *vibe_os_get_dma_channel(STM_DMA_TRANSFER_PACING pacing, uint32_t bytes_per_req, STM_DMA_TRANSFER_FLAGS flags) { return NULL; }
void vibe_os_release_dma_channel(DMA_Channel *chan) {}
void vibe_os_stop_dma_channel(DMA_Channel *chan) {}
void* vibe_os_create_dma_transfer(DMA_Channel *chan, DMA_transfer *transfer_list, STM_DMA_TRANSFER_PACING pacing, STM_DMA_TRANSFER_FLAGS flags) { return NULL; }
void  vibe_os_delete_dma_transfer(void *handle) {}
int vibe_os_start_dma_transfer(void *handle) { return -1; }
#endif /* !defined(CONFIG_ST_FDMA) */


/* -------- */
/* | lock | */
/* -------- */

void * __vibe_os_create_resource_lock(vibe_os_lock_class_key *key, const char *name)
{
    RESOURCE_LOCK *pLock;

#ifdef CONFIG_LOCKDEP
    /* struct lock_class_key takes no space if lockdep is disabled */
    /* Is our local opaque version of lock_class_key big enough? */
    BUILD_BUG_ON((sizeof(struct lock_class_key) > sizeof(vibe_os_lock_class_key)));
#endif

    pLock = kmalloc(sizeof(RESOURCE_LOCK),GFP_KERNEL);
    if (pLock != NULL)
    {
      spin_lock_init(&(pLock->theLock));
      lockdep_set_class_and_name(&(pLock->theLock), (struct lock_class_key *)key, name);
    }
    return pLock;
}


void vibe_os_delete_resource_lock ( void * ulHandle )
{
    kfree(ulHandle);
}


void vibe_os_lock_resource ( void * ulHandle )
{
    RESOURCE_LOCK *pLock = (RESOURCE_LOCK*)ulHandle;
    spin_lock_irqsave(&(pLock->theLock), pLock->lockFlags);
}


void vibe_os_unlock_resource ( void * ulHandle )
{
    RESOURCE_LOCK *pLock = (RESOURCE_LOCK*)ulHandle;
    spin_unlock_irqrestore(&(pLock->theLock),pLock->lockFlags);
}

/* ------------- */
/* | semaphore | */
/* ------------- */

void * vibe_os_create_semaphore ( int initVal )
{
  struct semaphore *pSema = kmalloc(sizeof(struct semaphore),GFP_KERNEL);
  if(pSema == NULL)
  {
    TRC(TRC_ID_ERROR, "Failed, cannot allocate this semaphore");
    return NULL;
  }
  sema_init(pSema,initVal);
  return pSema;
}


void vibe_os_delete_semaphore ( void * ulHandle )
{
  kfree(ulHandle);
}


int vibe_os_down_semaphore ( void * ulHandle )
{
  int res;
  struct semaphore *pSema = ulHandle;

  if ( vibe_os_in_interrupt() != 0)
  {
    TRC(TRC_ID_ERROR, "Semaphore called from interrupt context!!!");
  }

  res = down_interruptible(pSema);
  if((res == -EINTR) && signal_pending(current))
  {
    res = -ERESTARTSYS;
  }
  else if(res != 0)
  {
     TRC(TRC_ID_ERROR, "Failed to down sem 0x%p (res = %d)", pSema, res);
  }

  return res;
}


int vibe_os_down_trylock_semaphore ( void * ulHandle )
{
  /* if the semaphore is not available at the time of the call, down_trylock returns immediately with */
  /* a nonzero return value. Else it take the semaphore and return zero value. */
  struct semaphore *pSema = ulHandle;
  return( down_trylock(pSema) );
}


void vibe_os_up_semaphore ( void * ulHandle )
{
  struct semaphore *pSema = ulHandle;
  up(pSema);
}

/* --------- */
/* | mutex | */
/* --------- */

void * __vibe_os_create_mutex(vibe_os_lock_class_key *key, const char *name)
{
  struct mutex *pMutex;

  /* Initialize the mutex to unlocked state.                  */
  /* It is not allowed to initialize an already locked mutex. */

  /* The CONFIG_DEBUG_MUTEXES .config option turns on debugging checks that will enforce the */
  /* restrictions.  CONFIG_DEBUG_PROVE_LOCKING does deadlock debugging.                      */

#ifdef CONFIG_LOCKDEP
  /* struct lock_class_key takes no space if lockdep is disabled */
  /* Is our local opaque version of lock_class_key big enough? */
  BUILD_BUG_ON((sizeof(struct lock_class_key) > sizeof(vibe_os_lock_class_key)));
#endif
  pMutex = kmalloc(sizeof(struct mutex),GFP_KERNEL);
  if (pMutex != NULL)
  {
    mutex_init(pMutex);
    lockdep_set_class_and_name(pMutex, (struct lock_class_key *)key, name);
  }
  return pMutex;
}


int vibe_os_is_locked_mutex ( void * ulHandle )
{
  /* Returns 1 if the mutex is locked, 0 if unlocked. */
  struct mutex *pMutex = ulHandle;
  return mutex_is_locked(pMutex);
}


void vibe_os_delete_mutex ( void * ulHandle )
{
  /* Release any resource allocated by mutex_init() and free allocated memory. */
  /* Must not be called if mutex is still locked.                              */
  struct mutex *pMutex = ulHandle;
  mutex_destroy(pMutex);

  kfree(pMutex);
}


void vibe_os_lock_mutex ( void * ulHandle )
{
  /* The mutex must, later on, be released by the same task that acquired it.                         */
  /* Recursive locking is not allowed.                                                                */
  /* The task may not exit without first unlocking the mutex.                                         */
  /* Also, kernel memory where the mutex resides mutex must not be freed with the mutex still locked. */
  struct mutex *pMutex = ulHandle;

  if ( vibe_os_in_interrupt() != 0)
  {
    TRC(TRC_ID_ERROR, "Mutex called from interrupt context!!!");
  }

  mutex_lock(pMutex);
}


int vibe_os_trylock_mutex ( void * ulHandle )
{
  /* Try to acquire the mutex atomically. Returns 1 if the mutex has been acquired successfully, */
  /* and 0 on contention. */

  /* This function must not be used in interrupt context.          */
  /* The mutex must be released by the same task that acquired it. */
  struct mutex *pMutex = ulHandle;
  return( mutex_trylock(pMutex) );
}


void vibe_os_unlock_mutex ( void * ulHandle )
{
  /* Unlock a mutex that has been locked by this task previously. */
  /* This function must not be used in interrupt context.         */
  /* Unlocking of a not locked mutex is not allowed.              */
  struct mutex *pMutex = ulHandle;
  mutex_unlock(pMutex);
}

/* ---------  */

bool vibe_os_of_property_read_bool(const char * propname)
{
    return of_property_read_bool(ios_device.of_node, propname);
}
int vibe_os_of_property_read_u32(const char * propname, uint32_t * propval)
{
    int val = 0;
    int err = 0;

    err = of_property_read_u32(ios_device.of_node, propname, &val);
    *propval = val;
    return err;
}

int vibe_os_of_property_read_string(const char * propname, const char** propval)
{
    return of_property_read_string(ios_device.of_node, propname, propval);
}

#if defined(CONFIG_FW_LOADER) || defined(CONFIG_FW_LOADER_MODULE)
static void firmware_free_data (struct kref *kref)
{
  STMFirmware_private * const fw = container_of(kref, STMFirmware_private, kref);

  TRC(TRC_ID_MAIN_INFO, "%s: No more firmwares users: free cache data",fw->name);
  /* free STM firmware data */
  kfree(fw->firmware.pData);
}

static void firmware_release (struct device * device)
{
  /* free firmware cache */
  struct firmware_data * const fwd = (struct firmware_data *)dev_get_drvdata(device);
  struct list_head *pos, *n;

  if(!fwd)
    return;

  if(!device_trylock(device))
    return;

  list_for_each_safe (pos, n, &fwd->fw_cache)
  {
    STMFirmware_private * const fw = list_entry (pos,
                                                 STMFirmware_private,
                                                 fw_list);

    /* Release all fw users and free all fw cache data
     * Be carefull this will need to set the initial number
     * of kref to 2 (requester and us) to make it valid at this time
     */
    if(WARN_ON(kref_put(&fw->kref, firmware_free_data) != 1))
      firmware_free_data(&fw->kref);

    dev_info(device, "%s: Free fw cache and container\n", fw->name);
    /* no users left */
    list_del_init(&fw->fw_list);

    kfree(fw);
  }

  /* Release device drvdata */
  list_del_init(&fwd->fw_cache);
  dev_set_drvdata(device, NULL);

  device_unlock(device);
}

/*
 * requst_firmware_nowait() callback function
 *
 * This function is called by the kernel when a firmware is made available,
 * or if it times out waiting for the firmware.
 */
static void firmware_load_data(const struct firmware *fw, void *context)
{
  STMFirmware_private *fwp = context;
  struct device *dev = &fwp->pdev->dev;

  dev_info(dev, "loading firmware data.\n");

  fwp->firmware.pData = 0;

  if (!fw)
  {
    dev_err(dev, "firmware not found\n");
    goto out;
  }

  /* get some memory for STM firmware data */
  fwp->firmware.pData = kzalloc (fw->size, GFP_KERNEL);
  if (!fwp->firmware.pData)
  {
    dev_err(dev, "could not allocate memory for firmware data\n");
    goto release_out;
  }

  /* copy data to requestor */
  memcpy((void *)fwp->firmware.pData, fw->data, fw->size);
  fwp->firmware.ulDataSize = fw->size;

  dev_info(dev, "firmware data is loaded (size=%d).\n", fw->size);

release_out:
  /* release the linux firmware */
  release_firmware(fw);
out:
  return;
}

static int CacheGetFirmware (const char                 * const name,
                             const STMFirmware_private **st_firmware_priv)
{
  struct list_head     *pos;
  struct firmware_data * const fwd = (struct firmware_data *)dev_get_drvdata(&ios_device);
  int                   error = -ENOENT;
  struct device_node *fw_node=NULL;
  const char *fw_name=NULL;

  if (!st_firmware_priv)
    return -EINVAL;
  if (*st_firmware_priv != NULL)
    return -EINVAL;

  device_lock(&ios_device);

  /* first check if we have it cached already */
  list_for_each (pos, &fwd->fw_cache)
  {
    STMFirmware_private * const fw = list_entry (pos,
                                                 STMFirmware_private,
                                                 fw_list);
    if (!strcmp (fw->name, name))
    {
      pr_info("%s: successfully loaded from cache\n", fw->name);
      /* yes - so use it */
      kref_get (&fw->kref);

      *st_firmware_priv = fw;
      error = 0;
      goto out;
    }
  }

  /* firmware not found in cache - so load it. Do so only if it is save
   * to potentially sleep - request_firmware() is very likely to sleep!
   * We could kzalloc(GFP_ATOMIC) and use request_firmware_nowait() to
   * solve this problem, though.
   */

  might_sleep ();
  if (in_atomic ()
      || irqs_disabled ())
  {
    /* not save to sleep */
    TRC(TRC_ID_ERROR, "firmware '%s' not in cache and refusing to load",
            name);
  }
  else
  {
    /* save to sleep - continue loading */
    STMFirmware_private *fw;

    /* get some memory */
    fw = kzalloc (sizeof (*fw), GFP_KERNEL);
    if (!fw)
    {
      error = -ENOMEM;
      goto out;
    }

    fw->pdev = platform_device_register_data(&ios_device, name, -1, NULL, 0);
    if (IS_ERR(fw->pdev))
    {
      kfree(fw);
      pr_err("Failed to register firmware platform device.\n");
      error = -EINVAL;
      goto out;
    }

    /* request linux firmware */
    dev_info(&fw->pdev->dev, "requesting frm: '%s', %p/%p\n", name, &ios_device, &fw->firmware);
    fw_node = of_get_child_by_name(ios_device.of_node, "firmwares");
    if(IS_ERR_OR_NULL(fw_node))
    {
      dev_err(&fw->pdev->dev, "firmwares DT settings are missing\n");
      error = -ENOENT;
    }
    else
    {
      error = of_property_read_string(fw_node, name, &fw_name);
      if (likely (!error))
      {
        dev_info(&fw->pdev->dev, "using %s for %s firmware\n", fw_name, name);
        error = request_firmware (&fw->lnx_firmware, fw_name, &fw->pdev->dev);
      }
      of_node_put(fw_node);
    }
    if (likely (error))
    {
      platform_device_unregister(fw->pdev);
      kfree (fw);
      goto out;
    }

    firmware_load_data(fw->lnx_firmware, fw);
    if(!fw->firmware.pData)
    {
      error = -ENOENT;
      platform_device_unregister(fw->pdev);
      kfree (fw);
      goto out;
    }

    dev_info(&fw->pdev->dev, "frm '%s' successfully loaded\n", name);

    /* have the firmware */
    *st_firmware_priv = fw;

    /* remember fw data */
    strncpy (fw->name, name, FIRMWARE_NAME_MAX);
    fw->name[FIRMWARE_NAME_MAX] = '\0';

    /* Initialize the object users */
    kref_init (&fw->kref);
    /* Register ourself to be able to unload data at the end */
    kref_get (&fw->kref);

    /* add to cache */
    list_add_tail (&fw->fw_list, &fwd->fw_cache);

    platform_device_unregister(fw->pdev);
  }

out:
  if (unlikely (error))
    TRC(TRC_ID_ERROR, "loading frm: '%s' failed: %d", name, error);

  device_unlock(&ios_device);
  return error;
}
#endif

int vibe_os_request_firmware (const STMFirmware **firmware_p,
                            const char         * const name)
{
#if defined(CONFIG_FW_LOADER) || defined(CONFIG_FW_LOADER_MODULE)
  int                        error;
  const STMFirmware_private *st_firmware_priv = NULL;

  if(DISABLE_FIRMWARE_LOADING)
  {
    return -ENOENT;
  }

  if (unlikely (!firmware_p
                || *firmware_p
                || !name
                || !*name))
  {
    TRC(TRC_ID_ERROR, "vibe_os_request_firmware: error in parameters");
    return -EINVAL;
  }

  if (unlikely (!device_is_registered(&ios_device)))
    return -ENODEV;

  error = CacheGetFirmware (name, &st_firmware_priv);
  if (likely (!error))
    *firmware_p = &st_firmware_priv->firmware;

  return error;
#else
# warning No firmware loading support!
  TRC(TRC_ID_ERROR, "Your kernel was compiled without firmware loading"
          "support!");
  return -ENOENT;
#endif
}

void vibe_os_release_firmware (const STMFirmware *fw_pub)
{
#if defined(CONFIG_FW_LOADER) || defined(CONFIG_FW_LOADER_MODULE)
  struct list_head *pos, *n;
  struct firmware_data * const fwd = (struct firmware_data *)dev_get_drvdata(&ios_device);

  STMFirmware_private * const fw = container_of (fw_pub,
                                                 STMFirmware_private,
                                                 firmware);

  if (fw_pub == NULL)
  {
    TRC(TRC_ID_ERROR, "vibe_os_release_firmware: error: fw_pub==NULL");
    return;
  }

  /* first check if we have it cached already */
  list_for_each_safe (pos, n, &fwd->fw_cache)
  {
    STMFirmware_private * const fw_loc = list_entry (pos,
                                                 STMFirmware_private,
                                                 fw_list);
    if (!strcmp (fw->name, fw_loc->name))
    {
      /* yes - free it */
      kref_put(&fw_loc->kref, firmware_free_data);
      pr_info("%s: successfully removed from cache list\n", fw->name);
    }
  }
#endif
}

int
__attribute__ ((format (printf, 3, 4)))
vibe_os_snprintf(char * buf, uint32_t size, const char * fmt, ...)

{
  va_list args;
  int i;

  va_start(args, fmt);
  i=vsnprintf(buf,size,fmt,args);
  va_end(args);
  return i;
}

uint32_t vibe_os_call_bios (long call, unsigned int n_args, ...)
{
#ifdef HAVE_BIOS
  va_list va_args;
  uint32_t retval;
  int32_t args[n_args];

  va_start (va_args, n_args);
  for (retval = 0; retval < n_args; ++retval)
    args[retval] = va_arg (va_args, int32_t);
  va_end (va_args);

  switch (n_args)
    {
    case 0:
      retval = INLINE_BIOS (call, 0);
      break;
    case 1:
      retval = INLINE_BIOS (call, 1, args[0]);
      break;
    case 2:
      retval = INLINE_BIOS (call, 2, args[0], args[1]);
      break;
    case 3:
      retval = INLINE_BIOS (call, 3, args[0], args[1], args[2]);
      break;
    case 4:
      retval = INLINE_BIOS (call, 4, args[0], args[1], args[2], args[3]);
      break;
    case 5:
      retval = INLINE_BIOS (call, 5, args[0], args[1], args[2], args[3], args[4]);
      break;
    case 6:
      retval = INLINE_BIOS (call, 6, args[0], args[1], args[2], args[3], args[4], args[5]);
      break;

    default:
      retval = 0 /* BIOS_FAIL */;
      break;
    }

  return retval;
#else /* HAVE_BIOS */
  return 0; /* BIOS_FAIL */
#endif /* HAVE_BIOS */
}

/* Function reducing the numerator and denominator (without lost) */
void vibe_os_rational_reduction(uint64_t *pNum, uint64_t *pDen)
{
  if(*pNum == *pDen)
  {
    *pNum = 1;
    *pDen = 1;
  }
  else
  {
      // Reduction by multiple of 2
      while( (*pNum >= 2)              &&
             (*pDen >= 2)              &&
             ( (*pNum & 0x01ULL) == 0) &&
             ( (*pDen & 0x01ULL) == 0) )
      {
        *pNum = *pNum >> 1;
        *pDen = *pDen >> 1;
      }
  }
}

uint64_t vibe_os_div64(uint64_t num, uint64_t den)
{
  if (unlikely(den & 0xffffffff00000000ULL))
  {
    // Perform a loss less reduction of this rational number
    // In the past, div64_u64 was not accurate when denominator was > 0xFFFFFFFF.
    // So the intent of this reduction was to try to have denominator <= 0xFFFFFFFF.
    // Today this is not really required since div64_u64 is now accurate.
    vibe_os_rational_reduction(&num, &den);
  }

  return div64_u64(num, den);
}

/******************************************************************************
 *  Memory barriers stuff
 */
void vibe_os_write_memory_barrier()
{
    wmb();
}

void vibe_os_read_memory_barrier()
{
    rmb();
}

/******************************************************************************
 *  Work queue implementation
 */

struct VIBE_OS_Worker_s
{
    struct work_struct    work;
    VIBE_OS_WorkerFct_t   function;
    void                 *data;
};

VIBE_OS_WorkQueue_t vibe_os_create_work_queue(const char *name)
{
    struct workqueue_struct *workqueue;

    // Create a single thread and its queue of work/message
    workqueue = create_singlethread_workqueue(name);

    return(workqueue);
}

void vibe_os_destroy_work_queue(VIBE_OS_WorkQueue_t workqueue)
{

    flush_workqueue((struct workqueue_struct *)workqueue);

    destroy_workqueue((struct workqueue_struct *)workqueue);
}

void common_work_function(struct work_struct *my_work)
{
    struct VIBE_OS_Worker_s *my_worker = container_of(my_work, struct VIBE_OS_Worker_s, work);

    (*(my_worker->function))(&(my_worker->data));
}

void* vibe_os_allocate_workerdata(VIBE_OS_WorkerFct_t function, int data_size)
{
    struct VIBE_OS_Worker_s *worker;

    // allocate worker + data size
    worker = kmalloc(sizeof(struct VIBE_OS_Worker_s) - sizeof(void *) + data_size, GFP_KERNEL);
    if(!worker)
    {
        return 0;
    }

    // INIT_WORK calls PREPARE_WORK
    INIT_WORK(&(worker->work), common_work_function);

    worker->function = function;

    return ((void *)(((char *)worker) + offsetof(struct VIBE_OS_Worker_s, data)));
}

void vibe_os_delete_workerdata(void* data)
{
    struct VIBE_OS_Worker_s *my_worker = (struct VIBE_OS_Worker_s *) (((char *)data) - offsetof(struct VIBE_OS_Worker_s, data));

    kfree(my_worker);
}

bool vibe_os_queue_workerdata(VIBE_OS_WorkQueue_t workqueue, void* data)
{
    int result;

    struct VIBE_OS_Worker_s *my_worker = (struct VIBE_OS_Worker_s *) (((char *)data) - offsetof(struct VIBE_OS_Worker_s, data));

    result = queue_work((struct workqueue_struct *)workqueue, &(my_worker->work));
    if(!result)
    {
        // 0 means work already on a queueu, non-zero otherwise
        return false;
    }

    return true;
}

/******************************************************************************
 *  Message thread implementation
 */

struct VIBE_OS_MessageThread_s
{
    struct task_struct *thread;             /* message thread structure */

    wait_queue_head_t   wait_queue;         /* wait queue for blocking message thread: waiting new message arrival */
    bool                wait_queue_cond;    /* wait queue condition (set at true before waking up message thread)  */

    struct list_head    msg_list;           /* message list header                                                 */
    spinlock_t          msg_lock;           /* lock for inserting/removing message to/from message list header     */
};

struct VIBE_OS_Message_s
{
    struct list_head            list;       /* for linking message together                            */
    VIBE_OS_MessageCallback_t   callback;   /* message callback                                        */
    void                       *data;       /* message data to be provided to the message callback     */
};

int common_msg_thread_function(void *data)
{
    struct VIBE_OS_MessageThread_s *thread_ctx = (struct VIBE_OS_MessageThread_s *)data;
    struct VIBE_OS_Message_s       *msg_ctx;
    unsigned long                   flags;

    while(1)
    {
        /* wait message(s) to be posted to message thread */
        if(wait_event_interruptible(thread_ctx->wait_queue,
                                    (kthread_should_stop() ||
                                    thread_ctx->wait_queue_cond )
                                   ) !=0)
        {
            TRC(TRC_ID_MAIN_INFO, "%d",__LINE__);
            return -ERESTARTSYS;
        }

        /* reset wait queue condition */
        thread_ctx->wait_queue_cond = false;

        /* check if message thread should be stopped */
        if(kthread_should_stop())
        {
            return 0;
        }

        /* treat all pending message until queue is empty */
        while(1)
        {
            spin_lock_irqsave(&(thread_ctx->msg_lock), flags);
            if(!list_empty(&(thread_ctx->msg_list)))
            {
                /* extract first message pushed in the queue */
                msg_ctx = list_first_entry(&(thread_ctx->msg_list), struct VIBE_OS_Message_s, list);
                list_del_init(&(msg_ctx->list));
                spin_unlock_irqrestore(&(thread_ctx->msg_lock), flags);

                /* call message callback with its data parameter */
                (*(msg_ctx->callback))(&(msg_ctx->data));
            }
            else
            {
                /* no more message in the queue */
                spin_unlock_irqrestore(&(thread_ctx->msg_lock), flags);
                break;
            }
        }
    }

    return 0;
}

VIBE_OS_MessageThread_t vibe_os_create_message_thread(const char *name, const int *thread_settings)
{
    struct VIBE_OS_MessageThread_s *thread_ctx;
    int                             thread_sched_policy;
    struct sched_param              thread_sched_param;

    /* check input parameters */
    if((name==NULL) || (thread_settings==NULL))
    {
        TRC(TRC_ID_ERROR, "FAILED, invalid input parameters!");
        goto exit_alloc_fail;
    }

    /* allocate message thread */
    thread_ctx = kmalloc(sizeof(struct VIBE_OS_MessageThread_s), GFP_KERNEL);
    if(!thread_ctx)
    {
        TRC(TRC_ID_ERROR, "Failed to allocate message thread!");
        goto exit_alloc_fail;
    }

    /* initialize wait queue */
    init_waitqueue_head(&(thread_ctx->wait_queue));
    thread_ctx->wait_queue_cond = false;

    /* initialize message list header */
    INIT_LIST_HEAD(&(thread_ctx->msg_list));

    /* initialize message lock */
    spin_lock_init(&(thread_ctx->msg_lock));

    /* create message thread without waking up */
    thread_ctx->thread = kthread_create(common_msg_thread_function, thread_ctx, name);
    if(IS_ERR(thread_ctx->thread))
    {
        TRC(TRC_ID_ERROR, "Failed to create message thread");
        goto exit_create_thread_fail;
    }

    /* Set thread scheduling settings */
    thread_sched_policy               = thread_settings[0];
    thread_sched_param.sched_priority = thread_settings[1];
    if( sched_setscheduler(thread_ctx->thread, thread_sched_policy, &thread_sched_param) )
    {
        TRC(TRC_ID_ERROR, "FAILED to set thread scheduling parameters: name=%s, policy=%d, priority=%d", \
            name, thread_sched_policy, thread_sched_param.sched_priority);
    }

    /* wake up message thread */
    wake_up_process(thread_ctx->thread);

    return((VIBE_OS_MessageThread_t)thread_ctx);

exit_create_thread_fail:
    kfree(thread_ctx);

exit_alloc_fail:
    return 0;
}

void vibe_os_destroy_message_thread(VIBE_OS_MessageThread_t message_thread)
{
    unsigned long flags;
    struct VIBE_OS_MessageThread_s *thread_ctx = (struct VIBE_OS_MessageThread_s *)message_thread;

    /* check message thread validity */
    if(!thread_ctx || !thread_ctx->thread)
    {
        TRC(TRC_ID_ERROR, "Failed, invalid message thread");
        return;
    }

    /* stop and wait message thread ending */
    if(kthread_stop(thread_ctx->thread))
    {
        TRC(TRC_ID_ERROR, "Failed to stop message thread");
        return;
    }

    /* check message queue is empty */
    spin_lock_irqsave(&(thread_ctx->msg_lock), flags);
    if(!list_empty(&(thread_ctx->msg_list)))
    {
        spin_unlock_irqrestore(&(thread_ctx->msg_lock), flags);
        TRC(TRC_ID_ERROR, "Failed message list is not empty");
        return;
    }
    spin_unlock_irqrestore(&(thread_ctx->msg_lock), flags);

    /* delete message thread */
    thread_ctx->thread = 0;
    kfree(thread_ctx);
}

void* vibe_os_allocate_message(VIBE_OS_MessageCallback_t callback, int data_size)
{
    struct VIBE_OS_Message_s *msg_ctx;

    /* check message validity */
    if(!callback)
    {
        TRC(TRC_ID_ERROR, "Failed, invalid callback for this message");
        return 0;
    }

    /* allocate message + data size */
    msg_ctx = kmalloc(sizeof(struct VIBE_OS_Message_s) - sizeof(void *) + data_size, GFP_KERNEL);
    if(!msg_ctx)
    {
        TRC(TRC_ID_ERROR, "Failed, cannot allocate this message");
        return 0;
    }

    /* fill message with callback function to call */
    msg_ctx->callback = callback;

    /* initialize linked list */
    INIT_LIST_HEAD(&(msg_ctx->list));

    /* let user filling the data */
    return ((void *)(((char*)msg_ctx) + offsetof(struct VIBE_OS_Message_s, data)));
}

void vibe_os_delete_message(void* data)
{
    struct VIBE_OS_Message_s *msg_ctx = (struct VIBE_OS_Message_s *) (((char *)data) - offsetof(struct VIBE_OS_Message_s, data));

    /* check message validity */
    if(!msg_ctx->callback)
    {
        TRC(TRC_ID_ERROR, "Failed, message already deleted");
        return;
    }

    /* delete the message */
    msg_ctx->callback = 0;
    kfree(msg_ctx);
}

bool vibe_os_queue_message(VIBE_OS_MessageThread_t message_thread, void* data)
{
    unsigned long flags;

    struct VIBE_OS_MessageThread_s *thread_ctx = (struct VIBE_OS_MessageThread_s *)message_thread;
    struct VIBE_OS_Message_s        *msg_ctx   = (struct VIBE_OS_Message_s *) (((char *)data) - offsetof(struct VIBE_OS_Message_s, data));

    /* Put message in the queue */
    spin_lock_irqsave(&(thread_ctx->msg_lock), flags);
    list_add_tail(&(msg_ctx->list), &(thread_ctx->msg_list) );
    spin_unlock_irqrestore(&(thread_ctx->msg_lock), flags);

    /* Wakeup message thread */
    thread_ctx->wait_queue_cond = true;
    wake_up_interruptible(&(thread_ctx->wait_queue));

    return true;
}

/******************************************************************************
 *  Wait queue implementation
 */

struct VIBE_OS_WaitQueue_s
{
  wait_queue_head_t  wait_queue;
};


int vibe_os_allocate_queue_event(VIBE_OS_WaitQueue_t *queue)
{
  // allocate wait queue
  *queue = kmalloc(sizeof(struct VIBE_OS_WaitQueue_s), GFP_KERNEL);
  if(*queue == NULL)
  {
    return -ENOMEM;
  }

  init_waitqueue_head(&((*queue)->wait_queue));

  return 0;
}

void vibe_os_release_queue_event(VIBE_OS_WaitQueue_t queue)
{
  kfree(queue);
  queue = 0;
}

int vibe_os_wait_queue_event(VIBE_OS_WaitQueue_t queue,
                        volatile uint32_t* var, uint32_t value,
                        int cond, unsigned int timeout_ms)
{
  if(!var)
  {
    /* Unconditional sleep */
    interruptible_sleep_on(&queue->wait_queue);
    if(signal_pending(current))
    {
      return -ERESTARTSYS;
    }
  }
  else
  {
    /* Two conditions, wait either until *var == value or *var != value */
    if(timeout_ms == STMIOS_WAIT_TIME_FOREVER)
    {
      return wait_event_interruptible(queue->wait_queue,(cond?(*var == value):(*var != value)));
    }
    else
    {
      /* convert timeout from millisconds to jiffies value */
      uint32_t time_jiffies = (HZ * timeout_ms)/1000;
      return wait_event_interruptible_timeout(queue->wait_queue,(cond?(*var == value):(*var != value)), time_jiffies);
    }
  }
  return 0;
}

int vibe_os_wait_event(VIBE_OS_WaitQueue_t        queue,
                       VIBE_OS_WaitQueueCondFct_t cond,
                       void*                      data,
                       unsigned int               timeout_ms)
{
    if(timeout_ms == STMIOS_WAIT_TIME_FOREVER)
    {
      wait_event( queue->wait_queue, cond(data) );
      return 0;
    }
    else
    {
      /* convert timeout from millisconds to jiffies value */
      uint32_t time_jiffies = (HZ * timeout_ms)/1000;
      /* return 0 if timeout, else remaining times when condition appears */
      return wait_event_timeout(queue->wait_queue, cond(data), time_jiffies);
    }
}

void vibe_os_wake_up(VIBE_OS_WaitQueue_t queue)
{
  wake_up(&(queue->wait_queue));
}


void vibe_os_wake_up_queue_event(VIBE_OS_WaitQueue_t queue)
{
  wake_up_interruptible(&(queue->wait_queue));
}

void * vibe_os_get_wait_queue_data(VIBE_OS_WaitQueue_t queue)
{
  void *data = &(queue->wait_queue);
  return data;
}

/******************************************************************************
 *  Enable/Disable interrupt implementation
 */
void vibe_os_enable_irq(unsigned int irq)
{
    enable_irq(irq);
}

void vibe_os_disable_irq_nosync(unsigned int irq)
{
    disable_irq_nosync(irq);
}

void vibe_os_disable_irq(unsigned int irq)
{
    disable_irq(irq);
}

int vibe_os_in_interrupt(void)
{
    // Returns nonzero if in interrupt context and zero if in process context
    return( in_interrupt() );
}

/******************************************************************************
 *  Gpio access implementation
 */
int32_t vibe_os_gpio_get_value(uint32_t gpio)
{
    return gpio_get_value(gpio);
}

/******************************************************************************
 *  clock LLA implementation
 */

int vibe_os_clk_get(const char* name, struct vibe_clk *clock)
{
  struct clk *clk;

  if (!name)
    return -EINVAL;

  clk = devm_clk_get(&ios_device, name);
  if(IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Failed, unable to get clock %s\n",__func__,name);
    return PTR_ERR(clk);
  }

  clock->clk = clk;
  clock->enabled = false;
  strncpy(clock->name, name, VIBE_OS_CLK_MAX_NAME_LENGTH);
  clock->name[VIBE_OS_CLK_MAX_NAME_LENGTH-1]='\0';
  return 0;
}

int vibe_os_clk_put(struct vibe_clk *clock)
{
  struct clk *clk = (struct clk *) clock->clk;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  devm_clk_put(&ios_device, clk);

  clock->clk = 0;
  clock->enabled = false;
  clock->name[0] = '\0';
  return 0;
}

int vibe_os_clk_enable(struct vibe_clk *clock)
{
  struct clk *clk = (struct clk *) clock->clk;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  if(clock->enabled)
  {
    dev_err (&ios_device, "%s: Clock already enable %s\n",__func__,clock->name);
    return -EALREADY;
  }

  if(clk_prepare_enable(clk))
  {
    dev_err (&ios_device, "%s: Failed, unable to enable clock %s\n",__func__,clock->name);
    return -EBUSY;
  }

  clock->enabled = true;
  return 0;
}

int vibe_os_clk_disable(struct vibe_clk *clock)
{
  struct clk *clk = (struct clk *) clock->clk;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  if (!clock->enabled)
  {
    dev_err (&ios_device, "%s: Clock already disable %s\n",__func__,clock->name);
    return -EALREADY;
  }

  clk_disable_unprepare(clk);
  clock->enabled = false;
  return 0;
}

int vibe_os_clk_resume(struct vibe_clk *clock)
{
  struct clk *clk = (struct clk *) clock->clk;

  if (IS_ERR_OR_NULL(clk))
  {
   dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
   return -EINVAL;
  }

  /* Nothing to do if already disabled */
  if (!clock->enabled)
    return 0;

  dev_dbg (&ios_device, "%s: Enabling clock %s\n", __func__,clock->name);
  if (clk_prepare_enable(clk))
  {
    dev_err (&ios_device, "%s: Failed, unable to enable clock %s\n",__func__, clock->name);
    return -EBUSY;
  }

  return 0;
}

int vibe_os_clk_suspend(struct vibe_clk *clock)
{
  struct clk *clk = (struct clk *) clock->clk;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  /* Nothing to do if already disabled */
  if(!clock->enabled)
    return 0;

  dev_dbg (&ios_device, "%s: suspending clock %s\n", __func__, clock->name);
  clk_disable_unprepare(clk);

  return 0;
}

int vibe_os_clk_set_parent(struct vibe_clk *clock, struct vibe_clk *parent)
{
  struct clk *clk = (struct clk *) clock->clk;
  struct clk *pclk = (struct clk *) parent->clk;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  if (IS_ERR_OR_NULL(pclk))
  {
    dev_err (&ios_device, "%s: Invalid parent clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  if(clk_set_parent(clk, pclk))
  {
    dev_err (&ios_device, "%s: Failed, unable to set parent for clock %s\n",__func__, clock->name);
    return -EINVAL;
  }

  return 0;
}

int vibe_os_clk_get_parent(struct vibe_clk *clock, struct vibe_clk *parent)
{
  struct clk *clk = (struct clk *) clock->clk;
  struct clk *pclk = NULL;

  /* Reset parent struct vibe_clk */
  parent->name[0] = '\0';
  parent->clk = 0;
  parent->enabled = false;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  pclk = clk_get_parent(clk);
  if (IS_ERR_OR_NULL(pclk))
  {
    dev_err (&ios_device, "%s: Failed, unable to get parent %s\n",__func__,clock->name);
    return -EINVAL;
  }

  parent->clk = pclk;
  snprintf(parent->name, sizeof(parent->name), pclk->name);
  parent->name[sizeof(parent->name)-1]='\0';
  return 0;
}

int vibe_os_clk_set_rate(struct vibe_clk *clock, unsigned long rate)
{
  int error = 0;
  struct clk *clk = (struct clk *) clock->clk;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  if (clk_set_rate(clk, rate))
  {
    dev_err (&ios_device, "%s: Failed, unable to set rate for clock %s\n",__func__, clock->name);
    error = -EINVAL;
  }

  return error;
}

unsigned long vibe_os_clk_get_rate(struct vibe_clk *clock)
{
  unsigned long  rate = 0;
  struct clk *clk = (struct clk *) clock->clk;

  if (IS_ERR_OR_NULL(clk))
  {
    dev_err (&ios_device, "%s: Invalid clock reference %s\n",__func__,clock->name);
    return -EINVAL;
  }

  if (in_interrupt())
    rate = ((clk->new_rate > 0)&&(clk->new_rate != clk->rate))?clk->new_rate:clk->rate;
  else
    rate = clk_get_rate(clk);

  return rate;
}

/******************************************************************************
*  Chip version
 */

bool vibe_os_get_chip_version(uint32_t *major, uint32_t *minor)
{
    uint32_t maj, min;

    /* Check input parameters valid */
    if((major==NULL) || (minor==NULL))
    {
        return false;
    }

    maj = st_socinfo_get_version_major();
    min = st_socinfo_get_version_minor();

    *major = maj;
    *minor = min;

    return true;
}

/******************************************************************************
 *  Code for asynchronous Prints
 */

#ifdef CONFIG_DISPLAY_ASYNC_TRACES

static bool start_async_print_thread(void)
{
  VIBE_OS_thread_settings thread_settings;

  /* Create a thread and a wait_queue for asynchronous prints */
  vibe_os_allocate_queue_event(&async_trace.wait_queue);
  if (!async_trace.wait_queue)
  {
    printk(KERN_ERR "%s: Failed to create async_wait_queue!\n",__FUNCTION__);
    return false;
  }

  /* Reset the variables used for asynchronous prints */
  async_trace.print_trace_lost  = false;
  async_trace.print_read_index  = 0;
  async_trace.print_write_index = 0;

  spin_lock_init(&(async_trace.lock));

  /* This thread will be waken up when there is something new to print asynchronously */
  thread_settings.policy   = thread_vib_asyncprint[0];
  thread_settings.priority = thread_vib_asyncprint[1];
  if (!vibe_os_create_thread((VIBE_OS_ThreadFct_t) async_print_thread,
                                   NULL,
                                   "VIB-AsyncPrint",
                                   &thread_settings,
                                   &async_trace.print_thread_desc))
  {
    dev_err (&ios_device, "Failed to create VIB-AsyncPrint thread");
    return false;
  }

  vibe_os_wake_up_thread(async_trace.print_thread_desc);
  return true;
}

static bool stop_async_print_thread(void)
{
  /* Release the thread used for asynchronous prints */
  if (async_trace.print_thread_desc)
  {
    vibe_os_stop_thread(async_trace.print_thread_desc);
    async_trace.print_thread_desc = 0;
  }

  return true;
}

static bool async_buffer_write_data(const char *fmt, va_list args)
{
  async_print_slot_t  *pSlot;
  uint32_t             next_index;
  char                 text_to_print[ASYNC_PRINT_MAX_STRING_LENGTH];
  unsigned long        flags = 0;

  /* Retrieve the text to print and expand the arguments */
  vsnprintf (text_to_print, sizeof(text_to_print), fmt, args);

  /* Ensure print_read_index, print_write_index are not modified while writing in the slot */
  spin_lock_irqsave(&(async_trace.lock), flags);

  pSlot = &(async_trace.slots[async_trace.print_write_index]);

  /* We're now going to increment the write index */
  next_index = (async_trace.print_write_index + 1) % ASYNC_PRINT_SLOTS_NBR;

  if (next_index == async_trace.print_read_index)
  {
    if (!(async_trace.print_trace_lost))
    {
      async_trace.print_trace_lost = true;
      /* The async buffer is full, the write_index is not incremented */
      snprintf(pSlot->text, sizeof(pSlot->text), "=== WARNING! Some asynchronous traces have been lost! ===\n");
    }
  }
  else
  {
    if (async_trace.print_trace_lost)
    {
      /* Do not overwrite the error message already written in the buffer */
      async_trace.print_trace_lost = false;
    }
    else
    {
      /* Amend a prefix showing that it is an asynchronous print */
      snprintf(pSlot->text, sizeof(pSlot->text), "ASYNC: %s", text_to_print);

      /* The string may have been truncated. Ensure that it ends with \n\0 */
      pSlot->text[ASYNC_PRINT_MAX_STRING_LENGTH-2] = '\n';
      pSlot->text[ASYNC_PRINT_MAX_STRING_LENGTH-1] = '\0';
    }

    async_trace.print_write_index = next_index;
  }

  /* Ensure print_read_index, print_write_index are not modified while writing in the slot */
  spin_unlock_irqrestore(&(async_trace.lock), flags);

  /* Inform the thread that a new trace is available */
  vibe_os_wake_up_queue_event(async_trace.wait_queue);

  return true;
}


static bool async_buffer_write_data_var(const char *fmt,...)
{
    va_list ap;
    bool val;

    va_start(ap, fmt);
    val = async_buffer_write_data(fmt, ap);
    va_end(ap);

    return val;
}


static bool async_read_slot(char *pText)
{
    unsigned long flags = 0;

  /* Ensure print_read_index, print_write_index are not modified while writing in the slot */
  spin_lock_irqsave(&(async_trace.lock), flags);

  if (async_trace.print_write_index == async_trace.print_read_index)
  {
    /* The buffer is empty */
    spin_unlock_irqrestore(&(async_trace.lock), flags);
    return false;
  }
  else
  {
    memcpy(pText, async_trace.slots[async_trace.print_read_index].text, ASYNC_PRINT_MAX_STRING_LENGTH);

    /* Increment the read index */
    async_trace.print_read_index = (async_trace.print_read_index + 1) % ASYNC_PRINT_SLOTS_NBR;

    /* Ensure print_read_index, print_write_index are not modified while writing in the slot */
    spin_unlock_irqrestore(&(async_trace.lock), flags);

    return true;
  }
}

static void async_print_thread(void *data)
{
  char          text[ASYNC_PRINT_MAX_STRING_LENGTH];

  while (!vibe_os_thread_should_stop())
  {
    /* Wait for something new to print */
    vibe_os_wait_queue_event(async_trace.wait_queue, 0, 0, 0, 0);

    while (async_read_slot(text))
    {
      printk(text);
    }
  }

  /* "wait_queue" is not needed anymore */
  if (async_trace.wait_queue)
  {
    vibe_os_release_queue_event(async_trace.wait_queue);
    async_trace.wait_queue = 0;
  }
}

#endif

/******************************************************************************
 *  IDebug implementation
 */

#define TRC_OUTPUT_MASK ( (1<<TRC_ID_BY_CONSOLE) | (1<<TRC_ID_BY_KPTRACE) | (1<<TRC_ID_BY_STM) )

unsigned int trcmask[VIBE_OS_TRCMASK_SIZE] = {0};


#if defined(CONFIG_DISPLAY_REMOVE_TRACES)


int  vibe_debug_init(void) { return 0; }

void vibe_debug_release(void) { }

void vibe_break(const char *cond, const char *file, unsigned int line) { BUG(); }

void vibe_printf(const char *fmt,...) { }

void vibe_kpprintf(const char *fmt,...) { }

void vibe_trace( int id, char c, const char *pf, const char *fmt, ... ) { }


#else /* CONFIG_DISPLAY_REMOVE_TRACES */


#define __SET_TRC( id )                                          \
  do {                                                           \
        if(id < TRC_ID_LAST)                                     \
        {                                                        \
            trcmask[id/32] |= (1 << (id % 32));                  \
        }                                                        \
  } while ( 0 )


#if defined(CONFIG_DEBUG_FS)

#define xconcat(...)  # __VA_ARGS__
#define sconcat(a) xconcat(a)

// trc_id_names_string is set with a string that contains all the TRC_IDs defined in TRC_ID_ALL
// Note:
// The preprocessor is replacing comments (/*....*/) with one space so
// trc_id_names really contains only the names defined in TRC_ID_ALL, comas and
// blanks.
//
// During parsing (in vibe_trace_create_debugfs), some characters in
// trc_id_names_string are replaced with '\0' to isolate the different trc_ids.
// In the same time trc_id_names is initialized to point to the beginning of
// each identifier and actual length of TRC_ID_* are computed.
static char * trc_id_names_string = sconcat(TRC_ID_ALL);
static struct {
    unsigned   len;
    char     * name;
} trc_id_names[TRC_ID_LAST] = {{0, NULL}};

static ssize_t get_trace_levels(struct file *file, char __user *user_buf, size_t count, loff_t *ppos)
{
  int i = 0;
  ssize_t ret = 0;
#define OUT_BUF_MAX_LEN 512
  char out_buf[OUT_BUF_MAX_LEN];
  int out_buf_size = 0;

  memset(out_buf, 0, sizeof(out_buf));

  // Print trace groups one by one
  for (i = 0 ; i < VIBE_OS_TRCMASK_SIZE && out_buf_size<OUT_BUF_MAX_LEN; i++)
  {
	  out_buf_size += snprintf(&out_buf[out_buf_size], OUT_BUF_MAX_LEN-out_buf_size, "%u ", trcmask[i]);
  }

  out_buf[out_buf_size-1] = '\n'; // Overwrite the last space
  ret = simple_read_from_buffer(user_buf, count, ppos, out_buf, out_buf_size);

  return ret;
}

static ssize_t set_trace_levels(struct file *file, const char *user_buf, size_t count, loff_t *ppos)
{
  int i = 0;
  char in_buf[512];
  char *buf;
  char *token;
  unsigned int val;

  memset(in_buf, '\0', sizeof(in_buf));

  if (simple_write_to_buffer(in_buf, sizeof(in_buf), ppos, user_buf, count) != count)
    return count;

  buf = in_buf; /* since strsep modifies its pointer arg */
  for (token = strsep(&buf, " "); token != NULL; token = strsep(&buf, " "))
  {
    if (kstrtouint(token, 0, &val) != 0)
      break;

    if (i < VIBE_OS_TRCMASK_SIZE)
      trcmask[i++] = val;

    if (i == VIBE_OS_TRCMASK_SIZE)
      break;
  }

  while((i > 0) && (i < VIBE_OS_TRCMASK_SIZE))
  {
    trcmask[i] = 0;
    i++;
  }

  return count;
}

static ssize_t get_trace_setlevels(struct file *file, char *user_buf, size_t count, loff_t *ppos)
{
    /* For this file, we consider ppos as a bit position
     *
     * The implementation allows to read the file in several "read"
     * operations. This might be required if user_buf is small and cannot contain
     * all the TRC_IDs.
     *
     * For example if all traces are set and count is only 100 characters,
     * the first read operation will provide:
     * TRC_ID_BY_CONSOLE
     * TRC_ID_BY_KPTRACE
     * TRC_ID_BY_STM
     * TRC_ID_ERROR
     * TRC_ID_MAIN_INFO
     * Then the second read operation will provide:
     * TRC_ID_UNCLASSIFIED
     * TRC_ID_QUEUE_BUFFER
     * TRC_ID_PICT_QUEUE_RELEASE
     * TRC_ID_PICTURE_SCHEDULING
     * and so on ...
     */
    unsigned user_count = 0;
    loff_t pos = *ppos;
    unsigned len;

    if (pos >= TRC_ID_LAST) return 0;

    len = trc_id_names[pos].len;
    /* In the line below first +1 is for '\n', second +1 is for '\0' */
    while (user_count+len+1+1 < count  &&  pos < TRC_ID_LAST)
    {
        if ( (trcmask[pos/32] & (1 << (pos % 32))) != 0) {
            if (copy_to_user(user_buf+user_count, trc_id_names[pos].name, len)) return -EFAULT;
            user_count = user_count+len;
            if (copy_to_user(user_buf+user_count, "\n", 1)) return -EFAULT;
            user_count ++;
        }
        pos ++;
        if (pos < TRC_ID_LAST)
          len = trc_id_names[pos].len;
    }
    if (copy_to_user(user_buf+user_count, "\0", 1)) return -EFAULT;
    *ppos = pos;
    return user_count;
}
static ssize_t set_trace_setlevels(struct file *file, const char *user_buf, size_t count, loff_t *ppos)
{
  /* We assume that user_buf contains the following: "TRC_ID_*"
   * We are only parsing the first TRC_ID (we are not parsing multiple TRC_IDs
   * e.g. user_buf = "TRC_ID_MAIN_INFO TRC_ID_MIXER").
   */
  int i = 0;
  char input_ID[512];

  memset(input_ID, '\0', sizeof(input_ID));

  if (simple_write_to_buffer(input_ID, sizeof(input_ID), ppos, user_buf, count) != count)
    return count;

  input_ID[count-1] = '\0';
  for (i = 0; i < TRC_ID_LAST; i ++)
  {
    if (strcmp(input_ID, trc_id_names[i].name) == 0)
    {
      /* 'i' is a bit position in an array of uint32_t so we need a little bit of arithmetic */
      trcmask[i/32] |= 1 << (i % 32);
      break;
    }
  }

  return count;
}

static ssize_t get_trace_clearlevels(struct file *file, char *user_buf, size_t count, loff_t *ppos)
{
    /* For this file, we consider ppos as a bit position
     *
     * The implementation allows to read the file in several "read"
     * operations. This might be required if user_buf is small and cannot contain
     * all the TRC_IDs.
     *
     * For example if all traces are cleared and count is only 100 characters,
     * the first read operation will provide:
     * TRC_ID_BY_CONSOLE
     * TRC_ID_BY_KPTRACE
     * TRC_ID_BY_STM
     * TRC_ID_ERROR
     * TRC_ID_MAIN_INFO
     * Then the second read operation will provide:
     * TRC_ID_UNCLASSIFIED
     * TRC_ID_QUEUE_BUFFER
     * TRC_ID_PICT_QUEUE_RELEASE
     * TRC_ID_PICTURE_SCHEDULING
     * and so on ...
     */
    unsigned user_count = 0;
    loff_t pos = *ppos;
    unsigned len;

    if (pos >= TRC_ID_LAST) return 0;

    len = trc_id_names[pos].len;
    /* In the line below first +1 is for '\n', second +1 is for '\0' */
    while (user_count+len+1+1 < count  &&  pos < TRC_ID_LAST)
    {
        if ( (trcmask[pos/32] & (1 << (pos % 32))) == 0) {
            if (copy_to_user(user_buf+user_count, trc_id_names[pos].name, len)) return -EFAULT;
            user_count = user_count+len;
            if (copy_to_user(user_buf+user_count, "\n", 1)) return -EFAULT;
            user_count ++;
        }
        pos ++;
        if (pos < TRC_ID_LAST)
          len = trc_id_names[pos].len;
    }
    if (copy_to_user(user_buf+user_count, "\0", 1)) return -EFAULT;
    *ppos = pos;
    return user_count;
}
static ssize_t set_trace_clearlevels(struct file *file, const char *user_buf, size_t count, loff_t *ppos)
{
  /* We assume that user_buf contains the following: "TRC_ID_*"
   * We are only parsing the first TRC_ID (we are not parsing multiple TRC_IDs
   * e.g. user_buf = "TRC_ID_MAIN_INFO TRC_ID_MIXER").
   */
  int i = 0;
  char input_ID[512];

  memset(input_ID, '\0', sizeof(input_ID));

  if (simple_write_to_buffer(input_ID, sizeof(input_ID), ppos, user_buf, count) != count)
    return count;

  input_ID[count-1] = '\0';

  /* Look for input_ID in trc_id_names */
  for (i = 0; i < TRC_ID_LAST; i ++)
  {
    if (strcmp(input_ID, trc_id_names[i].name) == 0)
    {
      /* 'i' is a bit position in an array of uint32_t so we need a little bit of arithmetic */
      trcmask[i/32] &= ~(1 << (i % 32));
      break;
    }
  }

  return count;
}

const struct file_operations trace_levels_fops =
{
  .open   = simple_open,
  .read   = get_trace_levels,
  .write  = set_trace_levels,
};
const struct file_operations trace_setlevels_fops =
{
  .open   = simple_open,
  .read   = get_trace_setlevels,
  .write  = set_trace_setlevels,
};
const struct file_operations trace_clearlevels_fops =
{
  .open   = simple_open,
  .read   = get_trace_clearlevels,
  .write  = set_trace_clearlevels,
};

static struct dentry *vibe_trace_file = NULL, *vibe_trace_setfile = NULL, *vibe_trace_clearfile = NULL;

#define VIBE_TRACE_FILE_NAME "vibe_trace_mask"
#define VIBE_TRACE_SET_NAME  "vibe_trace_set"
#define VIBE_TRACE_CLEAR_NAME "vibe_trace_clear"

static void vibe_trace_create_debugfs(void)
{
    unsigned i, len;
  char * tmp = trc_id_names_string;

  if(debugfs_initialized())
  {
    vibe_trace_file      = debugfs_create_file(VIBE_TRACE_FILE_NAME , 0600, NULL, (void *) &trcmask, &trace_levels_fops);
    vibe_trace_setfile   = debugfs_create_file(VIBE_TRACE_SET_NAME  , 0600, NULL, (void *) &trcmask, &trace_setlevels_fops);
    vibe_trace_clearfile = debugfs_create_file(VIBE_TRACE_CLEAR_NAME, 0600, NULL, (void *) &trcmask, &trace_clearlevels_fops);

    /* Very simple parsing of trc_id_names_string to isolate the different
     * TRC_ID_.... names
     *
     * small optimization:
     * We are replacing the character following the identifier (TRC_ID_*) with a '\n'.
     * This allows to avoid calling twice copy_to_user in get_trace_* functions
     * (first to copy the identifier then to append a '\n').
     *
     * The length computed here doesn't take into account the '\n', it
     * correspond to the actual length of TRC_ID_* . This length is used in
     * set_trace_* functions (strncmp) to compare only the actual TRC_ID_*.
     */
    for (i=0; i<TRC_ID_LAST; i++)
    {
      while (*tmp != '\0'  &&  *tmp != 'T') tmp++;
      trc_id_names[i].name = tmp;
      len = 0;
      while (*tmp != '\0'  &&  *tmp != ' '  &&  *tmp != ',')
      {
        tmp ++;
        len ++;
      }
      trc_id_names[i].len = len;
      *tmp++ = '\0';
    }
  }
}

static void vibe_trace_remove_debugfs(void)
{
  if(vibe_trace_file)
    debugfs_remove(vibe_trace_file);
  if(vibe_trace_setfile)
    debugfs_remove(vibe_trace_setfile);
  if(vibe_trace_clearfile)
    debugfs_remove(vibe_trace_clearfile);
}
#endif


#if defined(CONFIG_MTT)
void vibe_mtt_init(void)
{
  uint32_t     mtt_prev_level = 0;

  if (mtt_open(MTT_COMP_ID_ANY, "DisplayEngine", &vibe_mtt_handle) != MTT_ERR_NONE)
  {
    /* fail to open mtt, so reset the handle for not printing on it. */
    vibe_mtt_handle = 0;
    printk(KERN_ERR "vibe_mtt_init() fails to open mtt\n");
    return;
  }

  if (mtt_set_component_filter(vibe_mtt_handle, MTT_LEVEL_ALL, &mtt_prev_level) != MTT_ERR_NONE)
  {
    /* fail to set mtt filter */
    printk(KERN_ERR "vibe_mtt_init() fails to set mtt filter\n");
    return;
  }

  if (mtt_print(vibe_mtt_handle, MTT_LEVEL_INFO, "DisplayEngine MTT tracer Opening done") != MTT_ERR_NONE)
  {
    /* fail to print by using mtt_print() */
    printk(KERN_ERR "vibe_mtt_init() fails to print by using mtt_print()\n");
  }
}

void vibe_mtt_release(void)
{
  if (vibe_mtt_handle != 0)
  {
    if (mtt_print(vibe_mtt_handle, MTT_LEVEL_INFO, "DisplayEngine MTT tracer Closing on-going") != MTT_ERR_NONE)
    {
      /* fail to print by using mtt_print() */
      printk(KERN_ERR "vibe_mtt_release() fails to print by using mtt_print()\n");
    }

    if (mtt_close(vibe_mtt_handle) != MTT_ERR_NONE)
    {
      /* fail to close mtt */
      printk(KERN_ERR "vibe_mtt_release() fails to close mtt\n");
    }

    /* no futur writing will be possible */
    vibe_mtt_handle = 0;
  }
}
#endif

int  vibe_debug_init(void)
{
  TRC(TRC_ID_MAIN_INFO, "trace array size is %d (last TRC ID is %d)\n", VIBE_OS_TRCMASK_SIZE , TRC_ID_LAST );

  /* Enable here the traces that you want to get by default */
  __SET_TRC(TRC_ID_BY_CONSOLE);
  __SET_TRC(TRC_ID_ERROR);
#if defined(DEBUG)
  __SET_TRC(TRC_ID_MAIN_INFO);
#endif


#if defined(CONFIG_MTT)
  /* Initialise MTT printer */
  vibe_mtt_init();
#endif

#if defined(CONFIG_DEBUG_FS)
  vibe_trace_create_debugfs();
#endif

  return 0;
}

void vibe_debug_release(void)
{
#if defined(CONFIG_DEBUG_FS)
  vibe_trace_remove_debugfs();
#endif

#if defined(CONFIG_MTT)
  /* Release MTT printer */
  vibe_mtt_release();
#endif

}


void vibe_break(const char *cond, const char *file, unsigned int line)
{
  TRC(TRC_ID_ERROR, "%s:%u:FATAL ASSERTION FAILURE %s", file, line, cond);
  BUG();
}


void vibe_printf(const char *fmt,...)
{
  va_list ap;

  va_start(ap,fmt);

  vprintk(fmt,ap);

  va_end(ap);
}


void vibe_trace( int id, char c, const char *pf, const char *fmt, ... )
{
  char prefix[MAX_STRING_LENGTH];
  char fct[MAX_LENGTH_FCT];
  char arg[MAX_LENGTH_ARG];
  int i, j;
  va_list ap;

  if ( ! (TRC_OUTPUT_MASK) )
  {
    return; /* no output selected */
  }

  /* Set prefix trace in prefix[] */
  if(id == TRC_ID_ERROR)
  {
    snprintf( prefix, MAX_STRING_LENGTH, "DE_ERROR");
  }
  else
  {
    prefix[0] = 0;
  }

  /* Set function name tracing in fct[] */
  fct[0] = 0;
  if ( c != ' ' ) /* not done for TRCBL */
    {
      /* get the function name (class name::function name in c++) from __PRETTY_FUNCTION__ */
      for ( i = 0, j = 0; i < MAX_LENGTH_FCT; i++ )
        {
          if (( pf[i] == '(' ) || ( pf[i] == 0 )) /* begin of parameters area */
            {
              break;
            }
          else if (( pf[i] == ' ' ) || ( pf[i] == '*' )) /* not still in function name */
            {
              j = 0;
            }
          else
            {
              fct[j++] = pf[i];
            }
        }

      fct[j++] = ' '; /* add " -" at the end of function name */
      fct[j++] = '-';
      fct[j] = 0; /* terminate function name string */
    }

  /* Set argument of trace in arg[] */
  va_start( ap, fmt );
  vsnprintf( arg, MAX_LENGTH_ARG, fmt, ap );
  va_end( ap );

  /* Tracing with printk() */
  if ( trcmask[0] & ( 1 << TRC_ID_BY_CONSOLE ))
    {
      char color_in[11];
      char color_out[5];
      color_in[0] = 0;
      color_out[0] = 0;

#ifdef TRACING_IN_COLOR
      if ( id == TRC_ID_ERROR )
        {
          snprintf( color_in, 11, "%c[%d;%dm", SET_COLOR, BOLD, RED );
          snprintf( color_out, 5, "%c[%dm", SET_COLOR, ATTR_OFF );
        }
#endif

#ifdef CONFIG_DISPLAY_ASYNC_TRACES
      async_buffer_write_data_var("vibe  %c %s %s%s%s%s\n", c, prefix, color_in, fct, arg, color_out);
#else
      /* no blank needed between fct and arg because fmt always begins with a blank */
      if ( id == TRC_ID_ERROR )
        pr_err("vibe  %c %s %s%s%s%s\n", c, prefix, color_in, fct, arg, color_out);
      else
        pr_info("vibe  %c %s %s%s%s%s\n", c, prefix, color_in, fct, arg, color_out);
#endif
    }

  /* Tracing with kpprintf() */
#ifdef CONFIG_KPTRACE
  if ( trcmask[0] & ( 1 << TRC_ID_BY_KPTRACE ))
    {
      /* no blank needed between fct and arg because fmt always begins with a blank */
      kpprintf( "vibe  %c %s %s%s\n", c, prefix, fct, arg );
    }
#endif

  /* Tracing with mtt_print() */
#ifdef CONFIG_MTT
  if ( trcmask[0] & ( 1 << TRC_ID_BY_STM ))
    {
      /* check if mtt handle was opened, else nothing to print */
      if (vibe_mtt_handle != 0)
        {
          mtt_print(vibe_mtt_handle, MTT_LEVEL_INFO, "vibe  %c %s %s%s\n", c, prefix, fct, arg);
        }
    }
#endif
}

#endif /* CONFIG_DISPLAY_REMOVE_TRACES */

/******************************************************************************/


static void __exit vibe_os_module_exit(void)
{
    TRC(TRC_ID_MAIN_INFO, "vibe_os_module_exit");

    vibe_debug_release();
    vibe_os_release();
}


static int __init vibe_os_module_init(void)
{
    TRC(TRC_ID_MAIN_INFO, "vibe_os_module_init");

    if(vibe_os_init()<0)
      return -1;

    if (vibe_debug_init()<0)
      return -1;

    return 0;
}

/******************************************************************************
 *  Modularization
 */

module_init(vibe_os_module_init);
module_exit(vibe_os_module_exit);

MODULE_LICENSE("GPL");

/******************************************************************************
 *  Exported symbols
 */

EXPORT_SYMBOL(vibe_os_init);
EXPORT_SYMBOL(vibe_os_release);
EXPORT_SYMBOL(vibe_os_create_thread);
EXPORT_SYMBOL(vibe_os_stop_thread);
EXPORT_SYMBOL(vibe_os_wake_up_thread);
EXPORT_SYMBOL(vibe_os_thread_should_stop);
EXPORT_SYMBOL(vibe_os_sleep_ms);
EXPORT_SYMBOL(vibe_os_read_register);
EXPORT_SYMBOL(vibe_os_write_register);
EXPORT_SYMBOL(vibe_os_read_byte_register);
EXPORT_SYMBOL(vibe_os_write_byte_register);
EXPORT_SYMBOL(vibe_os_get_system_time);
EXPORT_SYMBOL(vibe_os_get_one_second);
EXPORT_SYMBOL(vibe_os_stall_execution);
EXPORT_SYMBOL(vibe_os_map_memory);
EXPORT_SYMBOL(vibe_os_unmap_memory);
EXPORT_SYMBOL(vibe_os_zero_memory);
EXPORT_SYMBOL(vibe_os_memset);
EXPORT_SYMBOL(vibe_os_memcmp);
EXPORT_SYMBOL(vibe_os_memcpy);
#ifdef CONFIG_DISPLAY_MEMORY_GUARD
EXPORT_SYMBOL(vibe_os_check_dma_area_guards);
#endif
EXPORT_SYMBOL(vibe_os_flush_dma_area);
EXPORT_SYMBOL(vibe_os_memset_dma_area);
EXPORT_SYMBOL(vibe_os_memcpy_to_dma_area);
EXPORT_SYMBOL(vibe_os_memcpy_from_dma_area);
EXPORT_SYMBOL(vibe_os_allocate_memory);
EXPORT_SYMBOL(vibe_os_free_memory);
EXPORT_SYMBOL(vibe_os_allocate_bpa2_dma_area);
EXPORT_SYMBOL(vibe_os_allocate_kernel_dma_area);
EXPORT_SYMBOL(vibe_os_allocate_dma_area);
EXPORT_SYMBOL(vibe_os_free_dma_area);
EXPORT_SYMBOL(vibe_os_get_dma_channel);
EXPORT_SYMBOL(vibe_os_release_dma_channel);
EXPORT_SYMBOL(vibe_os_stop_dma_channel);
EXPORT_SYMBOL(vibe_os_create_dma_transfer);
EXPORT_SYMBOL(vibe_os_delete_dma_transfer);
EXPORT_SYMBOL(vibe_os_start_dma_transfer);

EXPORT_SYMBOL(__vibe_os_create_resource_lock);
EXPORT_SYMBOL(vibe_os_delete_resource_lock);
EXPORT_SYMBOL(vibe_os_lock_resource);
EXPORT_SYMBOL(vibe_os_unlock_resource);

EXPORT_SYMBOL(vibe_os_create_semaphore);
EXPORT_SYMBOL(vibe_os_delete_semaphore);
EXPORT_SYMBOL(vibe_os_down_semaphore);
EXPORT_SYMBOL(vibe_os_down_trylock_semaphore);
EXPORT_SYMBOL(vibe_os_up_semaphore);

EXPORT_SYMBOL(__vibe_os_create_mutex);
EXPORT_SYMBOL(vibe_os_is_locked_mutex);
EXPORT_SYMBOL(vibe_os_delete_mutex);
EXPORT_SYMBOL(vibe_os_lock_mutex);
EXPORT_SYMBOL(vibe_os_trylock_mutex);
EXPORT_SYMBOL(vibe_os_unlock_mutex);

EXPORT_SYMBOL(vibe_os_of_property_read_bool);
EXPORT_SYMBOL(vibe_os_of_property_read_u32);
EXPORT_SYMBOL(vibe_os_of_property_read_string);
EXPORT_SYMBOL(vibe_os_release_firmware);
EXPORT_SYMBOL(vibe_os_request_firmware);
EXPORT_SYMBOL(vibe_os_snprintf);
EXPORT_SYMBOL(vibe_os_call_bios);
EXPORT_SYMBOL(vibe_os_div64);
EXPORT_SYMBOL(vibe_os_write_memory_barrier);
EXPORT_SYMBOL(vibe_os_read_memory_barrier);
EXPORT_SYMBOL(vibe_os_rational_reduction);

EXPORT_SYMBOL(vibe_os_create_work_queue);
EXPORT_SYMBOL(vibe_os_destroy_work_queue);
EXPORT_SYMBOL(vibe_os_allocate_workerdata);
EXPORT_SYMBOL(vibe_os_delete_workerdata);
EXPORT_SYMBOL(vibe_os_queue_workerdata);

EXPORT_SYMBOL(vibe_os_create_message_thread);
EXPORT_SYMBOL(vibe_os_destroy_message_thread);
EXPORT_SYMBOL(vibe_os_allocate_message);
EXPORT_SYMBOL(vibe_os_delete_message);
EXPORT_SYMBOL(vibe_os_queue_message);

EXPORT_SYMBOL(vibe_os_allocate_queue_event);
EXPORT_SYMBOL(vibe_os_release_queue_event);
EXPORT_SYMBOL(vibe_os_get_wait_queue_data);
EXPORT_SYMBOL(vibe_os_wake_up_queue_event);
EXPORT_SYMBOL(vibe_os_wait_queue_event);
EXPORT_SYMBOL(vibe_os_wake_up);
EXPORT_SYMBOL(vibe_os_wait_event);

EXPORT_SYMBOL(vibe_os_enable_irq);
EXPORT_SYMBOL(vibe_os_disable_irq_nosync);
EXPORT_SYMBOL(vibe_os_disable_irq);
EXPORT_SYMBOL(vibe_os_in_interrupt);

EXPORT_SYMBOL(vibe_os_gpio_get_value);
EXPORT_SYMBOL(vibe_os_get_chip_version);

EXPORT_SYMBOL(vibe_break);
EXPORT_SYMBOL(vibe_printf);
EXPORT_SYMBOL(vibe_trace);
EXPORT_SYMBOL(trcmask);

EXPORT_SYMBOL(vibe_os_clk_get);
EXPORT_SYMBOL(vibe_os_clk_put);
EXPORT_SYMBOL(vibe_os_clk_enable);
EXPORT_SYMBOL(vibe_os_clk_disable);
EXPORT_SYMBOL(vibe_os_clk_resume);
EXPORT_SYMBOL(vibe_os_clk_suspend);
EXPORT_SYMBOL(vibe_os_clk_set_parent);
EXPORT_SYMBOL(vibe_os_clk_get_parent);
EXPORT_SYMBOL(vibe_os_clk_set_rate);
EXPORT_SYMBOL(vibe_os_clk_get_rate);
