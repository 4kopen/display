/***************************************************************************
This file is part of stmcore-display
COPYRIGHT (C) 2014 STMicroelectronics - All Rights Reserved
License type: GPLv2

stmcore-display is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

stmcore-display is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with  stmcore-display; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

The stmcore-display Library may alternatively be licensed under a proprietary
license from ST.

This file was last modified by STMicroelectronics on 2014/09/10
***************************************************************************/

#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/of.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/platform_device.h>

#include <asm/mach-types.h>
#include <asm/system.h>

#include <stm_display.h>
#include <linux/stm/stmcoredisplay.h>
#include <linux/stm/hdmiplatform.h>
#include <linux/stm/linkplatform.h>

#include <display/soc/stiH418/stiH418reg.h>
#include <display/soc/stiH418/stiH418device.h>
#include <vibe_debug.h>

extern void stmcore_display_dt_get_pdata(
        struct platform_device *pdev,
        struct stmcore_display_pipeline_data **pp,
        int *n);

static stm_display_filter_setup_t sd_luma_filter = {
  DENC_COEFF_LUMA,
  { .denc = { STM_FLT_DIV_1024,
              { -3, -4, -7, -1, 35, 5, -96, -15, 327, 542 }
            }
  }
};

static stm_display_filter_setup_t sd_chroma_filter = {
  DENC_COEFF_CHROMA,
  { .denc = { STM_FLT_DIV_512,
              {10, 6, -9, -25, -23, 13, 73, 134, 154 }
            }
  }
};

int stmcore_probe_device(struct platform_device *pdev,
                                struct stmcore_display_pipeline_data **pd,
                                int *nr_platform_devices)
{

  *pd = NULL;
  *nr_platform_devices = 0;
  stmcore_display_dt_get_pdata(pdev, pd, nr_platform_devices);
  if ( !*pd ) {
      TRC(TRC_ID_ERROR, "error while reading Device tree definition for display driver.\n");
      return -ENODEV;
  }

  /*
   * Setup HDMI platform device
   */
  TRC(TRC_ID_MAIN_INFO, "stmcore-display: STiH418 display: probed\n");

  return 0;
}

int stmcore_display_postinit(struct stmcore_display_pipeline_data *p)
{
    if(stm_display_output_set_compound_control(p->display_runtime->main_output, OUTPUT_CTRL_DAC_FILTER_COEFFICIENTS, &sd_luma_filter) < 0)
    {
      TRC(TRC_ID_ERROR,"error stm_display_output_set_compound_control() fails!\n");
    }
    if(stm_display_output_set_compound_control(p->display_runtime->main_output, OUTPUT_CTRL_DAC_FILTER_COEFFICIENTS, &sd_chroma_filter) < 0)
    {
      TRC(TRC_ID_ERROR,"error stm_display_output_set_compound_control() fails!\n");
    }
    return 0;
}

void stmcore_cleanup_device(void)
{
}
