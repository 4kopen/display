/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/stmcp_sysfs.c
 * Copyright (c) 2008-2013 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/version.h>
#include <linux/init.h>
#include <linux/sysfs.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <linux/stm/stmcorecp.h>
#include <stm_display_cp.h>


/******************************************************************************
 * STMCP Sysfs implementation.
 */
static ssize_t show_stmcp_status(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct stmcp_class_device_s *stmcp_class_device = dev_get_drvdata(dev);
  struct stmcp_device_s *stmcp = stmcp_class_device->stmcp;
  struct stmcp_netlink_s *netlink = stmcp->netlink;

  if(!stmcp || !netlink)
    return -EAGAIN;

  return snprintf(buf, PAGE_SIZE, "%s: hdl=%p, err=%d\n", netlink->name, stmcp, netlink->error);
}

static ssize_t stmcp_enable(struct device           *dev,
                            struct device_attribute *attr,
                            const char              *buf,
                            size_t                   count)
{
  int ret = 0;
  int type = STMCP_TYPE_UNKNOWN;
  int id = 0;
  stm_display_cp_h hdl;

  ret = sscanf(buf,"type=%d,id=%d", &type, &id);
  if (ret < 0)
  {
    TRC(TRC_ID_ERROR, "error stmcp_enable() returns %d", ret);
    return (ssize_t)ret;
  }

  ret = stm_display_cp_open((stmcp_type_t)type, id, &hdl);
  if (ret < 0)
  {
    TRC(TRC_ID_ERROR, "error stm_display_cp_open() returns %d", ret);
    return (ssize_t)ret;
  }

  ret = stm_display_cp_enable(hdl);
  if (ret < 0)
  {
    TRC(TRC_ID_ERROR, "error stm_display_cp_enable() returns %d", ret);
    stm_display_cp_close(hdl);
    return (ssize_t)ret;
  }

  stm_display_cp_close(hdl);
  return count;
}

static ssize_t stmcp_disable(struct device           *dev,
                             struct device_attribute *attr,
                             const char              *buf,
                             size_t                   count)
{
  int ret = 0;
  int type = STMCP_TYPE_UNKNOWN;
  int id = 0;
  stm_display_cp_h hdl;

  ret = sscanf(buf,"type=%d,id=%d", &type, &id);
  if (ret < 0)
  {
    TRC(TRC_ID_ERROR, "error stmcp_enable() returns %d", ret);
    return (ssize_t)ret;
  }

  ret = stm_display_cp_open((stmcp_type_t)type, id, &hdl);
  if (ret < 0)
  {
    TRC(TRC_ID_ERROR, "error stm_display_cp_open() returns %d", ret);
    return (ssize_t)ret;
  }

  ret = stm_display_cp_disable(hdl);
  if (ret < 0)
  {
    TRC(TRC_ID_ERROR, "error stm_display_cp_disable() returns %d", ret);
    stm_display_cp_close(hdl);
    return (ssize_t)ret;
  }

  stm_display_cp_close(hdl);
  return count;
}

static ssize_t stmcp_set_data(struct device           *dev,
                              struct device_attribute *attr,
                              const char              *buf,
                              size_t                   count)
{
  int ret = 0;
  int type = STMCP_TYPE_UNKNOWN;
  int id = 0;
  unsigned int          mode;
  unsigned short        databuf[STMCP_MAX_USER_DATA] = { 0 };
  unsigned char         data[STMCP_MAX_USER_DATA] = { 0 };
  unsigned int          i, len;

  char *buf2;
  stm_display_cp_h hdl;

  len = sscanf(buf, "type=%d,id=%d,mode=%04x", &type, &id, &mode);
  if(len == 3)
  {
    len = 0;
    buf2 = strstr(buf, "data=");
    if(buf2)
    {
      len = sscanf(buf2, "data=%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi,%hi"
                       , &databuf[0] , &databuf[1] , &databuf[2] , &databuf[3] , &databuf[4] , &databuf[5] , &databuf[6] , &databuf[7]
                       , &databuf[8] , &databuf[9] , &databuf[10], &databuf[11], &databuf[12], &databuf[13], &databuf[14], &databuf[15]
                       , &databuf[16]);

      if(len > 0)
      {
        for(i=0; i<len; i++)
          data[i] = (databuf[i] & 0xFF);
      }
    }

    ret = stm_display_cp_open((stmcp_type_t)type, id, &hdl);
    if (ret < 0)
    {
      TRC(TRC_ID_ERROR, "error stm_display_cp_open() returns %d", ret);
      return (ssize_t)ret;
    }

    ret = stm_display_cp_set_data(hdl, mode, (len?data:NULL), len);
    if (ret < 0)
    {
      TRC(TRC_ID_ERROR, "error stm_display_cp_set_data() returns %d", ret);
      stm_display_cp_close(hdl);
      return (ssize_t)ret;
    }

    stm_display_cp_close(hdl);
    return count;
  }

  TRC(TRC_ID_ERROR, "error stmcp_set_data() returns %d", -EINVAL);
  return (ssize_t)-EINVAL;
}


static struct device_attribute stmcp_device_attrs[] = {
__ATTR(status, S_IRUGO, show_stmcp_status, NULL),
__ATTR(enable, (S_IRUGO|S_IWUSR), show_stmcp_status, stmcp_enable),
__ATTR(disable, (S_IRUGO|S_IWUSR), show_stmcp_status, stmcp_disable),
__ATTR(set, (S_IRUGO|S_IWUSR), show_stmcp_status, stmcp_set_data)
};


int stmcp_create_class_device_files(struct stmcp_class_device_s *stmcp_class_device)
{
  int i,ret;

  for (i = 0; i < ARRAY_SIZE(stmcp_device_attrs); i++) {
    ret = device_create_file(stmcp_class_device->class_device, &stmcp_device_attrs[i]);
    if(ret)
      break;
  }

  return 0;
}
