/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/stmcp_api.c
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/module.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <stmcp_client.h>

extern int stmcp_get( const stmcp_type_t      type,
                      const int               id,
                      stm_display_cp_h *cp );

extern void stmcp_put( stm_display_cp_h cp );


int stm_display_cp_open ( const stmcp_type_t type,
                          const int          id,
                          stm_display_cp_h  *cp )
{
  int ret = 0;

  ret = stmcp_get(type, id, cp);

  return ret;
}
EXPORT_SYMBOL(stm_display_cp_open);

void stm_display_cp_close ( stm_display_cp_h cp )
{
  if(!IS_HANDLE_VALID(cp, VALID_STMCP_HANDLE))
    return;

  stmcp_put(cp);
}
EXPORT_SYMBOL(stm_display_cp_close);

int stm_display_cp_enable ( stm_display_cp_h cp )
{
  int ret = 0;

  if(!IS_HANDLE_VALID(cp, VALID_STMCP_HANDLE))
    return -EINVAL;

  ret = stmcp_enable(cp->stmcp_dev, cp->type, cp->id);

  return ret;
}
EXPORT_SYMBOL(stm_display_cp_enable);

int stm_display_cp_disable ( stm_display_cp_h cp )
{

  int ret = 0;

  if(!IS_HANDLE_VALID(cp, VALID_STMCP_HANDLE))
    return -EINVAL;

  ret = stmcp_disable(cp->stmcp_dev, cp->type, cp->id);

  return ret;
}
EXPORT_SYMBOL(stm_display_cp_disable);

int stm_display_cp_set_data ( stm_display_cp_h cp,
                              const unsigned int          mode,
                              const unsigned char * const data,
                              const int unsigned          len )
{
  int ret = 0;

  if(!IS_HANDLE_VALID(cp, VALID_STMCP_HANDLE))
    return -EINVAL;

  ret = stmcp_set(cp->stmcp_dev, cp->type, cp->id, mode, data, len);

  return ret;
}
EXPORT_SYMBOL(stm_display_cp_set_data);
