/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/stmcp_client.h
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 \***********************************************************************/

/* Define to prevent recursive inclusion */
#ifndef __STMCP_CLIENT_H
#define __STMCP_CLIENT_H

/* Includes --------------------------------------------------------------- */

#include <linux/socket.h>
#include <linux/netlink.h>
#include <linux/connector.h>
#include <net/sock.h>

#include <linux/stm/stmcorecp.h>


/* C++ support */
#ifdef __cplusplus
extern "C" {
#endif

/* Exported Constants ----------------------------------------------------- */

typedef enum stmcp_netlink_cmd_s
{
  STMCP_NETLINK_CMD_ENABLE    = 1
, STMCP_NETLINK_CMD_SET
, STMCP_NETLINK_CMD_DISABLE
, STMCP_NETLINK_CMD_INIT
, STMCP_NETLINK_CMD_TERM
, STMCP_NETLINK_CMD_UNKNOWN
} stmcp_netlink_cmd_t;

/* Exported Macros -------------------------------------------------------- */


/* Exported Variables ----------------------------------------------------- */

typedef struct stmcp_netlink_data_s
{
  struct stmcp_device_s          *handle;
  stmcp_netlink_cmd_t             cmd;
  stmcp_type_t                    type;
  int                             id;
  int                             devid;
  unsigned int                    mode;
  int                             len;
  unsigned char                   data[STMCP_MAX_USER_DATA];

  int                             error;
  unsigned int                    flags;
} stmcp_netlink_data_t;


/* Exported functions ----------------------------------------------------- */
extern int stmcp_netlink_start
    ( struct stmcp_device_s *stmcp );

extern void stmcp_netlink_stop
    ( struct stmcp_device_s *stmcp );

extern int stmcp_enable
    ( struct stmcp_device_s *stmcp
    , const stmcp_type_t type, const int id );

extern int stmcp_set
    ( struct stmcp_device_s *stmcp
    , const stmcp_type_t type, const int id
    , const unsigned int mode
    , const unsigned char * const data, const int len );

extern int stmcp_disable
    ( struct stmcp_device_s *stmcp
    , const stmcp_type_t type, const int id );


/* C++ support */
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __STMCP_CLIENT_H */
