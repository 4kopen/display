/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/stmcp_fops.c
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/cdev.h>
#include <linux/compiler.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/platform_device.h>
#include <linux/pm.h>
#include <linux/pm_runtime.h>

#include <asm/uaccess.h>
#include <linux/semaphore.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <linux/stm/stmcorecp.h>

#include "vbi_driver.h"

/* Needed for the STMCPIO */
#include "stmcp.h"

/* -------------------- port of Vibe on kernel 3.10 ------------------------ */
/*
 * The VM flag VM_RESERVED has been removed and VM_DONTDUMP should be
 * used instead. Unfortunately VM_DONTDUMP is not avilable in 3.4,
 * hence the code below :-(
 */
#define   STMCP_VM_FLAGS (VM_IO | VM_DONTDUMP | VM_DONTEXPAND)

struct stmcp_standard_list_entry_s
{
  stm_display_mode_id_t           mode;
  stmcp_output_standard_t         std;
};

struct stmcp_format_list_entry_s
{
  stm_video_output_format_t       format;
  stmcp_output_format_t           fmt;
};


static const struct stmcp_standard_list_entry_s stmcp_standard_list[] = {
  {STM_TIMING_MODE_RESERVED,          STMCPIO_STD_UNKNOWN      },
  {STM_TIMING_MODE_480I59940_13500,   STMCPIO_STD_NTSC         },
  {STM_TIMING_MODE_576I50000_13500,   STMCPIO_STD_PAL,         },
  {STM_TIMING_MODE_480P60000_27027,   STMCPIO_STD_480P_60,     },
  {STM_TIMING_MODE_480P59940_27000,   STMCPIO_STD_480P_59_94   },
  {STM_TIMING_MODE_576P50000_27000,   STMCPIO_STD_576P_50,     },
  /* */
  {STM_TIMING_MODE_1080I60000_74250,  STMCPIO_STD_1080I_60,    },
  /* */
  {STM_TIMING_MODE_720P60000_74250,   STMCPIO_STD_720P_60,     },
};

static const struct stmcp_format_list_entry_s stmcp_format_list[] = {
  {STM_VIDEO_OUT_NONE,  STMCPIO_OUTPUT_ANALOGUE_UNKNOWN },
  {STM_VIDEO_OUT_RGB,   STMCPIO_OUTPUT_ANALOGUE_RGB,    },
  {STM_VIDEO_OUT_YUV,   STMCPIO_OUTPUT_ANALOGUE_YUV,    },
  {STM_VIDEO_OUT_YC,    STMCPIO_OUTPUT_ANALOGUE_YC      },
  {STM_VIDEO_OUT_CVBS,  STMCPIO_OUTPUT_ANALOGUE_CVBS,   }
};


static inline stmcp_output_standard_t convert_mode_to_std(stm_display_mode_id_t mode)
{
  int i;

  if(mode == STM_TIMING_MODE_RESERVED)
    return STMCPIO_STD_UNKNOWN;

  for(i =0; i < ARRAY_SIZE(stmcp_standard_list); i++)
  {
    if(mode == stmcp_standard_list[i].mode)
      return stmcp_standard_list[i].std;
  }

  return STMCPIO_STD_UNKNOWN;
}

static inline uint32_t convert_format_to_fmt(uint32_t format)
{
  int i;
  uint32_t fmt = STMCPIO_OUTPUT_ANALOGUE_UNKNOWN;

  if(format == STM_VIDEO_OUT_NONE)
    return fmt;

  for(i =0; i < ARRAY_SIZE(stmcp_format_list); i++)
  {
    if((format & stmcp_format_list[i].format) == format)
      fmt |= stmcp_format_list[i].fmt;
  }

  return fmt;
}

static int stmcp_get_output_info(struct stmcp_class_device_s* stmcp_class_device, stmcp_output_info_t *output_info)
{
  int res = 0;
  stm_display_output_h  hOutput = NULL;
  stm_display_output_connection_status_t output_status;
  uint32_t              format;
  stm_display_mode_t    mode;

  memset(output_info, 0, sizeof(stmcp_output_info_t));

  if(stmcp_class_device->stmcp->display_device == NULL)
  {
    TRC(TRC_ID_ERROR, "Display device is invalid or suspended ?");
    return -ENODEV;
  }

  if((res = stm_display_device_open_output(stmcp_class_device->stmcp->display_device, stmcp_class_device->master_output_id, &hOutput))==0)
  {
    if((res = stm_display_output_get_connection_status(hOutput, &output_status))==0)
    {
      if((res = stm_display_output_get_current_display_mode(hOutput, &mode))==0)
      {
        output_info->std = convert_mode_to_std(mode.mode_id);
        if((res = stm_display_output_get_control(hOutput, OUTPUT_CTRL_VIDEO_OUT_SELECT, &format))==0)
        {
          output_info->fmt = convert_format_to_fmt(format);
        }
      }
    }
    stm_display_output_close(hOutput);
  }

  return res;
}

static int stmcp_set_cgms_sd_data(struct stmcp_class_device_s* stmcp_class_device, const uint32_t cgms_sd_mode)
{
  int res = 0;
  stm_display_output_h  hOutput = NULL;
  stm_display_output_connection_status_t output_status;

  if(stmcp_class_device->stmcp->display_device == NULL)
  {
    TRC(TRC_ID_ERROR, "Display device is invalid or suspended ?");
    return -ENODEV;
  }

  if((res = stm_display_device_open_output(stmcp_class_device->stmcp->display_device, stmcp_class_device->master_output_id, &hOutput))==0)
  {
    if((res = stm_display_output_get_connection_status(hOutput, &output_status))==0)
    {
      res = stm_display_output_set_control(hOutput, OUTPUT_CTRL_SD_CGMS, cgms_sd_mode);
    }
    stm_display_output_close(hOutput);
  }

  return res;
}

static void stmcp_cancel_pending_vsync_wait(struct stmcp_vsync_data_s *vsync_data)
{
  if(vsync_data->vsync_wait_count > 0)
  {
    vsync_data->vsync_wait_count = 0;
    wake_up_interruptible(&vsync_data->wait_queue);
  }
}

static int stmcp_wait_for_vsync(struct stmcp_vsync_data_s *vsync_data, uint32_t vsync_wait_count)
{
  stmcp_cancel_pending_vsync_wait(vsync_data);

  vsync_data->vsync_wait_count = vsync_wait_count;

  return (wait_event_interruptible(vsync_data->wait_queue,
                   (vsync_data->vsync_wait_count == 0)));
}

static int stmcp_open(struct inode *inode, struct file *filp)
{
  struct stmcp_class_device_s *stmcp_class_device = container_of(inode->i_cdev, struct stmcp_class_device_s, cdev);
  filp->private_data = stmcp_class_device;

  pm_runtime_get_sync(&stmcp_class_device->stmcp->pdev->dev);
  stmcp_class_device->vbi_device->hDisplay = stmcp_class_device->stmcp->display_device;
  return 0;
}

static int stmcp_release(struct inode *inode,struct file *filp)
{
  struct stmcp_class_device_s *stmcp_class_device = filp->private_data;
  pm_runtime_put_sync(&stmcp_class_device->stmcp->pdev->dev);
  stmcp_class_device->vbi_device->hDisplay = NULL;
  return 0;
}

static int stmcp_iomm_vma_fault(struct vm_area_struct *vma,
                                 struct vm_fault       *vmf)
{
    /* we want to provoke a bus error rather than give the client
       the zero page */
    return VM_FAULT_SIGBUS;
}

static struct vm_operations_struct stmcp_iomm_nopage_ops = {
  .fault  = stmcp_iomm_vma_fault,
};

static int
stmcp_mmap (struct file *filp,struct vm_area_struct * const vma)
{
  int ret = -EFAULT;
  unsigned long rawaddr, physaddr, vsize, off;
  unsigned int flags = vma->vm_flags;
  struct stmcp_class_device_s *stmcp_class_device = filp->private_data;

  if(stmcp_class_device->vbi_device)
  {
    vma->vm_flags |= (VM_WRITE | VM_SHARED);
    ret = stmcp_vbi_mmap(stmcp_class_device->vbi_device, vma);
  }

  if(ret == -EFAULT)
  {
    TRC(TRC_ID_STMCP_DEBUG, "offset = 0x%08X", (int) vma->vm_pgoff);
    vma->vm_flags = (flags | STMCP_VM_FLAGS);
    vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);

    rawaddr  = (vma->vm_pgoff << PAGE_SHIFT);
    physaddr = rawaddr;
    vsize = vma->vm_end - vma->vm_start;

    for (off=0; off<vsize; off+=PAGE_SIZE)
    {
      io_remap_pfn_range(vma, vma->vm_start+off, (physaddr+off) >> PAGE_SHIFT, PAGE_SIZE, vma->vm_page_prot);
    }

    // ensure we get bus errors when we access illegal memory address
    vma->vm_ops = &stmcp_iomm_nopage_ops;

    ret = 0;
  }

  return ret;
}

long stmcp_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
  long retval=0;

  struct stmcp_class_device_s *stmcp_class_device = (struct stmcp_class_device_s *)filp->private_data;
  switch(cmd)
  {
    case STMCPIO_WAIT_FOR_VSYNC :
    {
      if (arg == 0) {
        stmcp_cancel_pending_vsync_wait(stmcp_class_device->vsync_data);
        break;
      }
      retval = stmcp_wait_for_vsync(stmcp_class_device->vsync_data, arg);
      if (retval < 0) {
        TRC(TRC_ID_ERROR, "Failed to wait for %d vsync's", (int) arg);
      }
      break;
    }
    case STMCPIO_GET_OUTPUT_INFO :
    {
      stmcp_output_info_t output_info;

      if(copy_from_user(&output_info, (void*)arg, sizeof(output_info)))
        return -EFAULT;

      retval = stmcp_get_output_info(stmcp_class_device, &output_info);
      if (retval < 0) {
        TRC(TRC_ID_ERROR, "Failed to get output info");
      }
      else
      {
        if(copy_to_user((void*)arg,&output_info,sizeof(output_info)))
          retval = -EFAULT;
      }
      break;
    }
    case STMCPIO_SET_CGMS_ANALOG :
    {
      retval = stmcp_set_cgms_sd_data(stmcp_class_device, (int)arg);
      if (retval < 0) {
        TRC(TRC_ID_ERROR, "Failed to set cgms sd mode");
      }
      break;
    }
    default:
    {
      retval = stmcp_vbi_ioctl(stmcp_class_device->vbi_device, filp->f_flags, cmd, (void *)arg);
      break;
    }
  }

  return retval;
}

struct file_operations stmcp_fops = {
  .owner   = THIS_MODULE,
  .open    = stmcp_open,
  .release = stmcp_release,
  .mmap    = stmcp_mmap,
  .unlocked_ioctl = stmcp_ioctl
};
