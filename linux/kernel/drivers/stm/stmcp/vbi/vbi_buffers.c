/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/vbi/vbi_buffers.c
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 * V4L2 video output device driver for ST SoC display subsystems.
 *
 ***********************************************************************/

#include <linux/version.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/highmem.h>
#include <media/v4l2-dev.h>

#ifdef CONFIG_BPA2
#include <linux/bpa2.h>
#else
#error Kernel must have the BPA2 memory allocator configured
#endif

#include <asm/cacheflush.h>
#include <linux/semaphore.h>
#include <asm/io.h>
#include <asm/page.h>
#include <asm/div64.h>

#ifdef CONFIG_ARM
/*
 * ARM has mis-named this interface relative to all the other platforms
 * that define it (including x86). We are going to make life simple, given
 * that we never want to read bufferdata by using write combined.
 */
#define ioremap_cache ioremap_wc

/*
 * After v2.6.32, we no longer have flush_ioremap_region on ARM
 * TODO: Decide if we need to ensure a BPA allocated page is not stale
 *       in the cache after ioremapping.
 */
#define flush_ioremap_region(phys, virt, offset, size) do { } while (0)

#endif

#include <vibe_os.h>
#include <vibe_debug.h>

#include <stm_display.h>
#include <linux/stm/stmcoredisplay.h>

#include "vbi_driver.h"

#define DIV_ROUNDED_UP(num, den)        ( ( (num) + (den) - 1) / (den))


/*******************************************************************************
 *
 */
void stmcp_init_buffer_queues(stmcp_vbi_device_t * const device)
{
  TRCIN(TRC_ID_STMCP, "");

  if (!device->queues_inited)
  {

    rwlock_init(&device->pendingStreamQ.lock);
    INIT_LIST_HEAD(&device->pendingStreamQ.list);

    rwlock_init(&device->completeStreamQ.lock);
    INIT_LIST_HEAD(&device->completeStreamQ.list);

    device->queues_inited = 1;
  }

  TRCOUT(TRC_ID_STMCP, "");
}

/*******************************************************************************
 *  stmcp_send_next_buffer_to_display
 *
 *  Push a frame from the V4L2 queue to the display driver's queue
 */
void stmcp_send_next_buffer_to_display(stmcp_vbi_device_t * pDev)
{
  streaming_buffer_t *pStreamBuffer, *tmp;
  unsigned long level1_flags, level2_flags;
  int ret;

  if (pDev->hQueueInterface == NULL)
  {
    TRC(TRC_ID_STMCP_DEBUG, "fail due to invalid queue interface handle");
    return;
  }

  read_lock_irqsave(&pDev->pendingStreamQ.lock, level1_flags);
  list_for_each_entry_safe(pStreamBuffer, tmp, &pDev->pendingStreamQ.list, node)
  {
    read_unlock_irqrestore(&pDev->pendingStreamQ.lock, level1_flags);

    if ( (pStreamBuffer->bufferHeader.src.flags & STM_BUFFER_SRC_INTERLACED)
      && ((pStreamBuffer->bufferHeader.src.flags & (STM_BUFFER_SRC_TOP_FIELD_ONLY |
                                                    STM_BUFFER_SRC_BOTTOM_FIELD_ONLY)) == 0) )
    {
      /* The buffer contains an Interlaced FRAME. We should queue one field at a time */
      stm_display_buffer_t FirstFieldBuffer;
      stm_display_buffer_t SecondFieldBuffer;
      int32_t nfields;

      memcpy(&FirstFieldBuffer, &pStreamBuffer->bufferHeader,
             sizeof(stm_display_buffer_t));
      memcpy(&SecondFieldBuffer, &pStreamBuffer->bufferHeader,
             sizeof(stm_display_buffer_t));

      if (pStreamBuffer->bufferHeader.src.flags & STM_BUFFER_SRC_BOTTOM_FIELD_FIRST)
      {
        /* Bottom Field First stream */
        FirstFieldBuffer.src.flags |= STM_BUFFER_SRC_BOTTOM_FIELD_ONLY;
        SecondFieldBuffer.src.flags |= STM_BUFFER_SRC_TOP_FIELD_ONLY;
      }
      else
      {
        /* Top Field First stream */
        FirstFieldBuffer.src.flags |= STM_BUFFER_SRC_TOP_FIELD_ONLY;
        SecondFieldBuffer.src.flags |= STM_BUFFER_SRC_BOTTOM_FIELD_ONLY;
      }

      /* The display_callback   will be sent by the 1st field */
      /* The completed_callback will be sent by the 2nd field */
      FirstFieldBuffer.info.completed_callback = 0;
      SecondFieldBuffer.info.display_callback = 0;

      /* Compute the display duration of each field */
      nfields = pStreamBuffer->bufferHeader.info.nfields;
      FirstFieldBuffer.info.nfields = DIV_ROUNDED_UP(nfields, 2);
      SecondFieldBuffer.info.nfields = nfields - FirstFieldBuffer.info.nfields;

      ret = stm_display_source_queue_buffer(pDev->hQueueInterface, &FirstFieldBuffer);
      if (ret < 0)
      {
        /* There is a signal pending or the hardware queue is full. In either
           case do not take the streaming buffer off the pending queue,
           because it hasn't been written yet. */
        TRC(TRC_ID_STMCP_DEBUG, "1st field queueing failed");
        return;
      }

      if (SecondFieldBuffer.info.nfields > 0)
      {
        ret = stm_display_source_queue_buffer(pDev->hQueueInterface, &SecondFieldBuffer);
        if (ret < 0)
        {
          /* There is a signal pending or the hardware queue is full. In either
             case do not take the streaming buffer off the pending queue,
             because it hasn't been written yet. */
          TRC(TRC_ID_STMCP_DEBUG, "2nd field queueing failed");
          return;
        }
      }
    }
    else
    {
      /* The buffer contains a Progressive FRAME or a single Interlaced FIELD. We can queue it straight away */
      ret = stm_display_source_queue_buffer(pDev->hQueueInterface, &pStreamBuffer->bufferHeader);
      if (ret < 0)
      {
        /* There is a signal pending or the hardware queue is full. In either
           case do not take the streaming buffer off the pending queue,
           because it hasn't been written yet. */
        TRC(TRC_ID_STMCP_DEBUG, "buffer queueing failed");
        return;
      }
    }

    write_lock_irqsave(&pDev->pendingStreamQ.lock, level1_flags);
    list_del_init(&pStreamBuffer->node);
    write_unlock_irqrestore(&pDev->pendingStreamQ.lock, level1_flags);

    read_lock_irqsave(&pDev->pendingStreamQ.lock, level1_flags);

    /* Persistent buffer : mark this buffer as already displayed */
    if (pDev->isStreaming)
    {
      pStreamBuffer->vidBuf.flags |= STMCP_BUF_FLAG_DONE;

      write_lock_irqsave(&pDev->completeStreamQ.lock, level2_flags);
      list_add_tail(&pStreamBuffer->node,
              &pDev->completeStreamQ.list);
      write_unlock_irqrestore(&pDev->completeStreamQ.lock, level2_flags);

      wake_up_interruptible(&pDev->wqBufDQueue);
    }
  }
  read_unlock_irqrestore(&pDev->pendingStreamQ.lock, level1_flags);
}

static void allocateBuffer(unsigned long size, streaming_buffer_t * buf)
{
  const char *partname = {"v4l2-stmvout"};
  unsigned long nPages;

  nPages = (size + PAGE_SIZE - 1) / PAGE_SIZE;

  /* Search if the part name exists in bpa2 region */
  buf->bpa2_part = bpa2_find_part(partname);
  if (!buf->bpa2_part)
    goto bpa2_get_pages_failed;

  /* Try to get the pages */
  buf->physicalAddr = bpa2_alloc_pages(buf->bpa2_part, nPages, 1, GFP_KERNEL);
  if (!buf->physicalAddr)
    goto bpa2_get_pages_failed;

  /* most buffers may not cross a 64MB boundary */
#define PAGE64MB_SIZE  (1<<26)
  if ((buf->physicalAddr & ~(PAGE64MB_SIZE - 1))
      != ((buf->physicalAddr + size) & ~(PAGE64MB_SIZE - 1))) {
    TRC(TRC_ID_STMCP_DEBUG, "%8.lx + %lu (%lu pages) crosses a 64MB boundary, retrying!",
         buf->physicalAddr, size, nPages);

    /* FIXME: this is a hack until the point we get an updated BPA2 driver.
       Will be removed again! */
    /* free and immediately reserve, hoping nobody requests bpa2 memory in
       between... */
    bpa2_free_pages(buf->bpa2_part, buf->physicalAddr);
    buf->physicalAddr = bpa2_alloc_pages(buf->bpa2_part, nPages,
                 PAGE64MB_SIZE / PAGE_SIZE,
                 GFP_KERNEL);
    if (!buf->physicalAddr)
      goto bpa2_get_pages_failed;
    TRC(TRC_ID_STMCP_DEBUG, "new start: %.8lx", buf->physicalAddr);
  }

  if ((buf->physicalAddr & ~(PAGE64MB_SIZE - 1))
      != ((buf->physicalAddr + size) & ~(PAGE64MB_SIZE - 1))) {
    /* can only happen, if somebody else requested bpa2 memory while we were
       busy here */
    TRC(TRC_ID_STMCP_DEBUG, "%8.lx + %lu (%lu pages) crosses a 64MB boundary again! Ignoring this time...",
      buf->physicalAddr, size, nPages);
  }

  buf->cpuAddr = ioremap_cache(buf->physicalAddr, nPages * PAGE_SIZE);
  if (!buf->cpuAddr)
    goto bpa2_ioremap_failed;

  return;

bpa2_ioremap_failed:
  bpa2_free_pages(buf->bpa2_part, buf->physicalAddr);
  buf->physicalAddr = 0;

bpa2_get_pages_failed:
  buf->bpa2_part = NULL;
  buf->cpuAddr = NULL;
}

static void freeBuffer(streaming_buffer_t * buf)
{
  if ((!buf) || (!buf->bpa2_part))
    goto bpa2_pages_freed;

  iounmap(buf->cpuAddr);
  bpa2_free_pages(buf->bpa2_part, buf->physicalAddr);
  buf->bpa2_part = NULL;
  buf->physicalAddr = 0;
  buf->cpuAddr = NULL;

bpa2_pages_freed:
  return;
}

/******************************************************************************
 *
 */
int
stmcp_allocate_mmap_buffers(stmcp_vbi_device_t * pDev,
            stmcp_requestbuffers_t *req)
{
  int ret, i;
  unsigned long bufLen;
  stm_display_mode_t currentMode;

  if (!pDev->hOutput)
    return signal_pending(current) ? -ERESTARTSYS : -EIO;

  /* get the display mode */
  if ((ret = stm_display_output_get_current_display_mode(pDev->hOutput, &currentMode)) < 0)
    return signal_pending(current) ? -ERESTARTSYS : ret;

  /* Default input cropping window is then passed to the plane */
  memset(&(pDev->input_window), 0, sizeof(stm_rect_t));

  /* Default output cropping window is then passed to the plane */
  memset(&(pDev->output_window), 0, sizeof(stm_rect_t));

  /* set the buffer length to a multiple of the page size */
  bufLen = (((currentMode.mode_params.active_area_width * currentMode.mode_params.active_area_height * 4)
      + PAGE_SIZE - 1)
      & ~(PAGE_SIZE - 1));

  TRC(TRC_ID_STMCP_DEBUG, "%d buffers of 0x%x bytes", req->count, (unsigned int)bufLen);

  /* now allocate the buffers */
  BUG_ON(pDev->n_streamBuffers != 0);
  BUG_ON(pDev->streamBuffers != NULL);
  /* Sanity check added */
  if (!req->count)
    return 0;
  pDev->streamBuffers = vmalloc(req->count * sizeof(streaming_buffer_t));
  if (!pDev->streamBuffers)
    return 0;
  /* FIXME: that's pretty expensive! */
  memset(pDev->streamBuffers, 0, req->count * sizeof(streaming_buffer_t));
  pDev->n_streamBuffers = req->count;

  for (i = 0; i < req->count; ++i)
  {
    pDev->streamBuffers[i].pDev = pDev;

    allocateBuffer(bufLen, &pDev->streamBuffers[i]);

    if (pDev->streamBuffers[i].physicalAddr != 0)
    {
      TRC(TRC_ID_STMCP_DEBUG, "Allocated buffer %d at 0x%08lx",
          i,
          (unsigned long)pDev->streamBuffers[i].
          physicalAddr);

      INIT_LIST_HEAD(&pDev->streamBuffers[i].node);

      /*
       * Colorimetry management BT2020/BT601/BT709
       */
      pDev->streamBuffers[i].bufferHeader.src.flags = STM_BUFFER_SRC_COLORSPACE_709;

      if (currentMode.mode_params.scan_type == STM_PROGRESSIVE_SCAN)
      {
        pDev->streamBuffers[i].bufferHeader.info.nfields = 1;
        pDev->streamBuffers[i].bufferHeader.src.flags &= ~STM_BUFFER_SRC_INTERLACED;
      }
      else
      {
        pDev->streamBuffers[i].bufferHeader.info.nfields = 2;
        pDev->streamBuffers[i].bufferHeader.src.flags |= STM_BUFFER_SRC_INTERLACED;
        /*
         * It is slightly odd to determine the output field order by the
         * display standard, but that is what the API says to do. In reality
         * the source order should be specified by the next two cases below.
         */
        if (currentMode.mode_params.
            output_standards & (STM_OUTPUT_STD_NTSC_M |
              STM_OUTPUT_STD_NTSC_J |
              STM_OUTPUT_STD_NTSC_443))
          pDev->streamBuffers[i].bufferHeader.src.flags |= STM_BUFFER_SRC_BOTTOM_FIELD_FIRST;
      }

      pDev->streamBuffers[i].bufferHeader.src.primary_picture.
          video_buffer_addr =
          pDev->streamBuffers[i].physicalAddr;

      memset(&(pDev->streamBuffers[i].vidBuf), 0,
             sizeof(stmcp_buffer_t));
      pDev->streamBuffers[i].vidBuf.index = i;

      /*
       * Warning using the physical address as the mmap offset will break on
       * big 32bit address spaces.
       */
      pDev->streamBuffers[i].vidBuf.offset
          =
          pDev->streamBuffers[i].bufferHeader.src.
          primary_picture.video_buffer_addr;
      pDev->streamBuffers[i].vidBuf.length = bufLen;

      /* Clear the buffer to prevent data leaking leaking into userspace */
      /* While the area has been ioremapped (and thus SHOULD be initialized with memset_io, it doesn't really make sense since it is simply a piece of memory, no IO related */
      memset(pDev->streamBuffers[i].cpuAddr, 0, bufLen);
      flush_ioremap_region(pDev->streamBuffers[i].
               physicalAddr,
               pDev->streamBuffers[i].cpuAddr, 0,
               bufLen);

      pDev->streamBuffers[i].bufferHeader.src.primary_picture.
          video_buffer_size = bufLen;

      pDev->streamBuffers[i].bufferHeader.src.primary_picture.
          color_fmt = SURF_ARGB8888;
      pDev->streamBuffers[i].bufferHeader.src.primary_picture.
          pixel_depth = 32;

      /* Max size for primary picture */
      pDev->streamBuffers[i].bufferHeader.src.primary_picture.
          pitch = (currentMode.mode_params.active_area_width + 64);
      pDev->streamBuffers[i].bufferHeader.src.primary_picture.
          width = (currentMode.mode_params.active_area_width + 64);
      pDev->streamBuffers[i].bufferHeader.src.primary_picture.
          height = 1;

      /* Max size for the visible area */
      pDev->streamBuffers[i].bufferHeader.src.visible_area.x = 0;
      pDev->streamBuffers[i].bufferHeader.src.visible_area.y = 0;
      pDev->streamBuffers[i].bufferHeader.src.visible_area.
          width = (currentMode.mode_params.active_area_width + 64);
      pDev->streamBuffers[i].bufferHeader.src.visible_area.
          height = 1;

      if(currentMode.mode_params.scan_type == STM_INTERLACED_SCAN)
      {
        pDev->streamBuffers[i].bufferHeader.src.primary_picture.pitch *= 2;
        pDev->streamBuffers[i].bufferHeader.src.primary_picture.height *= 2;
        pDev->streamBuffers[i].bufferHeader.src.visible_area.height *= 2;
      }

      /*
       * Specify that the buffer is persistent, this allows us to pause video by
       * simply not queuing any more buffers and the last frame stays on screen.
       */
      pDev->streamBuffers[i].bufferHeader.info.ulFlags = STM_BUFFER_PRESENTATION_PERSISTENT;

      pDev->streamBuffers[i].bufferHeader.info.display_callback = NULL;
      pDev->streamBuffers[i].bufferHeader.info.completed_callback = NULL;
      pDev->streamBuffers[i].bufferHeader.info.puser_data = &(pDev->streamBuffers[i]);
    }
    else
    {
      TRC(TRC_ID_STMCP_DEBUG, "Maximum number of buffers allocated");

      /* We've allocated as many buffers as we can */
      req->count = i;
      break;
    }
  }

  return req->count;
}


/*******************************************************************************
 *
 */
int stmcp_deallocate_mmap_buffers(stmcp_vbi_device_t * pDev)
{
  int i;

  TRCIN(TRC_ID_STMCP_DEBUG, "");

  for (i = 0; i < pDev->n_streamBuffers; i++) {
    TRC(TRC_ID_STMCP_DEBUG, "system %d (%p): phys/count: %8.lx/%d",
        i, &pDev->streamBuffers[i],
        pDev->streamBuffers[i].physicalAddr,
        pDev->streamBuffers[i].mapCount);

    if (pDev->streamBuffers[i].physicalAddr != 0
        && (pDev->streamBuffers[i].mapCount > 0))
      return 0;
  }

  for (i = 0; i < pDev->n_streamBuffers; i++) {
    if (pDev->streamBuffers[i].physicalAddr != 0) {
      TRC(TRC_ID_STMCP_DEBUG, "de-allocating system buffer %d", i);

      freeBuffer(&pDev->streamBuffers[i]);
    }
  }

  vfree(pDev->streamBuffers);
  pDev->streamBuffers = NULL;
  pDev->n_streamBuffers = 0;

  TRCOUT(TRC_ID_STMCP_DEBUG, "");
  return 1;
}


/*******************************************************************************
 *
 */
int stmcp_queue_buffer(stmcp_vbi_device_t * pDev, stmcp_buffer_t *pVidBuf)
{
  int ret;
  stm_display_mode_t currentMode;
  streaming_buffer_t *pBuf;
  stm_display_buffer_t *__restrict bufferHeader;
  unsigned long flags;

  if (!pDev->hOutput)
    return signal_pending(current) ? -ERESTARTSYS : -EIO;

  if (pVidBuf->index >= pDev->n_streamBuffers)
  {
    TRCOUT(TRC_ID_ERROR, "buffer index %d is out of range", pVidBuf->index);
    return -EINVAL;
  }

  pBuf = &pDev->streamBuffers[pVidBuf->index];

  if (pBuf->vidBuf.flags & STMCP_BUF_FLAG_QUEUED)
  {
    TRCOUT(TRC_ID_ERROR, "buffer %d is already queued", pVidBuf->index);
    return -EIO;
  }

  if ((ret = stm_display_output_get_current_display_mode(pDev->hOutput, &currentMode)) < 0)
    return signal_pending(current) ? -ERESTARTSYS : ret;

  bufferHeader = &pBuf->bufferHeader;
  flush_ioremap_region(pBuf->physicalAddr, pBuf->cpuAddr,
           0, pBuf->vidBuf.length);

  if ((bufferHeader->src.flags & STM_BUFFER_SRC_INTERLACED) &&
      (currentMode.mode_params.scan_type == STM_PROGRESSIVE_SCAN))
  {
    TRCOUT(TRC_ID_ERROR, "de-interlaced content on a progressive display not supported");
    return -EINVAL;
  }

  bufferHeader->info.presentation_time = 0;
  bufferHeader->dst.ulFlags &= ~STM_BUFFER_DST_RESCALE_TO_VIDEO_RANGE;
  bufferHeader->info.ulFlags |= STM_BUFFER_PRESENTATION_GRAPHICS;

  /* Update visible area */
  bufferHeader->src.visible_area.x = pDev->input_window.x;
  bufferHeader->src.visible_area.y = pDev->input_window.y;
  bufferHeader->src.visible_area.width = pDev->input_window.width;
  bufferHeader->src.visible_area.height = pDev->input_window.height;

  pBuf->vidBuf.flags &= ~STMCP_BUF_FLAG_DONE;
  pBuf->vidBuf.flags |= STMCP_BUF_FLAG_QUEUED;

  /*
   * The API spec states that the flags are reflected in the structure the
   * user passed in, but nothing else is.
   */
  pVidBuf->flags = pBuf->vidBuf.flags;

  BUG_ON(!list_empty(&pBuf->node));
  write_lock_irqsave(&pDev->pendingStreamQ.lock, flags);
  list_add_tail(&pBuf->node, &pDev->pendingStreamQ.list);
  write_unlock_irqrestore(&pDev->pendingStreamQ.lock, flags);

  if (pDev->isStreaming)
    stmcp_send_next_buffer_to_display(pDev);

  return 0;
}

/*******************************************************************************
 *
 */
int stmcp_dequeue_buffer(stmcp_vbi_device_t * pDev, stmcp_buffer_t *pVidBuf)
{
  streaming_buffer_t *pBuf;
  unsigned long flags;

  write_lock_irqsave(&pDev->completeStreamQ.lock, flags);
  list_for_each_entry(pBuf, &pDev->completeStreamQ.list, node)
  {
    list_del_init(&pBuf->node);
    write_unlock_irqrestore(&pDev->completeStreamQ.lock, flags);

    pBuf->vidBuf.flags &= ~(STMCP_BUF_FLAG_QUEUED | STMCP_BUF_FLAG_DONE);

    *pVidBuf = pBuf->vidBuf;

    return 1;
  }
  write_unlock_irqrestore(&pDev->completeStreamQ.lock, flags);

  return 0;
}


/*******************************************************************************
 *
 */
void stmcp_delete_buffers_from_list(const struct list_head *const list)
{
  streaming_buffer_t *buffer, *tmp;

  list_for_each_entry_safe(buffer, tmp, list, node)
  {
    /* need to init the node as well, because the user might decide to start
       streaming again, without closing the device or calling VIDIOC_REQBUFS
       beforehand. In that case, we would not ever re init the list head,
       thus a BUG_ON() would trigger in stmcp_queue_buffer(). It's not
       correct to remove the BUG_ON(), though, because the node (and thus the
       list) will still have a bogus pointer! */
    list_del_init(&buffer->node);
    buffer->vidBuf.flags &= ~STMCP_BUF_FLAG_QUEUED;
    TRC(TRC_ID_STMCP_DEBUG, "dequeueing/discarding buffer %d", buffer->vidBuf.index);
  }
}

int stmcp_streamon(stmcp_vbi_device_t * pDev)
{
  int ret = 0;
  TRCIN(TRC_ID_STMCP_DEBUG, "pDev = %p", pDev);

  if (pDev->isStreaming)
  {
    TRC(TRC_ID_ERROR, "device already streaming");
    ret = -EBUSY;
    goto error;
  }

  if(pDev->hQueueInterface == NULL)
  {
    TRC(TRC_ID_ERROR, "no source interface available on device");
    ret = -EBUSY;
    goto error;
  }

  TRC(TRC_ID_STMCP_DEBUG, "Starting the stream");

  pDev->isStreaming = 1;
  wake_up_interruptible (&(pDev->wqStreamingState));

  /* Make sure any frames already on the pending queue are written to the
     hardware */
  stmcp_send_next_buffer_to_display (pDev);

error:
  TRCOUT(TRC_ID_STMCP_DEBUG, "pDev = %p", pDev);
  return ret;
}

int stmcp_streamoff(stmcp_vbi_device_t * pDev)
{
  unsigned long flags;
  TRCIN(TRC_ID_STMCP_DEBUG, "pDev = %p", pDev);

  if (pDev->isStreaming)
  {
    /* Discard the pending queue */
    TRC(TRC_ID_STMCP_DEBUG, "About to dequeue from pending queue");
    write_lock_irqsave(&pDev->pendingStreamQ.lock, flags);
    stmcp_delete_buffers_from_list(&pDev->pendingStreamQ.list);
    write_unlock_irqrestore(&pDev->pendingStreamQ.lock, flags);

    TRC(TRC_ID_STMCP_DEBUG, "About to flush plane = %p", pDev->hPlane);

    /*
     * Unlock (and Flush) the source, this will cause all the completed
     * callbacks for each queued buffer to be called.
     */

    BUG_ON(pDev->hQueueInterface == NULL);

    /* dqueue all buffers from the complete queue */
    TRC(TRC_ID_STMCP_DEBUG, "About to discard completed queue");
    write_lock_irqsave(&pDev->completeStreamQ.lock, flags);
    stmcp_delete_buffers_from_list(&pDev->completeStreamQ.list);
    write_unlock_irqrestore(&pDev->completeStreamQ.lock, flags);

    pDev->isStreaming = 0;
    wake_up_interruptible(&(pDev->wqStreamingState));
    /* there could be a task still waiting for buffers ...*/
    wake_up_interruptible(&pDev->wqBufDQueue);
  }

  TRCOUT(TRC_ID_STMCP_DEBUG, "pDev = %p", pDev);
  return 0;
}

/*******************************************************************************
 *
 */
streaming_buffer_t *stmcp_get_buffer_from_mmap_offset(stmcp_vbi_device_t * pDev,
              const unsigned long offset)
{
  streaming_buffer_t *this;
  int i;

  TRCIN(TRC_ID_STMCP_DEBUG, "offset = 0x%08x", (int)offset);

  for (i = 0, this = pDev->streamBuffers; i < pDev->n_streamBuffers; ++i, ++this)
  {
    if (offset >= this->vidBuf.offset && offset < (this->vidBuf.offset + this->vidBuf.length))
    {
      TRCOUT(TRC_ID_STMCP_DEBUG, "buffer found. index = %d", i);
      return this;
    }
  }

  TRCOUT(TRC_ID_STMCP_DEBUG, "buffer not found");
  return NULL;
}

/*******************************************************************************
 *  stmcp_set_output_crop - ioctl helper function for S_CROP
 */
int stmcp_set_output_crop(stmcp_vbi_device_t *pDev,
                          const stmcp_crop_t *pCrop)
{
  TRCIN(TRC_ID_STMCP_DEBUG, "(%ld,%ld-%ldx%ld)",
      (long)pCrop->c.left, (long)pCrop->c.top,
      (long)pCrop->c.width, (long)pCrop->c.height);

  /* The output cropping window is then passed to the plane */
  pDev->output_window.x = pCrop->c.left;
  pDev->output_window.y = pCrop->c.top;
  pDev->output_window.width = pCrop->c.width;
  pDev->output_window.height = pCrop->c.height;

  if(pDev->hPlane)
  {
    if (stm_display_plane_set_compound_control
        (pDev->hPlane, PLANE_CTRL_OUTPUT_WINDOW_VALUE, &(pDev->output_window)) < 0)
    {
      TRCOUT(TRC_ID_ERROR, "Unable to set the Output window rect");
      return signal_pending(current) ? -ERESTARTSYS : -EINVAL;
    }
  }

  TRCOUT(TRC_ID_STMCP_DEBUG, "");
  return 0;
}

/*******************************************************************************
 *  stmcp_set_input_crop - ioctl hepler function for S_CROP
 */
int stmcp_set_input_crop(stmcp_vbi_device_t *pDev,
                         const stmcp_crop_t *pCrop)
{
  TRCIN(TRC_ID_STMCP_DEBUG, "(%ld,%ld-%ldx%ld)",
      (long)pCrop->c.left, (long)pCrop->c.top,
      (long)pCrop->c.width, (long)pCrop->c.height);

  /* The input cropping window is then passed to the plane */
  pDev->input_window.x = pCrop->c.left;
  pDev->input_window.y = pCrop->c.top;
  pDev->input_window.width = pCrop->c.width;
  pDev->input_window.height = pCrop->c.height;

  if(pDev->hPlane)
  {
    if (stm_display_plane_set_compound_control
        (pDev->hPlane, PLANE_CTRL_INPUT_WINDOW_VALUE, &(pDev->input_window)) < 0)
    {
      TRCOUT(TRC_ID_ERROR, "Unable to set the Input window rect\n");
      return signal_pending(current) ? -ERESTARTSYS : -EINVAL;
    }
  }

  TRCOUT(TRC_ID_STMCP_DEBUG, "");
  return 0;
}
