/***********************************************************************
 * File: linux/kernel/drivers/stm/stmcp/vbi/vbi_driver.h
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 ***********************************************************************/

#ifndef __VBI_DRIVER_H
#define __VBI_DRIVER_H

#include "stmcp.h"

#if defined DEBUG
#  define PKMOD "vbi: "
#  define debug_msg(fmt,arg...) printk(PKMOD fmt,##arg)
#  define err_msg(fmt,arg...) printk(PKMOD fmt,##arg)
#  define info_msg(fmt,arg...) printk(PKMOD fmt,##arg)
#else
#  define debug_msg(fmt,arg...)
#  define err_msg(fmt,arg...)
#  define info_msg(fmt,arg...)
#endif

#define MAX_USER_BUFFERS    1
#define MAX_PIPELINES       3
#define MAX_PLANES          8

struct stmcp_vbi_device_s;

struct stmcp_queue_s
{
  rwlock_t         lock;
  struct list_head list; // of type struct streaming_buffer_s
};

typedef struct streaming_buffer_s
{
  struct list_head            node;
  stmcp_buffer_t              vidBuf;
  unsigned long               physicalAddr;
  const char                 *partname;
  char                       *cpuAddr;
  int                         mapCount;
  struct stmcp_vbi_device_s  *pDev;
  stm_display_buffer_t        bufferHeader;

  struct bpa2_part           *bpa2_part;

} streaming_buffer_t;

typedef struct stmcp_vbi_device_s
{
  char                        name[32];

  struct stmcp_queue_s        pendingStreamQ;
  struct stmcp_queue_s        completeStreamQ;

  const struct stmcore_display_pipeline_data
                             *display_pipeline;

  int                         currentOutputNum;
  int                         isStreaming;
  int                         isRegistered;

  stm_display_device_h        hDisplay;

  int                         source_id;
  stm_display_source_h        hSource;
  stm_display_source_queue_h  hQueueInterface;
  stm_rect_t                  input_window;

  int                         plane_id;
  stm_display_plane_h         hPlane;

  int                         output_id;
  stm_display_output_h        hOutput;
  stm_rect_t                  output_window;

  int                         queues_inited;
  wait_queue_head_t           wqBufDQueue;
  wait_queue_head_t           wqStreamingState;
  struct semaphore            devLock;

  streaming_buffer_t         *streamBuffers;
  int                         n_streamBuffers;

} stmcp_vbi_device_t;


/*
 * vbi_driver.c
 */
extern int stmcp_vbi_init_device        (stmcp_vbi_device_t      *device,
                                         const uint32_t           main_output_id);

extern void stmcp_vbi_destroy_device    (stmcp_vbi_device_t      *device);

extern int stmcp_vbi_mmap               (stmcp_vbi_device_t      *device,
                                         struct vm_area_struct   *vma);

extern long
stmcp_vbi_ioctl                         (stmcp_vbi_device_t       *device,
                                         unsigned int              f_flags,
                                         unsigned int              cmd,
                                         void                     *arg);

/*
 * vbi_buffers.c
 */
void stmcp_init_buffer_queues (stmcp_vbi_device_t * const device);

static inline int stmcp_has_queued_buffers(stmcp_vbi_device_t *pDev)
{
  return !list_empty(&pDev->pendingStreamQ.list);
}

static inline int stmcp_has_completed_buffers(stmcp_vbi_device_t *pDev)
{
  return !list_empty(&pDev->completeStreamQ.list);
}

int   stmcp_queue_buffer (stmcp_vbi_device_t *pDev, stmcp_buffer_t* pVidBuf);
int   stmcp_dequeue_buffer (stmcp_vbi_device_t *pDev, stmcp_buffer_t* pVidBuf);
void  stmcp_send_next_buffer_to_display (stmcp_vbi_device_t *pDev);
int   stmcp_streamon  (stmcp_vbi_device_t *pDev);
int   stmcp_streamoff (stmcp_vbi_device_t *pDev);

int   stmcp_set_output_crop (stmcp_vbi_device_t *pDev, const stmcp_crop_t *pCrop);
int   stmcp_set_input_crop  (stmcp_vbi_device_t *pDev, const stmcp_crop_t *pCrop);

int   stmcp_allocate_mmap_buffers   (stmcp_vbi_device_t *pDev, stmcp_requestbuffers_t* req);
int   stmcp_deallocate_mmap_buffers (stmcp_vbi_device_t *pDev);

streaming_buffer_t *stmcp_get_buffer_from_mmap_offset (stmcp_vbi_device_t *pDev, unsigned long offset);

void stmcp_delete_buffers_from_list (const struct list_head * const list);

#endif /* __VBI_DRIVER_H */
