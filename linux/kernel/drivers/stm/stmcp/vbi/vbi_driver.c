/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/vbi/stmcp_driver.c
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 * V4L2 video output device driver for ST SoC display subsystems.
 *
\***********************************************************************/

#include <linux/version.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/errno.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <media/v4l2-dev.h>
#include <media/v4l2-common.h>

#include <linux/semaphore.h>
#include <asm/page.h>

#include <stmdisplay.h>
#include <linux/stm/stmcoredisplay.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include "vbi_driver.h"


struct stmcp_ioctl_info_s
{
  unsigned int  cmd;
  char          str[32];
};

static const struct stmcp_ioctl_info_s
__attribute__((unused))
stmcp_ioctl_info[] =
{
  { 0xFFFFFFFF,                 "Unknown"                   }

, { STMCPIO_WAIT_FOR_VSYNC,     "STMCPIO_WAIT_FOR_VSYNC"    }
, { STMCPIO_GET_OUTPUT_INFO,    "STMCPIO_GET_OUTPUT_INFO"   }
, { STMCPIO_SET_CGMS_ANALOG,    "STMCPIO_SET_CGMS_ANALOG"   }

, { STMCPIO_REQBUFS,            "STMCPIO_REQBUFS"           }
, { STMCPIO_QUERYBUF,           "STMCPIO_QUERYBUF"          }
, { STMCPIO_QBUF,               "STMCPIO_QBUF"              }
, { STMCPIO_DQBUF,              "STMCPIO_DQBUF"             }
, { STMCPIO_S_CROP,             "STMCPIO_S_CROP"            }
, { STMCPIO_STREAMON,           "STMCPIO_STREAMON"          }
, { STMCPIO_STREAMOFF,          "STMCPIO_STREAMOFF"         }
};

static inline const char *
__attribute__((unused))
__cmd_to_str(const unsigned int cmd)
{
  int i;

  for(i=1; i < sizeof(stmcp_ioctl_info)/sizeof(struct stmcp_ioctl_info_s); i++)
  {
    if(stmcp_ioctl_info[i].cmd == cmd)
    {
      return stmcp_ioctl_info[i].str;
    }
  }

  return stmcp_ioctl_info[0].str;
}


static int
stmcp_vbi_get_output (stmcp_vbi_device_t *device)
{
  int ret = 0;
  const char *output_name = "";

  TRCIN(TRC_ID_STMCP, "");

  if (!device->hDisplay)
  {
    TRC(TRC_ID_ERROR, "Invalid device handle");
    return -EAGAIN;
  }

  if (device->hOutput)
  {
    TRC(TRC_ID_ERROR, "Output Is in use");
    return -EBUSY;
  }

  TRC(TRC_ID_STMCP_DEBUG, "Opening Output %d", device->output_id);
  if((ret = stm_display_device_open_output(device->hDisplay, device->output_id, &device->hOutput)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to open output %d", device->output_id);
    return ret;
  }

  if ((ret = stm_display_output_get_name(device->hOutput, &output_name)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to get output name (err=%d)", ret);
    return ret;
  }

  TRCOUT(TRC_ID_STMCP, "Using Ouptut %s", output_name);
  return ret;
}

static int
stmcp_vbi_put_output(stmcp_vbi_device_t *device)
{
  int ret = 0;
  const char *output_name = "";
  TRCIN(TRC_ID_STMCP, "");

  if (!device->hDisplay)
  {
    TRC(TRC_ID_ERROR, "Invalid device handle");
    return -EAGAIN;
  }

  if (!device->hOutput)
  {
    TRC(TRC_ID_ERROR, "Invalid output handle");
    return -ENODEV;
  }

  if (device->hPlane)
  {
    TRC(TRC_ID_ERROR, "Plane still in use");
    return -EBUSY;
  }

  if ((ret = stm_display_output_get_name(device->hOutput, &output_name)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to get output name (err=%d)", ret);
    return ret;
  }

  TRC(TRC_ID_STMCP, "Closing Output %s", output_name);
  stm_display_output_close(device->hOutput);
  device->hOutput = NULL;

  TRCOUT(TRC_ID_STMCP, "");
  return ret;
}


static int
stmcp_vbi_get_plane (stmcp_vbi_device_t *device)
{
  int ret = 0;
  const char *plane_name = "";
  uint32_t planeID, plane_status=0;
  stm_display_plane_h hPlane;

  TRCIN(TRC_ID_STMCP, "");

  if (!device->hDisplay)
  {
    TRC(TRC_ID_ERROR, "Invalid device handle");
    return -EAGAIN;
  }

  if (!device->hOutput)
  {
    TRC(TRC_ID_ERROR, "Invalid output handle");
    return -ENODEV;
  }

  for (planeID = 0; ; planeID++)
  {
    stm_plane_capabilities_t plane_caps;
    char *msg = "???";

    /* if the open fails, assumes no more planes! */
    if (stm_display_device_open_plane(device->hDisplay, planeID, &hPlane))
      break;

    if ((ret = stm_display_plane_get_name(hPlane, &plane_name)))
    {
      msg = "has no name";
      goto skip_plane;
    }

    if ((ret = stm_display_plane_get_capabilities(hPlane, &plane_caps)))
    {
      msg = "has no capabilities";
      goto skip_plane;
    }

    if (plane_caps & PLANE_CAPS_VBI)
    {
      if((ret = stm_display_plane_is_feature_applicable(hPlane, PLANE_FEAT_VIDEO_SCALING, NULL)) == 0)
      {
        msg = "video scaling is supported on this plane";
        goto skip_plane;
      }

      if((ret = stm_display_plane_get_status(hPlane, &plane_status)) < 0)
      {
        msg = "unable to get plane status";
        goto skip_plane;
      }

      if(plane_status & STM_STATUS_PLANE_CONNECTED_TO_SOURCE)
      {
        msg = "plane already in use";
        goto skip_plane;
      }

      ret = stm_display_plane_connect_to_output(hPlane, device->hOutput);
      if ((ret < 0) && ret != -EALREADY)
      {
        msg = "cannot be connected to this output";
        goto skip_plane;
      }
      device->plane_id = planeID;
      device->hPlane = hPlane;
      break;
    }
    else
    {
      msg = "not a VBI plane";
      goto skip_plane;
    }

skip_plane:
    stm_display_plane_close(hPlane);
    TRC(TRC_ID_STMCP_DEBUG, "plane (%d) %s skipped: %s! (%d)", planeID, plane_name, msg, ret);
  }

  if (!device->hPlane)
  {
    TRCOUT(TRC_ID_STMCP, "No available VBI plane for this device");
    return -ENODEV;
  }

  TRC(TRC_ID_STMCP_DEBUG, "Setting output window : (%ld,%ld-%ldx%ld)",
      (long)device->output_window.x, (long)device->output_window.y,
      (long)device->output_window.width, (long)device->output_window.height);

  /* Update the output window */
  if (stm_display_plane_set_compound_control
      (device->hPlane, PLANE_CTRL_OUTPUT_WINDOW_VALUE, &(device->output_window)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to set the Output window rect");
    return signal_pending(current) ? -ERESTARTSYS : -EINVAL;
  }

  TRCOUT(TRC_ID_STMCP, "Using Plane %s", plane_name);
  return 0;
}

static int
stmcp_vbi_put_plane(stmcp_vbi_device_t *device)
{
  int ret = 0;
  const char *plane_name = "";
  TRCIN(TRC_ID_STMCP, "");

  if (!device->hDisplay)
  {
    TRC(TRC_ID_ERROR, "Invalid device handle");
    return -EAGAIN;
  }

  if (!device->hOutput)
  {
    TRC(TRC_ID_ERROR, "Invalid output handle");
    return -ENODEV;
  }

  if (!device->hPlane)
  {
    TRC(TRC_ID_ERROR, "Invalid plane handle");
    return -ENODEV;
  }

  if ((ret = stm_display_plane_get_name(device->hPlane, &plane_name)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to get plane name (err=%d)", ret);
    return ret;
  }

  TRC(TRC_ID_STMCP, "Disconnecting Plane %s from output", plane_name);
  if((ret = stm_display_plane_disconnect_from_output(device->hPlane, device->hOutput)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to disconnect the plane from the output (err=%d)", ret);
    return ret;
  }

  TRC(TRC_ID_STMCP, "Closing Plane %s", plane_name);
  stm_display_plane_close(device->hPlane);
  device->hPlane = NULL;

  TRCOUT(TRC_ID_STMCP, "");
  return ret;
}


static int
stmcp_vbi_connect_plane_to_source (stmcp_vbi_device_t *device)
{
  int ret;
  stm_display_source_interface_params_t iface_params;
  uint32_t sourceID, source_status=0;
  const char *plane_name = "";

  TRCIN(TRC_ID_STMCP, "");

  if (!device->hDisplay)
  {
    TRC(TRC_ID_ERROR, "Invalid device handle");
    return -EAGAIN;
  }

  if (!device->hOutput)
  {
    TRC(TRC_ID_ERROR, "Invalid output handle");
    return -ENODEV;
  }

  if (!device->hPlane)
  {
    TRC(TRC_ID_ERROR, "Invalid plane handle");
    return -ENODEV;
  }

  if (device->hSource)
  {
    TRC(TRC_ID_ERROR, "Source already in use");
    return -EBUSY;
  }

  if ((ret = stm_display_plane_get_name(device->hPlane, &plane_name)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to get plane name (err=%d)", ret);
    return ret;
  }

  device->source_id = 0;
  for(sourceID=0; ; sourceID++) /* Iterate available sources */
  {
    if((ret = stm_display_device_open_source(device->hDisplay, sourceID, &(device->hSource))) < 0)
    {
      TRCOUT(TRC_ID_ERROR, "Failed to get a source for plane %s", plane_name);
      return ret;
    }

    if((ret = stm_display_source_get_status(device->hSource, &source_status)) < 0)
    {
      TRCOUT(TRC_ID_ERROR, "Failed to get a source status %s", plane_name);
      goto skip_source;
    }

    if(source_status & (STM_STATUS_QUEUE_LOCKED | STM_STATUS_SOURCE_CONNECTED))
    {
      TRCOUT(TRC_ID_STMCP_DEBUG, "Source already in use");
      goto skip_source;
    }

    iface_params.interface_type = STM_SOURCE_QUEUE_IFACE;
    if((ret = stm_display_source_get_interface(device->hSource, iface_params, (void *)&(device->hQueueInterface))) < 0)
    {
      TRC(TRC_ID_STMCP_DEBUG, "Failed to get queue interface");
      goto skip_source;
    }

    TRC(TRC_ID_STMCP_DEBUG, "Locking the source interface");
    if((ret = stm_display_source_queue_lock(device->hQueueInterface, false)) < 0)
    {
      TRC(TRC_ID_STMCP_DEBUG, "Failed to lock the source queue interface");
      goto skip_source;
    }

    if((ret = stm_display_plane_connect_to_source(device->hPlane, device->hSource)) == 0)
    {
      TRC(TRC_ID_STMCP, "Plane %s successfully connected to Source %u", plane_name, device->source_id);
      break;
    }

skip_source:
    /*
     * Cleanup and try the next source
     */
    if(device->hQueueInterface)
    {
      if((ret = stm_display_source_queue_unlock(device->hQueueInterface)) < 0)
        TRC(TRC_ID_STMCP_DEBUG, "Failed to unlock the source queue interface");
      if((ret = stm_display_source_queue_release(device->hQueueInterface)) < 0)
        TRC(TRC_ID_STMCP_DEBUG, "error while releasing source queue");
      device->hQueueInterface = NULL;
    }
    stm_display_source_close(device->hSource);
    device->hSource = NULL;
  }

  if(device->hSource)
  {
    TRC(TRC_ID_STMCP_DEBUG, "Setting input window : (%ld,%ld-%ldx%ld)",
        (long)device->input_window.x, (long)device->input_window.y,
        (long)device->input_window.width, (long)device->input_window.height);

    if (stm_display_plane_set_compound_control
        (device->hPlane, PLANE_CTRL_INPUT_WINDOW_VALUE, &(device->input_window)) < 0)
    {
      TRCOUT(TRC_ID_ERROR, "Unable to set the Input window rect\n");
      return signal_pending(current) ? -ERESTARTSYS : -EINVAL;
    }

    device->source_id = sourceID;
    TRCOUT(TRC_ID_STMCP, "");
    return 0;
  }

  TRCOUT(TRC_ID_ERROR, "Failed to connect plane %s to source", plane_name);
  return ret;
}

static int
stmcp_vbi_disconnect_plane_from_source(stmcp_vbi_device_t *device)
{
  int ret = 0;
  const char *plane_name = "";
  TRCIN(TRC_ID_STMCP, "");

  if (!device->hDisplay)
  {
    TRC(TRC_ID_ERROR, "Invalid device handle");
    return -EAGAIN;
  }

  if (!device->hOutput)
  {
    TRC(TRC_ID_ERROR, "Invalid output handle");
    return -ENODEV;
  }

  if (!device->hPlane)
  {
    TRC(TRC_ID_ERROR, "Invalid plane handle");
    return -ENODEV;
  }

  if ((ret = stm_display_plane_get_name(device->hPlane, &plane_name)) < 0)
  {
    TRCOUT(TRC_ID_ERROR, "Unable to get plane name (err=%d)", ret);
    return ret;
  }

  if (device->hSource)
  {
    if(device->hQueueInterface)
    {
      TRC(TRC_ID_STMCP, "Releasing the source interface %p", device->hQueueInterface);
      if((ret = stm_display_source_queue_flush(device->hQueueInterface, true)) < 0)
      {
        TRC( TRC_ID_ERROR, "Failed to flush the source queue");
        return ret;
      }
      if((ret = stm_display_source_queue_unlock(device->hQueueInterface)) < 0)
      {
        TRC( TRC_ID_ERROR, "Failed to unlock the source queue");
        return ret;
      }
      if((ret = stm_display_source_queue_release(device->hQueueInterface)) < 0)
      {
        TRC( TRC_ID_ERROR, "Failed to release the source queue");
        return ret;
      }
      device->hQueueInterface = NULL;
    }

    TRC(TRC_ID_STMCP, "Disconnecting plane %s from Source %p", plane_name, device->hSource);
    if((ret = stm_display_plane_disconnect_from_source(device->hPlane, device->hSource)) < 0)
    {
      TRC( TRC_ID_ERROR, "Failed to disconnect the plane from the source");
      return ret;
    }

    stm_display_source_close(device->hSource);
    device->hSource = NULL;
  }

  TRCOUT(TRC_ID_STMCP, "");
  return ret;
}


int
stmcp_vbi_init_device (stmcp_vbi_device_t *device,
                       const uint32_t main_output_id)
{
  TRCIN(TRC_ID_STMCP, "display=%p, output-id=%u", device->hDisplay, main_output_id);

  memset(device, 0, sizeof(stmcp_vbi_device_t));

  init_waitqueue_head(&(device->wqBufDQueue));
  init_waitqueue_head(&(device->wqStreamingState));
  sema_init(&(device->devLock), 1);

  stmcp_init_buffer_queues (device);

  device->output_id  = main_output_id;

  TRCOUT(TRC_ID_STMCP, "");
  return 0;
}


void
stmcp_vbi_destroy_device(stmcp_vbi_device_t *device)
{
  TRCIN(TRC_ID_STMCP, "");

  if(!device)
  {
    TRCOUT(TRC_ID_STMCP, "Invalid VBI device");
    return;
  }

  TRCOUT(TRC_ID_STMCP, "");
}


long stmcp_vbi_ioctl (stmcp_vbi_device_t    *device,
                      unsigned int           f_flags,
                      unsigned int           cmd,
                      void                  *arg)
{
  int ret = 0;
  struct semaphore *pLock = NULL;

  if (device == NULL)
    return -ENOTTY;

  pLock = &device->devLock;

  if (down_interruptible (pLock))
    return -ERESTARTSYS;

  TRCIN(TRC_ID_STMCP_DEBUG, "%s", __cmd_to_str(cmd));

  switch (cmd)
  {
    case STMCPIO_REQBUFS:
    {
      stmcp_requestbuffers_t * const req = arg;

      if (!stmcp_deallocate_mmap_buffers (device))
      {
        TRC(TRC_ID_ERROR, "%s: video buffer(s) still mapped, cannot change", __cmd_to_str(cmd));
        ret = -EBUSY;
      }
      else
      {
        if(req->count == 0)
        {
          stmcp_vbi_put_output(device);
        }
        else
        {
          if((ret = stmcp_vbi_get_output(device)) == 0)
          {
            if (!stmcp_allocate_mmap_buffers (device, req))
            {
              TRC(TRC_ID_ERROR, "%s: unable to allocate video buffers", __cmd_to_str(cmd));
              stmcp_vbi_put_output(device);
              ret = -ENOMEM;
            }
          }
        }
      }
    }
    break;

    case STMCPIO_QUERYBUF:
    {
      stmcp_buffer_t * const pBuf = arg;
      streaming_buffer_t * const pStrmBuf = &device->streamBuffers[pBuf->index];

      if (pBuf->index >= device->n_streamBuffers
          || !pStrmBuf->physicalAddr)
      {
        TRC(TRC_ID_ERROR, "%s: bad parameter stmcp_buffer.index: %u", __cmd_to_str(cmd), pBuf->index);
        ret = -EINVAL;
      }
      else
      {
        *pBuf = pStrmBuf->vidBuf;
      }
    }
    break;

    case STMCPIO_QBUF:
    {
      stmcp_buffer_t * const pBuf = arg;

      ret = stmcp_queue_buffer (device, pBuf);
    }
    break;

    case STMCPIO_DQBUF:
    {
      stmcp_buffer_t * const pBuf = arg;

      /*
       * We can release the driver lock now as stmcp_dequeue_buffer is safe
       * (the buffer queues have their own access locks), this makes
       * the logic a bit simpler particularly in the blocking case.
       */
      if (pLock)
        up (pLock);

      if ((f_flags & O_NONBLOCK) == O_NONBLOCK)
      {
        TRC(TRC_ID_STMCP_DEBUG, "%s: Non-Blocking dequeue", __cmd_to_str(cmd));
        if ((ret = stmcp_dequeue_buffer (device, pBuf)) != 0)
          ret = -EAGAIN;
      }
      else
      {
        TRC(TRC_ID_STMCP_DEBUG, "%s: Blocking dequeue", __cmd_to_str(cmd));
        ret = wait_event_interruptible (device->wqBufDQueue,
                                         stmcp_dequeue_buffer (device, pBuf));
      }

      goto exit;
    }
    break;

    case STMCPIO_S_CROP:
    {
      stmcp_crop_t * const crop = arg;

      switch (crop->type)
      {
        case STMCP_CROP_TYPE_OUTPUT:
          if ((ret = stmcp_set_output_crop(device, crop)) < 0)
            ret = -EINVAL;
          break;

        case STMCP_CROP_TYPE_SOURCE:
          if ((ret = stmcp_set_input_crop(device, crop)) < 0)
            ret = -EINVAL;
          break;

        default:
          TRC(TRC_ID_ERROR, "%s: invalid buffer type %d\n", __cmd_to_str(cmd), crop->type);
          ret = -EINVAL;
          break;
      }
    }
    break;

    case STMCPIO_STREAMON:
    {
      if((ret = stmcp_vbi_get_plane(device)) == 0)
      {
        if((ret = stmcp_vbi_connect_plane_to_source(device)) == 0)
        {
          if((ret = stmcp_streamon(device)) < 0)
            stmcp_vbi_disconnect_plane_from_source(device);
        }

        if(ret < 0)
          stmcp_vbi_put_plane(device);
      }
    }
    break;

    case STMCPIO_STREAMOFF:
    {
      if((ret = stmcp_streamoff(device)) == 0)
      {
        if((ret = stmcp_vbi_disconnect_plane_from_source(device)) == 0)
        {
          ret = stmcp_vbi_put_plane(device);
        }
      }
    }
    break;

    default:
      ret = -ENOTTY;
  }

  if (pLock)
    up (pLock);

exit:
  TRCOUT(TRC_ID_STMCP_DEBUG, "%s: ret=%d", __cmd_to_str(cmd), ret);
  return ret;
}

/**********************************************************
 * mmap helper functions
 **********************************************************/
static void
stmcp_vm_open (struct vm_area_struct *vma)
{
  streaming_buffer_t *pBuf = vma->vm_private_data;

  TRC(TRC_ID_STMCP_DEBUG, "%p [count=%d,vma=%08lx-%08lx]", pBuf,
             pBuf->mapCount, vma->vm_start, vma->vm_end);

  pBuf->mapCount++;
}

static void
stmcp_vm_close (struct vm_area_struct *vma)
{
  streaming_buffer_t * const pBuf = vma->vm_private_data;

  TRC(TRC_ID_STMCP_DEBUG, "%p [count=%d,vma=%08lx-%08lx]", pBuf,
             pBuf->mapCount, vma->vm_start, vma->vm_end);

  if (--pBuf->mapCount == 0)
  {
    TRC(TRC_ID_STMCP_DEBUG, "%p clearing mapped flag", pBuf);
    pBuf->vidBuf.flags &= ~STMCP_BUF_FLAG_MAPPED;
  }

  return;
}

static int
stmcp_vm_fault(struct vm_area_struct *vma, struct vm_fault *vmf)
{
  struct page *page;
  void *page_addr;
  unsigned long page_frame;
  unsigned long vaddr = (unsigned long)vmf->virtual_address;

  if (vaddr > vma->vm_end)
    return VM_FAULT_SIGBUS;

  /*
   * Note that this assumes an identity mapping between the page offset and
   * the pfn of the physical address to be mapped. This will get more complex
   * when the 32bit SH4 address space becomes available.
   */
  page_addr = (void*)((vaddr - vma->vm_start) + (vma->vm_pgoff << PAGE_SHIFT));

  page_frame = ((unsigned long) page_addr >> PAGE_SHIFT);

  if (!pfn_valid (page_frame))
    return VM_FAULT_SIGBUS;

  page = virt_to_page (__va (page_addr));

  get_page (page);

  vmf->page = page;
  return 0;
}

static struct vm_operations_struct stmcp_vm_ops_memory =
{
  .open   = stmcp_vm_open,
  .close  = stmcp_vm_close,
  .fault  = stmcp_vm_fault,
};

static struct vm_operations_struct stmcp_vm_ops_ioaddr =
{
  .open   = stmcp_vm_open,
  .close  = stmcp_vm_close,
  .fault  = NULL,
};

int
stmcp_vbi_mmap (stmcp_vbi_device_t     *device,
                struct vm_area_struct  *vma)
{
  streaming_buffer_t *pBuf;

  if (!(vma->vm_flags & VM_WRITE))
  {
    TRCOUT(TRC_ID_ERROR, "mmap app bug: PROT_WRITE please");
    return -EINVAL;
  }

  if (!(vma->vm_flags & VM_SHARED))
  {
    TRCOUT(TRC_ID_ERROR, "mmap app bug: MAP_SHARED please");
    return -EINVAL;
  }

  BUG_ON (device == NULL);

  if (down_interruptible (&device->devLock))
    return -ERESTARTSYS;

  TRC(TRC_ID_STMCP_DEBUG, "offset = 0x%08X", (int) vma->vm_pgoff);
  pBuf = stmcp_get_buffer_from_mmap_offset (device, vma->vm_pgoff*PAGE_SIZE);
  if (!pBuf)
  {
    /* no such buffer */
    TRCOUT(TRC_ID_STMCP_DEBUG, "Invalid offset parameter");
    goto err_badaddr;
  }

  TRC(TRC_ID_STMCP_DEBUG, "pBuf = 0x%08lx", (unsigned long) pBuf);

  if (pBuf->vidBuf.length != (vma->vm_end - vma->vm_start))
  {
    /* wrong length */
    TRCOUT(TRC_ID_ERROR, "Wrong length parameter");
    goto err_inval;
  }

  if (!pBuf->cpuAddr)
  {
    /* not requested */
    TRCOUT(TRC_ID_ERROR, "Buffer is not available for mapping");
    goto err_inval;
  }

  if (pBuf->mapCount > 0)
  {
    TRCOUT(TRC_ID_ERROR, "Buffer is already mapped");
    goto err_busy;
  }

  vma->vm_flags       |= VM_IO | VM_DONTDUMP | VM_DONTEXPAND | VM_LOCKED;
  vma->vm_page_prot    = pgprot_noncached (vma->vm_page_prot);
  vma->vm_private_data = pBuf;

  if (virt_addr_valid (pBuf->cpuAddr))
  {
    TRC(TRC_ID_STMCP_DEBUG, "remapping memory");
    vma->vm_ops = &stmcp_vm_ops_memory;
  }
  else
  {
    TRC(TRC_ID_STMCP_DEBUG, "remapping IO space");
    if (remap_pfn_range (vma, vma->vm_start, vma->vm_pgoff,
                         (vma->vm_end - vma->vm_start), vma->vm_page_prot))
    {
      up (&device->devLock);
      return -EAGAIN;
    }
    vma->vm_ops = &stmcp_vm_ops_ioaddr;
  }

  pBuf->mapCount = 1;
  pBuf->vidBuf.flags |= STMCP_BUF_FLAG_MAPPED;

  up (&device->devLock);

  return 0;

err_badaddr:
  up (&device->devLock);
  return -EFAULT;

err_busy:
  up (&device->devLock);
  return -EBUSY;

err_inval:
  up (&device->devLock);
  return -EINVAL;
}
