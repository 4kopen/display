/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/stmcp_module.c
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/platform_device.h>
#include <linux/string.h>
#include <linux/pm.h>
#include <linux/pm_runtime.h>

#include <asm/uaccess.h>
#include <asm/irq.h>

#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/err.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <linux/stm/stmcorecp.h>
#include <stmcp_client.h>

#include "vbi_driver.h"

extern struct file_operations stmcp_fops;
#if defined(SDK2_ENABLE_STMCP_ATTRIBUTES)
extern int stmcp_create_class_device_files(struct stmcp_class_device_s *stmcp_class_device);
#endif

#define STMCP_MAX_DEV 1
#define STMCP_MAX_ID  2

static dev_t stmcp_devt;
static int stmcp_count = STMCP_MAX_DEV;
static struct stmcp_device_s *stmcp_devices = NULL;

struct stm_display_cp_s stmcp_handles[STMCP_TYPE_COUNT][STMCP_MAX_ID];

struct of_device_id stmcp_match[] = {
    { .compatible = "st,stmcp" },
    {},
};

static void stmcp_unregister_device(struct stmcp_class_device_s *stmcp_class_device);

static void stmcp_vsync_cb(stm_vsync_context_handle_t context, uint32_t timingevent, stm_time64_t vsynctime)
{
  struct stmcp_vsync_data_s *vsync_data = (struct stmcp_vsync_data_s *)context;

  if (vsync_data->vsync_wait_count > 0) {
      vsync_data->vsync_wait_count--;

      if (vsync_data->vsync_wait_count == 0)
        wake_up_interruptible(&vsync_data->wait_queue);
  }
}


/******************************************************************************
 * stmcp module implementation
 */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("STMicroelectronics");
MODULE_DESCRIPTION("The STM analog copy protection driver");

#ifdef CONFIG_PM
static int stmcp_set_pm(struct device *dev, pm_message_t message)
{
  struct stmcp_device_s* stmcp = dev_get_drvdata(dev);

  if (stmcp == NULL)
  {
    TRCOUT(TRC_ID_ERROR, "STMCP is NULL !!");
    return -EINVAL;
  }

  switch (message.event) {
    case PM_EVENT_SUSPEND:
    case PM_EVENT_FREEZE:
      TRC(TRC_ID_POWER, "stmcp is %s...", (message.event == PM_EVENT_SUSPEND) ? "Suspending" : "Freezing");
      /*
       * close the display device
       */
      if(stmcp->display_device)
      {
        TRC(TRC_ID_STMCP_DEBUG, "Closing display device %d, hdl=%p", stmcp->display_device_id, stmcp->display_device);
        stm_display_device_close(stmcp->display_device);
        stmcp->display_device = NULL;
      }
      break;

    case PM_EVENT_RESUME:
    case PM_EVENT_RESTORE:
      TRC(TRC_ID_POWER, "stmcp is %s...", (message.event == PM_EVENT_RESUME) ? "Resuming" : "Restoring");
      /*
       * Open the display device before return
       */
      if(!stmcp->display_device)
      {
        if(stm_display_open_device(stmcp->display_device_id, &(stmcp->display_device))<0)
        {
          TRC(TRC_ID_ERROR, "STMCP cannot open display device = %u", stmcp->display_device_id);
          stmcp->display_device = NULL;
          return -ENODEV;
        }
        TRC(TRC_ID_STMCP_DEBUG, "Using display device %d, hdl=%p", stmcp->display_device_id, stmcp->display_device);
      }
      break;

    default:
      break;
  }

  return 0;
}
static int stmcp_pm_suspend(struct device *dev) {
  struct stmcp_device_s* stmcp = dev_get_drvdata(dev);

  /* Suspend the driver if not already the case before with DPM */
  if(!stmcp->rpm_suspended)
    return stmcp_set_pm(dev, PMSG_FREEZE);

  return 0;
}
static int stmcp_pm_resume(struct device *dev) {
  int ret = 0;
  struct stmcp_device_s* stmcp = dev_get_drvdata(dev);

  /* Bring Up the HW to Powered ON state */
  ret = stmcp_set_pm(dev, PMSG_RESTORE);
  if(stmcp->rpm_suspended)
    ret = stmcp_set_pm(dev, PMSG_SUSPEND);

  return ret;
}
#ifdef CONFIG_PM_RUNTIME
static int stmcp_pm_runtime_suspend(struct device *dev) {
  int ret = 0;
  struct stmcp_device_s* stmcp = dev_get_drvdata(dev);

  ret = stmcp_set_pm(dev, PMSG_SUSPEND);

  /* Update the Runtime power status */
  if(!ret)
    stmcp->rpm_suspended = 1;

  return ret;
}
static int stmcp_pm_runtime_resume(struct device *dev) {
  int ret = 0;
  struct stmcp_device_s* stmcp = dev_get_drvdata(dev);

  ret = stmcp_set_pm(dev, PMSG_RESUME);

  /* Update the Runtime power status */
  if(!ret)
    stmcp->rpm_suspended = 0;

  return ret;
}
#endif

static struct dev_pm_ops stmcp_pm_ops = {
    .suspend = stmcp_pm_suspend,
    .resume  = stmcp_pm_resume,
#ifdef CONFIG_PM_RUNTIME
    .runtime_suspend = stmcp_pm_runtime_suspend,
    .runtime_resume  = stmcp_pm_runtime_resume,
#endif
};
#endif


static int stmcp_create(struct stmcp_device_s *stmcp)
{
  memset(stmcp, 0, sizeof(*stmcp));

  mutex_init(&(stmcp->lock));
  stmcp->display_device_id = -1;

  return 0;
}

static void stmcp_del(struct stmcp_device_s* stmcp)
{
  if(!stmcp)
    return;

  while(stmcp->nr_display_pipelines > 0)
  {
    stmcp->nr_display_pipelines--;
    TRC(TRC_ID_STMCP_DEBUG, "Unregistering class device %d", stmcp->nr_display_pipelines);
    stmcp_unregister_device(&stmcp->stmcp_class_device[stmcp->nr_display_pipelines]);
  }

  /*
   * close the display device
   */
  if(stmcp->display_device)
  {
    TRC(TRC_ID_STMCP_DEBUG, "Closing display device %d, hdl=%p", stmcp->display_device_id, stmcp->display_device);
    stm_display_device_close(stmcp->display_device);
    stmcp->display_device = NULL;
  }

  mutex_destroy(&(stmcp->lock));
}

/****************************************************************************
 * Platform driver implementation
 */
static void stmcp_unregister_device(struct stmcp_class_device_s *stmcp_class_device)
{
  struct stmcp_vsync_data_s *vsync_data = stmcp_class_device->vsync_data;
  TRCIN(TRC_ID_STMCP, "");

  if(!IS_ERR(stmcp_class_device->class_device))
  {
    if(stmcp_class_device->vbi_device)
    {
      TRC(TRC_ID_STMCP_DEBUG, "Destroying VBI device %d", stmcp_class_device->dev_id);
      stmcp_vbi_destroy_device(stmcp_class_device->vbi_device);
    }

    if(vsync_data)
    {
      TRC(TRC_ID_STMCP_DEBUG, "Unregistering Vsync callbacks %d", stmcp_class_device->dev_id);
      stmcore_unregister_vsync_callback(stmcp_class_device->display_pipeline.display_runtime, &vsync_data->vsync_cb_info);
    }

    TRC(TRC_ID_STMCP_DEBUG, "Unregistering the device %d", stmcp_class_device->dev_id);
    device_unregister(stmcp_class_device->class_device);
    stmcp_class_device->class_device = NULL;

    TRC(TRC_ID_STMCP_DEBUG, "deleting class device %d", stmcp_class_device->dev_id);
    cdev_del(&stmcp_class_device->cdev);
  }

  TRCOUT(TRC_ID_STMCP, "");
}

static int __init stmcp_register_device(int id, struct stmcp_device_s *stmcp)
{
  struct stmcp_class_device_s *stmcp_class_device = &stmcp->stmcp_class_device[id];
  struct stmcp_vsync_data_s *vsync_data = NULL;
  TRCIN(TRC_ID_STMCP, "");

  TRC(TRC_ID_STMCP_DEBUG, "Initializing a new class device ops=%p", &stmcp_fops);
  cdev_init(&(stmcp_class_device->cdev), &stmcp_fops);

  stmcp_class_device->cdev.owner = THIS_MODULE;
  kobject_set_name(&(stmcp_class_device->cdev.kobj),"cp%d",id);

  TRC(TRC_ID_STMCP_DEBUG, "Adding the new class device");
  if(cdev_add(&(stmcp_class_device->cdev),MKDEV(MAJOR(stmcp_devt),id),1))
  {
    TRCOUT(TRC_ID_ERROR, "failed to add class device");
    return -ENODEV;
  }

  TRC(TRC_ID_STMCP_DEBUG, "Creating the class device");
  stmcp_class_device->class_device = device_create(stmcp_class_device->display_pipeline.class_device->class,
                                     stmcp_class_device->display_pipeline.class_device,
                                     stmcp_class_device->cdev.dev,
                                     stmcp_class_device,
                                     "%s", kobject_name(&stmcp_class_device->cdev.kobj));

  if(IS_ERR(stmcp_class_device->class_device))
  {
    stmcp_class_device->class_device = NULL;
    TRCOUT(TRC_ID_ERROR, "failed to create class device");
    return -ENODEV;
  }

#if defined(SDK2_ENABLE_STMCP_ATTRIBUTES)
  if(stmcp_create_class_device_files(stmcp_class_device))
    return -ENODEV;
#endif

  TRC(TRC_ID_STMCP_DEBUG, "Registering VSync callbacks %p for device %d", stmcp_class_device, id);
  vsync_data = kzalloc(sizeof(struct stmcp_vsync_data_s), GFP_KERNEL);
  if(!vsync_data)
  {
      return -ENOMEM;
  }
  INIT_LIST_HEAD(&(vsync_data->vsync_cb_info.node));
  vsync_data->vsync_cb_info.owner   = NULL;
  vsync_data->vsync_cb_info.context = vsync_data;
  vsync_data->vsync_cb_info.cb      = stmcp_vsync_cb;
  init_waitqueue_head(&vsync_data->wait_queue);
  if(stmcore_register_vsync_callback(stmcp_class_device->display_pipeline.display_runtime, &vsync_data->vsync_cb_info)<0)
  {
    TRC(TRC_ID_ERROR, "Cannot register stmcp vsync callback");
    return -ENODEV;
  }

  stmcp_class_device->vsync_data            = vsync_data;
  stmcp_class_device->dev_id                = id;
  stmcp_class_device->stmcp                 = stmcp;
  stmcp_class_device->master_output_id      = stmcp_class_device->display_pipeline.main_output_id;

  TRC(TRC_ID_STMCP_DEBUG, "Creating VBI device %d", id);
  stmcp_class_device->vbi_device = kzalloc(sizeof(stmcp_vbi_device_t), GFP_KERNEL);
  if (stmcp_class_device->vbi_device == 0)
  {
    TRCOUT(TRC_ID_ERROR, "Can't allocate memory for the new VBI device %d", id);
    return -ENOMEM;
  }
  stmcp_class_device->vbi_device->hDisplay = stmcp_class_device->stmcp->display_device;
  if(stmcp_vbi_init_device(stmcp_class_device->vbi_device, stmcp_class_device->master_output_id) < 0)
  {
    TRC(TRC_ID_ERROR, "Cannot create VBI device %d", id);
    stmcp_vbi_destroy_device(stmcp_class_device->vbi_device);
  }

  TRCOUT(TRC_ID_STMCP, "device created successfully");
  return 0;
}


static int stmcp_probe(struct platform_device *pdev)
{
  bool display_module = false;
  struct stmcp_device_s *stmcp;
  struct stmcp_class_device_s *stmcp_class_device;

  TRCIN(TRC_ID_STMCP, "probing device %d", pdev->id);

#ifdef CONFIG_STM_VIRTUAL_PLATFORM
  TRCOUT(TRC_ID_ERROR, "[VSOC] stmcp_probe - bypassed");
  return 0;
#endif

  if(pdev->id == -1)
  {
    pdev->id = 0;
  }
  else if(pdev->id >= STMCP_MAX_ID)
  {
    TRC(TRC_ID_ERROR, "ID is greater than the max stmcp number authorized");
    return -EINVAL;
  }

  stmcp = &stmcp_devices[pdev->id];
  if (stmcp_create(stmcp) < 0)
  {
    TRC(TRC_ID_ERROR, "Can't create the stmcp struct");
    return -EINVAL;
  }
  stmcp->pdev      = pdev;
  stmcp->device_id = pdev->id;
  stmcp->display_device_id = -1;
  stmcp->display_device = NULL;

  TRC(TRC_ID_STMCP_DEBUG, "Enabling PM runtime");
  pm_runtime_set_active(&pdev->dev);
  pm_runtime_enable(&pdev->dev);

  stmcp->nr_display_pipelines = 0;
  stmcp_class_device = stmcp->stmcp_class_device;

  while (stmcore_get_display_pipeline(stmcp->nr_display_pipelines, &stmcp_class_device->display_pipeline) == 0)
  {
    /* Update display device id */
    if(stmcp->nr_display_pipelines == 0)
    {
      stmcp->display_pipeline = stmcp_class_device->display_pipeline;
      stmcp->display_device_id = stmcp_class_device->display_pipeline.device_id;
      /* Try to get the display module */
      display_module = try_module_get(stmcp->display_pipeline.owner);
      if(!display_module)
      {
        TRC(TRC_ID_ERROR, "STMCP cannot get display module (id=%u)", stmcp->display_device_id);
        goto exit_nodev;
      }
      /*
       * Open the display device before return
       */
      if(stm_display_open_device(stmcp->display_device_id, &(stmcp->display_device))<0)
      {
        TRC(TRC_ID_ERROR, "STMCP cannot open display device = %u", stmcp->display_device_id);
        stmcp->display_device = NULL;
        goto exit_nodev;
      }
    }

    TRC(TRC_ID_STMCP_DEBUG, "Registering class device %d", stmcp->nr_display_pipelines);
    if(stmcp_register_device(stmcp->nr_display_pipelines, stmcp) < 0)
    {
      TRC(TRC_ID_ERROR, "Failed to register device %d !!", stmcp->nr_display_pipelines);
      goto exit_nodev;
    }

    if(++stmcp->nr_display_pipelines == STMCP_MAX_DISPLAY_PIPLINES)
    {
      TRC(TRC_ID_ERROR, "Max pipelines reached !!");
      goto exit_nodev;
    }
    stmcp_class_device++;
  }

  if(!stmcp->nr_display_pipelines)
  {
    TRC(TRC_ID_ERROR, "Could not find any display pipelines !!");
    goto exit_nodev;
  }

  platform_set_drvdata(pdev, stmcp);

  /* This will put the device in suspended state.
   * But this will close also the display device
   */
  TRC(TRC_ID_STMCP_DEBUG, "Suspending this device");
  pm_runtime_suspend(&pdev->dev);

  TRC(TRC_ID_STMCP_DEBUG, "Starting Netlink connector");
  stmcp_netlink_start(stmcp);

  TRCOUT(TRC_ID_STMCP, "probe finished");
  return 0;

exit_nodev:
  stmcp_del(stmcp);
  if(display_module)
    module_put(stmcp->display_pipeline.owner);
  TRC(TRC_ID_STMCP_DEBUG, "Disabling PM runtime");
  pm_runtime_disable(&pdev->dev);
  TRCOUT(TRC_ID_ERROR, "probe failed");
  return -ENODEV;
}


static int __exit stmcp_remove(struct platform_device *pdev)
{
  struct stmcp_device_s* stmcp = (struct stmcp_device_s *)platform_get_drvdata(pdev);

  TRCIN(TRC_ID_STMCP, "");

  if(!stmcp)
    return 0;

  TRC(TRC_ID_STMCP_DEBUG, "Stopping STMCP Netlink");
  stmcp_netlink_stop(stmcp);

  stmcp_del(stmcp);

  /*
   * Put the display module
   */
  module_put(stmcp->display_pipeline.owner);

  TRC(TRC_ID_STMCP_DEBUG, "Disabling PM runtime");
  pm_runtime_disable(&pdev->dev);

  platform_set_drvdata(pdev,NULL);

  TRC(TRC_ID_STMCP_DEBUG, "Freeing the stmcp_devices memory");
  kfree(stmcp_devices);

  return 0;
}


static void stmcp_shutdown(struct platform_device *pdev)
{
  /*
   * Missing pm_runtime_disable call in driver remove path caused
   * an "Unbalanaced pm_runtime_enable" warning when driver is reloaded.
   */
  TRC(TRC_ID_STMCP_DEBUG, "Disabling PM Runtime");
  pm_runtime_disable(&pdev->dev);
}


static struct platform_driver stmcp_driver = {
    .probe    = stmcp_probe,
    .shutdown = stmcp_shutdown,
    .remove   = __exit_p(stmcp_remove),
    .driver   = {
      .name     = "stmcp",
      .owner    = THIS_MODULE,
#ifdef CONFIG_PM
      .pm = &stmcp_pm_ops,
#endif
    },
   .driver.of_match_table = of_match_ptr(stmcp_match),
};

static int __init stmcp_init(void)
{
  stmcp_devices = kzalloc(sizeof(struct stmcp_device_s)*stmcp_count, GFP_KERNEL);
  if(!stmcp_devices)
  {
      return -ENOMEM;
  }
  if(alloc_chrdev_region(&stmcp_devt,0,1,"cp")<0)
  {
    TRC(TRC_ID_ERROR, "Unable to allocate device numbers");
    return -ENODEV;
  }
  platform_driver_register(&stmcp_driver);
  TRC(TRC_ID_STMCP, "stmcp module successfully loaded");

  memset(stmcp_handles, 0, sizeof(stmcp_handles));

  return 0;
}

static void __exit stmcp_exit(void)
{
  platform_driver_unregister(&stmcp_driver);
  unregister_chrdev_region(stmcp_devt,1);
  TRC(TRC_ID_STMCP, "stmcp module successfully unloaded");
}

module_init(stmcp_init);
module_exit(stmcp_exit);

int stmcp_get ( const stmcp_type_t type,
    const int               id,
    stm_display_cp_h *cp )
{
  struct stm_display_cp_s *handle;
  struct stmcp_device_s* stmcp_dev = &stmcp_devices[0];

  if ((id >= STMCP_MAX_ID) || (!stmcp_dev->pdev))
    return -ENODEV;

  handle = &stmcp_handles[type][id];
  handle->use_count++;

  if(handle->use_count == 1)
  {
    TRC(TRC_ID_STMCP_DEBUG, "First open for type=%d, id=%d : handle %p", (int)type, id, handle);
    handle->type = type;
    handle->id = id;
    handle->stmcp_dev = stmcp_dev;
    stm_coredisplay_magic_set(handle, VALID_STMCP_HANDLE);
    pm_runtime_get_sync(&stmcp_dev->pdev->dev);
  }

  TRC(TRC_ID_STMCP_DEBUG, "handle = %p", handle);

  *cp = (stm_display_cp_h) handle ;
  return 0;
}

void stmcp_put ( stm_display_cp_h cp )
{
  struct stm_display_cp_s *handle;
  struct stmcp_device_s* stmcp_dev;

  handle = (struct stm_display_cp_s *)cp;
  stmcp_dev = handle->stmcp_dev;
  handle->use_count--;

  if(handle->use_count == 0)
  {
    pm_runtime_put_sync(&stmcp_dev->pdev->dev);
    stm_coredisplay_magic_clear(handle);
  }
}
