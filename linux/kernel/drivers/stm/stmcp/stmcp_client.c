/***********************************************************************
 *
 * File: linux/kernel/drivers/stm/stmcp/stmcp_client.c
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 \***********************************************************************/

#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/freezer.h>
#include <linux/slab.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <asm/uaccess.h>
#include <linux/semaphore.h>

#include <vibe_os.h>
#include <vibe_debug.h>

#include <stmcp_client.h>


#define CN_STMCP_IDX    11
#define CN_STMCP_VAL    02

static const char *CN_STMCP_NAME = "cn_stmcp";
static const unsigned int STMCP_NETLINK_CMD_VALID = 0xA55AA55A;

#define CHECK_CMD_DATA_VALIDITY(cmd_data)                       \
({                                                              \
  int error = 0;                                                \
  if( cmd_data->flags != STMCP_NETLINK_CMD_VALID )              \
  {                                                             \
    TRC(TRC_ID_ERROR, "Invalid command data!!");                \
    error = -EBADMSG;                                           \
    cmd_data->error = error;                                    \
  }                                                             \
  error;                                                        \
})

#define SET_CMD_DATA_VALIDITY(cmd_data)                         \
  cmd_data.flags = STMCP_NETLINK_CMD_VALID;                     \

#define RESET_CMD_DATA(cmd_data)                                \
  memset(&cmd_data, 0, sizeof(stmcp_netlink_data_t));           \


static void stmcp_netlink_callback(struct cn_msg *msg, struct netlink_skb_parms *nsp)
{
  int i;
  stmcp_netlink_data_t    *cmd_data = (stmcp_netlink_data_t *)msg->data;
  struct stmcp_device_s   *stmcp    = (struct stmcp_device_s *)(cmd_data->handle);
  struct stmcp_netlink_s  *netlink  = stmcp->netlink;
  int sz = 0;
  char data_dbg[1024] = { 0 };

  mutex_lock(&(stmcp->lock));

  switch (cmd_data->cmd)
  {
    case STMCP_NETLINK_CMD_INIT :
    case STMCP_NETLINK_CMD_TERM :
    case STMCP_NETLINK_CMD_ENABLE :
    case STMCP_NETLINK_CMD_SET :
    case STMCP_NETLINK_CMD_DISABLE :
    {
      netlink->error = CHECK_CMD_DATA_VALIDITY(cmd_data);
      if( netlink->error < 0 )
      {
        TRC(TRC_ID_ERROR, "invalid command : error %d", netlink->error);
      }
      else
      {
        TRC(TRC_ID_STMCP_DEBUG, "Executed command %d", cmd_data->cmd);
        if(cmd_data->cmd < STMCP_NETLINK_CMD_INIT)
        {
          TRC(TRC_ID_STMCP_DEBUG, "hdl   = %p"      , cmd_data->handle);
          TRC(TRC_ID_STMCP_DEBUG, "type  = %d"      , cmd_data->type);
          TRC(TRC_ID_STMCP_DEBUG, "id    = %d"      , cmd_data->id);
          if(cmd_data->cmd == STMCP_NETLINK_CMD_SET)
          {
            TRC(TRC_ID_STMCP_DEBUG, "mode  = 0x%X"    , cmd_data->mode);
            if(cmd_data->len > 0)
            {
              sz += snprintf(&data_dbg[sz], sizeof(data_dbg), "data  = ");
              for (i = 0; (i < cmd_data->len) && (i<sizeof(cmd_data->data)); i++)
              {
                if(i == (cmd_data->len-1))
                  sz += snprintf(&data_dbg[sz], sizeof(data_dbg) - sz, "0x%02X", cmd_data->data[i]);
                else
                  sz += snprintf(&data_dbg[sz], sizeof(data_dbg) - sz, "0x%02X,", cmd_data->data[i]);
              }
              TRC(TRC_ID_STMCP_DEBUG, "len   = %d"      , cmd_data->len);
              TRC(TRC_ID_STMCP_DEBUG, "%s" , data_dbg);
            }
          }
          TRC(TRC_ID_STMCP_DEBUG, "flags = 0x%08X"  , cmd_data->flags);
          TRC(TRC_ID_STMCP_DEBUG, "error = %d"      , cmd_data->error);
        }
        netlink->error = cmd_data->error;
      }
      break;
    }
    default:
      netlink->error = -EBADMSG;
      TRC(TRC_ID_ERROR, "bad command %d : error %d", cmd_data->cmd, cmd_data->error);
      break;
  }

  mutex_unlock(&(stmcp->lock));

  /* Unlock the API */
  up (netlink->semlock);
}


static int stmcp_netlink_send_cmd( struct stmcp_netlink_s *netlink,
                                   const stmcp_netlink_data_t * const cmd_data )
{
  int i;
  int Error = 0;
  struct cn_msg *msg;
  char data_dbg[1024] = { 0 };
  int sz = 0;
  stmcp_netlink_data_t *msg_data;

  if(!netlink)
  {
    TRC(TRC_ID_ERROR, "stmcp_netlink_send_cmd: Invalid netlink handle !!");
    return -ENODEV;
  }

  if(!cmd_data)
  {
    TRC(TRC_ID_ERROR, "stmcp_netlink_send_cmd: Invalid data!!");
    return -EFAULT;
  }

  mutex_lock(&(netlink->lock));

  msg = kzalloc((sizeof(struct cn_msg) + sizeof(stmcp_netlink_data_t)), GFP_KERNEL);
  if(!msg)
  {
    TRC(TRC_ID_ERROR, "unable to allocate memory for link message!!");
    mutex_unlock(&(netlink->lock));
    return -ENOMEM;
  }

  msg->id.idx = netlink->id.idx;
  msg->id.val = netlink->id.val;
  msg->seq = 0;
  msg->ack = 0;
  msg->flags = 0;
  msg->len = sizeof(stmcp_netlink_data_t);
  msg_data = (stmcp_netlink_data_t *) msg->data;
  memcpy(msg_data, cmd_data, sizeof(stmcp_netlink_data_t));

  /* send a message to the server */
  TRC(TRC_ID_STMCP_DEBUG, "sending command %d"  , msg_data->cmd);
  if(cmd_data->cmd < STMCP_NETLINK_CMD_INIT)
  {
    TRC(TRC_ID_STMCP_DEBUG, "hdl   = %p"          , msg_data->handle);
    TRC(TRC_ID_STMCP_DEBUG, "type  = %d"          , msg_data->type);
    TRC(TRC_ID_STMCP_DEBUG, "id    = %d"          , msg_data->id);
    if(msg_data->cmd == STMCP_NETLINK_CMD_SET)
    {
      TRC(TRC_ID_STMCP_DEBUG, "mode  = 0x%X"    , msg_data->mode);
      if(msg_data->len > 0)
      {
        sz += snprintf(&data_dbg[sz], sizeof(data_dbg) - sz, "data  = ");
        for (i = 0; i < msg_data->len; i++)
        {
          if(i == (msg_data->len-1))
            sz += snprintf(&data_dbg[sz], sizeof(data_dbg) - sz, "0x%02X", msg_data->data[i]);
          else
            sz += snprintf(&data_dbg[sz], sizeof(data_dbg) - sz, "0x%02X,", msg_data->data[i]);
        }
        TRC(TRC_ID_STMCP_DEBUG, "len   = %d"      , msg_data->len);
        TRC(TRC_ID_STMCP_DEBUG, "%s" , data_dbg);
      }
    }
  }
  TRC(TRC_ID_STMCP_DEBUG, "flags = 0x%08X"  , msg_data->flags);

  TRC(TRC_ID_STMCP_DEBUG,  "Sending msg to Netlink {%u.%u}", msg->id.idx, msg->id.val);
  Error = cn_netlink_send(msg, 1, GFP_KERNEL);
  if (Error != 0)
  {
    netlink->error = Error;
    if(Error == -ESRCH)
      TRC(TRC_ID_STMCP, "netlink has no listeners !!");
    else
      TRC(TRC_ID_ERROR, "failed to send netlink message (Error=%d) !!", Error);
    goto exit;
  }

  /* wait for command completion */
  Error = down_timeout (netlink->semlock, netlink->timeout);

  TRC(TRC_ID_STMCP_DEBUG, "msg was successfully sent to Netlink {%u.%u}", msg->id.idx, msg->id.val);

exit:
  kfree(msg);
  mutex_unlock(&(netlink->lock));
  return Error;
}


static inline int stmcp_init ( struct stmcp_device_s *stmcp )
{
  int Error = 0;

  stmcp_netlink_data_t stmcp_socket_data;

  RESET_CMD_DATA(stmcp_socket_data);

  stmcp_socket_data.handle  = stmcp;
  stmcp_socket_data.cmd     = STMCP_NETLINK_CMD_INIT;

  SET_CMD_DATA_VALIDITY(stmcp_socket_data);
  Error = stmcp_netlink_send_cmd(stmcp->netlink, &stmcp_socket_data);

  /* STMCP daemon can be not yet started at this time so ignore the -ESRCH error */
  return (Error == -ESRCH ? 0 : Error);
}


static inline int stmcp_term ( struct stmcp_device_s *stmcp )
{
  int Error = 0;

  stmcp_netlink_data_t stmcp_socket_data;

  RESET_CMD_DATA(stmcp_socket_data);

  stmcp_socket_data.handle  = stmcp;
  stmcp_socket_data.cmd     = STMCP_NETLINK_CMD_TERM;

  SET_CMD_DATA_VALIDITY(stmcp_socket_data);
  Error = stmcp_netlink_send_cmd(stmcp->netlink, &stmcp_socket_data);

  /* STMCP daemon can be not yet started at this time so ignore the -ESRCH error */
  return (Error == -ESRCH ? 0 : Error);
}


int stmcp_netlink_start( struct stmcp_device_s *stmcp )
{
  int ret = 0;

  TRCIN(TRC_ID_STMCP, "");

  if(stmcp->netlink)
   return -EBUSY;

  stmcp->netlink = kzalloc(sizeof(struct stmcp_netlink_s), GFP_KERNEL);
  if (stmcp->netlink == 0) {
      return -ENOMEM;
  }

  stmcp->netlink->name      = CN_STMCP_NAME;
  stmcp->netlink->id.idx    = CN_STMCP_IDX;
  stmcp->netlink->id.val    = CN_STMCP_VAL;
  stmcp->netlink->error     = 0;

  stmcp->netlink->timeout   = ((20 * 60 * HZ)/1000);
  stmcp->netlink->semlock   = kmalloc(sizeof(struct semaphore),GFP_KERNEL);
  sema_init(stmcp->netlink->semlock,0);
  mutex_init(&(stmcp->netlink->lock));

  ret = cn_add_callback(&(stmcp->netlink->id), stmcp->netlink->name, stmcp_netlink_callback);
  if (ret)
  {
    TRC(TRC_ID_ERROR, "Unable to create connector callback, error %d", ret);
    mutex_destroy(&(stmcp->netlink->lock));
    return -EINVAL;
  }

  /* Initialize the daemon */
  ret = stmcp_init(stmcp);
  if (ret)
  {
    mutex_destroy(&(stmcp->netlink->lock));
    if(ret == -ESRCH)
      TRCOUT(TRC_ID_STMCP, "Unable to initialize daemon, error %d", ret);
    else
      TRCOUT(TRC_ID_ERROR, "Unable to initialize daemon, error %d", ret);
    return -EINVAL;
  }

  TRCOUT(TRC_ID_STMCP, "Netlink initialized with id={%u.%u}", stmcp->netlink->id.idx, stmcp->netlink->id.val);

  return ret;
}


void stmcp_netlink_stop( struct stmcp_device_s *stmcp )
{
  TRCIN(TRC_ID_STMCP, "");

  if(!stmcp->netlink)
    return;

  /* Terminate the daemon */
  stmcp_term(stmcp);

  cn_del_callback(&(stmcp->netlink->id));

  mutex_destroy(&(stmcp->netlink->lock));

  up (stmcp->netlink->semlock);

  kfree(stmcp->netlink->semlock);

  TRCOUT(TRC_ID_STMCP, "");
}


int stmcp_enable
    ( struct stmcp_device_s *stmcp
    , const stmcp_type_t type, const int id )
{
  int Error = 0;

  stmcp_netlink_data_t stmcp_socket_data;

  RESET_CMD_DATA(stmcp_socket_data);

  stmcp_socket_data.handle  = stmcp;
  stmcp_socket_data.cmd     = STMCP_NETLINK_CMD_ENABLE;
  stmcp_socket_data.type    = type;
  stmcp_socket_data.id      = id;

  SET_CMD_DATA_VALIDITY(stmcp_socket_data);
  Error = stmcp_netlink_send_cmd(stmcp->netlink, &stmcp_socket_data);

  return Error;
}


int stmcp_set
    ( struct stmcp_device_s *stmcp
    , const stmcp_type_t type, const int id
    , const unsigned int mode
    , const unsigned char * const data, const int len )
{
  int Error = 0;

  stmcp_netlink_data_t stmcp_socket_data;

  RESET_CMD_DATA(stmcp_socket_data);

  stmcp_socket_data.handle  = stmcp;
  stmcp_socket_data.cmd     = STMCP_NETLINK_CMD_SET;
  stmcp_socket_data.type    = type;
  stmcp_socket_data.id      = id;
  stmcp_socket_data.mode    = mode;
  if(data && (len > 0))
  {
    memcpy(stmcp_socket_data.data, data, len);
    stmcp_socket_data.len   = len;
  }

  SET_CMD_DATA_VALIDITY(stmcp_socket_data);
  Error = stmcp_netlink_send_cmd(stmcp->netlink, &stmcp_socket_data);

  return Error;
}


int stmcp_disable
    ( struct stmcp_device_s *stmcp
    , const stmcp_type_t type, const int id )
{
  int Error = 0;

  stmcp_netlink_data_t stmcp_socket_data;

  RESET_CMD_DATA(stmcp_socket_data);

  stmcp_socket_data.handle  = stmcp;
  stmcp_socket_data.cmd     = STMCP_NETLINK_CMD_DISABLE;
  stmcp_socket_data.type    = type;
  stmcp_socket_data.id      = id;

  SET_CMD_DATA_VALIDITY(stmcp_socket_data);
  Error = stmcp_netlink_send_cmd(stmcp->netlink, &stmcp_socket_data);

  return Error;
}
