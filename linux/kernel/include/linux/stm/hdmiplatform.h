/***********************************************************************
 *
 * File: linux/kernel/include/linux/stm/hdmiplatform.h
 * Copyright (c) 2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef HDMIPLATFORM_H_
#define HDMIPLATFORM_H_

struct stm_hdmi_platform_data {
  uint32_t              device_id;
  uint32_t              pipeline_id;
  uint32_t              link_device_id;
};

#endif /* HDMIPLATFORM_H_ */
