/***********************************************************************
 *
 * File: linux/kernel/include/linux/stm/cecplatform.h
 * Copyright (c) 2013 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef CECPLATFORM_H_
#define CECPLATFORM_H_

struct cec_config_data{
    unsigned int	address;
    unsigned int	mask;
};

struct stm_cec_platform_data {
    struct platform_device  *pdev;
    struct pinctrl  *pinctrl;
    unsigned int            num_config_data;
    struct cec_config_data  *config_data;
    struct reset_control    *reset;
    unsigned int clk_err_correction;
    uint8_t auto_ack_for_broadcast_tx;
};

#endif /* CECPLATFORM_H_ */
