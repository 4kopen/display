/***********************************************************************
 *
 * File: linux/kernel/include/linux/stm/linux-platform.h
 * Copyright (c) 2000-2010 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef _STM_LINUX_PLATFORM_H
#define _STM_LINUX_PLATFORM_H

#if defined(__cplusplus)
extern "C" {
#endif

/*! \file linux-platform.h
 *  \brief some specific types and macro definitions for Display
 */

/*
 * give access to errno for the API entrypoints
 */
#include <linux/errno.h>
#define ENOTSUP EOPNOTSUPP

/*
 * We cannot include <linux/types.h> here because its definition
 * of bool clashes with C++.
 */
typedef int            int32_t;
typedef short          int16_t;
typedef char           int8_t;
typedef unsigned int   uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char  uint8_t;
__extension__ typedef long long int64_t;
__extension__ typedef unsigned long long uint64_t;

/*
 * Some good old style C definitions for where we have imported code
 * from elsewhere.
 */
#define FALSE (1==2)
#define TRUE  (!FALSE)

#include <stm_registry.h>

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)
#define NULL 0
#else
#define NULL ((void *)0)
#endif

#endif /* _STM_LINUX_PLATFORM_H */
