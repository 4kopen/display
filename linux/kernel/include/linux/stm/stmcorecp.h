/***********************************************************************
 *
 * File: linux/kernel/include/linux/stm/stmcorecp.h
 * Copyright (c) 2015 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
\***********************************************************************/

#ifndef STMCORECP_H
#define STMCORECP_H

#include <linux/cdev.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
#include <linux/semaphore.h>
#include <linux/connector.h>

#include <stm_display.h>
#include <stm_display_cp.h>
#include <linux/stm/stmcoredisplay.h>

#include <vibe_debug.h>

#define VALID_STMCP_HANDLE  0x01230123
#define STMCP_MAX_DISPLAY_PIPLINES 8

struct stmcp_vsync_data_s {
  struct stmcore_vsync_cb               vsync_cb_info;
  uint32_t                              vsync_wait_count;
  wait_queue_head_t                     wait_queue;
};

struct stmcp_netlink_s {
  const char                          * name;
  struct cb_id                          id;
  int                                   error;

  unsigned long long                    timeout;
  struct mutex                          lock;
  struct semaphore                    * semlock;
};

struct stmcp_vbi_device_s;

struct stmcp_class_device_s {
  struct cdev                           cdev;
  struct device                       * class_device;

  int                                   dev_id;
  struct stmcore_display_pipeline_data  display_pipeline;
  int                                   master_output_id;

  struct stmcp_vsync_data_s           * vsync_data;
  struct stmcp_vbi_device_s           * vbi_device;
  struct stmcp_device_s               * stmcp;
};

struct stmcp_device_s {
  uint32_t                              device_id;
  struct platform_device               *pdev;
  stm_display_cp_h                      cp;

  struct stmcore_display_pipeline_data  display_pipeline;
  int                                   display_device_id;
  stm_display_device_h                  display_device;

  int                                   nr_display_pipelines;
  struct stmcp_class_device_s           stmcp_class_device[STMCP_MAX_DISPLAY_PIPLINES];

  struct mutex                          lock;

  struct stmcp_netlink_s               * netlink;

  /* Runtime Power status */
  int                                   rpm_suspended;
};

struct stm_display_cp_s
{
  struct stmcp_device_s * stmcp_dev;
  uint32_t                magic;

  stmcp_type_t            type;
  int                     id;

  int                     use_count;
};

#endif /* STMCORECP_H */
