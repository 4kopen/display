#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use Term::ANSIColor;

sub help_cmdline
{
    my $basename = basename($0);
    print "\nusage: $basename -f|-file <file> [-b|-before <string>] [-a|-after <string>] [-r|-replace] [-h|-help]\n";
    print "         -file    : give file\n";
    print "         -before  : give string to cut before\n";
    print "         -after   : give string to cut after\n";
    print "         -replace : overwrite input file\n";
    print "         -help    : print this help\n";
    print "\n";
    return 0;
}


my $help = undef;
my $file = undef;
my $before = undef;
my $after = undef;
my $replace = undef;

unless ( GetOptions(
             "file|f=s" => \$file,
             "before|b=s" => \$before,
             "after|after=s" => \$after,
             "replace|r" => \$replace,
             "help|h" => \$help,
         ))
{
    print "Error in command line arguments\n";
    help_cmdline();
    exit( -1 );
}

if ( defined $help )
{
    help_cmdline();
    exit( 0 );
}

if (( not defined $file ) or (( not defined $before ) and ( not defined $after )))
{
    print "Error in command line arguments\n";
    help_cmdline();
    exit( -1 );
}

my $cut = 0;
my $eof = 0;

if ( defined $before ) {
    $cut = 1;
}

if ( defined $replace ) {
    open( f_out, ">/tmp/$file" ) or die( "open /tmp/$file failed" );
}

open( f_in, "<$file" ) or die( "open $file failed" );

while ( defined( my $l = <f_in> ))
{
    chomp $l;

    if (( $eof == 0 ) and ( $cut == 1 ))
    {
        if ( $l =~ /(.*)$before(.*)/ )
        {
            if ( defined $2 ) {
                $l = $2;
            }

            $cut = 0;
        }
    }

    if (( $eof == 0 ) and ( $cut == 0 ))
    {
        if (( defined $after ) and ( $l =~ /(.*)$after(.*)/ ))
        {
            if ( defined $1 ) {
                $l = $1;
            }

            if ( defined $replace ) {
                print f_out "$l\n";
            }
            else {
                print "$l\n";
            }

            $cut = 1;
            $eof = 1;
        }
    }

    if ( $cut == 0 ) {
        if ( defined $replace ) {
            print f_out "$l\n";
        }
        else {
            print "$l\n";
        }
    }
}

close( f_in );

if ( defined $replace )
{
    close( f_out );
    system( "mv /tmp/$file $file" );
}

exit( 0 );
