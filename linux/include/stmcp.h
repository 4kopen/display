/***************************************************************************
This file is part of display_engine

COPYRIGHT (C) 2015 STMicroelectronics - All Rights Reserved

License type: GPLv2
display_engine is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License version 2 as published by
the Free Software Foundation.

display_engine is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with display_engine; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
The display_engine Library may alternatively be licensed under a proprietary
license from ST.

This file was created by STMicroelectronics on 2014-04-28
***************************************************************************/

#ifndef _STMCP_H
#define _STMCP_H

#include <linux/types.h>

/* Max User data lenght */
#define STMCPIO_MAX_USER_DATA               17 /* for MV 7.01 and CGMS TypeB */

typedef enum stmcp_output_standard_e
{
  STMCPIO_STD_UNKNOWN,
  /* analogue standards are a mess - the following values
           coincidentially coincide with the v4l2 defines... */
  STMCPIO_STD_PAL          = 0x0000000000000001LLU,
  /* */
  STMCPIO_STD_NTSC         = 0x0000000000001000LLU,
  /* */
  STMCPIO_STD_480P_60      = 0x0000000400000000LLU,
  STMCPIO_STD_480P_59_94   = 0x0000000800000000LLU,
  STMCPIO_STD_576P_50      = 0x0000001000000000LLU,
  /* */
  STMCPIO_STD_720P_59_94   = 0x0002000000000000ULL,
  STMCPIO_STD_720P_60      = 0x0001000000000000ULL,
  STMCPIO_STD_1080I_59_94  = 0x0000400000000000ULL,
  STMCPIO_STD_1080I_60     = 0x0000200000000000ULL,
} stmcp_output_standard_t;

typedef enum stmcp_output_format_e
{
  STMCPIO_OUTPUT_ANALOGUE_UNKNOWN,
  STMCPIO_OUTPUT_ANALOGUE_RGB     =  (1L<<0),
  STMCPIO_OUTPUT_ANALOGUE_YUV     =  (1L<<1),
  STMCPIO_OUTPUT_ANALOGUE_YC      =  (1L<<2),
  STMCPIO_OUTPUT_ANALOGUE_CVBS    =  (1L<<3)
} stmcp_output_format_t;

typedef struct stmcp_output_info_s
{
  stmcp_output_standard_t std;
  __u32                   fmt;
} stmcp_output_info_t;

/*
 * CGMS constants
 */
/* Analog Protection System for CGMS */
typedef enum stmcpio_cgms_sd_mode_e
{
  STMCPIO_CGMS_SD_COPY_FREELY                 /*!< Unlimited copies may be made of the content                                    */
, STMCPIO_CGMS_SD_COPY_NOMORE                 /*!< One generation of copies has already been made; no further copying is allowed  */
, STMCPIO_CGMS_SD_COPY_ONCE                   /*!< One generation of copies may be made                                           */
, STMCPIO_CGMS_SD_COPY_NEVER                  /*!< No copies may be made of the content                                           */
, STMCPIO_CGMS_SD_USER_DEFINED                /*!< User defined mode : specific 5bits data can be passed in data field            */
} stmcpio_cgms_sd_mode_t;

/* VBI buffers : Used for CGMS-HD */
typedef struct stmcp_requestbuffers_s
{
  __u32 count;                                /*!< The number of buffers requested or granted :
                                                   Maxcount =1 for allocation, Count = 0 for deallocation                         */
} stmcp_requestbuffers_t;

#define STMCP_BUF_FLAG_MAPPED       (1L<<0)   /*!< The buffer resides in device memory and has been mapped into the application's
                                                   address space, see Section 3.2> for details. Drivers set or clear this flag
                                                   when the VIDIOC_QUERYBUF, VIDIOC_QBUF or VIDIOC_DQBUF ioctl is called.
                                                   Set by the driver.                                                             */
#define STMCP_BUF_FLAG_QUEUED       (1L<<1)   /*!< Drivers set or clear this flag when the VIDIOC_QUERYBUF ioctl is called.
                                                   After (successful) calling the VIDIOC_QBUF ioctl it is always set
                                                   and after VIDIOC_DQBUF always cleared.                                         */
#define STMCP_BUF_FLAG_DONE         (1L<<2)   /*!< Drivers set or clear this flag when the VIDIOC_QUERYBUF ioctl is called.
                                                   After calling the VIDIOC_QBUF or VIDIOC_DQBUF it is always cleared.
                                                   The #define STMCP_BUF_FLAG_QUEUED and #define STMCP_BUF_FLAG_DONE flag are
                                                   mutually exclusive. They can be both cleared however, then the buffer
                                                   is in "dequeued" state, in the application domain to say so.                   */
typedef struct stmcp_buffer_s
{
  __u32 index;                                /*!< Number of the buffer, set by the CGMS plugin                                   */
  __u32 flags;                                /*!< Flags set by the application or driver                                         */
  __u32 length;                               /*!< Size of the buffer in bytes                                                    */
  __u32 offset;                               /*!< Offset of the buffer from the start of the device memory.
                                                   Serves as parameter to the mmap() function for CGMS plugin                     */
} stmcp_buffer_t;

typedef enum stmcp_crop_type_e
{
  STMCP_CROP_TYPE_SOURCE
, STMCP_CROP_TYPE_OUTPUT
} stmcp_crop_type_t;

typedef struct stmcp_rect_s
{
  __s32 left;                                 /*!< Horizontal offset of the top, left corner of the rectangle, in pixels.         */
  __s32 top;                                  /*!< Vertical offset of the top, left corner of the rectangle, in pixels.
                                                   Offsets increase to the right and down.                                        */
  __u32 width;                                /*!< Width of the rectangle, in pixels.                                             */
  __u32 height;                               /*!< Height of the rectangle, in pixels.                                            */
} stmcp_rect_t;

typedef struct stmcp_crop_s
{
  stmcp_crop_type_t type;                     /*!< Type of the data stream, set by the application.                               */
  stmcp_rect_t      c;                        /*!< Cropping rectangle, set by the application.                                    */
} stmcp_crop_t;

/*
 * Internal IOs : Used only by STMCP Daemon
 */

/* Timing info and synchro */
#define STMCPIO_WAIT_FOR_VSYNC              _IOW  ('H', 01, unsigned long long)
#define STMCPIO_GET_OUTPUT_INFO             _IOWR ('H', 02, stmcp_output_info_t)

/* CGMS-SD Support */
#define STMCPIO_SET_CGMS_ANALOG             _IOW  ('H', 10, unsigned long long)

/* Specific IO for CGMS-HD support : this is equivalent to V4L2 IOs */
#define STMCPIO_REQBUFS                     _IOW  ('H', 10, stmcp_requestbuffers_t)
#define STMCPIO_QUERYBUF                    _IOW  ('H', 11, stmcp_buffer_t)
#define STMCPIO_QBUF                        _IOW  ('H', 12, stmcp_buffer_t)
#define STMCPIO_DQBUF                       _IOW  ('H', 13, stmcp_buffer_t)
#define STMCPIO_S_CROP                      _IOW  ('H', 14, stmcp_crop_t)
#define STMCPIO_STREAMON                    _IO   ('H', 15)
#define STMCPIO_STREAMOFF                   _IO   ('H', 16)


/*
 * Exported IOs : To be confirmed if required
 */
#define STMCPIO_ENABLE                      _IO  ('H', 100)
#define STMCPIO_DISABLE                     _IO  ('H', 101)
#define STMCPIO_SET                         _IO  ('H', 102)

#endif /* _STMCP_H */
