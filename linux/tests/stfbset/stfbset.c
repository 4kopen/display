/***********************************************************************
 *
 * File: linux/tests/stfbcontrol/stfbset.c
 * Copyright (c) 2007 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 \***********************************************************************/

#include <sys/types.h>
#include <sys/ioctl.h>

#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include <string.h>

#include <linux/fb.h>

/*
 * This test builds against the version of stmfb.h in this source tree, rather
 * than the one that is shipped as part of the kernel headers package for
 * consistency. Normal user applications should use <linux/stmfb.h>
 */
#include <linux/kernel/drivers/video/stmfb.h>

char version[]="1.1";

void usage(void)
{
  printf("Usage: stfbset [options]\n");
  printf("\t-a,--transparency=[0-255]\n");
  printf("\t-b,--hdmi-deepcolour-bitdepth=[24|30|36|48]\n");
  printf("\t-c,--color-key=[enable=[0x0|0x1],inverted=[0x0|0x1],min_colour_key,max_colour_key]\n");
  printf("\t-B,--enable-hd-dac-filters=[y|Y|n|N]\n");
  printf("\t-d,--disable-output=[cvbs|svideo|yuv|rgb|hdmi|dvo]\n");
  printf("\t-e,--enable-output=[cvbs|svideo|yuv|rgb|hdmi|dvo]\n");
  printf("\t-f,--fb=DEVNAME (default=/dev/fb0)\n");
  printf("\t-g,--gain=[0-255]\n");
  printf("\t-h,--help\n");
  printf("\t-i,--dvo-data-clock-polarity=[opposite|same]\n");
  printf("\t-j,--input-window=[pos_x,pos_y,width,height\n");
  printf("\t-J,--output-window=[pos_x,pos_y,width,height\n");
  printf("\t-p,--premultipled-alpha\n");
  printf("\t-n,--non-premultiplied-alpha\n");
  printf("\t-o,--fullscreen-window=[0-1]\n");
  printf("\t-s,--show\n");
  printf("\t-C,--ycbcr-colourspace=[auto|601|709|bt2020]\n");
  printf("\t-D,--dvo-outputmode=[rgb|yuv|422|itu656|24bit_yuv]\n");
  printf("\t-F,--flicker-filter=[off|simple|adaptive]\n");
  printf("\t-H,--hdmi-colourspace=[rgb|yuv|422|420]\n");
  printf("\t-M,--mixer-background=0xAARRGGBB\n");
  printf("\t-m,--mixer-colorspace=[auto|601|709|2020]\n");
  printf("\t-P,--vps-data=[vpsenable=[0x0|0x1],vpsdata=[vpsdata[0],vpsdata[1],vpsdata[2],vpsdata[3],vpsdata[4],vpsdata[5]]]\n");
  printf("\t-R,--rescale-colour=[fullrange|videorange]\n");
  printf("\t-S,--dvo-sync=[emb|ext]\n");
  printf("\t-U,--full-range=[analogue|hdmi|dvo]\n");
  printf("\t-V,--video-range=[analogue|hdmi|dvo]\n");
  printf("\t-w,--wss-insertion=[enable|disable]\n");
  printf("\t-x,--cgms-insertion=[enable|disable]\n");
  printf("\t-3,--3d-mode=[2D|sidebyside|topandbottom]\n");
  printf("\t-Y,--hdr-format=[SDR|HDR|ST2084],st2086_metadata=[display_primaries_xy_0,display_primaries_xy_1,display_primaries_xy_2,white_point_xy,mastering_luminance],hdr_metadata=[maxCLL,maxFALL]\n");
  printf("\t-y,--hdr-mode=[auto|manual]\n");
  printf("\t-z,--enable-cc-field=[0-3]\n");
  printf("\t-I,--stmfbio_activate=[immediate|change]\n");
  printf("\t-v,--version\n");
}

static void exit_with_error_message(char *err_msg)
{
  perror(err_msg);
  exit(1);
}


void show_plane_info(const struct stmfbio_planeinfo2 *planeInfo)
{
  printf("Input Window    : %dx%d@%d-%d\n", planeInfo->config.source.dim.w,
         planeInfo->config.source.dim.h, planeInfo->config.source.x,
         planeInfo->config.source.y);
  printf("Output Window   : %dx%d@%d-%d\n", planeInfo->config.dest.dim.w,
         planeInfo->config.dest.dim.h, planeInfo->config.dest.x,
         planeInfo->config.dest.y);
}

void show_info(const struct stmfbio_var_screeninfo_ex2 *varEx,
               const struct stmfbio_output_configuration *outputConfig)
{
  printf("Color Keying    : ");
  if(varEx->caps & STMFBIO_VAR_CAPS_COLOURKEY)
  {
    if(varEx->colourKeyFlags & STMFBIO_COLOURKEY_FLAGS_ENABLE)
    {
      printf("On [min = 0x%08x - max = 0x%08x - ", varEx->min_colour_key, varEx->max_colour_key);
      if(varEx->colourKeyFlags & STMFBIO_COLOURKEY_FLAGS_INVERT)
      {
        printf("Inverse]\n");
      }
      else
      {
        printf("Normal]\n");
      }
    }
    else
    {
      printf("Off\n");
    }
  }
  else
  {
    printf("Unsupported\n");
  }

  printf("Flicker Filter  : ");
  if(varEx->caps & STMFBIO_VAR_CAPS_FLICKER_FILTER)
  {
    switch(varEx->ff_state)
    {
      case STMFBIO_FF_OFF:
        printf("Off\n");
        break;
      case STMFBIO_FF_SIMPLE:
        printf("Simple\n");
        break;
      case STMFBIO_FF_ADAPTIVE:
        printf("Adaptive\n");
        break;
    }
  }
  else
  {
    printf("Unsupported\n");
  }

  printf("Mixer Colour    : ");
  if(outputConfig->caps & STMFBIO_OUTPUT_CAPS_MIXER_BACKGROUND)
  {
    printf("0x%08x\n",outputConfig->mixer_background);
  }
  else
  {
    printf("None\n");
  }


  printf("Transparency    : ");
  if(varEx->caps & STMFBIO_VAR_CAPS_OPACITY)
    printf("%d\n",varEx->opacity);
  else
    printf("Unsupported\n");

  printf("Gain            : ");
  if(varEx->caps & STMFBIO_VAR_CAPS_GAIN)
    printf("%d\n",varEx->gain);
  else
    printf("Unsupported\n");

  printf("Blend Mode      : %s\n",varEx->premultiplied_alpha?"Premultiplied":"Non-premultipled");
  printf("Rescale Mode    : %s\n",varEx->rescale_colour_to_video_range?"Video range":"Full range");

  printf("Analogue Out    : ");
  if(outputConfig->caps & STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG)
  {
    if(outputConfig->analogue_config & STMFBIO_OUTPUT_ANALOGUE_RGB)
      printf("RGB ");
    if(outputConfig->analogue_config & STMFBIO_OUTPUT_ANALOGUE_YPrPb)
      printf("YUV ");
    if(outputConfig->analogue_config & STMFBIO_OUTPUT_ANALOGUE_YC)
      printf("S-Video ");
    if(outputConfig->analogue_config & STMFBIO_OUTPUT_ANALOGUE_CVBS)
      printf("CVBS ");

    if((outputConfig->analogue_config & STMFBIO_OUTPUT_ANALOGUE_MASK)==0)
      printf("Disabled ");

    if(outputConfig->analogue_config & STMFBIO_OUTPUT_ANALOGUE_CLIP_FULLRANGE)
      printf("(Full range");
    else
      printf("(Video range");

    switch(outputConfig->analogue_config & STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_MASK)
    {
      case STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_AUTO:
        printf(", Auto colourspace");
        break;
      case STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_601:
        printf(", 601 colourspace");
        break;
      case STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_709:
        printf(", 709 colourspace");
        break;
      case STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_BT2020:
        printf(", BT2020 colourspace");
        break;
    }

    if( outputConfig->mixer_colorspace == STMFBIO_OUTPUT_MIXER_COLORSPACE_AUTO)
    {
      printf(", Mixer Auto colourspace");
    }
    else if( outputConfig->mixer_colorspace == STMFBIO_OUTPUT_MIXER_COLORSPACE_601)
    {
      printf(", Mixer 601 colourspace");
    }
    else if( outputConfig->mixer_colorspace == STMFBIO_OUTPUT_MIXER_COLORSPACE_709)
    {
      printf(", Mixer 709 colourspace");
    }
    else if( outputConfig->mixer_colorspace == STMFBIO_OUTPUT_MIXER_COLORSPACE_2020)
    {
      printf(", Mixer BT2020 colourspace");
    }

    printf(")\n");
  }
  else
  {
    printf("None\n");
  }

  printf("HDMI Out        : ");
  if(outputConfig->caps & STMFBIO_OUTPUT_CAPS_HDMI_CONFIG)
  {
    printf("%s",outputConfig->hdmi_config&STMFBIO_OUTPUT_HDMI_DISABLED?"Disabled ":"Enabled ");

    if(!(outputConfig->hdmi_config & STMFBIO_OUTPUT_HDMI_422))
    {
      switch(outputConfig->hdmi_config & STMFBIO_OUTPUT_HDMI_COLOURDEPTH_MASK)
      {
        case STMFBIO_OUTPUT_HDMI_COLOURDEPTH_30BIT:
          printf("(30bit ");
          break;
        case STMFBIO_OUTPUT_HDMI_COLOURDEPTH_36BIT:
          printf("(36bit ");
          break;
        case STMFBIO_OUTPUT_HDMI_COLOURDEPTH_48BIT:
          printf("(48bit ");
          break;
        default:
          printf("(24bit ");
          break;
      }

      if(outputConfig->hdmi_config & STMFBIO_OUTPUT_HDMI_420)
        printf(" YUV 4:2:0) ");
      else
      {
        if(outputConfig->hdmi_config & STMFBIO_OUTPUT_HDMI_YUV)
          printf(" YUV 4:4:4 ");
        else
          printf(" RGB) ");
      }
    }
    else
    {
      printf("(YUV 4:2:2) ");
    }

    if(outputConfig->hdmi_config & STMFBIO_OUTPUT_HDMI_CLIP_FULLRANGE)
      printf("(Full range)");
    else
      printf("(Video range)");

    printf("\n");
  }
  else
  {
    printf("None\n");
  }

  printf("DVO Out         : ");
  if(outputConfig->caps & STMFBIO_OUTPUT_CAPS_DVO_CONFIG)
  {
    printf("%s",outputConfig->dvo_config&STMFBIO_OUTPUT_DVO_DISABLED?"Disabled ":"Enabled ");
    switch(outputConfig->dvo_config & STMFBIO_OUTPUT_DVO_MODE_MASK)
    {
      case STMFBIO_OUTPUT_DVO_YUV_444_16BIT:
        printf("(YUV 16bit 4:4:4) ");
        break;
      case STMFBIO_OUTPUT_DVO_YUV_444_24BIT:
        printf("(YUV 24bit 4:4:4) ");
        break;
      case STMFBIO_OUTPUT_DVO_YUV_422_16BIT:
        printf("(YUV 16bit 4:2:2) ");
        break;
      case STMFBIO_OUTPUT_DVO_ITUR656:
        printf("(ITU-R 656) ");
        break;
      case STMFBIO_OUTPUT_DVO_RGB_24BIT:
        printf("(RGB 24bit) ");
        break;
    }
    if(outputConfig->dvo_config & STMFBIO_OUTPUT_DVO_CLIP_FULLRANGE)
      printf("(Full range)");
    else
      printf("(Video range)");

    printf("\n");
  }
  else
  {
    printf("None\n");
  }

}

void show_hdr_info(const struct stmfbio_var_screeninfo_ex2 *varEx,
               const struct stmfbio_output_configuration *outputConfig)
{
  printf("Output HDR Mode : ");
  if(outputConfig->caps & STMFBIO_OUTPUT_CAPS_MIXER_HDR_MODE)
  {
    if(outputConfig->hdr_mode == STMFBIO_OUTPUT_HDR_MODE_MANUAL)
      printf("Manual ");
    else if(outputConfig->hdr_mode == STMFBIO_OUTPUT_HDR_MODE_AUTO)
      printf("Auto ");
    else
      printf("Unknown ");

    printf("\n");
  }

  printf("Dynamic Range   : ");
  if(outputConfig->caps & STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT)
  {
    if(outputConfig->hdr_format.eotf_type == STMFBIO_EOTF_GAMMA_SDR)
      printf("Gamma SDR ");
    else if(outputConfig->hdr_format.eotf_type == STMFBIO_EOTF_GAMMA_HDR)
      printf("Gamma HDR ");
    else if(outputConfig->hdr_format.eotf_type == STMFBIO_EOTF_GAMMA_HDR_HLG)
      printf("HLG HDR ");
    else if(outputConfig->hdr_format.eotf_type == STMFBIO_EOTF_SMPTE_ST2084)
      printf("SMPTE ST2084 ");
    else
      printf("Unknown HDR EOTF type ");

    printf("\n");

    printf("                : ST2086 Metadata : ");
    if(outputConfig->hdr_format.is_st2086_metadata_present)
    {
      const stmfbio_st2086_metadata_t *st2086_metadata = &outputConfig->hdr_format.st2086_metadata;

      printf("Present : ");
      printf("- display_primaries_x[3]          = {%d,%d,%d}\n", st2086_metadata->display_primaries_x_0,
             st2086_metadata->display_primaries_x_1, st2086_metadata->display_primaries_x_2);
      printf("                                              ");
      printf("- display_primaries_y[3]          = {%d,%d,%d}\n", st2086_metadata->display_primaries_y_0,
             st2086_metadata->display_primaries_y_1, st2086_metadata->display_primaries_y_2);
      printf("                                              ");
      printf("- white_points[2]                 = {%d,%d}\n", st2086_metadata->white_point_x, st2086_metadata->white_point_y);
      printf("                                              ");
      printf("- display_mastering_luminances[2] = {%d,%d}\n", st2086_metadata->max_display_mastering_luminance,
             st2086_metadata->min_display_mastering_luminance);
    }
    else
      printf("Not Present ");
    printf("\n");

    printf("                : HDR Metadata    : ");
    if(outputConfig->hdr_format.is_hdr_metadata_present)
    {
      const stmfbio_hdr_metadata_t *hdr_metadata = &outputConfig->hdr_format.hdr_metadata;

      printf("Present  :");
      printf("maxCLL = %d | maxFALL = %d", hdr_metadata->maxCLL, hdr_metadata->maxFALL);
    }
    else
      printf("Not Present ");
    printf("\n");
  }
  else
  {
    printf("Unsupported\n");
  }
}

#define OUT_CVBS   (1<<0)
#define OUT_SVIDEO (1<<1)
#define OUT_YUV    (1<<2)
#define OUT_RGB    (1<<3)
#define OUT_HDMI   (1<<4)
#define OUT_DVO    (1<<5)

#define EXTERNAL_SYNC (1 << 0)
#define EMBEDDED_SYNC (1 << 1)
#define DVO_DATA_CLOCK_NOT_INVERTED (1 << 0)
#define DVO_DATA_CLOCK_INVERTED     (1 << 1)

int main(int argc, char **argv)
{
  int fbfd;
  int change_alpha    = 0,     alpha       = 0;
  int change_gain     = 0,     gain        = 0;
  int change_blend    = 0,     blend       = 0;
  int change_dvo_mode = 0,     dvo_mode    = 0;
  int change_hdmi_colour = 0,  hdmi_colour = 0;
  int change_hdmi_bitdepth = 0, hdmi_bitdepth = 0;
  int change_rescale  = 0,     rescale     = 0;
  int change_cgms     = 0,     cgms        = 0;
  int change_wss      = 0,     wss         = 0;
  int change_vps      = 0;
  int change_cc_field = 0,     cc_field    = 0;
  struct stmfbio_vps vps;
  int change_hd_filter= 0,     hd_filter   = 0;
  int change_colourspace = 0,  colourspace = 0;
  int change_3d_mode  = 0,     mode_3d     = 0;
  int show            = 0;
  char *devname       = "/dev/fb0";
  int enables         = 0;
  int disables        = 0;
  int fullrange       = 0;
  int dvosync         = 0;
  int videorange      = 0;
  int change_ff_state = 0;
  int change_output_mode = 0;
  int stmfb_io_activate  = STMFBIO_ACTIVATE_IMMEDIATE;
  bool fullscreen = false;
  int dvo_data_clock_polarity    = 0;
  stmfbio_ff_state ff_state      = STMFBIO_FF_OFF;
  int change_mixer_background    = 0;
  int change_mixer_colorspace   = 0,  mixer_colorspace = 0;
  unsigned long mixer_background = 0;
  int option;

  /* Color Keying support */
  int change_ckey_state = 0;  /* change the color key state */
  int ckey_inverted     = 0;  /* color key mode (normal/inverted) */
  int ckey_state        = 0;  /* color key diabled */
  unsigned long min_colour_key = 0;
  unsigned long max_colour_key = 0;

  /* Framebuffer IOWindow management */
  int change_input_window  = 0;
  int change_output_window = 0;
  struct stmfbio_plane_rect in_rect   = {0};
  struct stmfbio_plane_rect out_rect  = {0};
  struct stmfbio_planeinfo2 planeInfo = {0};

  struct stmfbio_var_screeninfo_ex2 varEx = {0};
  struct stmfbio_output_configuration outputConfig = {0};
  struct stmfbio_3d_configuration config3D = {0};

  /* STMFB HDR Format support */
  int change_hdr_format   = 0;        /* change the output HDR Format configuration */
  char hdr_type[16]          = "SDR\n";
  char is_st2086_metadata_present[16] = "0\n";
  char is_hdr_metadata_present[16]    = "0\n";
  __u16 st2086_metadata[10];
  __u16 hdr_metadata[2];

  /* STMFB HDR Mode support */
  int change_hdr_mode = 0;             /* change the output HDR Mode configuration */
  stmfbio_output_hdr_mode_t hdr_mode = STMFBIO_OUTPUT_HDR_MODE_MANUAL;

  static struct option long_options[] = {
          { "transparency"           , 1, 0, 'a' },
          { "hdmi-deepcolour-bitdepth",1, 0, 'b' },
          { "enable-hd-dac-filters"  , 1, 0, 'B' },
          { "color-keying"           , 1, 0, 'c' },
          { "disable-output"         , 1, 0, 'd' },
          { "enable-output"          , 1, 0, 'e' },
          { "fb"                     , 1, 0, 'f' },
          { "gain"                   , 1, 0, 'g' },
          { "help"                   , 0, 0, 'h' },
          { "premultiplied-alpha"    , 0, 0, 'p' },
          { "non-premultiplied-alpha", 0, 0, 'n' },
          { "fullscreen-window"      , 1, 0, 'o' },
          { "show"                   , 0, 0, 's' },
          { "ycbcr-colourspace"      , 1, 0, 'C' },
          { "dvo-outputmode"         , 1, 0, 'D' },
          { "flicker-filter"         , 1, 0, 'F' },
          { "hdmi-colourspace"       , 1, 0, 'H' },
          { "dvo-data-clock-polarity", 1, 0, 'i' },
          { "input-window"           , 1, 0, 'j' },
          { "output-window"          , 1, 0, 'J' },
          { "mixer-background"       , 1, 0, 'M' },
          { "mixer-colorspace"       , 1, 0, 'm' },
          { "vps-data"               , 1, 0, 'P' },
          { "rescale-colour"         , 1, 0, 'R' },
          { "dvo-sync"               , 1, 0, 'S' },
          { "full-range"             , 1, 0, 'U' },
          { "video-range"            , 1, 0, 'V' },
          { "wss-insertion"          , 1, 0, 'w' },
          { "cgms-insertion"         , 1, 0, 'x' },
          { "3d-mode"                , 1, 0, '3' },
          { "enable-cc-field"        , 1, 0, 'z' },
          { "hdr-format"             , 1, 0, 'Y' },
          { "hdr-mode"               , 1, 0, 'y' },
          { "version"        	       , 0, 0, 'v' },
          { 0, 0, 0, 0 }
  };


  if(argc < 2)
  {
    show = 1;
  }

  while((option = getopt_long (argc, argv, "a:b:B:d:e:f:g:hpno:i:j:r:J:sC:D:F:H:M:m:P:R:S:Y:y:U:V:w:x:3:z:I:v", long_options, NULL)) != -1)
  {
    switch(option)
    {
      case 'a':
      {
        alpha = atoi(optarg);
        if(alpha<0 || alpha>255)
        {
          fprintf(stderr,"Alpha value out of range\n");
          exit(1);
        }
        else
        {
          change_alpha = 1;
        }

        break;
      }
      case 'b':
      {
        hdmi_bitdepth = atoi(optarg);
        switch(hdmi_bitdepth)
        {
          case 24:
            hdmi_bitdepth = STMFBIO_OUTPUT_HDMI_COLOURDEPTH_24BIT;
            break;
          case 30:
            hdmi_bitdepth = STMFBIO_OUTPUT_HDMI_COLOURDEPTH_30BIT;
            break;
          case 36:
            hdmi_bitdepth = STMFBIO_OUTPUT_HDMI_COLOURDEPTH_36BIT;
            break;
          case 48:
            hdmi_bitdepth = STMFBIO_OUTPUT_HDMI_COLOURDEPTH_48BIT;
            break;
          default:
            fprintf(stderr,"Invalid HDMI deepcolour mode\n");
            exit(1);
        }
        change_hdmi_bitdepth = 1;
        break;
      }
      case 'c':
      {
        int ret;
        ret = sscanf(optarg,"%01x %01x %08lx %08lx", &ckey_state, &ckey_inverted, &min_colour_key, &max_colour_key);
        if(ret == 4)
        {
          change_ckey_state = 1;
        }
        else
        {
          fprintf(stderr,"few argument (args number = %d)\n",ret);
          exit(1);
        }
        break;
      }
      case 'B':
      {
        switch(optarg[0])
        {
          case 'y':
          case 'Y':
            change_hd_filter = 1;
            hd_filter = 1;
            break;
          case 'n':
          case 'N':
            change_hd_filter = 1;
            hd_filter = 0;
            break;
          default:
            fprintf(stderr,"Unknown argument\n");
            exit(1);
        }
        break;
      }
      case 'd':
      {
        switch(optarg[0])
        {
          case 'c':
          case 'C':
            disables |= OUT_CVBS;
            break;
          case 'd':
          case 'D':
            disables |= OUT_DVO;
            break;
          case 'h':
          case 'H':
            disables |= OUT_HDMI;
            break;
          case 'r':
          case 'R':
            disables |= OUT_RGB;
            break;
          case 's':
          case 'S':
            disables |= OUT_SVIDEO;
            break;
          case 'y':
          case 'Y':
            disables |= OUT_YUV;
            break;
          default:
            fprintf(stderr,"Unknown disable output argument\n");
            exit(1);
        }
        break;
      }
      case 'C':
      {
        switch(optarg[0])
        {
          case 'a':
          case 'A':
            change_colourspace = 1;
            colourspace = STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_AUTO;
            break;
          case '6':
            change_colourspace = 1;
            colourspace = STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_601;
            break;
          case '7':
            change_colourspace = 1;
            colourspace = STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_709;
            break;
          case 'b':
            change_colourspace = 1;
            colourspace = STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_BT2020;
            break;
          default:
            fprintf(stderr,"Unknown YCbCr colourspace mode argument\n");
            exit(1);
        }
        break;
      }
      case 'D':
      {
        switch(optarg[0])
        {
          case 'i':
          case 'I':
            change_dvo_mode = 1;
            dvo_mode = STMFBIO_OUTPUT_DVO_ITUR656;
            break;
          case 'r':
          case 'R':
            change_dvo_mode = 1;
            dvo_mode = STMFBIO_OUTPUT_DVO_RGB_24BIT;
            break;
          case 'y':
          case 'Y':
            change_dvo_mode = 1;
            dvo_mode = STMFBIO_OUTPUT_DVO_YUV_444_16BIT;
            break;
          case '4':
            change_dvo_mode = 1;
            dvo_mode = STMFBIO_OUTPUT_DVO_YUV_422_16BIT;
            break;
          case '2':
            change_dvo_mode = 1;
            dvo_mode = STMFBIO_OUTPUT_DVO_YUV_444_24BIT;
            break;
          default:
            fprintf(stderr,"Unknown dvo output mode argument\n");
            exit(1);
        }
        break;
      }
      case 'e':
      {
        switch(optarg[0])
        {
          case 'c':
          case 'C':
            enables |= OUT_CVBS;
            break;
          case 'd':
          case 'D':
            enables |= OUT_DVO;
            break;
          case 'h':
          case 'H':
            enables |= OUT_HDMI;
            break;
          case 'r':
          case 'R':
            enables |= OUT_RGB;
            break;
          case 's':
          case 'S':
            enables |= OUT_SVIDEO;
            break;
          case 'y':
          case 'Y':
            enables |= OUT_YUV;
            break;
          default:
            fprintf(stderr,"Unknown enable output argument\n");
            exit(1);
        }
        break;
      }
      case 'f':
      {
        devname = optarg;
        break;
      }
      case 'F':
      {
        switch(optarg[0])
        {
          case 'a':
          case 'A':
            change_ff_state = 1;
            ff_state = STMFBIO_FF_ADAPTIVE;
            break;
          case 's':
          case 'S':
            change_ff_state = 1;
            ff_state = STMFBIO_FF_SIMPLE;
            break;
          case 'o':
          case 'O':
            change_ff_state = 1;
            ff_state = STMFBIO_FF_OFF;
            break;
          default:
            fprintf(stderr,"Unknown flicker filter argument\n");
            exit(1);
        }
        break;
      }
      case 'i':
      {
        switch(optarg[0])
        {
          case 'o':
          case 'O':
            dvo_data_clock_polarity = DVO_DATA_CLOCK_INVERTED;
            break;
          case 's':
          case 'S':
            dvo_data_clock_polarity = DVO_DATA_CLOCK_NOT_INVERTED;
            break;
          default:
            fprintf(stderr,"Unknown dvo data clock polarity argument\n");
            exit(1);
        }
        break;
      }
      case 'j':
      {
        int ret;
        ret = sscanf(optarg,"%d %d %d %d", &in_rect.x, &in_rect.y, &in_rect.dim.w, &in_rect.dim.h);
        if(ret == 4)
        {
          change_input_window = 1;
        }
        else
        {
          fprintf(stderr,"few argument (args number = %d)\n",ret);
          exit(1);
        }
        break;
      }
      case 'J':
      {
        int ret;
        ret = sscanf(optarg,"%d %d %d %d", &out_rect.x, &out_rect.y, &out_rect.dim.w, &out_rect.dim.h);
        if(ret == 4)
        {
          change_output_window = 1;
        }
        else
        {
          fprintf(stderr,"few argument (args number = %d)\n",ret);
          exit(1);
        }
        break;
      }
      case 'g':
      {
        gain = atoi(optarg);
        if(gain<0 || gain>255)
        {
          fprintf(stderr,"gain value out of range\n");
          exit(1);
        }
        else
        {
          change_gain = 1;
        }

        break;
      }
      case 'h':
      {
        usage();
        break;
      }
      case 'H':
      {
        switch(optarg[0])
        {
          case 'r':
          case 'R':
            change_hdmi_colour = 1;
            hdmi_colour = 0;
            break;
          case 'y':
          case 'Y':
            change_hdmi_colour = 1;
            hdmi_colour = 1;
            break;
          case '4':
            switch(atoi(optarg))
            {
               case 422:
                 change_hdmi_colour = 1;
                 hdmi_colour = 2;
                 break;
               case 420:
                 change_hdmi_colour = 1;
                 hdmi_colour = 3;
                 break;
               default:
                 fprintf(stderr,"Unknown hdmi colourspace argument\n");
                 exit(1);
               break;
            }
            break;
          default:
            fprintf(stderr,"Unknown hdmi colourspace argument\n");
            exit(1);
        }
        break;
      }
      case 'o':
      {
        change_output_mode = 1;
        switch(optarg[0])
        {
          case '0':
            fullscreen = false;
          break;
          case '1':
            fullscreen = true;
          break;
          default:
            fprintf(stderr,"0 manual output window 1 fullscreen window\n");
            exit(1);
        }
        break;
      }
      case 'p':
      {
        change_blend = 1;
        blend = 1;
        break;
      }
      case 'n':
      {
        change_blend = 1;
        blend = 0;
        break;
      }
      case 's':
      {
        show = 1;
        break;
      }
      case 'R':
      {
        switch(optarg[0])
        {
          case 'f':
          case 'F':
            change_rescale = 1;
            rescale = 0;
            break;
          case 'v':
          case 'V':
            change_rescale = 1;
            rescale = 1;
            break;
          default:
            fprintf(stderr,"Unknown rescale argument\n");
            exit(1);
            break;
        }
        break;
      }
      case 'M':
      {
        errno = 0;
        mixer_background = strtoul(optarg,NULL,16);
        if(errno != 0)
        {
          exit_with_error_message("Invalid mixer background colour argument");
          exit(1);
        }
        else
        {
          change_mixer_background = 1;
        }

        break;
      }
      case 'm':
      {
        switch(optarg[0])
        {
          case 'a':
          case 'A':
            change_mixer_colorspace = 1;
            mixer_colorspace = STMFBIO_OUTPUT_MIXER_COLORSPACE_AUTO;
            break;
          case '6':
            change_mixer_colorspace = 1;
            mixer_colorspace = STMFBIO_OUTPUT_MIXER_COLORSPACE_601;
            break;
          case '7':
            change_mixer_colorspace = 1;
            mixer_colorspace = STMFBIO_OUTPUT_MIXER_COLORSPACE_709;
            break;
          case '2':
            change_mixer_colorspace = 1;
            mixer_colorspace = STMFBIO_OUTPUT_MIXER_COLORSPACE_2020;
            break;
          default:
            fprintf(stderr,"Unknown YCbCr Mixer colourspace mode argument\n");
            exit(1);
        }
        break;
      }
      case 'x':
      {
        change_cgms = 1;
        cgms = atoi(optarg);
        break;
      }
      case 'w':
      {
        switch(optarg[0])
        {
          case 'e':
          case 'E':
            change_wss = 1;
            wss = 1;
            break;
          case 'd':
          case 'D':
            change_wss = 1;
            wss = 0;
            break;
          default:
            fprintf(stderr,"Unknown wss argument\n");
            exit(1);
            break;
        }
        break;
      }
      case 'P':
      {
        int ret, vpsenable;
        int vpsdata[6];
        ret = sscanf(optarg,"%01d,%02d,%02d,%02d,%02d,%02d,%02d", &vpsenable, &vpsdata[0], &vpsdata[1], &vpsdata[2], &vpsdata[3], &vpsdata[4], &vpsdata[5]);
        if(vpsenable == 0)
        {
           change_vps = 1;
           vps.vps_enable = vpsenable;
        }
        else
        {
          if(ret == 7)
          {
            change_vps = 1;
            vps.vps_enable = vpsenable;
            vps.vps_data[0] = vpsdata[0];
            vps.vps_data[1] = vpsdata[1];
            vps.vps_data[2] = vpsdata[2];
            vps.vps_data[3] = vpsdata[3];
            vps.vps_data[4] = vpsdata[4];
            vps.vps_data[5] = vpsdata[5];
          }
          else
          {
            fprintf(stderr,"few argument (args number = %d)\n",ret);
            exit(1);
          }
        }
        break;
      }
      case 'S':
      {
        switch(optarg[1])
        {
          case 'm':
          case 'M':
            dvosync = EMBEDDED_SYNC;
            break;
          case 'x':
          case 'X':
            dvosync = EXTERNAL_SYNC;
            break;
          default:
            fprintf(stderr,"Unknown dvo output sync argument, should be [ext|emb]\n");
            exit(1);
        }
        break;
      }
      case 'U':
      {
        switch(optarg[0])
        {
          case 'a':
          case 'A':
            fullrange |= OUT_CVBS;
            break;
          case 'd':
          case 'D':
            fullrange |= OUT_DVO;
            break;
          case 'h':
          case 'H':
            fullrange |= OUT_HDMI;
            break;
          default:
            fprintf(stderr,"Unknown full-range output argument\n");
            exit(1);
        }
        break;
      }
      case 'v':
      {
        printf("stfbset version %s\n", version);
	exit(0);
      }
      case 'V':
      {
        switch(optarg[0])
        {
          case 'a':
          case 'A':
            videorange |= OUT_CVBS;
            break;
          case 'd':
          case 'D':
            videorange |= OUT_DVO;
            break;
          case 'h':
          case 'H':
            videorange |= OUT_HDMI;
            break;
          default:
            fprintf(stderr,"Unknown video-range output argument\n");
            exit(1);
        }
        break;
      }
      case '3':
      {
        switch(optarg[0])
        {
          case '2':
            mode_3d = STMFBIO_3D_NONE;
            break;
          case 's':
          case 'S':
            mode_3d = STMFBIO_3D_SBS_HALF;
            break;
          case 't':
          case 'T':
            mode_3d = STMFBIO_3D_TOP_BOTTOM;
            break;
          default:
            fprintf(stderr,"Unknown 3D output argument\n");
            exit(1);
        }
        change_3d_mode = 1;
        break;
      }
      case 'z':
        change_cc_field = 1;
        cc_field = atoi(optarg);
        if((cc_field <0)||(cc_field>3))
        {
          fprintf(stderr,"enable cc field value out of range\n");
          exit(1);
        }
      break;
      case 'I':
        switch(optarg[0])
        {
          case 'i':
          case 'I':
            stmfb_io_activate = STMFBIO_ACTIVATE_IMMEDIATE;
            fprintf(stderr,"STMFBIO_ACTIVATE_IMMEDIATE\n");
            break;
          case 'c':
          case 'C':
            stmfb_io_activate = STMFBIO_ACTIVATE_ON_NEXT_CHANGE;
            fprintf(stderr,"STMFBIO_ACTIVATE_ON_NEXT_CHANGE\n");
            break;
          default:
            fprintf(stderr,"INVALID\n");
            exit(1);
        }
      break;
      case 'Y':
      {
        if(optarg)
        {
          int ret = 0;
          ret = sscanf(optarg,"%s %s %s %hu %hu %hu %hu %hu %hu %hu %hu %hu %hu %hu %hu",
                       hdr_type,
                       is_st2086_metadata_present,
                       is_hdr_metadata_present,
                       &st2086_metadata[0],
                       &st2086_metadata[1],
                       &st2086_metadata[2],
                       &st2086_metadata[3],
                       &st2086_metadata[4],
                       &st2086_metadata[5],
                       &st2086_metadata[6],
                       &st2086_metadata[7],
                       &st2086_metadata[8],
                       &st2086_metadata[9],
                       &hdr_metadata[0],
                       &hdr_metadata[1]);
          if(ret < 2)
          {
            fprintf(stderr,"few HDR Format arguments (args number = %d)\n",ret);
            exit(1);
          }
          change_hdr_format = 1;
        }
      }
      break;
      case 'y':
      {
        switch(optarg[0])
        {
          case 'a':
          case 'A':
            change_hdr_mode = 1;
            hdr_mode = STMFBIO_OUTPUT_HDR_MODE_AUTO;
            break;
          case 'm':
          case 'M':
            change_hdr_mode = 1;
            hdr_mode = STMFBIO_OUTPUT_HDR_MODE_MANUAL;
            break;
          default:
            fprintf(stderr,"Unknown output HDR mode argument\n");
            exit(1);
        }
      }
      break;
      default:
      {
        usage();
        exit(1);
      }
    }
  }

  if((fbfd = open(devname, O_RDWR)) < 0)
  {
    fprintf (stderr, "Unable to open %s: %d (%m)\n", devname, errno);
    exit(1);
  }

  if(change_cgms)
  {
    ioctl(fbfd, STMFBIO_SET_CGMS_ANALOG, cgms);
  }

  if(change_wss)
  {
    ioctl(fbfd, STMFBIO_SET_WSS_ANALOG, wss);
  }

  if(change_vps)
  {
    if (ioctl(fbfd, STMFBIO_SET_VPS_ANALOG, &vps)<0)
      exit_with_error_message("setting VPS data failed");
  }

  if(change_cc_field)
  {
    if (ioctl(fbfd, STMFBIO_SET_CLOSED_CAPTION, cc_field)<0)
      exit_with_error_message("setting Closed Caption field failed");
  }

  if(change_hd_filter)
  {
    ioctl(fbfd, STMFBIO_SET_DAC_HD_FILTER, hd_filter);
  }

  varEx.layerid  = 0;
  varEx.caps     = 0;
  varEx.activate = stmfb_io_activate;

  outputConfig.outputid = STMFBIO_OUTPUTID_MAIN;
  if(ioctl(fbfd, STMFBIO_GET_OUTPUT_CONFIG, &outputConfig)<0)
    exit_with_error_message("Getting current output configuration failed");

  outputConfig.caps = 0;
  outputConfig.activate = stmfb_io_activate;

  config3D.outputid          = STMFBIO_OUTPUTID_MAIN;
  config3D.activate          = stmfb_io_activate;
  config3D.framebuffer_type  = STMFBIO_3D_FB_MONO;
  config3D.framebuffer_depth = 0;
  config3D.mode              = mode_3d;

  if(change_3d_mode)
  {
    if(ioctl(fbfd, STMFBIO_SET_3D_CONFIG, &config3D)<0)
      exit_with_error_message("setting 3D configuration failed");
  }

  if(change_alpha)
  {
    varEx.caps |= STMFBIO_VAR_CAPS_OPACITY;
    varEx.opacity = alpha;
  }

  if(change_gain)
  {
    varEx.caps |= STMFBIO_VAR_CAPS_GAIN;
    varEx.gain  = gain;
  }

  if(change_blend)
  {
    varEx.caps |= STMFBIO_VAR_CAPS_PREMULTIPLIED;
    varEx.premultiplied_alpha = blend;
  }

  if(change_rescale)
  {
    varEx.caps |= STBFBIO_VAR_CAPS_RESCALE_COLOUR_TO_VIDEO_RANGE;
    varEx.rescale_colour_to_video_range = rescale;
  }

  if(change_ff_state)
  {
    varEx.caps |= STMFBIO_VAR_CAPS_FLICKER_FILTER;
    varEx.ff_state = ff_state;
  }

  if(change_ckey_state)
  {
    varEx.caps |= STMFBIO_VAR_CAPS_COLOURKEY;
    if(ckey_state)
    {
      varEx.colourKeyFlags |= STMFBIO_COLOURKEY_FLAGS_ENABLE;
    }
    if(ckey_inverted)
    {
      varEx.colourKeyFlags |= STMFBIO_COLOURKEY_FLAGS_INVERT;
    }
    varEx.min_colour_key = min_colour_key;
    varEx.max_colour_key = max_colour_key;
  }

  if(change_output_mode)
  {
    varEx.caps |= STMFBIO_VAR_CAPS_FULLSCREEN;
    varEx.fullscreen = fullscreen;
  }

  if(change_mixer_background)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_MIXER_BACKGROUND;
    outputConfig.mixer_background = mixer_background;
  }

  if(change_hdmi_colour)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
    outputConfig.hdmi_config &= ~(STMFBIO_OUTPUT_HDMI_YUV|STMFBIO_OUTPUT_HDMI_422|STMFBIO_OUTPUT_HDMI_420);

    switch(hdmi_colour)
    {
      case 1:
        outputConfig.hdmi_config |= STMFBIO_OUTPUT_HDMI_YUV;
        break;
      case 2:
        outputConfig.hdmi_config |= (STMFBIO_OUTPUT_HDMI_YUV|STMFBIO_OUTPUT_HDMI_422);
        break;
      case 3:
        outputConfig.hdmi_config |= (STMFBIO_OUTPUT_HDMI_YUV|STMFBIO_OUTPUT_HDMI_420);
        break;
      default:
        break;
    }
  }

  if(change_hdmi_bitdepth)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
    outputConfig.hdmi_config &= ~STMFBIO_OUTPUT_HDMI_COLOURDEPTH_MASK;
    outputConfig.hdmi_config |= hdmi_bitdepth;
  }

  if(change_dvo_mode)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
    outputConfig.dvo_config &= ~STMFBIO_OUTPUT_DVO_MODE_MASK;
    outputConfig.dvo_config |= dvo_mode;
  }

  if(change_colourspace)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
    outputConfig.analogue_config &= ~STMFBIO_OUTPUT_ANALOGUE_COLORSPACE_MASK;
    outputConfig.analogue_config |= colourspace;
  }

  if(change_mixer_colorspace)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_MIXER_COLORSPACE;
    outputConfig.mixer_colorspace = mixer_colorspace;
  }

  if(disables)
  {
    if(disables&OUT_CVBS)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config &= ~STMFBIO_OUTPUT_ANALOGUE_CVBS;
    }
    if(disables&OUT_SVIDEO)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config &= ~STMFBIO_OUTPUT_ANALOGUE_YC;
    }
    if(disables&OUT_YUV)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config &= ~STMFBIO_OUTPUT_ANALOGUE_YPrPb;
    }
    if(disables&OUT_RGB)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config &= ~STMFBIO_OUTPUT_ANALOGUE_RGB;
    }
    if(disables&OUT_HDMI)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
      outputConfig.hdmi_config |= STMFBIO_OUTPUT_HDMI_DISABLED;
    }
    if(disables&OUT_DVO)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
      outputConfig.dvo_config |= STMFBIO_OUTPUT_DVO_DISABLED;
    }
  }

  if(enables)
  {
    if(enables&OUT_CVBS)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config |= STMFBIO_OUTPUT_ANALOGUE_CVBS;
    }
    if(enables&OUT_SVIDEO)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config |= STMFBIO_OUTPUT_ANALOGUE_YC;
    }
    if(enables&OUT_YUV)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config |= STMFBIO_OUTPUT_ANALOGUE_YPrPb;
    }
    if(enables&OUT_RGB)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config |= STMFBIO_OUTPUT_ANALOGUE_RGB;
    }
    if(enables&OUT_HDMI)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
      outputConfig.hdmi_config &= ~STMFBIO_OUTPUT_HDMI_DISABLED;
    }
    if(enables&OUT_DVO)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
      outputConfig.dvo_config &= ~STMFBIO_OUTPUT_DVO_DISABLED;
    }
  }


  if(fullrange)
  {
    if(fullrange&OUT_CVBS)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config |= STMFBIO_OUTPUT_ANALOGUE_CLIP_FULLRANGE;
    }
    if(fullrange&OUT_HDMI)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
      outputConfig.hdmi_config |= STMFBIO_OUTPUT_HDMI_CLIP_FULLRANGE;
    }
    if(fullrange&OUT_DVO)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
      outputConfig.dvo_config |= STMFBIO_OUTPUT_DVO_CLIP_FULLRANGE;
    }
  }


  if(dvosync)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
    if(dvosync&EMBEDDED_SYNC)
    {
      outputConfig.dvo_config |= STMFBIO_OUTPUT_DVO_EMBEDDED_SYNC;
    }
    else
    {
      outputConfig.dvo_config &= ~STMFBIO_OUTPUT_DVO_EMBEDDED_SYNC;
    }
  }


  if(dvo_data_clock_polarity)
  {
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
    if(dvo_data_clock_polarity&DVO_DATA_CLOCK_INVERTED)
    {
      outputConfig.dvo_config |= STMFBIO_OUTPUT_DVO_INVERT_DATA_CLOCK;
    }
    else
    {
      outputConfig.dvo_config &= ~STMFBIO_OUTPUT_DVO_INVERT_DATA_CLOCK;
    }
  }


  if(videorange)
  {
    if(videorange&OUT_CVBS)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_ANALOGUE_CONFIG;
      outputConfig.analogue_config &= ~STMFBIO_OUTPUT_ANALOGUE_CLIP_FULLRANGE;
    }
    if(videorange&OUT_HDMI)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_HDMI_CONFIG;
      outputConfig.hdmi_config &= ~STMFBIO_OUTPUT_HDMI_CLIP_FULLRANGE;
    }
    if(videorange&OUT_DVO)
    {
      outputConfig.caps |= STMFBIO_OUTPUT_CAPS_DVO_CONFIG;
      outputConfig.dvo_config &= ~STMFBIO_OUTPUT_DVO_CLIP_FULLRANGE;
    }
  }

  if(change_hdr_mode)
  {
    outputConfig.caps     |= STMFBIO_OUTPUT_CAPS_MIXER_HDR_MODE;
    outputConfig.hdr_mode  = hdr_mode;
  }

  if(change_hdr_format)
  {
    if((!strcmp(hdr_type,"SDR")) || (!strcmp(hdr_type,"sdr")))
    {
      outputConfig.hdr_format.eotf_type = STMFBIO_EOTF_GAMMA_SDR;
    }
    else if((!strcmp(hdr_type,"HDR")) || (!strcmp(hdr_type,"hdr")))
    {
      outputConfig.hdr_format.eotf_type = STMFBIO_EOTF_GAMMA_HDR;
    }
    else if((!strcmp(hdr_type,"ST2084")) || (!strcmp(hdr_type,"st2084")))
    {
      outputConfig.hdr_format.eotf_type = STMFBIO_EOTF_SMPTE_ST2084;
    }
    else if((!strcmp(hdr_type,"HLG")) || (!strcmp(hdr_type,"hlg")))
    {
      outputConfig.hdr_format.eotf_type = STMFBIO_EOTF_GAMMA_HDR_HLG;
    }
    else
    {
      fprintf(stderr,"Unknown HDR EOTF Type argument\n");
      exit(1);
    }
    outputConfig.caps |= STMFBIO_OUTPUT_CAPS_MIXER_HDR_FORMAT;
    outputConfig.hdr_format.is_st2086_metadata_present = strtoul(is_st2086_metadata_present,NULL,8);
    outputConfig.hdr_format.is_hdr_metadata_present    = strtoul(is_hdr_metadata_present,NULL,8);
    if(outputConfig.hdr_format.is_st2086_metadata_present)
    {
      outputConfig.hdr_format.st2086_metadata.display_primaries_x_0  = st2086_metadata[0];
      outputConfig.hdr_format.st2086_metadata.display_primaries_y_0  = st2086_metadata[1];
      outputConfig.hdr_format.st2086_metadata.display_primaries_x_1  = st2086_metadata[2];
      outputConfig.hdr_format.st2086_metadata.display_primaries_y_1  = st2086_metadata[3];
      outputConfig.hdr_format.st2086_metadata.display_primaries_x_2  = st2086_metadata[4];
      outputConfig.hdr_format.st2086_metadata.display_primaries_y_2  = st2086_metadata[5];
      outputConfig.hdr_format.st2086_metadata.white_point_x          = st2086_metadata[6];
      outputConfig.hdr_format.st2086_metadata.white_point_y          = st2086_metadata[7];

      outputConfig.hdr_format.st2086_metadata.max_display_mastering_luminance = st2086_metadata[8];
      outputConfig.hdr_format.st2086_metadata.min_display_mastering_luminance = st2086_metadata[9];
    }
    else
    {
      memset(&outputConfig.hdr_format.st2086_metadata, 0, sizeof(stmfbio_st2086_metadata_t));
    }

    if(outputConfig.hdr_format.is_hdr_metadata_present)
    {
      outputConfig.hdr_format.hdr_metadata.maxCLL  = hdr_metadata[0];
      outputConfig.hdr_format.hdr_metadata.maxFALL = hdr_metadata[1];
    }
    else
    {
      memset(&outputConfig.hdr_format.hdr_metadata, 0, sizeof(stmfbio_hdr_metadata_t));
    }
  }

  if(varEx.caps != STMFBIO_VAR_CAPS_NONE)
  {
    if(ioctl(fbfd, STMFBIO_SET_VAR_SCREENINFO_EX2, &varEx)<0)
      exit_with_error_message("setting extended screen info failed");
  }

  if(outputConfig.caps != STMFBIO_OUTPUT_CAPS_NONE)
  {
    if(ioctl(fbfd, STMFBIO_SET_OUTPUT_CONFIG, &outputConfig)<0)
      exit_with_error_message("setting output configuration failed");
  }

  if((change_input_window) || (change_output_window))
  {
    if(ioctl(fbfd, STMFBIO_GET_PLANEMODE2, &planeInfo)<0)
      exit_with_error_message("setting extended screen info failed");

    if(change_input_window)
    {
      planeInfo.config.source = in_rect;
      printf("new input rect --> %dx%d@%d-%d\n", in_rect.dim.w, in_rect.dim.h,
              in_rect.x, in_rect.y);
    }
    if(change_output_window)
    {
      planeInfo.config.dest   = out_rect;
      printf("new output rect --> %dx%d@%d-%d\n", out_rect.dim.w, out_rect.dim.h,
              out_rect.x, out_rect.y);
    }
    if(ioctl(fbfd, STMFBIO_SET_PLANEMODE2, &planeInfo)<0)
      exit_with_error_message("setting plane info failed");
  }

  if(show)
  {
    if(ioctl(fbfd, STMFBIO_GET_OUTPUT_CONFIG, &outputConfig)<0)
      exit_with_error_message("Getting current output configuration failed");
    if(ioctl(fbfd, STMFBIO_GET_VAR_SCREENINFO_EX2, &varEx)<0)
      exit_with_error_message("Getting extended screen info failed");
    if(ioctl(fbfd, STMFBIO_GET_PLANEMODE2, &planeInfo)<0)
      exit_with_error_message("Getting current plane info failed");

    show_info(&varEx,&outputConfig);
    show_plane_info(&planeInfo);
    show_hdr_info(&varEx,&outputConfig);
  }

  return 0;
}
