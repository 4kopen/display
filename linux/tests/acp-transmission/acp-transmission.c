/*
 * acp-transmission.c
 *
 * Copyright (C) STMicroelectronics Limited 2008. All rights reserved.
 *
 * Test to exercise the sending of ACP info frames
 */
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <getopt.h>


#include <linux/kernel/drivers/stm/hdmi/stmhdmi.h>

#define HDMIDEV "/dev/hdmi0.0"

static struct option long_options[] = {
  { "help", 0, 0, 'h' },
  { "version", 0, 0, 'V' },
  { 0, 0, 0, 0 }
};
char version[]="1.0";

static void issue_help_message(void)
{
  printf("Usage: acp-transmission [OPTIONS]\n");
  printf("Send ACP packet to the connected HDMI device.\n");
  printf("\n");
  printf("No option is mandatory to send ACP packet.\n");
  printf("  -h, --help               Display this help, then exit.\n");
  printf("  -V, --version            Show version of acp-transmission.\n");
  printf("\n");
}

int main(int argc, char *argv[])
{
        struct stmhdmiio_data_packet acp = {};
        int fd;
        int result;
        int option;

        while ((option = getopt_long (argc, argv, "hV", long_options, NULL)) != -1) {
          switch (option) {
          case 'h':
            issue_help_message();
            exit(0);
          case 'V':
            printf("acp-transmission version %s\n",version);
            exit(0);
          }
        }
        fd = open(HDMIDEV, O_RDWR);
        assert(fd);

        acp.type = 4;
        if (argc >= 2)
                acp.version = strtol(argv[1], NULL, 0);
        else
                acp.version = 1;

        result = ioctl(fd, STMHDMIIO_SEND_DATA_PACKET, &acp);
        if(result<0)
          perror("failed");

        close(fd);

        return 0;
}
