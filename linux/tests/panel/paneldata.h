/***********************************************************************
 *
 * File: linux/tests/panel/paneldata.h
 * Copyright (c) 2012 STMicroelectronics Limited.
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License version 2.  See the file COPYING in the main directory of this archive for
 * more details.
 *
 \***********************************************************************/

#ifndef _PANELDATA_H
#define _PANELDATA_H

#include "cmo_m216h1_lo1.h"
#include "cmo_m236h5_loa.h"

#endif /* _PANELDATA_H */
